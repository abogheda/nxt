// ignore_for_file: depend_on_referenced_packages

import 'dart:developer';

import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_native_splash/flutter_native_splash.dart';

import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:nxt/config/router/router.dart';
import 'package:nxt/data/service/hive_services.dart';
import 'package:nxt/utils/helpers/custom_notification_helper.dart';
import 'package:nxt/utils/helpers/flutter_local_notifications.dart';
import 'package:nxt/utils/helpers/locator.dart';
import 'package:nxt/utils/in_app_purchases_helper/dash_counter.dart';
import 'package:nxt/utils/in_app_purchases_helper/dash_purchase.dart';
import 'package:nxt/view/screens/challenges/cubit/challenge_cubit.dart';
import 'package:nxt/view/screens/complete_profile_screen/cubit/complete_profile_cubit.dart';
import 'package:nxt/view/screens/complete_profile_screen/widgets/screen_sections/select_vertical_screen.dart';
import 'package:nxt/view/screens/home/cubit/home_cubit.dart';
import 'package:nxt/view/screens/learn/master_classes/cubit/class_cubit.dart';
import 'package:nxt/view/screens/navigation_and_appbar/import_widget.dart';
import 'package:nxt/view/screens/welcome_to_project/check_box_cubit/check_box_cubit.dart';
import 'package:nxt/view/screens/welcome_to_project/welcome_to_project_imports.dart';
import 'package:provider/provider.dart';
import 'package:timeago/timeago.dart' as timeago;

import 'config/theme/theme.dart';
import 'firebase_options.dart';
import 'utils/bloc_observer/bloc_observer.dart';
import 'view/screens/Language/language_screen.dart';

void setTimeAgo() {
  timeago.setLocaleMessages('ar', timeago.ArMessages());
  timeago.setLocaleMessages('ar_short', timeago.ArShortMessages());
  timeago.setLocaleMessages('en', timeago.EnMessages());
  timeago.setLocaleMessages('en_short', timeago.EnShortMessages());
}

Future<void> main() async {
  WidgetsBinding widgetsBinding = WidgetsFlutterBinding.ensureInitialized();
  FlutterNativeSplash.preserve(widgetsBinding: widgetsBinding);
  Future.delayed(const Duration(milliseconds: 1500)).then((_) => FlutterNativeSplash.remove());
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);
  await Firebase.initializeApp(options: DefaultFirebaseOptions.currentPlatform);
  await NotificationHelper.instance.setupNotification();
  NotificationHelper.instance.getToken().then((token) => log('fcm token => ${token ?? 'token here not available'}'));
  await FirebaseAnalytics.instance.logAppOpen();
  locatorSetup();
  await EasyLocalization.ensureInitialized();
  await HiveHelper.init();
  await ScreenUtil.ensureScreenSize();
  setTimeAgo();
  if (HiveHelper.getKeepMeLoggedIn == false) await HiveHelper.logout();
  bool keepMe = HiveHelper.getKeepMeLoggedIn ?? false;
  bool? isFirstTime = await HiveHelper.getOnBoarding;
  bool isNotCompleted = await HiveHelper.isNotCompleted;
  bool isRegularUser = HiveHelper.isRegularUser;
  bool isLogged = HiveHelper.isLogged;
  if (kDebugMode) {
    print("token : ${HiveHelper.getUserToken}");
  }
  final home = isFirstTime
      ? const LanguageScreen()
      : keepMe
          ? isLogged
              ? isNotCompleted
                  ? isRegularUser
                      ? const SelectVerticalScreen()
                      : Navigation()
                  : Navigation()
              : const WelcomeToProject()
          : const WelcomeToProject();

  // ignore: deprecated_member_use
  BlocOverrides.runZoned(
    () async {
      WidgetsFlutterBinding.ensureInitialized();
      FlutterLocalNotificationUtils.instance.init();
      runApp(EasyLocalization(
        path: 'assets/lang',
        supportedLocales: const [Locale('en', 'US'), Locale('ar', 'EG')],
        fallbackLocale: const Locale('en', 'US'),
        child: MyApp(home: home),
      ));
    },
    blocObserver: MyBlocObserver(),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({
    Key? key,
    required this.home,
  }) : super(key: key);
  final Widget home;

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<DashCounter>(create: (_) => DashCounter()),
        ChangeNotifierProvider<DashPurchases>(
          create: (context) => DashPurchases(
            context.read<DashCounter>(),
          ),
          lazy: false,
        ),
      ],
      child: ScreenUtilInit(
          designSize: const Size(360, 690),
          builder: (BuildContext context, Widget? widget) {
            return MultiBlocProvider(
              providers: [
                BlocProvider(
                    create: (context) => HomeCubit()
                      ..getHomeCarouselsData()
                      ..getAllHomeData()
                      ..getUserDataValidate()),
                BlocProvider(
                  create: (context) => ClassCubit()..getAllClasses(filter: context.read<HomeCubit>().categoryType),
                ),
                BlocProvider(create: (context) => CheckboxCubit()),
                BlocProvider(create: (context) => CompleteProfileCubit()),
                BlocProvider(
                  create: (context) => ChallengeCubit()
                    ..getAllChallenges(filter: context.read<HomeCubit>().categoryType)
                    ..getJoinedChallenges(filter: context.read<HomeCubit>().categoryType)
                    ..getWonChallenges(filter: context.read<HomeCubit>().categoryType),
                ),
              ],
              child: MaterialApp(
                debugShowCheckedModeBanner: false,
                title: 'Project NXT',
                localizationsDelegates: context.localizationDelegates,
                supportedLocales: context.supportedLocales,
                locale: context.locale,
                themeMode: ThemeMode.light,
                theme: lightTheme(context),
                darkTheme: darkTheme(context),
                onGenerateRoute: onGenerateRoute,
                navigatorKey: navigatorKey,
                home: home,
              ),
            );
          }),
    );
  }
}
