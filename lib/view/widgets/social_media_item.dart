
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class SocialMediaItem extends StatelessWidget {
final String image;
final Function() onTap;
  const SocialMediaItem({Key? key, required this.image, required this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 3),
      child: InkWell(
        onTap: onTap,
        child: CircleAvatar(
          backgroundColor: Colors.white,
          radius: 22,
          child: SvgPicture.asset(image,),
        ),
      ),
    );
  }
}
