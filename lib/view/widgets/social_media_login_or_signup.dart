import 'package:flutter/material.dart';
import 'package:nxt/view/widgets/social_media_item.dart';


class SocialMediaLoginOrSignUp extends StatelessWidget {
  const SocialMediaLoginOrSignUp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SocialMediaItem(image: 'assets/images/facebook.svg', onTap: () {  },),
        SocialMediaItem(image: 'assets/images/apple.svg', onTap: () {  },),
        SocialMediaItem(image: 'assets/images/g+.svg', onTap: () {  },),
        SocialMediaItem(image: 'assets/images/tiktok.svg', onTap: () {  },),
        SocialMediaItem(image: 'assets/images/snapchat.svg', onTap: () {  },),
      ],
    );
  }
}
