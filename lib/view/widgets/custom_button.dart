// ignore_for_file: deprecated_member_use

import 'package:flutter/material.dart';
import '../../utils/constants/resources.dart';

class CustomButton extends StatelessWidget {
  final void Function()? onTap;
  final double? width;
  final Color? color;
  final Color? borderColor;
  final Color? textColor;
  final double? height;
  final Widget? child;
  const CustomButton({
    Key? key,
    required this.onTap,
    this.borderColor,
    this.width,
    this.color,
    this.textColor,
    this.height,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: width,
      height: height,
      child: ElevatedButton(
          onPressed: onTap,
          style: ElevatedButton.styleFrom(
            side: BorderSide(color: borderColor ?? Colors.transparent),
            primary: color ?? Theme.of(context).primaryColor,
            textStyle: AppTextStyles.bold13.copyWith(color: textColor ?? Colors.white),
          ),
          child: child),
    );
  }
}
