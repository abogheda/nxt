import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../../data/service/hive_services.dart';
import '../../utils/constants/app_const.dart';

class LogoAppBar extends StatelessWidget {
  const LogoAppBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        SizedBox(height: MediaQuery.of(context).padding.top + height * 0.02),
        Center(
            child: SvgPicture.asset(
          // HiveHelper.isEgypt ?
          'assets/images/fab_logo.svg'
          // : 'assets/images/logo.svg'
          ,
          height: HiveHelper.isEgypt ? height * 0.1 : height * 0.09,
        )),
      ],
    );
  }
}
