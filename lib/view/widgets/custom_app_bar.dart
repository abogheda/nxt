import 'package:flutter/material.dart';

class CustomAppBar extends StatelessWidget implements PreferredSizeWidget {
  final bool isHome;
  final double? elevation;
  final Widget? actionWidget;
  final String title;
  const CustomAppBar({
    Key? key,
    this.isHome = false,
    this.elevation,
    this.actionWidget,
    required this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: Text(title),
      elevation: elevation,
      actions: actionWidget != null ? [actionWidget!] : null,
    );
  }

  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);
}
