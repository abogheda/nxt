import 'package:flutter/material.dart';

import 'app_loader.dart';

class LoadingStateWidget extends StatelessWidget {
  const LoadingStateWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Center(child: AppLoader());
  }
}
