import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

import '../../utils/constants/app_const.dart';

class PickImageDialog extends StatelessWidget {
  const PickImageDialog({super.key, required this.pickFromCamera, required this.pickFromGallery});
  final Function() pickFromCamera;
  final Function() pickFromGallery;
  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(16)),
      elevation: 0,
      backgroundColor: Colors.transparent,
      child: Container(
        height: height * 0.25,
        decoration: const BoxDecoration(
            color: Colors.white, shape: BoxShape.rectangle, borderRadius: BorderRadius.all(Radius.circular(12))),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text('captureOrChoose'.tr(),
                textAlign: TextAlign.center,
                style: Theme.of(context)
                    .textTheme
                    .titleMedium!
                    .copyWith(fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal')),
            const SizedBox(height: 24),
            Row(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                InkWell(
                  onTap: pickFromCamera,
                  child: Container(
                    padding: const EdgeInsets.all(24),
                    decoration:
                        BoxDecoration(borderRadius: BorderRadius.circular(12), border: Border.all(color: Colors.black)),
                    child: const Icon(Icons.camera_alt_rounded),
                  ),
                ),
                const SizedBox(width: 24),
                InkWell(
                  onTap: pickFromGallery,
                  child: Container(
                    padding: const EdgeInsets.all(24),
                    decoration:
                        BoxDecoration(borderRadius: BorderRadius.circular(12), border: Border.all(color: Colors.black)),
                    child: const Icon(Icons.photo_size_select_actual_rounded),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
