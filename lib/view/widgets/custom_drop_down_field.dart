import 'package:flutter/material.dart';
import '../../utils/constants/resources.dart';

class CustomDropDownTextField<T> extends StatefulWidget {
  final List<DropdownMenuItem<T>> items;
  final void Function(T?) onChanged;
  final String? hint;
  final Color? fillColor;
  final Widget? prefix;
  final double? height;
  final TextStyle? hintTextStyle;
  final String? Function(T?)? validator;
  final T? value;
  const CustomDropDownTextField({
    Key? key,
    required this.items,
    required this.onChanged,
    this.hint,
    this.fillColor,
    this.prefix,
    this.height,
    this.hintTextStyle,
    this.validator,
    this.value,
  }) : super(key: key);

  @override
  State<CustomDropDownTextField<T>> createState() => _CustomDropDownTextFieldState<T>();
}

class _CustomDropDownTextFieldState<T> extends State<CustomDropDownTextField<T>> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: widget.height ?? 56,
      child: DropdownButtonFormField<T>(
        value: widget.value,
        icon: const Icon(Icons.keyboard_arrow_down_outlined),
        items: widget.items,
        style: TextStyle(fontSize: 12, color: Theme.of(context).primaryColor, fontWeight: FontWeight.bold),
        onChanged: widget.onChanged,
        validator: widget.validator,
        decoration: InputDecoration(
            filled: true,
            prefixIcon: widget.prefix,
            hintText: widget.hint,
            fillColor: widget.fillColor ?? Colors.white,
            hintStyle: widget.hintTextStyle ??
                const TextStyle(fontSize: 14, color: AppColors.iconsColor, fontWeight: FontWeight.w500),
            border: const OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(8.0))),
            errorStyle: const TextStyle(fontSize: 12, height: .9),
            enabledBorder: const OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(8.0)), borderSide: BorderSide(width: .75)),
            focusedBorder: const OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(8.0)),
                borderSide: BorderSide(color: AppColors.greyTxtColor))),
      ),
    );
  }
}
