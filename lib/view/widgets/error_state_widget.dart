import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import '../../utils/constants/resources.dart';

class ErrorStateWidget extends StatelessWidget {
  const ErrorStateWidget({
    Key? key,
    required this.hasRefresh,
    this.onRefresh,
  }) : super(key: key);
  final bool hasRefresh;
  final Future<void> Function()? onRefresh;

  @override
  Widget build(BuildContext context) {
    return hasRefresh
        ? RefreshIndicator(
            onRefresh: onRefresh!,
            child: ListView(
              padding: EdgeInsets.only(left: width * 0.03, right: width * 0.03, top: height * 0.33),
              children: [
                Text(
                  'errorOccurred'.tr(),
                  textAlign: TextAlign.center,
                  style: AppTextStyles.huge44.copyWith(
                    fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal',
                  ),
                ),
              ],
            ),
          )
        : Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                'errorOccurred'.tr(),
                textAlign: TextAlign.center,
                style: AppTextStyles.huge44.copyWith(
                  fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal',
                ),
              ),
            ],
          );
  }
}
