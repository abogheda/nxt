import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import '../../utils/constants/app_const.dart';

import '../../utils/constants/app_colors.dart';
import '../../utils/constants/app_text_style.dart';

class SignUpSuccessfullyDialog extends StatelessWidget {
  const SignUpSuccessfullyDialog({Key? key, this.onPressed}) : super(key: key);
  final Function()? onPressed;
  @override
  Widget build(BuildContext context) {
    final buttonTextStyle = Theme.of(context)
        .textTheme
        .bodyText2!
        .copyWith(fontFamily: isEn ? 'Montserrat' : 'Tajawal', fontWeight: FontWeight.bold);
    return AlertDialog(
      actionsAlignment: MainAxisAlignment.center,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15.0)),
      contentPadding: EdgeInsets.only(right: width * 0.1, left: width * 0.1, top: height * 0.07, bottom: height * 0.03),
      actionsPadding: EdgeInsets.only(bottom: height * 0.025, right: width * 0.09, left: width * 0.09),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text("signedUpSuccessfully".tr(), style: AppTextStyles.huge36, textAlign: TextAlign.center),
          SizedBox(height: height * .01),
          Text(
            "pleaseLogIn".tr(),
            textAlign: TextAlign.center,
            style: Theme.of(context).textTheme.bodySmall!.copyWith(
                fontFamily: isEn ? 'Montserrat' : 'Tajawal', fontWeight: FontWeight.bold, color: AppColors.iconsColor),
          )
        ],
      ),
      actions: [
        SizedBox(
          height: height * 0.05,
          width: double.infinity,
          child: ElevatedButton(
            onPressed: onPressed,
            child: Text(
              "continue".tr(),
              style: buttonTextStyle.copyWith(color: Colors.white),
              textAlign: TextAlign.center,
            ),
          ),
        ),
      ],
    );
  }
}
