import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter_svg/svg.dart';
import '../../utils/constants/app_const.dart';

class ShareButton extends StatelessWidget {
  const ShareButton({Key? key, this.onPressed}) : super(key: key);
  final Future<void> Function()? onPressed;
  @override
  Widget build(BuildContext context) {
    return TextButton.icon(
      onPressed: onPressed,
      icon: SvgPicture.asset('assets/images/share_arrow.svg', width: 20, height: 20, color: Colors.black),
      label: Text(
        'share'.tr(),
        style: TextStyle(
          fontSize: 14,
          fontWeight: FontWeight.w900,
          color: Colors.black,
          fontFamily: isEn ? 'Montserrat' : 'Tajawal',
        ),
      ),
    );
  }
}
