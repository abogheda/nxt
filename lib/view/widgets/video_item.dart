import 'dart:developer';
import 'dart:io';

import 'package:chewie/chewie.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:nxt/view/widgets/app_loader.dart';
import 'package:video_player/video_player.dart';

// 'https://flutter.github.io/assets-for-api-docs/assets/videos/butterfly.mp4'

class VideoItem extends StatefulWidget {
  final String videoUrl;
  final File? file;

  final bool controls;
  const VideoItem({
    Key? key,
    required this.videoUrl,
    this.file,
    this.controls = true,
  }) : super(key: key);
  @override
  State<VideoItem> createState() => _VideoItemState();
}

class _VideoItemState extends State<VideoItem> {
  late VideoPlayerController videoPlayerController;
  ChewieController? chewieController;
  late Widget playerWidget;

  void initPlayer() async {
    log('lllll : ${widget.videoUrl}');
    videoPlayerController = (widget.file is File)
        ? VideoPlayerController.file(widget.file!)
        : VideoPlayerController.network(
            widget.videoUrl,
            //  'https://new.ostaa.net/storage/app/public/uploads/posts/post_1080974124.mp4',
            // 'https://flutter.github.io/assets-for-api-docs/assets/videos/butterfly.mp4',
          );
    log('message : ${videoPlayerController.dataSource}');
    await videoPlayerController.initialize();

    chewieController = ChewieController(
      videoPlayerController: videoPlayerController,
      autoPlay: false,
      fullScreenByDefault: false,
      looping: false,
      allowMuting: true,
      allowFullScreen: true,
      showControlsOnInitialize: false,
      showOptions: false,
      showControls: widget.controls,
      deviceOrientationsOnEnterFullScreen: [
        DeviceOrientation.portraitUp,
        DeviceOrientation.portraitDown,
        DeviceOrientation.landscapeLeft,
        DeviceOrientation.landscapeRight,
      ],
      deviceOrientationsAfterFullScreen: [
        DeviceOrientation.portraitUp,
      ],
    );

    setState(() {
      playerWidget = Directionality(
        textDirection: TextDirection.ltr,
        child: Chewie(
          controller: chewieController!,
        ),
      );
    });
  }

  @override
  void initState() {
    initPlayer();
    super.initState();
  }

  @override
  void dispose() {
    videoPlayerController.dispose();
    chewieController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return chewieController != null
        ? playerWidget
        : const Center(child: AppLoader());
  }
}
