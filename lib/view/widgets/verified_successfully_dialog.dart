import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import '../../utils/constants/app_const.dart';
import '../../utils/constants/app_text_style.dart';

class VerifiedSuccessfullyDialog extends StatelessWidget {
  const VerifiedSuccessfullyDialog(
      {Key? key, this.onPressed, this.text, this.textStyle, this.buttonText, this.contentPadding, this.actionWidget})
      : super(key: key);
  final Function()? onPressed;
  final String? text;
  final String? buttonText;
  final TextStyle? textStyle;
  final EdgeInsetsGeometry? contentPadding;
  final Widget? actionWidget;
  @override
  Widget build(BuildContext context) {
    final buttonTextStyle = Theme.of(context)
        .textTheme
        .bodyText2!
        .copyWith(fontFamily: isEn ? 'Montserrat' : 'Tajawal', fontWeight: FontWeight.bold);
    return AlertDialog(
      actionsAlignment: MainAxisAlignment.center,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15.0)),
      contentPadding: contentPadding ??
          EdgeInsets.only(right: width * 0.1, left: width * 0.1, top: height * 0.07, bottom: height * 0.05),
      actionsPadding: EdgeInsets.only(bottom: height * 0.025, right: width * 0.09, left: width * 0.09),
      content: Text(text ?? "verifiedSuccessfully".tr(),
          style: textStyle ?? AppTextStyles.huge36, textAlign: TextAlign.center),
      actions: [
        SizedBox(
          height: height * 0.05,
          width: double.infinity,
          child: actionWidget ??
              ElevatedButton(
                onPressed: onPressed,
                child: Text(
                  buttonText ?? "continue".tr(),
                  style: buttonTextStyle.copyWith(color: Colors.white),
                  textAlign: TextAlign.center,
                ),
              ),
        ),
      ],
    );
  }
}
