
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import '../../../../../../utils/constants/resources.dart';

class EditColumnSingle extends StatelessWidget {
  const EditColumnSingle({
    Key? key,
    required this.onPressed,
    required this.header,
    required this.fallbackText,
    this.showEditButton = true, required this.child,
  }) : super(key: key);
  final Function() onPressed;
  final String header;
  final Widget child;
  final String fallbackText;
  final bool showEditButton;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Padding(
              padding: const EdgeInsetsDirectional.only(start: 10.0),
              child: Text(header,
                  style: Theme.of(context)
                      .textTheme
                      .titleLarge!
                      .copyWith(fontFamily: isEn ? 'BebasNeue' : 'Tajawal')),
            ),
            showEditButton
                ? TextButton(
              onPressed: onPressed,
              style: TextButton.styleFrom(padding: EdgeInsets.zero),
              child: Text(
                'edit'.tr(),
                style: TextStyle(
                  color: AppColors.semiBlack,
                  decoration: TextDecoration.underline,
                  fontFamily: isEn ? 'Montserrat' : 'Tajawal',
                  fontSize: 14,
                  fontWeight: FontWeight.w900,
                ),
              ),
            )
                : Column(
              children: <Widget>[
                Opacity(
                  opacity: 0,
                  child: TextButton(
                    onPressed: onPressed,
                    style: TextButton.styleFrom(padding: EdgeInsets.zero),
                    child: Text(
                      'edit'.tr(),
                      style: TextStyle(
                        color: AppColors.semiBlack,
                        decoration: TextDecoration.underline,
                        fontFamily: isEn ? 'Montserrat' : 'Tajawal',
                        fontSize: 14,
                        fontWeight: FontWeight.w900,
                      ),
                    ),
                  ),
                ),
              ],
            ),
            
          ],
        ),
        child,
        SizedBox(height: height * 0.01),
        Container(color: Colors.black, width: double.infinity, height: 1.5),
        SizedBox(height: height * 0.01),
      ],
    );
  }
}
