import 'package:flutter/material.dart';

class LoadingStack extends StatelessWidget {
  const LoadingStack({Key? key, required this.widget}) : super(key: key);
  final Widget widget;
  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.center,
      children: [
        Opacity(opacity: 0, child: widget),
        const Center(
            widthFactor: 1,
            heightFactor: 1,
            child: SizedBox(height: 16, width: 16, child: CircularProgressIndicator(strokeWidth: 2))),
      ],
    );
  }
}
