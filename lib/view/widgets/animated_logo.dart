import 'dart:async';
import 'package:flutter/material.dart';

class AnimatedLogo extends StatefulWidget {
  final bool isLoadingIndicator;
  final double? height;
  const AnimatedLogo({Key? key, this.isLoadingIndicator = false, this.height}) : super(key: key);

  @override
  State<AnimatedLogo> createState() => _AnimatedLogoState();
}

class _AnimatedLogoState extends State<AnimatedLogo> {
  late Timer _timer;
  late String _imgPath;
  final _fabImages = [
    'assets/images/new_sp1.png',
    'assets/images/new_sp2.png',
    'assets/images/new_sp3.png',
    'assets/images/new_sp4.png',
  ];
  // final _images = [
  //   'assets/images/sp1.webp',
  //   'assets/images/sp2.webp',
  //   'assets/images/sp3.webp',
  //   'assets/images/sp4.webp',
  // ];
  @override
  void initState() {
    _imgPath = _fabImages[0];
    // _imgPath = HiveHelper.isEgypt ? _fabImages[0] : _images[0];
    _timer = Timer.periodic(Duration(milliseconds: widget.isLoadingIndicator ? 65 : 300), (timer) {
      if (mounted) {
        setState(() {
          _imgPath =
                  // HiveHelper.isEgypt ?
                  _fabImages[timer.tick % _fabImages.length]
              // : _images[timer.tick % _images.length]
              ;
        });
      }
    });
    super.initState();
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Image.asset(
        _imgPath,
        width: widget.isLoadingIndicator ? 75 : null,
        height: widget.height,
      ),
    );
  }
}
