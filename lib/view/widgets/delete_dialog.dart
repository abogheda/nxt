import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import '../../utils/constants/app_const.dart';

import '../../config/router/router.dart';
import '../screens/more/more_widgets/custom_logout_dialog.dart';
import 'loading_stack.dart';

class DeleteDialog extends StatelessWidget {
  const DeleteDialog({Key? key, required this.onPressed, required this.isLoading, required this.headerText})
      : super(key: key);
  final Function() onPressed;
  final bool isLoading;
  final String headerText;
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      actionsAlignment: MainAxisAlignment.center,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15.0)),
      title: Text(headerText,
          textAlign: TextAlign.center,
          style: Theme.of(context).textTheme.titleSmall!.copyWith(fontFamily: isEn ? 'Montserrat' : 'Tajawal')),
      actions: [
        SimpleButton(text: "cancel".tr(), onPressed: () => MagicRouter.pop()),
        isLoading
            ? LoadingStack(widget: SimpleButton(text: "delete".tr(), onPressed: () {}, invertedColors: true))
            : SimpleButton(text: "delete".tr(), onPressed: onPressed, invertedColors: true),
      ],
    );
  }
}
