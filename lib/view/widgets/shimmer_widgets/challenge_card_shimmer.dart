import 'package:flutter/material.dart';
import '../../../utils/constants/app_const.dart';
import 'custom_shimmer.dart';

class ChallengeCardShimmer extends StatelessWidget {
  const ChallengeCardShimmer({Key? key, required this.padding}) : super(key: key);
  final EdgeInsetsGeometry padding;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: padding,
      child: CustomShimmer(
        child: Container(
          height: height * 0.21,
          decoration: ShapeDecoration(
            color: Colors.grey[400]!,
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)),
          ),
        ),
      ),
    );
  }
}
