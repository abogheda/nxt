import 'package:flutter/material.dart';
import '../../../utils/constants/app_const.dart';

import '../../../utils/constants/app_colors.dart';
import 'custom_shimmer.dart';

class ReadsCardShimmer extends StatelessWidget {
  const ReadsCardShimmer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: EdgeInsets.symmetric(vertical: height * 0.012),
          child: Material(
            color: Colors.white,
            elevation: 2,
            child: Container(
              decoration: BoxDecoration(border: Border.all(color: AppColors.greyOutText)),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: height * 0.017),
                  Padding(
                    padding: EdgeInsetsDirectional.only(start: width * 0.01 + 3, end: width * 0.01 + 3),
                    child: CustomShimmer(
                        child: Row(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
                      Expanded(child: Container(width: width * 0.6, height: 21.0, color: Colors.white))
                    ])),
                  ),
                  Padding(
                    padding: EdgeInsetsDirectional.only(start: width * 0.01, end: width * 0.01),
                    child: Wrap(
                      textDirection: TextDirection.ltr,
                      crossAxisAlignment: WrapCrossAlignment.start,
                      alignment: WrapAlignment.start,
                      children: [
                        CustomShimmer(
                          child: Wrap(
                            children: [
                              Container(
                                margin: const EdgeInsets.symmetric(horizontal: 3, vertical: 8),
                                decoration: const BoxDecoration(
                                    color: Colors.black, borderRadius: BorderRadius.all(Radius.circular(4))),
                                child: SizedBox(width: width * 0.12, height: height * 0.035),
                              ),
                              Container(
                                margin: const EdgeInsets.symmetric(horizontal: 3, vertical: 8),
                                decoration: const BoxDecoration(
                                    color: Colors.black, borderRadius: BorderRadius.all(Radius.circular(4))),
                                child: SizedBox(width: width * 0.3, height: height * 0.035),
                              ),
                              Container(
                                margin: const EdgeInsets.symmetric(horizontal: 3, vertical: 8),
                                decoration: const BoxDecoration(
                                    color: Colors.black, borderRadius: BorderRadius.all(Radius.circular(4))),
                                child: SizedBox(width: width * 0.24, height: height * 0.035),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                  const SizedBox(height: 7),
                  SizedBox(
                    width: double.infinity,
                    child: Stack(
                      alignment: Alignment.bottomRight,
                      children: [
                        CustomShimmer(
                            child: Container(color: Colors.white, height: height * 0.22, width: double.infinity)),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Container(
                              decoration: BoxDecoration(borderRadius: BorderRadius.circular(8)),
                              height: 7,
                              width: width * 0.22,
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  CustomShimmer(
                      child: Container(
                          margin: const EdgeInsets.all(6.0), width: width * 0.7, height: 12.0, color: Colors.white))
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}
