import 'package:flutter/material.dart';
import '../../../utils/constants/resources.dart';
import 'custom_shimmer.dart';

const colorRadius = Radius.circular(10);
final borderRadius = BorderRadius.circular(10);

class MyJobCardShimmer extends StatelessWidget {
  const MyJobCardShimmer({Key? key, this.padding, this.elevation}) : super(key: key);
  final EdgeInsetsGeometry? padding;
  final double? elevation;
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: padding ?? const EdgeInsets.only(bottom: 10.0),
      decoration: BoxDecoration(borderRadius: borderRadius, border: Border.all(color: Colors.black, width: 0.2)),
      child: Material(
        elevation: elevation ?? 2,
        borderRadius: borderRadius,
        child: IntrinsicHeight(
          child: Stack(
            children: [
              CustomShimmer(
                child: Container(
                  width: 10,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                      topLeft: isEn ? colorRadius : Radius.zero,
                      bottomLeft: isEn ? colorRadius : Radius.zero,
                      topRight: isEn ? Radius.zero : colorRadius,
                      bottomRight: isEn ? Radius.zero : colorRadius,
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: EdgeInsetsDirectional.only(start: 16, end: width * 0.006),
                      child: Row(
                        children: [
                          CustomShimmer(
                            child: Container(
                              width: 36,
                              height: 36,
                              decoration: const BoxDecoration(color: Colors.white, shape: BoxShape.circle),
                            ),
                          ),
                          SizedBox(width: width * 0.015),
                          CustomShimmer(
                            child: SizedBox(
                              width: width * 0.55,
                              height: height * 0.06,
                              child: Align(
                                alignment: AppConst.isEn ? Alignment.centerLeft : Alignment.centerRight,
                                child: ColoredBox(
                                  color: Colors.white,
                                  child: Text(
                                    'title' * 6,
                                    maxLines: 2,
                                    textAlign: TextAlign.start,
                                    overflow: TextOverflow.ellipsis,
                                    style: Theme.of(context).textTheme.titleLarge!.copyWith(
                                          fontFamily: AppConst.isEn ? 'BebasNeue' : 'Tajawal',
                                          fontWeight: FontWeight.w500,
                                        ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    CustomShimmer(
                      child: Padding(
                        padding: EdgeInsetsDirectional.only(start: width * 0.15),
                        child: ColoredBox(
                          color: Colors.white,
                          child: Text(
                            'name' * 2,
                            style: Theme.of(context).textTheme.bodyText2!.copyWith(
                                  color: const Color(0xFF555555),
                                  fontFamily: isEn ? 'Inter-Medium' : 'Tajawal',
                                ),
                          ),
                        ),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        CustomShimmer(
                          child: Padding(
                            padding: EdgeInsetsDirectional.only(start: width * 0.15),
                            child: ColoredBox(
                              color: Colors.white,
                              child: Text(
                                'timeago' * 3,
                                style: TextStyle(
                                  fontSize: 12,
                                  color: AppColors.iconsColor,
                                  fontFamily: isEn ? 'Inter-Medium' : 'Tajawal',
                                ),
                              ),
                            ),
                          ),
                        ),
                        CustomShimmer(
                          child: Padding(
                            padding: const EdgeInsets.only(bottom: 12.0, left: 12.0, right: 12.0),
                            child: Icon(
                              Icons.arrow_forward_ios_rounded,
                              textDirection: isEn ? TextDirection.ltr : TextDirection.rtl,
                            ),
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
