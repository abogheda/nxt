import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class CustomShimmer extends StatelessWidget {
  const CustomShimmer({Key? key, required this.child, this.highlightColor, this.baseColor}) : super(key: key);
  final Widget child;
  final Color? highlightColor;
  final Color? baseColor;
  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
      baseColor: baseColor ?? Colors.grey[300]!,
      highlightColor: highlightColor ?? Colors.grey[100]!,
      direction: ShimmerDirection.ltr,
      period: const Duration(seconds: 1),
      child: child,
    );
  }
}
