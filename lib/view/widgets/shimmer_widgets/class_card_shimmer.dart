import 'package:flutter/material.dart';
import '../../../utils/constants/app_const.dart';
import '../../../utils/constants/app_colors.dart';
import 'custom_shimmer.dart';

class ClassCardShimmer extends StatelessWidget {
  const ClassCardShimmer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: EdgeInsets.symmetric(vertical: height * 0.009),
          child: Material(
            color: Colors.white,
            elevation: 2,
            child: DecoratedBox(
              decoration: BoxDecoration(border: Border.all(color: AppColors.greyOutText)),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: height * 0.017),
                  CustomShimmer(
                      child: Container(
                    width: width * 0.6,
                    height: height * 0.025,
                    color: Colors.white,
                    margin: EdgeInsets.symmetric(horizontal: width * 0.03, vertical: 4),
                  )),
                  CustomShimmer(
                      child: Container(
                    width: width * 0.75,
                    height: height * 0.015,
                    color: Colors.white,
                    margin: EdgeInsets.symmetric(horizontal: width * 0.03, vertical: 4),
                  )),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: width * 0.03, vertical: 4),
                    child: CustomShimmer(
                      child: Wrap(
                        spacing: 4,
                        runSpacing: 8,
                        children: [
                          Container(
                            decoration: const BoxDecoration(
                                color: Colors.white, borderRadius: BorderRadius.all(Radius.circular(4))),
                            child: SizedBox(width: width * 0.3, height: height * 0.03),
                          ),
                          Container(
                            decoration: const BoxDecoration(
                                color: Colors.white, borderRadius: BorderRadius.all(Radius.circular(4))),
                            child: SizedBox(width: width * 0.12, height: height * 0.03),
                          ),
                          Container(
                            decoration: const BoxDecoration(
                                color: Colors.white, borderRadius: BorderRadius.all(Radius.circular(4))),
                            child: SizedBox(width: width * 0.24, height: height * 0.03),
                          ),
                        ],
                      ),
                    ),
                  ),
                  const SizedBox(height: 8),
                  Center(
                      child: CustomShimmer(
                          child: Container(color: Colors.white, height: height * 0.22, width: width - 2))),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: width * 0.03, vertical: 12.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: <Widget>[
                            CustomShimmer(
                              child: Container(
                                width: 30,
                                height: 30,
                                decoration: const BoxDecoration(color: Colors.white, shape: BoxShape.circle),
                              ),
                            ),
                            const SizedBox(width: 8),
                            CustomShimmer(
                                child: Container(width: width * 0.2, height: height * 0.022, color: Colors.white)),
                          ],
                        ),
                        CustomShimmer(
                            child: Container(width: width * 0.3, height: height * 0.016, color: Colors.white)),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}
