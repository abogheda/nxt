import 'package:flutter/material.dart';
import '../../../utils/constants/resources.dart';
import 'custom_shimmer.dart';

class UserResultCardShimmer extends StatelessWidget {
  const UserResultCardShimmer({Key? key, this.horizontalPadding}) : super(key: key);
  final double? horizontalPadding;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: height * 0.01, horizontal: horizontalPadding ?? width * 0.03),
      child: Container(
        decoration: BoxDecoration(
            border: Border.all(color: const Color(0xFF707070), width: 0.2), borderRadius: BorderRadius.circular(6)),
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: height * 0.025, horizontal: height * 0.02),
          child: Row(
            children: <Widget>[
              CustomShimmer(child: Container(height: width * 0.13, width: width * 0.13, color: Colors.white)),
              Padding(
                padding: EdgeInsetsDirectional.only(start: width * 0.03),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: height * 0.005),
                      child: CustomShimmer(
                        child: ColoredBox(
                          color: Colors.white,
                          child: Text('userName' * 3, style: Theme.of(context).textTheme.headline6),
                        ),
                      ),
                    ),
                    CustomShimmer(
                      child: ColoredBox(
                        color: Colors.white,
                        child: Text(
                          'birthDate',
                          style: Theme.of(context)
                              .textTheme
                              .bodyText1!
                              .copyWith(fontFamily: isEn ? 'Montserrat' : 'Tajawal'),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class UserResultShimmerListView extends StatelessWidget {
  const UserResultShimmerListView({Key? key, this.horizontalPadding}) : super(key: key);
  final double? horizontalPadding;

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      shrinkWrap: true,
      itemCount: 3,
      itemBuilder: (context, index) => UserResultCardShimmer(horizontalPadding: horizontalPadding),
    );
  }
}
