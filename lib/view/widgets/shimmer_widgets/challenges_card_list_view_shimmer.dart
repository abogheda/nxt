import 'package:flutter/material.dart';

import '../../../utils/constants/resources.dart';
import 'challenge_card_shimmer.dart';

class ChallengesCardListViewShimmer extends StatelessWidget {
  const ChallengesCardListViewShimmer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: 2,
      itemBuilder: (context, index) => ChallengeCardShimmer(
        padding: EdgeInsets.symmetric(vertical: height * 0.008),
      ),
    );
  }
}
