import 'package:flutter/material.dart';
import '../../utils/constants/app_const.dart';

import 'three_bounce.dart';

class AppLoader extends StatelessWidget {
  final Color color;
  const AppLoader({Key? key, this.color = Colors.black}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: SpinKitThreeBounce(
        color: color,
        size: width * 0.08,
      ),
    );
  }
}
