// ignore_for_file: must_be_immutable

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import '../../utils/constants/app_const.dart';

class NoStateWidget extends StatelessWidget {
  bool isDefaultText;
  String? newText;
  TextStyle? newTextStyle;
  double? height;
  NoStateWidget({
    Key? key,
    this.isDefaultText = true,
    this.newText,
    this.newTextStyle,
    this.height,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SizedBox(height: height ?? 25),
        Center(
          child: Text(
            isDefaultText ? 'noStateText'.tr() : newText!,
            textAlign: TextAlign.center,
            style: isDefaultText
                ? Theme.of(context).textTheme.titleMedium!.copyWith(fontFamily: isEn ? 'BebasNeue' : 'Tajawal')
                : newTextStyle,
          ),
        ),
      ],
    );
  }
}
