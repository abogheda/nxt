import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import '../../utils/constants/app_const.dart';

class SaveButton extends StatelessWidget {
  const SaveButton({
    Key? key,
    required this.onPressed,
    required this.isSaved,
    required this.saveColor,
  }) : super(key: key);
  final Function()? onPressed;
  final bool isSaved;
  final String saveColor;
  @override
  Widget build(BuildContext context) {
    return TextButton.icon(
      onPressed: onPressed,
      icon: SvgPicture.asset(isSaved ? 'assets/images/saved.svg' : 'assets/images/save.svg',
          color: isSaved ? Color(saveColor.toHex()) : Colors.black, height: 20, width: 20),
      label: Text(
        isSaved ? 'saved'.tr() : 'save'.tr(),
        style: TextStyle(
          fontSize: 14,
          fontWeight: FontWeight.w900,
          color: isSaved ? Color(saveColor.toHex()) : Colors.black,
          fontFamily: isEn ? 'Montserrat' : 'Tajawal',
        ),
      ),
    );
  }
}
