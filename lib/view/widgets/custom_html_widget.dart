import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';

import '../../utils/constants/app_const.dart';
import '../../utils/helpers/utils.dart';

class CustomHtmlWidget extends StatelessWidget {
  const CustomHtmlWidget({
    Key? key,
    required this.data,
  }) : super(key: key);

  final String? data;

  @override
  Widget build(BuildContext context) {
    final htmlThemeData = Theme.of(context)
        .copyWith(textTheme: Theme.of(context).textTheme.apply(fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal'));

    return Html(
      data: data ?? "notAvailable".tr(),
      style: Style.fromThemeData(htmlThemeData),
      onLinkTap: (String? url, RenderContext context, Map<String, String> attributes, element) async {
        await Utils.launchAppUrl(url: url!);
      },
    );
  }
}
