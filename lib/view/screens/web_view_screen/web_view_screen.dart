// ignore_for_file: library_private_types_in_public_api

import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'web_view_data.dart';

// ignore: must_be_immutable
class WebViewScreen extends StatefulWidget {

  const WebViewScreen(
     {
    Key? key,
  }): super(key: key);

  @override
  _WebViewScreenState createState() => _WebViewScreenState();
}

class _WebViewScreenState extends State<WebViewScreen> with WebViewScreenData {
  @override
  void initState() {
    flutterWebviewPlugin.close();
    // onChangeUrl(context);
    super.initState();
  }

  @override
  void dispose() {
    urlState.cancel();
    flutterWebviewPlugin.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WebviewScaffold(
      key: scaffold,
      withZoom: true,
      url:"https://projectnxt.app/rules/",
      appBar: AppBar(

        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back_ios,
            color: Colors.white,
          ),
          onPressed: () {
            Navigator.of(context).pop();
            // ExtendedNavigator.root.popAndPush(Routes.home);
          },
        ),
      ),
    );
  }
}
