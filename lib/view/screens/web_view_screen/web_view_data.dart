import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';

class WebViewScreenData {
  late StreamSubscription<String> urlState;
  final FlutterWebviewPlugin flutterWebviewPlugin = FlutterWebviewPlugin();
  final GlobalKey<ScaffoldState> scaffold = GlobalKey();

  onChangeUrl(
    BuildContext context,
  ) {
    urlState = flutterWebviewPlugin.onUrlChanged.listen((String url) {
      flutterWebviewPlugin.close();
      Navigator.pop(context);
    });
  }
}
