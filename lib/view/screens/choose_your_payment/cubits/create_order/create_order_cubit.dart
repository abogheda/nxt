import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nxt/data/models/choose_payment_models/create_order.dart';
import 'package:nxt/data/repos/payment/payment_repo.dart';


part 'create_order_state.dart';

class CreateOrderCubit extends Cubit<CreateOrderState> {
  CreateOrderCubit() : super(CreateOrderInitial());
  static CreateOrderCubit get(context) => BlocProvider.of(context);

  CreateOrderModel? createOrderModel;
  Future<void> createOrder(
      {required String planId,
      String code = '',
      String paymentMethod = '',
      String challengeId = '',
      String phoneNumber = ''}) async {
    emit(CreateOrderLoading());
    final res = await PaymentRepo.createOrder(
        planId: planId,
        challengeId: challengeId,
        code: code,
        paymentMethod: paymentMethod,
        phoneNumber: phoneNumber);
    if (res.statusCode != 400) {
      createOrderModel = res.data;
      print("object link : ${res.data?.ref}");
      emit(CreateOrderSuccess());

    } else {
      emit(CreateOrderFailed(res.message));
    }
  }
}
