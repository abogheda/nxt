part of 'create_order_cubit.dart';

abstract class CreateOrderState extends Equatable {
  const CreateOrderState();
}

class CreateOrderInitial extends CreateOrderState {
  @override
  List<Object> get props => [];
}class CreateOrderLoading extends CreateOrderState {
  @override
  List<Object> get props => [];
}class CreateOrderSuccess extends CreateOrderState {
  @override
  List<Object> get props => [];
}class CreateOrderFailed extends CreateOrderState {
  final String message;

  const CreateOrderFailed(this.message);
  @override
  List<Object> get props => [message];
}
