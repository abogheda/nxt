part of 'payment_method_cubit.dart';

abstract class PaymentMethodState extends Equatable {
  const PaymentMethodState();
  @override
  List<Object> get props => [];
}

class PaymentMethodInitial extends PaymentMethodState {}
class PaymentMethodLoading extends PaymentMethodState {}
class PaymentMethodSuccess extends PaymentMethodState {}
class PaymentMethodFailed extends PaymentMethodState {}
