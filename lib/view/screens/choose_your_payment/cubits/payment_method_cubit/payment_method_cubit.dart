import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nxt/data/models/choose_payment_models/payment_method.dart';

import '../../../../../data/repos/payment/payment_repo.dart';

part 'payment_method_state.dart';

class PaymentMethodCubit extends Cubit<PaymentMethodState> {
  PaymentMethodCubit() : super(PaymentMethodInitial());
  static PaymentMethodCubit get(context) => BlocProvider.of(context);
  List<PaymentMethod>? paymentMethodList;
  Future<void> getPaymentMethodList() async {
    emit(PaymentMethodLoading());
    final res = await PaymentRepo.paymentMethodList();
    if (res.data != null) {
      paymentMethodList = res.data;
      emit(PaymentMethodSuccess());
    } else {
      emit(PaymentMethodFailed());
    }
  }
}
