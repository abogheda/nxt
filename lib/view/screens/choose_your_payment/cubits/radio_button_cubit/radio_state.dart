part of 'radio_cubit.dart';

abstract class RadioState extends Equatable {
  final int type;
  const RadioState(this.type);
}

class RadioInitial extends RadioState {
  const RadioInitial() : super(0);

  @override
  List<Object> get props => [type];
}

class RadioUpdated extends RadioState {
  const RadioUpdated(int type) : super(type);

  @override
  List<Object> get props => [type];
}
