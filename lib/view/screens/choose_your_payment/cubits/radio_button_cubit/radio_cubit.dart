import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:equatable/equatable.dart';

part 'radio_state.dart';

class RadioCubit extends Cubit<RadioState> {
  RadioCubit() : super(const RadioInitial());

  onUpdateRadioMethod(int type){
    emit(RadioUpdated(type));
  }

}
