
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:nxt/config/router/router.dart';
import 'package:nxt/utils/constants/app_colors.dart';
import 'package:nxt/view/screens/choose_your_payment/cubits/create_order/create_order_cubit.dart';
import 'package:nxt/view/screens/choose_your_payment/cubits/payment_method_cubit/payment_method_cubit.dart';
import 'package:nxt/view/screens/choose_your_payment/widgets/test_in.dart';
import 'package:nxt/view/widgets/app_loader.dart';
import '../../../data/service/hive_services.dart';
import '../../../utils/constants/app_const.dart';
import '../../widgets/custom_button.dart';
import '../complete_profile_screen/base_complete_profile_screen.dart';
import '../complete_profile_screen/cubit/complete_profile_cubit.dart';
import '../navigation_and_appbar/import_widget.dart';
import '../payment_web_view/PaymentOnline.dart';
import 'cubits/radio_button_cubit/radio_cubit.dart';

class ChooseYourPayment extends StatefulWidget {
  final num price;
  final String planId;
  const ChooseYourPayment({Key? key, required this.price, required this.planId})
      : super(key: key);

  @override
  State<ChooseYourPayment> createState() => _ChooseYourPaymentState();
}

class _ChooseYourPaymentState extends State<ChooseYourPayment> {
  @override
  Widget build(BuildContext context) {
    return BaseCompleteProfileScreen(
      showBackButton: true,
      showSkipButton: false,
      child: BlocBuilder<CompleteProfileCubit, CompleteProfileState>(
        builder: (context, state) {
          final cubit = CompleteProfileCubit.get(context);
          return BlocProvider(
            create: (context) => PaymentMethodCubit()..getPaymentMethodList(),
            child: BlocBuilder<PaymentMethodCubit, PaymentMethodState>(
              builder: (context, state) {
                var paymentMethodCubit = PaymentMethodCubit.get(context);
                return BlocProvider(
                  create: (context) => RadioCubit(),
                  child: BlocBuilder<RadioCubit, RadioState>(
                    builder: (context, radioState) {
                      return ListView(
                        children: [
                          Text(
                            'choosePayWay'.tr(),
                            textAlign: TextAlign.center,
                            style: Theme.of(context)
                                .textTheme
                                .headlineMedium!
                                .copyWith(
                                    fontSize: 32,
                                    color: Colors.black,
                                    fontFamily: AppConst.isEn
                                        ? 'BebasNeue'
                                        : 'Tajawal'),
                          ),
                          SizedBox(height: widget.height * 0.03),
                          if (state is PaymentMethodLoading) const AppLoader(),
                          if (state is PaymentMethodSuccess)
                            ...List.generate(
                              paymentMethodCubit.paymentMethodList?.length ?? 0,
                              (index) => Container(
                                height: 45.h,
                                width: 200.w,
                                margin: REdgeInsets.symmetric(
                                    horizontal: 25.w, vertical: 5.h),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(6),
                                    border: Border.all(
                                        color: Colors.grey.shade400)),
                                child: Row(
                                  children: [
                                    Radio(
                                        value: index,
                                        groupValue: radioState.type,
                                        onChanged: (value) {
                                          context
                                              .read<RadioCubit>()
                                              .onUpdateRadioMethod(index);
                                        }),
                                    Text(
                                      paymentMethodCubit
                                              .paymentMethodList![index].name ??
                                          "",
                                      style: TextStyle(
                                          fontFamily: "Montserrat",
                                          fontSize: 13.sp,
                                          color: AppColors.greyTxtColor),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          SizedBox(height: 30.h),
                          Padding(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 25.0),
                            child: BlocProvider(
                              create: (context) => CreateOrderCubit(),
                              child: BlocBuilder<CreateOrderCubit,
                                  CreateOrderState>(
                                builder: (context, orderState) {
                                  var createOrderCubit =
                                      CreateOrderCubit.get(context);
                                  if (orderState is CreateOrderLoading) {
                                    return const AppLoader();
                                  }
                                  if (context.read<RadioCubit>().state.type ==
                                      0) {
                                    return CustomButton(
                                      width: 300.w,
                                      onTap: () async {
                                      await  createOrderCubit.createOrder(
                                            planId: widget.planId,
                                            paymentMethod: paymentMethodCubit
                                                    .paymentMethodList![context
                                                        .read<RadioCubit>()
                                                        .state
                                                        .type]
                                                    .id ??
                                                "",
                                            phoneNumber: HiveHelper
                                                    .getUserInfo?.user?.phone ??
                                                '');
                                        cubit.increaseCompleteProfileIndex();
                                      MagicRouter.navigateTo(
                                        BlocProvider.value(
                                          value: cubit,
                                          child: PaymentOnline(
                                            url: createOrderCubit
                                                .createOrderModel
                                                ?.ref ??
                                                '',
                                          ),
                                        ),
                                      );

                                      },
                                      child: Text(
                                          "forgot.continue".tr().toUpperCase()),
                                    );
                                  }

                                  if (context.read<RadioCubit>().state.type ==
                                      1) {
                                    return CustomButton(
                                        onTap: () {
                                          MagicRouter.navigateTo(PurchasePage(
                                            planId: widget.planId,
                                          ));
                                        },
                                        child: Text("forgot.continue"
                                            .tr()
                                            .toUpperCase()));
                                  }
                                  if (context.read<RadioCubit>().state.type ==
                                      2) {
                                    return Container();
                                  }
                                  return Container();
                                },
                              ),
                            ),
                          ),
                          HiveHelper.fromHome == false
                              ? Center(
                                  child: TextButton(
                                    onPressed: () async {
                                      await cubit.skipProfile();
                                      MagicRouter.navigateAndPopAll(
                                          Navigation());
                                    },
                                    child: Text(
                                      "payLater".tr(),
                                      style: TextStyle(
                                          fontSize: 13.sp,
                                          color: Colors.black,
                                          fontWeight: FontWeight.w800,
                                          decoration: TextDecoration.underline,
                                          fontFamily: AppConst.isEn
                                              ? "Montserrat"
                                              : "Tajawal"),
                                    ),
                                  ),
                                )
                              : Container(),
                        ],
                      );
                    },
                  ),
                );
              },
            ),
          );
        },
      ),
    );
  }
}
