// ignore_for_file: library_private_types_in_public_api

import 'package:easy_localization/easy_localization.dart' as local;
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:numeric_keyboard/numeric_keyboard.dart';
import 'package:nxt/utils/constants/app_const.dart';
import 'package:nxt/utils/helpers/popup_helper.dart';
import 'package:nxt/view/screens/forgot_password/controller/forgot_password_cubit.dart';
import 'package:nxt/view/screens/reset/reset_screen.dart';
import '../../../config/router/router.dart';
import 'widgets/loader_hud.dart';

class OTPScreen extends StatefulWidget {
  const OTPScreen({Key? key}) : super(key: key);

  @override
  _OTPScreenState createState() => _OTPScreenState();
}

class _OTPScreenState extends State<OTPScreen> {
  String text = '';

  void _onKeyboardTap(String value) {
    setState(() {
      if (text.length < 6) {
        text = text + value;
      }
      // if (text.length == 6) {
      //   if (!(await widget.hasInternetConnection)) {
      //     PopupHelper.showBasicSnack(msg: "internet_error".tr(), color: Colors.red);
      //     return;
      //   }
      //   ForgotPasswordCubit.get(context).verifyPhone(text: text);
      // }
    });
  }

  Widget otpNumberWidget(int position) {
    try {
      return Container(
        height: widget.width * 0.12,
        width: widget.width * 0.12,
        decoration: BoxDecoration(
            border: Border.all(color: Colors.black, width: 1),
            borderRadius: const BorderRadius.all(Radius.circular(8))),
        child: Center(
            child: Text(
          text[position],
          style: const TextStyle(color: Colors.black),
        )),
      );
    } catch (e) {
      return Container(
        height: widget.width * 0.12,
        width: widget.width * 0.12,
        decoration: BoxDecoration(
            border: Border.all(color: Colors.black, width: 0),
            borderRadius: const BorderRadius.all(Radius.circular(8))),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<ForgotPasswordCubit, ForgotPasswordState>(listener: (context, state) {
      if (state is VerifyPhoneSuccess) {
        if (state.verifyPhoneModel.password != null) {
          MagicRouter.navigateTo(BlocProvider.value(
            value: ForgotPasswordCubit.get(context),
            child: const ResetScreen(),
          ));
        } else if (state.verifyPhoneModel.statusCode != 200 ||
            state.verifyPhoneModel.statusCode != 201 ||
            state.verifyPhoneModel.message != null) {
          PopupHelper.showBasicSnack(msg: state.verifyPhoneModel.message.toString(), color: Colors.red);
        }
      } else if (state is VerifyPhoneFailed) {
        PopupHelper.showBasicSnack(msg: "normalErrorMsg".tr(), color: Colors.red);
      }
    }, builder: (context, state) {
      final cubit = ForgotPasswordCubit.get(context);
      return LoaderHUD(
        inAsyncCall: state is VerifyPhoneLoading,
        child: Scaffold(
          appBar: AppBar(
            elevation: 0,
            backgroundColor: Colors.transparent,
            foregroundColor: Colors.black,
          ),
          body: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Expanded(
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Expanded(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Container(
                              margin: const EdgeInsets.symmetric(horizontal: 20),
                              child: Text('forgot.digits'.tr(),
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 26,
                                    fontWeight: FontWeight.bold,
                                    fontFamily: widget.isEn ? 'Montserrat' : null,
                                  ))),
                          Container(
                            constraints: const BoxConstraints(maxWidth: 500),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              textDirection: TextDirection.ltr,
                              children: <Widget>[
                                otpNumberWidget(0),
                                otpNumberWidget(1),
                                otpNumberWidget(2),
                                otpNumberWidget(3),
                                otpNumberWidget(4),
                                otpNumberWidget(5),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                      constraints: const BoxConstraints(maxWidth: 500),
                      child: ElevatedButton(
                        onPressed: () {
                          if (text.isEmpty) {
                            PopupHelper.showBasicSnack(msg: 'forgot.digits_empty'.tr(), color: Colors.red);
                            return;
                          }
                          if (text.length != 6) {
                            PopupHelper.showBasicSnack(msg: 'forgot.six_digits'.tr(), color: Colors.red);
                            return;
                          }
                          cubit.verifyPhone(text: text);
                        },
                        child: Container(
                          padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 8),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text('reset_password.confirm'.tr(),
                                  style: TextStyle(color: Colors.white, fontFamily: widget.isEn ? 'Montserrat' : null)),
                              Container(
                                padding: const EdgeInsets.all(8),
                                decoration: const BoxDecoration(
                                    color: Colors.black, borderRadius: BorderRadius.all(Radius.circular(20))),
                                child: const Icon(Icons.arrow_forward_ios, size: 16, color: Colors.white),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                    Directionality(
                      textDirection: TextDirection.ltr,
                      child: NumericKeyboard(
                        onKeyboardTap: _onKeyboardTap,
                        textColor: Colors.black,
                        rightIcon: const Icon(
                          Icons.backspace,
                          color: Colors.black,
                        ),
                        rightButtonFn: () {
                          setState(() {
                            if (text.isEmpty) {
                              return;
                            }
                            text = text.substring(0, text.length - 1);
                          });
                        },
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      );
    });
  }
}
