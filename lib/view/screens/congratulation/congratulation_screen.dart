import 'package:auto_size_text/auto_size_text.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import '../../../config/router/router.dart';
import '../../../utils/constants/resources.dart';
import '../complete_profile_screen/widgets/imports.dart';

import '../../widgets/logo_app_bar.dart';
import '../navigation_and_appbar/import_widget.dart';

class CongratulationScreen extends StatelessWidget {
  const CongratulationScreen({Key? key, this.text, this.onPressed}) : super(key: key);
  final String? text;
  final void Function()? onPressed;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          const LogoAppBar(),
          Expanded(
            child: Center(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 52),
                child: AutoSizeText(
                  text ?? 'congratulations'.tr(),
                  maxLines: 3,
                  style: AppTextStyles.huge44,
                  textAlign: TextAlign.center,
                ),
              ),
            ),
          ),
          SaveAndContinueButton(
            onPressed: onPressed ?? () async => MagicRouter.navigateAndPopAll(Navigation()),
            text: 'clickHereToStart'.tr().toUpperCase(),
          )
        ],
      ),
    );
  }
}
