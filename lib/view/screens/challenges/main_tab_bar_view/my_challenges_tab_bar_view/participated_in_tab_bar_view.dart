import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'participated_in_list_view_builder.dart';

import '../../../../../utils/constants/resources.dart';
import '../../../../widgets/error_state_widget.dart';
import '../../../../widgets/no_state_widget.dart';
import '../../../../widgets/shimmer_widgets/challenges_card_list_view_shimmer.dart';
import '../../../home/cubit/home_cubit.dart';
import '../../cubit/challenge_cubit.dart';

class ParticipatedInTabBarView extends StatelessWidget {
  const ParticipatedInTabBarView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: width * 0.03),
      child: Column(
        children: [
          SizedBox(height: height * 0.016),
          Expanded(
            child: BlocBuilder<ChallengeCubit, ChallengeState>(
              builder: (context, state) {
                final cubit = ChallengeCubit.get(context);
                if (state is GetJoinedChallengeLoading || cubit.joinedChallengesList == null) {
                  return const ChallengesCardListViewShimmer();
                } else if (state is GetJoinedChallengeFailed) {
                  return ErrorStateWidget(
                      hasRefresh: true,
                      onRefresh: () async =>
                          await cubit.getJoinedChallenges(filter: context.read<HomeCubit>().categoryType));
                } else {
                  if (cubit.joinedChallengesList!.isEmpty) {
                    return RefreshIndicator(
                        onRefresh: () async =>
                            await cubit.getJoinedChallenges(filter: context.read<HomeCubit>().categoryType),
                        child: ListView(children: [NoStateWidget(height: height * 0.35)]));
                  } else {
                    return ParticipatedInListViewBuilder(challengeList: cubit.joinedChallengesList!);
                  }
                }
              },
            ),
          ),
        ],
      ),
    );
  }
}
