import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../../../data/models/challenge/won_challenge_model.dart';

import '../../../../../config/router/router.dart';
import '../../../../../utils/constants/app_const.dart';
import '../../../../../utils/constants/app_images.dart';
import '../../../home/cubit/home_cubit.dart';
import '../../cubit/challenge_cubit.dart';
import '../../screens/view_challenge/view_challenge_screen.dart';
import '../../widgets/challenge_card.dart';

class WonListViewBuilder extends StatelessWidget {
  const WonListViewBuilder({
    Key? key,
    required this.challengeList,
  }) : super(key: key);

  final List<WonChallengeModel> challengeList;

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      onRefresh: () async =>
          ChallengeCubit.get(context).getWonChallenges(filter: context.read<HomeCubit>().categoryType),
      child: ListView.builder(
        itemCount: challengeList.length,
        itemBuilder: (context, index) {
          final challenge = challengeList[index];
          return ChallengeCard(
            image: challenge.media ?? placeHolderUrl,
            color: challenge.category!.color!.toHex(),
            title: challenge.title ?? "notAvailable".tr(),
            supTitle: challenge.supTitle ?? "notAvailable".tr(),
            padding: EdgeInsets.only(bottom: height * 0.016),
            onTap: () => MagicRouter.navigateTo(BlocProvider.value(
              value: ChallengeCubit()..onInit(challengeId: challenge.id!),
              child: ViewChallengeScreen(challengeId: challenge.id!),
            )),
          );
        },
      ),
    );
  }
}
