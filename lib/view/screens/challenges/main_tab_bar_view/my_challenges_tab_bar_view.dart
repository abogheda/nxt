import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import '../../../../utils/constants/app_const.dart';
import 'my_challenges_tab_bar_view/won_tab_bar_view.dart';
import '../../../../config/router/router.dart';
import 'my_challenges_tab_bar_view/participated_in_tab_bar_view.dart';

class MyChallengesTabBarView extends StatefulWidget {
  const MyChallengesTabBarView({Key? key}) : super(key: key);

  @override
  State<MyChallengesTabBarView> createState() => _MyChallengesTabBarViewState();
}

class _MyChallengesTabBarViewState extends State<MyChallengesTabBarView> with SingleTickerProviderStateMixin {
  late TabController _tabController;

  List<Tab> tabTitleList = [Tab(child: Text('participatedIn'.tr())), Tab(child: Text('won'.tr()))];

  List<Widget> tabViewList = [const ParticipatedInTabBarView(), const WonTabBarView()];
  @override
  void initState() {
    _tabController = TabController(length: tabTitleList.length, vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    _tabController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          height: widget.height * 0.06,
          child: TabBar(
            physics: const NeverScrollableScrollPhysics(),
            controller: _tabController,
            indicatorColor: Colors.black,
            labelColor: Colors.black,
            unselectedLabelColor: const Color(0xFF989898),
            labelStyle: Theme.of(MagicRouter.currentContext!)
                .textTheme
                .headline6!
                .copyWith(fontFamily: AppConst.isEn ? 'BebasNeue' : 'Tajawal'),
            tabs: tabTitleList,
          ),
        ),
        Expanded(
          child: TabBarView(
            physics: const NeverScrollableScrollPhysics(),
            controller: _tabController,
            children: tabViewList,
          ),
        )
      ],
    );
  }
}
