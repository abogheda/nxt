import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import '../../../../utils/constants/app_const.dart';
import 'challenges_tab_bar_view/all_tab_bar_view.dart';
import 'challenges_tab_bar_view/opened_tab_bar_view.dart';
import '../../../../config/router/router.dart';

class ChallengesTabBarView extends StatefulWidget {
  const ChallengesTabBarView({Key? key}) : super(key: key);

  @override
  State<ChallengesTabBarView> createState() => _ChallengesTabBarViewState();
}

class _ChallengesTabBarViewState extends State<ChallengesTabBarView> with SingleTickerProviderStateMixin {
  late TabController _tabController;
  List<Tab> tabTitleList = [Tab(child: Text('all'.tr())), Tab(child: Text('opened'.tr()))];
  List<Widget> tabViewList = [const AllTabBarView(), const OpenedTabBarView()];

  @override
  void initState() {
    _tabController = TabController(length: tabTitleList.length, vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    _tabController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          height: widget.height * 0.06,
          child: TabBar(
            physics: const NeverScrollableScrollPhysics(),
            controller: _tabController,
            indicatorColor: Colors.black,
            labelColor: Colors.black,
            labelStyle: Theme.of(MagicRouter.currentContext!)
                .textTheme
                .headline6!
                .copyWith(fontFamily: AppConst.isEn ? 'BebasNeue' : 'Tajawal'),
            unselectedLabelColor: const Color(0xFF989898),
            tabs: tabTitleList,
          ),
        ),
        Expanded(
          child: TabBarView(
            physics: const NeverScrollableScrollPhysics(),
            controller: _tabController,
            children: tabViewList,
          ),
        )
      ],
    );
  }
}
