import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../../../utils/constants/resources.dart';

import '../../../../widgets/error_state_widget.dart';
import '../../../../widgets/no_state_widget.dart';
import '../../../../widgets/shimmer_widgets/challenges_card_list_view_shimmer.dart';
import '../../../home/cubit/home_cubit.dart';
import '../../cubit/challenge_cubit.dart';
import '../../widgets/all_challenges_list_view_builder.dart';

class OpenedTabBarView extends StatelessWidget {
  const OpenedTabBarView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ChallengeCubit cubit = ChallengeCubit.get(context);
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: width * 0.03),
      child: Column(
        children: [
          SizedBox(height: height * 0.016),
          Expanded(
            child: BlocBuilder<ChallengeCubit, ChallengeState>(
              buildWhen: (previous, current) => current is! ChallengeInitial,
              builder: (context, state) {
                if (state is ChallengeLoading) {
                  return const ChallengesCardListViewShimmer();
                } else if (state is ChallengeFailed) {
                  return ErrorStateWidget(
                    hasRefresh: true,
                    onRefresh: () async =>
                        ChallengeCubit.get(context).getAllChallenges(filter: context.read<HomeCubit>().categoryType),
                  );
                } else {
                  if (cubit.openChallengesList!.isEmpty) {
                    return RefreshIndicator(
                        onRefresh: () async =>
                            await cubit.getAllChallenges(filter: context.read<HomeCubit>().categoryType),
                        child: ListView(children: [NoStateWidget(height: height * 0.35)]));
                  } else {
                    return AllChallengesListViewBuilder(challengeList: cubit.openChallengesList!);
                  }
                }
              },
            ),
          ),
        ],
      ),
    );
  }
}
