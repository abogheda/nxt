import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../../widgets/shimmer_widgets/challenges_card_list_view_shimmer.dart';

import '../../../../../utils/constants/resources.dart';
import '../../../../widgets/error_state_widget.dart';
import '../../../../widgets/no_state_widget.dart';
import '../../../home/cubit/home_cubit.dart';
import '../../cubit/challenge_cubit.dart';
import '../../widgets/all_challenges_list_view_builder.dart';

class AllTabBarView extends StatelessWidget {
  const AllTabBarView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: width * 0.03),
      child: Column(
        children: [
          SizedBox(height: height * 0.016),
          Expanded(
            child: BlocBuilder<ChallengeCubit, ChallengeState>(
              buildWhen: (previous, current) => current is! ChallengeInitial,
              builder: (context, state) {
                final cubit = ChallengeCubit.get(context);
                if (state is ChallengeLoading || cubit.allChallenges == null) {
                  return const ChallengesCardListViewShimmer();
                }
                if (state is ChallengeFailed) {
                  return ErrorStateWidget(
                    hasRefresh: true,
                    onRefresh: () async => await ChallengeCubit.get(context)
                        .getAllChallenges(filter: context.read<HomeCubit>().categoryType),
                  );
                }
                if (cubit.allChallenges!.isEmpty) {
                  return NoStateWidget();
                } else {
                  return AllChallengesListViewBuilder(challengeList: cubit.allChallenges!);
                }
              },
            ),
          ),
        ],
      ),
    );
  }
}
