import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:transparent_image/transparent_image.dart';
import '../../../../utils/constants/resources.dart';

class ChallengeCard extends StatelessWidget {
  const ChallengeCard({
    Key? key,
    required this.image,
    required this.color,
    required this.title,
    required this.supTitle,
    required this.onTap,
    required this.padding,
    this.alignStart = true,
  }) : super(key: key);
  final String image;
  final int color;
  final String title;
  final String supTitle;
  final void Function()? onTap;
  final EdgeInsetsGeometry padding;
  final bool alignStart;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: padding,
      child: Stack(
        alignment: Alignment.bottomCenter,
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(8),
            child: FadeInImage.memoryNetwork(
              image: image,
              fit: BoxFit.cover,
              height: height * 0.21,
              width: double.infinity,
              placeholder: kTransparentImage,
            ),
          ),
          Positioned.fill(
            child: DecoratedBox(
              decoration: BoxDecoration(
                color: Colors.black.withOpacity(0.5),
                borderRadius: BorderRadius.circular(8),
              ),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Container(
                decoration: BoxDecoration(
                    color: Color(color),
                    borderRadius: BorderRadius.only(
                        bottomRight: const Radius.circular(8),
                        bottomLeft: const Radius.circular(8),
                        topLeft: Radius.circular(isEn ? 8 : 0),
                        topRight: Radius.circular(isEn ? 0 : 8))),
                height: 7,
                width: width * 0.22,
              ),
            ],
          ),
          Positioned(
            left: isEn
                ? alignStart
                    ? width * 0.03
                    : null
                : null,
            right: isEn
                ? null
                : alignStart
                    ? width * 0.03
                    : null,
            child: Column(
              crossAxisAlignment: alignStart ? CrossAxisAlignment.start : CrossAxisAlignment.center,
              children: [
                Text(
                  "#$title",
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                  textAlign: TextAlign.center,
                  style:
                      TextStyle(fontSize: 23, fontFamily: AppConst.isEn ? 'BebasNeue' : 'Tajawal', color: Colors.white),
                ),
                const SizedBox(height: 10),
                Text(
                  supTitle,
                  maxLines: 3,
                  overflow: TextOverflow.ellipsis,
                  textAlign: alignStart ? TextAlign.start : TextAlign.center,
                  style: AppTextStyles.semiBold14
                      .apply(color: const Color(0xffCECECE), fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal'),
                ),
                const SizedBox(height: 10),
                MaterialButton(
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
                  color: Colors.white,
                  onPressed: onTap,
                  child: Text(
                    'viewChallenge'.tr(),
                    style: AppTextStyles.semiBold13.copyWith(fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal'),
                    textAlign: TextAlign.center,
                  ),
                ),
                const SizedBox(height: 8),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
