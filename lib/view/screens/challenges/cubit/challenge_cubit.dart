// ignore_for_file: unnecessary_import, depend_on_referenced_packages

import 'dart:async';
import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nxt/data/models/challenge/challenge.dart';
import 'package:nxt/data/models/challenge/view_submission_model.dart' as view_submission;
import 'package:nxt/data/repos/general_repo.dart';
import 'package:nxt/view/screens/challenges/screens/view_challenge/view_challenge_model.dart';

import '../../../../data/enum/report_type.dart';
import '../../../../data/enum/save.dart';
import '../../../../data/enum/talent_type.dart';
import '../../../../data/models/base_models/base_single_res.dart';
import '../../../../data/models/challenge/challenge_status.dart';
import '../../../../data/models/challenge/joined_challenge.dart';
import '../../../../data/models/challenge/vote_user_challenge.dart';
import '../../../../data/models/challenge/won_challenge_model.dart';
import '../../../../data/models/report/report_model.dart';
import '../../../../data/models/save/save_model.dart';
import '../../../../data/repos/challenges/challenges_repo.dart';
import '../../../../data/repos/imports.dart';
import '../../../../data/repos/media_repo.dart';
import '../../../../data/repos/report/report_repo.dart';
import '../../../../data/repos/user_challenge/user_challenge_repo.dart';
import '../../../../utils/helpers/picker_helper.dart';
import '../../../../utils/helpers/utils.dart';

part 'challenge_state.dart';

class ChallengeCubit extends Cubit<ChallengeState> {
  ChallengeCubit() : super(ChallengeInitial());
  static ChallengeCubit get(context) => BlocProvider.of(context);
  ChallengeStatus? challengeStatus;
  ViewChallengeModel? viewChallengeModel;
  view_submission.ViewSubmissionModel? viewSubmissionModel;
  List<Challenge>? allChallenges;
  List<Challenge>? openChallengesList;
  List<JoinedChallengeAll>? joinedChallengesList;
  List<WonChallengeModel>? wonChallengesList;
  List<VoteUserChallenge>? allVoteUserChallenge;
  File? userChallengeVideo;
  File? userChallengeAudio;
  File? userChallengeText;
  double reelUploadingProgress = 0;
  StreamController<double> streamController = StreamController<double>();
  Future<void> onInit({required String challengeId}) async {
    await getCurrentUserChallenge(challengeId);
    if (viewChallengeModel!.userJoined!) {
      await getSubmission(challengeId);
    }
  }

  Future<void> reportUserChallenge({required String userChallengeId}) async {
    emit(ReportUserChallengeLoading());
    final res = await ReportRepo.reportItem(
      contentId: userChallengeId,
      contentType: ReportType.userchallenge.type,
    );
    if (res.data != null) {
      emit(ReportUserChallengeDone(reportModel: res));
    } else {
      emit(ReportUserChallengeFailed(res.message ?? ""));
    }
  }

  Future<void> addVoteForUserChallenge({required String challengeId}) async {
    emit(AddVoteLoading());
    final res = await UserChallengeRepo.addVote(challengeId: challengeId);
    if (res.data != null) {
      emit(AddVoteSuccess());
    } else {
      emit(AddVoteFailed(msg: res.message ?? ""));
    }
  }

  Future<void> deleteVoteForUserChallenge({required String challengeId}) async {
    emit(DeleteVoteLoading());
    final res = await UserChallengeRepo.deleteVote(challengeId: challengeId);
    if (res.data != null) {
      emit(DeleteVoteSuccess());
    } else {
      emit(DeleteVoteFailed(msg: res.message ?? ""));
    }
  }

  Future<void> addUserChallengeToSavedItems({String? userChallengeId}) async {
    emit(SaveUserChallengeLoading());
    final res = await SaveRepo.addToSavedItems(
      SaveModel(
        item: userChallengeId,
        type: SavedItemsEnum.userchallenges,
      ),
    );
    if (res.data != null) {
      emit(SaveUserChallengeSuccess());
    } else {
      emit(SaveUserChallengeFailed(msg: res.message ?? ""));
    }
  }

  Future<void> deleteUserChallengeFromSavedItems({String? userChallengeId}) async {
    emit(SaveUserChallengeLoading());
    final res = await SaveRepo.deleteFromSavedItems(
      SaveModel(
        item: userChallengeId,
        type: SavedItemsEnum.userchallenges,
      ),
    );
    if (res.data != null) {
      emit(SaveUserChallengeSuccess());
    } else {
      emit(SaveUserChallengeFailed(msg: res.message ?? ""));
    }
  }

  Future<void> addChallengeToSavedItems({String? challengeId}) async {
    emit(SaveChallengeLoading());
    final res = await SaveRepo.addToSavedItems(
      SaveModel(
        item: challengeId,
        type: SavedItemsEnum.challenges,
      ),
    );
    if (res.data != null) {
      emit(SaveChallengeSuccess());
    } else {
      emit(SaveChallengeFailed(msg: res.message ?? ""));
    }
  }

  Future<void> deleteChallengeFromSavedItems({String? challengeId}) async {
    emit(SaveChallengeLoading());
    final res = await SaveRepo.deleteFromSavedItems(
      SaveModel(
        item: challengeId,
        type: SavedItemsEnum.challenges,
      ),
    );
    if (res.data != null) {
      emit(SaveChallengeSuccess());
    } else {
      emit(SaveChallengeFailed(msg: res.message ?? ""));
    }
  }

  Future<void> addOrRemoveLikeOnUserChallenge({String? userChallengeId}) async {
    emit(LikeOnUserChallengeLoading());
    final res = await UserChallengeRepo.addOrRemoveLikeOnUserChallenge(userChallengeId: userChallengeId!);
    if (res.data != null) {
      emit(LikeOnUserChallengeDone());
    } else {
      emit(LikeOnUserChallengeFailed(res.message ?? ""));
    }
  }

  Future<bool> pickVideo() async {
    final video = await PickerHelper.pickVideoFile();
    if (video != null) {
      userChallengeVideo = video;
      return true;
    }
    return false;
  }

  Future<bool> pickAudio() async {
    final audio = await PickerHelper.pickAudioFile();
    if (audio != null) {
      userChallengeAudio = File(audio.path);
      return true;
    }
    return false;
  }

  Future<bool> pickText() async {
    final text = await PickerHelper.pickTextFile();
    if (text != null) {
      userChallengeText = File(text.path);
      return true;
    }
    return false;
  }

  Future<void> uploadChallengeMedia(
      {required String challengeId, required num reSubmitCount, required File media}) async {
    emit(UploadLoading());
    final res = await GeneralRepo.uploadMedia([media], onProgress: (fileNum, progress, total) {
      reelUploadingProgress = progress / total;
      streamController.sink.add(reelUploadingProgress);
      if (reelUploadingProgress == 1.0) {
        streamController.close();
        streamController = StreamController<double>();
      }
    });
    res.fold(
      (urls) async {
        emit(const UploadDone(challengeDataUploaded: false));
        challengeStatus = reSubmitCount == 0
            ? await MediaRepo.postChallengeMedia(media: urls.first, challengeId: challengeId)
            : await MediaRepo.patchChallengeMedia(media: urls.first, challengeId: challengeId);
      },
      (errorMsg) {
        emit(UploadingFailed(errorMsg));
      },
    );
  }

  Future<void> getAllChallenges({TalentCategoryType? filter}) async {
    emit(ChallengeLoading());
    final res = await ChallengeRepo.getChallenges(filter?.id);
    if (res.data != null) {
      allChallenges = res.data!;
      openChallengesList = allChallenges!
          .where((challenge) =>
              DateTime.now().isAfter(Utils.localDateFromIsoUTC(challenge.registerationStartDate!.toString())!) &&
              DateTime.now().isBefore(Utils.localDateFromIsoUTC(challenge.registerationEndDate!.toString())!))
          .toList();
      emit(ChallengeSuccess(challenge: res.data!));
    } else {
      emit(ChallengeFailed(res.message ?? ""));
    }
  }

  Future<void> getJoinedChallenges({TalentCategoryType? filter}) async {
    emit(GetJoinedChallengeLoading());
    final res = await ChallengeRepo.getJoinedChallenges(filter?.id);
    if (res.data != null) {
      joinedChallengesList = res.data!;
      emit(GetJoinedChallengeSuccess());
    } else {
      emit(GetJoinedChallengeFailed(res.message ?? ""));
    }
  }

  Future<void> getWonChallenges({TalentCategoryType? filter}) async {
    emit(GetWonChallengeLoading());
    final res = await ChallengeRepo.getWonChallenges(filter?.id);
    if (res.data != null) {
      wonChallengesList = res.data!;
      emit(GetWonChallengeSuccess());
    } else {
      emit(GetWonChallengeFailed(res.message ?? ""));
    }
  }

  Future<void> getVotesForChallenge(String challengeId) async {
    emit(ChallengeVotesLoading());
    final res = await ChallengeRepo.getVotesForChallenge(challengeId);
    if (res.data != null) {
      allVoteUserChallenge = res.data!;
      emit(ChallengeVotesSuccess());
    } else {
      emit(ChallengeVotesFailed(res.message ?? ""));
    }
  }

  Future<void> getCurrentUserChallenge(String challengeId) async {
    emit(ViewChallengeLoading());
    final res = await ChallengeRepo.getUserChallengeForUser(challengeId);
    if (res.data != null) {
      viewChallengeModel = res.data!;
      emit(ViewChallengeSuccess());
    } else {
      emit(ViewChallengeFailed(res.message ?? ""));
    }
  }

  Future<void> getSubmission(String challengeId) async {
    emit(GetSubmissionLoading());
    final res = await ChallengeRepo.getSubmission(challengeId);
    if (res.data != null) {
      viewSubmissionModel = res.data!;
      emit(GetSubmissionSuccess());
    } else {
      emit(GetSubmissionFailed(res.message ?? ""));
    }
  }
}
