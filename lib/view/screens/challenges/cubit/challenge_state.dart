part of 'challenge_cubit.dart';

abstract class ChallengeState extends Equatable {
  const ChallengeState();

  @override
  List<Object> get props => [];
}

class ChallengeInitial extends ChallengeState {}

class ChangeVoteState extends ChallengeState {}

class ChallengeSuccess extends ChallengeState {
  final List<Challenge> challenge;

  const ChallengeSuccess({required this.challenge});
}

class ChallengeLoading extends ChallengeState {}

class ChallengeFailed extends ChallengeState {
  final String msg;
  const ChallengeFailed(this.msg);
}

abstract class UploadingState extends ChallengeState {
  const UploadingState();
}

class UploadDone extends UploadingState {
  final bool challengeDataUploaded;

  const UploadDone({required this.challengeDataUploaded});
}

class UploadLoading extends UploadingState {}

class UploadingFailed extends UploadingState {
  final String errorMsg;
  const UploadingFailed(this.errorMsg);
}

class GetJoinedChallengeSuccess extends ChallengeState {}

class GetJoinedChallengeLoading extends ChallengeState {}

class GetJoinedChallengeFailed extends ChallengeState {
  final String msg;
  const GetJoinedChallengeFailed(this.msg);
}

class GetWonChallengeSuccess extends ChallengeState {}

class GetWonChallengeLoading extends ChallengeState {}

class GetWonChallengeFailed extends ChallengeState {
  final String msg;
  const GetWonChallengeFailed(this.msg);
}

class ViewChallengeLoading extends ChallengeState {}

class ViewChallengeSuccess extends ChallengeState {}

class ViewChallengeFailed extends ChallengeState {
  final String msg;
  const ViewChallengeFailed(this.msg);
}

class GetSubmissionLoading extends ChallengeState {}

class GetSubmissionSuccess extends ChallengeState {}

class GetSubmissionFailed extends ChallengeState {
  final String msg;
  const GetSubmissionFailed(this.msg);
}

class UserChallengeSuccess extends ChallengeState {}

class UserChallengeLoading extends ChallengeState {}

class UserChallengeFailed extends ChallengeState {}

class ChallengeVotesSuccess extends ChallengeState {}

class ChallengeVotesLoading extends ChallengeState {}

class ChallengeVotesFailed extends ChallengeState {
  final String msg;
  const ChallengeVotesFailed(this.msg);
}
//===============================================================

class AddVoteSuccess extends ChallengeState {}

class AddVoteLoading extends ChallengeState {}

class AddVoteFailed extends ChallengeState {
  final String msg;
  const AddVoteFailed({required this.msg});
}

class DeleteVoteSuccess extends ChallengeState {}

class DeleteVoteLoading extends ChallengeState {}

class DeleteVoteFailed extends ChallengeState {
  final String msg;
  const DeleteVoteFailed({required this.msg});
}
//===============================================================

class SaveUserChallengeSuccess extends ChallengeState {}

class SaveUserChallengeLoading extends ChallengeState {}

class SaveUserChallengeFailed extends ChallengeState {
  final String msg;
  const SaveUserChallengeFailed({required this.msg});
}

class SaveChallengeSuccess extends ChallengeState {}

class SaveChallengeLoading extends ChallengeState {}

class SaveChallengeFailed extends ChallengeState {
  final String msg;
  const SaveChallengeFailed({required this.msg});
}

class LikeOnUserChallengeDone extends ChallengeState {}

class LikeOnUserChallengeLoading extends ChallengeState {}

class LikeOnUserChallengeFailed extends ChallengeState {
  final String msg;
  const LikeOnUserChallengeFailed(this.msg);
}

class ReportUserChallengeDone extends ChallengeState {
  final BaseSingleResponse<ReportModel> reportModel;
  const ReportUserChallengeDone({required this.reportModel});
}

class ReportUserChallengeLoading extends ChallengeState {}

class ReportUserChallengeFailed extends ChallengeState {
  final String msg;
  const ReportUserChallengeFailed(this.msg);
}
