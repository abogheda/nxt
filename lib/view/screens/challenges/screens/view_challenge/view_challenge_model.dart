import '../../../../../data/models/base_models/base_model.dart';
import '../../../../../data/models/challenge/plan_price_model.dart';

class ViewChallengeModel extends BaseModel {
  ViewChallengeModel({
    this.id,
    this.prize,
    this.media,
    this.uploadType,
    this.category,
    this.released,
    this.registerationStartDate,
    this.registerationEndDate,
    this.votingStartDate,
    this.votingEndDate,
    this.winnerAnnouncementDate,
    this.featured,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.userJoined,
    this.userSaved,
    this.title,
    this.supTitle,
    this.description,
    this.requirements,
    this.terms,
  });
  @override
  fromJson(Map<String, dynamic> json) => ViewChallengeModel.fromJson(json);
  ViewChallengeModel.fromJson(dynamic json) {
    id = json['_id'];
    prize = json['prize'];
    media = json['media'];
    uploadType = json['uploadType'];
    category = json['category'] != null ? Category.fromJson(json['category']) : null;
    planPriceModel = json['price'] != null ? PlanPriceModel.fromJson(json['price']) : null;
    released = json['released'];
    registerationStartDate = json['registerationStartDate'];
    registerationEndDate = json['registerationEndDate'];
    votingStartDate = json['votingStartDate'];
    votingEndDate = json['votingEndDate'];
    winnerAnnouncementDate = json['winnerAnnouncementDate'];
    featured = json['featured'];
    removed = json['removed'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    v = json['__v'];
    userJoined = json['userJoined'];
    userSaved = json['userSaved'];
    title = json['title'];
    supTitle = json['supTitle'];
    description = json['description'];
    requirements = json['requirements'];
    terms = json['terms'];
  }
  String? id;
  String? prize;
  String? media;
  String? uploadType;
  Category? category;
  bool? released;
  String? registerationStartDate;
  String? registerationEndDate;
  String? votingStartDate;
  String? votingEndDate;
  String? winnerAnnouncementDate;
  bool? featured;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  num? v;
  bool? userJoined;
  bool? userSaved;
  String? title;
  String? supTitle;
  String? description;
  String? requirements;
  PlanPriceModel? planPriceModel;
  String? terms;
  ViewChallengeModel copyWith({
    String? id,
    String? prize,
    String? media,
    PlanPriceModel? planPriceModel,
    String? uploadType,
    Category? category,
    bool? released,
    String? registerationStartDate,
    String? registerationEndDate,
    String? votingStartDate,
    String? votingEndDate,
    String? winnerAnnouncementDate,
    bool? featured,
    bool? removed,
    String? createdAt,
    String? updatedAt,
    num? v,
    num? videocount,
    bool? userJoined,
    bool? userSaved,
    String? title,
    String? supTitle,
    String? description,
    String? requirements,
    String? terms,
  }) =>
      ViewChallengeModel(
        id: id ?? this.id,
        prize: prize ?? this.prize,
        media: media ?? this.media,
        uploadType: uploadType ?? this.uploadType,
        category: category ?? this.category,
        released: released ?? this.released,
        registerationStartDate: registerationStartDate ?? this.registerationStartDate,
        registerationEndDate: registerationEndDate ?? this.registerationEndDate,
        votingStartDate: votingStartDate ?? this.votingStartDate,
        votingEndDate: votingEndDate ?? this.votingEndDate,
        winnerAnnouncementDate: winnerAnnouncementDate ?? this.winnerAnnouncementDate,
        featured: featured ?? this.featured,
        removed: removed ?? this.removed,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
        v: v ?? this.v,
        userJoined: userJoined ?? this.userJoined,
        userSaved: userSaved ?? this.userSaved,
        title: title ?? this.title,
        supTitle: supTitle ?? this.supTitle,
        description: description ?? this.description,
        requirements: requirements ?? this.requirements,
        terms: terms ?? this.terms,
      );
  @override
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['_id'] = id;
    map['prize'] = prize;
    map['media'] = media;
    map['uploadType'] = uploadType;
    if (category != null) {
      map['category'] = category?.toJson();
    }
    map['released'] = released;
    map['registerationStartDate'] = registerationStartDate;
    map['registerationEndDate'] = registerationEndDate;
    map['votingStartDate'] = votingStartDate;
    map['votingEndDate'] = votingEndDate;
    map['winnerAnnouncementDate'] = winnerAnnouncementDate;
    map['featured'] = featured;
    map['removed'] = removed;
    map['createdAt'] = createdAt;
    map['updatedAt'] = updatedAt;
    map['__v'] = v;
    map['userJoined'] = userJoined;
    map['userSaved'] = userSaved;
    map['title'] = title;
    map['supTitle'] = supTitle;
    map['description'] = description;
    map['requirements'] = requirements;
    map['terms'] = terms;
    return map;
  }
}

class Category {
  Category({
    this.id,
    this.color,
    this.image,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.featured,
    this.title,
    this.description,
  });

  Category.fromJson(dynamic json) {
    id = json['_id'];
    color = json['color'];
    image = json['image'];
    removed = json['removed'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    v = json['__v'];
    featured = json['featured'];
    title = json['title'];
    description = json['description'];
  }
  String? id;
  String? color;
  String? image;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  num? v;
  bool? featured;
  String? title;
  String? description;
  Category copyWith({
    String? id,
    String? color,
    String? image,
    bool? removed,
    String? createdAt,
    String? updatedAt,
    num? v,
    bool? featured,
    String? title,
    String? description,
  }) =>
      Category(
        id: id ?? this.id,
        color: color ?? this.color,
        image: image ?? this.image,
        removed: removed ?? this.removed,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
        v: v ?? this.v,
        featured: featured ?? this.featured,
        title: title ?? this.title,
        description: description ?? this.description,
      );
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['_id'] = id;
    map['color'] = color;
    map['image'] = image;
    map['removed'] = removed;
    map['createdAt'] = createdAt;
    map['updatedAt'] = updatedAt;
    map['__v'] = v;
    map['featured'] = featured;
    map['title'] = title;
    map['description'] = description;
    return map;
  }
}
