// ignore_for_file: use_build_context_synchronously

import 'dart:io';

import 'package:easy_localization/easy_localization.dart' as local;
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nxt/data/service/hive_services.dart';
import 'package:nxt/view/screens/challenges/screens/view_challenge/view_challenge_model.dart';
import 'package:nxt/view/screens/challenges/screens/view_challenge/view_challenge_widgets/view_challenge_button.dart';
import 'package:nxt/view/screens/challenges/screens/view_challenge/view_your_submission_button.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:share_plus/share_plus.dart';

import '../../../../../config/router/router.dart';
import '../../../../../data/enum/upload_type.dart';
import '../../../../../data/models/challenge/view_submission_model.dart';
import '../../../../../data/models/choose_bundle/plan_model.dart';
import '../../../../../utils/constants/resources.dart';
import '../../../../../utils/helpers/dynamic_links_service.dart';
import '../../../../../utils/helpers/popup_helper.dart';
import '../../../../../utils/helpers/utils.dart';
import '../../../../widgets/app_loader.dart';
import '../../../../widgets/error_state_widget.dart';
import '../../../../widgets/save_button.dart';
import '../../../../widgets/share_button.dart';
import '../../../choose_your_bundle/choose_your_bundle.dart';
import '../../../more/more_widgets/custom_logout_dialog.dart';
import '../../../navigation_and_appbar/import_widget.dart';
import '../../cubit/challenge_cubit.dart';
import '../challenge_video/view_submission.dart';
import '../vote_now/challenge_vote.dart';
import 'view_challenge_widgets/challenge_image.dart';
import 'view_challenge_widgets/challenge_info.dart';

class ViewChallengeScreen extends StatefulWidget {
  const ViewChallengeScreen({Key? key, required this.challengeId}) : super(key: key);
  final String challengeId;

  @override
  State<ViewChallengeScreen> createState() => _ViewChallengeScreenState();
}

class _ViewChallengeScreenState extends State<ViewChallengeScreen> {
  late DynamicLinkService _dynamicLinkService;
  @override
  void initState() {
    initMethod();
    super.initState();
  }

  initMethod() async {
    if (HiveHelper.isLogged) _dynamicLinkService = DynamicLinkService();
    // await Utils.addSecureFlag();
  }

  @override
  void dispose() {
    // Utils.clearSecureFlag();
    super.dispose();
  }

  void navigateToVote(
      ViewChallengeModel challenge, ChallengeCubit cubit, DateTime votingStartDate, DateTime votingEndDate) {
    var now = DateTime.now();
    if (challenge.uploadType == UploadType.video.type || challenge.uploadType == UploadType.audio.type) {
      MagicRouter.navigateTo(BlocProvider.value(
        value: cubit..getVotesForChallenge(challenge.id!),
        child: ChallengeVote(
          challenge: challenge,
          showLike: now.isBefore(votingStartDate) || now.isAfter(votingEndDate),
          showVote: now.isAfter(votingStartDate) && now.isBefore(votingEndDate),
        ),
      ));
    }
    return;
  }

  Future<void> pickFileAndNavigate({
    required BuildContext context,
    required ChallengeCubit cubit,
    required ViewChallengeModel challenge,
    required num reSubmitCount,
    required Permission androidPermission,
    required Permission iOSPermission,
  }) async {
    var status = Platform.isAndroid ? await androidPermission.status : await iOSPermission.status;
    if (status.isGranted) {
      bool result = false;
      File? media;
      if (challenge.uploadType == UploadType.video.type) {
        result = await cubit.pickVideo();
        media = cubit.userChallengeVideo;
      } else if (challenge.uploadType == UploadType.audio.type) {
        result = await cubit.pickAudio();
        media = cubit.userChallengeAudio;
      } else if (challenge.uploadType == UploadType.text.type) {
        result = await cubit.pickText();
        media = cubit.userChallengeText;
      }
      if (result) {
        MagicRouter.navigateTo(BlocProvider.value(
          value: cubit..uploadChallengeMedia(challengeId: challenge.id!, reSubmitCount: reSubmitCount, media: media!),
          child: ViewSubmission(challenge: challenge),
        ));
        return;
      }
    } else if (status.isDenied) {
      final PermissionStatus permissionStatus =
          Platform.isAndroid ? await androidPermission.status : await iOSPermission.status;
      if (permissionStatus == PermissionStatus.permanentlyDenied || Platform.isIOS
          ? permissionStatus == PermissionStatus.denied
          : false) {
        await showPermissionDialog(context);
      }
    }
  }

  showPermissionDialog(BuildContext context) async {
    showDialog(
      context: context,
      builder: (context) {
        return Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            CustomAskPermissionDialog(
              onPressed: () async {
                MagicRouter.pop();
                await openAppSettings();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return CustomScaffoldWidget(
      extendBodyBehindAppBar: false,
      isBackArrow: true,
      isThereLogo: true,
      isThereActions: true,
      body: BlocConsumer<ChallengeCubit, ChallengeState>(
        listener: (context, state) {
          if (state is ViewChallengeFailed) PopupHelper.showBasicSnack(msg: state.msg.toString(), color: Colors.red);
        },
        builder: (context, state) {
          final cubit = ChallengeCubit.get(context);
          if (state is ViewChallengeLoading || state is GetSubmissionLoading) {
            return const AppLoader();
          } else if (state is ViewChallengeFailed) {
            return ErrorStateWidget(
                hasRefresh: true, onRefresh: () async => await cubit.onInit(challengeId: widget.challengeId));
          } else {
            final challenge = cubit.viewChallengeModel!;
            final challengeSubmission = cubit.viewSubmissionModel ?? ViewSubmissionModel(reSubmitCount: 0);
            final registrationStartDate = Utils.localDateFromIsoUTC(challenge.registerationStartDate!)!;
            final registrationEndDate = Utils.localDateFromIsoUTC(challenge.registerationEndDate!)!;
            final votingStartDate = Utils.localDateFromIsoUTC(challenge.votingStartDate!)!;
            final votingEndDate = Utils.localDateFromIsoUTC(challenge.votingEndDate!)!;
            final planModel = PlanModel(id: challenge.planPriceModel?.sId,title: challenge.planPriceModel?.lang![0].title
            ,features: challenge.planPriceModel?.lang![0].features,price: challenge.planPriceModel?.price);
            final winnerAnnouncementDate = Utils.localDateFromIsoUTC(challenge.winnerAnnouncementDate!)!;
            final dateFormat = local.DateFormat("MMMM d", widget.isEn ? 'en_US' : 'ar_EG');
            return Column(
              children: [
                Expanded(
                  child: RefreshIndicator(
                    onRefresh: () async => cubit.onInit(challengeId: widget.challengeId),
                    child: ListView(
                      children: [
                        const Divider(color: Colors.black, thickness: 0.9),
                        ChallengeImage(
                          challengeImg: challenge.media!,
                          challengeDesc: challenge.supTitle!.toUpperCase(),
                          challengeTitle: challenge.title ?? 'noTitle'.tr(),
                          nxtColor: Color(challenge.category!.color!.toHex()),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: widget.width * 0.02),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              SaveButton(
                                isSaved: challenge.userSaved ?? false,
                                saveColor: '#000000',
                                onPressed: () async {
                                  if (challenge.userSaved == true) {
                                    await cubit.deleteChallengeFromSavedItems(challengeId: challenge.id!);
                                    setState(() => challenge.userSaved = !challenge.userSaved!);
                                  } else {
                                    await cubit.addChallengeToSavedItems(challengeId: challenge.id!);
                                    setState(() => challenge.userSaved = !challenge.userSaved!);
                                  }
                                },
                              ),
                              ShareButton(
                                onPressed: () async {
                                  final viewChallengeUrl = await _dynamicLinkService.createDynamicLink(
                                      url: 'challenge/view-challenge/${challenge.id!}/');
                                  await Share.share(viewChallengeUrl.toString(), subject: 'checkChallenge'.tr());
                                },
                              ),
                            ],
                          ),
                        ),
                        ChallengeInfo(
                          challengeDescription: "#${challenge.description ?? 'noDescription'.tr()}",
                          challengePrize: challenge.prize!,
                          challengeRequirements: challenge.requirements!,
                          challengeRegister:
                              " ${dateFormat.format(registrationStartDate)} → ${dateFormat.format(registrationEndDate)}",
                          challengeRegisterTime: " (${registrationEndDate.difference(
                                registrationStartDate,
                              ).inDays} ${'days'.tr()})",
                          challengeVote:
                              " ${dateFormat.format(votingStartDate)} → ${dateFormat.format(votingEndDate)} ",
                          challengeWinnerTime: " ${dateFormat.format(winnerAnnouncementDate)}",
                          challengeVoteTime: "(${votingEndDate.difference(votingStartDate).inDays} ${'days'.tr()})",
                        ),
                      ],
                    ),
                  ),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    challenge.userJoined == false || cubit.viewSubmissionModel == null
                        ? const SizedBox()
                        : ViewYourSubmissionButton(challenge: challenge, cubit: cubit),
                    Container(
                        height: widget.height * 0.01,
                        width: double.infinity,
                        color: challenge.category!.color == null
                            ? AppColors.blueColor
                            : Color(challenge.category!.color!.toHex())),
                    MaterialButton(
                      height: widget.height * 0.09,
                      minWidth: double.infinity,
                      shape: const ContinuousRectangleBorder(),
                      color: Colors.black,
                      child: buildButton(challenge: challenge, challengeSubmission: challengeSubmission),
                      onPressed: () async {
                        var now = DateTime.now();
                        //===============================================================
                        if (now.isBefore(registrationStartDate)) {
                          debugPrint("1");
                          return;
                        }
                        //===============================================================
                        if (now.isBefore(votingStartDate) && now.isAfter(registrationEndDate)) {
                          debugPrint("2");
                          return;
                        }
                        //===============================================================
                        if (now.isAfter(winnerAnnouncementDate)) {
                          await Utils.launchAppUrl(
                              url: AppConst.isProduction
                                  ? "${AppConst.productionUrl}/challenge/view-challenge/${challenge.id!}/voting/"
                                  : "${AppConst.developmentUrl}/challenge/view-challenge/${challenge.id!}/voting/");
                          return;
                        }
                        //===============================================================
                        if (now.isAfter(votingEndDate) && now.isBefore(winnerAnnouncementDate)) {
                          navigateToVote(challenge, cubit, votingStartDate, votingEndDate);
                        }
                        //===============================================================
                        if (now.isAfter(Utils.localDateFromIsoUTC(challenge.votingStartDate!)!) &&
                            now.isBefore(Utils.localDateFromIsoUTC(challenge.votingEndDate!)!)) {
                          navigateToVote(challenge, cubit, votingStartDate, votingEndDate);
                        }
                        //===============================================================
                        if(HiveHelper.getUserInfo?.user?.subscriptionEnd == false || cubit.viewChallengeModel?.userJoined == true){
                          if (now.isAfter(Utils.localDateFromIsoUTC(challenge.registerationStartDate!)!) &&
                              now.isBefore(Utils.localDateFromIsoUTC(challenge.registerationEndDate!)!)) {
                            if (challengeSubmission.reSubmitCount! < 3) {
                              await pickFileAndNavigate(
                                cubit: cubit,
                                context: context,
                                challenge: challenge,
                                iOSPermission: Permission.storage,
                                androidPermission: Permission.mediaLibrary,
                                reSubmitCount: challengeSubmission.reSubmitCount!,
                              );
                            }
                          }
                        }
                        else{
                        // var model =  PlanModel(id: "255",title: "test",features: ["dddd","sssss"]);
                          MagicRouter.navigateTo( ChooseYourBundle(fromHome: true,planModel: planModel,));
                        }
                      },
                    ),
                  ],
                ),
              ],
            );
          }
        },
      ),
    );
  }
}
