import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lottie/lottie.dart';
import '../../../../../../utils/constants/app_const.dart';
import '../../../cubit/challenge_cubit.dart';

class ViewSubmissionAudioPlayer extends StatefulWidget {
  const ViewSubmissionAudioPlayer({Key? key, required this.url, required this.title}) : super(key: key);
  final String url;
  final String title;

  @override
  State<ViewSubmissionAudioPlayer> createState() => _ViewSubmissionAudioPlayerState();
}

class _ViewSubmissionAudioPlayerState extends State<ViewSubmissionAudioPlayer> {
  final audioPlayer = AudioPlayer();
  bool isPlaying = false;
  Duration duration = Duration.zero;
  Duration position = Duration.zero;
  late ChallengeCubit cubit;
  Future setAudio() async {
    audioPlayer.setSourceUrl(widget.url);
  }

  @override
  void initState() {
    super.initState();
    setAudio();
    audioPlayer.onPlayerStateChanged.listen((state) {
      setState(() {
        isPlaying = state == PlayerState.playing;
      });

      audioPlayer.onDurationChanged.listen((newDuration) {
        setState(() {
          duration = newDuration;
        });
      });

      audioPlayer.onPositionChanged.listen((newPosition) {
        setState(() {
          position = newPosition;
        });
      });
    });
    cubit = context.read<ChallengeCubit>();
  }

  @override
  void dispose() {
    audioPlayer.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(elevation: 0.0, backgroundColor: Colors.transparent, foregroundColor: Colors.black),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Stack(
              alignment: Alignment.center,
              children: <Widget>[
                Lottie.asset('assets/images/circular_audio.json', fit: BoxFit.cover, animate: isPlaying),
                SizedBox(
                  width: widget.width * 0.3,
                  height: widget.width * 0.3,
                  child: Image.asset(
                    'assets/images/logo.png',
                  ),
                ),
              ],
            ),
            const SizedBox(height: 16.0),
            Text(widget.title,
                style: TextStyle(fontFamily: widget.isEn ? 'Montserrat' : 'Tajawal', fontWeight: FontWeight.bold)),
            const SizedBox(height: 16.0),
            Slider(
              min: 0,
              max: duration.inSeconds.toDouble(),
              value: position.inSeconds.toDouble(),
              onChanged: (value) async {
                final position = Duration(seconds: value.toInt());
                await audioPlayer.seek(position);
                await audioPlayer.resume();
              },
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    formatTime(duration: position),
                    style: TextStyle(fontFamily: widget.isEn ? 'Montserrat' : 'Tajawal', fontWeight: FontWeight.bold),
                  ),
                  Text(
                    formatTime(duration: duration - position),
                    style: TextStyle(fontFamily: widget.isEn ? 'Montserrat' : 'Tajawal', fontWeight: FontWeight.bold),
                  ),
                ],
              ),
            ),
            CircleAvatar(
              radius: 36.0,
              backgroundColor: Colors.black,
              child: IconButton(
                icon: Icon(isPlaying ? Icons.pause : Icons.play_arrow, color: Colors.white),
                onPressed: () async {
                  if (isPlaying) {
                    await audioPlayer.pause();
                  } else {
                    await audioPlayer.resume();
                  }
                },
              ),
            ),
            // Padding(
            //   padding: EdgeInsets.symmetric(horizontal: widget.width * 0.2, vertical: 20),
            //   child: Row(
            //     mainAxisAlignment: MainAxisAlignment.spaceAround,
            //     children: <Widget>[
            //       TextButton.icon(
            //         label: Text(
            //           cubit.viewSubmissionModel!.userSaved ?? false ? 'saved'.tr() : 'save'.tr(),
            //           style: textStyle.copyWith(color: Colors.black),
            //         ),
            //         onPressed: () async {
            //           if (cubit.viewSubmissionModel!.userSaved! == false) {
            //             await cubit.addUserChallengeToSavedItems(userChallengeId: cubit.viewSubmissionModel!.id);
            //           } else {
            //             await cubit.deleteUserChallengeFromSavedItems(userChallengeId: cubit.viewSubmissionModel!.id);
            //           }
            //           setState(() {
            //             if (cubit.viewSubmissionModel!.userSaved! == false) {}
            //             if (cubit.viewSubmissionModel!.userSaved! == true) {
            //               cubit.viewSubmissionModel!.saveCount = cubit.viewSubmissionModel!.saveCount! - 1;
            //             }
            //             cubit.viewSubmissionModel!.userSaved = !cubit.viewSubmissionModel!.userSaved!;
            //           });
            //         },
            //         icon: SvgPicture.asset(
            //           cubit.viewSubmissionModel!.userSaved ?? false
            //               ? 'assets/images/saved.svg'
            //               : 'assets/images/save.svg',
            //           color: Colors.black,
            //         ),
            //       ),
            //     ],
            //   ),
            // )
          ],
        ),
      ),
    );
  }
}

String formatTime({required Duration duration}) {
  String twoDigits(int n) => n.toString().padLeft(2, '0');
  final hours = twoDigits(duration.inHours);
  final minutes = twoDigits(duration.inMinutes.remainder(60));
  final seconds = twoDigits(duration.inSeconds.remainder(60));
  return [if (duration.inHours > 0) hours, minutes, seconds].join(':');
}
