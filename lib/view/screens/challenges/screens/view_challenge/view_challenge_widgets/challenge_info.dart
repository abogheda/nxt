import 'package:easy_localization/easy_localization.dart' as local;
import 'package:flutter/material.dart';
import '../../../../../../utils/constants/app_colors.dart';
import '../../../../../../utils/constants/app_const.dart';

class ChallengeInfo extends StatefulWidget {
  final String challengeDescription;
  final String challengeRequirements;
  final String challengePrize;
  final String challengeRegister;
  final String challengeRegisterTime;
  final String challengeVoteTime;
  final String challengeVote;
  final String challengeWinnerTime;

  const ChallengeInfo({
    Key? key,
    required this.challengeDescription,
    required this.challengeRequirements,
    required this.challengePrize,
    required this.challengeRegister,
    required this.challengeRegisterTime,
    required this.challengeVoteTime,
    required this.challengeVote,
    required this.challengeWinnerTime,
  }) : super(key: key);

  @override
  State<ChallengeInfo> createState() => _ChallengeInfoState();
}

class _ChallengeInfoState extends State<ChallengeInfo> {
  @override
  Widget build(BuildContext context) {
    final titleTextStyle =
        Theme.of(context).textTheme.titleLarge!.copyWith(fontFamily: widget.isEn ? 'BebasNeue' : 'Tajawal');
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 12),
          child: Align(
            alignment: widget.isEn ? Alignment.centerLeft : Alignment.centerRight,
            child: RichText(
              text: TextSpan(
                text: "",
                children: [
                  TextSpan(text: "Description \n", style: titleTextStyle),
                  TextSpan(
                    text: widget.challengeDescription,
                    style: TextStyle(
                      height: 1.2,
                      color: AppColors.iconsColor,
                      fontFamily: widget.isEn ? 'Inter-Medium' : 'Tajawal',
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(bottom: widget.height * 0.006),
          child: const Divider(color: AppColors.greyOutText, thickness: 0.5),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 12),
          child: Align(
            alignment: widget.isEn ? Alignment.centerLeft : Alignment.centerRight,
            child: Text('requirements'.tr(), style: titleTextStyle),
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 12),
          child: Align(
            alignment: widget.isEn ? Alignment.centerLeft : Alignment.centerRight,
            child: Text(
              widget.challengeRequirements,
              style: TextStyle(
                height: 1.2,
                color: AppColors.iconsColor,
                fontFamily: widget.isEn ? 'Inter-Medium' : 'Tajawal',
              ),
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(bottom: widget.height * 0.006),
          child: const Divider(color: AppColors.greyOutText, thickness: 0.5),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 12),
          child: Align(
            alignment: widget.isEn ? Alignment.centerLeft : Alignment.centerRight,
            child: Text('prizes'.tr(), style: titleTextStyle),
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 12),
          child: Align(
            alignment: widget.isEn ? Alignment.centerLeft : Alignment.centerRight,
            child: Text(
              widget.challengePrize,
              style: TextStyle(
                height: 1.2,
                color: AppColors.iconsColor,
                fontFamily: widget.isEn ? 'Inter-Medium' : 'Tajawal',
              ),
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(bottom: widget.height * 0.006),
          child: const Divider(color: AppColors.greyOutText, thickness: 0.5),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 12),
          child: Align(
            alignment: widget.isEn ? Alignment.centerLeft : Alignment.centerRight,
            child: Text('deadline'.tr(), style: titleTextStyle),
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 4),
          child: Align(
            alignment: widget.isEn ? Alignment.centerLeft : Alignment.centerRight,
            child: RichText(
              text: TextSpan(
                text: "reg_sub".tr(),
                style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                      color: const Color(0xff555555),
                      fontWeight: FontWeight.bold,
                      fontFamily: widget.isEn ? 'Inter-Medium' : 'Tajawal',
                    ),
                children: [
                  TextSpan(
                    text: widget.challengeRegister,
                    style: const TextStyle(
                      fontSize: 13,
                      color: AppColors.iconsColor,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                  TextSpan(
                      text: widget.challengeRegisterTime,
                      style: const TextStyle(fontSize: 13, color: AppColors.iconsColor, fontWeight: FontWeight.w500)),
                ],
              ),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 4),
          child: Align(
            alignment: widget.isEn ? Alignment.centerLeft : Alignment.centerRight,
            child: RichText(
              text: TextSpan(
                text: "-${'voting'.tr()}:",
                style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                      color: const Color(0xff555555),
                      fontWeight: FontWeight.bold,
                      fontFamily: widget.isEn ? 'Inter-Medium' : 'Tajawal',
                    ),
                children: [
                  TextSpan(
                    text: widget.challengeVote,
                    style: const TextStyle(
                      fontSize: 13,
                      color: AppColors.iconsColor,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                  TextSpan(
                    text: widget.challengeVoteTime,
                    style: const TextStyle(
                      fontSize: 13,
                      color: AppColors.iconsColor,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 4),
          child: Align(
            alignment: widget.isEn ? Alignment.centerLeft : Alignment.centerRight,
            child: RichText(
              text: TextSpan(
                text: "-${'winnerAnnounced'.tr()}:",
                style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                      color: const Color(0xff555555),
                      fontWeight: FontWeight.bold,
                      fontFamily: widget.isEn ? 'Inter-Medium' : 'Tajawal',
                    ),
                children: [
                  TextSpan(
                    text: widget.challengeWinnerTime,
                    style: const TextStyle(
                      fontSize: 13,
                      color: AppColors.iconsColor,
                      fontWeight: FontWeight.w400,
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}
