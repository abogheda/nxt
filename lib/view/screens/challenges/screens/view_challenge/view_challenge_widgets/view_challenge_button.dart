import 'package:easy_localization/easy_localization.dart' as local;
import 'package:flutter/material.dart';

import '../../../../../../data/models/challenge/view_submission_model.dart';
import '../../../../../../utils/constants/app_const.dart';
import '../../../../../../utils/constants/app_text_style.dart';
import '../../../../../../utils/helpers/utils.dart';
import '../view_challenge_model.dart';

Widget buildButton({required ViewChallengeModel challenge, required ViewSubmissionModel? challengeSubmission}) {
  final textStySemiBold16 =
      AppTextStyles.semiBold16.apply(color: Colors.white, fontFamily: AppConst.isEn ? 'Inter-Medium' : 'Tajawal');
  final textStyleRegular16 =
      AppTextStyles.regular16.apply(color: Colors.white, fontFamily: AppConst.isEn ? 'Inter-Medium' : 'Tajawal');
  final textStyleBold18 =
      AppTextStyles.bold18.apply(color: Colors.white, fontFamily: AppConst.isEn ? 'Inter-Medium' : 'Tajawal');

  var now = DateTime.now();
  final registrationStartDate = Utils.localDateFromIsoUTC(challenge.registerationStartDate!)!;
  print("start date :$registrationStartDate");
  final registrationEndDate = Utils.localDateFromIsoUTC(challenge.registerationEndDate!)!;
  final votingStartDate = Utils.localDateFromIsoUTC(challenge.votingStartDate!)!;
  final votingEndDate = Utils.localDateFromIsoUTC(challenge.votingEndDate!)!;
  final winnerAnnouncementDate = Utils.localDateFromIsoUTC(challenge.winnerAnnouncementDate!)!;
  if (now.isBefore(registrationStartDate)) {
    return Text(
      'comingSoon'.tr().toUpperCase(),
      style: textStySemiBold16,
      textAlign: TextAlign.center,
    );
  }
  if (now.isBefore(votingStartDate) && now.isAfter(registrationEndDate)) {
    return Text(
      'votingSoon'.tr().toUpperCase(),
      style: textStySemiBold16,
      textAlign: TextAlign.center,
    );
  }
  if (now.isAfter(votingStartDate) && now.isBefore(votingEndDate)) {
    return Text(
      'votesOpen'.tr(),
      style: textStySemiBold16,
      textAlign: TextAlign.center,
    );
  }
  if (now.isAfter(votingEndDate) && now.isBefore(winnerAnnouncementDate)) {
    return Text('waitWinner'.tr(), style: textStySemiBold16, textAlign: TextAlign.center);
  }
  if (now.isAfter(registrationStartDate) && now.isBefore(registrationEndDate)) {
    if (challengeSubmission == null ||
        challengeSubmission.reSubmitCount == null ||
        challengeSubmission.reSubmitCount == 0) {
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          RichText(
            textAlign: TextAlign.center,
            text: TextSpan(text: 'joinChallenge'.tr(), style: textStyleRegular16),
          ),
          RichText(
            textAlign: TextAlign.center,
            text: TextSpan(text: 'uploadFileHere'.tr(), style: textStyleBold18),
          ),
        ],
      );
    } else {
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          RichText(
            textAlign: TextAlign.center,
            text: TextSpan(text: 'alreadySubmitted'.tr(), style: textStyleRegular16),
          ),
          challengeSubmission.reSubmitCount == 3
              ? const SizedBox(height: 0)
              : RichText(
                  textAlign: TextAlign.center,
                  text: TextSpan(text: 'resubmit'.tr(), style: textStyleBold18),
                ),
        ],
      );
    }
  }
  else if (now.isAfter(winnerAnnouncementDate)) {
    return Text('winner'.tr(), style: textStySemiBold16, textAlign: TextAlign.center);
  } else {
    return Text('waitWinner'.tr(), style: textStySemiBold16, textAlign: TextAlign.center);
  }
}
