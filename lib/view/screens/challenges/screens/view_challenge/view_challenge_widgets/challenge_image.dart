import 'package:flutter/material.dart';
import 'package:transparent_image/transparent_image.dart';

import '../../../../../../utils/constants/app_const.dart';

class ChallengeImage extends StatefulWidget {
  final String challengeImg;
  final Color nxtColor;
  final String challengeTitle;
  final String challengeDesc;
  const ChallengeImage(
      {Key? key,
      required this.nxtColor,
      required this.challengeTitle,
      required this.challengeDesc,
      required this.challengeImg})
      : super(key: key);

  @override
  State<ChallengeImage> createState() => _ChallengeImageState();
}

class _ChallengeImageState extends State<ChallengeImage> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: widget.width * 0.03),
      child: Stack(
        alignment: Alignment.bottomCenter,
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(8),
            child: FadeInImage.memoryNetwork(
              placeholder: kTransparentImage,
              image: widget.challengeImg,
              fit: BoxFit.cover,
              width: double.infinity,
              height: widget.height * 0.19,
            ),
          ),
          Positioned.fill(
            child: DecoratedBox(
              decoration: BoxDecoration(
                color: Colors.black.withOpacity(0.5),
                borderRadius: BorderRadius.circular(8),
              ),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Container(
                decoration: BoxDecoration(
                  color: widget.nxtColor,
                  borderRadius: BorderRadius.circular(8),
                ),
                height: 7,
                width: widget.width * 0.25,
              ),
            ],
          ),
          Column(
            children: [
              const SizedBox(height: 10),
              Text(
                widget.challengeTitle,
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.headline4!.copyWith(
                      color: Colors.white,
                      fontFamily: AppConst.isEn ? 'BebasNeue' : 'Tajawal',
                    ),
              ),
              const SizedBox(height: 10),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Text(
                  widget.challengeDesc,
                  textAlign: TextAlign.center,
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                  style: Theme.of(context).textTheme.bodyText1!.copyWith(
                        color: Colors.white,
                        fontWeight: FontWeight.w500,
                        fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal',
                      ),
                ),
              ),
              const SizedBox(height: 40)
            ],
          ),
        ],
      ),
    );
  }
}
