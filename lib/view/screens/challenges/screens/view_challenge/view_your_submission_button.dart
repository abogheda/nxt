import 'package:easy_localization/easy_localization.dart' as local;
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'view_challenge_model.dart';
import '../../../../../config/router/router.dart';

import '../../../../../data/enum/upload_type.dart';
import '../../../../../utils/constants/resources.dart';
import '../../../../../utils/helpers/utils.dart';
import '../../cubit/challenge_cubit.dart';
import '../challenge_video/view_submission_video_player.dart';
import 'view_challenge_widgets/view_submission_audio_player.dart';

class ViewYourSubmissionButton extends StatelessWidget {
  const ViewYourSubmissionButton({
    Key? key,
    required this.challenge,
    required this.cubit,
  }) : super(key: key);

  final ViewChallengeModel challenge;
  final ChallengeCubit cubit;

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      elevation: 0.0,
      shadowColor: Colors.transparent,
      child: TextButton(
        onPressed: () async {
          if (challenge.uploadType == UploadType.video.type) {
            MagicRouter.navigateTo(BlocProvider.value(
                value: cubit,
                child: ViewSubmissionVideoPlayer(
                    // challengeID: challenge.id!,
                    videoUrl: cubit.viewSubmissionModel!.media!)));
            return;
          }
          if (challenge.uploadType == UploadType.audio.type) {
            MagicRouter.navigateTo(
              BlocProvider.value(
                value: cubit,
                child: ViewSubmissionAudioPlayer(
                    url: cubit.viewSubmissionModel!.media!,
                    title: cubit.viewSubmissionModel!.challenge!.category!.title ?? "notAvailable".tr()),
              ),
            );
            return;
          }
          if (challenge.uploadType == UploadType.text.type) {
            await Utils.launchAppUrl(url: cubit.viewSubmissionModel!.media!);
            return;
          }
        },
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              'viewSubmission'.tr(),
              style: Theme.of(context).textTheme.headlineSmall!.copyWith(
                  color: AppColors.blueColor,
                  fontFamily: isEn ? 'BebasNeue' : 'Tajawal',
                  backgroundColor: Colors.transparent,
                  decoration: TextDecoration.underline),
            ),
            Icon(Icons.arrow_forward_ios_rounded,
                color: AppColors.blueColor, textDirection: isEn ? TextDirection.ltr : TextDirection.rtl)
          ],
        ),
      ),
    );
  }
}
