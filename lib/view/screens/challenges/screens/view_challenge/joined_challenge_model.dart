import '../../../../../data/models/base_models/base_model.dart';

class JoinedChallengeModel extends BaseModel {
  JoinedChallengeModel({
    this.id,
    this.challenge,
  });
  @override
  fromJson(Map<String, dynamic> json) => JoinedChallengeModel.fromJson(json);

  JoinedChallengeModel.fromJson(dynamic json) {
    id = json['_id'];
    challenge = json['challenge'] != null ? Challenge.fromJson(json['challenge']) : null;
  }
  String? id;
  Challenge? challenge;
  JoinedChallengeModel copyWith({
    String? id,
    Challenge? challenge,
  }) =>
      JoinedChallengeModel(
        id: id ?? this.id,
        challenge: challenge ?? this.challenge,
      );
  @override
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['_id'] = id;
    if (challenge != null) {
      map['challenge'] = challenge?.toJson();
    }
    return map;
  }
}

class Challenge {
  Challenge({
    this.id,
    this.media,
    this.uploadType,
    this.category,
    this.released,
    this.registerationStartDate,
    this.registerationEndDate,
    this.votingStartDate,
    this.votingEndDate,
    this.winnerAnnouncementDate,
    this.featured,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.title,
    this.supTitle,
    this.description,
    this.requirements,
    this.terms,
  });

  Challenge.fromJson(dynamic json) {
    id = json['_id'];
    media = json['media'];
    uploadType = json['uploadType'];
    category = json['category'];
    released = json['released'];
    registerationStartDate = json['registerationStartDate'];
    registerationEndDate = json['registerationEndDate'];
    votingStartDate = json['votingStartDate'];
    votingEndDate = json['votingEndDate'];
    winnerAnnouncementDate = json['winnerAnnouncementDate'];
    featured = json['featured'];
    removed = json['removed'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    v = json['__v'];
    title = json['title'];
    supTitle = json['supTitle'];
    description = json['description'];
    requirements = json['requirements'];
    terms = json['terms'];
  }
  String? id;
  String? media;
  String? uploadType;
  String? category;
  bool? released;
  String? registerationStartDate;
  String? registerationEndDate;
  String? votingStartDate;
  String? votingEndDate;
  String? winnerAnnouncementDate;
  bool? featured;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  num? v;
  String? title;
  String? supTitle;
  String? description;
  String? requirements;
  String? terms;
  Challenge copyWith({
    String? id,
    String? media,
    String? uploadType,
    String? category,
    bool? released,
    String? registerationStartDate,
    String? registerationEndDate,
    String? votingStartDate,
    String? votingEndDate,
    String? winnerAnnouncementDate,
    bool? featured,
    bool? removed,
    String? createdAt,
    String? updatedAt,
    num? v,
    String? title,
    String? supTitle,
    String? description,
    String? requirements,
    String? terms,
  }) =>
      Challenge(
        id: id ?? this.id,
        media: media ?? this.media,
        uploadType: uploadType ?? this.uploadType,
        category: category ?? this.category,
        released: released ?? this.released,
        registerationStartDate: registerationStartDate ?? this.registerationStartDate,
        registerationEndDate: registerationEndDate ?? this.registerationEndDate,
        votingStartDate: votingStartDate ?? this.votingStartDate,
        votingEndDate: votingEndDate ?? this.votingEndDate,
        winnerAnnouncementDate: winnerAnnouncementDate ?? this.winnerAnnouncementDate,
        featured: featured ?? this.featured,
        removed: removed ?? this.removed,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
        v: v ?? this.v,
        title: title ?? this.title,
        supTitle: supTitle ?? this.supTitle,
        description: description ?? this.description,
        requirements: requirements ?? this.requirements,
        terms: terms ?? this.terms,
      );
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['_id'] = id;
    map['media'] = media;
    map['uploadType'] = uploadType;
    map['category'] = category;
    map['released'] = released;
    map['registerationStartDate'] = registerationStartDate;
    map['registerationEndDate'] = registerationEndDate;
    map['votingStartDate'] = votingStartDate;
    map['votingEndDate'] = votingEndDate;
    map['winnerAnnouncementDate'] = winnerAnnouncementDate;
    map['featured'] = featured;
    map['removed'] = removed;
    map['createdAt'] = createdAt;
    map['updatedAt'] = updatedAt;
    map['__v'] = v;
    map['title'] = title;
    map['supTitle'] = supTitle;
    map['description'] = description;
    map['requirements'] = requirements;
    map['terms'] = terms;
    return map;
  }
}
