import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../../../data/enum/upload_type.dart';
import '../../../../../config/router/router.dart';
import '../../../../../utils/constants/app_colors.dart';
import '../../../../../utils/constants/app_const.dart';
import '../../../../../utils/constants/app_text_style.dart';
import '../../../../widgets/error_state_widget.dart';
import '../view_challenge/view_challenge_model.dart';
import 'user_challenge_page_view_builder.dart';
import '../votes_thumbnail.dart';
import '../../../navigation_and_appbar/import_widget.dart';
import '../../../../widgets/loading_state_widget.dart';
import '../../../../widgets/no_state_widget.dart';
import '../../cubit/challenge_cubit.dart';

class ChallengeVote extends StatefulWidget {
  final ViewChallengeModel challenge;
  final bool showLike;
  final bool showVote;
  const ChallengeVote({
    Key? key,
    required this.challenge,
    this.showLike = true,
    this.showVote = true,
  }) : super(key: key);

  @override
  State<ChallengeVote> createState() => _ChallengeVoteState();
}

class _ChallengeVoteState extends State<ChallengeVote> {
  @override
  Widget build(BuildContext context) {
    return CustomScaffoldWidget(
      backgroundColor: Colors.white,
      extendBodyBehindAppBar: false,
      isBackArrow: true,
      isInProfile: false,
      isLogoClickable: true,
      isThereActions: true,
      isThereLogo: true,
      body: Column(
        children: [
          const Divider(color: Colors.black, thickness: 0.9),
          const SizedBox(height: 20),
          Center(
            child: Text(
              widget.challenge.category!.title ?? 'noTitle'.tr(),
              style: AppTextStyles.huge44.copyWith(fontFamily: AppConst.isEn ? 'BebasNeue' : 'Tajawal'),
            ),
          ),
          Text("#${widget.challenge.description!}",
              textAlign: TextAlign.center, style: TextStyle(fontSize: 18, color: Theme.of(context).primaryColor)),
          const SizedBox(height: 20),
          Container(width: double.infinity, height: 8, color: Color(widget.challenge.category!.color!.toHex())),
          BlocBuilder<ChallengeCubit, ChallengeState>(
            builder: (context, state) {
              final cubit = ChallengeCubit.get(context);
              final votesList = cubit.allVoteUserChallenge;
              if (state is ChallengeVotesFailed) {
                return ErrorStateWidget(
                  hasRefresh: true,
                  onRefresh: () async {
                    await cubit.getVotesForChallenge(widget.challenge.id!);
                  },
                );
              } else if (state is ChallengeVotesLoading || votesList == null) {
                return const Center(child: LoadingStateWidget());
              } else {
                if (votesList.isEmpty) {
                  return Center(child: NoStateWidget());
                }
                return Expanded(
                  child: GridView.builder(
                    shrinkWrap: true,
                    itemCount: votesList.length,
                    gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
                      mainAxisSpacing: 5,
                      childAspectRatio: 1,
                      crossAxisSpacing: 5,
                      maxCrossAxisExtent: 160,
                    ),
                    itemBuilder: (context, index) {
                      return Container(
                        decoration: const BoxDecoration(color: AppColors.iconsColor),
                        child: InkWell(
                          onTap: () => MagicRouter.navigateTo(
                            BlocProvider.value(
                              value: cubit,
                              child: UserChallengePageViewBuilder(
                                userChallengeIndex: index,
                                itemCount: votesList.length,
                                votesList: votesList,
                                isVideo: widget.challenge.uploadType == UploadType.video.type,
                                showLike: widget.showLike,
                                showVote: widget.showVote,
                                challengeId: widget.challenge.id!,
                              ),
                            ),
                          ),
                          child: Stack(
                            fit: StackFit.expand,
                            children: [
                              widget.challenge.uploadType == UploadType.video.type
                                  ? VotesThumbnail(voteThumbnail: votesList[index].media!.toString())
                                  : Container(
                                      decoration: const BoxDecoration(color: AppColors.iconsColor),
                                      child: const Padding(
                                          padding: EdgeInsets.all(8.0), child: Icon(Icons.music_note, size: 48))),
                              Align(
                                alignment: Alignment.bottomLeft,
                                child: Container(
                                  padding: const EdgeInsets.symmetric(horizontal: 6.0),
                                  margin: const EdgeInsets.symmetric(horizontal: 2.0, vertical: 4),
                                  decoration:
                                      BoxDecoration(color: Colors.black26, borderRadius: BorderRadius.circular(4)),
                                  child: Row(
                                    mainAxisSize: MainAxisSize.min,
                                    crossAxisAlignment: CrossAxisAlignment.end,
                                    children: [
                                      const Icon(Icons.play_arrow, color: Colors.black),
                                      const SizedBox(width: 5),
                                      Text(votesList[index].votesCount.toString(),
                                          style: AppTextStyles.huge20.apply(color: Colors.black))
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      );
                    },
                  ),
                );
              }
            },
          ),
        ],
      ),
    );
  }
}
