import 'package:flutter/material.dart';

class ReportReasonItem extends StatelessWidget {
  final String reason;
  final Function() onTap;
  const ReportReasonItem({
    Key? key,
    required this.reason,
    required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Padding(
        padding: const EdgeInsets.all(8),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              reason,
              style: const TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.w400,
              ),
            ),
            const Icon(
              Icons.arrow_forward_ios_outlined,
              size: 16,
            ),
          ],
        ),
      ),
    );
  }
}
