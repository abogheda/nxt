import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'report_reason_item.dart';
import 'package:share_plus/share_plus.dart';
import 'package:transparent_image/transparent_image.dart';

import '../../../../../config/router/router.dart';
import '../../../../../data/models/challenge/vote_user_challenge.dart';
import '../../../../../data/service/hive_services.dart';
import '../../../../../utils/constants/resources.dart';
import '../../../../../utils/helpers/popup_helper.dart';
import '../../../../../utils/helpers/utils.dart';
import '../../../community_user_profile/community_user_profile.dart';
import '../../../talent_profile/talent_profile_screen.dart';
import '../../cubit/challenge_cubit.dart';

class UserChallengeDetails extends StatefulWidget {
  const UserChallengeDetails({
    Key? key,
    required this.voteUserChallenge,
    this.showLike = true,
    this.showVote = true,
    required this.challengeId,
  }) : super(key: key);
  final VoteUserChallenge voteUserChallenge;
  final bool showLike;
  final bool showVote;
  final String challengeId;
  @override
  State<UserChallengeDetails> createState() => _UserChallengeDetailsState();
}

class _UserChallengeDetailsState extends State<UserChallengeDetails> {
  @override
  Widget build(BuildContext context) {
    final cubit = ChallengeCubit.get(context);
    return BlocConsumer<ChallengeCubit, ChallengeState>(
      listener: (context, state) {
        if (state is ReportUserChallengeDone) {
          Navigator.pop(context);
          if (state.reportModel.statusCode == 404 || state.reportModel.message != null) {
            PopupHelper.showBasicSnack(msg: 'videoAlreadyReported'.tr());
            return;
          }
          PopupHelper.showBasicSnack(msg: 'videoReported'.tr());
        }
        if (state is ReportUserChallengeFailed) {
          Navigator.pop(context);
          PopupHelper.showBasicSnack(msg: state.msg);
        }
      },
      builder: (context, state) {
        return Positioned(
          bottom: widget.height * 0.05,
          left: widget.isEn ? (widget.width * 0.04) : 0.0,
          right: widget.isEn ? 0.0 : (widget.width * 0.04),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Row(
                children: [
                  GestureDetector(
                    onTap: () => MagicRouter.navigateTo(
                        HiveHelper.getUserInfo!.user!.id == widget.voteUserChallenge.user!.id
                            ? const TalentProfileScreen()
                            : CommunityUserProfile(userId: widget.voteUserChallenge.user!.id!)),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(20.0),
                      child: widget.voteUserChallenge.user!.avatar != null &&
                              widget.voteUserChallenge.user != null &&
                              widget.voteUserChallenge.user!.avatar != ''
                          ? SizedBox(
                              height: 26,
                              width: 26,
                              child: FadeInImage.memoryNetwork(
                                image: widget.voteUserChallenge.user!.avatar!,
                                fit: BoxFit.cover,
                                placeholder: kTransparentImage,
                              ),
                            )
                          : SizedBox(
                              height: 26,
                              width: 26,
                              child: Image.asset('assets/images/profile_icon.png', fit: BoxFit.cover),
                            ),
                    ),
                  ),
                  const SizedBox(width: 8),
                  Container(
                      decoration: BoxDecoration(color: Colors.black12, borderRadius: BorderRadius.circular(12.0)),
                      padding: const EdgeInsets.symmetric(vertical: 6, horizontal: 6),
                      child: Text("@${widget.voteUserChallenge.user!.name ?? "notAvailable".tr()}", style: textStyle)),
                ],
              ),
              Padding(
                padding: const EdgeInsetsDirectional.only(end: 12.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        widget.showVote
                            ? IconButton(
                                icon: (state is AddVoteLoading || state is DeleteVoteLoading)
                                    ? const Padding(
                                        padding: EdgeInsets.all(8.0),
                                        child: CircularProgressIndicator.adaptive(
                                            strokeWidth: 2, backgroundColor: Colors.white))
                                    : Image.asset(
                                        widget.voteUserChallenge.userVoted ?? false
                                            ? 'assets/images/voting_chip_filled.png'
                                            : 'assets/images/voting_chip.png',
                                        color: Colors.white,
                                        height: 24,
                                        width: 24,
                                      ),
                                onPressed: () async {
                                  if (widget.voteUserChallenge.userVoted! == true) {
                                    await ChallengeCubit.get(context)
                                        .deleteVoteForUserChallenge(challengeId: widget.voteUserChallenge.id!);
                                  } else {
                                    await ChallengeCubit.get(context)
                                        .addVoteForUserChallenge(challengeId: widget.voteUserChallenge.id!);
                                  }
                                  setState(() {
                                    if (widget.voteUserChallenge.userVoted! == false) {
                                      widget.voteUserChallenge.votesCount = widget.voteUserChallenge.votesCount! + 1;
                                    }
                                    if (widget.voteUserChallenge.userVoted! == true) {
                                      if (widget.voteUserChallenge.votesCount == 0) {
                                        return;
                                      }
                                      widget.voteUserChallenge.votesCount = widget.voteUserChallenge.votesCount! - 1;
                                    }
                                    widget.voteUserChallenge.userVoted = !widget.voteUserChallenge.userVoted!;
                                  });
                                },
                              )
                            // ElevatedButton(
                            //         onPressed: () async {
                            //           if (widget.voteUserChallenge.userVoted! == true) {
                            //             await cubit.deleteVoteForUserChallenge(challengeId: widget.voteUserChallenge.id!);
                            //           } else {
                            //             await cubit.addVoteForUserChallenge(challengeId: widget.voteUserChallenge.id!);
                            //           }
                            //           setState(() {
                            //             if (widget.voteUserChallenge.userVoted! == false) {
                            //               widget.voteUserChallenge.votesCount = widget.voteUserChallenge.votesCount! + 1;
                            //             }
                            //             if (widget.voteUserChallenge.userVoted! == true) {
                            //               if (widget.voteUserChallenge.votesCount == 0) {
                            //                 return;
                            //               }
                            //               widget.voteUserChallenge.votesCount = widget.voteUserChallenge.votesCount! - 1;
                            //             }
                            //             widget.voteUserChallenge.userVoted = !widget.voteUserChallenge.userVoted!;
                            //           });
                            //         },
                            //         style: ElevatedButton.styleFrom(
                            //           elevation: 0,
                            //           padding: EdgeInsets.zero,
                            //           primary: Colors.black.withOpacity(0.6),
                            //           shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
                            //         ),
                            //         child: Padding(
                            //           padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 6),
                            //           child: Text(
                            //             widget.voteUserChallenge.userVoted!
                            //                 ? 'voted'.tr().toUpperCase()
                            //                 : 'vote'.tr().toUpperCase(),
                            //             style: TextStyle(
                            //               fontSize: 12,
                            //               fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal',
                            //               fontWeight: FontWeight.bold,
                            //             ),
                            //           ),
                            //         ),
                            //       )
                            : const SizedBox(),
                        Text(
                          widget.voteUserChallenge.votesCount.toString(),
                          style: textStyle,
                        ),
                      ],
                    ),
                    widget.showLike
                        ? Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              IconButton(
                                icon: state is LikeOnUserChallengeLoading
                                    ? const Padding(
                                        padding: EdgeInsets.all(8.0),
                                        child: CircularProgressIndicator.adaptive(
                                            strokeWidth: 2, backgroundColor: Colors.white))
                                    : SvgPicture.asset(
                                        widget.voteUserChallenge.userLiked ?? false
                                            ? 'assets/images/liked.svg'
                                            : 'assets/images/like.svg',
                                      ),
                                onPressed: () async {
                                  await ChallengeCubit.get(context)
                                      .addOrRemoveLikeOnUserChallenge(userChallengeId: widget.voteUserChallenge.id!);
                                  setState(() {
                                    if (widget.voteUserChallenge.userLiked! == false) {
                                      widget.voteUserChallenge.likedCount = widget.voteUserChallenge.likedCount! + 1;
                                    }
                                    if (widget.voteUserChallenge.userLiked! == true) {
                                      if (widget.voteUserChallenge.likedCount == 0) {
                                        return;
                                      }
                                      widget.voteUserChallenge.likedCount = widget.voteUserChallenge.likedCount! - 1;
                                    }
                                    widget.voteUserChallenge.userLiked = !widget.voteUserChallenge.userLiked!;
                                  });
                                },
                              ),
                              Text(
                                widget.voteUserChallenge.likedCount.toString(),
                                style: textStyle,
                              ),
                            ],
                          )
                        : const SizedBox(),
                    widget.voteUserChallenge.user!.name! == HiveHelper.getUserInfo!.user!.name!
                        ? const SizedBox()
                        : Column(
                            children: <Widget>[
                              SizedBox(height: widget.height * 0.03),
                              IconButton(
                                  onPressed: () async {
                                    Utils.showAlertDialog(
                                      context: context,
                                      headerText: "",
                                      customActions: [],
                                      yesButtonText: "report".tr(),
                                      noButtonText: "cancel".tr(),
                                      customContent: Column(
                                        mainAxisSize: MainAxisSize.min,
                                        children: [
                                          Text(
                                            "reportVideo".tr(),
                                            style: Theme.of(context).textTheme.headline6!.copyWith(
                                                  fontFamily: AppConst.isEn ? 'BebasNeue' : 'Tajawal',
                                                  color: Colors.black,
                                                ),
                                          ),
                                          const SizedBox(height: 10),
                                          ReportReasonItem(
                                            reason: "nudity".tr(),
                                            onTap: () {
                                              Navigator.pop(context);
                                              Utils.showAlertDialog(
                                                context: context,
                                                headerText: "reportVideo".tr(),
                                                yesButtonText: "report".tr(),
                                                noButtonText: "cancel".tr(),
                                                onPressed: () {
                                                  cubit.reportUserChallenge(
                                                    userChallengeId: widget.voteUserChallenge.id!,
                                                  );
                                                },
                                              );
                                            },
                                          ),
                                          ReportReasonItem(
                                            reason: "violence".tr(),
                                            onTap: () {
                                              Navigator.pop(context);
                                              Utils.showAlertDialog(
                                                context: context,
                                                headerText: "reportVideo".tr(),
                                                yesButtonText: "report".tr(),
                                                noButtonText: "cancel".tr(),
                                                onPressed: () {
                                                  cubit.reportUserChallenge(
                                                    userChallengeId: widget.voteUserChallenge.id!,
                                                  );
                                                },
                                              );
                                            },
                                          ),
                                          ReportReasonItem(
                                            reason: "harassment".tr(),
                                            onTap: () {
                                              Navigator.pop(context);
                                              Utils.showAlertDialog(
                                                context: context,
                                                headerText: "reportVideo".tr(),
                                                yesButtonText: "report".tr(),
                                                noButtonText: "cancel".tr(),
                                                onPressed: () {
                                                  cubit.reportUserChallenge(
                                                    userChallengeId: widget.voteUserChallenge.id!,
                                                  );
                                                },
                                              );
                                            },
                                          ),
                                          ReportReasonItem(
                                            reason: "hateSpeech".tr(),
                                            onTap: () {
                                              Navigator.pop(context);
                                              Utils.showAlertDialog(
                                                context: context,
                                                headerText: "reportVideo".tr(),
                                                yesButtonText: "report".tr(),
                                                noButtonText: "cancel".tr(),
                                                onPressed: () {
                                                  cubit.reportUserChallenge(
                                                    userChallengeId: widget.voteUserChallenge.id!,
                                                  );
                                                },
                                              );
                                            },
                                          ),
                                          ReportReasonItem(
                                            reason: "suicideOrSelfInjury".tr(),
                                            onTap: () {
                                              Navigator.pop(context);
                                              Utils.showAlertDialog(
                                                context: context,
                                                headerText: "reportVideo".tr(),
                                                yesButtonText: "report".tr(),
                                                noButtonText: "cancel".tr(),
                                                onPressed: () {
                                                  cubit.reportUserChallenge(
                                                    userChallengeId: widget.voteUserChallenge.id!,
                                                  );
                                                },
                                              );
                                            },
                                          ),
                                          ReportReasonItem(
                                            reason: "drugs".tr(),
                                            onTap: () {
                                              Navigator.pop(context);
                                              Utils.showAlertDialog(
                                                context: context,
                                                headerText: "reportVideo".tr(),
                                                yesButtonText: "report".tr(),
                                                noButtonText: "cancel".tr(),
                                                onPressed: () {
                                                  cubit.reportUserChallenge(
                                                    userChallengeId: widget.voteUserChallenge.id!,
                                                  );
                                                },
                                              );
                                            },
                                          ),
                                          ReportReasonItem(
                                            reason: "spam".tr(),
                                            onTap: () {
                                              Navigator.pop(context);
                                              Utils.showAlertDialog(
                                                context: context,
                                                headerText: "reportVideo".tr(),
                                                yesButtonText: "report".tr(),
                                                noButtonText: "cancel".tr(),
                                                onPressed: () {
                                                  cubit.reportUserChallenge(
                                                    userChallengeId: widget.voteUserChallenge.id!,
                                                  );
                                                },
                                              );
                                            },
                                          ),
                                          ReportReasonItem(
                                            reason: "terrorism".tr(),
                                            onTap: () {
                                              Navigator.pop(context);
                                              Utils.showAlertDialog(
                                                context: context,
                                                headerText: "reportVideo".tr(),
                                                yesButtonText: "report".tr(),
                                                noButtonText: "cancel".tr(),
                                                onPressed: () {
                                                  cubit.reportUserChallenge(
                                                    userChallengeId: widget.voteUserChallenge.id!,
                                                  );
                                                },
                                              );
                                            },
                                          ),
                                          ReportReasonItem(
                                            reason: "other".tr(),
                                            onTap: () {
                                              Navigator.pop(context);
                                              Utils.showAlertDialog(
                                                context: context,
                                                headerText: "reportVideo".tr(),
                                                yesButtonText: "report".tr(),
                                                noButtonText: "cancel".tr(),
                                                onPressed: () {
                                                  cubit.reportUserChallenge(
                                                    userChallengeId: widget.voteUserChallenge.id!,
                                                  );
                                                },
                                              );
                                            },
                                          ),
                                        ],
                                      ),
                                      onPressed: () {},
                                    );
                                  },
                                  icon: const Icon(
                                    Icons.flag,
                                    color: Colors.white,
                                  )),
                              Text('report'.tr(), style: textStyle),
                            ],
                          ),
                    SizedBox(height: widget.height * 0.03),
                    Column(
                      children: <Widget>[
                        IconButton(
                          icon: state is SaveUserChallengeLoading
                              ? const Padding(
                                  padding: EdgeInsets.all(8.0),
                                  child:
                                      CircularProgressIndicator.adaptive(strokeWidth: 2, backgroundColor: Colors.white))
                              : SvgPicture.asset(
                                  widget.voteUserChallenge.userSaved ?? false
                                      ? 'assets/images/saved.svg'
                                      : 'assets/images/save.svg',
                                  height: 24,
                                  width: 24,
                                ),
                          onPressed: () async {
                            if (widget.voteUserChallenge.userSaved! == false) {
                              await cubit.addUserChallengeToSavedItems(userChallengeId: widget.voteUserChallenge.id);
                            } else {
                              await cubit.deleteUserChallengeFromSavedItems(
                                  userChallengeId: widget.voteUserChallenge.id);
                            }
                            setState(() {
                              if (widget.voteUserChallenge.userSaved! == false) {
                                widget.voteUserChallenge.saveCount = widget.voteUserChallenge.saveCount! + 1;
                              }
                              if (widget.voteUserChallenge.userSaved! == true) {
                                if (widget.voteUserChallenge.saveCount == 0) {
                                  return;
                                }
                                widget.voteUserChallenge.saveCount = widget.voteUserChallenge.saveCount! - 1;
                              }
                              widget.voteUserChallenge.userSaved = !widget.voteUserChallenge.userSaved!;
                            });
                          },
                        ),
                        Text(
                          widget.voteUserChallenge.saveCount.toString(),
                          style: textStyle,
                        ),
                      ],
                    ),
                    SizedBox(height: widget.height * 0.03),
                    Column(
                      children: <Widget>[
                        IconButton(
                          onPressed: () async => await Share.share(AppConst.isProduction
                              ? "${AppConst.productionUrl}/challenge/view-challenge/${widget.challengeId}/${widget.voteUserChallenge.id}?Voter=true"
                              : "${AppConst.developmentUrl}/challenge/view-challenge/${widget.challengeId}/${widget.voteUserChallenge.id}?Voter=true"),
                          icon: SvgPicture.asset(
                            'assets/images/share_arrow.svg',
                            height: 24,
                            width: 24,
                          ),
                        ),
                        Text('share'.tr(), style: textStyle),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}

final textStyle = Theme.of(MagicRouter.currentContext!).textTheme.caption!.copyWith(
      fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal',
      fontWeight: FontWeight.bold,
      color: Colors.white,
    );
