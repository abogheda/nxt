import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../cubit/challenge_cubit.dart';
import 'user_challenge_details.dart';
import '../../../../../data/models/challenge/vote_user_challenge.dart';
import '../../../talent_profile/screens/reels/reel_widgets/challenge_media_player.dart';

class UserChallengePageViewBuilder extends StatefulWidget {
  final int userChallengeIndex;
  final int itemCount;
  final bool isVideo;
  final bool showLike;
  final bool showVote;
  final String challengeId;
  final List<VoteUserChallenge> votesList;

  const UserChallengePageViewBuilder({
    Key? key,
    required this.userChallengeIndex,
    required this.itemCount,
    required this.votesList,
    required this.isVideo,
    this.showLike = true,
    this.showVote = true,
    required this.challengeId,
  }) : super(key: key);

  @override
  State<UserChallengePageViewBuilder> createState() => _UserChallengePageViewBuilderState();
}

class _UserChallengePageViewBuilderState extends State<UserChallengePageViewBuilder> {
  late PageController controller;

  @override
  void initState() {
    super.initState();
    controller = PageController(initialPage: widget.userChallengeIndex);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.black,
        extendBodyBehindAppBar: true,
        appBar: AppBar(
          systemOverlayStyle: const SystemUiOverlayStyle(
              statusBarColor: Colors.black,
              systemStatusBarContrastEnforced: true,
              statusBarIconBrightness: Brightness.dark),
          elevation: 0,
          backgroundColor: Colors.transparent,
        ),
        body: BlocConsumer<ChallengeCubit, ChallengeState>(
          listener: (context, state) {},
          builder: (context, state) {
            return PageView.builder(
              controller: controller,
              scrollDirection: Axis.vertical,
              itemCount: widget.itemCount,
              itemBuilder: (context, index) {
                final voteUserChallenge = widget.votesList[index];
                return Stack(
                  children: [
                    ChallengeMediaPlayer(challengeMedia: voteUserChallenge.media!, isVideo: widget.isVideo),
                    UserChallengeDetails(
                      voteUserChallenge: voteUserChallenge,
                      showLike: widget.showLike,
                      showVote: widget.showVote,
                      challengeId: widget.challengeId,
                    ),
                  ],
                );
              },
            );
          },
        ));
  }
}
