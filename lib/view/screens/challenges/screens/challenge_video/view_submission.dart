import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import '../../../../../data/enum/upload_type.dart';
import '../../../../../utils/constants/app_const.dart';
import '../../../../../utils/helpers/picker_helper.dart';
import '../../../../widgets/error_state_widget.dart';
import '../view_challenge/view_challenge_model.dart';
import '../../../navigation_and_appbar/import_widget.dart';
import '../../cubit/challenge_cubit.dart';
import '../../../../../config/router/router.dart';

import '../../../../../utils/constants/app_colors.dart';
import '../../../../../utils/constants/app_text_style.dart';
import '../../../../widgets/loading_state_widget.dart';
import 'view_submission_video_player.dart';

class ViewSubmission extends StatelessWidget {
  final ViewChallengeModel challenge;

  const ViewSubmission({Key? key, required this.challenge}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomScaffoldWidget(
      isBackArrow: true,
      isThereLogo: true,
      isThereActions: true,
      extendBodyBehindAppBar: true,
      body: Center(
        child: SingleChildScrollView(
          child: BlocBuilder<ChallengeCubit, ChallengeState>(
            builder: (context, state) {
              final cubit = ChallengeCubit.get(context);
              if (state is UploadLoading) {
                return StreamBuilder(
                    initialData: cubit.reelUploadingProgress,
                    stream: cubit.streamController.stream,
                    builder: (context, snapshot) {
                      return Column(
                        children: [
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: width * 0.3),
                            child: CircularPercentIndicator(
                              animation: true,
                              animationDuration: 2500,
                              curve: Curves.easeOut,
                              percent: snapshot.data as double,
                              addAutomaticKeepAlive: false,
                              animateFromLastPercent: true,
                              progressColor: Colors.black,
                              backgroundColor: Colors.black12,
                              radius: width * 0.3,
                              center: Text('${(cubit.reelUploadingProgress * 100).toStringAsFixed(1)}%',
                                  style: Theme.of(context)
                                      .textTheme
                                      .headlineLarge!
                                      .copyWith(fontFamily: isEn ? 'Montserrat' : 'Tajawal', color: Colors.black)),
                            ),
                          ),
                          SizedBox(height: height * 0.03),
                          Text('uploading'.tr(),
                              style: TextStyle(fontFamily: isEn ? 'Montserrat' : 'Tajawal', color: Colors.black))
                        ],
                      );
                    });
              }
              if (state is UploadingFailed) return const ErrorStateWidget(hasRefresh: false);
              return BlocBuilder<ChallengeCubit, ChallengeState>(
                builder: (context, state) {
                  if (state is UserChallengeLoading) return const LoadingStateWidget();
                  if (state is UserChallengeFailed) return const ErrorStateWidget(hasRefresh: false);
                  return Container(
                    padding: EdgeInsets.symmetric(horizontal: width * 0.09, vertical: 30),
                    margin: EdgeInsets.symmetric(horizontal: width * 0.03, vertical: 10),
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.black),
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(8)),
                    child: Column(
                      children: [
                        Container(
                          height: 45,
                          width: 45,
                          decoration: BoxDecoration(color: Colors.black, borderRadius: BorderRadius.circular(80)),
                          child: const Icon(
                            Icons.done,
                            size: 30,
                            color: Colors.white,
                          ),
                        ),
                        const SizedBox(height: 25),
                        Text(
                          'thanksSubmission'.tr().toUpperCase(),
                          textAlign: TextAlign.center,
                          style: AppTextStyles.huge36.copyWith(fontFamily: AppConst.isEn ? 'BebasNeue' : 'Tajawal'),
                        ),
                        const SizedBox(height: 10),
                        Text(
                          'jury'.tr(),
                          textAlign: TextAlign.center,
                          style: AppTextStyles.semiBold13.apply(
                            color: AppColors.iconsColor,
                            fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal',
                          ),
                        ),
                        const SizedBox(height: 15),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 16),
                          child: MaterialButton(
                            height: 42,
                            minWidth: double.infinity,
                            color: Colors.black,
                            shape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(4))),
                            child: Text(
                              'viewYourSubmission'.tr(),
                              style: AppTextStyles.semiBold13
                                  .copyWith(color: Colors.white, fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal'),
                            ),
                            onPressed: () {
                              if (challenge.uploadType == UploadType.video.type) {
                                MagicRouter.navigateAndPopUntilFirstPage(
                                  BlocProvider.value(
                                    value: cubit..getSubmission(challenge.id!),
                                    child: ViewSubmissionVideoPlayer(
                                        // challengeID: challenge.id!,
                                        videoUrl: cubit.challengeStatus!.media!),
                                  ),
                                );
                                return;
                              }
                              if (challenge.uploadType == UploadType.audio.type) {
                                PickerHelper.openFile(path: cubit.userChallengeAudio!.path);
                                MagicRouter.navigateAndPopUntilFirstPage(Navigation());
                                return;
                              }
                              if (challenge.uploadType == UploadType.text.type) {
                                PickerHelper.openFile(path: cubit.userChallengeText!.path);
                                MagicRouter.navigateAndPopUntilFirstPage(Navigation());
                                return;
                              }
                            },
                          ),
                        ),
                        const SizedBox(height: 10),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 16),
                          child: MaterialButton(
                            height: 42,
                            minWidth: double.infinity,
                            shape: RoundedRectangleBorder(
                              side: const BorderSide(color: Colors.black, width: 1),
                              borderRadius: BorderRadius.circular(4),
                            ),
                            onPressed: () {
                              MagicRouter.navigateAndPopAll(Navigation());
                            },
                            child: Text(
                              'home'.toUpperCase(),
                              style: AppTextStyles.semiBold13
                                  .copyWith(color: Colors.black, fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal'),
                            ),
                          ),
                        )
                      ],
                    ),
                  );
                },
              );
            },
          ),
        ),
      ),
    );
  }
}
