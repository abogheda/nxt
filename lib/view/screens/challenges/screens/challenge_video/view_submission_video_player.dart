import 'package:chewie/chewie.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:transparent_image/transparent_image.dart';
import '../../../../../utils/constants/app_const.dart';
import '../../../../widgets/app_loader.dart';
import 'package:video_player/video_player.dart';

class ViewSubmissionVideoPlayer extends StatefulWidget {
  final String videoUrl;
  const ViewSubmissionVideoPlayer({
    Key? key,
    required this.videoUrl,
  }) : super(key: key);

  @override
  State<ViewSubmissionVideoPlayer> createState() => _ViewSubmissionVideoPlayerState();
}

class _ViewSubmissionVideoPlayerState extends State<ViewSubmissionVideoPlayer> {
  late VideoPlayerController videoPlayerController;
  ChewieController? chewieController;
  @override
  void initState() {
    super.initState();
    initPlayer();
  }

  void initPlayer() async {
    videoPlayerController = VideoPlayerController.network(widget.videoUrl);
    await videoPlayerController.initialize();
    chewieController = ChewieController(
      videoPlayerController: videoPlayerController,
      autoPlay: true,
      autoInitialize: true,
      looping: false,
      allowPlaybackSpeedChanging: true,
      allowFullScreen: true,
      allowMuting: true,
      materialProgressColors: ChewieProgressColors(),
      placeholder: const AppLoader(),
      cupertinoProgressColors: ChewieProgressColors(),
      errorBuilder: (context, url) {
        return Stack(
          alignment: Alignment.bottomCenter,
          children: [
            FadeInImage.memoryNetwork(
              image: 'https://static.thenounproject.com/png/504708-200.png',
              placeholder: kTransparentImage,
            ),
            Text(
              'errorOccurred'.tr(),
              style: const TextStyle(color: Colors.white),
            ),
          ],
        );
      },
    );
    setState(() {});
  }

  @override
  void dispose() {
    videoPlayerController.dispose();
    chewieController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Center(
        child: chewieController == null
            ? Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const AppLoader(color: Colors.white),
                    const SizedBox(height: 10),
                    Text(
                      'loadingVideo'.tr(),
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: 18, color: Colors.white, fontFamily: widget.isEn ? 'Montserrat' : 'Tajawal'),
                    )
                  ],
                ),
              )
            : Stack(
                children: [
                  Chewie(controller: chewieController!),
                  Positioned(
                    top: MediaQuery.of(context).padding.top,
                    left: 0,
                    child: RotatedBox(
                      quarterTurns: widget.isEn ? 0 : 2,
                      child: const BackButton(color: Colors.white),
                    ),
                  ),
                ],
              ),
      ),
    );
  }
}
