import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:video_thumbnail/video_thumbnail.dart';

import '../../../../utils/constants/app_colors.dart';
import '../../../widgets/app_loader.dart';

class VotesThumbnail extends StatefulWidget {
  const VotesThumbnail({Key? key, required this.voteThumbnail}) : super(key: key);
  final String voteThumbnail;

  @override
  State<VotesThumbnail> createState() => _VotesThumbnailState();
}

class _VotesThumbnailState extends State<VotesThumbnail> {
  Uint8List? uint8list;
  Future init() async {
    uint8list = await VideoThumbnail.thumbnailData(
      video: widget.voteThumbnail,
      imageFormat: ImageFormat.WEBP,
      quality: 50,
      timeMs: 0,
    );
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    init();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(color: AppColors.iconsColor),
      child: uint8list == null
          ? const Padding(padding: EdgeInsets.all(8.0), child: AppLoader())
          : Image.memory(uint8list!, fit: BoxFit.cover),
    );
  }
}
