import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import '../../../data/service/hive_services.dart';
import '../../../utils/constants/resources.dart';
import 'main_tab_bar_view/challenges_tab_bar_view.dart';
import 'main_tab_bar_view/my_challenges_tab_bar_view.dart';
import 'main_tab_bar_view/organization_my_challenges_tab_bar_view.dart';

class ChallengesScreen extends StatefulWidget {
  const ChallengesScreen({Key? key}) : super(key: key);

  @override
  State<ChallengesScreen> createState() => _ChallengesScreenState();
}

class _ChallengesScreenState extends State<ChallengesScreen> with SingleTickerProviderStateMixin {
  late TabController _tabController;

  List<Tab> tabTitleList = [
    HiveHelper.isRegularUser
        ? Tab(child: Text('challenges'.tr()))
        : Tab(child: Text('allChallenges'.tr().toUpperCase())),
    Tab(child: Text('myChallenge'.tr())),
  ];
  List<Widget> tabViewList = [
    const ChallengesTabBarView(),
    HiveHelper.isRegularUser ? const MyChallengesTabBarView() : const OrganizationMyChallengesTabBarView(),
  ];

  @override
  void initState() {
    _tabController = TabController(length: tabTitleList.length, vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    _tabController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          height: widget.height * 0.06,
          color: Colors.black,
          child: TabBar(
            physics: const NeverScrollableScrollPhysics(),
            controller: _tabController,
            indicator: const BoxDecoration(color: Colors.black),
            labelColor: Colors.white,
            labelStyle:
                Theme.of(context).textTheme.headline6!.copyWith(fontFamily: AppConst.isEn ? 'BebasNeue' : 'Tajawal'),
            unselectedLabelColor: const Color(0xFF989898),
            tabs: tabTitleList,
          ),
        ),
        Expanded(
          child: TabBarView(
            physics: const NeverScrollableScrollPhysics(),
            controller: _tabController,
            children: tabViewList,
          ),
        )
      ],
    );
  }
}
