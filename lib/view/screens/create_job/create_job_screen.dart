import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../config/router/router.dart';
import '../../../utils/constants/resources.dart';
import 'controller/create_job_cubit.dart';
import '../organization_connect/component/create_job_header_text.dart';
import '../organization_connect/component/vertical_drop_down.dart';

import '../../../utils/helpers/popup_helper.dart';
import '../../widgets/custom_button.dart';
import '../../widgets/error_state_widget.dart';
import '../navigation_and_appbar/import_widget.dart';
import '../organization_connect/create_job_tab_bar_view/language_tab_bar_view.dart';
import '../organization_connect/component/add_cover_widget.dart';
import '../organization_connect/component/age_text_fields_row.dart';
import '../organization_connect/component/country_drop_down.dart';
import '../organization_connect/component/gender_drop_down.dart';
import '../organization_connect/component/job_type_drop_down.dart';
import '../organization_connect/component/jobs_dates_text_fields_row.dart';
import '../organization_connect/component/preview_job_screen.dart';

class CreateJobScreen extends StatefulWidget {
  const CreateJobScreen({Key? key}) : super(key: key);

  @override
  State<CreateJobScreen> createState() => _CreateJobScreenState();
}

class _CreateJobScreenState extends State<CreateJobScreen> {
  @override
  Widget build(BuildContext context) {
    return CustomScaffoldWidget(
      extendBodyBehindAppBar: false,
      isBackArrow: true,
      isThereLogo: true,
      isThereActions: true,
      body: BlocProvider(
        create: (context) => CreateJobCubit()..getCountries(),
        child: BlocConsumer<CreateJobCubit, CreateJobState>(
          listener: (context, state) {},
          builder: (context, state) {
            final cubit = CreateJobCubit.get(context);
            if (state is GetCountriesFailed) {
              return ErrorStateWidget(hasRefresh: true, onRefresh: () async => await cubit.getCountries());
            }
            return Stack(
              children: [
                Positioned(
                  top: 0,
                  bottom: 0,
                  right: 0,
                  left: 0,
                  child: ListView(
                    shrinkWrap: true,
                    children: [
                      const AddCoverWidget(),
                      SizedBox(height: widget.height * 0.015),
                      const VerticalDropDown(),
                      const GenderDropDown(),
                      const JobTypeDropDown(),
                      const CountryDropDown(),
                      CreateJobHeaderText(text: 'age'.tr()),
                      const AgeTextFieldsRow(),
                      SizedBox(height: widget.height * 0.01),
                      CreateJobHeaderText(text: 'jobDates'.tr()),
                      const JobsDatesTextFieldsRow(),
                      SizedBox(height: widget.height * 0.9, child: const LanguageTabBarView())
                    ],
                  ),
                ),
                Positioned(
                  right: 0,
                  left: 0,
                  bottom: 0,
                  child: Container(
                    color: Colors.black,
                    child: CustomButton(
                      height: widget.height * 0.08,
                      width: double.infinity,
                      onTap: () {
                        if (cubit.coverImage == null) {
                          PopupHelper.showBasicSnack(msg: 'addCover'.tr());
                          return;
                        }
                        if (cubit.verticalId == null) {
                          PopupHelper.showBasicSnack(msg: 'chooseVerticalError'.tr());
                          return;
                        }
                        if (cubit.gender == null) {
                          PopupHelper.showBasicSnack(msg: 'chooseGenderError'.tr());
                          return;
                        }
                        if (cubit.jobType == null) {
                          PopupHelper.showBasicSnack(msg: 'chooseTypeError'.tr());
                          return;
                        }
                        if (cubit.selectedCountryIdList.isEmpty) {
                          PopupHelper.showBasicSnack(msg: 'chooseCountryError'.tr());
                          return;
                        }
                        if (cubit.ageToController.text.isNotEmpty &&
                            int.tryParse(cubit.ageToController.text).runtimeType != int) {
                          PopupHelper.showBasicSnack(msg: "enterValidAgeTo".tr());
                          return;
                        }
                        if (cubit.ageFromController.text.isNotEmpty &&
                            int.tryParse(cubit.ageFromController.text).runtimeType != int) {
                          PopupHelper.showBasicSnack(msg: "enterValidAgeFrom".tr());
                          return;
                        }
                        if (int.parse(cubit.ageFromController.value.text) >
                            int.parse(cubit.ageToController.value.text)) {
                          PopupHelper.showBasicSnack(msg: 'ageRangeError'.tr());
                          return;
                        }
                        if (cubit.englishJobTitleController.text.isEmpty ||
                            cubit.englishJobSubTitleController.text.isEmpty ||
                            cubit.englishJobDescriptionController.text.isEmpty ||
                            cubit.englishJobAreaController.text.isEmpty) {
                          PopupHelper.showBasicSnack(msg: 'fillEnglish'.tr());
                          return;
                        }
                        if (cubit.arabicJobTitleController.text.isEmpty ||
                            cubit.arabicJobSubTitleController.text.isEmpty ||
                            cubit.arabicJobDescriptionController.text.isEmpty ||
                            cubit.arabicJobAreaController.text.isEmpty) {
                          PopupHelper.showBasicSnack(msg: 'fillArabic'.tr());
                          return;
                        }
                        if (cubit.ageFormKey.currentState!.validate() && cubit.datesFormKey.currentState!.validate()) {
                          MagicRouter.navigateTo(BlocProvider.value(value: cubit, child: const PreviewJobScreen()));
                        }
                      },
                      child: Text(
                        'previewJob'.tr().toUpperCase(),
                        style: TextStyle(
                          fontSize: 18,
                          color: Colors.white,
                          fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal',
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ),
                )
              ],
            );
          },
        ),
      ),
    );
  }
}
