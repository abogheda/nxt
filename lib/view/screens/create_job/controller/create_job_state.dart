part of 'create_job_cubit.dart';

@immutable
abstract class CreateJobState {}

class CreateJobInitial extends CreateJobState {}

class UpdateCountryListState extends CreateJobState {}

class AddToSelectedCountryListState extends CreateJobState {}

class RemoveFromSelectedCountryListState extends CreateJobState {}

class GetCountriesSuccess extends CreateJobState {}

class GetCountriesLoading extends CreateJobState {}

class GetCountriesFailed extends CreateJobState {
  final String msg;
  GetCountriesFailed(this.msg);
}

class CreateJobSuccess extends CreateJobState {}

class CreateJobLoading extends CreateJobState {}

class CreateJobFailed extends CreateJobState {
  final String msg;
  CreateJobFailed(this.msg);
}

class UploadCoverImageSuccess extends CreateJobState {}

class UploadCoverImageLoading extends CreateJobState {}

class UploadCoverImageFailed extends CreateJobState {
  final String msg;
  UploadCoverImageFailed(this.msg);
}
