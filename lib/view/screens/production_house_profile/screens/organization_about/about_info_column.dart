import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import '../../../../../utils/constants/app_const.dart';
import '../../../../../utils/helpers/popup_helper.dart';
import 'package:url_launcher/url_launcher.dart';

class AboutInfoColumn extends StatelessWidget {
  const AboutInfoColumn({
    Key? key,
    required this.title,
    required this.text,
    this.isUnderline = false,
    this.isClickable = false,
    this.showDivider = true,
  }) : super(key: key);
  final String title;
  final String text;
  final bool isUnderline;
  final bool isClickable;
  final bool showDivider;
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Offstage(
          offstage: !showDivider,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox(height: height * 0.01),
              const Divider(color: Color(0xFFA5A5A5), thickness: 1),
            ],
          ),
        ),
        Padding(
            padding: EdgeInsets.only(top: height * 0.01, bottom: height * 0.006),
            child: Text(title,
                style: Theme.of(context).textTheme.titleSmall!.copyWith(
                    color: const Color(0xFFA5A5A5),
                    fontFamily: isEn ? 'Inter-Medium' : 'Tajawal',
                    fontWeight: FontWeight.w400))),
        InkWell(
          onTap: isClickable
              ? () async {
                  String url = '';
                  if (text.contains('https://') || text.contains('http://')) {
                    url = text.toLowerCase();
                  } else {
                    url = 'https://${text.toLowerCase()}';
                  }
                  try {
                    await launchUrl(
                      Uri.parse(url),
                      mode: LaunchMode.externalApplication,
                    );
                  } catch (_) {
                    PopupHelper.showBasicSnack(msg: 'cannotOpenLink'.tr());
                  }
                }
              : null,
          child: Text(text.toUpperCase(),
              style: Theme.of(context).textTheme.titleMedium!.copyWith(
                    fontFamily: isEn ? 'Inter-Medium' : 'Tajawal',
                    fontWeight: FontWeight.bold,
                    decoration: isUnderline ? TextDecoration.underline : null,
                  )),
        ),
      ],
    );
  }
}
