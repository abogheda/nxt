import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nxt/data/models/expert/expert_profile_model.dart';
import '../../../../../data/service/hive_services.dart';
import '../../../../../utils/constants/resources.dart';

import '../../controller/organization_profile_cubit.dart';
import 'about_info_column.dart';

class ExpertProfileAboutTab extends StatelessWidget {
  const ExpertProfileAboutTab({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<OrganizationProfileCubit, OrganizationProfileState>(
      builder: (context, state) {
        final cubit = OrganizationProfileCubit.get(context);
        final expert = cubit.expertProfileModel!.expert ?? ExpertProfileModelExpert();
        return RefreshIndicator(
          onRefresh: () async => await cubit.getExpertProfile(),
          child: ListView(
            children: [
              Card(
                elevation: 2.5,
                margin: EdgeInsets.symmetric(horizontal: width * 0.06, vertical: height * 0.025),
                shape: const RoundedRectangleBorder(side: BorderSide(color: Color(0xFF707070), width: 0.2)),
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: width * 0.065, vertical: height * 0.03),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          const Icon(Icons.arrow_left_sharp),
                          Text(
                            HiveHelper.getUserName == ''
                                ? "notAvailable".tr()
                                : HiveHelper.getUserName ?? "notAvailable".tr(),
                            style: isEn
                                ? Theme.of(context).textTheme.headlineMedium!.copyWith(
                                    decorationThickness: 1,
                                    color: Colors.transparent,
                                    decorationColor: Colors.black,
                                    decoration: TextDecoration.underline,
                                    decorationStyle: TextDecorationStyle.solid,
                                    fontFamily: isEn ? 'BebasNeue' : 'Tajawal',
                                    shadows: const [Shadow(color: Colors.black, offset: Offset(0, -4))],
                                  )
                                : Theme.of(context).textTheme.titleLarge!.copyWith(
                                    decorationThickness: 1,
                                    color: Colors.transparent,
                                    decorationColor: Colors.black,
                                    decoration: TextDecoration.underline,
                                    decorationStyle: TextDecorationStyle.solid,
                                    fontFamily: isEn ? 'BebasNeue' : 'Tajawal',
                                    shadows: const [Shadow(color: Colors.black, offset: Offset(0, -4))],
                                  ),
                          ),
                        ],
                      ),
                      AboutInfoColumn(
                          title: 'scope'.tr(), showDivider: false, text: expert.scope ?? "notAvailable".tr()),
                      AboutInfoColumn(
                        isUnderline: true,
                        isClickable: expert.website == null ? false : true,
                        title: 'website'.tr(),
                        text: expert.website ?? "notAvailable".tr(),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
