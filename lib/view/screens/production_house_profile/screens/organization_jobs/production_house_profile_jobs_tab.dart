import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../../../config/router/router.dart';
import '../../../job_applicants/job_applicants_screen.dart';
import '../../../../../utils/constants/app_const.dart';
import '../../../../../utils/helpers/popup_helper.dart';
import '../../../../widgets/app_loader.dart';
import '../../../../widgets/error_state_widget.dart';
import '../../../../widgets/shimmer_widgets/my_job_card_shimmer.dart';
import '../../../create_job/create_job_screen.dart';
import '../../../my_jobs/widgets/my_job_card.dart';
import '../../../organization_connect/controller/organization_connect_cubit.dart';

class ProductionHouseProfileJobsTab extends StatelessWidget {
  const ProductionHouseProfileJobsTab({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => OrganizationConnectCubit()..getAllJobsForOwner(),
      child: BlocConsumer<OrganizationConnectCubit, OrganizationConnectState>(
        listener: (context, state) {
          if (state is GetAllJobsForOwnerFailed) PopupHelper.showBasicSnack(msg: state.msg, color: Colors.red);
        },
        builder: (context, state) {
          var cubit = OrganizationConnectCubit.get(context);
          final allJobsForOrganizationList = cubit.allJobsForOrganizationList;
          if (allJobsForOrganizationList == null) {
            return const AppLoader();
          } else if (state is GetAllJobsForOwnerFailed) {
            return ErrorStateWidget(
              hasRefresh: true,
              onRefresh: () async => await cubit.getAllJobsForOwner(),
            );
          }
          return RefreshIndicator(
            onRefresh: () async => await cubit.getAllJobsForOwner(),
            child: allJobsForOrganizationList.isNotEmpty
                ? Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: height * 0.01, horizontal: width * 0.03),
                        child: SizedBox(
                          width: width,
                          child: ElevatedButton(
                            onPressed: () => MagicRouter.navigateTo(const CreateJobScreen()),
                            style: ElevatedButton.styleFrom(padding: const EdgeInsets.symmetric(vertical: 12)),
                            child: Text('postNewJob'.tr().toUpperCase(),
                                style: TextStyle(fontFamily: isEn ? 'Montserrat' : 'Tajawal')),
                          ),
                        ),
                      ),
                      Expanded(
                        child: state is GetAllJobsForOwnerLoading
                            ? ListView.builder(
                                itemCount: 3,
                                padding: const EdgeInsets.all(10),
                                itemBuilder: (context, index) => const MyJobCardShimmer(),
                              )
                            : ListView.builder(
                                itemCount: allJobsForOrganizationList.length,
                                padding: const EdgeInsets.all(10),
                                itemBuilder: (context, index) {
                                  final job = allJobsForOrganizationList[index];
                                  String getName() {
                                    String name = '';
                                    if (job.organization != null) {
                                      name = job.organization!.name!;
                                    } else {
                                      name = job.expert!.expert!.name!;
                                    }
                                    return name;
                                  }

                                  String getLogo() {
                                    String logo = '';
                                    if (job.organization != null) {
                                      logo = job.organization!.logo!;
                                    } else {
                                      logo = job.expert!.expert!.logo!;
                                    }
                                    return logo;
                                  }

                                  return MyJobCard(
                                    id: job.id!,
                                    title: job.title,
                                    createdAt: job.createdAt,
                                    color: job.category!.color,
                                    name: getName(),
                                    logo: getLogo(),
                                    onPressed: () {
                                      MagicRouter.navigateTo(BlocProvider.value(
                                          value: cubit..getApplicants(jobId: job.id!),
                                          child: JobApplicantsScreen(job: job)));
                                    },
                                  );
                                },
                              ),
                      ),
                    ],
                  )
                : ListView(
                    children: <Widget>[
                      SizedBox(height: height * 0.3),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: width * 0.09),
                        child: Text('addJob'.tr(),
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: const Color(0xFFCECECE), fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal')),
                      ),
                      SizedBox(height: height * 0.02),
                      Align(
                        child: SizedBox(
                          width: width * 0.85,
                          child: ElevatedButton(
                            onPressed: () => MagicRouter.navigateTo(const CreateJobScreen()),
                            style: ElevatedButton.styleFrom(padding: const EdgeInsets.symmetric(vertical: 12)),
                            child: Text('postNewJob'.tr().toUpperCase(),
                                style: TextStyle(fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal')),
                          ),
                        ),
                      )
                    ],
                  ),
          );
        },
      ),
    );
  }
}
