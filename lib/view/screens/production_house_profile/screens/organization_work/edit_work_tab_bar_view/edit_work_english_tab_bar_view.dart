import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../../../utils/constants/app_const.dart';
import '../../../../../../utils/helpers/validators.dart';
import '../../../../../widgets/custom_text_field.dart';
import '../../../../organization_connect/component/create_job_header_text.dart';
import '../../../controller/organization_profile_cubit.dart';

class EditWorkEnglishTabBarView extends StatelessWidget {
  const EditWorkEnglishTabBarView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<OrganizationProfileCubit, OrganizationProfileState>(
      builder: (context, state) {
        final cubit = OrganizationProfileCubit.get(context);
        return Padding(
          padding: EdgeInsets.symmetric(horizontal: width * 0.03),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(height: height * 0.01),
                  CreateJobHeaderText(text: 'workTagInEnglish'.tr(), horizontalPadding: 0),
                  CustomTextField(
                    hint: 'addWorkTag'.tr(),
                    validator: Validators.generalField,
                    type: TextInputType.text,
                    controller: cubit.editEnglishTagController,
                    onFieldSubmitted: (val) =>
                        FocusScope.of(context).requestFocus(cubit.editEnglishDescriptionFocusNode),
                  ),
                  SizedBox(height: height * 0.01),
                  CreateJobHeaderText(text: 'workDescriptionInEnglish'.tr(), horizontalPadding: 0),
                  CustomTextField(
                    hint: 'addWorkDescription'.tr(),
                    validator: Validators.generalField,
                    type: TextInputType.text,
                    controller: cubit.editEnglishDescriptionController,
                    focusNode: cubit.editEnglishDescriptionFocusNode,
                    onFieldSubmitted: (val) {},
                  ),
                ],
              ),
            ],
          ),
        );
      },
    );
  }
}
