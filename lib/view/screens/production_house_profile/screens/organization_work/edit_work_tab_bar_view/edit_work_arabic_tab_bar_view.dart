import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

import '../../../../../../utils/constants/app_const.dart';
import '../../../../../../utils/helpers/validators.dart';
import '../../../../../widgets/custom_text_field.dart';
import '../../../../organization_connect/component/create_job_header_text.dart';
import '../../../controller/organization_profile_cubit.dart';

class EditWorkArabicTabBarView extends StatelessWidget {
  const EditWorkArabicTabBarView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final cubit = OrganizationProfileCubit.get(context);
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: width * 0.03),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox(height: height * 0.01),
              CreateJobHeaderText(text: 'workTagInArabic'.tr(), horizontalPadding: 0),
              CustomTextField(
                hint: 'addWorkTag'.tr(),
                validator: Validators.generalField,
                type: TextInputType.text,
                controller: cubit.editArabicTagController,
                onFieldSubmitted: (val) {
                  FocusScope.of(context).requestFocus(cubit.editArabicDescriptionFocusNode);
                },
              ),
              SizedBox(height: height * 0.01),
              CreateJobHeaderText(text: 'workDescriptionInArabic'.tr(), horizontalPadding: 0),
              CustomTextField(
                hint: 'addWorkDescription'.tr(),
                validator: Validators.generalField,
                type: TextInputType.text,
                controller: cubit.editArabicDescriptionController,
                focusNode: cubit.editArabicDescriptionFocusNode,
                onFieldSubmitted: (val) {},
              ),
            ],
          ),
        ],
      ),
    );
  }
}
