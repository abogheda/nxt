import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../../../config/router/router.dart';
import '../../../../../utils/constants/resources.dart';
import '../../../../../utils/helpers/popup_helper.dart';
import '../../../../widgets/app_loader.dart';
import '../../../../widgets/error_state_widget.dart';
import '../../../../widgets/loading_state_widget.dart';
import '../../component/create_work_screen.dart';
import '../../component/organization_work_card.dart';
import '../../controller/organization_profile_cubit.dart';

class ProductionHouseProfileWorkTab extends StatelessWidget {
  const ProductionHouseProfileWorkTab({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<OrganizationProfileCubit, OrganizationProfileState>(
      listener: (context, state) {
        if (state is GetAllWorksFailed) PopupHelper.showBasicSnack(msg: state.msg, color: Colors.red);
      },
      builder: (context, state) {
        var cubit = OrganizationProfileCubit.get(context);
        final allWorksList = cubit.allWorksList;
        if (state is GetAllWorksLoading || allWorksList == null) {
          return const LoadingStateWidget();
          //TODO: add shimmer here
          // return ListView.builder(
          //   itemCount: 3,
          //   padding: const EdgeInsets.all(10),
          //   itemBuilder: (context, index) => const MyJobCardShimmer(),
          // );
        } else if (state is GetAllWorksFailed) {
          return ErrorStateWidget(hasRefresh: true, onRefresh: () async => await cubit.getAllWorks());
        }
        return RefreshIndicator(
          onRefresh: () async => await cubit.getAllWorks(),
          child: allWorksList.isNotEmpty
              ? Column(
                  children: [
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: height * 0.01, horizontal: width * 0.03),
                      child: SizedBox(
                        width: width,
                        child: ElevatedButton(
                          onPressed: () =>
                              MagicRouter.navigateTo(BlocProvider.value(value: cubit, child: const CreateWorkScreen())),
                          style: ElevatedButton.styleFrom(padding: const EdgeInsets.symmetric(vertical: 12)),
                          child: Text('addWork'.tr().toUpperCase(),
                              style: TextStyle(fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal')),
                        ),
                      ),
                    ),
                    Expanded(
                      child: state is GetAllWorksLoading
                          ? const AppLoader()
                          : ListView.builder(
                              itemCount: allWorksList.length,
                              padding: const EdgeInsets.all(10),
                              itemBuilder: (context, index) {
                                final work = allWorksList[index];
                                return OrganizationWorkCard(work: work);
                              },
                            ),
                    ),
                  ],
                )
              : ListView(
                  children: <Widget>[
                    SizedBox(height: height * 0.3),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: width * 0.09),
                      child: Text('addWorkNow'.tr(),
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: const Color(0xFFCECECE), fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal')),
                    ),
                    SizedBox(height: height * 0.02),
                    Align(
                      child: SizedBox(
                        width: width * 0.85,
                        child: ElevatedButton(
                          onPressed: () => MagicRouter.navigateTo(BlocProvider.value(
                            value: cubit,
                            child: const CreateWorkScreen(),
                          )),
                          style: ElevatedButton.styleFrom(padding: const EdgeInsets.symmetric(vertical: 12)),
                          child: Text('addWork'.tr().toUpperCase(),
                              style: TextStyle(fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal')),
                        ),
                      ),
                    )
                  ],
                ),
        );
      },
    );
  }
}
