part of 'organization_profile_cubit.dart';

@immutable
abstract class OrganizationProfileState {}

class OrganizationProfileInitial extends OrganizationProfileState {}

class GetOrganizationProfileSuccess extends OrganizationProfileState {}

class GetOrganizationProfileLoading extends OrganizationProfileState {}

class GetOrganizationProfileFailed extends OrganizationProfileState {
  final String msg;
  GetOrganizationProfileFailed(this.msg);
}

class GetExpertProfileSuccess extends OrganizationProfileState {}

class GetExpertProfileLoading extends OrganizationProfileState {}

class GetExpertProfileFailed extends OrganizationProfileState {
  final String msg;
  GetExpertProfileFailed(this.msg);
}

class CreateWorkSuccess extends OrganizationProfileState {}

class CreateWorkLoading extends OrganizationProfileState {}

class CreateWorkFailed extends OrganizationProfileState {
  final String msg;
  CreateWorkFailed(this.msg);
}

class EditWorkSuccess extends OrganizationProfileState {}

class EditWorkLoading extends OrganizationProfileState {}

class EditWorkFailed extends OrganizationProfileState {
  final String msg;
  EditWorkFailed(this.msg);
}

class UploadCoverImageSuccess extends OrganizationProfileState {}

class UploadCoverImageLoading extends OrganizationProfileState {}

class UploadCoverImageFailed extends OrganizationProfileState {
  final String msg;
  UploadCoverImageFailed(this.msg);
}

class GetAllWorksSuccess extends OrganizationProfileState {}

class GetAllWorksLoading extends OrganizationProfileState {}

class GetAllWorksFailed extends OrganizationProfileState {
  final String msg;
  GetAllWorksFailed(this.msg);
}

class DeleteWorkSuccess extends OrganizationProfileState {}

class DeleteWorkLoading extends OrganizationProfileState {}

class DeleteWorkFailed extends OrganizationProfileState {
  final String msg;
  DeleteWorkFailed(this.msg);
}
