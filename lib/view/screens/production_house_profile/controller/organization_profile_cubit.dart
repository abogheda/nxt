// ignore_for_file: unnecessary_import, depend_on_referenced_packages
import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';
import 'package:nxt/data/models/organization/work_model.dart';
import 'package:nxt/data/repos/expert/expert_repo.dart';

import '../../../../data/models/expert/expert_profile_model.dart';
import '../../../../data/models/organization/create_work_model.dart';
import '../../../../data/models/organization/organization_profile_model.dart';
import '../../../../data/repos/general_repo.dart';
import '../../../../data/repos/organization/organization_repo.dart';
import '../../../../data/service/hive_services.dart';
import '../../../../utils/helpers/picker_helper.dart';

part 'organization_profile_state.dart';

class OrganizationProfileCubit extends Cubit<OrganizationProfileState> {
  OrganizationProfileCubit() : super(OrganizationProfileInitial());
  static OrganizationProfileCubit get(context) => BlocProvider.of(context);
  OrganizationProfileModel? organizationProfileModel;
  ExpertProfileModel? expertProfileModel;
  List<WorkModel>? allWorksList;
  File? createCoverImage, editCoverImage;
  String? createCoverImageUrl, editCoverImageUrl;
  TextEditingController createEnglishTagController = TextEditingController(),
      createEnglishDescriptionController = TextEditingController(),
      createArabicTagController = TextEditingController(),
      createArabicDescriptionController = TextEditingController();
  FocusNode createEnglishDescriptionFocusNode = FocusNode(), createArabicDescriptionFocusNode = FocusNode();
  TextEditingController editEnglishTagController = TextEditingController(),
      editEnglishDescriptionController = TextEditingController(),
      editArabicTagController = TextEditingController(),
      editArabicDescriptionController = TextEditingController();
  FocusNode editEnglishDescriptionFocusNode = FocusNode(), editArabicDescriptionFocusNode = FocusNode();

  @override
  Future<void> close() {
    createEnglishTagController.dispose();
    createEnglishDescriptionController.dispose();
    createArabicTagController.dispose();
    createArabicDescriptionController.dispose();
    createEnglishDescriptionFocusNode.dispose();
    createArabicDescriptionFocusNode.dispose();
    editEnglishTagController.dispose();
    editEnglishDescriptionController.dispose();
    editArabicTagController.dispose();
    editArabicDescriptionController.dispose();
    editEnglishDescriptionFocusNode.dispose();
    editArabicDescriptionFocusNode.dispose();
    return super.close();
  }

  void resetAllCreateFields() {
    createCoverImageUrl = null;
    createCoverImage = null;
    createEnglishTagController.clear();
    createEnglishDescriptionController.clear();
    createArabicTagController.clear();
    createArabicDescriptionController.clear();
  }

  void setEditFields({required WorkModel? work}) {
    editCoverImageUrl = null;
    editCoverImage = null;
    if (work != null) {
      editEnglishTagController = TextEditingController(text: work.lang![0]!.tag!);
      editEnglishDescriptionController = TextEditingController(text: work.lang![0]!.description!);
      editArabicTagController = TextEditingController(text: work.lang![1]!.tag!);
      editArabicDescriptionController = TextEditingController(text: work.lang![1]!.description!);
    }
  }

  Future<void> onInit() async {
    if (HiveHelper.getUserInfo!.user!.role == 'Organization') {
      await getOrganizationProfile();
    } else if (HiveHelper.getUserInfo!.user!.role == 'Expert') {
      await getExpertProfile();
    }
  }

  Future<void> getOrganizationProfile() async {
    emit(GetOrganizationProfileLoading());
    final res = await OrganizationRepo.getOrganizationDataValidate();
    if (res.message != null) {
      emit(GetOrganizationProfileFailed(res.message ?? ""));
      return;
    }
    if (res.data != null) {
      organizationProfileModel = res.data;
      await HiveHelper.cacheProfileInfo(
        name: organizationProfileModel!.organization!.name,
        avatar: organizationProfileModel!.organization!.logo,
        birthDate: organizationProfileModel!.updatedAt,
      );
      emit(GetOrganizationProfileSuccess());
    } else {
      emit(GetOrganizationProfileFailed(res.message ?? ""));
    }
  }

  Future<void> getExpertProfile() async {
    emit(GetExpertProfileLoading());
    final res = await ExpertRepo.getExpertDataValidate();
    if (res.message != null) {
      emit(GetExpertProfileFailed(res.message ?? ""));
      return;
    }
    if (res.data != null) {
      expertProfileModel = res.data;
      if (expertProfileModel!.expert != null) {
        await HiveHelper.cacheProfileInfo(
          name: expertProfileModel!.expert!.name,
          avatar: expertProfileModel!.expert!.logo,
          birthDate: expertProfileModel!.updatedAt,
        );
      }
      emit(GetExpertProfileSuccess());
    } else {
      emit(GetExpertProfileFailed(res.message ?? ""));
    }
  }

  Future<void> getAllWorks() async {
    try {
      emit(GetAllWorksLoading());
      final res = await OrganizationRepo.getAllWorks();
      if (res.data != null) {
        allWorksList = res.data;
        emit(GetAllWorksSuccess());
      } else {
        emit(GetAllWorksFailed(res.message ?? ""));
      }
    } catch (e) {
      debugPrint(e.toString());
      emit(GetAllWorksFailed('normalErrorMsg'.tr()));
    }
  }

  Future<void> deleteWork({required String id}) async {
    emit(DeleteWorkLoading());
    final res = await OrganizationRepo.deleteWork(id: id);
    if (res.data != null) {
      emit(DeleteWorkSuccess());
    } else {
      emit(DeleteWorkFailed(res.message ?? ""));
    }
  }

  Future pickCoverCreateImage() async {
    createCoverImage = await PickerHelper.pickGalleryImage();
    emit(UploadCoverImageLoading());
  }

  Future pickCoverEditImage() async {
    editCoverImage = await PickerHelper.pickGalleryImage();
    emit(UploadCoverImageLoading());
  }

  Future<void> createNewWork() async {
    emit(CreateWorkLoading());
    if (createCoverImage != null) {
      final response = await GeneralRepo.uploadMedia([createCoverImage!]);
      response.fold(
        (url) async {
          createCoverImageUrl = url.first;
          emit(UploadCoverImageSuccess());
        },
        (errorMsg) => emit(UploadCoverImageFailed(errorMsg)),
      );
    }
    final res = await OrganizationRepo.createNewWork(
      createWorkModel: CreateWorkModel(
        lang: [
          Lang(
            tag: createEnglishTagController.text.trim(),
            description: createEnglishDescriptionController.text.trim(),
          ),
          Lang(
            tag: createArabicTagController.text.trim(),
            description: createArabicDescriptionController.text.trim(),
          )
        ],
        poster: createCoverImageUrl,
      ),
    );
    if (res.data != null) {
      emit(CreateWorkSuccess());
    } else {
      emit(CreateWorkFailed(res.message ?? ""));
    }
  }

  Future<void> editWork({required String workId, required String poster}) async {
    editCoverImageUrl = poster;
    emit(EditWorkLoading());
    if (editCoverImage != null) {
      final response = await GeneralRepo.uploadMedia([editCoverImage!]);
      response.fold(
        (url) async {
          editCoverImageUrl = url.first;
          emit(UploadCoverImageSuccess());
        },
        (errorMsg) => emit(UploadCoverImageFailed(errorMsg)),
      );
    }
    final res = await OrganizationRepo.editNewWork(
      workId: workId,
      createWorkModel: CreateWorkModel(
        lang: [
          Lang(
            tag: editEnglishTagController.text.trim(),
            description: editEnglishDescriptionController.text.trim(),
          ),
          Lang(
            tag: editArabicTagController.text.trim(),
            description: editArabicDescriptionController.text.trim(),
          )
        ],
        poster: editCoverImageUrl,
      ),
    );
    if (res.data != null) {
      emit(EditWorkSuccess());
    } else {
      emit(EditWorkFailed(res.message ?? ""));
    }
  }
}
