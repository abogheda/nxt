import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nxt/view/screens/production_house_profile/screens/organization_work/edit_work_tab_bar_view/edit_work_language_tab_bar_view.dart';
import '../../../../config/router/router.dart';
import '../../../../utils/constants/resources.dart';
import '../../../widgets/app_loader.dart';
import '../../../../utils/helpers/popup_helper.dart';
import '../../../widgets/custom_button.dart';
import '../../navigation_and_appbar/import_widget.dart';
import '../controller/organization_profile_cubit.dart';
import 'edit_production_house_cover_widget.dart';

class EditWorkScreen extends StatefulWidget {
  const EditWorkScreen({Key? key, required this.poster, required this.workId}) : super(key: key);
  final String poster;
  final String workId;
  @override
  State<EditWorkScreen> createState() => _EditWorkScreenState();
}

class _EditWorkScreenState extends State<EditWorkScreen> {
  @override
  Widget build(BuildContext context) {
    return CustomScaffoldWidget(
      extendBodyBehindAppBar: false,
      isBackArrow: true,
      isThereLogo: true,
      isThereActions: true,
      body: BlocConsumer<OrganizationProfileCubit, OrganizationProfileState>(
        listener: (context, state) {
          if (state is EditWorkFailed) PopupHelper.showBasicSnack(msg: state.msg, color: Colors.red);
        },
        builder: (context, state) {
          final cubit = OrganizationProfileCubit.get(context);
          return Stack(
            children: [
              Positioned(
                top: 0,
                bottom: 0,
                right: 0,
                left: 0,
                child: ListView(
                  shrinkWrap: true,
                  children: [
                    EditProductionHouseCoverWidget(imageUrl: widget.poster),
                    SizedBox(
                      height: widget.height * 0.45,
                      child: const EditWorkLanguageTabBarView(),
                    ),
                  ],
                ),
              ),
              Positioned(
                right: 0,
                left: 0,
                bottom: 0,
                child: Container(
                  color: Colors.black,
                  child: CustomButton(
                    height: widget.height * 0.08,
                    width: double.infinity,
                    onTap: state is EditWorkLoading
                        ? null
                        : () async {
                            if (cubit.editEnglishTagController.text.isEmpty ||
                                cubit.editEnglishDescriptionController.text.isEmpty) {
                              PopupHelper.showBasicSnack(msg: 'fillEnglish'.tr());
                              return;
                            }
                            if (cubit.editArabicTagController.text.isEmpty ||
                                cubit.editArabicDescriptionController.text.isEmpty) {
                              PopupHelper.showBasicSnack(msg: 'fillArabic'.tr());
                              return;
                            }
                            await cubit.editWork(workId: widget.workId, poster: widget.poster);
                            MagicRouter.pop();
                            await cubit.getAllWorks();
                          },
                    child: state is EditWorkLoading
                        ? const AppLoader(color: Colors.white)
                        : Text(
                            'updateWork'.tr().toUpperCase(),
                            style: TextStyle(
                              fontSize: 18,
                              color: Colors.white,
                              fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal',
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                  ),
                ),
              )
            ],
          );
        },
      ),
    );
  }
}
