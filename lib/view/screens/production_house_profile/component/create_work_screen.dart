import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../../config/router/router.dart';
import '../../../../utils/constants/resources.dart';
import '../../../widgets/app_loader.dart';
import '../../../../utils/helpers/popup_helper.dart';
import '../../../widgets/custom_button.dart';
import '../../navigation_and_appbar/import_widget.dart';
import '../controller/organization_profile_cubit.dart';
import '../screens/organization_work/create_work_tab_bar_view/create_work_language_tab_bar_view.dart';
import 'add_production_house_cover_widget.dart';

class CreateWorkScreen extends StatefulWidget {
  const CreateWorkScreen({Key? key}) : super(key: key);

  @override
  State<CreateWorkScreen> createState() => _CreateWorkScreenState();
}

class _CreateWorkScreenState extends State<CreateWorkScreen> {
  @override
  void didChangeDependencies() {
    OrganizationProfileCubit.get(context).resetAllCreateFields();
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return CustomScaffoldWidget(
      extendBodyBehindAppBar: false,
      isBackArrow: true,
      isThereLogo: true,
      isThereActions: true,
      body: BlocConsumer<OrganizationProfileCubit, OrganizationProfileState>(
        listener: (context, state) {
          if (state is CreateWorkFailed) PopupHelper.showBasicSnack(msg: state.msg, color: Colors.red);
        },
        builder: (context, state) {
          final cubit = OrganizationProfileCubit.get(context);
          return Stack(
            children: [
              Positioned(
                top: 0,
                bottom: 0,
                right: 0,
                left: 0,
                child: ListView(
                  shrinkWrap: true,
                  children: [
                    const AddProductionHouseCoverWidget(),
                    SizedBox(
                      height: widget.height * 0.45,
                      child: const CreateWorkLanguageTabBarView(),
                    ),
                  ],
                ),
              ),
              Positioned(
                right: 0,
                left: 0,
                bottom: 0,
                child: Container(
                  color: Colors.black,
                  child: CustomButton(
                    height: widget.height * 0.08,
                    width: double.infinity,
                    onTap: state is CreateWorkLoading
                        ? null
                        : () async {
                            if (cubit.createCoverImage == null) {
                              PopupHelper.showBasicSnack(msg: 'addCover'.tr());
                              return;
                            }
                            if (cubit.createEnglishTagController.text.isEmpty ||
                                cubit.createEnglishDescriptionController.text.isEmpty) {
                              PopupHelper.showBasicSnack(msg: 'fillEnglish'.tr());
                              return;
                            }
                            if (cubit.createArabicTagController.text.isEmpty ||
                                cubit.createArabicDescriptionController.text.isEmpty) {
                              PopupHelper.showBasicSnack(msg: 'fillArabic'.tr());
                              return;
                            }
                            await cubit.createNewWork();
                            MagicRouter.pop();
                            await cubit.getAllWorks();
                          },
                    child: state is CreateWorkLoading
                        ? const AppLoader(color: Colors.white)
                        : Text(
                            'createWork'.tr().toUpperCase(),
                            style: TextStyle(
                              fontSize: 18,
                              color: Colors.white,
                              fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal',
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                  ),
                ),
              )
            ],
          );
        },
      ),
    );
  }
}
