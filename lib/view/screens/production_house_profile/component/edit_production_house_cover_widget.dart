import 'package:dotted_border/dotted_border.dart';
import 'package:easy_image_viewer/easy_image_viewer.dart';
import 'package:flutter/material.dart';
import '../../../../utils/constants/resources.dart';

import '../controller/organization_profile_cubit.dart';

class EditProductionHouseCoverWidget extends StatefulWidget {
  const EditProductionHouseCoverWidget({Key? key, required this.imageUrl}) : super(key: key);
  final String imageUrl;
  @override
  State<EditProductionHouseCoverWidget> createState() => _EditProductionHouseCoverWidgetState();
}

class _EditProductionHouseCoverWidgetState extends State<EditProductionHouseCoverWidget> {
  @override
  Widget build(BuildContext context) {
    final cubit = OrganizationProfileCubit.get(context);
    final isImageSelected = cubit.editCoverImage != null;
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: widget.width * 0.03),
      child: DottedBorder(
        borderType: BorderType.RRect,
        radius: const Radius.circular(6),
        dashPattern: const [15, 10],
        child: ClipRRect(
          borderRadius: const BorderRadius.all(Radius.circular(6.0)),
          child: Center(
            child: ClipRRect(
              borderRadius: BorderRadius.circular(6.0),
              child: SizedBox(
                height: widget.height * 0.2,
                child: Stack(
                  children: <Widget>[
                    GestureDetector(
                      onTap: isImageSelected
                          ? () async => await showImageViewer(
                                context,
                                FileImage(cubit.editCoverImage!),
                                swipeDismissible: true,
                                doubleTapZoomable: true,
                              )
                          : () async => await showImageViewer(
                                context,
                                NetworkImage(widget.imageUrl),
                                swipeDismissible: true,
                                doubleTapZoomable: true,
                              ),
                      child: isImageSelected
                          ? Image.file(cubit.editCoverImage!, fit: BoxFit.cover, width: widget.width)
                          : Image.network(widget.imageUrl, fit: BoxFit.cover, width: widget.width),
                    ),
                    Positioned(
                      left: 0,
                      right: 0,
                      bottom: 0,
                      top: 0,
                      child: Align(
                        child: CircleAvatar(
                          backgroundColor: Colors.black38,
                          radius: 32.0,
                          child: isImageSelected
                              ? InkWell(
                                  onTap: () => setState(() => cubit.editCoverImage = null),
                                  child: const Icon(Icons.delete, color: Colors.red, size: 36),
                                )
                              : InkWell(
                                  onTap: () async => await cubit.pickCoverEditImage(),
                                  child: const Icon(Icons.change_circle_outlined, size: 36, color: Colors.white),
                                ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
