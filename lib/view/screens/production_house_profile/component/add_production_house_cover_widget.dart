import 'package:dotted_border/dotted_border.dart';
import 'package:easy_image_viewer/easy_image_viewer.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import '../../../../utils/constants/resources.dart';

import '../controller/organization_profile_cubit.dart';

class AddProductionHouseCoverWidget extends StatefulWidget {
  const AddProductionHouseCoverWidget({Key? key}) : super(key: key);

  @override
  State<AddProductionHouseCoverWidget> createState() => _AddProductionHouseCoverWidgetState();
}

class _AddProductionHouseCoverWidgetState extends State<AddProductionHouseCoverWidget> {
  @override
  Widget build(BuildContext context) {
    final cubit = OrganizationProfileCubit.get(context);
    final isImageSelected = cubit.createCoverImage == null;
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: widget.width * 0.03),
      child: InkWell(
        onTap: () async {
          FocusManager.instance.primaryFocus?.unfocus();
          await cubit.pickCoverCreateImage();
          setState(() {});
        },
        borderRadius: BorderRadius.circular(6),
        splashColor: Colors.black26,
        child: DottedBorder(
          borderType: BorderType.RRect,
          radius: const Radius.circular(6),
          dashPattern: const [15, 10],
          child: ClipRRect(
            borderRadius: const BorderRadius.all(Radius.circular(6.0)),
            child: Center(
              child: isImageSelected
                  ? Column(
                      children: [
                        Padding(
                            padding: EdgeInsets.symmetric(vertical: widget.height * 0.02),
                            child: const Icon(Icons.add_rounded)),
                        Padding(
                            padding: EdgeInsets.only(bottom: widget.height * 0.015),
                            child: Text('addCoverImage'.tr(),
                                style: Theme.of(context).textTheme.subtitle1!.copyWith(
                                    fontFamily: widget.isEn ? 'Montserrat' : 'Tajawal',
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black))),
                        Padding(
                            padding: EdgeInsets.only(bottom: widget.height * 0.035),
                            child: Text('png'.tr(),
                                style: Theme.of(context).textTheme.subtitle2!.copyWith(
                                    fontFamily: widget.isEn ? 'Montserrat' : 'Tajawal',
                                    color: const Color(0xFFA5A5A5)))),
                      ],
                    )
                  : ClipRRect(
                      borderRadius: BorderRadius.circular(6.0),
                      child: SizedBox(
                        height: widget.height * 0.2,
                        child: Stack(
                          children: <Widget>[
                            GestureDetector(
                              onTap: () async => await showImageViewer(context, FileImage(cubit.createCoverImage!),
                                  swipeDismissible: true, doubleTapZoomable: true),
                              child: Image.file(cubit.createCoverImage!, fit: BoxFit.cover, width: widget.width),
                            ),
                            Positioned(
                              left: 0,
                              right: 0,
                              bottom: 0,
                              top: 0,
                              child: Align(
                                child: CircleAvatar(
                                  backgroundColor: Colors.black38,
                                  radius: 32.0,
                                  child: InkWell(
                                    onTap: () => setState(() => cubit.createCoverImage = null),
                                    child: const Icon(Icons.delete, color: Colors.red, size: 36),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
            ),
          ),
        ),
      ),
    );
  }
}
