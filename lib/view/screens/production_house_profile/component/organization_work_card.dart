import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../../config/router/router.dart';
import 'package:transparent_image/transparent_image.dart';
import '../../../../../utils/constants/resources.dart';
import '../../../../data/models/organization/work_model.dart';
import '../../../../utils/helpers/utils.dart';
import '../controller/organization_profile_cubit.dart';
import 'edit_work_screen.dart';

enum MenuItems {
  edit,
  delete;
}

class OrganizationWorkCard extends StatelessWidget {
  const OrganizationWorkCard({Key? key, required this.work}) : super(key: key);
  final WorkModel work;

  deleteWork(BuildContext context) {
    Utils.showAlertDialog(
      context: context,
      headerText: 'deleteWork'.tr(),
      yesButtonText: 'delete'.tr(),
      onPressed: () async {
        final cubit = OrganizationProfileCubit.get(context);
        await cubit.deleteWork(id: work.id!);
        MagicRouter.pop();
        await cubit.getAllWorks();
      },
    );
  }

  String getTag() {
    String tag = "notAvailable".tr();
    if (isEn) {
      tag = work.lang![0]!.tag == null ? "notAvailable".tr() : "#${work.lang![0]!.tag!.toUpperCase()}";
    } else {
      tag = work.lang![1]!.tag == null ? "notAvailable".tr() : "#${work.lang![1]!.tag!.toUpperCase()}";
    }
    return tag;
  }

  String getDescription() {
    String description = "notAvailable".tr();
    if (isEn) {
      description =
          work.lang![0]!.description == null ? "notAvailable".tr() : '${work.lang![0]!.description!} #POWEREDBYNXT';
    } else {
      description =
          work.lang![1]!.description == null ? "notAvailable".tr() : '${work.lang![1]!.description!} #POWEREDBYNXT';
    }
    return description;
  }

  @override
  Widget build(BuildContext context) {
    void editWork(BuildContext context) async {
      MagicRouter.navigateTo(BlocProvider.value(
        value: OrganizationProfileCubit.get(context)..setEditFields(work: work),
        child: EditWorkScreen(poster: work.poster ?? placeHolderUrl, workId: work.id!),
      ));
    }

    return Card(
      elevation: 2.5,
      margin: EdgeInsets.symmetric(horizontal: width * 0.03, vertical: height * 0.01),
      shape: const RoundedRectangleBorder(side: BorderSide(color: Color(0xFF707070), width: 0.2)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Stack(
            children: [
              Container(
                width: width,
                height: height * 0.3,
                color: Colors.black26,
                child: FadeInImage.memoryNetwork(
                  fit: BoxFit.cover,
                  placeholder: kTransparentImage,
                  image: work.poster ?? placeHolderUrl,
                ),
              ),
              Positioned(
                bottom: height * 0.01,
                left: isEn ? height * 0.01 : 0,
                right: isEn ? 0 : height * 0.01,
                child: Align(
                  alignment: isEn ? Alignment.bottomLeft : Alignment.bottomRight,
                  child: Container(
                    padding: const EdgeInsets.all(4),
                    decoration: BoxDecoration(color: Colors.black26, borderRadius: BorderRadius.circular(6)),
                    child: Text(
                      getTag(),
                      style: Theme.of(context)
                          .textTheme
                          .titleLarge!
                          .copyWith(color: Colors.white, fontFamily: AppConst.isEn ? 'BebasNeue' : 'Tajawal'),
                    ),
                  ),
                ),
              ),
              Positioned(
                top: 0,
                right: 0,
                child: PopupMenuButton<MenuItems>(
                  onSelected: (value) {
                    switch (value) {
                      case MenuItems.edit:
                        editWork(context);
                        break;
                      case MenuItems.delete:
                        deleteWork(context);
                        break;
                      default:
                    }
                  },
                  itemBuilder: (BuildContext context) {
                    return [
                      PopupMenuItem(
                        value: MenuItems.edit,
                        child: Text('edit'.tr(), style: TextStyle(fontFamily: isEn ? 'BebasNeue' : 'Tajawal')),
                      ),
                      PopupMenuItem(
                        value: MenuItems.delete,
                        child: Text('delete'.tr(), style: TextStyle(fontFamily: isEn ? 'BebasNeue' : 'Tajawal')),
                      ),
                    ];
                  },
                ),
              ),
            ],
          ),
          Padding(
            padding: EdgeInsets.all(width * 0.02),
            child: Text(
              getDescription(),
              style: Theme.of(context).textTheme.bodyText2!.copyWith(
                    fontFamily: isEn ? 'Inter-Medium' : 'Tajawal',
                  ),
            ),
          ),
        ],
      ),
    );
  }
}
