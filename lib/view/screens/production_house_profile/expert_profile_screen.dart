import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'screens/organization_about/expert_profile_about_tab.dart';
import 'widgets/production_house_profile_header.dart';
import '../../../utils/constants/resources.dart';
import '../../widgets/error_state_widget.dart';
import '../../widgets/loading_state_widget.dart';
import '../navigation_and_appbar/import_widget.dart';
import 'controller/organization_profile_cubit.dart';
import 'screens/organization_jobs/production_house_profile_jobs_tab.dart';
import 'screens/organization_work/production_house_profile_work_tab.dart';

class ExpertProfileScreen extends StatefulWidget {
  const ExpertProfileScreen({Key? key}) : super(key: key);

  @override
  State<ExpertProfileScreen> createState() => _ExpertProfileScreenState();
}

class _ExpertProfileScreenState extends State<ExpertProfileScreen> with SingleTickerProviderStateMixin {
  late TabController _tabController;

  List<Tab> tabTitleList = [
    Tab(child: Text("about".tr())),
    Tab(child: Text("work".tr())),
    Tab(child: Text("jobs".tr().toUpperCase())),
  ];

  @override
  void initState() {
    _tabController = TabController(length: tabTitleList.length, vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    _tabController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<OrganizationProfileCubit>(
      create: (_) => OrganizationProfileCubit()
        ..onInit()
        ..getAllWorks(),
      child: BlocBuilder<OrganizationProfileCubit, OrganizationProfileState>(
        builder: (context, state) {
          final cubit = OrganizationProfileCubit.get(context);
          return CustomScaffoldWidget(
            isInProfile: true,
            extendBodyBehindAppBar: false,
            isBackArrow: true,
            isThereLogo: true,
            isThereActions: true,
            backgroundColor: Colors.white,
            body: (state is GetExpertProfileLoading || cubit.expertProfileModel == null)
                ? const LoadingStateWidget()
                : (state is GetOrganizationProfileFailed || state is GetExpertProfileFailed)
                    ? ErrorStateWidget(hasRefresh: true, onRefresh: () async => await cubit.onInit())
                    : Column(
                        children: [
                          const ProductionHouseProfileHeader(),
                          Expanded(
                            child: Column(
                              children: [
                                Container(
                                  color: Colors.black,
                                  child: TabBar(
                                    physics: const NeverScrollableScrollPhysics(),
                                    controller: _tabController,
                                    indicator: const BoxDecoration(color: Colors.black),
                                    labelColor: Colors.white,
                                    labelStyle: Theme.of(context)
                                        .textTheme
                                        .headline6!
                                        .copyWith(fontFamily: AppConst.isEn ? 'BebasNeue' : 'Tajawal'),
                                    unselectedLabelColor: AppColors.greyOutText,
                                    tabs: tabTitleList,
                                  ),
                                ),
                                Expanded(
                                  child: TabBarView(
                                    controller: _tabController,
                                    children: const [
                                      ExpertProfileAboutTab(),
                                      ProductionHouseProfileWorkTab(),
                                      ProductionHouseProfileJobsTab(),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
          );
        },
      ),
    );
  }
}
