import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nxt/view/screens/sign_up/expert/controller/expert_sign_up_cubit.dart';
import '../../../../config/router/router.dart';
import '../../../../data/service/hive_services.dart';
import '../../../../utils/constants/app_colors.dart';
import '../../../../utils/constants/app_images.dart';
import 'package:transparent_image/transparent_image.dart';
import '../../../../utils/constants/app_const.dart';
import '../../sign_up/expert/edit_expert_details_screen.dart';
import '../controller/organization_profile_cubit.dart';

class ProductionHouseProfileHeader extends StatelessWidget {
  const ProductionHouseProfileHeader({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<OrganizationProfileCubit, OrganizationProfileState>(
      builder: (context, state) {
        return Container(
          decoration: const BoxDecoration(
            border: Border(
              top: BorderSide(color: Colors.black, width: 0.9, style: BorderStyle.solid),
              bottom: BorderSide(color: Colors.black, width: 0.9, style: BorderStyle.solid),
            ),
          ),
          child: Row(
            children: [
              Container(
                height: width * 0.13,
                width: width * 0.13,
                decoration: BoxDecoration(
                  color: Colors.black26,
                  image: DecorationImage(
                      fit: BoxFit.cover,
                      image: HiveHelper.getUserAvatar != null && HiveHelper.getUserAvatar != ''
                          ? FadeInImage.memoryNetwork(
                              image: HiveHelper.getUserAvatar ?? userAvatarPlaceHolderUrl,
                              placeholder: kTransparentImage,
                            ).image
                          : Image.asset('assets/images/profile_icon.png').image),
                ),
              ),
              SizedBox(width: width * 0.03),
              SizedBox(
                width: width * 0.65,
                child: Text(
                  HiveHelper.getUserName == ''
                      ? "@${"notAvailable".tr()}"
                      : "@${HiveHelper.getUserName ?? "notAvailable".tr()}",
                  maxLines: 1,
                  softWrap: true,
                  style: Theme.of(context)
                      .textTheme
                      .titleLarge!
                      .copyWith(overflow: TextOverflow.ellipsis, fontFamily: AppConst.isEn ? 'BebasNeue' : 'Tajawal'),
                ),
              ),
              if (HiveHelper.getUserInfo!.user!.role == 'Expert')
                TextButton(
                  onPressed: () {
                    MagicRouter.navigateTo(
                      BlocProvider.value(
                        value: ExpertSignUpCubit(),
                        child: EditExpertDetailsScreen(organizationCubit: OrganizationProfileCubit.get(context)),
                      ),
                    );
                  },
                  child: Text(
                    'edit'.tr(),
                    style: Theme.of(context).textTheme.bodyText1!.copyWith(
                        color: AppColors.semiBlack,
                        fontWeight: FontWeight.bold,
                        decoration: TextDecoration.underline,
                        fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal'),
                  ),
                )
            ],
          ),
        );
      },
    );
  }
}
