import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../utils/constants/app_const.dart';
import 'component/community_user_profile_header.dart';

import '../../../utils/constants/app_colors.dart';
import '../../widgets/loading_state_widget.dart';
import '../connect/cubit/connect_cubit.dart';
import '../navigation_and_appbar/import_widget.dart';
import 'component/community_user_about_tab.dart';
import 'component/community_user_experience_tab.dart';
import 'component/community_user_reels_tab.dart';

class CommunityUserProfile extends StatefulWidget {
  const CommunityUserProfile({Key? key, required this.userId}) : super(key: key);
  final String userId;

  @override
  State<CommunityUserProfile> createState() => _CommunityUserProfileState();
}

class _CommunityUserProfileState extends State<CommunityUserProfile> with SingleTickerProviderStateMixin {
  late TabController _tabController;

  List<Tab> tabTitleList = [
    Tab(child: Text("about".tr())),
    Tab(child: Text("experiences".tr())),
    Tab(child: Text("reels".tr())),
  ];

  @override
  void initState() {
    _tabController = TabController(length: tabTitleList.length, vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    _tabController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => ConnectCubit()..getUserDataById(userId: widget.userId),
      child: CustomScaffoldWidget(
        isInProfile: false,
        extendBodyBehindAppBar: false,
        isBackArrow: true,
        isThereLogo: true,
        isThereActions: true,
        backgroundColor: Colors.white,
        body: BlocConsumer<ConnectCubit, ConnectState>(
          listener: (context, state) {},
          builder: (context, state) {
            final cubit = ConnectCubit.get(context);
            if (cubit.communityUser == null) return const LoadingStateWidget();
            final communityUser = cubit.communityUser!.user!;
            return Column(
              children: [
                CommunityUserProfileHeader(
                    name: communityUser.name, avatar: communityUser.avatar, birthDate: communityUser.birthDate),
                Expanded(
                  child: Column(
                    children: [
                      Row(children: [
                        Container(color: AppColors.yellowColor, width: widget.width * 0.5, height: 8),
                        Container(color: AppColors.blueColor, width: widget.width * 0.5, height: 8)
                      ]),
                      Container(
                        color: Colors.black,
                        child: TabBar(
                          physics: const NeverScrollableScrollPhysics(),
                          controller: _tabController,
                          indicator: const BoxDecoration(color: Colors.black),
                          labelColor: Colors.white,
                          labelStyle: Theme.of(context)
                              .textTheme
                              .headline6!
                              .copyWith(fontFamily: widget.isEn ? 'BebasNeue' : 'Tajawal'),
                          unselectedLabelColor: AppColors.greyOutText,
                          tabs: tabTitleList,
                        ),
                      ),
                      Expanded(
                        child: TabBarView(
                          controller: _tabController,
                          children: [
                            CommunityUserAboutTab(userId: widget.userId),
                            CommunityUserExperienceTab(userId: widget.userId),
                            CommunityUserReelsTab(userId: widget.userId),
                          ],
                        ),
                      ),
                    ],
                  ),
                )
              ],
            );
          },
        ),
      ),
    );
  }
}
