import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:url_launcher/url_launcher.dart';
import '../../../../utils/constants/app_const.dart' as app;
import '../../../../utils/constants/app_colors.dart';
import '../../../../utils/helpers/popup_helper.dart';
import '../../../widgets/error_state_widget.dart';
import '../../../widgets/loading_state_widget.dart';
import '../../connect/cubit/connect_cubit.dart';
import '../../talent_profile/screens/experience/exp_widgets/experience_widget.dart';

class CommunityUserExperienceTab extends StatelessWidget {
  const CommunityUserExperienceTab({Key? key, required this.userId}) : super(key: key);
  final String userId;
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ConnectCubit, ConnectState>(
      builder: (context, state) {
        final cubit = ConnectCubit.get(context);
        final communityUser = cubit.communityUser!;
        if (state is GetCommunityUserFailed) {
          return ErrorStateWidget(hasRefresh: true, onRefresh: () async => cubit.getUserDataById(userId: userId));
        }
        if (state is GetCommunityUserLoading) return const LoadingStateWidget();
        return RefreshIndicator(
          onRefresh: () async => cubit.getUserDataById(userId: userId),
          child: ListView(
            children: [
              Column(
                children: [
                  SizedBox(height: height * 0.01),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Padding(
                        padding: const EdgeInsetsDirectional.only(start: 10.0),
                        child: Text(
                          "link".tr(),
                          style: Theme.of(context)
                              .textTheme
                              .titleLarge!
                              .copyWith(fontFamily: isEn ? 'BebasNeue' : 'Tajawal'),
                        ),
                      ),
                      Opacity(
                        opacity: 0.0,
                        child: TextButton(
                          onPressed: null,
                          child: Text(
                            'save'.tr(),
                            style: TextStyle(
                              fontSize: 14,
                              color: AppColors.semiBlack,
                              fontWeight: FontWeight.bold,
                              decoration: TextDecoration.underline,
                              fontFamily: isEn ? 'Montserrat' : 'Tajawal',
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(height: 10),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: width * 0.03),
                    child: Align(
                      alignment: Alignment.topLeft,
                      child: InkWell(
                        onTap: communityUser.portfolioLink == null || communityUser.portfolioLink == ''
                            ? null
                            : () async {
                                String url = '';
                                if (communityUser.portfolioLink!.contains('https://') ||
                                    communityUser.portfolioLink!.contains('http://')) {
                                  url = communityUser.portfolioLink!.toLowerCase();
                                } else {
                                  url = 'https://${communityUser.portfolioLink!.toLowerCase()}';
                                }
                                try {
                                  await launchUrl(
                                    Uri.parse(url),
                                    mode: LaunchMode.externalApplication,
                                  );
                                } catch (_) {
                                  PopupHelper.showBasicSnack(msg: 'cannotOpenLink'.tr());
                                }
                              },
                        child: Text(
                          communityUser.portfolioLink == ''
                              ? 'emptyLink'.tr()
                              : communityUser.portfolioLink ?? 'emptyLink'.tr(),
                          style: TextStyle(
                              color: (communityUser.portfolioLink == null || communityUser.portfolioLink == '')
                                  ? Colors.black
                                  : const Color(0xff1567F9),
                              decoration: (communityUser.portfolioLink == null || communityUser.portfolioLink == '')
                                  ? null
                                  : TextDecoration.underline,
                              fontFamily: isEn ? 'Montserrat' : 'Tajawal'),
                        ),
                      ),
                    ),
                  )
                ],
              ),
              SizedBox(height: height * 0.01),
              Container(color: Colors.black, width: double.infinity, height: 1.5),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Padding(
                    padding: const EdgeInsetsDirectional.only(start: 10.0),
                    child: Text(
                      "experiences".tr(),
                      style:
                          Theme.of(context).textTheme.titleLarge!.copyWith(fontFamily: isEn ? 'BebasNeue' : 'Tajawal'),
                    ),
                  ),
                  Opacity(
                    opacity: 0.0,
                    child: TextButton(
                      onPressed: null,
                      child: Text(
                        'edit'.tr(),
                        style: TextStyle(
                          color: AppColors.semiBlack,
                          decoration: TextDecoration.underline,
                          fontFamily: isEn ? 'Montserrat' : 'Tajawal',
                          fontSize: 14,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              communityUser.experiences!.isEmpty
                  ? Text(
                      'noExperiences'.tr(),
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontFamily: isEn ? 'Montserrat' : 'Tajawal',
                      ),
                    )
                  : ListView.builder(
                      shrinkWrap: true,
                      physics: const NeverScrollableScrollPhysics(),
                      itemCount: communityUser.experiences!.length,
                      itemBuilder: (context, index) {
                        final workFrom =
                            DateTime.parse(communityUser.experiences![index]!.workFrom ?? DateTime.now().toString());
                        final workTo =
                            DateTime.parse(communityUser.experiences![index]!.workTo ?? DateTime.now().toString());
                        return ExperienceWidget(
                          experienceColor: AppColors.blueColor,
                          expTitle: communityUser.experiences![index]!.jobTitle!,
                          expInfo: communityUser.experiences![index]!.roleDescription!,
                          expDate: '${DateFormat("yyyy").format(workFrom)} - ${DateFormat("yyyy").format(workTo)}',
                        );
                      },
                    ),
              SizedBox(height: height * 0.01),
            ],
          ),
        );
      },
    );
  }
}
