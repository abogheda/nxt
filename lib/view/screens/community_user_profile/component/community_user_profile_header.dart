import 'package:age_calculator/age_calculator.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import '../../../../utils/constants/app_const.dart';
import 'package:transparent_image/transparent_image.dart';

import '../../../../utils/constants/app_colors.dart';

class CommunityUserProfileHeader extends StatelessWidget {
  const CommunityUserProfileHeader({Key? key, required this.avatar, required this.name, required this.birthDate})
      : super(key: key);

  final String? avatar;
  final String? name;
  final String? birthDate;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        border: Border(
          top: BorderSide(color: Colors.black, width: 0.9, style: BorderStyle.solid),
          bottom: BorderSide(color: Colors.black, width: 0.9, style: BorderStyle.solid),
        ),
      ),
      child: Row(
        children: [
          Container(
            height: width * 0.13,
            width: width * 0.13,
            decoration: BoxDecoration(
              color: AppColors.greyOutText,
              image: DecorationImage(
                  fit: BoxFit.cover,
                  image: avatar != null && avatar != ''
                      ? FadeInImage.memoryNetwork(
                          image: avatar!,
                          placeholder: kTransparentImage,
                        ).image
                      : Image.asset('assets/images/profile_icon.png').image),
            ),
          ),
          SizedBox(width: width * 0.03),
          Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                width: width * 0.65,
                child: Text(
                  "@${name ?? "notAvailable".tr()}",
                  style: Theme.of(context)
                      .textTheme
                      .titleLarge!
                      .copyWith(overflow: TextOverflow.ellipsis, fontFamily: isEn ? 'BebasNeue' : 'Tajawal'),
                  maxLines: 1,
                  softWrap: true,
                ),
              ),
              Text(
                birthDate == null
                    ? "notAvailable".tr()
                    : '${AgeCalculator.age(DateTime.parse(birthDate!)).years} ${'year'.tr()}',
                style: Theme.of(context).textTheme.bodyText1!.copyWith(fontFamily: isEn ? 'Montserrat' : 'Tajawal'),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
