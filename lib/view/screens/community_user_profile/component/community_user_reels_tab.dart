import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../../../../utils/constants/app_const.dart';
import '../../../../data/models/profile_reel_model.dart';
import '../../../widgets/error_state_widget.dart';
import '../../../widgets/loading_state_widget.dart';
import '../../../widgets/no_state_widget.dart';
import '../../connect/cubit/connect_cubit.dart';
import '../../talent_profile/screens/reels/reel_widgets/reels_thumbnail.dart';

class CommunityUserReelsTab extends StatelessWidget {
  const CommunityUserReelsTab({Key? key, required this.userId}) : super(key: key);
  final String userId;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ConnectCubit, ConnectState>(
      builder: (context, state) {
        final cubit = ConnectCubit.get(context);
        if (state is GetCommunityUserLoading || cubit.communityUser == null) return const LoadingStateWidget();
        if (state is GetCommunityUserFailed) {
          return ErrorStateWidget(hasRefresh: true, onRefresh: () async => await cubit.getUserDataById(userId: userId));
        }
        List<ProfileReelModel> profileReels = [];
        cubit.communityUser!.reels!.map((reel) {
          profileReels.add(ProfileReelModel(
            v: reel!.v,
            id: reel.id,
            text: reel.text,
            title: reel.title,
            media: reel.media,
            user: reel.user!.id,
            removed: reel.removed,
            updatedAt: reel.updatedAt,
            createdAt: reel.createdAt,
            saveCount: reel.saveCount,
            uploadType: reel.uploadType,
            votesCount: reel.votesCount,
          ));
        }).toList();
        return SizedBox(
          height: double.infinity,
          child: Column(
            children: [
              profileReels.isEmpty
                  ? Expanded(
                      child: RefreshIndicator(
                          onRefresh: () async => await cubit.getUserDataById(userId: userId),
                          child: ListView(children: [SizedBox(height: height * 0.3), NoStateWidget()])))
                  : Expanded(
                      child: RefreshIndicator(
                        onRefresh: () async => await cubit.getUserDataById(userId: userId),
                        child: GridView.builder(
                          itemCount: profileReels.length,
                          gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
                            mainAxisSpacing: 5,
                            childAspectRatio: 1,
                            crossAxisSpacing: 5,
                            maxCrossAxisExtent: 160,
                          ),
                          itemBuilder: (context, index) {
                            return ReelsThumbnail(
                              key: UniqueKey(),
                              videoIndex: index,
                              showDelete: false,
                              reels: profileReels,
                              userName: cubit.communityUser!.user!.name,
                              userAvatar: cubit.communityUser!.user!.avatar,
                              reelThumbnail: profileReels[index].media!,
                              isAudio: profileReels[index].media!.endsWith('.mp3') ||
                                  profileReels[index].media!.endsWith('.wav') ||
                                  profileReels[index].media!.endsWith('.aac'),
                            );
                          },
                        ),
                      ),
                    ),
            ],
          ),
        );
      },
    );
  }
}
