import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import '../../../../utils/constants/resources.dart';

class CommunityUserAboutPhysicalWidget extends StatelessWidget {
  const CommunityUserAboutPhysicalWidget({
    Key? key,
    this.userGender,
    this.userHeight,
    this.userWeight,
  }) : super(key: key);
  final String? userGender;
  final int? userHeight;
  final int? userWeight;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(height: height * 0.01),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Padding(
              padding: const EdgeInsetsDirectional.only(start: 10.0),
              child: Text(
                'physical'.tr(),
                style: Theme.of(context).textTheme.titleLarge!.copyWith(fontFamily: isEn ? 'BebasNeue' : 'Tajawal'),
              ),
            ),
          ],
        ),
        SizedBox(height: height * 0.01),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: width * 0.03),
          child: Column(
            children: [
              Container(
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.black),
                  borderRadius: BorderRadius.circular(8),
                ),
                child: Row(
                  children: <Widget>[
                    SvgPicture.asset('assets/images/gender.svg', height: 48),
                    Text(
                      userGender ?? "notAvailable".tr(),
                      style: AppTextStyles.medium14,
                    )
                  ],
                ),
              ),
              SizedBox(height: height * 0.01),
              Row(
                children: [
                  Expanded(
                    child: Container(
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.black),
                        borderRadius: BorderRadius.circular(8),
                      ),
                      child: Row(
                        children: <Widget>[
                          SvgPicture.asset('assets/images/weight.svg', height: 48),
                          Text('${userWeight ?? "notAvailable".tr()}', style: AppTextStyles.medium14)
                        ],
                      ),
                    ),
                  ),
                  SizedBox(width: width * 0.04),
                  Container(
                    height: width * 0.13,
                    width: width * 0.13,
                    alignment: Alignment.center,
                    decoration: const BoxDecoration(
                      color: Colors.black,
                      borderRadius: BorderRadius.all(Radius.circular(14.0)),
                    ),
                    child: Text(
                      'kg'.tr(),
                      style: TextStyle(
                        fontSize: 14,
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal',
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(height: height * 0.01),
              Row(
                children: [
                  Expanded(
                    child: Container(
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.black),
                        borderRadius: BorderRadius.circular(8),
                      ),
                      child: Row(
                        children: <Widget>[
                          SvgPicture.asset('assets/images/height.svg', height: 48),
                          Text(
                            '${userHeight ?? "notAvailable".tr()}',
                            style: AppTextStyles.medium14,
                          )
                        ],
                      ),
                    ),
                  ),
                  SizedBox(width: width * 0.04),
                  Container(
                    height: width * 0.13,
                    width: width * 0.13,
                    alignment: Alignment.center,
                    decoration: const BoxDecoration(
                      color: Colors.black,
                      borderRadius: BorderRadius.all(Radius.circular(14.0)),
                    ),
                    child: Text(
                      'cm'.tr(),
                      style: TextStyle(
                        fontSize: 14,
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal',
                      ),
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ],
    );
  }
}
