import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import '../../../../utils/constants/app_const.dart' as app;

import '../../../../utils/constants/app_images.dart';
import '../../talent_profile/screens/about/about_widgets/about_look_widget.dart';

class CommunityUserAboutLookWidget extends StatelessWidget {
  const CommunityUserAboutLookWidget(
      {Key? key, required this.hairColor, required this.eyeColor, required this.skinColor})
      : super(key: key);
  final String? hairColor;
  final String? eyeColor;
  final String? skinColor;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: height * 0.01),
        Padding(
          padding: const EdgeInsetsDirectional.only(start: 10.0),
          child: Text(
            'look'.tr(),
            style: Theme.of(context).textTheme.titleLarge!.copyWith(fontFamily: isEn ? 'BebasNeue' : 'Tajawal'),
          ),
        ),
        SizedBox(height: height * 0.01),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: width * 0.03),
          child: Row(
            children: <Widget>[
              SizedBox(
                height: height * 0.16,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: const <Widget>[
                    TitleText(text: 'skinColor'),
                    TitleText(text: 'hairColor'),
                    TitleText(text: 'eyeColor'),
                  ],
                ),
              ),
              SizedBox(width: width * 0.05),
              SizedBox(
                height: height * 0.16,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    LookImage(
                        imagePath: AppImages.getSkinImage(skinColor == ''
                            ? 'Black'
                            : StringExtension(skinColor)?.capitalize() ??
                                StringExtension(skinColor ?? 'Black').capitalize())),
                    LookImage(
                      imagePath: AppImages.getHairImage(
                        hairColor == ''
                            ? 'Black'
                            : StringExtension(hairColor)?.capitalize() ??
                                StringExtension(hairColor ?? 'Black').capitalize(),
                      ),
                    ),
                    LookImage(
                      imagePath: AppImages.getEyeImage(
                        eyeColor == ''
                            ? 'Black'
                            : StringExtension(eyeColor)?.capitalize() ??
                                StringExtension(eyeColor ?? 'Black').capitalize(),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(width: width * 0.05),
              SizedBox(
                height: height * 0.16,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    SubTitleText(
                        userSubtitle: skinColor == '' ? 'notSelected'.tr() : skinColor ?? 'notSelected'.tr(),
                        subTitle: skinColor ?? "notAvailable".tr()),
                    SubTitleText(
                        userSubtitle: hairColor == '' ? 'notSelected'.tr() : hairColor ?? 'notSelected'.tr(),
                        subTitle: hairColor ?? "notAvailable".tr()),
                    SubTitleText(
                        userSubtitle: eyeColor == '' ? 'notSelected'.tr() : eyeColor ?? 'notSelected'.tr(),
                        subTitle: eyeColor ?? "notAvailable".tr()),
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
