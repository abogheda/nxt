// ignore_for_file: must_be_immutable

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:nxt/utils/constants/app_const.dart';

import '../../../../data/models/local_model/user_shot_model.dart';
import '../../talent_profile/screens/about/about_widgets/imports.dart';

class CommunityUserAboutPhotosWidget extends StatefulWidget {
  CommunityUserAboutPhotosWidget({Key? key, required this.shots, required this.index}) : super(key: key);
  List<String> shots;
  final int index;

  @override
  State<CommunityUserAboutPhotosWidget> createState() => _CommunityUserAboutPhotosWidgetState();
}

class _CommunityUserAboutPhotosWidgetState extends State<CommunityUserAboutPhotosWidget> {
  List<UserShotModel> userImages = [];

  @override
  void initState() {
    for (final image in widget.shots) {
      userImages.add(UserShotModel(
        index: widget.shots.indexOf(image),
        imageType: ImageType.url,
        image: image,
      ));
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Padding(
              padding: const EdgeInsetsDirectional.only(start: 10.0),
              child: Text(
                'shots'.tr(),
                style:
                    Theme.of(context).textTheme.titleLarge!.copyWith(fontFamily: widget.isEn ? 'BebasNeue' : 'Tajawal'),
              ),
            ),
            Opacity(
              opacity: 0,
              child: TextButton(
                style: TextButton.styleFrom(padding: EdgeInsets.zero),
                onPressed: () {},
                child: Text(
                  'save'.tr(),
                  style: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.w900,
                    decoration: TextDecoration.underline,
                    fontFamily: widget.isEn ? 'Montserrat' : 'Tajawal',
                  ),
                ),
              ),
            ),
          ],
        ),
        Container(
          constraints: BoxConstraints(maxHeight: widget.height * 0.2),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: userImages
                .map((userImage) => OnePhoto(
                      model: userImage,
                      isEditClicked: false,
                      key: UniqueKey(),
                    ))
                .toList(),
          ),
        ),
      ],
    );
  }
}
