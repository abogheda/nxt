import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../../utils/constants/app_const.dart' as app;

import '../../../../data/enum/talent_type.dart';
import '../../../../data/models/local_model/user_shot_model.dart';
import '../../../../utils/constants/app_images.dart';
import '../../../widgets/error_state_widget.dart';
import '../../../widgets/loading_state_widget.dart';
import '../../connect/cubit/connect_cubit.dart';
import '../../talent_profile/screens/about/edit_widgets/edit_column.dart';
import 'community_user_about_look_widget.dart';
import 'community_user_about_photos_widget.dart';
import 'community_user_about_physical_widget.dart';

class CommunityUserAboutTab extends StatefulWidget {
  const CommunityUserAboutTab({Key? key, required this.userId}) : super(key: key);
  final String userId;

  @override
  State<CommunityUserAboutTab> createState() => _CommunityUserAboutTabState();
}

class _CommunityUserAboutTabState extends State<CommunityUserAboutTab> {
  late bool isActing;
  List<String> addedCategories = [];
  List<UserShotModel> userImages = [];
  @override
  void initState() {
    for (var category in ConnectCubit.get(context).communityUser!.categories!) {
      addedCategories.add(category!.id!);
    }
    isActing = addedCategories.contains(TalentCategoryType.acting.id);
    for (final image in ConnectCubit.get(context).communityUser!.shots!) {
      userImages.add(UserShotModel(
        index: ConnectCubit.get(context).communityUser!.shots!.indexOf(image),
        imageType: ImageType.url,
        image: image,
      ));
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ConnectCubit, ConnectState>(
      builder: (context, state) {
        final cubit = ConnectCubit.get(context);
        if (state is GetCommunityUserFailed) {
          return ErrorStateWidget(
              hasRefresh: true, onRefresh: () async => await cubit.getUserDataById(userId: widget.userId));
        } else if (state is GetCommunityUserLoading) {
          return const LoadingStateWidget();
        } else {
          final cubit = ConnectCubit.get(context);
          return RefreshIndicator(
            onRefresh: () async => await cubit.getUserDataById(userId: widget.userId),
            child: ListView(
              children: [
                EditColumn(
                  header: 'verticals'.tr(),
                  showEditButton: false,
                  list: cubit.communityUser!.categories,
                  fallbackText: 'noVerticals'.tr(),
                  wrapList: cubit.communityUser!.categories!
                      .map((item) => EditItemChip(itemText: item!.title.toString()))
                      .toList(),
                  onPressed: () {},
                ),
                EditColumn(
                  header: 'talents'.tr(),
                  showEditButton: false,
                  list: cubit.communityUser!.talents!,
                  fallbackText: 'noTalents'.tr(),
                  wrapList: cubit.communityUser!.talents!
                      .map((item) => EditItemChip(itemText: item!.title.toString()))
                      .toList(),
                  onPressed: () {},
                ),
                EditColumn(
                  list: cubit.communityUser!.specialSkills,
                  header: 'skills'.tr(),
                  showEditButton: false,
                  fallbackText: 'noSkills'.tr(),
                  wrapList: cubit.communityUser!.specialSkills!
                      .map((item) => EditItemChip(itemText: item.toString()))
                      .toList(),
                  onPressed: () {},
                ),
                isActing
                    ? CommunityUserAboutPhysicalWidget(
                        userGender: cubit.communityUser!.gender,
                        userHeight: cubit.communityUser!.height,
                        userWeight: cubit.communityUser!.weight,
                      )
                    : const SizedBox(),
                isActing
                    ? Column(
                        children: [
                          SizedBox(height: widget.height * 0.01),
                          Container(color: Colors.black, width: double.infinity, height: 1.5),
                          CommunityUserAboutLookWidget(
                              hairColor: cubit.communityUser!.hairColor,
                              eyeColor: cubit.communityUser!.eyeColor,
                              skinColor: cubit.communityUser!.skinColor)
                        ],
                      )
                    : const SizedBox(),
                isActing
                    ? Column(
                        children: [
                          SizedBox(height: widget.height * 0.01),
                          Container(color: Colors.black, width: double.infinity, height: 1.5),
                          CommunityUserAboutPhotosWidget(
                            index: cubit.communityUser!.shots!.length,
                            shots: ConnectCubit.get(context).communityUser!.shots!.isEmpty
                                ? [placeHolderUrl, placeHolderUrl, placeHolderUrl]
                                : cubit.communityUser!.shots ?? [placeHolderUrl, placeHolderUrl, placeHolderUrl],
                          ),
                        ],
                      )
                    : const SizedBox(),
                SizedBox(height: widget.height * 0.025),
              ],
            ),
          );
        }
      },
    );
  }
}
