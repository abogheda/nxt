import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import '../../../../../utils/constants/app_colors.dart';
import '../../../../../utils/constants/app_const.dart';

import '../../../../widgets/custom_text_field.dart';

class ProfileTextFormField extends StatelessWidget {
  const ProfileTextFormField({
    Key? key,
    required this.inputType,
    required this.hint,
    this.controller,
    this.label,
    this.validator,
    this.margin,
    this.focusNode,
    this.onFieldSubmitted,
  }) : super(key: key);
  final TextInputType inputType;
  final String hint;
  final String? label;
  final TextEditingController? controller;
  final String? Function(String?)? validator;
  final EdgeInsetsGeometry? margin;
  final FocusNode? focusNode;
  final Function(String)? onFieldSubmitted;
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: margin ?? EdgeInsets.symmetric(horizontal: width * 0.05, vertical: 6),
      child: CustomTextField(
        hint: hint,
        label: label,
        type: inputType,
        focusNode: focusNode,
        validator: validator,
        controller: controller,
        onFieldSubmitted: onFieldSubmitted,
      ),
    );
  }
}
