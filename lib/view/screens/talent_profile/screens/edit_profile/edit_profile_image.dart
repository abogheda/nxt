import 'package:dotted_border/dotted_border.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:transparent_image/transparent_image.dart';
import '../../../../../data/service/hive_services.dart';
import '../../../../../utils/constants/resources.dart';

import '../../controller/profile_cubit.dart';

class EditProfileImage extends StatefulWidget {
  const EditProfileImage({Key? key}) : super(key: key);

  @override
  State<EditProfileImage> createState() => _EditProfileImageState();
}

class _EditProfileImageState extends State<EditProfileImage> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProfileCubit, ProfileState>(
      builder: (context, state) {
        final cubit = ProfileCubit.get(context);
        return Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Padding(
              padding: EdgeInsets.symmetric(horizontal: widget.width * 0.05, vertical: 12),
              child: DottedBorder(
                borderType: BorderType.RRect,
                radius: const Radius.circular(6),
                dashPattern: const [20, 10],
                child: SizedBox(
                  height: widget.height * 0.15,
                  width: widget.width * 0.3,
                  child: HiveHelper.getUserAvatar == null ||
                          HiveHelper.getUserAvatar == '' ||
                          cubit.usersProfileModel!.user!.avatar == ''
                      ? cubit.profileImage == null
                          ? IconButton(
                              onPressed: () async {
                                await cubit.pickImageForProfile();
                                setState(() {});
                              },
                              icon: const Icon(
                                Icons.add,
                                color: Colors.black,
                              ),
                            )
                          : ClipRRect(
                              borderRadius: const BorderRadius.all(Radius.circular(6.0)),
                              child: InkWell(
                                onTap: () async {
                                  await cubit.pickImageForProfile();
                                  setState(() {});
                                },
                                child: Image.file(cubit.profileImage!, fit: BoxFit.cover),
                              ),
                            )
                      : cubit.profileImage != null
                          ? ClipRRect(
                              borderRadius: const BorderRadius.all(Radius.circular(6.0)),
                              child: InkWell(
                                onTap: () async {
                                  await cubit.pickImageForProfile();
                                  setState(() {});
                                },
                                child: Image.file(cubit.profileImage!, fit: BoxFit.cover),
                              ),
                            )
                          : ClipRRect(
                              borderRadius: const BorderRadius.all(Radius.circular(6.0)),
                              child: InkWell(
                                onTap: () async {
                                  await cubit.pickImageForProfile();
                                  setState(() {});
                                },
                                child: FadeInImage.memoryNetwork(
                                  image: HiveHelper.getUserAvatar!,
                                  fit: BoxFit.cover,
                                  placeholder: kTransparentImage,
                                ),
                              ),
                            ),
                ),
              ),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  'replaceImage'.tr(),
                  style: Theme.of(context).textTheme.titleMedium!.copyWith(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal',
                      ),
                ),
                SizedBox(height: widget.height * 0.01),
                Text(
                  'png'.tr(),
                  style: TextStyle(
                    fontSize: 12,
                    color: AppColors.greyOutText,
                    fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal',
                  ),
                ),
              ],
            ),
          ],
        );
      },
    );
  }
}
