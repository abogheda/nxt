import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../../../config/router/router.dart';
import '../../../../../utils/helpers/popup_helper.dart';
import '../../../../../utils/helpers/validators.dart';
import '../../controller/profile_cubit.dart';
import 'edit_profile_screen.dart';
import 'profile_date_text_form_field.dart';
import '../../../navigation_and_appbar/import_widget.dart';
import '../../../../../data/models/user_models/users_profile_model.dart';
import '../../../../../utils/constants/app_const.dart';
import '../../../../widgets/app_loader.dart';

class AddBirthDateScreen extends StatefulWidget {
  const AddBirthDateScreen({Key? key, required this.usersProfileModel}) : super(key: key);
  final UsersProfileModel usersProfileModel;

  @override
  State<AddBirthDateScreen> createState() => _AddBirthDateScreenState();
}

class _AddBirthDateScreenState extends State<AddBirthDateScreen> {
  late TextEditingController birthController;
  final formKey = GlobalKey<FormState>();
  late ProfileCubit cubit;
  @override
  void initState() {
    cubit = context.read<ProfileCubit>();
    birthController = TextEditingController(text: DateFormat("dd/MM/yyyy").format(DateTime(DateTime.now().year - 15)));
    super.initState();
  }

  @override
  void dispose() {
    birthController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<ProfileCubit, ProfileState>(
      listener: (context, state) {
        if (state is EditUserBirthDateFailed) {
          PopupHelper.showBasicSnack(msg: state.msg, color: Colors.red);
          return;
        }
        if (state is EditUserBirthDateSuccess) {
          MagicRouter.pop();
          MagicRouter.navigateTo(
            BlocProvider.value(
              value: ProfileCubit.get(context),
              child: EditProfileScreen(usersProfileModel: cubit.usersProfileModel!),
            ),
          );
        }
      },
      builder: (context, state) {
        final cubit = ProfileCubit.get(context);
        return CustomScaffoldWidget(
          backgroundColor: Colors.white,
          extendBodyBehindAppBar: false,
          isBackArrow: true,
          isInProfile: true,
          isLogoClickable: true,
          isThereActions: true,
          isThereLogo: true,
          body: Column(
            children: [
              const Divider(color: Colors.black, thickness: 0.9),
              Expanded(
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(height: widget.height * 0.01),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: widget.width * 0.03),
                      child: Text(
                        'updateBirthdateHeader'.tr(),
                        textAlign: TextAlign.center,
                        style: Theme.of(context).textTheme.titleLarge!.copyWith(
                              color: Colors.black,
                              fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal',
                            ),
                      ),
                    ),
                    SizedBox(height: widget.height * 0.02),
                    Form(
                      key: formKey,
                      child: Column(
                        children: <Widget>[
                          ProfileDateTextFormField(
                            controller: birthController,
                            validator: Validators.generalField,
                            initialValue:
                                DateTime.parse(widget.usersProfileModel.user!.birthDate ?? DateTime.now().toString()),
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(height: 12),
                    state is EditUserBirthDateLoading
                        ? const AppLoader()
                        : ElevatedButton(
                            onPressed: () async {
                              if (formKey.currentState!.validate()) {
                                setState(() {});
                                await cubit.editUserBirthDate(
                                    birthDate: DateFormat('dd/MM/yyyy').parse(birthController.text).toIso8601String());
                                await cubit.getUserData();
                              }
                            },
                            child: Text(
                              'updateBirthdate'.tr(),
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal',
                              ),
                            ),
                          )
                  ],
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
