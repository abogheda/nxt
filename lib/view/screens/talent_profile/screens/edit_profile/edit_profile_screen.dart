import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../../../config/router/router.dart';
import '../../../../../utils/constants/app_colors.dart';
import '../../../../../utils/constants/app_text_style.dart';
import '../../../../../utils/helpers/popup_helper.dart';
import '../../../../../utils/helpers/validators.dart';
import '../../../../widgets/custom_drop_down_field.dart';
import '../../controller/profile_cubit.dart';
import '../change_password/change_password_screen.dart';
import 'profile_date_text_form_field.dart';
import 'profile_text_form_field.dart';
import '../../../../widgets/custom_button.dart';
import '../../../navigation_and_appbar/import_widget.dart';

import '../../../../../data/models/user_models/users_profile_model.dart';
import '../../../../../data/service/hive_services.dart';
import '../../../../../utils/constants/app_const.dart';
import '../../../../widgets/app_loader.dart';
import 'edit_profile_image.dart';

class EditProfileScreen extends StatefulWidget {
  const EditProfileScreen({Key? key, required this.usersProfileModel})
      : super(key: key);
  final UsersProfileModel usersProfileModel;

  @override
  State<EditProfileScreen> createState() => _EditProfileScreenState();
}

class _EditProfileScreenState extends State<EditProfileScreen> {
  late TextEditingController nameController;
  late TextEditingController emailController;
  late TextEditingController birthController;
  late TextEditingController nationalityController;
  late TextEditingController countryController;
  late TextEditingController actFromController;
  late TextEditingController actToController;
  final formKey = GlobalKey<FormState>();
  late ProfileCubit cubit;
  late String gender ;


  @override
  void initState() {
    final user = widget.usersProfileModel;
    cubit = context.read<ProfileCubit>();
    nameController = TextEditingController(text: user.user?.name!);
    emailController = TextEditingController(text: user.user?.email!);
    nationalityController = TextEditingController(text: user.user?.nationality ?? '');
    countryController = TextEditingController(text: user.user?.countryName!);
    actFromController = TextEditingController(text: user.user?.actingAgeFrom.toString());
    actToController = TextEditingController(text: user.user?.actingAgeTo.toString());
    gender = cubit.usersProfileModel?.gender ?? '';
    birthController = TextEditingController(
        text: DateFormat("dd/MM/yyyy").format(DateTime.parse(
            HiveHelper.getUserBirthDate ?? DateTime.now().toString())));
    super.initState();
  }

  @override
  void dispose() {
    nameController.dispose();
    emailController.dispose();
    birthController.dispose();
    cubit.profileImageUrl = null;
    cubit.profileImage = null;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<ProfileCubit, ProfileState>(
      listener: (context, state) {
        if (state is EditProfileFailed) {
          PopupHelper.showBasicSnack(msg: state.msg, color: Colors.red);
          return;
        }
        if (state is UploadProfileImageFailed) {
          PopupHelper.showBasicSnack(msg: state.msg, color: Colors.red);
          return;
        }
        if (state is EditProfileSuccess) MagicRouter.pop();
      },
      builder: (context, state) {
        final cubit = ProfileCubit.get(context);
        return CustomScaffoldWidget(
          backgroundColor: Colors.white,
          extendBodyBehindAppBar: false,
          isBackArrow: true,
          isInProfile: true,
          isLogoClickable: true,
          isThereActions: true,
          isThereLogo: true,
          body: Stack(
            children: [
              Positioned(
                top: 0,
                right: 0,
                left: 0,
                bottom: 0,
                child: ListView(
                  padding: const EdgeInsets.symmetric(horizontal: 15),
                  children: [
                    const Divider(color: Colors.black, thickness: 0.9),
                    SizedBox(height: widget.height * 0.01),
                    Text(
                      'about'.tr(),
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.titleLarge!.copyWith(
                            color: AppColors.semiBlack,
                            fontFamily: AppConst.isEn ? 'BebasNeue' : 'Tajawal',
                          ),
                    ),
                    SizedBox(height: widget.height * 0.02),
                    Form(
                      key: formKey,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          BlocProvider.value(
                            value: cubit,
                            child: const EditProfileImage(),
                          ),
                          Text(
                            'nameHint'.tr(),
                            textAlign: TextAlign.start,
                            style: Theme.of(context)
                                .textTheme
                                .titleLarge!
                                .copyWith(
                                  color: AppColors.semiBlack,
                                  fontFamily:
                                      AppConst.isEn ? 'BebasNeue' : 'Tajawal',
                                ),
                          ),
                          ProfileTextFormField(
                            inputType: TextInputType.name,
                            hint: 'nameHint'.tr(),
                            label: 'nameHint'.tr(),
                            controller: nameController,
                            validator: Validators.generalField,
                          ),
                          Text(
                            'emailHint'.tr(),
                            textAlign: TextAlign.center,
                            style: Theme.of(context)
                                .textTheme
                                .titleLarge!
                                .copyWith(
                                  color: AppColors.semiBlack,
                                  fontFamily:
                                      AppConst.isEn ? 'BebasNeue' : 'Tajawal',
                                ),
                          ),
                          ProfileTextFormField(
                            inputType: TextInputType.emailAddress,
                            hint: 'emailHint'.tr(),
                            controller: emailController,
                            validator: Validators.email,
                          ),
                          Text(
                            'dateOfBirth'.tr(),
                            textAlign: TextAlign.center,
                            style: Theme.of(context)
                                .textTheme
                                .titleLarge!
                                .copyWith(
                                  color: AppColors.semiBlack,
                                  fontFamily:
                                      AppConst.isEn ? 'BebasNeue' : 'Tajawal',
                                ),
                          ),
                          ProfileDateTextFormField(
                            controller: birthController,
                            validator: Validators.generalField,
                            initialValue: DateTime.parse(
                                widget.usersProfileModel.user!.birthDate ??
                                    DateTime.now().toString()),
                          ),
                          Text(
                            'ACTING AGE',
                            textAlign: TextAlign.center,
                            style: Theme.of(context)
                                .textTheme
                                .titleLarge!
                                .copyWith(
                              color: AppColors.semiBlack,
                              fontFamily:
                              AppConst.isEn ? 'BebasNeue' : 'Tajawal',
                            ),
                          ),
                          Row(
                            children: [
                              Expanded(
                                child: ProfileTextFormField(
                                  inputType: TextInputType.number,
                                  hint: '',
                                  controller:actFromController,
                                  validator: Validators.generalField,
                                ),
                              ),
                              Expanded(
                                child: ProfileTextFormField(
                                  inputType: TextInputType.number,
                                  hint: '',
                                  controller: actToController,
                                  validator: Validators.generalField,
                                ),
                              ),
                            ],
                          ),
                          Text(
                            'nationality'.tr(),
                            textAlign: TextAlign.center,
                            style: Theme.of(context)
                                .textTheme
                                .titleLarge!
                                .copyWith(
                              color: AppColors.semiBlack,
                              fontFamily:
                              AppConst.isEn ? 'BebasNeue' : 'Tajawal',
                            ),
                          ),
                          ProfileTextFormField(
                            inputType: TextInputType.text,
                            hint: 'nationality'.tr(),
                            controller: nationalityController,
                            validator: Validators.generalField,
                          ),
                          Text(
                            'country'.tr(),
                            textAlign: TextAlign.center,
                            style: Theme.of(context)
                                .textTheme
                                .titleLarge!
                                .copyWith(
                              color: AppColors.semiBlack,
                              fontFamily:
                              AppConst.isEn ? 'BebasNeue' : 'Tajawal',
                            ),
                          ),
                          ProfileTextFormField(
                            inputType: TextInputType.text,
                            hint: 'country'.tr(),
                            controller: countryController,
                            validator: Validators.generalField,
                          ),
                          Text(
                            'gender'.tr(),
                            textAlign: TextAlign.center,
                            style: Theme.of(context)
                                .textTheme
                                .titleLarge!
                                .copyWith(
                              color: AppColors.semiBlack,
                              fontFamily:
                              AppConst.isEn ? 'BebasNeue' : 'Tajawal',
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.symmetric(horizontal: widget.width * 0.05, vertical: 6),
                            child: CustomDropDownTextField<String>(
                              items: [
                                DropdownMenuItem(
                                  value: 'Male',
                                  child: Text('Male'.tr(),
                                      style: TextStyle(
                                        fontFamily: widget.isEn ? 'Montserrat' : 'Tajawal',
                                      )),
                                ),
                                DropdownMenuItem(
                                  value: 'Female',
                                  child: Text('Female'.tr(),
                                      style: TextStyle(
                                        fontFamily: widget.isEn ? 'Montserrat' : 'Tajawal',
                                      )),
                                )
                              ],
                              onChanged: (p0) {
                                gender = p0!;
                              },
                              hint: cubit.usersProfileModel?.gender,
                              hintTextStyle: AppTextStyles.medium14,
                              // prefix: SvgPicture.asset('assets/images/gender.svg'),
                            ),
                          ),
                        ],
                      ),
                    ),
                    // Padding(
                    //   padding:
                    //       EdgeInsets.symmetric(horizontal: widget.width * 0.05),
                    //   child: ElevatedButton(
                    //     onPressed: () =>
                    //         MagicRouter.navigateTo(BlocProvider.value(
                    //       value: cubit,
                    //       child: const ChangePasswordScreen(),
                    //     )),
                    //     child: Text(
                    //       'changePassword'.tr(),
                    //       style: TextStyle(
                    //           fontFamily:
                    //               widget.isEn ? 'Montserrat' : 'Tajawal',
                    //           fontWeight: FontWeight.bold),
                    //     ),
                    //   ),
                    // ),
                    SizedBox(height: widget.height * 0.1),
                  ],
                ),
              ),
              Positioned(
                right: 0,
                left: 0,
                bottom: 0,
                child: Container(
                  color: Colors.black,
                  child: CustomButton(
                    height: widget.height * 0.08,
                    width: double.infinity,
                    onTap: () async {
                      if (formKey.currentState!.validate()) {
                        setState(() {});
                        await cubit.editUserProfile(
                            name: nameController.text.trim(),
                            email: emailController.text.trim(),
                            birthDate: DateFormat('dd/MM/yyyy')
                                .parse(birthController.text)
                                .toIso8601String(),
                            avatar: HiveHelper.getUserAvatar!,
                            nationality: nationalityController.text,
                            country: countryController.text,
                            actingAgeFrom: int.parse(actFromController.text),
                            actingAgeTo: int.parse(actToController.text), gender: gender);
                        await cubit.getUserData();
                      }
                    },
                    child: state is GetProfileLoading ||
                            state is EditProfileLoading ||
                            state is UploadProfileImageLoading
                        ? const AppLoader(color: Colors.white)
                        : Text(
                            'saveChange'.tr().toUpperCase(),
                            style: TextStyle(
                              fontSize: 18,
                              color: Colors.white,
                              fontFamily:
                                  AppConst.isEn ? 'Montserrat' : 'Tajawal',
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
