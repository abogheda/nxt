import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import '../../../../../utils/constants/app_colors.dart';
import '../../../../../utils/constants/app_const.dart';

import '../../../../../utils/helpers/picker_helper.dart';
import '../../../../widgets/custom_text_field.dart';

class ProfileDateTextFormField extends StatefulWidget {
  const ProfileDateTextFormField({
    Key? key,
    required this.controller,
    required this.initialValue,
    this.validator,
    this.margin,
  }) : super(key: key);
  final TextEditingController controller;
  final DateTime initialValue;
  final String? Function(String?)? validator;
  final EdgeInsetsGeometry? margin;

  @override
  State<ProfileDateTextFormField> createState() => _ProfileDateTextFormFieldState();
}

class _ProfileDateTextFormFieldState extends State<ProfileDateTextFormField> {
  final year = DateTime.now().year;
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: widget.margin ?? EdgeInsets.symmetric(horizontal: widget.width * 0.05, vertical: 6),
      child: CustomTextField(
        controller: widget.controller,
        prefix: Padding(
            padding: const EdgeInsets.all(8.0),
            child: SvgPicture.asset('assets/images/date_icon.svg', color: AppColors.iconsColor, width: 24)),
        type: TextInputType.datetime,
        hint: 'birthDateHint'.tr(),
        validator: widget.validator,
        onTap: () async {
          FocusManager.instance.primaryFocus?.unfocus();
          final date = await PickerHelper.pickDate(
            currentTime: widget.initialValue,
            minTime: DateTime(year - 90),
            maxTime: DateTime(
              DateTime.now().year - 13,
              DateTime.now().month,
              DateTime.now().day,
            ),
          );
          if (date != null) {
            setState(() {
              widget.controller.text = DateFormat("dd/MM/yyyy").format(date);
            });
          }
        },
      ),
    );
  }
}
