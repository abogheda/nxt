part of 'imports.dart';

class OneSkillWidget extends StatefulWidget {
  final String skillName;
  const OneSkillWidget({Key? key, required this.skillName}) : super(key: key);

  @override
  State<OneSkillWidget> createState() => _OneSkillWidgetState();
}

class _OneSkillWidgetState extends State<OneSkillWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        border: Border.all(color: AppColors.borderGreyColor),
        borderRadius: const BorderRadius.all(Radius.circular(6.0)),
        color: Colors.black,
      ),
      padding: const EdgeInsets.all(12),
      child: Text(
        widget.skillName,
        style: AppTextStyles.bold14.copyWith(
          color: Colors.white,
        ),
      ),
    );
  }
}
