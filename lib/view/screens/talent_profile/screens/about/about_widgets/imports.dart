import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:transparent_image/transparent_image.dart';
import '../../../../../../utils/constants/resources.dart';
import '../../../../../../utils/helpers/validators.dart';
import '../../../controller/profile_cubit.dart';
import 'dart:io';
import 'package:dotted_border/dotted_border.dart';
import '../../../../../../data/models/local_model/user_shot_model.dart';
import '../../../../../../utils/helpers/picker_helper.dart';
import 'package:flutter_svg/svg.dart';
import '../../../../../widgets/custom_text_field.dart';
import '../../../../../widgets/custom_drop_down_field.dart';

import '../../../../../../config/router/router.dart';
import '../../../../../widgets/pick_image_dialog.dart';

part 'one_photo_widget.dart';
part 'one_skill_widget.dart';
part '../../experience/exp_widgets/about_shots_widget.dart';
part 'about_physical_widget.dart';
