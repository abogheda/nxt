part of 'imports.dart';

class OnePhoto extends StatelessWidget {
  final bool? isEditClicked;
  final UserShotModel model;

  const OnePhoto({
    Key? key,
    required this.model,
    this.isEditClicked = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DottedBorder(
      borderType: BorderType.RRect,
      radius: const Radius.circular(6),
      dashPattern: isEditClicked! ? const [1, 0.001] : const [20, 10],
      child: ClipRRect(
        borderRadius: const BorderRadius.all(Radius.circular(6.0)),
        child: model.imageType == ImageType.url
            ? FadeInImage.memoryNetwork(
                image: model.image == '' ? userAvatarPlaceHolderUrl : model.image,
                placeholder: kTransparentImage,
                fit: BoxFit.cover,
                width: width * 0.29,
                height: height * 0.2)
            : Image.file(File(model.image), fit: BoxFit.cover, width: width * 0.29, height: height * 0.2),
      ),
    );
  }
}
