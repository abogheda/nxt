import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../../../../utils/constants/resources.dart';
import '../../../../complete_profile_screen/cubit/complete_profile_cubit.dart';
import '../../../controller/profile_cubit.dart';

import '../../../../../../config/router/router.dart';
import '../edit_widgets/edit_about_look.dart';

class AboutLookWidget extends StatefulWidget {
  final String skinColor;
  final String? skinColorImg;
  final String hairColor;
  final String? hairColorImg;
  final String eyeColor;
  final String? eyeColorImg;
  const AboutLookWidget(
      {Key? key,
      required this.skinColor,
      required this.skinColorImg,
      required this.hairColor,
      required this.hairColorImg,
      required this.eyeColor,
      required this.eyeColorImg})
      : super(key: key);

  @override
  State<AboutLookWidget> createState() => _AboutLookWidgetState();
}

class _AboutLookWidgetState extends State<AboutLookWidget> {
  late ProfileCubit profileCubit;
  @override
  void initState() {
    profileCubit = context.read<ProfileCubit>();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(height: widget.height * 0.01),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Padding(
              padding: const EdgeInsetsDirectional.only(start: 10.0),
              child: Text(
                'look'.tr(),
                style:
                    Theme.of(context).textTheme.titleLarge!.copyWith(fontFamily: widget.isEn ? 'BebasNeue' : 'Tajawal'),
              ),
            ),
            BlocProvider(
              create: (context) => CompleteProfileCubit(),
              child: TextButton(
                onPressed: () async {
                  await MagicRouter.navigateTo(EditAboutLook(profileCubit: context.read<ProfileCubit>()));
                  setState(() {});
                },
                child: Text(
                  'edit'.tr(),
                  style: TextStyle(
                    fontSize: 14,
                    color: AppColors.semiBlack,
                    fontWeight: FontWeight.w900,
                    decoration: TextDecoration.underline,
                    fontFamily: widget.isEn ? 'Montserrat' : 'Tajawal',
                  ),
                ),
              ),
            ),
          ],
        ),
        SizedBox(height: widget.height * 0.01),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: widget.width * 0.03),
          child: Column(
            children: [
              Row(
                children:  [
                  const TitleText(text: "weight"),
                  SizedBox(width: widget.width * 0.05),
                  SubTitleText(userSubtitle: profileCubit.usersProfileModel?.weight.toString(), subTitle: 'KG',),
                  const SizedBox(width: 5),

                  const TitleText(text: "KG   ,"),
                  SizedBox(width: widget.width * 0.05),
                  const TitleText(text: "HEIGHT"),
                  SizedBox(width: widget.width * 0.05),
                  SubTitleText(userSubtitle: profileCubit.usersProfileModel?.height.toString(), subTitle: 'CM',),
                  const SizedBox(width: 5),

                  const TitleText(text: "CM"),
                ],
              ),
              Row(
                children: <Widget>[
                  SizedBox(
                    height: widget.height * 0.16,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: const <Widget>[
                        TitleText(text: 'skinColor'),
                        TitleText(text: 'hairColor'),
                        TitleText(text: 'eyeColor'),
                      ],
                    ),
                  ),
                  SizedBox(width: widget.width * 0.05),
                  // SizedBox(
                  //   height: widget.height * 0.16,
                  //   child: Column(
                  //     crossAxisAlignment: CrossAxisAlignment.start,
                  //     mainAxisAlignment: MainAxisAlignment.spaceAround,
                  //     children: <Widget>[
                  //       LookImage(
                  //         imagePath: AppImages.getSkinImage(
                  //           profileCubit.userSkinColor == ''
                  //               ? 'Black'
                  //               : StringExtension(profileCubit.userSkinColor)?.capitalize() ??
                  //                   StringExtension(widget.skinColorImg ?? 'Black').capitalize(),
                  //         ),
                  //       ),
                  //       LookImage(
                  //         imagePath: AppImages.getHairImage(
                  //           profileCubit.userHairColor == ''
                  //               ? 'Black'
                  //               : StringExtension(profileCubit.userHairColor)?.capitalize() ??
                  //                   StringExtension(widget.hairColorImg ?? 'Black').capitalize(),
                  //         ),
                  //       ),
                  //       LookImage(
                  //         imagePath: AppImages.getEyeImage(
                  //           profileCubit.userEyeColor == ''
                  //               ? 'Black'
                  //               : StringExtension(profileCubit.userEyeColor)?.capitalize() ??
                  //                   StringExtension(widget.eyeColorImg ?? 'Black').capitalize(),
                  //         ),
                  //       ),
                  //     ],
                  //   ),
                  // ),
                  // SizedBox(width: widget.width * 0.05),
                  SizedBox(
                    height: widget.height * 0.16,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        SubTitleText(userSubtitle: profileCubit.userSkinColor, subTitle: widget.skinColor),
                        SubTitleText(userSubtitle: profileCubit.userHairColor, subTitle: widget.hairColor),
                        SubTitleText(userSubtitle: profileCubit.userEyeColor, subTitle: widget.eyeColor),
                      ],
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
        SizedBox(height: widget.height * 0.01),
        Container(color: Colors.black, width: double.infinity, height: 1.5),
        SizedBox(height: widget.height * 0.01),
      ],
    );
  }
}

extension StringExtension on String {
  String capitalize() {
    if (this == '') {
      return 'Black';
    }
    return "${this[0].toUpperCase()}${substring(1)}";
  }
}

class TitleText extends StatelessWidget {
  const TitleText({Key? key, required this.text}) : super(key: key);
  final String text;
  @override
  Widget build(BuildContext context) {
    return Text(
      text.tr().toUpperCase(),
      style: Theme.of(context).textTheme.bodyText1!.copyWith(
            fontWeight: FontWeight.bold,
            fontFamily: isEn ? 'Montserrat' : 'Tajawal',
          ),
    );
  }
}

class SubTitleText extends StatelessWidget {
  const SubTitleText({
    Key? key,
    required this.userSubtitle,
    required this.subTitle,
  }) : super(key: key);
  final String? userSubtitle;
  final String subTitle;
  @override
  Widget build(BuildContext context) {
    return Text(
      userSubtitle ?? subTitle,
      style: Theme.of(context).textTheme.bodyText1!.copyWith(
          color: AppColors.iconsColor, fontWeight: FontWeight.bold, fontFamily: isEn ? 'Montserrat' : 'Tajawal'),
    );
  }
}

class LookImage extends StatelessWidget {
  const LookImage({Key? key, required this.imagePath}) : super(key: key);
  final String imagePath;
  @override
  Widget build(BuildContext context) {
    return Image.asset(
      imagePath,
      width: width * 0.09,
      height: width * 0.09,
    );
  }
}
