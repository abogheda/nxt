import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import '../../../../../../utils/constants/resources.dart';
import '../../../../complete_profile_screen/cubit/complete_profile_cubit.dart';
import '../../../controller/profile_cubit.dart';

import '../../../../../../config/router/router.dart';
import '../edit_widgets/edit_social_media.dart';

class SocialMediaRow extends StatefulWidget {

  const SocialMediaRow(
      {Key? key,})
      : super(key: key);

  @override
  State<SocialMediaRow> createState() => _SocialMediaRowState();
}

class _SocialMediaRowState extends State<SocialMediaRow> {
  late ProfileCubit profileCubit;
  @override
  void initState() {
    profileCubit = context.read<ProfileCubit>();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(height: widget.height * 0.01),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Padding(
              padding: const EdgeInsetsDirectional.only(start: 10.0),
              child: Text(
                'SOCIAl MEDIA PROFILES',
                style:
                Theme.of(context).textTheme.titleLarge!.copyWith(fontFamily: widget.isEn ? 'BebasNeue' : 'Tajawal'),
              ),
            ),
            BlocProvider(
              create: (context) => CompleteProfileCubit(),
              child: TextButton(
                onPressed: () async {
                  await MagicRouter.navigateTo(EditSocialMediaProfile(profileCubit: context.read<ProfileCubit>()));
                  setState(() {});
                },
                child: Text(
                  'edit'.tr(),
                  style: TextStyle(
                    fontSize: 14,
                    color: AppColors.semiBlack,
                    fontWeight: FontWeight.w900,
                    decoration: TextDecoration.underline,
                    fontFamily: widget.isEn ? 'Montserrat' : 'Tajawal',
                  ),
                ),
              ),
            ),
          ],
        ),
        Row(
          children: List.generate(profileCubit.usersProfileModel?.socialMedia?.length ??0,
                  (index) => Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: SvgPicture.asset('assets/images/${profileCubit.usersProfileModel?.socialMedia?[index].name}_social.svg', width: 35),
                  )),
        ),
        SizedBox(height: widget.height * 0.01),
        Container(color: Colors.black, width: double.infinity, height: 1.5),
        SizedBox(height: widget.height * 0.01),
      ],
    );
  }
}
