import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../../../../utils/constants/resources.dart';
import '../../../../complete_profile_screen/cubit/complete_profile_cubit.dart';
import '../../../controller/profile_cubit.dart';

import '../../../../../../config/router/router.dart';
import '../../experience/exp_widgets/experience_widget.dart';
import '../edit_widgets/edit_about_look.dart';
import '../edit_widgets/edit_educational_experiences.dart';

class AboutEducationalExperiences extends StatefulWidget {

  const AboutEducationalExperiences(
      {Key? key,})
      : super(key: key);

  @override
  State<AboutEducationalExperiences> createState() => _AboutEducationalExperiencesState();
}

class _AboutEducationalExperiencesState extends State<AboutEducationalExperiences> {
  late ProfileCubit profileCubit;
  @override
  void initState() {
    profileCubit = context.read<ProfileCubit>();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(height: widget.height * 0.01),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Padding(
              padding: const EdgeInsetsDirectional.only(start: 10.0),
              child: Text(
                'educationalExperiences'.tr(),
                style:
                Theme.of(context).textTheme.titleLarge!.copyWith(fontFamily: widget.isEn ? 'BebasNeue' : 'Tajawal'),
              ),
            ),
            BlocProvider(
              create: (context) => CompleteProfileCubit(),
              child: TextButton(
                onPressed: () async {
                  await MagicRouter.navigateTo(ExperienceExperiencesEdit(profileCubit: context.read<ProfileCubit>()));
                  setState(() {});
                },
                child: Text(
                  'edit'.tr(),
                  style: TextStyle(
                    fontSize: 14,
                    color: AppColors.semiBlack,
                    fontWeight: FontWeight.w900,
                    decoration: TextDecoration.underline,
                    fontFamily: widget.isEn ? 'Montserrat' : 'Tajawal',
                  ),
                ),
              ),
            ),
          ],
        ),
        ListView.builder(
          shrinkWrap: true,
          physics: const NeverScrollableScrollPhysics(),
          itemCount: profileCubit.usersProfileModel?.educations?.length,
          itemBuilder: (context, index) {
            return ExperienceWidget(
              expTitle: profileCubit.usersProfileModel?.educations![index].courseName! ?? '',
              expInfo: profileCubit.usersProfileModel?.educations![index].courseLocation! ?? '',
              expDate: '',
            );
          },
        ),
        SizedBox(height: widget.height * 0.01),
        Container(color: Colors.black, width: double.infinity, height: 1.5),
        SizedBox(height: widget.height * 0.01),
      ],
    );
  }
}








