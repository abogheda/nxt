part of 'imports.dart';

class AboutPhysicalWidget extends StatefulWidget {
  final String userWeight;
  final String userHeight;
  final String userGender;
  const AboutPhysicalWidget({Key? key, required this.userWeight, required this.userHeight, required this.userGender})
      : super(key: key);

  @override
  State<AboutPhysicalWidget> createState() => _AboutPhysicalWidgetState();
}

class _AboutPhysicalWidgetState extends State<AboutPhysicalWidget> {
  bool isEnabled = false;
  final _formKey = GlobalKey<FormState>();
  final _formKey2 = GlobalKey<FormState>();

  late ProfileCubit cubit;

  String? gender;
  String? userWeight;
  String? userHeight;

  @override
  void initState() {
    cubit = context.read<ProfileCubit>();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProfileCubit, ProfileState>(
      builder: (context, state) {
        return Column(
          children: [
            SizedBox(height: widget.height * 0.01),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Padding(
                  padding: const EdgeInsetsDirectional.only(start: 10.0),
                  child: Text(
                    'physical'.tr(),
                    style: Theme.of(context)
                        .textTheme
                        .titleLarge!
                        .copyWith(fontFamily: widget.isEn ? 'BebasNeue' : 'Tajawal'),
                  ),
                ),
                TextButton(
                  style: TextButton.styleFrom(padding: EdgeInsets.zero),
                  onPressed: () {
                    if (isEnabled == false) {
                      setState(() {
                        isEnabled = true;
                      });
                    } else {
                      if (_formKey.currentState!.validate() && _formKey2.currentState!.validate()) {
                        _formKey.currentState!.save();
                        setState(() {
                          isEnabled = false;
                        });
                        context.read<ProfileCubit>().editPhysical(
                              gender: gender,
                              height: userHeight,
                              weight: userWeight,
                            );
                      }
                    }
                  },
                  child: Text(
                    isEnabled ? 'save'.tr() : 'edit'.tr(),
                    style: TextStyle(
                      color: AppColors.semiBlack,
                      decoration: TextDecoration.underline,
                      fontFamily: widget.isEn ? 'Montserrat' : 'Tajawal',
                      fontSize: 14,
                      fontWeight: FontWeight.w900,
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(height: widget.height * 0.01),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: widget.width * 0.03),
              child: Column(
                children: [
                  Stack(
                    children: [
                      CustomDropDownTextField<String>(
                        items: [
                          DropdownMenuItem(
                            value: 'Male',
                            child: Text('Male'.tr(),
                                style: TextStyle(
                                  fontFamily: widget.isEn ? 'Montserrat' : 'Tajawal',
                                )),
                          ),
                          DropdownMenuItem(
                            value: 'Female',
                            child: Text('Female'.tr(),
                                style: TextStyle(
                                  fontFamily: widget.isEn ? 'Montserrat' : 'Tajawal',
                                )),
                          )
                        ],
                        onChanged: (p0) {
                          gender = p0;
                        },
                        hint: widget.userGender,
                        hintTextStyle: AppTextStyles.medium14,
                        prefix: SvgPicture.asset('assets/images/gender.svg'),
                      ),
                      if (!isEnabled)
                        const Positioned.fill(
                          child: ColoredBox(
                            color: Colors.transparent,
                          ),
                        )
                    ],
                  ),
                  SizedBox(height: widget.height * 0.01),
                  Row(
                    children: [
                      Expanded(
                        child: Form(
                          key: _formKey,
                          child: CustomTextField(
                            validator: Validators.weight,
                            textStyle: TextStyle(fontFamily: widget.isEn ? 'Montserrat' : 'Tajawal'),
                            onChange: (weight) {
                              if (_formKey.currentState!.validate()) {
                                userWeight = weight;
                              } else {
                                userWeight = weight;
                              }
                            },
                            initialValue: widget.userWeight,
                            type: TextInputType.number,
                            enabled: isEnabled,
                            hint: 'yourWeight'.tr(),
                            prefix: SvgPicture.asset('assets/images/weight.svg'),
                          ),
                        ),
                      ),
                      SizedBox(width: widget.width * 0.04),
                      Container(
                        height: widget.width * 0.13,
                        width: widget.width * 0.13,
                        alignment: Alignment.center,
                        decoration: const BoxDecoration(
                          color: Colors.black,
                          borderRadius: BorderRadius.all(Radius.circular(14.0)),
                        ),
                        child: Text(
                          'kg'.tr(),
                          style: TextStyle(
                            fontSize: 14,
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal',
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: widget.height * 0.01),
                  Row(
                    children: [
                      Expanded(
                        child: Form(
                          key: _formKey2,
                          child: CustomTextField(
                            validator: Validators.height,
                            onChange: (height) {
                              if (_formKey2.currentState!.validate()) {
                                userHeight = height;
                              } else {
                                userHeight = height;
                              }
                            },
                            initialValue: widget.userHeight,
                            enabled: isEnabled,
                            hint: 'yourHeight'.tr(),
                            prefix: SvgPicture.asset('assets/images/height.svg'),
                            type: TextInputType.number,
                          ),
                        ),
                      ),
                      SizedBox(width: widget.width * 0.04),
                      Container(
                        height: widget.width * 0.13,
                        width: widget.width * 0.13,
                        alignment: Alignment.center,
                        decoration: const BoxDecoration(
                          color: Colors.black,
                          borderRadius: BorderRadius.all(Radius.circular(14.0)),
                        ),
                        child: Text(
                          'cm'.tr(),
                          style: TextStyle(
                            fontSize: 14,
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal',
                          ),
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ],
        );
      },
    );
  }
}
