import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nxt/view/screens/talent_profile/screens/about/edit_widgets/edit_membership.dart';
import '../../../../../config/router/router.dart';
import '../../../../../data/models/user_models/users_profile_model.dart';
import '../../../../../utils/constants/resources.dart';
import '../../../../widgets/edit_column_single.dart';
import '../../../../widgets/error_state_widget.dart';
import '../../../../widgets/loading_state_widget.dart';
import '../../../edit_bio/edit_bio_screen.dart';
import '../../controller/profile_cubit.dart';
import 'about_widgets/about_educational_experiences.dart';
import 'about_widgets/imports.dart';
import 'about_widgets/about_look_widget.dart';
import 'about_widgets/social_media_row.dart';
import 'edit_widgets/edit_column.dart';
import 'edit_widgets/edit_interests.dart';
import 'edit_widgets/edit_skills.dart';
import 'edit_widgets/edit_categories.dart';

class ProfileAboutTab extends StatefulWidget {
  const ProfileAboutTab({Key? key}) : super(key: key);

  @override
  State<ProfileAboutTab> createState() => _ProfileAboutTabState();
}

class _ProfileAboutTabState extends State<ProfileAboutTab> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProfileCubit, ProfileState>(
      builder: (context, state) {
        final cubit = ProfileCubit.get(context);
        if (state is GetProfileFailed) {
          return ErrorStateWidget(
              hasRefresh: true,
              onRefresh: () async => await cubit.getUserData());
        } else if (state is GetProfileLoading) {
          return const LoadingStateWidget();
        } else {
          final userDataProfile =
              cubit.usersProfileModel ?? UsersProfileModel();

          return RefreshIndicator(
            onRefresh: () async => await cubit.init(),
            child: ListView(
              children: [
                EditColumnSingle(
                  header: 'bio'.tr(),
                  fallbackText: 'clickToAddBio'.tr(),
                  onPressed: () {
                    MagicRouter.navigateTo(
                      BlocProvider.value(
                          value: cubit..bioController,
                          child: const EditBioScreen()),
                    );
                  },
                  child: userDataProfile.bio?.isNotEmpty == true
                      ? Center(
                          child: Text(userDataProfile.bio ?? '',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontFamily:
                                      widget.isEn ? 'Montserrat' : 'Tajawal')),
                        )
                      : Center(
                          child: Text('clickToAddBio'.tr(),
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontFamily:
                                      widget.isEn ? 'Montserrat' : 'Tajawal')),
                        ),
                ),
                AboutLookWidget(
                  eyeColor: userDataProfile.eyeColor == ''
                      ? 'notSelected'.tr()
                      : userDataProfile.eyeColor ?? 'notSelected'.tr(),
                  eyeColorImg: userDataProfile.eyeColor ?? 'Black',
                  hairColor: userDataProfile.hairColor == ''
                      ? 'notSelected'.tr()
                      : userDataProfile.hairColor ?? 'notSelected'.tr(),
                  hairColorImg: userDataProfile.hairColor ?? 'Black',
                  skinColor: userDataProfile.skinColor == ''
                      ? 'notSelected'.tr()
                      : userDataProfile.skinColor ?? 'notSelected'.tr(),
                  skinColorImg: userDataProfile.skinColor ?? 'Black',
                ),
                const AboutEducationalExperiences(),
                Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Padding(
                          padding: const EdgeInsetsDirectional.only(start: 10.0),
                          child: Text(
                            'Union Membership',
                            style:
                            Theme.of(context).textTheme.titleLarge!.copyWith(fontFamily: widget.isEn ? 'BebasNeue' : 'Tajawal'),
                          ),
                        ),
                        TextButton(
                          style: TextButton.styleFrom(padding: EdgeInsets.zero),
                          onPressed: () async {
                            MagicRouter.navigateTo(
                                BlocProvider.value(
                                    value: cubit..getMembershipList(), child: const EditMembership()));
                          },
                          child: Text(
                          'edit'.tr(),
                            style: TextStyle(
                              fontSize: 14,
                              color: AppColors.semiBlack,
                              fontWeight: FontWeight.w900,
                              decoration: TextDecoration.underline,
                              fontFamily: widget.isEn ? 'Montserrat' : 'Tajawal',
                            ),
                          ),
                        ),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20.0),
                      child:Align(alignment: Alignment.topLeft,child: Text(cubit.memberModel?.title ?? '')),
                    ),
                    SizedBox(height: widget.height * 0.01),
                    Container(color: Colors.black, width: double.infinity, height: 1.5),
                    SizedBox(height: widget.height * 0.01),
                  ],
                ),
                EditColumn(
                  list: userDataProfile.specialSkills,
                  header: 'skills'.tr(),
                  fallbackText: 'clickToAddSkills'.tr(),
                  wrapList: userDataProfile.specialSkills!
                      .map((item) =>
                          EditItemChip(itemText: item.title.toString()))
                      .toList(),
                  onPressed: () {
                    MagicRouter.navigateTo(
                      BlocProvider.value(
                          value: cubit..getSkills(), child: const EditSkills()),
                    );
                  },
                ),
                EditColumn(
                  list: userDataProfile.talents,
                  header: 'Interests',
                  fallbackText: 'clickToAddTalents'.tr(),
                  wrapList: userDataProfile.talents!
                      .map((item) =>
                          EditItemChip(itemText: item.title.toString()))
                      .toList(),
                  onPressed: () {
                    MagicRouter.navigateTo(
                      BlocProvider.value(
                          value: cubit..getTalents(),
                          child: const EditInterest()),
                    );
                  },
                ),
                const SocialMediaRow(),



                SizedBox(height: widget.height * 0.025),
              ],
            ),
          );
        }
      },
    );
  }
}
