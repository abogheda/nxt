import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter_svg/svg.dart';
import '../../../../../../config/router/router.dart';
import '../../../../../../utils/constants/resources.dart';
import '../../../../../../utils/helpers/validators.dart';
import '../../../../../widgets/custom_text_field.dart';
import '../../../../navigation_and_appbar/import_widget.dart';
import '../../../controller/profile_cubit.dart';

class EditAboutLook extends StatefulWidget {
  final ProfileCubit profileCubit;
  const EditAboutLook({Key? key, required this.profileCubit}) : super(key: key);

  @override
  State<EditAboutLook> createState() => _EditAboutLookState();
}

class _EditAboutLookState extends State<EditAboutLook> {
  String? _eye;
  String? _skin;
  String? _hair;
  late ProfileCubit cubit;

  @override
  void initState() {
    cubit = widget.profileCubit;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider.value(
      value: widget.profileCubit,
      child: CustomScaffoldWidget(
        extendBodyBehindAppBar: false,
        isBackArrow: true,
        isThereLogo: true,
        isThereActions: true,
        backgroundColor: Colors.white,
        body: Stack(
          children: <Widget>[
            Positioned(
              top: 0,
              left: 0,
              right: 0,
              bottom: 0,
              child: ListView(
                children: [
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: widget.width * 0.03),
                    child: Center(
                      child: Text(
                        'look'.tr(),
                        style:
                        Theme.of(context).textTheme.titleLarge!.copyWith(fontFamily: widget.isEn ? 'BebasNeue' : 'Tajawal'),
                      ),
                    ),
                  ),
                  SizedBox(height: widget.height * 0.012),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      children: [
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.only(right: 8.0),
                            child: CustomTextField(
                              controller: cubit.weightController,
                              validator: Validators.weight,
                              textStyle: TextStyle(fontFamily: widget.isEn ? 'Montserrat' : 'Tajawal'),
                              suffix: Container(
                                height: widget.width * 0.13,
                                width: widget.width * 0.13,
                                alignment: Alignment.center,
                                decoration: const BoxDecoration(
                                  color: Colors.black,
                                  borderRadius: BorderRadius.only(bottomRight: Radius.circular(5.0),
                                      topRight: Radius.circular(5.0)),
                                ),
                                child: Text(
                                  'kg'.tr(),
                                  style: TextStyle(
                                    fontSize: 14,
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal',
                                  ),
                                ),
                              ),
                              // initialValue: cubit.usersProfileModel?.weight.toString(),
                              type: TextInputType.number,
                              hint: 'yourWeight'.tr(),
                              prefix: SvgPicture.asset('assets/images/weight.svg'),
                            ),
                          ),
                        ),
                        Expanded(
                          child: CustomTextField(
                            validator: Validators.height,
                            textStyle: TextStyle(fontFamily: widget.isEn ? 'Montserrat' : 'Tajawal'),
                            controller: cubit.heightController,
                            suffix: Container(
                              height: widget.width * 0.13,
                              width: widget.width * 0.13,
                              alignment: Alignment.center,
                              decoration: const BoxDecoration(
                                color: Colors.black,
                                borderRadius: BorderRadius.only(bottomRight: Radius.circular(5.0),topRight: Radius.circular(5.0)),
                              ),
                              child: Text(
                                'cm'.tr(),
                                style: TextStyle(
                                  fontSize: 14,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                  fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal',
                                ),
                              ),
                            ),
                            // initialValue: cubit.usersProfileModel?.height.toString(),
                            type: TextInputType.number,
                            hint: 'yourHeight'.tr(),
                            prefix: SvgPicture.asset('assets/images/height.svg'),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
                        child: Text(
                          'skinColor'.tr(),
                          style: Theme.of(context).textTheme.bodyText1!.copyWith(
                                fontWeight: FontWeight.bold,
                                fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal',
                              ),
                        ),
                      ),
                      Container(
                        width: widget.width,
                        height: widget.height * 0.12,
                        margin: const EdgeInsets.symmetric(vertical: 8),
                        child: ListView.builder(
                          shrinkWrap: true,
                          itemCount: AppConst.skinColors.length,
                          scrollDirection: Axis.horizontal,
                          padding: const EdgeInsets.symmetric(horizontal: 12.0),
                          itemBuilder: (context, index) {
                            var item = AppConst.skinColors[index];
                            return AnimatedContainer(
                              duration: const Duration(milliseconds: 200),
                              curve: Curves.easeInCubic,
                              padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 2),
                              decoration: BoxDecoration(
                                  border: Border.all(
                                    width: 1.5,
                                    color: _skin == item ? Colors.black : Colors.transparent,
                                  ),
                                  borderRadius: const BorderRadius.all(Radius.circular(6.0))),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Padding(
                                    padding: EdgeInsets.symmetric(vertical: widget.height * 0.003),
                                    child: GestureDetector(
                                      onTap: () => setState(() => _skin = item),
                                      child: Image.asset(AppImages.getSkinImage(item)),
                                    ),
                                  ),
                                  Text(
                                    item.toLowerCase().tr(),
                                    style: AppTextStyles.bold12.copyWith(
                                      color: AppColors.iconsColor,
                                      fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal',
                                    ),
                                  ),
                                ],
                              ),
                            );
                          },
                        ),
                      ),
                      SizedBox(height: widget.height * 0.015),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
                        child: Text(
                          'hairColor'.tr(),
                          style: Theme.of(context).textTheme.bodyText1!.copyWith(
                                fontWeight: FontWeight.bold,
                                fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal',
                              ),
                        ),
                      ),
                      Container(
                        width: widget.width,
                        height: widget.height * 0.12,
                        margin: const EdgeInsets.symmetric(vertical: 8),
                        child: ListView.builder(
                          shrinkWrap: true,
                          itemCount: AppConst.hairColors.length,
                          scrollDirection: Axis.horizontal,
                          padding: const EdgeInsets.symmetric(horizontal: 12.0),
                          itemBuilder: (context, index) {
                            var item = AppConst.hairColors[index];
                            return AnimatedContainer(
                              duration: const Duration(milliseconds: 200),
                              curve: Curves.easeInCubic,
                              padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 2),
                              decoration: BoxDecoration(
                                  border: Border.all(
                                    width: 1.5,
                                    color: _hair == item ? Colors.black : Colors.transparent,
                                  ),
                                  borderRadius: const BorderRadius.all(Radius.circular(6.0))),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Padding(
                                    padding: EdgeInsets.symmetric(vertical: widget.height * 0.003),
                                    child: GestureDetector(
                                      onTap: () {
                                        setState(() {
                                          _hair = item;
                                        });
                                      },
                                      child: Image.asset(AppImages.getHairImage(item)),
                                    ),
                                  ),
                                  Text(
                                    item.toLowerCase().tr(),
                                    style: AppTextStyles.bold12.copyWith(
                                      color: AppColors.iconsColor,
                                      fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal',
                                    ),
                                  ),
                                ],
                              ),
                            );
                          },
                        ),
                      ),
                      SizedBox(height: widget.height * 0.015),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
                        child: Text(
                          'eyeColor'.tr(),
                          style: Theme.of(context).textTheme.bodyText1!.copyWith(
                                fontWeight: FontWeight.bold,
                                fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal',
                              ),
                        ),
                      ),
                      Container(
                        width: widget.width,
                        height: widget.height * 0.12,
                        margin: const EdgeInsets.symmetric(vertical: 8),
                        child: ListView.builder(
                          shrinkWrap: true,
                          itemCount: AppConst.eyeColors.length,
                          scrollDirection: Axis.horizontal,
                          padding: const EdgeInsets.symmetric(horizontal: 12.0),
                          itemBuilder: (context, index) {
                            var item = AppConst.eyeColors[index];
                            return AnimatedContainer(
                              duration: const Duration(milliseconds: 200),
                              curve: Curves.easeInCubic,
                              padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 2),
                              decoration: BoxDecoration(
                                  border: Border.all(
                                    width: 1.5,
                                    color: _eye == item ? Colors.black : Colors.transparent,
                                  ),
                                  borderRadius: const BorderRadius.all(Radius.circular(6.0))),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Padding(
                                    padding: EdgeInsets.symmetric(vertical: widget.height * 0.003),
                                    child: GestureDetector(
                                      onTap: () {
                                        setState(() {
                                          _eye = item;
                                        });
                                      },
                                      child: Image.asset(AppImages.getEyeImage(item)),
                                    ),
                                  ),
                                  Text(
                                    item.toLowerCase().tr(),
                                    style: AppTextStyles.bold12.copyWith(
                                      color: AppColors.iconsColor,
                                      fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal',
                                    ),
                                  ),
                                ],
                              ),
                            );
                          },
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            Positioned(
              bottom: 0,
              left: 0,
              right: 0,
              child: Builder(
                builder: (ctx) {
                  return MaterialButton(
                    height: widget.height * 0.08,
                    minWidth: double.infinity,
                    color: Colors.black,
                    onPressed: (() async{
                      if (_eye == null && _hair == null && _skin == null && cubit.heightController.text.isEmpty &&
                          cubit.weightController.text.isEmpty) {
                        MagicRouter.pop();
                      } else {
                        ctx.read<ProfileCubit>().editLook(eyeColor: _eye, hairColor: _hair, skinColor: _skin);
                        await cubit.getUserData();
                        MagicRouter.pop();
                      }
                    }),
                    child: Text(
                      'saveEdits'.tr(),
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 18,
                        color: Colors.white,
                        fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal',
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
