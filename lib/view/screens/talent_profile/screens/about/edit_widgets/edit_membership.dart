import 'package:dotted_border/dotted_border.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nxt/data/models/user_models/education_model.dart';
import 'package:nxt/view/widgets/custom_drop_down_field.dart';
import '../../../../../../config/router/router.dart';
import '../../../../../../data/models/get_member_ship_model.dart';
import '../../../../../../utils/constants/app_colors.dart';
import '../../../../../../utils/constants/app_const.dart';
import '../../../../../../utils/constants/app_text_style.dart';

import '../../../../../../utils/helpers/picker_helper.dart';
import '../../../../navigation_and_appbar/import_widget.dart';
import '../../../controller/profile_cubit.dart';

class EditMembership extends StatefulWidget {
  const EditMembership({
    Key? key,
  }) : super(key: key);

  @override
  State<EditMembership> createState() => _EditMembershipState();
}

class _EditMembershipState extends State<EditMembership> {
  late ProfileCubit cubit;

  @override
  void initState() {
    cubit = context.read<ProfileCubit>();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider.value(
      value: cubit,
      child: CustomScaffoldWidget(
          backgroundColor: Colors.white,
          extendBodyBehindAppBar: false,
          isBackArrow: true,
          isInProfile: true,
          isLogoClickable: true,
          body: Stack(
            children: [
              Positioned(
                bottom: 0,
                left: 0,
                right: 0,
                top: 0,
                child: ListView(
                  padding:
                      EdgeInsets.symmetric(horizontal: widget.width * 0.03),
                  children: [
                    CustomDropDownTextField<String>(
                        items: List.generate(
                          cubit.memberShipList?.length ?? 0,
                          (index) => DropdownMenuItem(
                            value: cubit.memberShipList![index].id,
                            child: Text(
                                cubit.memberShipList![index].lang![0].title ??
                                    '',
                                style: TextStyle(
                                    fontFamily: AppConst.isEn
                                        ? 'Montserrat'
                                        : 'Tajawal')),
                          ),
                        ),
                        onChanged: (v) {
                          cubit.memberId = v;
                        }),
                    InkWell(
                      onTap: ()async{
                      await  cubit.pickMemberPdf();
                      },
                      child: DottedBorder(borderType: BorderType.RRect,
                          child: SizedBox(
                            height: 50,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children:  [
                                Image.asset('assets/images/pdf.png',height: 30,),
                                const Text(" +  Upload a Union Membership doc."),
                              ],
                            ),
                          )),
                    )
                  ],
                ),
              ),
              Positioned(
                bottom: 0,
                left: 0,
                right: 0,
                child: MaterialButton(
                  onPressed: () async {
                      await cubit.addMemberShip();
                      MagicRouter.pop();
                  },
                  height: widget.height * 0.08,
                  minWidth: double.infinity,
                  color: Colors.black,
                  child: Text(
                    "save".tr(),
                    style: TextStyle(
                      fontSize: 18,
                      color: Colors.white,
                      fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal',
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              )
            ],
          )),
    );
  }
}
