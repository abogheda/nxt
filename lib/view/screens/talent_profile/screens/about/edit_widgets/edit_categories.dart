import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../../../../utils/constants/app_colors.dart';
import '../../../../../../utils/constants/app_const.dart';
import '../../../../../../utils/constants/app_text_style.dart';
import '../../../../../../utils/helpers/popup_helper.dart';
import '../../../../../widgets/error_state_widget.dart';
import '../../../../../widgets/loading_state_widget.dart';
import '../../../../navigation_and_appbar/import_widget.dart';
import '../../../controller/profile_cubit.dart';

import '../../../../../../config/router/router.dart';
import '../../../../../widgets/app_loader.dart';
import '../../../../../widgets/no_state_widget.dart';
import 'edit_talents.dart';

class EditCategories extends StatefulWidget {
  const EditCategories({Key? key}) : super(key: key);

  @override
  State<EditCategories> createState() => _EditCategoriesState();
}

bool isSelected = false;

class _EditCategoriesState extends State<EditCategories> {
  @override
  Widget build(BuildContext context) {
    return CustomScaffoldWidget(
      backgroundColor: Colors.white,
      extendBodyBehindAppBar: false,
      isBackArrow: true,
      isInProfile: true,
      isLogoClickable: true,
      body: BlocBuilder<ProfileCubit, ProfileState>(
        builder: (context, state) {
          final cubit = ProfileCubit.get(context);
          if (state is GetCategoriesFailed) {
            return ErrorStateWidget(
              hasRefresh: true,
              onRefresh: () async {
                await cubit.getCategories();
              },
            );
          } else if (state is GetCategoriesLoading) {
            return const LoadingStateWidget();
          } else {
            final categories = cubit.categoriesList;
            return Stack(
              children: [
                Positioned(
                    right: 0,
                    left: 0,
                    bottom: 0,
                    top: 0,
                    child: categories == null || categories.isEmpty
                        ? Center(child: NoStateWidget())
                        : ListView(
                            padding: EdgeInsets.only(top: widget.height * 0.03, left: 6.0, right: 6.0),
                            children: [
                              Wrap(
                                spacing: 6,
                                runSpacing: 6,
                                children: categories.map(
                                  (item) {
                                    return InkWell(
                                      onTap: () {
                                        if (cubit.addedCategories.contains(item.id)) {
                                          cubit.addedCategories.remove(item.id);
                                        } else {
                                          cubit.addedCategories.add(item.id!);
                                        }
                                        cubit.addedCategories.toSet().toList();
                                        setState(() {});
                                      },
                                      borderRadius: const BorderRadius.all(Radius.circular(6.0)),
                                      child: Container(
                                        decoration: BoxDecoration(
                                            border: Border.all(color: AppColors.borderGreyColor),
                                            borderRadius: const BorderRadius.all(Radius.circular(6.0)),
                                            color: cubit.addedCategories.contains(item.id) ? Colors.black : null),
                                        padding: const EdgeInsets.all(12),
                                        child: Text(
                                          item.title == null ? "notAvailable".tr() : item.title!.toUpperCase(),
                                          style: AppTextStyles.bold14.copyWith(
                                            color: cubit.addedCategories.contains(item.id) ? Colors.white : null,
                                            fontFamily: widget.isEn ? 'Montserrat' : 'Tajawal',
                                          ),
                                        ),
                                      ),
                                    );
                                  },
                                ).toList(),
                              ),
                            ],
                          )),
                Positioned(
                  bottom: 0,
                  left: 0,
                  right: 0,
                  child: MaterialButton(
                    height: widget.height * 0.08,
                    minWidth: double.infinity,
                    color: Colors.black,
                    onPressed: () async {
                      if (cubit.addedCategories.isEmpty) {
                        PopupHelper.showBasicSnack(msg: 'selectOneCategory'.tr(), color: Colors.red);
                        return;
                      }
                      await MagicRouter.navigateAndReplace(
                        BlocProvider.value(value: cubit..getTalents(), child: const EditTalents()),
                      );
                    },
                    child: state is GetProfileLoading || state is UpdateTalentsLoading
                        ? const AppLoader(color: Colors.white)
                        : Text(
                            'chooseTalents'.tr().toUpperCase(),
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 18,
                              color: Colors.white,
                              fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal',
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                  ),
                )
              ],
            );
          }
        },
      ),
    );
  }
}
