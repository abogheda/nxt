import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import '../../../../../../utils/constants/resources.dart';

class EditColumn extends StatefulWidget {
  const EditColumn({
    Key? key,
    required this.onPressed,
    required this.list,
    required this.header,
    required this.fallbackText,
    required this.wrapList,
    this.showEditButton = true,
  }) : super(key: key);
  final Function() onPressed;
  final List? list;
  final String header;
  final String fallbackText;
  final List<Widget> wrapList;
  final bool showEditButton;
  @override
  State<EditColumn> createState() => _EditColumnState();
}

class _EditColumnState extends State<EditColumn> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Padding(
              padding: const EdgeInsetsDirectional.only(start: 10.0),
              child: Text(widget.header,
                  style: Theme.of(context)
                      .textTheme
                      .titleLarge!
                      .copyWith(fontFamily: widget.isEn ? 'BebasNeue' : 'Tajawal')),
            ),
            widget.showEditButton
                ? TextButton(
                    onPressed: widget.onPressed,
                    style: TextButton.styleFrom(padding: EdgeInsets.zero),
                    child: Text(
                      'edit'.tr(),
                      style: TextStyle(
                        color: AppColors.semiBlack,
                        decoration: TextDecoration.underline,
                        fontFamily: widget.isEn ? 'Montserrat' : 'Tajawal',
                        fontSize: 14,
                        fontWeight: FontWeight.w900,
                      ),
                    ),
                  )
                : Column(
                    children: <Widget>[
                      Opacity(
                        opacity: 0,
                        child: TextButton(
                          onPressed: widget.onPressed,
                          style: TextButton.styleFrom(padding: EdgeInsets.zero),
                          child: Text(
                            'edit'.tr(),
                            style: TextStyle(
                              color: AppColors.semiBlack,
                              decoration: TextDecoration.underline,
                              fontFamily: widget.isEn ? 'Montserrat' : 'Tajawal',
                              fontSize: 14,
                              fontWeight: FontWeight.w900,
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
          ],
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 6.0),
          child: widget.list == null || widget.list!.isEmpty
              ? Center(
                  child: Text(widget.fallbackText,
                      textAlign: TextAlign.center,
                      style: TextStyle(fontFamily: widget.isEn ? 'Montserrat' : 'Tajawal')))
              : Wrap(spacing: 6, runSpacing: 6, children: widget.wrapList),
        ),
        SizedBox(height: widget.height * 0.01),
        Container(color: Colors.black, width: double.infinity, height: 1.5),
        SizedBox(height: widget.height * 0.01),
      ],
    );
  }
}

class EditItemChip extends StatelessWidget {
  const EditItemChip({Key? key, required this.itemText}) : super(key: key);
  final String itemText;
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        border: Border.all(color: AppColors.borderGreyColor),
        borderRadius: const BorderRadius.all(Radius.circular(6.0)),
        color: Colors.black,
      ),
      padding: const EdgeInsets.all(12),
      child: Text(
        itemText,
        style: TextStyle(
          color: Colors.white,
          fontSize: 14,
          fontWeight: FontWeight.bold,
          fontFamily: isEn ? 'Montserrat' : 'Tajawal',
        ),
      ),
    );
  }
}
