import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:nxt/data/models/user_models/education_model.dart';
import 'package:nxt/data/models/user_models/social_media.dart';
import '../../../../../../config/router/router.dart';
import '../../../../../../data/models/user_models/job_experience_model.dart';
import '../../../../../../utils/constants/app_colors.dart';
import '../../../../../../utils/constants/app_const.dart';
import '../../../../../../utils/constants/app_text_style.dart';
import '../../../../../../utils/helpers/picker_helper.dart';
import '../../../../../../utils/helpers/popup_helper.dart';
import '../../../../../../utils/helpers/validators.dart';
import '../../../../../widgets/custom_drop_down_field.dart';
import '../../../../../widgets/custom_text_field.dart';
import '../../../../navigation_and_appbar/import_widget.dart';
import '../../../controller/profile_cubit.dart';

import '../../../../../widgets/app_loader.dart';

class EditSocialMediaProfile extends StatefulWidget {
  final ProfileCubit profileCubit;
  const EditSocialMediaProfile({Key? key, required this.profileCubit})
      : super(key: key);

  @override
  State<EditSocialMediaProfile> createState() =>
      _EditSocialMediaProfileState();
}

class _EditSocialMediaProfileState extends State<EditSocialMediaProfile> {
  final formKey = GlobalKey<FormState>();
  late ProfileCubit cubit;

  @override
  void initState() {
    cubit = widget.profileCubit;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider.value(value: widget.profileCubit,child: CustomScaffoldWidget(
        backgroundColor: Colors.white,
        extendBodyBehindAppBar: false,
        isBackArrow: true,
        isInProfile: true,
        isLogoClickable: true,
        body: Stack(
          children: [
            Positioned(
              bottom: 0,
              left: 0,
              right: 0,
              top: 0,
              child: Form(
                key: formKey,
                child: Column(
                  children: [
                    SocialMediaTextFieldItem(image: 'assets/images/facebook_social.svg',
                        hint: 'facebook',controller: cubit.facebookController),
                    SocialMediaTextFieldItem(image: 'assets/images/instagram_social.svg',
                        hint: 'instagram',controller: cubit.instagramController),
                    SocialMediaTextFieldItem(image: 'assets/images/youtube_social.svg',
                        hint: 'youtube',controller: cubit.youtubeController),
                    SocialMediaTextFieldItem(image: 'assets/images/twitter_social.svg',
                        hint: 'twitter',controller: cubit.twitterController),
                    SocialMediaTextFieldItem(image: 'assets/images/tiktok_social.svg',
                        hint: 'tiktok',controller: cubit.tiktokController),
                  ],
                ),
              ),
            ),
            Positioned(
              bottom: 0,
              left: 0,
              right: 0,
              child: MaterialButton(
                onPressed: () async {
                  if (formKey.currentState!.validate()) {
                    await cubit.editSocialMediaProfile();
                    await cubit.getUserData();
                    MagicRouter.pop();
                  }
                },
                height: widget.height * 0.08,
                minWidth: double.infinity,
                color: Colors.black,
                child: Text(
                  "save".tr(),
                  style: TextStyle(
                    fontSize: 18,
                    color: Colors.white,
                    fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal',
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            )
          ],
        )),);
  }
}
class SocialMediaTextFieldItem extends StatelessWidget {
  final String image;
  final String hint;
  final TextEditingController controller;
  const SocialMediaTextFieldItem({Key? key, required this.image, required this.hint, required this.controller}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
         children: [
           SvgPicture.asset(image, width: 35),
           const SizedBox(
             width: 10,
           ),
           Expanded(
             flex: 2,
             child: CustomTextField(
               hint: hint,
               // validator: Valid,
               type: TextInputType.text,
               controller: controller,
               onFieldSubmitted: (_) async {
                 if(controller.text.isNotEmpty && controller.text.contains("https://")){
                   context.read<ProfileCubit>().addedSocialMedia.add(SocialMedia(name: hint,link: controller.text));
                 }
               },
             ),
           )
         ],
      ),
    );
  }
}

