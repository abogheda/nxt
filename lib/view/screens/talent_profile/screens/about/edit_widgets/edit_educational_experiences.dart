import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:nxt/data/models/user_models/education_model.dart';
import '../../../../../../config/router/router.dart';
import '../../../../../../data/models/user_models/job_experience_model.dart';
import '../../../../../../utils/constants/app_colors.dart';
import '../../../../../../utils/constants/app_const.dart';
import '../../../../../../utils/constants/app_text_style.dart';
import '../../../../../../utils/helpers/picker_helper.dart';
import '../../../../../../utils/helpers/popup_helper.dart';
import '../../../../../../utils/helpers/validators.dart';
import '../../../../../widgets/custom_drop_down_field.dart';
import '../../../../../widgets/custom_text_field.dart';
import '../../../../navigation_and_appbar/import_widget.dart';
import '../../../controller/profile_cubit.dart';

import '../../../../../widgets/app_loader.dart';

class ExperienceExperiencesEdit extends StatefulWidget {
  final ProfileCubit profileCubit;
  const ExperienceExperiencesEdit({Key? key, required this.profileCubit})
      : super(key: key);

  @override
  State<ExperienceExperiencesEdit> createState() =>
      _ExperienceExperiencesEditState();
}

class _ExperienceExperiencesEditState extends State<ExperienceExperiencesEdit> {
  final formKey = GlobalKey<FormState>();
  late ProfileCubit cubit;

  @override
  void initState() {
    cubit = widget.profileCubit;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider.value(value: widget.profileCubit,child: CustomScaffoldWidget(
        backgroundColor: Colors.white,
        extendBodyBehindAppBar: false,
        isBackArrow: true,
        isInProfile: true,
        isLogoClickable: true,
        body: Stack(
          children: [
            Positioned(
              bottom: 0,
              left: 0,
              right: 0,
              top: 0,
              child: ListView(
                padding: EdgeInsets.symmetric(horizontal: widget.width * 0.03),
                children: [
                  Form(
                    key: formKey,
                    child: ListView.builder(
                      shrinkWrap: true,
                      physics: const ClampingScrollPhysics(),
                      itemCount: cubit.educationExperiences!.length,
                      itemBuilder: (context, index) {
                        final item = cubit.educationExperiences![index];
                        return Container(
                          key: ValueKey(cubit.educationExperiences!.indexOf(item)),
                          decoration: BoxDecoration(
                            border: Border.all(width: 1),
                            borderRadius:
                            const BorderRadius.all(Radius.circular(6.0)),
                          ),
                          margin: const EdgeInsets.only(bottom: 8),
                          padding: const EdgeInsets.only(
                              bottom: 12, left: 12, right: 12),
                          child: Column(
                            children: [
                              Align(
                                alignment: Alignment.topRight,
                                child: IconButton(
                                  onPressed: () {
                                    setState(() {
                                      cubit.educationExperiences!.removeAt(index);
                                    });
                                  },
                                  padding: EdgeInsets.zero,
                                  icon: const Icon(Icons.clear),
                                ),
                              ),
                              CustomDropDownTextField<String>(
                                key: UniqueKey(),
                                value: cubit.education,
                                hint: "Education",
                                hintTextStyle: Theme.of(context)
                                    .textTheme
                                    .bodyText1!
                                    .copyWith(fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal', color: AppColors.iconsColor),
                                prefix: Padding(padding: const EdgeInsets.all(4), child: SvgPicture.asset('assets/images/gender.svg')),
                                items: [
                                  DropdownMenuItem(
                                    value: '1',
                                    child: Text('General Education',
                                        style: TextStyle(fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal')),
                                  ),
                                  DropdownMenuItem(
                                    value: 'Course',
                                    child: Text('Course', style: TextStyle(fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal')),
                                  )
                                ],
                                onChanged: (value) {
                                  cubit.education = value!.capitalize;
                                  print("object : ${cubit.education}");
                                },
                                validator: (value) {
                                  if (value == null) {
                                    return 'empty_field'.tr();
                                  } else if (value.isEmpty) {
                                    return 'empty_field'.tr();
                                  } else {
                                    return null;
                                  }
                                },
                              ),
                              const SizedBox(height: 12),
                              CustomTextField(
                                hint: 'roleDescription'.tr(),
                                initialValue: item.courseName,
                                validator: Validators.generalField,
                                height: 56,
                                onChange: (val) {
                                  item.courseName = val;
                                  formKey.currentState!.save();
                                },
                              ),
                              const SizedBox(height: 8),
                              CustomTextField(
                                hint: 'roleDescription'.tr(),
                                initialValue: item.courseLocation,
                                validator: Validators.generalField,
                                height: 56,
                                onChange: (val) {
                                  item.courseLocation = val;
                                  formKey.currentState!.save();
                                },
                              ),
                            ],
                          ),
                        );
                      },
                    ),
                  ),
                  TextButton.icon(
                    onPressed: () {
                      cubit.educationExperiences!.add(EducationModel());
                      setState(() {});
                    },
                    icon: const Icon(
                      Icons.add_box_rounded,
                      color: Colors.black,
                    ),
                    label: Text(
                      'addMore'.tr(),
                      style: AppTextStyles.medium14
                          .copyWith(color: AppColors.greyTxtColor),
                    ),
                  ),
                  SizedBox(height: widget.height * 0.1),
                ],
              ),
            ),
            Positioned(
              bottom: 0,
              left: 0,
              right: 0,
              child: MaterialButton(
                onPressed: () async {
                  if (formKey.currentState!.validate()) {
                    await cubit.editEducationExperiences(
                        educationList: cubit.educationExperiences);
                    MagicRouter.pop();
                  }
                },
                height: widget.height * 0.08,
                minWidth: double.infinity,
                color: Colors.black,
                child: Text(
                  "save".tr(),
                  style: TextStyle(
                    fontSize: 18,
                    color: Colors.white,
                    fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal',
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            )
          ],
        )),);
  }
}
