import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:nxt/view/widgets/custom_button.dart';
import '../../../../../../config/router/router.dart';
import '../../../../../../data/models/talent_model.dart';
import '../../../../../../utils/constants/app_colors.dart';
import '../../../../../../utils/constants/app_const.dart';
import '../../../../../../utils/constants/app_text_style.dart';
import '../../../../../../utils/helpers/validators.dart';
import '../../../../../widgets/app_loader.dart';
import '../../../../../widgets/custom_text_field.dart';
import '../../../../navigation_and_appbar/import_widget.dart';
import '../../../controller/profile_cubit.dart';

class EditInterest extends StatefulWidget {
  const EditInterest({Key? key}) : super(key: key);

  @override
  State<EditInterest> createState() => _EditInterestState();
}

class _EditInterestState extends State<EditInterest> {
  final TextEditingController talentController = TextEditingController();
  final formKey = GlobalKey<FormState>();
  @override
  void dispose() {
    talentController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return CustomScaffoldWidget(
      backgroundColor: Colors.white,
      extendBodyBehindAppBar: false,
      isBackArrow: true,
      isInProfile: true,
      isLogoClickable: true,
      body: BlocBuilder<ProfileCubit, ProfileState>(
        builder: (context, state) {
          final cubit = ProfileCubit.get(context);
          final talents = cubit.myTalents ?? [];
          return Stack(
            children: [
              Positioned(
                right: 0,
                left: 0,
                bottom: 0,
                top: 0,
                child: ListView(
                  padding: EdgeInsets.only(
                      top: widget.height * 0.03, left: 6.0, right: 6.0),
                  children: [
                    SizedBox(height: widget.height * 0.01),
                    // Center(
                    //     child: Text('pressEnterToAddSkill'.tr(),
                    //         style: TextStyle(
                    //             fontFamily:
                    //             widget.isEn ? 'Montserrat' : 'Tajawal'))),
                    SizedBox(height: widget.height * 0.01),
                    Form(
                      key: formKey,
                      child: Row(
                        children: [
                          Expanded(
                            flex: 2,
                            child: Autocomplete<TalentModel>(
                              displayStringForOption: (TalentModel talent) => talent.title ?? '',
                              optionsBuilder: (TextEditingValue textEditingValue)
                              {
                                return cubit.talentsList!
                                    .where((TalentModel talent) => talent.title!.toLowerCase()
                                    .startsWith(textEditingValue.text.toLowerCase())
                                )
                                    .toList();
                              },
                              onSelected: (item) {
                                cubit.myTalents?.add(TalentModel(
                                    id: item.id,
                                    title: item.title));
                                cubit.addedTalents.add(item.id?? "");
                                setState(() {});

                              },
                              optionsViewBuilder: (
                                  BuildContext context,
                                  AutocompleteOnSelected<TalentModel> onSelected,
                                  Iterable<TalentModel> options
                                  ) {
                                return Align(
                                  alignment: Alignment.topLeft,
                                  child: Material(
                                    child: Container(
                                      width: 200,
                                      color: AppColors.semiBlack,
                                      child: ListView.builder(
                                        padding: const EdgeInsets.all(10.0),
                                        itemCount: options.length,
                                        itemBuilder: (BuildContext context, int index) {
                                          final TalentModel option = options.elementAt(index);

                                          return GestureDetector(
                                            onTap: () {
                                              onSelected(option);
                                            },
                                            child: ListTile(
                                              title: Text(option.title!, style: const TextStyle(color: Colors.white)),
                                            ),
                                          );
                                        },
                                      ),
                                    ),
                                  ),
                                );
                              },
                              fieldViewBuilder: (
                                  BuildContext context,
                                  TextEditingController fieldTextEditingController,
                                  FocusNode fieldFocusNode,
                                  VoidCallback onFieldSubmitted
                                  ) {
                                return CustomTextField(
                                  hint: 'addSkill'.tr(),
                                  textStyle: TextStyle(fontFamily: widget.isEn ? 'Montserrat' : 'Tajawal'),
                                  controller: fieldTextEditingController,
                                  validator: Validators.generalField,
                                  type: TextInputType.text,
                                  focusNode: fieldFocusNode,
                                  onFieldSubmitted: (value) {
                                    if (formKey.currentState!.validate()) {
                                      cubit.talent = value;
                                      // cubit.addedSkills.add(value);
                                      setState(() {});
                                    }
                                  },
                                );
                              },

                            ),
                          ),
                          SizedBox(
                            width: 20.w,
                          ),
                          Expanded(
                              flex: 1,
                              child: CustomButton(
                                onTap: () {
                                  cubit.talentsList?.forEach((element) {
                                    if (element.title == talentController.text) {
                                      cubit.addedTalents.add(element.id!);
                                      // cubit.addedSkills.add(element.title!);
                                      cubit.talentsList?.add(TalentModel(
                                          id: element.id,
                                          title: element.title));
                                      talentController.clear();
                                      setState(() {});
                                    }
                                  });
                                },
                                height: 47,
                                child: const Text("ADD"),
                              )),
                        ],
                      ),
                    ),
                    SizedBox(height: widget.height * 0.01),
                    Wrap(
                      spacing: 6,
                      runSpacing: 6,
                      children: talents.map(
                            (item) {
                          return InkWell(
                            onTap: () async {
                              if (cubit.myTalents!.contains(item)) {
                                cubit.myTalents!.remove(item);
                              }
                              setState(() {});
                            },
                            borderRadius:
                            const BorderRadius.all(Radius.circular(6.0)),
                            child: Container(
                              decoration: BoxDecoration(
                                  border: Border.all(
                                      color: AppColors.borderGreyColor),
                                  borderRadius: const BorderRadius.all(
                                      Radius.circular(6.0)),
                                  color: cubit.myTalents!.contains(item)
                                      ? Colors.black
                                      : null),
                              padding: const EdgeInsets.all(12),
                              child: Row(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  Flexible(
                                    child: Text(
                                      item.title ?? '',
                                      style: AppTextStyles.bold14.copyWith(
                                        color: cubit.myTalents!.contains(item)
                                            ? Colors.white
                                            : null,
                                        fontFamily: widget.isEn
                                            ? 'Montserrat'
                                            : 'Tajawal',
                                      ),
                                    ),
                                  ),
                                  const SizedBox(width: 4),
                                  const Icon(Icons.clear,
                                      color: Colors.white, size: 16.0),
                                ],
                              ),
                            ),
                          );
                        },
                      ).toList(),
                    ),
                    SizedBox(
                        height: widget.height * 0.1 +
                            MediaQuery.of(context).viewInsets.bottom),
                  ],
                ),
              ),
              Positioned(
                bottom: 0,
                left: 0,
                right: 0,
                child: MaterialButton(
                  height: widget.height * 0.08,
                  minWidth: double.infinity,
                  color: Colors.black,
                  onPressed: () async {
                    await cubit.editTalents();
                    await cubit.getUserData();
                    MagicRouter.pop();
                  },
                  child:
                  state is GetProfileLoading || state is UpdateSkillLoading
                      ? const AppLoader(color: Colors.white)
                      : Text(
                    'save'.tr().toUpperCase(),
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 18,
                      color: Colors.white,
                      fontFamily:
                      AppConst.isEn ? 'Montserrat' : 'Tajawal',
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              )
            ],
          );
        },
      ),
    );
  }
}
