import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'profile_reel_details.dart';

import '../../../../../../data/models/profile_reel_model.dart';
import 'data/profile_reel_video_player.dart';

class ProfileReelVideoPage extends StatefulWidget {
  final int videoIndex;
  final bool showDelete;
  final String? userName;
  final String? userAvatar;
  final List<ProfileReelModel> reels;
  const ProfileReelVideoPage({
    Key? key,
    required this.reels,
    required this.videoIndex,
    this.showDelete = true,
    this.userName,
    this.userAvatar,
  }) : super(key: key);

  @override
  State<ProfileReelVideoPage> createState() => _ProfileReelVideoPageState();
}

class _ProfileReelVideoPageState extends State<ProfileReelVideoPage> {
  late PageController controller;

  @override
  void initState() {
    super.initState();
    controller = PageController(initialPage: widget.videoIndex);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      backgroundColor: Colors.transparent,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        foregroundColor: Colors.white,
        systemOverlayStyle: const SystemUiOverlayStyle(
            statusBarColor: Colors.black,
            systemStatusBarContrastEnforced: true,
            statusBarIconBrightness: Brightness.dark),
      ),
      body: PageView.builder(
        controller: controller,
        scrollDirection: Axis.vertical,
        itemCount: widget.reels.length,
        itemBuilder: (context, index) {
          return Stack(
            children: [
              ProfileReelVideoPlayer(reelVideo: widget.reels[index].media!),
              ProfileReelDetails(
                reelId: widget.reels[index].id!,
                showDelete: widget.showDelete,
                userName: widget.userName,
                userAvatar: widget.userAvatar,
              )
            ],
          );
        },
      ),
    );
  }
}
