import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import '../../../../../../data/service/hive_services.dart';
import '../../../../../../utils/constants/app_const.dart';

class ChallengeAudioPlayer extends StatefulWidget {
  const ChallengeAudioPlayer({Key? key, required this.challengeMedia}) : super(key: key);
  final String challengeMedia;

  @override
  State<ChallengeAudioPlayer> createState() => _ChallengeAudioPlayerState();
}

class _ChallengeAudioPlayerState extends State<ChallengeAudioPlayer> {
  final audioPlayer = AudioPlayer()..setReleaseMode(ReleaseMode.loop);
  bool isPlaying = false;

  Future setAudio() async {
    audioPlayer.setSourceUrl(widget.challengeMedia).then((value) => audioPlayer.resume());
  }

  @override
  void initState() {
    super.initState();
    setAudio();
    audioPlayer.onPlayerStateChanged.listen((state) => setState(() => isPlaying = state == PlayerState.playing));
  }

  @override
  void dispose() {
    audioPlayer.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () async {
        if (isPlaying) {
          await audioPlayer.pause();
        } else {
          await audioPlayer.resume();
        }
      },
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: widget.width * 0.03),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Stack(
              alignment: Alignment.center,
              children: <Widget>[
                Lottie.asset('assets/images/circular_audio.json', fit: BoxFit.cover, animate: isPlaying),
                SizedBox(
                  width: widget.width * 0.3,
                  height: widget.width * 0.3,
                  child: Image.asset(
                    HiveHelper.isEgypt ? 'assets/images/logo_fab_white.png' : 'assets/images/splash.png',
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
