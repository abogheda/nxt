import 'package:flutter/material.dart';
import 'challenge_audio_player.dart';
import 'challenge_video_player.dart';

class ChallengeMediaPlayer extends StatefulWidget {
  final String challengeMedia;
  final bool isVideo;
  const ChallengeMediaPlayer({
    Key? key,
    required this.challengeMedia,
    required this.isVideo,
  }) : super(key: key);

  @override
  State<ChallengeMediaPlayer> createState() => _ChallengeMediaPlayerState();
}

class _ChallengeMediaPlayerState extends State<ChallengeMediaPlayer> {
  @override
  Widget build(BuildContext context) {
    return widget.isVideo
        ? ChallengeVideoPlayer(challengeMedia: widget.challengeMedia)
        : ChallengeAudioPlayer(challengeMedia: widget.challengeMedia);
  }
}
