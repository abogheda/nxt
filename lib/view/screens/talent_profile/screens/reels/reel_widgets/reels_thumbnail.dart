import 'dart:typed_data';

import '../../../../../../config/router/router.dart';
import '../../../../../widgets/app_loader.dart';
import 'package:video_thumbnail/video_thumbnail.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../../../../utils/constants/app_colors.dart';
import '../../../controller/profile_cubit.dart';
import 'profile_reel_video_page.dart';

import '../../../../../../data/models/profile_reel_model.dart';

class ReelsThumbnail extends StatefulWidget {
  final String? reelThumbnail;
  final int? videoIndex;
  final List<ProfileReelModel>? reels;
  final bool isAudio;
  final bool showDelete;
  final String? userName;
  final String? userAvatar;
  final Function()? onPressed;
  final Widget? widgetOnThumbnail;
  const ReelsThumbnail({
    Key? key,
    this.reelThumbnail,
    this.videoIndex,
    this.reels,
    this.showDelete = true,
    this.isAudio = false,
    this.userName,
    this.userAvatar,
    this.onPressed,
    this.widgetOnThumbnail,
  }) : super(key: key);

  @override
  State<ReelsThumbnail> createState() => _ReelsThumbnailState();
}

class _ReelsThumbnailState extends State<ReelsThumbnail> {
  Uint8List? uint8list;
  Future init() async {
    uint8list = await VideoThumbnail.thumbnailData(
      timeMs: 10,
      quality: 50,
      video: widget.reelThumbnail!,
      imageFormat: ImageFormat.WEBP,
    );
    if (mounted) {
      setState(() {});
    }
  }

  @override
  void initState() {
    super.initState();
    if (!widget.isAudio) init();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(color: AppColors.iconsColor),
      child: InkWell(
        onTap: widget.onPressed ??
            () {
              MagicRouter.navigateTo(
                BlocProvider(
                  create: (context) => ProfileCubit(),
                  child: ProfileReelVideoPage(
                    reels: widget.reels!,
                    showDelete: widget.showDelete,
                    videoIndex: widget.videoIndex!,
                    userName: widget.userName,
                    userAvatar: widget.userAvatar,
                  ),
                ),
              );
            },
        child: widget.isAudio
            ? Container(
                decoration: const BoxDecoration(color: AppColors.iconsColor),
                child: const Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Icon(Icons.music_note, size: 48),
                ))
            : uint8list == null
                ? const Padding(padding: EdgeInsets.all(8.0), child: AppLoader())
                : Stack(
                    children: [
                      Positioned(
                          top: 0, bottom: 0, right: 0, left: 0, child: Image.memory(uint8list!, fit: BoxFit.cover)),
                      Positioned(
                        top: 0,
                        bottom: 0,
                        right: 0,
                        left: 0,
                        child: widget.widgetOnThumbnail ?? const SizedBox(),
                      )
                    ],
                  ),
      ),
    );
  }
}
