import 'reel_user.dart';

class Video {
  final User postedBy;
  final String caption;
  late int likes;
  late int saves;

  Video(
    this.postedBy,
    this.caption,
    this.likes,
    this.saves,
  );
}
