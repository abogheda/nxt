import 'package:chewie/chewie.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:video_player/video_player.dart';

import '../../../../../../../utils/constants/app_const.dart';
import '../../../../../../../utils/constants/app_text_style.dart';
import '../../../../../../widgets/app_loader.dart';

class ProfileReelVideoPlayer extends StatefulWidget {
  final String reelVideo;

  const ProfileReelVideoPlayer({Key? key, required this.reelVideo}) : super(key: key);

  @override
  State<ProfileReelVideoPlayer> createState() => _ProfileReelVideoPlayerState();
}

class _ProfileReelVideoPlayerState extends State<ProfileReelVideoPlayer> {
  ChewieController? chewieController;
  VideoPlayerController? videoPlayerController;
  @override
  void initState() {
    super.initState();
    initPlayer();
  }

  void initPlayer() async {
    videoPlayerController = VideoPlayerController.network(widget.reelVideo);
    await videoPlayerController!.initialize();
    chewieController = ChewieController(
      autoPlay: true,
      looping: true,
      zoomAndPan: false,
      allowMuting: false,
      showOptions: false,
      showControls: true,
      autoInitialize: true,
      allowFullScreen: false,
      fullScreenByDefault: false,
      showControlsOnInitialize: false,
      allowPlaybackSpeedChanging: false,
      placeholder: const AppLoader(),
      videoPlayerController: videoPlayerController!,
      materialProgressColors: ChewieProgressColors(
          backgroundColor: Colors.transparent,
          bufferedColor: Colors.transparent,
          playedColor: Colors.transparent,
          handleColor: Colors.transparent),
      errorBuilder: (context, url) {
        return Stack(
          alignment: Alignment.bottomCenter,
          children: [
            FadeInImage.memoryNetwork(
              image: 'https://static.thenounproject.com/png/504708-200.png',
              placeholder: kTransparentImage,
            ),
            Text(
              "errorTry".tr(),
              style: AppTextStyles.black14
                  .copyWith(fontFamily: widget.isEn ? 'Montserrat' : 'Tajawal', color: Colors.white),
            ),
          ],
        );
      },
    );
    setState(() {});
  }

  @override
  void dispose() {
    videoPlayerController!.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return chewieController == null
        ? Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const AppLoader(color: Colors.white),
              const SizedBox(height: 10),
              Text(
                'pleaseWaitLoading'.tr(),
                textAlign: TextAlign.center,
                style: AppTextStyles.black18
                    .copyWith(fontFamily: widget.isEn ? 'Montserrat' : 'Tajawal', color: Colors.white),
              ),
            ],
          )
        : Chewie(controller: chewieController!);
  }
}
