import 'package:flutter/material.dart';
import 'package:chewie/chewie.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:transparent_image/transparent_image.dart';
import '../../../../../../utils/constants/app_const.dart';
import '../../../../../../utils/constants/app_text_style.dart';
import '../../../../../widgets/app_loader.dart';
import 'package:video_player/video_player.dart';

class ChallengeVideoPlayer extends StatefulWidget {
  const ChallengeVideoPlayer({Key? key, required this.challengeMedia}) : super(key: key);
  final String challengeMedia;

  @override
  State<ChallengeVideoPlayer> createState() => _ChallengeVideoPlayerState();
}

class _ChallengeVideoPlayerState extends State<ChallengeVideoPlayer> {
  ChewieController? chewieController;
  VideoPlayerController? videoPlayerController;
  @override
  void initState() {
    initVideoPlayer();
    super.initState();
  }

  void initVideoPlayer() async {
    videoPlayerController = VideoPlayerController.network(widget.challengeMedia);
    await videoPlayerController!.initialize();
    chewieController = ChewieController(
      videoPlayerController: videoPlayerController!,
      autoPlay: true,
      autoInitialize: true,
      looping: true,
      fullScreenByDefault: false,
      allowPlaybackSpeedChanging: false,
      allowFullScreen: false,
      showControls: true,
      showControlsOnInitialize: false,
      showOptions: false,
      zoomAndPan: false,
      placeholder: const AppLoader(),
      materialProgressColors: ChewieProgressColors(
          backgroundColor: Colors.transparent,
          bufferedColor: Colors.transparent,
          playedColor: Colors.transparent,
          handleColor: Colors.transparent),
      errorBuilder: (context, url) {
        return Stack(
          alignment: Alignment.bottomCenter,
          children: [
            FadeInImage.memoryNetwork(
              image: 'https://static.thenounproject.com/png/504708-200.png',
              placeholder: kTransparentImage,
            ),
            Text(
              "errorTry".tr(),
              style: AppTextStyles.black14
                  .copyWith(fontFamily: widget.isEn ? 'Montserrat' : 'Tajawal', color: Colors.white),
            ),
          ],
        );
      },
    );
    setState(() {});
  }

  @override
  void dispose() {
    videoPlayerController!.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return chewieController == null
        ? Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const AppLoader(color: Colors.white),
              const SizedBox(height: 10),
              Text(
                'pleaseWaitLoading'.tr(),
                textAlign: TextAlign.center,
                style: AppTextStyles.black18
                    .copyWith(fontFamily: widget.isEn ? 'Montserrat' : 'Tajawal', color: Colors.white),
              ),
            ],
          )
        : Chewie(controller: chewieController!);
  }
}
