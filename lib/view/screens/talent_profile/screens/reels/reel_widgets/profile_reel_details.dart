import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../../../../utils/helpers/popup_helper.dart';
import '../../../../../../utils/helpers/utils.dart';
import 'package:transparent_image/transparent_image.dart';

import '../../../../../../config/router/router.dart';
import '../../../../../../data/service/hive_services.dart';
import '../../../../../../utils/constants/resources.dart';
import '../../../../../widgets/delete_dialog.dart';
import '../../../controller/profile_cubit.dart';
import '../../../talent_profile_screen.dart';

final textStyle = Theme.of(MagicRouter.currentContext!)
    .textTheme
    .titleSmall!
    .copyWith(fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal', fontWeight: FontWeight.bold, color: Colors.white);

class ProfileReelDetails extends StatelessWidget {
  const ProfileReelDetails({Key? key, required this.reelId, this.showDelete = true, this.userName, this.userAvatar})
      : super(key: key);
  final String reelId;
  final bool showDelete;
  final String? userName;
  final String? userAvatar;
  @override
  Widget build(BuildContext context) {
    return BlocConsumer<ProfileCubit, ProfileState>(
      listener: (context, state) async {
        if (state is ReportReelSuccess) {
          Navigator.pop(context);
          if (state.reportModel.statusCode == 404 || state.reportModel.message != null) {
            PopupHelper.showBasicSnack(msg: 'reelAlreadyReported'.tr());
            return;
          }
          PopupHelper.showBasicSnack(msg: 'reelReported'.tr());
        }
      },
      builder: (context, state) {
        final cubit = ProfileCubit.get(context);

        return Positioned(
          bottom: height * 0.06,
          left: isEn ? (width * 0.04) : 0.0,
          right: isEn ? 0.0 : (width * 0.04),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Row(
                children: [
                  ClipRRect(
                      borderRadius: BorderRadius.circular(20.0),
                      child: SizedBox(
                        height: 26,
                        width: 26,
                        child: HiveHelper.getUserAvatar != null && HiveHelper.getUserAvatar != ''
                            ? FadeInImage.memoryNetwork(
                                image: userAvatar == null
                                    ? HiveHelper.getUserAvatar!
                                    : userAvatar ?? userAvatarPlaceHolderUrl,
                                fit: BoxFit.cover,
                                placeholder: kTransparentImage,
                              )
                            : Image.asset('assets/images/profile_icon.png', fit: BoxFit.cover),
                      )),
                  const SizedBox(width: 8),
                  Container(
                      decoration: BoxDecoration(color: Colors.black12, borderRadius: BorderRadius.circular(12.0)),
                      padding: const EdgeInsets.symmetric(vertical: 6, horizontal: 6),
                      child: Text(
                          userName == null
                              ? "@${HiveHelper.getUserName ?? "notAvailable".tr()}"
                              : userName ?? "notAvailable".tr(),
                          style: textStyle)),
                ],
              ),
              Padding(
                padding: const EdgeInsetsDirectional.only(end: 12.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    showDelete
                        ? IconButton(
                            onPressed: () async => showDialog(
                                  context: context,
                                  builder: (context) => Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      DeleteDialog(
                                        isLoading: state is DeleteProfileReelLoading,
                                        headerText: "deleteReel".tr(),
                                        onPressed: () async {
                                          await cubit.deleteProfileReel(reelId: reelId).then((_) async {
                                            Navigator.pop(context);
                                            Navigator.pop(context);
                                            MagicRouter.navigateAndReplace(const TalentProfileScreen(initialIndex: 2));
                                          });
                                        },
                                      ),
                                    ],
                                  ),
                                ),
                            icon: const Icon(Icons.delete, color: Colors.white))
                        : const SizedBox(),
                    showDelete
                        ? Text("delete".tr(), style: textStyle)
                        : InkWell(
                            onTap: () {
                              Utils.showAlertDialog(
                                  context: context,
                                  headerText: "reportVideo".tr(),
                                  yesButtonText: "report".tr(),
                                  noButtonText: "cancel".tr(),
                                  onPressed: () async => await cubit.reportReel(reelId: reelId));
                            },
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                const Icon(Icons.flag, color: Colors.white),
                                const SizedBox(height: 4),
                                Text(
                                  'report'.tr(),
                                  style: TextStyle(
                                    fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal',
                                    fontWeight: FontWeight.bold,
                                    color: Colors.white,
                                  ),
                                ),
                              ],
                            ),
                          ),
                  ],
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
