import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../../../utils/constants/app_const.dart';
import '../../../../../utils/constants/app_text_style.dart';
import '../../../../../utils/helpers/picker_helper.dart';
import '../../../../../utils/helpers/popup_helper.dart';
import '../../../../widgets/app_loader.dart';
import '../../../../widgets/error_state_widget.dart';
import '../../../../widgets/loading_state_widget.dart';
import '../../controller/profile_cubit.dart';
import '../../../../widgets/no_state_widget.dart';
import 'reel_widgets/reels_thumbnail.dart';

class ProfileReelsTab extends StatelessWidget {
  const ProfileReelsTab({Key? key, required this.cubit}) : super(key: key);
  final ProfileCubit cubit;

  @override
  Widget build(BuildContext context) {
    return BlocConsumer(
      bloc: cubit,
      listener: (context, state) {
        if (state is UploadReelFailed) PopupHelper.showBasicSnack(msg: state.msg);
        if (state is DeleteProfileReelSuccess || state is GetAllReelsSuccess) {
          if (ProfileCubit.get(context).reelId != null) {
            ProfileCubit.get(context).testMethod(reelId: ProfileCubit.get(context).reelId!);
          }
        }
      },
      builder: (context, state) {
        if (state is GetAllReelsLoading || cubit.reelsList == null) {
          return const LoadingStateWidget();
        } else if (state is GetAllReelsFailed) {
          return ErrorStateWidget(hasRefresh: true, onRefresh: () async => await cubit.getAllUserReels());
        }
        final reels = cubit.reelsList!;
        return SizedBox(
          height: double.infinity,
          child: Column(
            children: [
              reels.isEmpty
                  ? Expanded(
                      child: RefreshIndicator(
                          onRefresh: () async => await cubit.init(),
                          child: ListView(children: [SizedBox(height: height * 0.3), NoStateWidget()])))
                  : Expanded(
                      child: RefreshIndicator(
                        onRefresh: () async => await cubit.init(),
                        child: GridView.builder(
                          itemCount: reels.length,
                          gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
                            mainAxisSpacing: 5,
                            childAspectRatio: 1,
                            crossAxisSpacing: 5,
                            maxCrossAxisExtent: 160,
                          ),
                          itemBuilder: (context, index) {
                            return ReelsThumbnail(
                              key: UniqueKey(),
                              reels: reels,
                              videoIndex: index,
                              reelThumbnail: reels[index].media!,
                              isAudio: reels[index].media!.endsWith('.mp3') ||
                                  reels[index].media!.endsWith('.wav') ||
                                  reels[index].media!.endsWith('.aac'),
                            );
                          },
                        ),
                      ),
                    ),
              const SizedBox(height: 1),
              Container(
                height: height * 0.075,
                width: width,
                color: Colors.black,
                child: ElevatedButton(
                  onPressed: cubit.state is UploadReelLoading
                      ? null
                      : () async {
                          final video = await PickerHelper.pickVideoFile();
                          if (video != null) cubit.uploadReel(video);
                        },
                  child: cubit.state is UploadReelLoading
                      ? const AppLoader(color: Colors.white)
                      : Text(
                          'uploadReel'.tr(),
                          style: AppTextStyles.bold18.copyWith(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontFamily: isEn ? 'Montserrat' : 'Tajawal',
                          ),
                        ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
