import 'package:flutter/material.dart';

import '../../../../../config/router/router.dart';
import '../../../../../utils/constants/app_colors.dart';
import '../../../navigation_and_appbar/import_widget.dart';
import '../../widget/PastExperienceItem.dart';
import 'EditPastExpItemDetails.dart';

class EditPastExperience extends StatefulWidget {
  const EditPastExperience({Key? key}) : super(key: key);

  @override
  State<EditPastExperience> createState() => _EditPastExperienceState();
}

class _EditPastExperienceState extends State<EditPastExperience> {
  @override
  Widget build(BuildContext context) {
    return CustomScaffoldWidget(
      isBackArrow: true,
      body: Column(
        children: [
          Container(
            margin: EdgeInsets.symmetric(
                vertical: MediaQuery.of(context).size.height * 0.01),
            color: Colors.black,
            width: double.infinity,
            height: 1.5,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 30),
            child: Text(
              'Photos',
              style: Theme.of(context).textTheme.titleLarge!.copyWith(
                    fontFamily: 'BebasNeue',
                    fontSize: 50,
                  ),
            ),
          ),
          Flexible(
            child: ListView(
              children: [
                ...List.generate(
                  6,
                  (index) => Row(
                    children: [
                      const PastExperienceItem(showShadow: true),
                      Container(
                        width: MediaQuery.of(context).size.width * .2,
                        padding: const EdgeInsets.symmetric(horizontal: 10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            InkWell(
                              onTap: () async => await MagicRouter.navigateTo(
                                  const EditPastExpItemDetails()),
                              child: _text("Edit", AppColors.semiBlack),
                            ),
                            _text("Delete", AppColors.redColor),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(10),
                  child: Row(
                    children: [
                      const Icon(
                        Icons.add_box_rounded,
                        color: Colors.black,
                      ),
                      _text(
                        "LIST MORE",
                        AppColors.semiBlack,
                        isUnderlined: false,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _text(String title, Color color, {bool isUnderlined = true}) {
    return Text(
      title,
      style: TextStyle(
        fontSize: 13,
        color: color,
        fontWeight: FontWeight.w900,
        decoration:
            isUnderlined ? TextDecoration.underline : TextDecoration.none,
        fontFamily: 'Montserrat',
        overflow: TextOverflow.ellipsis,
      ),
    );
  }
}
