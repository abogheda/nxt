import 'package:flutter/material.dart';
import 'package:nxt/config/router/router.dart';

import '../../../../../utils/constants/app_colors.dart';
import '../../../../widgets/custom_text_field.dart';
import '../../../navigation_and_appbar/import_widget.dart';

class EditPastExpItemDetails extends StatefulWidget {
  const EditPastExpItemDetails({Key? key}) : super(key: key);

  @override
  State<EditPastExpItemDetails> createState() => _EditPastExpItemDetailsState();
}

class _EditPastExpItemDetailsState extends State<EditPastExpItemDetails> {
  @override
  Widget build(BuildContext context) {
    return CustomScaffoldWidget(
      isBackArrow: true,
      body: Column(
        children: [
          Container(
            margin: EdgeInsets.symmetric(
                vertical: MediaQuery.of(context).size.height * 0.01),
            color: Colors.black,
            width: double.infinity,
            height: 1.5,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 30),
            child: Text(
              'PAST EXPERIENCES',
              style: Theme.of(context).textTheme.titleLarge!.copyWith(
                    fontFamily: 'BebasNeue',
                    fontSize: 50,
                  ),
            ),
          ),
          Container(
            margin: const EdgeInsets.symmetric(horizontal: 15),
            padding:
                const EdgeInsets.only(top: 5, bottom: 15, right: 10, left: 10),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              border: Border.all(color: Colors.black),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  margin: const EdgeInsets.all(10),
                  alignment: Alignment.centerRight,
                  child: _text("Delete", AppColors.redColor, isUnderlined: true),
                ),
                _text("LINK TITLE", AppColors.semiBlack),
                Container(
                  margin: const EdgeInsets.only(top: 5, bottom: 15),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(color: AppColors.semiBlack),
                  ),
                  child: CustomTextField(
                    showBorder: false,
                    hint: "Movie Trailer",
                    type: TextInputType.multiline,
                    controller: TextEditingController(),
                    enabled: true,
                  ),
                ),
                _text("LINK", AppColors.semiBlack),
                Container(
                  margin: const EdgeInsets.only(top: 5, bottom: 15),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(color: AppColors.semiBlack),
                  ),
                  child: CustomTextField(
                    showBorder: false,
                    hint: "https://youtu.be/51jE3D09npI",
                    type: TextInputType.multiline,
                    controller: TextEditingController(),
                    enabled: true,
                  ),
                ),
              ],
            ),
          ),
          const Spacer(),
          InkWell(
            onTap: () => MagicRouter.pop(),
            child: Container(
              color: Colors.black,
              width: MediaQuery.of(context).size.width,
              alignment: Alignment.center,
              padding: const EdgeInsets.symmetric(vertical: 30, horizontal: 20),
              child: Text(
                'SAVE EDITS',
                style: Theme.of(context).textTheme.titleLarge!.copyWith(
                      fontFamily: 'BebasNeue',
                      fontSize: 30,
                      color: Colors.white,
                    ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _text(String title, Color color, {bool isUnderlined = false}) {
    return Text(
      title,
      style: TextStyle(
        fontSize: 13,
        color: color,
        fontWeight: FontWeight.w900,
        decoration:
            isUnderlined ? TextDecoration.underline : TextDecoration.none,
        fontFamily: 'Montserrat',
        overflow: TextOverflow.ellipsis,
      ),
    );
  }
}
