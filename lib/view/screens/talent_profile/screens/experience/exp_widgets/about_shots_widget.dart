part of '../../about/about_widgets/imports.dart';

class AboutPhotosWidget extends StatefulWidget {
  const AboutPhotosWidget({
    Key? key,
    required this.shots,
    required this.index,
  }) : super(key: key);
  final List<String> shots;
  final int index;

  @override
  State<AboutPhotosWidget> createState() => _AboutPhotosWidgetState();
}

class _AboutPhotosWidgetState extends State<AboutPhotosWidget> {
  bool isClicked = false;
  List<UserShotModel> userImages = [];
  Future<void> _captureImage(UserShotModel shotModel) async {
    final photo = await PickerHelper.pickFrontCameraImage();
    // final photo = await PickerHelper.pickCameraImage();
    if (photo != null) {
      userImages[shotModel.index] = shotModel.copyWith(
        ImageType.file,
        photo.path,
      );
      if(mounted){
        setState(() {});
      }
    }
  }

  Future<void> _pickImage(UserShotModel shotModel) async {
    final photo = await PickerHelper.pickGalleryImage();
    if (photo != null) {
      userImages[shotModel.index] = shotModel.copyWith(
        ImageType.file,
        photo.path,
      );
      setState(() {});
    }
  }

  @override
  void initState() {
    for (final image in widget.shots) {
      userImages.add(UserShotModel(
        index: widget.shots.indexOf(image),
        imageType: ImageType.url,
        image: image,
      ));
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final cubit = ProfileCubit.get(context);
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Padding(
              padding: const EdgeInsetsDirectional.only(start: 10.0),
              child: Text(
                'shots'.tr(),
                style:
                    Theme.of(context).textTheme.titleLarge!.copyWith(fontFamily: widget.isEn ? 'BebasNeue' : 'Tajawal'),
              ),
            ),
            TextButton(
              style: TextButton.styleFrom(padding: EdgeInsets.zero),
              onPressed: () async {
                if (isClicked) {
                  await cubit.uploadUserShots(userImages);
                  await cubit.getUserData();
                }
                if(mounted){
                  setState(() => isClicked = !isClicked);
                }
              },
              child: Text(
                isClicked ? 'save'.tr() : 'edit'.tr(),
                style: TextStyle(
                  fontSize: 14,
                  color: AppColors.semiBlack,
                  fontWeight: FontWeight.w900,
                  decoration: TextDecoration.underline,
                  fontFamily: widget.isEn ? 'Montserrat' : 'Tajawal',
                ),
              ),
            ),
          ],
        ),
        Container(
          constraints: BoxConstraints(maxHeight: widget.height * 0.2),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: userImages
                .map(
                  (userImage) => InkWell(
                    borderRadius: BorderRadius.circular(6),
                    onTap: isClicked
                        ? () => showDialog(
                            context: context,
                            builder: (context) => PickImageDialog(
                                pickFromCamera: () => _captureImage(userImage),
                                pickFromGallery: () {
                                  _pickImage(userImage);
                                  MagicRouter.pop();
                                }))
                        : null,
                    child: OnePhoto(
                      model: userImage,
                      isEditClicked: isClicked,
                      key: UniqueKey(),
                    ),
                  ),
                )
                .toList(),
          ),
        ),
      ],
    );
  }
}
