import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import '../../../../../../data/models/user_models/users_profile_model.dart';
import '../../../../../../utils/constants/resources.dart';
import '../../../../../../utils/helpers/popup_helper.dart';
import '../../../../../widgets/custom_text_field.dart';
import '../../../controller/profile_cubit.dart';

class ProfileLink extends StatefulWidget {
  final UsersProfileModel userDataProfile;
  const ProfileLink({Key? key, required this.userDataProfile}) : super(key: key);

  @override
  State<ProfileLink> createState() => _ProfileLinkState();
}

bool isEnabled = false;

class _ProfileLinkState extends State<ProfileLink> {
  final _formKey = GlobalKey<FormState>();
  String? profileLink;
  late TextEditingController linkController;
  @override
  void initState() {
    linkController = TextEditingController(text: widget.userDataProfile.portfolioLink!);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final cubit = ProfileCubit.get(context);
    return Column(
      children: [
        SizedBox(height: widget.height * 0.01),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Padding(
              padding: const EdgeInsetsDirectional.only(start: 10.0),
              child: Text(
                "link".tr(),
                style:
                    Theme.of(context).textTheme.titleLarge!.copyWith(fontFamily: widget.isEn ? 'BebasNeue' : 'Tajawal'),
              ),
            ),
            TextButton(
              onPressed: () async {
                if (isEnabled == false) {
                  isEnabled = true;
                  setState(() {});
                } else {
                  if (_formKey.currentState!.validate()) {
                    isEnabled = false;
                    setState(() {});
                    await cubit.editPortfolioLink(portfolioLink: profileLink);
                    await cubit.getUserData();
                  }
                }
              },
              child: Text(
                isEnabled ? 'save'.tr() : 'edit'.tr(),
                style: TextStyle(
                  color: AppColors.semiBlack,
                  decoration: TextDecoration.underline,
                  fontFamily: widget.isEn ? 'Montserrat' : 'Tajawal',
                  fontSize: 14,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ],
        ),
        const SizedBox(height: 10),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: widget.width * 0.03),
          child: isEnabled
              ? Form(
                  key: _formKey,
                  child: CustomTextField(
                    controller: linkController,
                    hint: 'enterLink'.tr(),
                    validator: (website) {
                      String pattern =
                          r'[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@%_\+.~#?&//=]*)';
                      RegExp regExp = RegExp(pattern);
                      if (website!.isEmpty || website == '') {
                        return "enterWebsite".tr();
                      } else if (!(regExp.hasMatch(website))) {
                        return 'enterValidUrl'.tr();
                      } else {
                        return null;
                      }
                    },
                    onChange: (p0) {
                      profileLink = p0;
                      _formKey.currentState!.save();
                    },
                  ),
                )
              : Align(
                  alignment: Alignment.topLeft,
                  child: InkWell(
                    onTap: widget.userDataProfile.portfolioLink == null || widget.userDataProfile.portfolioLink == ''
                        ? null
                        : () async {
                            String url = '';
                            if (widget.userDataProfile.portfolioLink!.contains('https://') ||
                                widget.userDataProfile.portfolioLink!.contains('http://')) {
                              url = widget.userDataProfile.portfolioLink!.toLowerCase();
                            } else {
                              url = 'https://${widget.userDataProfile.portfolioLink!.toLowerCase()}';
                            }
                            try {
                              await launchUrl(
                                Uri.parse(url),
                                mode: LaunchMode.externalApplication,
                              );
                            } catch (_) {
                              PopupHelper.showBasicSnack(msg: 'cannotOpenLink'.tr());
                            }
                          },
                    child: Text(
                      cubit.userPortfolioLink == null
                          ? (widget.userDataProfile.portfolioLink == null || widget.userDataProfile.portfolioLink == '')
                              ? 'noLink'.tr()
                              : widget.userDataProfile.portfolioLink.toString()
                          : widget.userDataProfile.portfolioLink.toString(),
                      style: TextStyle(
                          color: (widget.userDataProfile.portfolioLink == null ||
                                  widget.userDataProfile.portfolioLink == '')
                              ? Colors.black
                              : const Color(0xff1567F9),
                          decoration: (widget.userDataProfile.portfolioLink == null ||
                                  widget.userDataProfile.portfolioLink == '')
                              ? null
                              : TextDecoration.underline,
                          fontFamily: widget.isEn ? 'Montserrat' : 'Tajawal'),
                    ),
                  ),
                ),
        )
      ],
    );
  }
}
