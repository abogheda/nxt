import 'package:flutter/material.dart';
import '../../../../../../utils/constants/app_colors.dart';
import '../../../../../../utils/constants/app_const.dart';

class ExperienceWidget extends StatefulWidget {
  const ExperienceWidget({
    Key? key,
     this.experienceColor,
    required this.expTitle,
    required this.expInfo,
    required this.expDate,
  }) : super(key: key);
  final Color? experienceColor;
  final String expTitle;
  final String expInfo;
  final String expDate;

  @override
  State<ExperienceWidget> createState() => _ExperienceWidgetState();
}

final family = AppConst.isEn ? 'Montserrat' : 'Tajawal';

class _ExperienceWidgetState extends State<ExperienceWidget> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(height: widget.height * 0.01),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: widget.width * 0.03),
          child: Material(
            elevation: 2,
            child: Container(
              width: double.infinity,
              decoration: BoxDecoration(border: Border.all(color: AppColors.greyTxtColor, width: 0.5)),
              child: IntrinsicHeight(
                child: Stack(
                  children: [
                    // Container(color: widget.experienceColor, width: 10),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(height: widget.height * 0.01),
                        Padding(
                          padding: EdgeInsetsDirectional.only(start: widget.width * 0.075),
                          child: Text(
                            widget.expTitle,
                            style: Theme.of(context)
                                .textTheme
                                .titleMedium!
                                .copyWith(fontWeight: FontWeight.bold, fontFamily: family),
                          ),
                        ),
                        SizedBox(height: widget.height * 0.005),
                        Padding(
                          padding: EdgeInsetsDirectional.only(start: widget.width * 0.075),
                          child: Text(
                            widget.expInfo,
                            style: Theme.of(context).textTheme.titleSmall!.copyWith(fontFamily: family),
                          ),
                        ),
                        // SizedBox(height: widget.height * 0.005),
                        // Padding(
                        //   padding: EdgeInsetsDirectional.only(start: widget.width * 0.075),
                        //   child: Text(
                        //     widget.expDate,
                        //     style: Theme.of(context).textTheme.bodyText1!.copyWith(
                        //         fontWeight: FontWeight.normal, fontFamily: family, color: AppColors.greyTxtColor),
                        //   ),
                        // ),
                        SizedBox(height: widget.height * 0.01),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
