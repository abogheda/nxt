
import 'dart:io';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nxt/utils/constants/app_const.dart';

import '../../../../../../config/router/router.dart';
import '../../../../../../utils/constants/app_colors.dart';
import '../../../../../../utils/constants/app_text_style.dart';
import '../../../../../../utils/helpers/picker_helper.dart';
import '../../../../../widgets/app_loader.dart';
import '../../../../../widgets/no_state_widget.dart';
import '../../../controller/profile_cubit.dart';
import '../../reels/reel_widgets/reels_thumbnail.dart';
import '../exp_edit/edit_reels.dart';

class MediaVideoWidget extends StatefulWidget {
  const MediaVideoWidget({
    Key? key,
    required this.videos,
    required this.index,
  }) : super(key: key);
  final List<String> videos;
  final int index;

  @override
  State<MediaVideoWidget> createState() => _MediaVideoWidgetState();
}

class _MediaVideoWidgetState extends State<MediaVideoWidget> {


  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final cubit = ProfileCubit.get(context);
    final reels = cubit.reelsList!;

    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Padding(
              padding: const EdgeInsetsDirectional.only(start: 10.0),
              child: Text(
                'Videos',
                style:
                Theme.of(context).textTheme.titleLarge!.copyWith(fontFamily: widget.isEn ? 'BebasNeue' : 'Tajawal'),
              ),
            ),
            TextButton(
              style: TextButton.styleFrom(padding: EdgeInsets.zero),
              onPressed: () async {

                  await MagicRouter.navigateTo(EditReel(profileCubit: context.read<ProfileCubit>()));
                  setState(() {});

              },
              child: Text(
                 'edit'.tr(),
                style: TextStyle(
                  fontSize: 14,
                  color: AppColors.semiBlack,
                  fontWeight: FontWeight.w900,
                  decoration: TextDecoration.underline,
                  fontFamily: widget.isEn ? 'Montserrat' : 'Tajawal',
                ),
              ),
            ),
          ],
        ),
        SizedBox(
          height: 200,
          child: Column(
            children: [
              reels.isEmpty
                  ? Expanded(
                  child: RefreshIndicator(
                      onRefresh: () async => await cubit.init(),
                      child: ListView(children: [SizedBox(height: widget.height * 0.3), NoStateWidget()])))
                  : Expanded(
                child: RefreshIndicator(
                  onRefresh: () async => await cubit.init(),
                  child: ListView.builder(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    scrollDirection: Axis.horizontal,
                    itemCount: reels.length,
                    itemBuilder: (context, index) {
                      return ReelsThumbnail(
                        key: UniqueKey(),
                        reels: reels,
                        videoIndex: index,
                        reelThumbnail: reels[index].media!,
                        isAudio: reels[index].media!.endsWith('.mp3') ||
                            reels[index].media!.endsWith('.wav') ||
                            reels[index].media!.endsWith('.aac'),
                      );
                    },
                  ),
                ),
              ),
              const SizedBox(height: 1),
            ],
          ),
        )
      ],
    );
  }
}
