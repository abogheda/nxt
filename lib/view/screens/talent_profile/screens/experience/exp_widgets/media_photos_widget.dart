
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:nxt/utils/constants/app_const.dart';

import '../../../../../../config/router/router.dart';
import '../../../../../../data/models/local_model/user_shot_model.dart';
import '../../../../../../utils/constants/app_colors.dart';
import '../../../../../../utils/helpers/picker_helper.dart';
import '../../../controller/profile_cubit.dart';
import '../../about/about_widgets/imports.dart';

class MediaPhotosWidget extends StatefulWidget {
  const MediaPhotosWidget({
    Key? key,
    required this.shots,
    required this.index,
  }) : super(key: key);
  final List<String> shots;
  final int index;

  @override
  State<MediaPhotosWidget> createState() => _MediaPhotosWidgetState();
}

class _MediaPhotosWidgetState extends State<MediaPhotosWidget> {
  bool isClicked = false;
  List<UserShotModel> userImages = [];
  Future<void> _captureImage(UserShotModel shotModel) async {
    final photo = await PickerHelper.pickFrontCameraImage();
    // final photo = await PickerHelper.pickCameraImage();
    if (photo != null) {
      userImages[shotModel.index] = shotModel.copyWith(
        ImageType.file,
        photo.path,
      );
      setState(() {});
    }
  }

  Future<void> _pickImage(UserShotModel shotModel) async {
    final photo = await PickerHelper.pickGalleryImage();
    if (photo != null) {
      print("object : ${photo.path}");
      userImages[shotModel.index] = shotModel.copyWith(
        ImageType.file,
        photo.path,
      );
      if(mounted){
        setState(() {});
      }
    }
  }

  @override
  void initState() {
    for (final image in widget.shots) {
      userImages.add(UserShotModel(
        index: widget.shots.indexOf(image),
        imageType: ImageType.url,
        image: image,
      ));
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final cubit = ProfileCubit.get(context);
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Padding(
              padding: const EdgeInsetsDirectional.only(start: 10.0),
              child: Text(
                'Photos',
                style:
                    Theme.of(context).textTheme.titleLarge!.copyWith(fontFamily: widget.isEn ? 'BebasNeue' : 'Tajawal'),
              ),
            ),
            TextButton(
              style: TextButton.styleFrom(padding: EdgeInsets.zero),
              onPressed: () async {
                if (isClicked) {
                  await cubit.uploadUserPhotos(userImages);
                  await cubit.getUserData();
                }
                if(mounted){
                  setState(() => isClicked = !isClicked);
                }
              },
              child: Text(
                isClicked ? 'save'.tr() : 'edit'.tr(),
                style: TextStyle(
                  fontSize: 14,
                  color: AppColors.semiBlack,
                  fontWeight: FontWeight.w900,
                  decoration: TextDecoration.underline,
                  fontFamily: widget.isEn ? 'Montserrat' : 'Tajawal',
                ),
              ),
            ),
          ],
        ),
        Container(
          constraints: BoxConstraints(maxHeight: widget.height * 0.2),
          child: ListView(
            scrollDirection: Axis.horizontal,
            children: userImages
                .map(
                  (userImage) => InkWell(
                    borderRadius: BorderRadius.circular(6),
                    onTap: isClicked
                        ? () async{
                      await  _pickImage(userImage);
                        // MagicRouter.pop();

                    }
                        : null,
                    child: OnePhoto(
                      model: userImage,
                      isEditClicked: isClicked,
                      key: UniqueKey(),
                    ),
                  ),
                )
                .toList(),
          ),
        ),
      ],
    );
  }
}
