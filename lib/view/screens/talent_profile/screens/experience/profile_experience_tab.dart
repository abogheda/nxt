import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nxt/view/screens/talent_profile/widget/PastExperienceSection.dart';
import '../../../../../data/models/user_models/users_profile_model.dart';
import '../../../../../utils/constants/app_colors.dart';
import '../../../../../utils/constants/app_const.dart';
import '../../../../../utils/constants/resources.dart';
import '../../../../widgets/error_state_widget.dart';
import '../../../../widgets/loading_state_widget.dart';
import '../../controller/profile_cubit.dart';
import '../../../../../config/router/router.dart';
import '../about/about_widgets/imports.dart';
import 'exp_edit/experiences_edit.dart';
import 'exp_widgets/experience_widget.dart';
import 'exp_widgets/media_photos_widget.dart';
import 'exp_widgets/media_video_widget.dart';
import 'exp_widgets/profile_link_widget.dart';

class ProfileExperienceTab extends StatelessWidget {
  const ProfileExperienceTab({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProfileCubit, ProfileState>(
      builder: (context, state) {
        final cubit = ProfileCubit.get(context);
        final usersProfileModel = cubit.usersProfileModel ?? UsersProfileModel();
        final placeHolderList = [
          placeHolderUrl,
          placeHolderUrl,
          placeHolderUrl
        ];
        if (state is GetProfileFailed) {
          return ErrorStateWidget(hasRefresh: true, onRefresh: () async => cubit.getUserData());
        }
        if (state is GetProfileLoading) return const LoadingStateWidget();
        return RefreshIndicator(
          onRefresh: () async => cubit.getUserData(),
          child: ListView(
            children: [
              Column(
                 children: [
                   AboutPhotosWidget(
                     key: UniqueKey(),
                     index: usersProfileModel.shots!.length,
                     shots: usersProfileModel.shots == []
                         ? placeHolderList
                         : usersProfileModel.shots ?? placeHolderList,
                   ),
                   SizedBox(height: height * 0.01),
                   Container(color: Colors.black, width: double.infinity, height: 1.5),
                 ],
               ),
              Column(
                 children: [
                   MediaPhotosWidget(
                     key: UniqueKey(),
                     index: usersProfileModel.photos?.length ?? 0,
                     shots: usersProfileModel.photos == []
                         ? placeHolderList
                         : usersProfileModel.photos ?? placeHolderList,
                   ),
                   SizedBox(height: height * 0.01),
                   Container(color: Colors.black, width: double.infinity, height: 1.5),
                 ],
               ),
              Column(
                 children: [
                   MediaVideoWidget(
                     key: UniqueKey(),
                     index: usersProfileModel.photos?.length ?? 0,
                     videos: usersProfileModel.photos == []
                         ? placeHolderList
                         : usersProfileModel.photos ?? placeHolderList,
                   ),
                   SizedBox(height: height * 0.01),
                   Container(color: Colors.black, width: double.infinity, height: 1.5),
                 ],
               ),
              SizedBox(height: height * 0.01),
              const PastExperienceSection(),
            ],
          ),
        );
      },
    );
  }
}
