import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../../../config/router/router.dart';

import '../../../../../../utils/constants/app_const.dart';

import '../../../../../widgets/no_state_widget.dart';
import '../../../../navigation_and_appbar/import_widget.dart';
import '../../../controller/profile_cubit.dart';
import '../../reels/reel_widgets/reels_thumbnail.dart';

class EditReel extends StatefulWidget {
  final ProfileCubit profileCubit;
  const EditReel({Key? key, required this.profileCubit}) : super(key: key);

  @override
  State<EditReel> createState() => _EditReelState();
}

class _EditReelState extends State<EditReel> {
  late ProfileCubit cubit;

  @override
  void initState() {
    cubit = widget.profileCubit;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final reels = cubit.reelsList!;
    return BlocProvider.value(
      value: widget.profileCubit,
      child: CustomScaffoldWidget(
        backgroundColor: Colors.white,
        extendBodyBehindAppBar: false,
        isBackArrow: true,
        isInProfile: true,
        isLogoClickable: true,
        body: Stack(
          children: [
            Positioned(
              bottom: 0,
              left: 0,
              right: 0,
              top: 0,
              child: Column(
                children: [
                  reels.isEmpty
                      ? Expanded(
                      child: RefreshIndicator(
                          onRefresh: () async => await cubit.init(),
                          child: ListView(children: [SizedBox(height: widget.height * 0.3), NoStateWidget()])))
                      : Expanded(
                    child: RefreshIndicator(
                      onRefresh: () async => await cubit.init(),
                      child: GridView.builder(
                        itemCount: reels.length,
                        gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
                          mainAxisSpacing: 5,
                          childAspectRatio: 1,
                          crossAxisSpacing: 5,
                          maxCrossAxisExtent: 160,
                        ),
                        itemBuilder: (context, index) {
                          return ReelsThumbnail(
                            key: UniqueKey(),
                            reels: reels,
                            videoIndex: index,
                            reelThumbnail: reels[index].media!,
                            isAudio: reels[index].media!.endsWith('.mp3') ||
                                reels[index].media!.endsWith('.wav') ||
                                reels[index].media!.endsWith('.aac'),
                          );
                        },
                      ),
                    ),
                  ),
                  const SizedBox(height: 1),
                ],
              ),
            ),
            Positioned(
              bottom: 0,
              left: 0,
              right: 0,
              child: MaterialButton(
                onPressed: () async {
                  await cubit.getUserData();
                  MagicRouter.pop();
                },
                height: widget.height * 0.08,
                minWidth: double.infinity,
                color: Colors.black,
                child: Text(
                  "save".tr(),
                  style: TextStyle(
                    fontSize: 18,
                    color: Colors.white,
                    fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal',
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
