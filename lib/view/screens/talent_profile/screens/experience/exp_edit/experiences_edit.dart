import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../../../../config/router/router.dart';
import '../../../../../../data/models/user_models/job_experience_model.dart';
import '../../../../../../utils/constants/app_colors.dart';
import '../../../../../../utils/constants/app_const.dart';
import '../../../../../../utils/constants/app_text_style.dart';
import '../../../../../../utils/helpers/picker_helper.dart';
import '../../../../../../utils/helpers/popup_helper.dart';
import '../../../../../../utils/helpers/validators.dart';
import '../../../../../widgets/custom_text_field.dart';
import '../../../../navigation_and_appbar/import_widget.dart';
import '../../../controller/profile_cubit.dart';

import '../../../../../widgets/app_loader.dart';

class ExperienceEdit extends StatefulWidget {
  const ExperienceEdit({Key? key}) : super(key: key);

  @override
  State<ExperienceEdit> createState() => _ExperienceEditState();
}

class _ExperienceEditState extends State<ExperienceEdit> {
  final formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    final cubit = ProfileCubit.get(context);
    return CustomScaffoldWidget(
      backgroundColor: Colors.white,
      extendBodyBehindAppBar: false,
      isBackArrow: true,
      isInProfile: true,
      isLogoClickable: true,
      body: BlocBuilder<ProfileCubit, ProfileState>(
        builder: (context, state) {
          return Stack(
            children: [
              Positioned(
                bottom: 0,
                left: 0,
                right: 0,
                top: 0,
                child: ListView(
                  padding: EdgeInsets.symmetric(horizontal: widget.width * 0.03),
                  children: [
                    Form(
                      key: formKey,
                      child: ListView.builder(
                        shrinkWrap: true,
                        physics: const ClampingScrollPhysics(),
                        itemCount: cubit.jobExperiences!.length,
                        itemBuilder: (context, index) {
                          final item = cubit.jobExperiences![index];
                          return Container(
                            key: ValueKey(cubit.jobExperiences!.indexOf(item)),
                            decoration: BoxDecoration(
                              border: Border.all(width: 1),
                              borderRadius: const BorderRadius.all(Radius.circular(6.0)),
                            ),
                            margin: const EdgeInsets.only(bottom: 8),
                            padding: const EdgeInsets.only(bottom: 12, left: 12, right: 12),
                            child: Column(
                              children: [
                                Align(
                                  alignment: Alignment.topRight,
                                  child: IconButton(
                                    onPressed: () {
                                      setState(() {
                                        cubit.jobExperiences!.removeAt(index);
                                      });
                                    },
                                    padding: EdgeInsets.zero,
                                    icon: const Icon(Icons.clear),
                                  ),
                                ),
                                CustomTextField(
                                    hint: 'jobTitle'.tr(),
                                    initialValue: item.jobTitle,
                                    validator: Validators.generalField,
                                    onChange: (val) {
                                      item.jobTitle = val;
                                      formKey.currentState!.save();
                                    }),
                                const SizedBox(height: 12),
                                CustomTextField(
                                  hint: 'roleDescription'.tr(),
                                  initialValue: item.roleDescription,
                                  validator: Validators.generalField,
                                  height: 56,
                                  onChange: (val) {
                                    item.roleDescription = val;
                                    formKey.currentState!.save();
                                  },
                                ),
                                const SizedBox(height: 8),
                                Row(
                                  children: [
                                    Expanded(
                                      child: StatefulBuilder(builder: (_, stState) {
                                        return CustomTextField(
                                          key: ValueKey(item.workFrom),
                                          hint: 'from'.tr(),
                                          validator: Validators.generalField,
                                          initialValue: item.workFrom == null
                                              ? null
                                              : DateFormat("dd/MM/yyyy").format(item.workFrom!),
                                          onTap: () async {
                                            FocusManager.instance.primaryFocus?.unfocus();
                                            final date = await PickerHelper.pickDate(
                                                currentTime: item.workFrom ?? DateTime.now());
                                            if (date != null) {
                                              item.workFrom = date;
                                              stState(() {});
                                            }
                                          },
                                        );
                                      }),
                                    ),
                                    const SizedBox(width: 8),
                                    Expanded(
                                      child: StatefulBuilder(builder: (_, stState) {
                                        return CustomTextField(
                                          key: ValueKey(item.workTo),
                                          hint: 'to'.tr(),
                                          validator: Validators.generalField,
                                          initialValue: item.workTo == null
                                              ? null
                                              : DateFormat("dd/MM/yyyy").format(item.workTo!),
                                          onTap: () async {
                                            if (item.workFrom == null) {
                                              PopupHelper.showBasicSnack(msg: 'please insert work from date');
                                              return;
                                            }
                                            FocusManager.instance.primaryFocus?.unfocus();
                                            final date = await PickerHelper.pickDate(
                                              currentTime: item.workTo ?? DateTime.now(),
                                              minTime: item.workFrom ?? DateTime.now(),
                                              maxTime: DateTime(2200),
                                            );
                                            if (date != null) {
                                              item.workTo = date;
                                              stState(() {});
                                            }
                                          },
                                        );
                                      }),
                                    ),
                                  ],
                                )
                              ],
                            ),
                          );
                        },
                      ),
                    ),
                    TextButton.icon(
                      onPressed: () {
                        cubit.jobExperiences!.add(JobExperienceModel());
                        setState(() {});
                      },
                      icon: const Icon(
                        Icons.add_box_rounded,
                        color: Colors.black,
                      ),
                      label: Text(
                        'addMore'.tr(),
                        style: AppTextStyles.medium14.copyWith(color: AppColors.greyTxtColor),
                      ),
                    ),
                    SizedBox(height: widget.height * 0.1),
                  ],
                ),
              ),
              Positioned(
                bottom: 0,
                left: 0,
                right: 0,
                child: MaterialButton(
                  onPressed: () async {
                    if (formKey.currentState!.validate()) {
                      await cubit.editExperiences(experiencesList: cubit.jobExperiences);
                      MagicRouter.pop();
                    }
                  },
                  height: widget.height * 0.08,
                  minWidth: double.infinity,
                  color: Colors.black,
                  child: state is ExperiencesLoading
                      ? const AppLoader(color: Colors.white)
                      : Text(
                          "save".tr(),
                          style: TextStyle(
                            fontSize: 18,
                            color: Colors.white,
                            fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal',
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                ),
              )
            ],
          );
        },
      ),
    );
  }
}
