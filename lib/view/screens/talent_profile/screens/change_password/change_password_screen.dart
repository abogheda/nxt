import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../../../utils/constants/app_const.dart';
import '../../controller/profile_cubit.dart';
import '../edit_profile/profile_text_form_field.dart';

import '../../../../../config/router/router.dart';
import '../../../../../utils/helpers/popup_helper.dart';
import '../../../../../utils/helpers/validators.dart';
import '../../../../widgets/app_loader.dart';
import '../../../../widgets/custom_button.dart';

class ChangePasswordScreen extends StatefulWidget {
  const ChangePasswordScreen({Key? key}) : super(key: key);

  @override
  State<ChangePasswordScreen> createState() => _ChangePasswordScreenState();
}

class _ChangePasswordScreenState extends State<ChangePasswordScreen> {
  final passwordFormKey = GlobalKey<FormState>();
  late TextEditingController currentPasswordController;
  late TextEditingController newPasswordController;
  late TextEditingController confirmNewPasswordController;
  late ProfileCubit cubit;

  @override
  void initState() {
    cubit = context.read<ProfileCubit>();
    currentPasswordController = TextEditingController();
    newPasswordController = TextEditingController();
    confirmNewPasswordController = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    currentPasswordController.dispose();
    newPasswordController.dispose();
    confirmNewPasswordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(elevation: 0.0, foregroundColor: Colors.black, backgroundColor: Colors.transparent),
      body: BlocConsumer<ProfileCubit, ProfileState>(
        listener: (context, state) {
          if (state is UpdatePasswordFailed) {
            PopupHelper.showBasicSnack(msg: state.msg, color: Colors.red);
            return;
          }
          if (state is UpdatePasswordSuccess) MagicRouter.pop();
        },
        builder: (context, state) {
          final cubit = ProfileCubit.get(context);
          return Stack(
            children: [
              Positioned(
                top: 0,
                right: 0,
                left: 0,
                bottom: 0,
                child: ListView(
                  children: [
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: widget.width * 0.05),
                      child: Text(
                        'changePassword'.tr(),
                        style: Theme.of(context).textTheme.titleLarge!.copyWith(
                              fontFamily: widget.isEn ? 'Montserrat' : 'Tajawal',
                              fontWeight: FontWeight.bold,
                            ),
                      ),
                    ),
                    const SizedBox(height: 12.0),
                    Form(
                      key: passwordFormKey,
                      child: Column(
                        children: <Widget>[
                          ProfileTextFormField(
                            inputType: TextInputType.visiblePassword,
                            hint: 'currentPasswordHint'.tr(),
                            controller: currentPasswordController,
                            validator: Validators.password,
                          ),
                          ProfileTextFormField(
                            inputType: TextInputType.visiblePassword,
                            hint: 'newPasswordHint'.tr(),
                            controller: newPasswordController,
                            validator: Validators.password,
                          ),
                          ProfileTextFormField(
                            inputType: TextInputType.visiblePassword,
                            hint: 'confirmNewPasswordHint'.tr(),
                            controller: confirmNewPasswordController,
                            validator: Validators.password,
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: widget.height * 0.1),
                  ],
                ),
              ),
              Positioned(
                right: 0,
                left: 0,
                bottom: 0,
                child: Container(
                  color: Colors.black,
                  child: CustomButton(
                    height: widget.height * 0.08,
                    width: double.infinity,
                    onTap: () async {
                      if (passwordFormKey.currentState!.validate()) {
                        if (currentPasswordController.text == newPasswordController.text ||
                            currentPasswordController.text == confirmNewPasswordController.text) {
                          PopupHelper.showBasicSnack(msg: 'validation.newPass'.tr(), color: Colors.red);
                          return;
                        }
                        if (newPasswordController.text != confirmNewPasswordController.text) {
                          PopupHelper.showBasicSnack(msg: 'validation.samePass'.tr(), color: Colors.red);
                          return;
                        }
                        await cubit.updatePassword(
                          currentPassword: currentPasswordController.text,
                          newPassword: newPasswordController.text,
                        );
                        await cubit.getUserData();
                      }
                    },
                    child: state is GetProfileLoading || state is UpdatePasswordLoading
                        ? const AppLoader(color: Colors.white)
                        : Text(
                            'saveChange'.tr().toUpperCase(),
                            style: TextStyle(
                              fontSize: 18,
                              color: Colors.white,
                              fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal',
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                  ),
                ),
              )
            ],
          );
        },
      ),
    );
  }
}
