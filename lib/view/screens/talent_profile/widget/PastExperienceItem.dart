import 'package:flutter/material.dart';

import '../../../../utils/constants/app_colors.dart';
import '../../../../utils/helpers/utils.dart';

class PastExperienceItem extends StatelessWidget {
  final bool showShadow;
  const PastExperienceItem({Key? key, this.showShadow = false}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width * .8 - 30,
      margin: const EdgeInsets.symmetric(vertical: 5, horizontal: 10),
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: showShadow ? Colors.grey.withOpacity(.7) : Colors.transparent,
            blurRadius: 5, spreadRadius: 1
          ),
        ]
      ),
      child: Row(
        children: [
          Image.network(
            "https://www.elwekalanews.net/UploadCache/libfiles/16/8/600x338o/68.jpg",
            height: 55,
            width: 60,
            fit: BoxFit.cover,
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Text(
                    'MOVIE TRAILER',
                    style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w900,
                      fontFamily: 'Montserrat',
                    ),
                  ),
                  InkWell(
                    onTap: ()=> Utils.launchAppUrl(url: "https://youtu.be/51jE3D09npI"),
                    child: const Text(
                      'https://youtu.be/51jE3D09npI',
                      style: TextStyle(
                        fontSize: 14,
                        color: AppColors.blueColor,
                        fontWeight: FontWeight.w900,
                        decoration: TextDecoration.underline,
                        fontFamily: 'Montserrat',
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
