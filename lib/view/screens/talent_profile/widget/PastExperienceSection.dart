import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:nxt/view/screens/talent_profile/screens/edit_past_experience/EditPastExperience.dart';
import 'package:nxt/view/screens/talent_profile/widget/PastExperienceItem.dart';

import '../../../../config/router/router.dart';
import '../../../../utils/constants/app_colors.dart';

class PastExperienceSection extends StatelessWidget {
  const PastExperienceSection({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            Expanded(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: Text(
                  'PAST EXPERIENCE',
                  style: Theme.of(context)
                      .textTheme
                      .titleLarge!
                      .copyWith(fontFamily: 'BebasNeue'),
                ),
              ),
            ),
            TextButton(
              style: TextButton.styleFrom(padding: EdgeInsets.zero),
              onPressed: () async {
                await MagicRouter.navigateTo(const EditPastExperience());
                // setState(() {});
              },
              child: Text(
                'edit'.tr(),
                style: const TextStyle(
                  fontSize: 14,
                  color: AppColors.semiBlack,
                  fontWeight: FontWeight.w900,
                  decoration: TextDecoration.underline,
                  fontFamily: 'Montserrat',
                ),
              ),
            ),
          ],
        ),
        ...List.generate(
          3,
          (index) => const PastExperienceItem(),
        ),
        Container(
          margin: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height * 0.01),
          color: Colors.black,
          width: double.infinity,
          height: 1.5,
        ),
      ],
    );
  }
}
