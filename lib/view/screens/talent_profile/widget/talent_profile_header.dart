import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:transparent_image/transparent_image.dart';
import '../../../../utils/constants/app_colors.dart';
import '../../../../utils/constants/app_const.dart';
import 'package:age_calculator/age_calculator.dart';
import '../controller/profile_cubit.dart';
import '../../../../config/router/router.dart';
import '../../../../data/service/hive_services.dart';
import '../screens/edit_profile/add_birth_date_screen.dart';
import '../screens/edit_profile/edit_profile_screen.dart';

class TalentProfileHeader extends StatelessWidget {
  const TalentProfileHeader({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProfileCubit, ProfileState>(
      builder: (context, state) {
        final cubit = ProfileCubit.get(context);
        return Container(
          width: width,
          decoration: const BoxDecoration(
            border: Border(
              top: BorderSide(
                  color: Colors.black, width: 0.9, style: BorderStyle.solid),
              bottom: BorderSide(
                  color: Colors.black, width: 0.9, style: BorderStyle.solid),
            ),
          ),
          child: Row(
            children: [
              Container(
                height: width * 0.5,
                width: width * 0.4,
                decoration: BoxDecoration(
                  color: AppColors.greyOutText,
                  image: DecorationImage(
                      fit: BoxFit.cover,
                      image: HiveHelper.getUserAvatar != null &&
                              HiveHelper.getUserAvatar != ''
                          ? FadeInImage.memoryNetwork(
                              image: HiveHelper.getUserAvatar!,
                              placeholder: kTransparentImage,
                            ).image
                          : Image.asset('assets/images/profile_icon.png')
                              .image),
                ),
              ),
              SizedBox(
                height: 160.h,
                width: 210.w,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text("About"),
                        const Spacer(),
                        InkWell(
                          onTap: () {
                            MagicRouter.navigateTo(
                              BlocProvider.value(
                                value: ProfileCubit.get(context),
                                child:
                                    cubit.usersProfileModel!.user!.birthDate ==
                                            null
                                        ? AddBirthDateScreen(
                                            usersProfileModel:
                                                cubit.usersProfileModel!)
                                        : EditProfileScreen(
                                            usersProfileModel:
                                                cubit.usersProfileModel!),
                              ),
                            );
                          },
                          child: Text(
                            'edit'.tr(),
                            style: Theme.of(context)
                                .textTheme
                                .bodyText1!
                                .copyWith(
                                    color: AppColors.semiBlack,
                                    fontWeight: FontWeight.bold,
                                    decoration: TextDecoration.underline,
                                    fontFamily: AppConst.isEn
                                        ? 'Montserrat'
                                        : 'Tajawal'),
                          ),
                        )
                      ],
                    ),
                    InfoRow(title: 'Name', value: HiveHelper.getUserName ?? ""),
                    InfoRow(
                      title: 'DATE OF BIRTH',
                      value: HiveHelper.getUserInfo!.user!.birthDate
                              ?.substring(0, 10) ??
                          "",
                    ),
                    InfoRow(
                      title: 'ACTING AGE',
                      value: "${cubit.usersProfileModel?.user?.actingAgeFrom} - ${cubit.usersProfileModel?.user?.actingAgeTo}"

                          ,
                    ),
                    InfoRow(
                        title: 'NATIONALITY',
                        value: cubit.usersProfileModel?.user?.nationality ?? ""),
                    InfoRow(
                        title: 'RESIDENCE',
                        value: cubit.usersProfileModel?.user?.profileCountry ?? ''),

                    InfoRow(
                        title: 'GENDER',
                        value: cubit.usersProfileModel?.user?.gender ?? ""),
                  ],
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}

class InfoRow extends StatelessWidget {
  final String title;
  final String value;
  const InfoRow({Key? key, required this.title, required this.value})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Text(
          "$title : ",
          style: Theme.of(context).textTheme.titleLarge!.copyWith(
                overflow: TextOverflow.ellipsis,
                fontFamily: AppConst.isEn ? 'BebasNeue' : 'Tajawal',
              ),
          maxLines: 1,
          softWrap: true,
        ),
        Text(
          value,
          style: TextStyle(
              color: AppColors.greyTxtColor,
              fontFamily: AppConst.isEn ? 'BebasNeue' : 'Tajawal',
              fontSize: 15),
          maxLines: 1,
          softWrap: true,
        )
      ],
    );
  }
}
