import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../utils/constants/resources.dart';

import 'screens/about/profile_about_tab.dart';
import 'screens/experience/profile_experience_tab.dart';
import 'screens/reels/profile_reels_tab.dart';

import '../navigation_and_appbar/import_widget.dart';
import 'controller/profile_cubit.dart';
import 'widget/talent_profile_header.dart';

class TalentProfileScreen extends StatefulWidget {
  const TalentProfileScreen({Key? key, this.initialIndex = 0}) : super(key: key);

  final int initialIndex;

  @override
  State<TalentProfileScreen> createState() => _TalentProfileScreenState();
}

class _TalentProfileScreenState extends State<TalentProfileScreen> with SingleTickerProviderStateMixin {
  late TabController _tabController;

  List<Tab> tabTitleList = [
    const Tab(child: Text("DETAILS")),
    const Tab(child: Text("MEDIA")),
    const Tab(child: Text("Highlights")),
  ];

  @override
  void initState() {
    _tabController = TabController(length: tabTitleList.length, vsync: this, initialIndex: widget.initialIndex);
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    _tabController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<ProfileCubit>(
      create: (_) => ProfileCubit()..init(),
      child: BlocBuilder<ProfileCubit, ProfileState>(
        builder: (context, state) {
          return CustomScaffoldWidget(
            isInProfile: true,
            extendBodyBehindAppBar: false,
            isBackArrow: true,
            isThereLogo: true,
            isThereActions: true,
            backgroundColor: Colors.white,
            body: Column(
              children: [
                const TalentProfileHeader(),
                Expanded(
                  child: Column(
                    children: [
                      Row(
                        children: [
                          Container(color: AppColors.yellowColor, width: widget.width * 0.5, height: 8),
                          Container(color: AppColors.blueColor, width: widget.width * 0.5, height: 8)
                        ],
                      ),
                      Container(
                        color: Colors.black,
                        child: TabBar(
                          physics: const NeverScrollableScrollPhysics(),
                          controller: _tabController,
                          indicator: const BoxDecoration(color: Colors.black),
                          labelColor: Colors.white,
                          labelStyle: Theme.of(context)
                              .textTheme
                              .headline6!
                              .copyWith(fontFamily: AppConst.isEn ? 'BebasNeue' : 'Tajawal'),
                          unselectedLabelColor: AppColors.greyOutText,
                          tabs: tabTitleList,
                        ),
                      ),
                      Expanded(
                        child: TabBarView(
                          controller: _tabController,
                          children: [
                            const ProfileAboutTab(),
                            const ProfileExperienceTab(),
                            ProfileReelsTab(cubit: BlocProvider.of(context)),
                          ],
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          );
        },
      ),
    );
  }
}
