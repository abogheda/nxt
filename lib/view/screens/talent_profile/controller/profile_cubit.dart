// ignore_for_file: unnecessary_import, depend_on_referenced_packages
import 'dart:io';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nxt/config/router/router.dart';
import 'package:nxt/data/models/talent_model.dart';
import 'package:nxt/data/models/user_models/complete_prof_model.dart';
import 'package:nxt/data/models/user_models/job_experience_model.dart';
import 'package:nxt/data/models/user_models/membership_model.dart';
import 'package:nxt/data/models/user_models/users_profile_model.dart';
import 'package:nxt/data/repos/imports.dart';
import 'package:nxt/data/service/hive_services.dart';
import 'package:nxt/view/screens/home/cubit/home_cubit.dart';
import '../../../../data/enum/report_type.dart';
import '../../../../data/enum/talent_type.dart';
import '../../../../data/models/base_models/base_single_res.dart';
import '../../../../data/models/get_member_ship_model.dart';
import '../../../../data/models/local_model/user_shot_model.dart';
import '../../../../data/models/profile_reel_model.dart';
import '../../../../data/models/report/report_model.dart';
import '../../../../data/models/skills_model.dart';
import '../../../../data/models/user_model.dart' as user;
import '../../../../data/models/user_models/education_model.dart';
import '../../../../data/models/user_models/educations.dart';
import '../../../../data/models/user_models/social_media.dart';
import '../../../../data/repos/general_repo.dart';
import '../../../../data/repos/media_repo.dart';
import '../../../../data/repos/report/report_repo.dart';
import '../../../../utils/helpers/picker_helper.dart';

part 'profile_state.dart';

class ProfileCubit extends Cubit<ProfileState> {
  ProfileCubit() : super(ProfileInitial());
  static ProfileCubit get(context) => BlocProvider.of(context);
  TextEditingController bioController = TextEditingController();
  TextEditingController weightController = TextEditingController();
  TextEditingController heightController = TextEditingController();
  // social media
  TextEditingController facebookController = TextEditingController();
  TextEditingController instagramController = TextEditingController();
  TextEditingController youtubeController = TextEditingController();
  TextEditingController twitterController = TextEditingController();
  TextEditingController tiktokController = TextEditingController();

  FocusNode bioFocusNode = FocusNode();
  UsersProfileModel? usersProfileModel;
  List<ProfileReelModel>? reelsList;
  List<SkillsModel>? skills;
  List<SkillsModel>? mySkills;
  String? userSkinColor;
  String? userHairColor;
  String? userEyeColor;
  File? memberPdf;
  String? memberPdfUrl;
  File? profileImage;
  String? profileImageUrl;
  String? reelId;
  List<Categories>? categoriesList;
  List<TalentModel>? talentsList;
  List<TalentModel>? myTalents;
  MembershipModel? memberModel;
  List<GetMemberShipModel>? memberShipList;
  List<SocialMedia>? socialMediaList;
  List<String> addedTalents = [];
  List<String> addedSkillsId = [];
  List<String> addedSkills = [];
  List<SocialMedia> addedSocialMedia = [];
  String? skill = '';
  String? talent = '';
  String? memberId = '';
  List<String> addedCategories = [];
  List<JobExperienceModel>? _jobExperiences = [JobExperienceModel()];
  List<EducationModel>? _educationExperiences = [EducationModel()];
  List<SocialMedia>? _socialMedia = [SocialMedia()];
  List<JobExperienceModel>? get jobExperiences => _jobExperiences;
  List<EducationModel>? get educationExperiences => _educationExperiences;
  List<SocialMedia>? get socialMediaProfile => _socialMedia;
  late bool isActing;
  String? education = 'General Education';
  final userInfoFormKey = GlobalKey<FormState>();
  Future<void> init() async {
    await getUserData();
    await getAllUserReels();
    await HomeCubit.get(MagicRouter.currentContext!).getUserData();
  }

  Future pickImageForProfile() async {
    profileImage = await PickerHelper.pickGalleryImage();
    emit(ProfileInitial());
  }

  Future pickMemberPdf() async {
    memberPdf = await PickerHelper.pickTextFile();
    emit(ProfileInitial());
  }

  void emitState({required ProfileState state}) => emit(state);

  // audio media
  File? userAudio;
  Future<bool> pickAudio() async {
    final audio = await PickerHelper.pickAudioFile();
    if (audio != null) {
      userAudio = File(audio.path);
      return true;
    }
    return false;
  }
    Future<void> uploadAudioMedia(
      {required File media}) async {
    emit(UploadLoading());
    final res = await GeneralRepo.uploadMedia([media]);
    res.fold(
          (urls) async {
        // emit(const UploadDone(challengeDataUploaded: false));

      },
          (errorMsg) {
        emit(UploadingFailed(errorMsg));
      },
    );
  }
  Future<void> getUserData() async {
    emit(GetProfileLoading());
    final res = await ProfileRepo.getUserData();
    if (res.data != null && res.data != null) {
      usersProfileModel = res.data;
      if (usersProfileModel!.specialSkills == null ||
          usersProfileModel!.specialSkills! == []) {
        usersProfileModel!.specialSkills = [];
      }
      for (var category in usersProfileModel!.categories!) {
        addedCategories.add(category.id!);
      }
      for (var talent in usersProfileModel!.talents!) {
        addedTalents.add(talent.id!);
      }
      memberModel = usersProfileModel!.membership!;
      myTalents = usersProfileModel!.talents!;
      isActing = addedCategories.contains(TalentCategoryType.acting.id);
      mySkills = usersProfileModel!.specialSkills!;
      socialMediaList = usersProfileModel!.socialMedia!;
      bioController.text = usersProfileModel?.bio ?? "";
      weightController.text = usersProfileModel?.weight.toString() ?? '';
      heightController.text = usersProfileModel?.height.toString() ?? '';
      _jobExperiences = usersProfileModel!.experiences;
      _educationExperiences = usersProfileModel!.educations;
      for (var i = 0; i < usersProfileModel!.educations!.length; i++) {
        education = usersProfileModel!.educations![i].educationType;
      }
      final copiedUser = usersProfileModel!.user!;
      await HiveHelper.cacheUserModel(
        userModel: HiveHelper.getUserInfo!.copyWith(
          user: user.User(
            bio: copiedUser.bio,
            id: copiedUser.id,
            profileStatus: copiedUser.profileStatus,
            phone: copiedUser.phone,
            birthDate: copiedUser.birthDate,
            email: copiedUser.email,
            name: copiedUser.name,
            package: copiedUser.package,
            verify: copiedUser.verify,
            v: copiedUser.v,
            createdAt: copiedUser.createdAt,
            organization: copiedUser.organization,
            avatar: copiedUser.avatar,
            provider: copiedUser.provider,
            role: copiedUser.role,
            updatedAt: copiedUser.updatedAt,
            providerId: copiedUser.providerId,
            facebookId: copiedUser.facebookId,
            googleId: copiedUser.googleId,
            verifyPhone: copiedUser.verifyPhone,
            removed: copiedUser.removed,
            invitation: copiedUser.invitation,
            invitationExpiryDate: copiedUser.invitationExpiryDate,
            myInvitationCode: copiedUser.myInvitationCode,
            eula: copiedUser.eula,
          ),
        ),
      );
      if (copiedUser.role == 'Expert') {
        await HiveHelper.cacheProfileInfo(
          name: copiedUser.expert!.name,
          avatar: copiedUser.expert!.logo,
          birthDate: copiedUser.birthDate,
        );
      } else if (copiedUser.role == 'Organization') {
        await HiveHelper.cacheProfileInfo(
          birthDate: copiedUser.birthDate,
          name: copiedUser.organization!.name,
          avatar: copiedUser.organization!.logo,
        );
      } else {
        await HiveHelper.cacheProfileInfo(
          name: copiedUser.name,
          avatar: copiedUser.avatar,
          birthDate: copiedUser.birthDate,
        );
      }
      addedTalents.clear();
      addedCategories.clear();
      emit(GetProfileSuccess());
    } else {
      emit(GetProfileFailed(res.message ?? ""));
    }
  }

  Future<void> getAllUserReels() async {
    emit(GetAllReelsLoading());
    final res = await MediaRepo.getAllUserReels();
    if (res.data != null) {
      reelsList = res.data!;
      emit(GetAllReelsSuccess());
    } else {
      emit(GetAllReelsFailed(res.message ?? ""));
    }
  }

  Future<void> deleteProfileReel({required String reelId}) async {
    emit(DeleteProfileReelLoading());
    final res = await MediaRepo.deleteProfileReel(reelId: reelId);
    if (res.data != null) {
      emit(DeleteProfileReelSuccess());
    } else {
      emit(DeleteProfileReelFailed(res.message ?? ""));
    }
  }

  void testMethod({required String reelId}) {
    reelsList!.removeWhere((reel) => reel.id! == reelId);
    emit(LolXD());
  }

  Future<void> uploadReel(File file) async {
    emit(UploadReelLoading());
    final res = await GeneralRepo.uploadMedia([file]);
    res.fold(
      (urls) async {
        for (var url in urls) {
          final response = await MediaRepo.uploadReels(url: url);
          if (response.message == null) {
            await getAllUserReels();
            emit(UploadReelDone());
          } else {
            UploadReelFailed(response.message ?? '');
          }
        }
      },
      (errorMsg) => emit(UploadReelFailed(errorMsg)),
    );
  }

  Future<void> uploadUserShots(List<UserShotModel> userImages) async {
    final List<String> shotsUrls = List.filled(3, "");
    List<UserShotModel> shotModels = [];
    for (final image in userImages) {
      if (image.imageType == ImageType.url) {
        shotsUrls[image.index] = image.image;
      } else {
        shotModels.add(UserShotModel(
          index: image.index,
          imageType: ImageType.file,
          image: image.image,
        ));
      }
    }
    emit(UploadLoading());
    final res = await GeneralRepo.uploadMedia(
        shotModels.map((e) => File(e.image)).toList());
    res.fold(
      (respUrls) async {
        for (var i = 0; i < respUrls.length; i++) {
          shotsUrls[shotModels[i].index] = respUrls[i];
        }
        emit(UploadDone());

        await ProfileRepo.editProfile(CompleteProfileModel(shots: shotsUrls));
      },
      (errorMsg) => emit(UploadingFailed(errorMsg)),
    );
  }
  Future<void> uploadUserPhotos(List<UserShotModel> userImages) async {
    final List<String> shotsPhotos= [];
    List<UserShotModel> photosModels = [];
    for (final image in userImages) {
      if (image.imageType == ImageType.url) {
        shotsPhotos[image.index] = image.image;
      } else {
        photosModels.add(UserShotModel(
          index: image.index,
          imageType: ImageType.file,
          image: image.image,
        ));
      }
    }
    emit(UploadPhotosLoading());
    final res = await GeneralRepo.uploadMedia(
        photosModels.map((e) => File(e.image)).toList());
    res.fold(
      (respUrls) async {
        for (var i = 0; i < respUrls.length; i++) {
          shotsPhotos[photosModels[i].index] = respUrls[i];
        }        await ProfileRepo.editProfile(CompleteProfileModel(photos: shotsPhotos));

        emit(UploadPhotosDone());

      },
      (errorMsg) => emit(UploadPhotosFailed(errorMsg)),
    );
  }

  Future<void> editSkills() async {
    emit(UpdateSkillLoading());
    final res = await ProfileRepo.editProfile(
        CompleteProfileModel(specialSkills: addedSkillsId));
    if (res.data != null) {
      emit(UpdateSkillSuccess());
    } else {
      emit(UpdateSkillFailed(res.message ?? ""));
    }
  }

  Future<void> editSocialMediaProfile() async {
    emit(SocialMediaProfileLoading());
    final res = await ProfileRepo.editProfile(
        CompleteProfileModel(socialLinks: addedSocialMedia));
    if (res.data != null) {
      emit(SocialMediaProfileSuccess());
    } else {
      emit(SocialMediaProfileFailed(res.message ?? ""));
    }
  }

  // void addAndRemoveSkills({required String skill, required bool isAdd}) {
  //   if (isAdd) {
  //     skills!.add(skill);
  //   } else {
  //     skills!.remove(skill);
  //   }
  // }

  Future<void> editPhysical(
      {String? height, String? weight, String? gender}) async {
    emit(PhysicalLoading());
    final res = await ProfileRepo.editProfile(
      CompleteProfileModel(
          height: double.tryParse(height ?? ""),
          weight: double.tryParse(weight ?? ""),
          gender: gender),
    );
    if (res.data != null) {
      emit(PhysicalSuccess());
    } else {
      emit(PhysicalFailed(res.message ?? ""));
    }
  }

  Future<void> getCategories() async {
    for (var category in usersProfileModel!.categories!) {
      addedCategories.add(category.id!);
    }
    emit(GetCategoriesLoading());
    final res = await ProfileRepo.getCategories();
    if (res.data != null) {
      categoriesList = res.data!;
      emit(GetCategoriesSuccess());
    } else {
      emit(GetCategoriesFailed(res.message ?? ""));
    }
  }

  Future<void> editCategories() async {
    emit(UpdateCategoriesLoading());
    final res = await ProfileRepo.editProfile(
        CompleteProfileModel(categories: addedCategories));
    if (res.data != null) {
      emit(UpdateCategoriesSuccess());
    } else {
      emit(UpdateCategoriesFailed(res.message ?? ""));
    }
  }

  Future<void> editBio() async {
    emit(GetBioLoading());
    final res = await ProfileRepo.editProfile(
        CompleteProfileModel(bio: bioController.text));
    if (res.data != null) {
      emit(GetBioSuccess());
    } else {
      emit(GetBioFailed(res.message ?? ""));
    }
  }

  Future<void> getTalents() async {
    emit(GetTalentsLoading());
    final res = await ProfileRepo.getAllTalents();
    talentsList = res.data;
    if (res.data != null) {
      emit(GetTalentsSuccess());
    } else {
      emit(GetTalentsFailed(res.message ?? ""));
    }
  }
  Future<void> getSkills() async {
    emit(GetSkillsLoading());
    final res = await ProfileRepo.getSkills();
    skills = res.data;
    if (res.data != null) {
      emit(GetSkillsSuccess());
    } else {
      emit(GetSkillsFailed(res.message ?? ""));
    }
  }

  Future<void> editTalents() async {
    emit(UpdateTalentsLoading());
    final res = await ProfileRepo.editProfile(
        CompleteProfileModel(talents: addedTalents));
    if (res.data != null) {
      emit(UpdateTalentsSuccess());
    } else {
      emit(UpdateTalentsFailed(res.message ?? ""));
    }
  }

  void addAndRemoveTalents({required String title, required bool isAdd}) {
    if (isAdd) {
      addedTalents.add(title);
    } else {
      addedTalents.remove(title);
    }
  }

  Future<void> editLook(
      {String? skinColor, String? hairColor, String? eyeColor}) async {
    emit(LookLoading());
    userEyeColor = eyeColor;
    userSkinColor = skinColor;
    userHairColor = hairColor;
    final res = await ProfileRepo.editProfile(
      CompleteProfileModel(
        skinColor: skinColor,
        hairColor: hairColor,
        eyeColor: eyeColor,
        height: double.parse(heightController.text),
        weight: double.parse(weightController.text),
      ),
    );
    if (res.data != null) {
      emit(LookSuccess());
    } else {
      emit(LookFailed(res.message ?? ""));
    }
  }

  List<String> shotsPaths = List.filled(3, "");
  void setShotPath(String path, int index) {
    shotsPaths[index] = path;
  }

  Future<void> editShots({List<String>? photos}) async {
    emit(ShotsLoading());
    shotsPaths = photos!;
    final res =
        await ProfileRepo.editProfile(CompleteProfileModel(shots: photos));
    if (res.data != null) {
      emit(ShotsSuccess());
    } else {
      emit(ShotsFailed(res.message ?? ""));
    }
  }

  String? userPortfolioLink;
  Future<void> editPortfolioLink({String? portfolioLink}) async {
    emit(PortfolioLinkLoading());
    userPortfolioLink = portfolioLink;
    final res = await ProfileRepo.editProfile(CompleteProfileModel(
        portfolioLink: portfolioLink!.trim().toLowerCase()));
    if (res.data != null) {
      emit(PortfolioLinkSuccess());
    } else {
      emit(PortfolioLinkFailed(res.message ?? ""));
    }
  }

  Future<void> editExperiences({
    required List<JobExperienceModel>? experiencesList,
  }) async {
    emit(ExperiencesLoading());
    final res = await ProfileRepo.editProfile(
        CompleteProfileModel(experiences: experiencesList));
    if (res.data != null) {
      emit(ExperiencesSuccess());
    } else {
      emit(ExperiencesFailed(res.message ?? ""));
    }
  }

  Future<void> editEducationExperiences({
    required List<EducationModel>? educationList,
  }) async {
    emit(EducationLoading());
    final res = await ProfileRepo.editProfile(
        CompleteProfileModel(educations: educationList));
    if (res.data != null) {
      emit(EducationSuccess());
    } else {
      emit(EducationFailed(res.message ?? ""));
    }
  }

  Future<void> editUserProfile({
    required String name,
    required String email,
    required String birthDate,
    required String avatar,
    required String nationality,
    required String country,
    required String gender,
    required int actingAgeFrom,
    required int actingAgeTo,
  }) async {
    emit(EditProfileLoading());
    if (profileImage != null) {
      final response = await GeneralRepo.uploadMedia([profileImage!]);
      response.fold(
        (url) async {
          profileImageUrl = url.first;
          emit(UploadProfileImageSuccess());
        },
        (errorMsg) => emit(UploadProfileImageFailed(errorMsg)),
      );
    }
    print("object : $name,$email,$birthDate,$profileImageUrl,$gender,$actingAgeTo,$actingAgeFrom,$country,$nationality");
    final res = await ProfileRepo.editUserProfileData(
      name: name,
      email: email,
      birthDate: birthDate,
      avatar: profileImageUrl == null ? avatar : profileImageUrl!, nationality: nationality, country: country,
      gender: gender, actingAgeFrom: actingAgeFrom,
      actingAgeTo: actingAgeTo,
    );
    if (res.message != null) {
      await getUserData();
      emit(EditProfileFailed(res.message!));
      return;
    }
    if (res.data != null) {
      await getUserData();
      await HiveHelper.cacheProfileInfo(
        avatar: res.data!.avatar,
        birthDate: res.data!.birthDate,
        name: res.data!.name,
        nationality: nationality, country: country,
        gender: gender, actingAgeFrom: actingAgeFrom,
        actingAgeTo: actingAgeTo,
      );
      emit(EditProfileSuccess());
    } else {
      emit(EditProfileFailed(res.message!));
    }
  }

  Future<void> addMemberShip() async {
    if (memberPdf != null) {
      final response = await GeneralRepo.uploadMedia([memberPdf!]);
      response.fold(
        (url) async {
          memberPdfUrl = url.first;
          emit(UploadMemberShipImageSuccess());
        },
        (errorMsg) => emit(UploadMemberShipImageFailed(errorMsg)),
      );
    }
    final res = await ProfileRepo.editProfile(CompleteProfileModel(
        membership: memberId, membershipDocument: memberPdfUrl));
    if (res.data != null) {
      emit(EditMemberSuccess());
    } else {
      emit(EditMemberFailed(res.message ?? ""));
    }
  }

  Future<void> editUserBirthDate({
    required String birthDate,
  }) async {
    emit(EditUserBirthDateLoading());
    final res = await ProfileRepo.editUserBirthDate(birthDate: birthDate);
    if (res.message != null) {
      emit(EditUserBirthDateFailed(res.message!));
      return;
    }
    if (res.data != null) {
      await HiveHelper.cacheUserBirthDate(birthDate: res.data!.birthDate!);
      emit(EditUserBirthDateSuccess());
    } else {
      emit(EditUserBirthDateFailed(res.message!));
    }
  }

  Future<void> updatePassword({
    required String currentPassword,
    required String newPassword,
  }) async {
    emit(UpdatePasswordLoading());
    final res = await ProfileRepo.updatePassword(
      currentPassword: currentPassword,
      newPassword: newPassword,
    );
    if (res.message != null) {
      emit(UpdatePasswordFailed(res.message!));
      return;
    }
    if (res.data != null) {
      emit(UpdatePasswordSuccess());
    } else {
      emit(UpdatePasswordFailed(res.message!));
    }
  }

  Future<void> reportReel({required String reelId}) async {
    emit(ReportReelLoading());
    final res = await ReportRepo.reportItem(
      contentId: reelId,
      contentType: ReportType.reel.type,
    );
    if (res.data != null) {
      emit(ReportReelSuccess(reportModel: res));
    } else {
      emit(ReportReelFailed(res.message ?? ""));
    }
  }

  Future<void> getMembershipList() async {
    emit(GetMemberShipLoading());
    final res = await ProfileRepo.getAllMemberShip();
    memberShipList = res.data;
    if (res.data != null) {
      emit(GetMemberShipSuccess());
    } else {
      emit(GetMemberShipFailed(res.message ?? ""));
    }
  }
}
