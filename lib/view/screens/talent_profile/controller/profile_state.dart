part of 'profile_cubit.dart';

abstract class ProfileState extends Equatable {
  const ProfileState();

  @override
  List<Object> get props => [];
}

class ProfileInitial extends ProfileState {}

class LolXD extends ProfileState {}

class GetProfileSuccess extends ProfileState {}

class GetProfileLoading extends ProfileState {}

class GetProfileFailed extends ProfileState {
  final String msg;
  const GetProfileFailed(this.msg);
}

class GetAllReelsSuccess extends ProfileState {}

class GetAllReelsLoading extends ProfileState {}

class GetAllReelsFailed extends ProfileState {
  final String msg;
  const GetAllReelsFailed(this.msg);
}

class UploadReelDone extends ProfileState {}

class UploadReelLoading extends ProfileState {}

class UploadReelFailed extends ProfileState {
  final String msg;
  const UploadReelFailed(this.msg);
}

class UpdateSkillSuccess extends ProfileState {}

class UpdateSkillLoading extends ProfileState {}

class UpdateSkillFailed extends ProfileState {
  final String msg;
  const UpdateSkillFailed(this.msg);
}
class SocialMediaProfileSuccess extends ProfileState {}

class SocialMediaProfileLoading extends ProfileState {}

class SocialMediaProfileFailed extends ProfileState {
  final String msg;
  const SocialMediaProfileFailed(this.msg);
}

class DeleteProfileReelSuccess extends ProfileState {}

class DeleteProfileReelLoading extends ProfileState {}

class DeleteProfileReelFailed extends ProfileState {
  final String msg;
  const DeleteProfileReelFailed(this.msg);
}

class PhysicalSuccess extends ProfileState {}

class PhysicalLoading extends ProfileState {}

class PhysicalFailed extends ProfileState {
  final String msg;
  const PhysicalFailed(this.msg);
}

class LookSuccess extends ProfileState {}

class LookLoading extends ProfileState {}

class LookFailed extends ProfileState {
  final String msg;
  const LookFailed(this.msg);
}
class EducationSuccess extends ProfileState {}

class EducationLoading extends ProfileState {}

class EducationFailed extends ProfileState {
  final String msg;
  const EducationFailed(this.msg);
}

class ShotsSuccess extends ProfileState {}

class ShotsLoading extends ProfileState {}

class ShotsFailed extends ProfileState {
  final String msg;
  const ShotsFailed(this.msg);
}

class PortfolioLinkSuccess extends ProfileState {}

class PortfolioLinkLoading extends ProfileState {}

class PortfolioLinkFailed extends ProfileState {
  final String msg;
  const PortfolioLinkFailed(this.msg);
}

class ExperiencesSuccess extends ProfileState {}

class ExperiencesLoading extends ProfileState {}

class ExperiencesFailed extends ProfileState {
  final String msg;
  const ExperiencesFailed(this.msg);
}

class EditProfileSuccess extends ProfileState {}

class EditProfileLoading extends ProfileState {}

class EditProfileFailed extends ProfileState {
  final String msg;
  const EditProfileFailed(this.msg);
}class EditMemberSuccess extends ProfileState {}

class EditMemberLoading extends ProfileState {}

class EditMemberFailed extends ProfileState {
  final String msg;
  const EditMemberFailed(this.msg);
}

class EditUserBirthDateSuccess extends ProfileState {}

class EditUserBirthDateLoading extends ProfileState {}

class EditUserBirthDateFailed extends ProfileState {
  final String msg;
  const EditUserBirthDateFailed(this.msg);
}

class UploadLoading extends ProfileState {}

class UploadDone extends ProfileState {}

class UploadingFailed extends ProfileState {
  final String msg;
  const UploadingFailed(this.msg);
}
class UploadPhotosLoading extends ProfileState {}

class UploadPhotosDone extends ProfileState {}

class UploadPhotosFailed extends ProfileState {
  final String msg;
  const UploadPhotosFailed(this.msg);
}

class UploadProfileImageLoading extends ProfileState {}

class UploadProfileImageSuccess extends ProfileState {}

class UploadProfileImageFailed extends ProfileState {
  final String msg;
  const UploadProfileImageFailed(this.msg);
}
class UploadMemberShipLoading extends ProfileState {}

class UploadMemberShipImageSuccess extends ProfileState {}

class UploadMemberShipImageFailed extends ProfileState {
  final String msg;
  const UploadMemberShipImageFailed(this.msg);
}

class UpdatePasswordSuccess extends ProfileState {}

class UpdatePasswordLoading extends ProfileState {}

class UpdatePasswordFailed extends ProfileState {
  final String msg;
  const UpdatePasswordFailed(this.msg);
}

class GetCategoriesSuccess extends ProfileState {}

class GetCategoriesLoading extends ProfileState {}

class GetCategoriesFailed extends ProfileState {
  final String msg;
  const GetCategoriesFailed(this.msg);
}class GetBioSuccess extends ProfileState {}

class GetBioLoading extends ProfileState {}

class GetBioFailed extends ProfileState {
  final String msg;
  const GetBioFailed(this.msg);
}

class UpdateCategoriesSuccess extends ProfileState {}

class UpdateCategoriesLoading extends ProfileState {}

class UpdateCategoriesFailed extends ProfileState {
  final String msg;
  const UpdateCategoriesFailed(this.msg);
}

class GetTalentsSuccess extends ProfileState {}

class GetTalentsLoading extends ProfileState {}

class GetTalentsFailed extends ProfileState {
  final String msg;
  const GetTalentsFailed(this.msg);
}
class GetSkillsSuccess extends ProfileState {}

class GetSkillsLoading extends ProfileState {}

class GetSkillsFailed extends ProfileState {
  final String msg;
  const GetSkillsFailed(this.msg);
}
class GetMemberShipSuccess extends ProfileState {}

class GetMemberShipLoading extends ProfileState {}

class GetMemberShipFailed extends ProfileState {
  final String msg;
  const GetMemberShipFailed(this.msg);
}

class UpdateTalentsSuccess extends ProfileState {}

class UpdateTalentsLoading extends ProfileState {}

class UpdateTalentsFailed extends ProfileState {
  final String msg;
  const UpdateTalentsFailed(this.msg);
}

class ReportReelSuccess extends ProfileState {
  final BaseSingleResponse<ReportModel> reportModel;
  const ReportReelSuccess({required this.reportModel});
}

class ReportReelLoading extends ProfileState {}

class ReportReelFailed extends ProfileState {
  final String msg;
  const ReportReelFailed(this.msg);
}
