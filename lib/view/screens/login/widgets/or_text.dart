import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import '../../../../utils/constants/resources.dart';

class OrText extends StatelessWidget {
  const OrText({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: height * 0.02),
      child: Text(
        'or'.tr().toUpperCase(),
        textAlign: TextAlign.center,
        style: Theme.of(context).textTheme.bodySmall!.copyWith(
              color: AppColors.greyTxtColor,
              fontFamily: isEn ? 'Montserrat' : 'Tajawal',
              fontWeight: FontWeight.bold,
            ),
      ),
    );
  }
}
