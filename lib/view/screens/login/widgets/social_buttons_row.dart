// ignore_for_file: use_build_context_synchronously

import 'dart:io';

import 'package:easy_localization/easy_localization.dart' as local;
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:nxt/utils/helpers/popup_helper.dart';
import '../../../../data/service/hive_services.dart';
import '../../../../utils/constants/resources.dart';
import '../../../widgets/loading_stack.dart';
import '../../welcome_to_project/check_box_cubit/check_box_cubit.dart';
import '../cubit/login_cubit.dart';

class SocialButtonsRow extends StatelessWidget {
  const SocialButtonsRow({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<LoginCubit, LoginState>(
      builder: (context, loginState) {
        return Padding(
          padding: EdgeInsets.symmetric(horizontal: width * 0.07),
          child: BlocBuilder<CheckboxCubit, CheckboxState>(
            builder: (context, checkboxState) {
              final isChecked = context.read<CheckboxCubit>().state.isChecked;
              return Row(
                textDirection: TextDirection.ltr,
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SocialButton(
                      child: loginState is FacebookSignInLoadingState
                          ? LoadingStack(widget: SvgPicture.asset('assets/images/facebook.svg'))
                          : SvgPicture.asset('assets/images/facebook.svg'),
                      onTap: () async {
                        if (isChecked == false) {
                          PopupHelper.showBasicSnack(msg: 'makeSureAll'.tr(), color: Colors.red);
                          return;
                        }
                        await HiveHelper.logout();
                        await LoginCubit.get(context).facebookSignIn();
                      }),

                  SizedBox(width: width * 0.04),
                  SocialButton(
                      child: loginState is GoogleSignInLoadingState
                          ? LoadingStack(widget: SvgPicture.asset('assets/images/g+.svg'))
                          : SvgPicture.asset('assets/images/g+.svg'),
                      onTap: () async {
                        if (isChecked == false) {
                          PopupHelper.showBasicSnack(msg: 'makeSureAll'.tr(), color: Colors.red);
                          return;
                        }
                        await HiveHelper.logout();
                        await LoginCubit.get(context).googleSignIn();
                      }),
                  SizedBox(width: width * 0.04),
                  Offstage(
                    offstage: Platform.isAndroid,
                    child: Row(
                      children: <Widget>[
                        SizedBox(width: width * 0.04),
                        SocialButton(
                            child:  SvgPicture.asset('assets/images/apple.svg'),
                            onTap: () async {
                              if (isChecked == false) {
                                PopupHelper.showBasicSnack(msg: 'makeSureAll'.tr(), color: Colors.red);
                                return;
                              }
                              // await HiveHelper.logout();
                              // await LoginCubit.get(context).googleSignIn();
                            })
                      ],
                    ),
                  ),
                  // SizedBox(width: width * 0.04),
                  SocialButton(
                      child: SvgPicture.asset('assets/images/tiktok.svg'),
                      onTap: () async {
                        if (isChecked == false) {
                          PopupHelper.showBasicSnack(msg: 'makeSureAll'.tr(), color: Colors.red);
                          return;
                        }
                        // await HiveHelper.logout();
                        // await LoginCubit.get(context).googleSignIn();
                      }),
                  SizedBox(width: width * 0.04),
                  SocialButton(
                      child:  SvgPicture.asset('assets/images/snapchat.svg'),
                      onTap: () async {
                        if (isChecked == false) {
                          PopupHelper.showBasicSnack(msg: 'makeSureAll'.tr(), color: Colors.red);
                          return;
                        }
                        // await HiveHelper.logout();
                        // await LoginCubit.get(context).googleSignIn();
                      }),
                ],
              );
            },
          ),
        );
      },
    );
  }
}

class SocialButton extends StatelessWidget {
  const SocialButton({Key? key, required this.child, this.onTap}) : super(key: key);
  final Widget child;
  final Function()? onTap;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        border: Border.all(color: const Color(0xFFDDDDDD)),
      ),
      child: InkWell(
        onTap: onTap,
        customBorder: const CircleBorder(),
        child: child,
      ),
    );
  }
}
