import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import '../../../../config/router/router.dart';
import '../../../../utils/constants/resources.dart';
import '../../sign_up/talent/talent_sign_up_screen.dart';

class SignUpWidget extends StatelessWidget {
  const SignUpWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.bottomCenter,
      child: ElevatedButton(
        onPressed: () => MagicRouter.navigateTo(const TalentSignUpScreen()),
        style: ElevatedButton.styleFrom(shape: const RoundedRectangleBorder(borderRadius: BorderRadius.zero)),
        child: SizedBox(
          height: height * 0.08,
          child: Center(
            child: RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                text: '${'firstTime'.tr()} ',
                style: TextStyle(
                  fontSize: 16,
                  color: Colors.white,
                  fontWeight: FontWeight.w600,
                  fontFamily: isEn ? 'Montserrat' : 'Tajawal',
                ),
                children: [
                  TextSpan(
                    text: 'singUp'.tr().toUpperCase(),
                    style: TextStyle(
                      fontSize: 18,
                      color: Colors.white,
                      fontWeight: FontWeight.w900,
                      fontFamily: isEn ? 'Montserrat' : 'Tajawal',
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
