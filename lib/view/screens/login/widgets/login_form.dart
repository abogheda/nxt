// ignore_for_file: use_build_context_synchronously

import 'package:easy_localization/easy_localization.dart' as local;
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import '../../../../utils/constants/resources.dart';
import '../../../../utils/helpers/popup_helper.dart';
import '../../../../config/router/router.dart';
import '../../../../utils/helpers/validators.dart';
import '../../../widgets/app_loader.dart';
import '../../../widgets/custom_text_field.dart';
import '../../../widgets/custom_button.dart';
import '../../forgot_password/forgot_password_screen.dart';
import '../../sign_up/talent/custom_intl_phone_field.dart';
import '../cubit/login_cubit.dart';

class LoginForm extends StatelessWidget {
  const LoginForm({Key? key, required this.cubit, required this.state}) : super(key: key);

  final LoginCubit cubit;
  final LoginState state;
  Future<void> validateAndLogin() async {
    if (!(await hasInternetConnection)) {
      PopupHelper.showBasicSnack(msg: "internet_error".tr(), color: Colors.red);
      return;
    }
    cubit.formKey.currentState!.save();
    if (cubit.formKey.currentState!.validate()) {
      FocusManager.instance.primaryFocus?.unfocus();
      await FirebaseAnalytics.instance.logLogin(loginMethod: 'email');
      await cubit.userLogin();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 24),
      child: Form(
        key: cubit.formKey,
        child: Column(
          children: [
            Directionality(
              textDirection: TextDirection.ltr,
              child: CustomIntlPhoneField(
                hintText: 'phoneNumber'.tr(),
                invalidNumberMessage: "validation.enter_valid_phone".tr(),
                controller: cubit.phoneController,
                onSaved: (phone) => cubit.countryCode = phone!.countryCode,
                onSubmitted: (val) {
                  FocusScope.of(context).requestFocus(cubit.passwordFocusNode);
                  cubit.formKey.currentState!.save();
                },
              ),
            ),
            const SizedBox(height: 4),
            Directionality(
              textDirection: TextDirection.ltr,
              child: CustomTextField(
                prefix: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: SvgPicture.asset('assets/images/lock.svg', color: AppColors.iconsColor, width: 24),
                ),
                hint: 'password'.tr(),
                validator: Validators.password,
                type: TextInputType.visiblePassword,
                controller: cubit.passwordController,
                focusNode: cubit.passwordFocusNode,
                onFieldSubmitted: (_) async => validateAndLogin(),
              ),
            ),
            CheckBoxAndResetPassRow(cubit: cubit),
            SizedBox(height: height * 0.015),
            SizedBox(
              height: height * 0.055,
              child: (state is LoginLoadingState)
                  ? const AppLoader()
                  : CustomButton(
                      onTap: () async => validateAndLogin(),
                      width: double.infinity,
                      height: double.infinity,
                      child: Text('login.login'.tr().toUpperCase(),
                          style: TextStyle(fontFamily: isEn ? 'Montserrat' : 'Tajawal',
                              fontWeight: FontWeight.w700,fontSize: 13.sp))),
            ),
          ],
        ),
      ),
    );
  }
}

class CheckBoxAndResetPassRow extends StatelessWidget {
  const CheckBoxAndResetPassRow({Key? key, required this.cubit}) : super(key: key);
  final LoginCubit cubit;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Checkbox(
              value: cubit.isChecked,
              materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
              onChanged: (value) => cubit.changeCheckBox(value),
            ),
            Text(
              'keep_me'.tr(),
              softWrap: true,
              style: TextStyle(
                fontFamily: isEn ? 'Montserrat' : 'Tajawal',
                fontWeight: FontWeight.bold,
                color: Colors.black,
                fontSize: 12.sp
              )
            ),
          ],
        ),
        // Spacer(),
        Align(
          alignment: isEn ? Alignment.centerRight : Alignment.centerLeft,
          child: TextButton(
              onPressed: () => MagicRouter.navigateTo(const ForgotPasswordScreen()),
              child: FittedBox(
                child: Text(
                  'forgetPassword'.tr().toUpperCase(),
                  style: TextStyle(
                    fontFamily: isEn ? 'Montserrat' : 'Tajawal',
                    fontSize: 10.sp,
                    color: AppColors.greyTxtColor
                  )
                ),
              )),
        ),
      ],
    );
  }
}
