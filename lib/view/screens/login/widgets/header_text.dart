import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import '../../../../utils/constants/resources.dart';

class HeaderText extends StatelessWidget {
  const HeaderText({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: width * 0.19),
      child: Column(
        children: [
          FittedBox(
            child: Text(
              "${'hello'.tr()},",
              style: Theme.of(context).textTheme.titleLarge!.copyWith(fontSize: width, height: 1),
              textAlign: TextAlign.center,
            ),
          ),
          FittedBox(
            // fit: BoxFit.fitWidth,
            child: SizedBox(
              child: Text(
                'nxtTalent'.tr(),
                style: Theme.of(context).textTheme.bodyLarge!.copyWith(fontSize: width, height: 1),
                textAlign: TextAlign.center,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
