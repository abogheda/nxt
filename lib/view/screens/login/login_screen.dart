import 'dart:io';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:nxt/view/screens/login/widgets/social_buttons_row.dart';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nxt/view/screens/login/widgets/title_text.dart';
import 'package:nxt/view/screens/sign_up/talent/talent_verification_screen.dart';

import '../../../config/router/router.dart';
import '../../../data/service/hive_services.dart';
import '../../../utils/constants/resources.dart';
import '../../../utils/helpers/popup_helper.dart';
import '../complete_profile_screen/widgets/screen_sections/select_vertical_screen.dart';
import '../sign_up/talent/controller/talent_sign_up_cubit.dart';
import '../../../utils/helpers/update_dialog.dart';
import '../../widgets/logo_app_bar.dart';
import '../navigation_and_appbar/import_widget.dart';
import '../subscribe/subscription_ended_screen.dart';
import 'cubit/login_cubit.dart';
import 'widgets/header_text.dart';
import 'widgets/login_form.dart';
import 'widgets/sign_up_widget.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  @override
  void initState() {
    checkNewVersion(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => LoginCubit()..onInit(),
      child: BlocConsumer<LoginCubit, LoginState>(
        listener: (_, state) async {
          //==== Number and password login ====
          if (state is LoginErrorState) PopupHelper.showBasicSnack(msg: state.error.toString(), color: Colors.red);
          if (state is LoginSuccessState) {
            if (state.userModel.token == null || state.userModel.token == '') {
              PopupHelper.showBasicSnack(msg: state.userModel.message.toString(), color: Colors.red);
              return;
            }
            if (state.userModel.token != null) {
              //==== talent handle sign up steps ====
              if (HiveHelper.isRegularUser) {
                if (state.userModel.user!.verify == false) {
                  MagicRouter.navigateAndPopAll(BlocProvider(
                      create: (context) => TalentSignUpCubit()..sendCodeAndStartTimer(),
                      child: const TalentVerificationScreen()));
                  return;
                }
                // if (state.userModel.user!.terms == false) {
                //   MagicRouter.navigateAndPopAll(BlocProvider(
                //       create: (context) => TalentSignUpCubit()..getLegal(),
                //       child: const TalentConfirmTermsScrollableScreen()));
                //   return;
                // }
                // if (state.userModel.user!.privacy == false) {
                //   MagicRouter.navigateAndPopAll(BlocProvider(
                //       create: (context) => TalentSignUpCubit()..getLegal(),
                //       child: const TalentConfirmPrivacyScrollableScreen()));
                //   return;
                // }
                // if (state.userModel.user!.eula == false) {
                //   MagicRouter.navigateAndPopAll(BlocProvider(
                //       create: (context) => TalentSignUpCubit()..getLegal(),
                //       child: const TalentConfirmEULAScrollableScreen()));
                //   return;
                // }
                // if ((state.userModel.user!.invitation ?? false) == false) {
                //   MagicRouter.navigateAndPopAll(BlocProvider(
                //       create: (context) => TalentSignUpCubit(),
                //       child: const TalentInvitationScreen(isFirstTime: false)));
                //   return;
                // }
                // if (state.userModel.user!.invitationExpiryDate != null) {
                //   if (state.userModel.user!.invitation ??
                //       false == true &&
                //           DateTime.now().isAfter(DateTime.parse(
                //               state.userModel.user!.invitationExpiryDate!))) {
                //     MagicRouter.navigateAndPopAll(const InvitationEndScreen());
                //   }
                // }
                else if (state.userModel.user!.subscriptionEnd ?? false == true) {
                  MagicRouter.navigateAndPopAll(const SubscriptionEndedScreen());
                } else {
                  MagicRouter.navigateAndPopAll(Navigation());
                  return;
                }
              }
              // //==== expert handle sign up steps ====
              // else if (state.userModel.user!.role == 'Expert') {
              //   if (state.userModel.user!.verify == false) {
              //     MagicRouter.navigateAndPopAll(BlocProvider(
              //         create: (context) => ExpertSignUpCubit()..sendCodeAndStartTimer(),
              //         child: const ExpertVerificationScreen()));
              //     return;
              //   }
              //   if (state.userModel.user!.terms == false) {
              //     MagicRouter.navigateAndPopAll(BlocProvider(
              //         create: (context) => ExpertSignUpCubit()..getLegal(),
              //         child: const ExpertConfirmTermsScrollableScreen()));
              //     return;
              //   }
              //   if (state.userModel.user!.privacy == false) {
              //     MagicRouter.navigateAndPopAll(BlocProvider(
              //         create: (context) => ExpertSignUpCubit()..getLegal(),
              //         child: const ExpertConfirmPrivacyScrollableScreen()));
              //     return;
              //   }
              //   if (state.userModel.user!.eula == false) {
              //     MagicRouter.navigateAndPopAll(BlocProvider(
              //         create: (context) => ExpertSignUpCubit()..getLegal(),
              //         child: const ExpertConfirmEULAScrollableScreen()));
              //     return;
              //   }
              //   if (state.userModel.user!.expert == null) {
              //     MagicRouter.navigateAndPopAll(
              //         BlocProvider(create: (context) => ExpertSignUpCubit(), child: const ExpertDetailsScreen()));
              //     return;
              //   }
              // }
              // //==== organization handle sign up steps ====
              // else if (state.userModel.user!.role == 'Organization') {
              //   if (state.userModel.user!.verify == false) {
              //     MagicRouter.navigateAndPopAll(BlocProvider(
              //         create: (context) => OrganizationSignUpCubit()..sendCodeAndStartTimer(),
              //         child: const OrganizationVerificationScreen()));
              //     return;
              //   }
              //   if (state.userModel.user!.terms == false) {
              //     MagicRouter.navigateAndPopAll(BlocProvider(
              //         create: (context) => OrganizationSignUpCubit()..getLegal(),
              //         child: const OrganizationConfirmTermsScrollableScreen()));
              //     return;
              //   }
              //   if (state.userModel.user!.privacy == false) {
              //     MagicRouter.navigateAndPopAll(BlocProvider(
              //         create: (context) => OrganizationSignUpCubit()..getLegal(),
              //         child: const OrganizationConfirmPrivacyScrollableScreen()));
              //     return;
              //   }
              //   if (state.userModel.user!.eula == false) {
              //     MagicRouter.navigateAndPopAll(BlocProvider(
              //         create: (context) => OrganizationSignUpCubit()..getLegal(),
              //         child: const OrganizationConfirmEULAScrollableScreen()));
              //     return;
              //   }
              // } else {
              //   MagicRouter.navigateAndPopAll(Navigation());
              //   return;
              // }
              //==== most probably it will never reach this if statement ====
              if (HiveHelper.getUserInfo!.user!.profileStatus == "NotCompleted") {
                if (HiveHelper.isRegularUser) {
                  MagicRouter.navigateAndPopAll(const SelectVerticalScreen());
                  // MagicRouter.navigateAndPopAll(const CompleteProfileScreen());
                } else {
                  MagicRouter.navigateAndPopAll(Navigation());
                }
              } else {
                MagicRouter.navigateAndPopAll(Navigation());
              }
            }
          }
          if (state is GoogleSignInSuccessState) {
            if (state.userModel.token == null || state.userModel.token == '') {
              PopupHelper.showBasicSnack(msg: state.userModel.message.toString(), color: Colors.red);
              return;
            }
            if (state.userModel.user!.verify == false) {
              PopupHelper.showBasicSnack(msg: 'notVerified'.tr(), color: Colors.red);
              await HiveHelper.logout();
              return;
            }
            if (state.userModel.token != null) {
              if (HiveHelper.getUserInfo!.user!.profileStatus == "NotCompleted") {
                MagicRouter.navigateAndPopAll(const SelectVerticalScreen());
                // MagicRouter.navigateAndPopAll(const CompleteProfileScreen());
              } else {
                MagicRouter.navigateAndPopAll(Navigation());
              }
            }
          }
          //==== Facebook login ====
          if (state is FacebookSignInSuccessState) {
            if (state.userModel.token == null || state.userModel.token == '') {
              PopupHelper.showBasicSnack(msg: state.userModel.message.toString(), color: Colors.red);
              return;
            }
            if (state.userModel.user!.verify == false) {
              PopupHelper.showBasicSnack(msg: 'notVerified'.tr(), color: Colors.red);
              await HiveHelper.logout();
              return;
            }
            if (state.userModel.token != null) {
              if (HiveHelper.getUserInfo!.user!.profileStatus == "NotCompleted") {
                MagicRouter.navigateAndPopAll(const SelectVerticalScreen());
                // MagicRouter.navigateAndPopAll(const CompleteProfileScreen());
              } else {
                MagicRouter.navigateAndPopAll(Navigation());
              }
            }
            if (state is FacebookSignInErrorState) {
              PopupHelper.showBasicSnack(msg: state.userModel.message.toString(), color: Colors.red);
            }
          }
        },
        builder: (context, state) {
          final cubit = LoginCubit.get(context);
          return Scaffold(
            resizeToAvoidBottomInset: Platform.isAndroid ? false : true,
            body: Stack(
              children: <Widget>[
                ListView(
                  shrinkWrap: true,
                  children: [
                    const LogoAppBar(),
                    SizedBox(height: widget.height * 0.03),
                    const HeaderText(),
                    TitleText(text: 'logIntoAcc'.tr()),
                    LoginForm(cubit: cubit, state: state),
                    SizedBox(height: 22.h),
                    Align(
                      alignment: Alignment.center,
                      child: Text(
                        'or'.tr().toUpperCase(),
                        maxLines: 1,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 14.sp,
                          color: AppColors.greyTxtColor,
                          fontWeight: FontWeight.w600,
                          fontFamily: 'Montserrat',
                        ),
                      ),
                    ),
                    SizedBox(height: widget.height * 0.015),
                    const SocialButtonsRow(),
                    SizedBox(height: widget.height * 0.1 +
                        MediaQuery.of(context).viewInsets.bottom * 0.8),
                  ],
                ),
                const SignUpWidget(),
              ],
            ),
          );
        },
      ),
    );
  }
}
