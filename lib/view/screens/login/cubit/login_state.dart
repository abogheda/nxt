part of 'login_cubit.dart';

abstract class LoginState extends Equatable {
  const LoginState();

  @override
  List<Object> get props => [];
}

class LoginInitial extends LoginState {}

class LoginChangePasswordVisibilityState extends LoginState {}

class GetAppInfoSuccess extends LoginState {}

class GetAppInfoLoading extends LoginState {}

class GetAppInfoFailed extends LoginState {
  final String msg;
  const GetAppInfoFailed(this.msg);
}

//===============================================================
class ChangeCheckBoxState extends LoginState {}

//===============================================================

class LoginLoadingState extends LoginState {}

class LoginSuccessState extends LoginState {
  final UserModel userModel;

  const LoginSuccessState({required this.userModel});
}

class LoginErrorState extends LoginState {
  final String error;

  const LoginErrorState({required this.error});
}

//===============================================================
class GoogleSignInLoadingState extends LoginState {}

class GoogleSignInSuccessState extends LoginState {
  final UserModel userModel;

  const GoogleSignInSuccessState({required this.userModel});
}

class GoogleSignInErrorState extends LoginState {
  final String error;

  const GoogleSignInErrorState({required this.error});
}
//===============================================================
class AppleSignInLoadingState extends LoginState {}

class AppleSignInSuccessState extends LoginState {
  final UserModel userModel;

  const AppleSignInSuccessState({required this.userModel});
}

class AppleSignInErrorState extends LoginState {
  final String error;

  const AppleSignInErrorState({required this.error});
}
//===============================================================
class FacebookSignInLoadingState extends LoginState {}

class FacebookSignInSuccessState extends LoginState {
  final UserModel userModel;

  const FacebookSignInSuccessState({required this.userModel});
}

class FacebookSignInErrorState extends LoginState {
  final String error;

  const FacebookSignInErrorState({required this.error});
}
//===============================================================
