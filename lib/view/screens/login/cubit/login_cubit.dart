import 'dart:developer';

import 'package:easy_localization/easy_localization.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:sign_in_with_apple/sign_in_with_apple.dart';

import '../../../../data/models/user_model.dart';
import '../../../../data/repos/auth/auth_repo.dart';
import '../../../../data/service/hive_services.dart';
import '../../../../data/service/remote_service.dart';
import '../../../../utils/helpers/google_sign_in_helper.dart';

part 'login_state.dart';

class LoginCubit extends Cubit<LoginState> {
  LoginCubit() : super(LoginInitial());

  static LoginCubit get(context) => BlocProvider.of(context);

  @override
  Future<void> close() {
    phoneController.dispose();
    passwordController.dispose();
    passwordFocusNode.dispose();
    return super.close();
  }

  UserModel? userModel;
  bool? isChecked = false;
  // bool isHidden = true;
  // bool isAndroidHidden = true;
  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  TextEditingController phoneController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  FocusNode passwordFocusNode = FocusNode();
  String countryCode = '';
  void onInit() async {
    await HiveHelper.cacheKeepMeLoggedIn(value: isChecked!);
  }

  void changeCheckBox(bool? value) async {
    isChecked = value!;
    emit(ChangeCheckBoxState());
    emit(LoginInitial());
  }

  Future<void> userLogin() async {
    await HiveHelper.logout();
    emit(LoginLoadingState());
    final res = await AuthRepo.userLogin(
      password: passwordController.text.trim(),
      phone: countryCode + phoneController.text.trim().toString(),
    );
    if (res.data != null) {
      userModel = res.data;
      if (userModel!.token != null) {
        APIService.setHeaderToken(token: userModel!.token);
        await HiveHelper.cacheUserInfo(token: userModel!.token!.toString(), userModel: userModel!);
        await cacheUserModelWithInfo(role: userModel!.user!.role);
        await HiveHelper.checkAndCacheCountry(userPhone: userModel!.user!.phone);
      }
      await HiveHelper.cacheKeepMeLoggedIn(value: isChecked!);
      emit(LoginSuccessState(userModel: userModel!));
    } else {
      emit(LoginErrorState(error: res.message!));
    }
  }

  //===============================================================
  Future<void> googleSignIn() async {
    emit(GoogleSignInLoadingState());
    try {
      final user = await GoogleSIgnInApi.login();
      final res = await AuthRepo.googleSignIn(token: user!.accessToken!);
      if (res.data != null) {
        userModel = res.data;
        if (userModel!.token != null && userModel!.user!.verify == true) {
          APIService.setHeaderToken(token: userModel!.token);
          await HiveHelper.cacheUserInfo(token: userModel!.token!.toString(), userModel: userModel!);
          await cacheUserModelWithInfo(role: userModel!.user!.role);
        }
        await HiveHelper.cacheKeepMeLoggedIn(value: isChecked!);
        emit(GoogleSignInSuccessState(userModel: userModel!));
      } else {
        await GoogleSignIn().signOut();
        emit(GoogleSignInErrorState(error: res.message!));
      }
    } catch (e) {
      log('!=!=!=' * 22);
      log(e.toString());
      log('!=!=!=' * 22);
      await GoogleSignIn().signOut();
      emit(GoogleSignInErrorState(error: "errorOccurred".tr()));
    }
  }
  Future<String?> appleAuth() async {
    emit(AppleSignInLoadingState());
    final credential = await SignInWithApple.getAppleIDCredential(
      scopes: [
        AppleIDAuthorizationScopes.email,
        AppleIDAuthorizationScopes.fullName,
      ],
      webAuthenticationOptions: WebAuthenticationOptions(
        clientId: 'package name',
        redirectUri: Uri.parse(
          "flutter-sign-in-with-apple-example.glitch.me",
        ),
      ),
    );
    try {

      // todo : waiting api for apple login
      final res = await AuthRepo.googleSignIn(token: credential.identityToken!);
      if (res.data != null) {

        userModel = res.data;
        if (userModel!.token != null && userModel!.user!.verify == true) {
          APIService.setHeaderToken(token: userModel!.token);
          await HiveHelper.cacheUserInfo(token: userModel!.token!.toString(), userModel: userModel!);
          await cacheUserModelWithInfo(role: userModel!.user!.role);
        }
        await HiveHelper.cacheKeepMeLoggedIn(value: isChecked!);
        emit(AppleSignInSuccessState(userModel: userModel!));
      } else {
        // await SignInWithApple;
        emit(AppleSignInErrorState(error: res.message!));
        // return credential.identityToken;
      }
    } catch (e) {
      log('!=!=!=' * 22);
      log(e.toString());
      log('!=!=!=' * 22);
      // await GoogleSignIn().signOut();
      emit(GoogleSignInErrorState(error: "errorOccurred".tr()));
    }
    return credential.identityToken;

  }

  Future<void> facebookSignIn() async {
    try {
      emit(FacebookSignInLoadingState());
      final LoginResult user = await FacebookAuth.instance.login(permissions: ['email', 'public_profile']);
      final res = await AuthRepo.facebookSignIn(token: user.accessToken!.token);
      if (res.data != null) {
        userModel = res.data;
        if (userModel!.token != null && userModel!.user!.verify == true) {
          APIService.setHeaderToken(token: userModel!.token);
          await HiveHelper.cacheUserInfo(token: userModel!.token!.toString(), userModel: userModel!);
          await cacheUserModelWithInfo(role: userModel!.user!.role);
        }
        await HiveHelper.cacheKeepMeLoggedIn(value: isChecked!);
        emit(FacebookSignInSuccessState(userModel: userModel!));
      } else {
        await FacebookAuth.instance.logOut();
        emit(FacebookSignInErrorState(error: res.message!));
      }
    } catch (e) {
      await FacebookAuth.instance.logOut();
      emit(FacebookSignInErrorState(error: e.toString()));
    }
  }

  Future<void> cacheUserModelWithInfo({required String? role}) async {
    //TODO: add the expert cache here
    if (role == 'Expert') {
      if (userModel!.user!.expert == null) {
        await HiveHelper.cacheProfileInfo(
          name: userModel!.user!.name,
          avatar: userModel!.user!.avatar,
          birthDate: userModel!.user!.birthDate,
        );
      } else {
        await HiveHelper.cacheProfileInfo(
          name: userModel!.user!.expert!.name,
          avatar: userModel!.user!.expert!.logo,
          birthDate: userModel!.user!.birthDate,
        );
      }
    } else if (role == 'Organization') {
      if (userModel!.user!.organization == null) {
        await HiveHelper.cacheProfileInfo(
          name: userModel!.user!.name,
          avatar: userModel!.user!.avatar,
          birthDate: userModel!.user!.birthDate,
        );
      } else {
        await HiveHelper.cacheProfileInfo(
          birthDate: userModel!.user!.birthDate,
          name: userModel!.user!.organization!.name,
          avatar: userModel!.user!.organization!.logo,
        );
      }
    } else {
      await HiveHelper.cacheProfileInfo(
        name: userModel!.user!.name,
        avatar: userModel!.user!.avatar,
        birthDate: userModel!.user!.birthDate,
      );
    }
  }
}

/*
final fb = FacebookLogin();


try {
      final res = await fb.logIn(permissions: [
        FacebookPermission.publicProfile,
        FacebookPermission.email,
      ]);

      switch (res.status) {
        case FacebookLoginStatus.success:
          final FacebookAccessToken? accessToken = res.accessToken;

          myPrint('#### Access token ==> ${accessToken!.token}');

          final profile = await fb.getUserProfile();
          final imageUrl = await fb.getProfileImageUrl(width: 300, height: 300);
          final email = await fb.getUserEmail();

          myPrint('#### Profile Name ==> ${profile!.name}');
          myPrint('#### User ID ==> ${profile.userId}');
          myPrint('#### Profile image ==> $imageUrl');

          if (email != null) {
            myPrint('#### Email ==> $email');
            await showLoginSuccessAlert(context, tr(context, "facebook"));
            await completeLoginWithFacebook(
                context, accessToken.token, profile.userId, email);
          }

          break;
        case FacebookLoginStatus.cancel:
          await showLoginFailureAlert(context, tr(context, "facebook"));
          break;
        case FacebookLoginStatus.error:
          await showLoginFailureAlert(context, tr(context, "facebook"));
          myPrint('#### Error while log in: ${res.error}');
          break;
      }
    } catch (e) {
      myPrint("ERROR ==> ${e.toString()}");
    }
 */
