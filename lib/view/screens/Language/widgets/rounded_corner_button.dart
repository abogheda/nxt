import 'package:flutter/material.dart';

class RoundedCornerButton extends StatelessWidget {
  final Widget child;
  final Function() onPressed;
  final bool? isSelected;

  const RoundedCornerButton({
    Key? key,
    this.isSelected = false,
    required this.onPressed,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 50,
      child: OutlinedButton(
        style: ButtonStyle(
          elevation: MaterialStateProperty.all(0),
          alignment: Alignment.center,
          side: MaterialStateProperty.all(const BorderSide(width: 1, color: Colors.black)),
          backgroundColor: MaterialStateProperty.all(isSelected! ? Colors.black : Colors.white),
          shape: MaterialStateProperty.all(
            RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
          ),
        ),
        onPressed: onPressed,
        child: child,
      ),
    );
  }
}
