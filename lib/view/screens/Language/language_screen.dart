import 'package:flutter/material.dart';
import '../../../utils/helpers/update_dialog.dart';
import 'package:flutter_svg/svg.dart';
import '../../../utils/constants/app_const.dart';
import 'component/continue_button.dart';

import 'component/information_text.dart';
import 'component/lang_buttons_row.dart';

class LanguageScreen extends StatefulWidget {
  const LanguageScreen({Key? key}) : super(key: key);

  @override
  State<LanguageScreen> createState() => _LanguageScreenState();
}

class _LanguageScreenState extends State<LanguageScreen> {
  @override
  void initState() {
    checkNewVersion(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: widget.width * 0.07),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              SizedBox(height: widget.height * 0.1),
              SvgPicture.asset('assets/images/fab_logo.svg', width: widget.width * 0.8),
              // Image.asset('assets/images/splash.png', width: widget.width * 0.5),
              SizedBox(height: widget.height * 0.03),
              const InformationText(),
              SizedBox(height: widget.height * 0.07),
              const LangButtonsRow(),
              SizedBox(height: widget.height * 0.03),
              const ContinueButton(),
            ],
          ),
        ),
      ),
    );
  }
}
