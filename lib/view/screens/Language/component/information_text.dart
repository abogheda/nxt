import 'package:flutter/material.dart';

class InformationText extends StatelessWidget {
  const InformationText({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Text(
          'الرجاء تحديد اللغة',
          style: Theme.of(context).textTheme.titleMedium!.copyWith(
                fontSize: 18.0,
                fontWeight: FontWeight.w400,
                fontFamily: 'Tajawal',
              ),
        ),
        const SizedBox(height: 4.0),
        SizedBox(
          width: 230,
          child: Text(
            'Please select your language of the App',
            textAlign: TextAlign.center,
            style: Theme.of(context)
                .textTheme
                .titleMedium!
                .copyWith(fontSize: 16.0, fontWeight: FontWeight.w500, fontFamily: 'Montserrat'),
          ),
        ),
      ],
    );
  }
}
