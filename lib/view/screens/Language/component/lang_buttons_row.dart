// ignore_for_file: implementation_imports

import 'package:flutter/material.dart';
import 'package:easy_localization/src/public_ext.dart';
import '../../../../utils/constants/app_const.dart';
import '../widgets/rounded_corner_button.dart';

class LangButtonsRow extends StatelessWidget {
  const LangButtonsRow({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      textDirection: TextDirection.ltr,
      children: <Widget>[
        Expanded(
          child: RoundedCornerButton(
            isSelected: AppConst.isEn,
            onPressed: () {
              if (context.locale.languageCode == 'ar') {
                context.setLocale(const Locale('en', 'US'));
              }
            },
            child: Text(
              'English',
              softWrap: true,
              style: Theme.of(context).textTheme.titleMedium!.copyWith(
                  fontSize: 18.0,
                  fontWeight: FontWeight.w400,
                  color: AppConst.isEn ? Colors.white : Colors.black,
                  fontFamily: 'Montserrat'),
            ),
          ),
        ),
        const SizedBox(width: 12.0),
        Expanded(
          child: RoundedCornerButton(
            isSelected: context.locale.languageCode == 'ar',
            onPressed: () {
              if (AppConst.isEn) {
                context.setLocale(const Locale('ar', 'EG'));
              }
            },
            child: Text(
              'العربية',
              softWrap: true,
              style: Theme.of(context).textTheme.titleMedium!.copyWith(
                    fontSize: 18.0,
                    fontWeight: FontWeight.w400,
                    fontFamily: 'Tajawal',
                    color: context.locale.languageCode == 'ar' ? Colors.white : Colors.black,
                  ),
            ),
          ),
        ),
      ],
    );
  }
}
