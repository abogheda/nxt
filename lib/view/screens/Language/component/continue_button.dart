import 'package:flutter/material.dart';
import '../../../../config/router/router.dart';
import '../../../../utils/constants/resources.dart';
import '../../click_to_start/click_to_start_screen.dart';

class ContinueButton extends StatelessWidget {
  const ContinueButton({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 50,
      width: double.infinity,
      child: OutlinedButton(
        style: ButtonStyle(
          elevation: MaterialStateProperty.all(0),
          alignment: Alignment.center,
          side: MaterialStateProperty.all(const BorderSide(width: 1, color: Colors.black)),
          backgroundColor: MaterialStateProperty.all(Colors.black),
          shape: MaterialStateProperty.all(RoundedRectangleBorder(borderRadius: BorderRadius.circular(6.0))),
        ),
        onPressed: () => MagicRouter.navigateTo(const ClickToStartScreen()),
        child: Text(
          isEn ? 'CONTINUE' : 'الإستمرار',
          softWrap: true,
          style: isEn
              ? Theme.of(context)
                  .textTheme
                  .titleMedium!
                  .copyWith(fontWeight: FontWeight.w400, color: Colors.white, fontFamily: 'Montserrat')
              : Theme.of(context).textTheme.titleMedium!.copyWith(
                    fontSize: 17.0,
                    fontWeight: FontWeight.w400,
                    color: Colors.white,
                    fontFamily: 'Tajawal',
                  ),
        ),
      ),
    );
  }
}
