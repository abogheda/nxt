import 'package:flutter/material.dart';

import '../../../../utils/constants/app_colors.dart';

class NotificationCard extends StatelessWidget {
  final Color notifyColor;
  const NotificationCard({Key? key, required this.notifyColor}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    DateTime date = DateTime(
      DateTime.now().year,
      DateTime.now().month,
      DateTime.now().day,
      DateTime.now().hour,
      DateTime.now().minute,
    );
    return Padding(
      padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
      child: Material(
        elevation: 2,
        child: Container(
          decoration: BoxDecoration(
            border: Border.all(color: AppColors.greyTxtColor, width: 0.5),
          ),
          //TODO IntrinsicHeight
          child: IntrinsicHeight(
            child: Stack(
              children: [
                Container(
                  color: notifyColor,
                  width: 10,
                ),
                // const SizedBox(height: 25),

                // const SizedBox(width: 10),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const SizedBox(height: 8),
                    const Padding(
                      padding: EdgeInsets.only(left: 20),
                      child: Text(
                        "Lorem ipsum dolor sit amet, consetetur"
                        " sadipscing elitr, sed diam nonumy eirmod.",
                        style: TextStyle(
                          fontSize: 14,
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    const SizedBox(height: 15),
                    Padding(
                      padding: const EdgeInsets.only(left: 20),
                      child: Text(
                        date.toString(),
                        style:
                            const TextStyle(fontSize: 14, color: AppColors.greyTxtColor, fontWeight: FontWeight.bold),
                      ),
                    ),
                    const SizedBox(height: 8),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
