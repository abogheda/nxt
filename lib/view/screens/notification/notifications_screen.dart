import 'package:flutter/material.dart';
import '../../../utils/constants/resources.dart';
import '../navigation_and_appbar/import_widget.dart';
import 'notify_widgets/notification_card.dart';

class NotificationsScreen extends StatefulWidget {
  const NotificationsScreen({Key? key}) : super(key: key);

  @override
  State<NotificationsScreen> createState() => _NotificationsScreenState();
}

class _NotificationsScreenState extends State<NotificationsScreen> {
  @override
  Widget build(BuildContext context) {
    return CustomScaffoldWidget(
      backgroundColor: Colors.white,
      extendBodyBehindAppBar: false,
      isBackArrow: true,
      isInProfile: false,
      isLogoClickable: true,
      isThereActions: true,
      isInNotification: true,
      body: ListView(
        children: [
          const Divider(color: Colors.black, thickness: 0.9),
          const SizedBox(height: 20),
          Center(
            child: Text(
              "NOTIFICATIONS",
              style: AppTextStyles.bold18,
            ),
          ),
          const SizedBox(height: 20),
          const NotificationCard(
            notifyColor: AppColors.blueColor,
          ),
          const SizedBox(height: 20),
          const NotificationCard(
            notifyColor: AppColors.yellowColor,
          ),
          const SizedBox(height: 20),
          const NotificationCard(
            notifyColor: AppColors.redColor,
          ),
          const SizedBox(height: 20),
          const NotificationCard(
            notifyColor: AppColors.blueColor,
          ),
          const SizedBox(height: 20),
          const NotificationCard(
            notifyColor: AppColors.yellowColor,
          ),
          const SizedBox(height: 20),
          const NotificationCard(
            notifyColor: AppColors.redColor,
          ),
          const SizedBox(height: 20),
          const NotificationCard(
            notifyColor: AppColors.blueColor,
          ),
          const SizedBox(height: 20),
          const NotificationCard(
            notifyColor: AppColors.yellowColor,
          ),
          const SizedBox(height: 20),
          const NotificationCard(
            notifyColor: AppColors.redColor,
          ),
          const SizedBox(height: 20),
          const NotificationCard(
            notifyColor: AppColors.blueColor,
          ),
          const SizedBox(height: 20),
          const NotificationCard(
            notifyColor: AppColors.yellowColor,
          ),
          const SizedBox(height: 20),
          const NotificationCard(
            notifyColor: AppColors.redColor,
          ),
          const SizedBox(height: 20),
        ],
      ),
    );
  }
}
