import 'package:flutter/material.dart';
import 'controller/scout_cubit.dart';
import 'user_result_card.dart';

import '../../widgets/no_state_widget.dart';

class ScoutFilterUsersListView extends StatelessWidget {
  const ScoutFilterUsersListView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final usersFilterList = ScoutCubit.get(context).userFilterResultList!;
    return ListView.builder(
      shrinkWrap: true,
      itemCount: usersFilterList.length,
      itemBuilder: (context, index) {
        final user = usersFilterList[index].user!;
        if (usersFilterList.isEmpty) return NoStateWidget();
        return UserResultCard(
          birthDate: user.birthDate,
          avatar: user.avatar,
          userName: user.name,
          userId: user.id!,
        );
      },
    );
  }
}
