// ignore_for_file: must_be_immutable

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nxt/utils/constants/app_const.dart';
import 'package:syncfusion_flutter_core/theme.dart';
import 'package:syncfusion_flutter_sliders/sliders.dart';

import '../controller/scout_cubit.dart';

class ScoutFilterSlider extends StatefulWidget {
  ScoutFilterSlider({
    Key? key,
    required this.header,
    required this.start,
    required this.end,
    required this.formatTextInteger,
    required this.filterValues,
  }) : super(key: key);
  final num end;
  final num start;
  final String header;
  final int formatTextInteger;
  SfRangeValues filterValues;
  @override
  State<ScoutFilterSlider> createState() => _ScoutFilterSliderState();
}

class _ScoutFilterSliderState extends State<ScoutFilterSlider> {
  String labelFormat(_, String formattedText) {
    String text = '';
    switch (widget.formatTextInteger) {
      case 0:
        text = '$formattedText ${"year".tr()}';
        break;
      case 1:
        text = '$formattedText ${"cm".tr()}';
        break;
      case 2:
        text = '$formattedText ${"kg".tr()}';
        break;
      default:
    }
    return text;
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ScoutCubit, ScoutState>(
      builder: (context, state) {
        final cubit = ScoutCubit.get(context);
        return Padding(
          padding: EdgeInsets.symmetric(horizontal: widget.width * 0.03),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                widget.header,
                style:
                    Theme.of(context).textTheme.headline5!.copyWith(fontFamily: widget.isEn ? 'BebasNeue' : 'Tajawal'),
              ),
              SfRangeSliderTheme(
                data: SfRangeSliderThemeData(
                  thumbStrokeWidth: 3,
                  thumbColor: Colors.white,
                  thumbStrokeColor: Colors.black,
                  tooltipTextStyle: TextStyle(fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal'),
                ),
                child: SfRangeSlider(
                  stepSize: 1,
                  max: widget.end,
                  min: widget.start,
                  showLabels: true,
                  enableTooltip: true,
                  minorTicksPerInterval: 1,
                  values: widget.filterValues,
                  tooltipTextFormatterCallback: labelFormat,
                  labelFormatterCallback: labelFormat,
                  onChanged: (SfRangeValues values) {
                    setState(() {
                      widget.filterValues = values;
                    });
                    switch (widget.formatTextInteger) {
                      case 0:
                        cubit.ageEnd = values.end;
                        cubit.ageStart = values.start;
                        break;
                      case 1:
                        cubit.heightEnd = values.end;
                        cubit.heightStart = values.start;
                        break;
                      case 2:
                        cubit.weightEnd = values.end;
                        cubit.weightStart = values.start;
                        break;
                      default:
                    }
                  },
                ),
              ),
              SizedBox(height: widget.height * 0.01),
            ],
          ),
        );
      },
    );
  }
}
