import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'characteristics_color_column.dart';
import 'scout_filter_slider.dart';

import '../../../../config/router/router.dart';
import '../../../../utils/constants/app_colors.dart';
import '../../../../utils/constants/app_const.dart';
import '../../../../utils/constants/app_images.dart';
import '../../../widgets/error_state_widget.dart';
import '../../navigation_and_appbar/import_widget.dart';
import '../controller/scout_cubit.dart';
import 'scout_filter_chips.dart';

class ScoutFilterScreen extends StatefulWidget {
  const ScoutFilterScreen({Key? key}) : super(key: key);

  @override
  State<ScoutFilterScreen> createState() => _ScoutFilterScreenState();
}

class _ScoutFilterScreenState extends State<ScoutFilterScreen> {
  @override
  void didChangeDependencies() {
    ScoutCubit.get(context).resetAllFilters();
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return CustomScaffoldWidget(
      extendBodyBehindAppBar: false,
      isBackArrow: true,
      isThereLogo: true,
      isThereActions: true,
      body: BlocConsumer<ScoutCubit, ScoutState>(
        listener: (context, state) {},
        builder: (context, state) {
          var cubit = ScoutCubit.get(context);
          if (state is GetTalentsFailed) {
            return ErrorStateWidget(hasRefresh: true, onRefresh: () async => await cubit.getAllTalents());
          }
          return Stack(
            children: [
              Positioned(
                top: 0,
                left: 0,
                right: 0,
                bottom: 0,
                child: RefreshIndicator(
                  onRefresh: () async => await cubit.getAllTalents(),
                  child: ListView(
                    children: [
                      Container(color: Colors.black, width: double.infinity, height: 1.5),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: widget.width * 0.02),
                        child: Stack(
                          children: [
                            Center(
                              child: Padding(
                                padding: EdgeInsets.symmetric(vertical: widget.height * 0.015),
                                child: Text(
                                  'filterBy'.tr(),
                                  style: Theme.of(context).textTheme.titleLarge!.copyWith(
                                      color: AppColors.greyOutText, fontFamily: widget.isEn ? 'BebasNeue' : 'Tajawal'),
                                ),
                              ),
                            ),
                            Positioned(
                              top: 0,
                              bottom: 0,
                              right: widget.isEn ? 0.0 : null,
                              left: widget.isEn ? null : 0.0,
                              child: TextButton(
                                onPressed: () => cubit.resetAllFilters(),
                                style: TextButton.styleFrom(
                                  minimumSize: Size.zero,
                                  tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                                  padding: const EdgeInsets.symmetric(horizontal: 8),
                                ),
                                child: Text(
                                  'reset'.tr(),
                                  style: TextStyle(
                                    fontSize: 14,
                                    color: AppColors.semiBlack,
                                    fontWeight: FontWeight.w900,
                                    decoration: TextDecoration.underline,
                                    fontFamily: widget.isEn ? 'Montserrat' : 'Tajawal',
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                      ScoutFilterChips(
                        header: 'vertical'.tr(),
                        selectedItem: cubit.verticalTitle,
                        chipsList: cubit.verticalChipsList,
                        setItem: (item) => cubit.verticalTitle = item,
                      ),
                      ScoutFilterChips(
                        header: 'talent'.tr(),
                        isTalent: true,
                        selectedItem: cubit.talentTitle,
                        chipsList: cubit.talentsListTitles,
                        setItem: (item) => cubit.talentTitle = item,
                      ),
                      ScoutFilterChips(
                        header: 'gender'.tr(),
                        selectedItem: cubit.gender,
                        chipsList: cubit.genderChipsList,
                        setItem: (item) => cubit.gender = item,
                      ),
                      ScoutFilterSlider(
                        end: cubit.constAgeEnd!,
                        start: cubit.constAgeStart!,
                        header: 'age'.tr(),
                        formatTextInteger: 0,
                        filterValues: cubit.ageRangeValues,
                      ),
                      ScoutFilterSlider(
                        header: 'height'.tr(),
                        end: cubit.constHeightEnd!,
                        start: cubit.constHeightStart!,
                        formatTextInteger: 1,
                        filterValues: cubit.heightRangeValues,
                      ),
                      ScoutFilterSlider(
                        header: 'weight'.tr(),
                        end: cubit.constWeightEnd!,
                        start: cubit.constWeightStart!,
                        formatTextInteger: 2,
                        filterValues: cubit.weightRangeValues,
                      ),
                      CharacteristicsColorColumn(
                        header: 'skinColor'.tr(),
                        imagesList: AppConst.skinColors,
                        colorImages: AppImages.getSkinImage,
                        selectedCharacteristics: cubit.skinColor,
                        setItem: (item) => cubit.skinColor = item,
                      ),
                      CharacteristicsColorColumn(
                        header: 'hairColor'.tr(),
                        imagesList: AppConst.hairColors,
                        colorImages: AppImages.getHairImage,
                        selectedCharacteristics: cubit.hairColor,
                        setItem: (item) => cubit.hairColor = item,
                      ),
                      CharacteristicsColorColumn(
                        header: 'eyeColor'.tr(),
                        imagesList: AppConst.eyeColors,
                        colorImages: AppImages.getEyeImage,
                        selectedCharacteristics: cubit.eyeColor,
                        setItem: (item) => cubit.eyeColor = item,
                      ),
                      SizedBox(height: widget.height * 0.09)
                    ],
                  ),
                ),
              ),
              Positioned(
                bottom: 0,
                left: 0,
                right: 0,
                child: Builder(
                  builder: (ctx) {
                    return MaterialButton(
                      height: widget.height * 0.08,
                      minWidth: double.infinity,
                      color: Colors.black,
                      onPressed: () async {
                        MagicRouter.pop();
                        await cubit.getUserResultsByFilters();
                      },
                      child: Text(
                        'applyNow'.tr(),
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 18,
                            color: Colors.white,
                            fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal',
                            fontWeight: FontWeight.bold),
                      ),
                    );
                  },
                ),
              ),
            ],
          );
        },
      ),
    );
  }
}
