// ignore_for_file: must_be_immutable

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:nxt/utils/constants/app_const.dart';

import '../../../../utils/constants/app_colors.dart';

class CharacteristicsColorColumn extends StatefulWidget {
  CharacteristicsColorColumn({
    Key? key,
    required this.header,
    required this.selectedCharacteristics,
    required this.imagesList,
    required this.colorImages,
    required this.setItem,
  }) : super(key: key);
  final String header;
  String? selectedCharacteristics;
  final List imagesList;
  final Function(String name) colorImages;
  final Function(String item) setItem;
  @override
  State<CharacteristicsColorColumn> createState() => _CharacteristicsColorColumnState();
}

class _CharacteristicsColorColumnState extends State<CharacteristicsColorColumn> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.symmetric(horizontal: widget.width * 0.03),
          child: Text(
            widget.header,
            style: Theme.of(context).textTheme.headline5!.copyWith(fontFamily: widget.isEn ? 'BebasNeue' : 'Tajawal'),
          ),
        ),
        Container(
          width: widget.width,
          height: widget.height * 0.12,
          margin: const EdgeInsets.symmetric(vertical: 8),
          child: ListView.builder(
            shrinkWrap: true,
            itemCount: widget.imagesList.length,
            scrollDirection: Axis.horizontal,
            padding: const EdgeInsets.symmetric(horizontal: 12.0),
            itemBuilder: (context, index) {
              final String item = widget.imagesList[index];
              return AnimatedContainer(
                duration: const Duration(milliseconds: 150),
                curve: Curves.easeInCubic,
                padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 2),
                margin: const EdgeInsets.symmetric(horizontal: 1),
                decoration: BoxDecoration(
                    border: Border.all(
                      width: 1.5,
                      color: widget.selectedCharacteristics == item ? Colors.black : Colors.transparent,
                    ),
                    borderRadius: const BorderRadius.all(Radius.circular(6.0))),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: widget.height * 0.003),
                      child: GestureDetector(
                        onTap: () {
                          widget.setItem(item);
                          setState(() => widget.selectedCharacteristics = item);
                        },
                        child: Image.asset(widget.colorImages(item)),
                      ),
                    ),
                    Text(
                      item.toLowerCase().tr(),
                      style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.bold,
                        color: AppColors.iconsColor,
                        fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal',
                      ),
                    ),
                  ],
                ),
              );
            },
          ),
        ),
        SizedBox(height: widget.height * 0.015),
      ],
    );
  }
}
