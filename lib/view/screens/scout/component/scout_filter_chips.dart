// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nxt/utils/constants/app_const.dart';

import '../../../../utils/constants/app_colors.dart';
import '../../../widgets/shimmer_widgets/custom_shimmer.dart';
import '../controller/scout_cubit.dart';

class ScoutFilterChips extends StatefulWidget {
  ScoutFilterChips({
    Key? key,
    required this.chipsList,
    required this.header,
    this.isTalent = false,
    required this.selectedItem,
    required this.setItem,
  }) : super(key: key);
  String? selectedItem;
  final List<String> chipsList;
  final String header;
  final bool? isTalent;
  final Function(String item) setItem;

  @override
  State<ScoutFilterChips> createState() => _ScoutFilterChipsState();
}

class _ScoutFilterChipsState extends State<ScoutFilterChips> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ScoutCubit, ScoutState>(
      builder: (context, state) {
        return Padding(
          padding: EdgeInsets.symmetric(horizontal: widget.width * 0.03),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(widget.header,
                  style: Theme.of(context)
                      .textTheme
                      .headline5!
                      .copyWith(fontFamily: widget.isEn ? 'BebasNeue' : 'Tajawal')),
              Center(
                child: Wrap(
                  spacing: widget.width * 0.01,
                  alignment: WrapAlignment.center,
                  children: widget.isTalent! && state is GetTalentsLoading ||
                          widget.isTalent! && ScoutCubit.get(context).talentsList == null
                      ? ['Talent lol', 'Music', 'Now What'].map((chip) {
                          return CustomShimmer(
                            child: ChoiceChip(
                              label: Text(chip),
                              selectedColor: Colors.black,
                              backgroundColor: Colors.white,
                              labelStyle: Theme.of(context).textTheme.bodyText1!.copyWith(
                                    fontWeight: FontWeight.bold,
                                    fontFamily: widget.isEn ? 'Montserrat' : 'Tajawal',
                                  ),
                              selected: true,
                              onSelected: null,
                              padding: EdgeInsets.symmetric(
                                  horizontal: widget.width * 0.03, vertical: widget.height * 0.012),
                              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4)),
                            ),
                          );
                        }).toList()
                      : widget.chipsList.map((chip) {
                          return ChoiceChip(
                            label: Text(chip),
                            selectedColor: Colors.black,
                            backgroundColor: Colors.white,
                            labelStyle: Theme.of(context).textTheme.bodyText1!.copyWith(
                                  fontWeight: FontWeight.bold,
                                  fontFamily: widget.isEn ? 'Montserrat' : 'Tajawal',
                                  color: widget.selectedItem == chip ? Colors.white : Colors.black,
                                ),
                            selected: widget.selectedItem == chip,
                            onSelected: (_) {
                              if (widget.selectedItem == chip) {
                                setState(() => widget.selectedItem = null);
                              } else {
                                setState(() => widget.selectedItem = chip);
                              }
                              widget.setItem(chip);
                            },
                            padding:
                                EdgeInsets.symmetric(horizontal: widget.width * 0.03, vertical: widget.height * 0.012),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(4),
                              side: BorderSide(
                                  color: widget.selectedItem == chip ? Colors.transparent : AppColors.borderGreyColor),
                            ),
                          );
                        }).toList(),
                ),
              ),
              SizedBox(height: widget.height * 0.01),
            ],
          ),
        );
      },
    );
  }
}
