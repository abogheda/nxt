import 'package:age_calculator/age_calculator.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import '../../../config/router/router.dart';
import '../../../utils/constants/app_images.dart';
import 'package:transparent_image/transparent_image.dart';

import '../../../../utils/constants/app_const.dart';
import '../../../utils/constants/app_colors.dart';
import '../community_user_profile/community_user_profile.dart';

class UserResultCard extends StatelessWidget {
  const UserResultCard(
      {Key? key, required this.userName, required this.avatar, required this.birthDate, required this.userId})
      : super(key: key);
  final String? userName;
  final String? avatar;
  final String? birthDate;
  final String userId;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: height * 0.01, horizontal: width * 0.03),
      child: InkWell(
        onTap: () => MagicRouter.navigateTo(CommunityUserProfile(userId: userId)),
        splashColor: Colors.black12,
        borderRadius: BorderRadius.circular(6),
        child: Container(
          decoration: BoxDecoration(
              border: Border.all(color: const Color(0xFF707070), width: 0.2), borderRadius: BorderRadius.circular(6)),
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: height * 0.025, horizontal: height * 0.02),
            child: Row(
              children: <Widget>[
                Container(
                  height: width * 0.13,
                  width: width * 0.13,
                  decoration: BoxDecoration(
                    color: AppColors.greyOutText,
                    image: DecorationImage(
                        fit: BoxFit.cover,
                        image: avatar != null && avatar != ''
                            ? FadeInImage.memoryNetwork(
                                image: avatar ?? userAvatarPlaceHolderUrl,
                                placeholder: kTransparentImage,
                              ).image
                            : Image.asset('assets/images/profile_icon.png').image),
                  ),
                ),
                Padding(
                  padding: EdgeInsetsDirectional.only(start: width * 0.03),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                          padding: EdgeInsets.symmetric(vertical: height * 0.005),
                          child: Text(userName ?? "notAvailable".tr(), style: Theme.of(context).textTheme.headline6)),
                      Text(
                        birthDate == null
                            ? "notAvailable".tr()
                            : '${AgeCalculator.age(DateTime.parse(birthDate!)).years} ${'year'.tr()}',
                        style: Theme.of(context)
                            .textTheme
                            .bodyText1!
                            .copyWith(fontFamily: isEn ? 'Montserrat' : 'Tajawal'),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
