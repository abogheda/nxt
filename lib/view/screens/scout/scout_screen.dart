import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import '../../../config/router/router.dart';
import '../../../utils/constants/resources.dart';
import '../../../utils/helpers/validators.dart';
import '../../widgets/custom_text_field.dart';
import '../../widgets/no_state_widget.dart';
import '../../widgets/shimmer_widgets/user_result_card_shimmer.dart';
import 'component/scout_filter_screen.dart';
import 'controller/scout_cubit.dart';
import 'scout_filter_users_list_view.dart';
import 'scout_search_users_list_view.dart';

class ScoutScreen extends StatelessWidget {
  const ScoutScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => ScoutCubit()..getUserResultsByFilters(),
      child: BlocConsumer<ScoutCubit, ScoutState>(
        listener: (context, state) {},
        builder: (context, state) {
          final cubit = ScoutCubit.get(context);
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                  color: Colors.black, height: 1.5, width: width, margin: EdgeInsets.only(bottom: height * 0.025)),
              Padding(
                padding: EdgeInsets.only(left: width * 0.03, right: width * 0.03, bottom: height * 0.015),
                child: Row(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
                  Expanded(
                    child: Form(
                      key: cubit.userSearchFormKey,
                      child: CustomTextField(
                        hint: 'searchTalents'.tr(),
                        controller: cubit.userSearchController,
                        validator: Validators.generalField,
                        prefixIconConstraints: const BoxConstraints(maxWidth: 48, maxHeight: 48),
                        prefix: Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 6),
                            child: SvgPicture.asset('assets/images/search.svg')),
                        onFieldSubmitted: (value) async {
                          if (cubit.userSearchFormKey.currentState!.validate()) {
                            await cubit.searchForUsers(name: cubit.userSearchController.text.trim());
                          }
                        },
                        suffix: IconButton(
                          splashRadius: 1,
                          icon: SvgPicture.asset('assets/images/clear.svg'),
                          onPressed: () => cubit.userSearchController.clear(),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(width: width * 0.02),
                  SizedBox(
                    height: width * 0.12,
                    width: width * 0.13,
                    child: ElevatedButton(
                      onPressed: () => MagicRouter.navigateTo(
                          BlocProvider.value(value: cubit..getAllTalents(), child: const ScoutFilterScreen())),
                      child: SvgPicture.asset('assets/images/filter.svg'),
                    ),
                  )
                ]),
              ),
              Padding(
                  padding: EdgeInsets.symmetric(vertical: height * 0.005, horizontal: width * 0.03),
                  child: Text('results'.tr(), style: Theme.of(context).textTheme.headline6)),
              Expanded(
                child: RefreshIndicator(
                  onRefresh: () async {
                    if (cubit.searchUserResultList != null) {
                      cubit.searchUserResultList!.clear();
                    }
                    if (cubit.userFilterResultList != null) {
                      cubit.userFilterResultList!.clear();
                    }
                    if (cubit.searchUserResultList == null) {
                      await cubit.getUserResultsByFilters();
                    } else {
                      if (cubit.userSearchFormKey.currentState!.validate()) {
                        await cubit.searchForUsers(name: cubit.userSearchController.text.trim());
                      }
                    }
                  },
                  child: state is GetUserResultsLoading || state is SearchUserByNameLoading
                      ? const UserResultShimmerListView()
                      : cubit.searchUserResultList == null
                          ? cubit.userFilterResultList!.isEmpty
                              ? NoStateWidget()
                              : const ScoutFilterUsersListView()
                          : cubit.searchUserResultList!.isEmpty
                              ? NoStateWidget()
                              : const ScoutSearchUsersListView(),
                ),
              ),
              // Expanded(
              //   child: cubit.searchJobAndChallengesList == null
              //       ? const SizedBox()
              //       : RefreshIndicator(
              //           onRefresh: () async => await cubit.searchForJobsAndChallenges(
              //               word: cubit.jobAndChallengeSearchController.text.trim()),
              //           child: state is SearchJobAndChallengeLoading
              //               ? const AppLoader()
              //               : cubit.searchJobAndChallengesList!.isEmpty
              //                   ? NoStateWidget()
              //                   : ListView(
              //                       padding: EdgeInsets.symmetric(horizontal: width * 0.03),
              //                       children: [
              //                         for (final element in cubit.searchJobAndChallengesList!)
              //                           if (element.jobStartDate == null)
              //                             ChallengeCard(
              //                               image: Challenge.fromSearch(element).media ?? placeHolderUrl,
              //                               color: (Challenge.fromSearch(element).category!.color ?? '#f5ed05').toHex(),
              //                               title: Challenge.fromSearch(element).title ?? "notAvailable".tr(),
              //                               supTitle: Challenge.fromSearch(element).supTitle ?? "notAvailable".tr(),
              //                               padding: EdgeInsets.only(top: height * 0.01),
              //                               onTap: () => MagicRouter.navigateTo(
              //                                 BlocProvider.value(
              //                                   value: ChallengeCubit()
              //                                     ..onInit(challengeId: Challenge.fromSearch(element).id!),
              //                                   child:
              //                                       ViewChallengeScreen(challengeId: Challenge.fromSearch(element).id!),
              //                                 ),
              //                               ),
              //                             )
              //                           else
              //                             MyJobCard(
              //                               elevation: 0,
              //                               id: JobsModel.fromSearch(element).id!,
              //                               padding: EdgeInsets.only(top: height * 0.01),
              //                               name:
              //                                   JobsModel.fromSearch(element).organization!.name ?? "notAvailable".tr(),
              //                               color: JobsModel.fromSearch(element).category!.color!,
              //                               logo: JobsModel.fromSearch(element).organization!.logo ??
              //                                   userAvatarPlaceHolderUrl,
              //                               title: JobsModel.fromSearch(element).title!,
              //                               createdAt: JobsModel.fromSearch(element).createdAt ??
              //                                   DateTime.now().toIso8601String(),
              //                             ),
              //                         SizedBox(height: height * 0.01)
              //                       ],
              //                     )),
              // )
            ],
          );
        },
      ),
    );
  }
}
