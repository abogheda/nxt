// ignore_for_file: depend_on_referenced_packages, unnecessary_import

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:syncfusion_flutter_sliders/sliders.dart';
import '../../../../data/enum/talent_type.dart';
import '../../../../data/models/scout/results_model.dart';
import '../../../../data/models/scout/search_results_model.dart';
import '../../../../data/models/talent_model.dart';
import '../../../../data/repos/imports.dart';
import '../../../../data/repos/scout/scout_repo.dart';
part 'scout_state.dart';

class ScoutCubit extends Cubit<ScoutState> {
  ScoutCubit() : super(ScoutInitial());
  static ScoutCubit get(context) => BlocProvider.of(context);
  // final jobAndChallengeSearchController = TextEditingController();
  final userSearchController = TextEditingController();
  final userSearchFormKey = GlobalKey<FormState>();
  final jobAndChallengeSearchFormKey = GlobalKey<FormState>();
  List<TalentModel>? talentsList;
  TalentModel? talentModel;
  List<ResultsModel>? userFilterResultList;
  List<SearchResultsModel>? searchUserResultList;
  // List<SearchJobAndChallengesModel>? searchJobAndChallengesList;
  SfRangeValues ageRangeValues = const SfRangeValues(13, 90);
  SfRangeValues heightRangeValues = const SfRangeValues(0, 250);
  SfRangeValues weightRangeValues = const SfRangeValues(0, 300);
  num? constAgeStart = 13.0,
      constAgeEnd = 90.0,
      constHeightStart = 0.0,
      constHeightEnd = 250.0,
      constWeightStart = 0.0,
      constWeightEnd = 300.0,
      ageStart,
      ageEnd,
      heightStart,
      heightEnd,
      weightStart,
      weightEnd;
  List<String> talentsListTitles = [];
  String? verticalTitle, talentTitle, verticalId, talentId, gender, skinColor, hairColor, eyeColor;
  List<String> verticalChipsList = [
    TalentCategoryType.music.name.tr().toUpperCase(),
    TalentCategoryType.filmMaking.name.tr().toUpperCase(),
    TalentCategoryType.acting.name.tr().toUpperCase()
  ];
  final List<String> genderChipsList = ["Male", "Female"];
//===============================================================
  @override
  Future<void> close() {
    // jobAndChallengeSearchController.dispose();
    userSearchController.dispose();
    return super.close();
  }

  Future<void> getAllTalents() async {
    talentsList = null;
    talentId = null;
    talentTitle = null;
    talentsListTitles.clear();
    emit(GetTalentsLoading());
    final res = await ProfileRepo.getAllTalents();
    talentsList = res.data;
    if (res.data != null) {
      for (TalentModel talent in talentsList!) {
        talentsListTitles.add(talent.title!);
      }
      emit(GetTalentsSuccess());
    } else {
      emit(GetTalentsFailed(msg: res.message ?? ""));
    }
  }

  void resetAllFilters() {
    emit(ResetAllFiltersLoadingState());
    verticalTitle = null;
    talentTitle = null;
    verticalId = null;
    talentId = null;
    talentModel = null;
    gender = null;
    ageRangeValues = const SfRangeValues(13, 90);
    heightRangeValues = const SfRangeValues(0, 250);
    weightRangeValues = const SfRangeValues(0, 300);
    ageStart = null;
    ageEnd = null;
    heightStart = null;
    heightEnd = null;
    weightStart = null;
    weightEnd = null;
    skinColor = null;
    hairColor = null;
    eyeColor = null;
    emit(ResetAllFiltersSuccessState());
  }

  void getVerticalIdAndTalentId() {
    if (verticalTitle == TalentCategoryType.music.name.tr().toUpperCase()) {
      verticalId = TalentCategoryType.music.id;
    } else if (verticalTitle == TalentCategoryType.filmMaking.name.tr().toUpperCase()) {
      verticalId = TalentCategoryType.filmMaking.id;
    } else if (verticalTitle == TalentCategoryType.acting.name.tr().toUpperCase()) {
      verticalId = TalentCategoryType.acting.id;
    }
    if (talentsList != null && talentTitle != null) {
      talentModel = talentsList!.firstWhere((talent) => talent.title == talentTitle);
      talentId = talentModel!.id;
    }
  }

  Future<void> getUserResultsByFilters() async {
    searchUserResultList = null;
    emit(GetUserResultsLoading());
    final res = await ScoutRepo.getUserResultsByFilters(
      filtrationMap: {
        'categories': verticalId ?? '',
        'talents': talentId ?? '',
        'gender': gender ?? '',
        'birthDateMin': ageEnd == null ? '' : DateTime(DateTime.now().year - ageEnd!.round()),
        'birthDateMax': ageStart == null ? '' : DateTime(DateTime.now().year - ageStart!.round()),
        'heightMin': heightEnd ?? '',
        'heightMax': heightStart ?? '',
        'weightMin': weightEnd ?? '',
        'weightMax': weightStart ?? '',
        'skinColor': skinColor ?? '',
        'hairColor': hairColor ?? '',
        'eyeColor': eyeColor ?? '',
      },
    );
    if (res.data != null) {
      userFilterResultList = res.data;
      emit(GetUserResultsSuccess());
    } else {
      emit(GetUserResultsFailed(msg: res.message ?? ""));
    }
  }

  // Future<void> searchForJobsAndChallenges({required String word}) async {
  //   emit(SearchJobAndChallengeLoading());
  //   final res = await ScoutRepo.searchForJobsAndChallenges(word: word);
  //   if (res.data != null) {
  //     searchJobAndChallengesList = res.data;
  //     emit(SearchJobAndChallengeSuccess());
  //   } else {
  //     emit(SearchJobAndChallengeFailed(msg: res.message ?? ""));
  //   }
  // }

  Future<void> searchForUsers({required String name}) async {
    userFilterResultList = null;
    emit(SearchUserByNameLoading());
    final res = await ScoutRepo.searchForUsers(name: name);
    if (res.data != null) {
      searchUserResultList = res.data;
      emit(SearchUserByNameSuccess());
    } else {
      emit(SearchUserByNameFailed(msg: res.message ?? ""));
    }
  }
}
