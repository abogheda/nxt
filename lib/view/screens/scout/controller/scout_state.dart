part of 'scout_cubit.dart';

@immutable
abstract class ScoutState {}

class ScoutInitial extends ScoutState {}

class ResetAllFiltersLoadingState extends ScoutState {}

class ResetAllFiltersSuccessState extends ScoutState {}

class GetTalentsSuccess extends ScoutState {}

class GetTalentsLoading extends ScoutState {}

class GetTalentsFailed extends ScoutState {
  final String msg;
  GetTalentsFailed({required this.msg});
}

class GetUserResultsSuccess extends ScoutState {}

class GetUserResultsLoading extends ScoutState {}

class GetUserResultsFailed extends ScoutState {
  final String msg;
  GetUserResultsFailed({required this.msg});
}

class SearchJobAndChallengeSuccess extends ScoutState {}

class SearchJobAndChallengeLoading extends ScoutState {}

class SearchJobAndChallengeFailed extends ScoutState {
  final String msg;
  SearchJobAndChallengeFailed({required this.msg});
}

class SearchUserByNameSuccess extends ScoutState {}

class SearchUserByNameLoading extends ScoutState {}

class SearchUserByNameFailed extends ScoutState {
  final String msg;
  SearchUserByNameFailed({required this.msg});
}
