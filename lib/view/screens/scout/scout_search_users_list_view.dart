import 'package:flutter/material.dart';
import 'user_result_card.dart';

import 'controller/scout_cubit.dart';

class ScoutSearchUsersListView extends StatelessWidget {
  const ScoutSearchUsersListView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final usersSearchList = ScoutCubit.get(context).searchUserResultList!;
    return ListView.builder(
      shrinkWrap: true,
      itemCount: usersSearchList.length,
      itemBuilder: (context, index) {
        final user = usersSearchList[index];
        return UserResultCard(
          birthDate: user.birthDate,
          avatar: user.avatar,
          userName: user.name,
          userId: user.id!,
        );
      },
    );
  }
}
