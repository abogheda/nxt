// ignore_for_file: depend_on_referenced_packages, unnecessary_import
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nxt/data/models/home/home.dart';
import 'package:nxt/data/repos/imports.dart';
import 'package:nxt/utils/constants/resources.dart';
import '../../../../data/enum/talent_type.dart';
import '../../../../data/models/home/home_carousel_model.dart';
import '../../../../data/models/home/home_carousels_model.dart';
import '../../../../data/models/user_model.dart' as user;

import '../../../../data/models/user_models/users_profile_model.dart';
import '../../../../data/repos/home_carousel/home_carousel_repo.dart';
import '../../../../data/service/hive_services.dart';

part 'home_state.dart';

class HomeCubit extends Cubit<HomeState> {
  HomeCubit() : super(HomeInitial());
  static HomeCubit get(context) => BlocProvider.of(context);
  String? userAvatar;
  List<HomeModel>? homeCategories;
  TalentCategoryType? categoryType;
  UsersProfileModel? usersProfileModel;
  HomeCarouselModel? carouselModel;
  List<HomeCarouselsModel>? carouselModelList;
  Future<void> cacheCountry({required String phone}) async {
    bool startsWith({required String firstThreeDigits}) => phone.startsWith(firstThreeDigits);
    if (startsWith(firstThreeDigits: '+2010') ||
        startsWith(firstThreeDigits: '+2011') ||
        startsWith(firstThreeDigits: '+2012') ||
        startsWith(firstThreeDigits: '+2015')) {
      await HiveHelper.cacheCountry(country: 'Egypt');
    } else {
      await HiveHelper.userBox.delete('country');
    }
  }

  Future<void> getUserData() async {
    if (HiveHelper.isRegularUser) {
      emit(HomeProfileLoading());
      final res = await ProfileRepo.getUserData();
      if (res.data != null) {
        usersProfileModel = res.data;
        if (userAvatar == '') {
          userAvatar = userAvatarPlaceHolderUrl;
        } else {
          userAvatar = usersProfileModel!.user!.avatar;
        }
        final responseUserModel = usersProfileModel!.user!;
        await cacheCountry(phone: responseUserModel.phone!);
        await HiveHelper.cacheUserModel(
          userModel: HiveHelper.getUserInfo!.copyWith(
            user: user.User(
              id: responseUserModel.id,
              name: responseUserModel.name,
              role: responseUserModel.role,
              eula: responseUserModel.eula,
              email: responseUserModel.email,
              phone: responseUserModel.phone,
              terms: responseUserModel.terms,
              verify: responseUserModel.verify,
              expert: responseUserModel.expert,
              avatar: responseUserModel.avatar,
              package: responseUserModel.package,
              removed: responseUserModel.removed,
              privacy: responseUserModel.privacy,
              provider: responseUserModel.provider,
              googleId: responseUserModel.googleId,
              updatedAt: responseUserModel.updatedAt,
              createdAt: responseUserModel.createdAt,
              birthDate: responseUserModel.birthDate,
              facebookId: responseUserModel.facebookId,
              providerId: responseUserModel.providerId,
              invitation: responseUserModel.invitation,
              verifyPhone: responseUserModel.verifyPhone,
              countryCode: responseUserModel.countryCode,
              countryName: responseUserModel.countryName,
              organization: responseUserModel.organization,
              confirmPhone: responseUserModel.confirmPhone,
              profileStatus: responseUserModel.profileStatus,
              subscriptionEnd: responseUserModel.subscriptionEnd,
              myInvitationCode: responseUserModel.myInvitationCode,
              firstSubscription: responseUserModel.firstSubscription,
              invitationExpiryDate: responseUserModel.invitationExpiryDate,
            ),
          ),
        );
        if (responseUserModel.role == 'Expert') {
          await HiveHelper.cacheProfileInfo(
            name: responseUserModel.expert!.name,
            avatar: responseUserModel.expert!.logo,
            birthDate: responseUserModel.birthDate,
          );
        } else if (responseUserModel.role == 'Organization') {
          await HiveHelper.cacheProfileInfo(
            birthDate: responseUserModel.birthDate,
            name: responseUserModel.organization!.name,
            avatar: responseUserModel.organization!.logo,
          );
        } else {
          await HiveHelper.cacheProfileInfo(
            name: responseUserModel.name,
            avatar: responseUserModel.avatar,
            birthDate: responseUserModel.birthDate,
          );
        }
        emit(HomeProfileSuccess());
      } else {
        emit(HomeProfileFailed(message: res.message, statusCode: res.statusCode));
      }
    }
  }

  Future<void> getUserDataValidate() async {
    emit(HomeProfileLoading());
    final res = await ProfileRepo.getUserDataValidate();
    if (res.message != null) {
      emit(HomeProfileFailed(
        message: res.message,
        statusCode: res.statusCode,
      ));
      return;
    }
    if (res.data != null) {
      usersProfileModel = res.data;
      if (userAvatar == '') {
        userAvatar = userAvatarPlaceHolderUrl;
      }
      emit(HomeProfileSuccess());
    } else {
      emit(HomeProfileFailed(
        message: res.message,
        statusCode: res.statusCode,
      ));
    }
  }

  Future<void> getAllHomeData({TalentCategoryType? filter}) async {
    emit(HomeLoading());
    final res = await HomeRepo.getHomePageData(filter?.id ?? categoryType?.id);
    if (res.data != null) {
      homeCategories = res.data!;
      emit(HomeSuccess());
    } else {
      emit(HomeFailed(message: res.message, statusCode: res.statusCode));
    }
  }

  Future<void> editUserLanguage({required String language}) async {
    emit(UpdateUserLanguageLoading());
    final res = await ProfileRepo.editUserLanguage(language: language);
    if (res.data != null) {
      emit(UpdateUserLanguageSuccess());
    } else {
      emit(UpdateUserLanguageFailed(message: res.message));
    }
  }

  Future<void> getHomeCarouselsData() async {
    emit(GetHomeCarouselsLoading());
    final res = await HomeCarouselRepo.getHomeCarouselsData();
    if (res.data != null) {
      carouselModelList = res.data;
      emit(GetHomeCarouselsSuccess());
    } else {
      emit(GetHomeCarouselsFailed());
    }
  }

  Future<void> getHomeCarouselById({required String id}) async {
    emit(GetHomeCarouselByIdLoading());
    final res = await HomeCarouselRepo.getHomeCarouselById(id: id);
    if (res.data != null) {
      carouselModel = res.data;
      emit(GetHomeCarouselByIdSuccess());
    } else {
      emit(GetHomeCarouselByIdFailed());
    }
  }

  // Future<void> addToSavedItems({String? adminPostId}) async {
  //   emit(SaveHomePostLoading());
  //   final res = await SaveRepo.addToSavedItems(
  //     SaveModel(
  //       item: adminPostId,
  //       type: SavedItemsEnum.adminposts,
  //     ),
  //   );
  //   if (res.data != null) {
  //     emit(SaveHomePostSuccess());
  //   } else {
  //     emit(SaveHomePostFailed(msg: res.message ?? ""));
  //   }
  // }

  // Future<void> deleteFromSavedItems({String? adminPostId}) async {
  //   emit(DeleteSaveHomePostLoading());
  //   final res = await SaveRepo.deleteFromSavedItems(
  //     SaveModel(
  //       item: adminPostId,
  //       type: SavedItemsEnum.adminposts,
  //     ),
  //   );
  //   if (res.data != null) {
  //     emit(DeleteSaveHomePostSuccess());
  //     return;
  //   } else {
  //     emit(DeleteSaveHomePostFailed(msg: res.message ?? ""));
  //   }
  // }
}
