part of 'home_cubit.dart';

abstract class HomeState extends Equatable {
  const HomeState();

  @override
  List<Object> get props => [];
}

class HomeInitial extends HomeState {}

class HomeProfileSuccess extends HomeState {}

class HomeProfileLoading extends HomeState {}

class HomeProfileIsEmpty extends HomeState {}

class HomeProfileFailed extends HomeState {
  final String? message;
  final int? statusCode;
  const HomeProfileFailed({required this.message, required this.statusCode});
}

class HomeSuccess extends HomeState {}

class HomeLoading extends HomeState {}

class HomeFailed extends HomeState {
  final String? message;

  final int? statusCode;
  const HomeFailed({required this.message, required this.statusCode});
}

class UpdateUserLanguageSuccess extends HomeState {}

class UpdateUserLanguageLoading extends HomeState {}

class UpdateUserLanguageFailed extends HomeState {
  final String? message;
  const UpdateUserLanguageFailed({required this.message});
}

class CountrySuccess extends HomeState {}

class CountryLoading extends HomeState {}

class CountryFailed extends HomeState {
  final String msg;
  const CountryFailed(this.msg);
}

class GetHomeCarouselsSuccess extends HomeState {}

class GetHomeCarouselsLoading extends HomeState {}

class GetHomeCarouselsFailed extends HomeState {}

class GetHomeCarouselByIdSuccess extends HomeState {}

class GetHomeCarouselByIdLoading extends HomeState {}

class GetHomeCarouselByIdFailed extends HomeState {}

class SaveHomePostSuccess extends HomeState {}

class SaveHomePostLoading extends HomeState {}

class SaveHomePostFailed extends HomeState {
  final String msg;

  const SaveHomePostFailed({required this.msg});
}

class DeleteSaveHomePostSuccess extends HomeState {}

class DeleteSaveHomePostLoading extends HomeState {}

class DeleteSaveHomePostFailed extends HomeState {
  final String msg;

  const DeleteSaveHomePostFailed({required this.msg});
}
