// ignore_for_file: must_be_immutable
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nxt/data/models/challenge/challenge.dart';
import 'package:nxt/utils/constants/resources.dart';
import 'package:nxt/view/widgets/error_state_widget.dart';
import 'package:nxt/view/screens/home/cubit/home_cubit.dart';
import '../../../config/router/router.dart';
import '../../../data/models/classes/master_class.dart';
import '../../../data/service/hive_services.dart';
import '../../../utils/helpers/popup_helper.dart';
import '../../widgets/shimmer_widgets/challenge_card_shimmer.dart';
import '../challenges/cubit/challenge_cubit.dart';
import '../challenges/screens/view_challenge/view_challenge_screen.dart';
import '../challenges/widgets/challenge_card.dart';
import '../login/login_screen.dart';
import 'home_widgets/carousel_cards_list.dart';
import 'home_widgets/home_class_card.dart';
import 'home_widgets/home_post_card.dart';
import 'home_widgets/home_class_card_shimmer.dart';
import 'home_widgets/home_post_card_shimmer.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  void initState() {
    onHomeInit(context);
    super.initState();
  }

//==== this method to make sure the user data cached and profile status update solve the complete profile issue ====
  void onHomeInit(BuildContext context) async {
    await BlocProvider.of<HomeCubit>(context).getUserData();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<HomeCubit, HomeState>(
      listener: (context, state) async {
        if (state is HomeProfileFailed) {
          if (state.message == 'Invalid Token' || state.statusCode == 401) {
            await HiveHelper.logout();
            MagicRouter.navigateAndPopAll(const LoginScreen());
            PopupHelper.showBasicSnack(msg: 'accountWasDeleted'.tr(), color: Colors.red);
          }
          return;
        }
        if (state is HomeFailed) {
          PopupHelper.showBasicSnack(msg: state.message!, color: Colors.red);
          return;
        }
      },
      builder: (context, state) {
        final cubit = HomeCubit.get(context);
        if (state is HomeFailed || state is HomeProfileFailed) {
          return ErrorStateWidget(
            hasRefresh: true,
            onRefresh: () async {
              await cubit.getUserData();
              await cubit.getAllHomeData();
              await cubit.getHomeCarouselsData();
            },
          );
        }
        if (state is HomeLoading || cubit.homeCategories == null) {
          return ListView(
            physics: const BouncingScrollPhysics(),
            children: [
              Container(
                  color: Colors.black, height: 1.5, width: widget.width, margin: const EdgeInsets.only(bottom: 8)),
              const CarouselCardShimmer(),
              const HomeClassCardShimmer(),
              ChallengeCardShimmer(
                  padding: EdgeInsets.symmetric(vertical: widget.height * 0.008, horizontal: widget.width * 0.03)),
              const HomePostCardShimmer()
            ],
          );
        }
        final homeData = cubit.homeCategories;
        return RefreshIndicator(
          onRefresh: () async {
            cubit.categoryType == null
                ? await cubit.getAllHomeData()
                : await cubit.getAllHomeData(filter: cubit.categoryType);
            await cubit.getHomeCarouselsData();
            await cubit.getUserData();
          },
          child: ListView(
            children: [
              Container(
                  color: Colors.black, height: 1.5, width: widget.width, margin: const EdgeInsets.only(bottom: 8)),
              const CarouselCardsList(),
              for (final element in homeData!)
                if (element.author != null)
                  HomeClassCard(
                    released: MasterClass.fromHomeModel(element).released ?? true,
                    masterClassId: MasterClass.fromHomeModel(element).id!,
                    poster: MasterClass.fromHomeModel(element).poster,
                    color: (MasterClass.fromHomeModel(element).category!.color ?? '#f5ed05').toHex(),
                    category: MasterClass.fromHomeModel(element).category!.title,
                    authorName: MasterClass.fromHomeModel(element).homeAuthorModel!.name,
                    courseName: MasterClass.fromHomeModel(element).title,
                    details: MasterClass.fromHomeModel(element).details,
                  )
                else if (element.requirements != null)
                  ChallengeCard(
                    image: Challenge.fromHomeChallenge(element).media ?? placeHolderUrl,
                    color: (Challenge.fromHomeChallenge(element).category!.color ?? '#f5ed05').toHex(),
                    title: Challenge.fromHomeChallenge(element).title ?? "notAvailable".tr(),
                    supTitle: Challenge.fromHomeChallenge(element).supTitle ?? "notAvailable".tr(),
                    padding: EdgeInsets.symmetric(horizontal: widget.width * 0.03, vertical: widget.height * 0.008),
                    onTap: () {
                      MagicRouter.navigateTo(
                      BlocProvider.value(
                        value: ChallengeCubit()..onInit(challengeId: Challenge.fromHomeChallenge(element).id!),
                        child: ViewChallengeScreen(challengeId: Challenge.fromHomeChallenge(element).id!),
                      ));
                      },
                    // onTap: () =>HiveHelper.getUserInfo?.user?.subscriptionEnd == false ? MagicRouter.navigateTo(
                    //   BlocProvider.value(
                    //     value: ChallengeCubit()..onInit(challengeId: Challenge.fromHomeChallenge(element).id!),
                    //     child: ViewChallengeScreen(challengeId: Challenge.fromHomeChallenge(element).id!),
                    //   ),
                    // ): MagicRouter.navigateTo(const ChooseYourBundle(fromHome: true,)),
                  )
                else
                  HomePostCard(
                    image: element.media ?? placeHolderUrl,
                    color: (element.category!.color ?? '#f5ed05').toHex(),
                    postType: element.category!.title == null
                        ? "notAvailable".tr()
                        : element.category!.title!.trim().toUpperCase(),
                    postText: element.text ?? "notAvailable".tr(),
                  )
            ],
          ),
        );
      },
    );
  }
}
