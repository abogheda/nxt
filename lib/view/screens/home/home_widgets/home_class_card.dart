import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:transparent_image/transparent_image.dart';
import '../../../../utils/constants/resources.dart';
import '../../../widgets/custom_button.dart';
import '../../../../config/router/router.dart';
import '../../learn/master_classes/screens/class_screen.dart';

class HomeClassCard extends StatefulWidget {
  final String masterClassId;
  final int color;
  final String? poster;
  final String? category;
  final String? authorName;
  final String? courseName;
  final String? details;
  final bool released;
  const HomeClassCard({
    Key? key,
    required this.masterClassId,
    required this.color,
    this.poster,
    this.category,
    this.authorName,
    this.courseName,
    this.details,
    this.released = false,
  }) : super(key: key);

  @override
  State<HomeClassCard> createState() => _HomeClassCardState();
}

class _HomeClassCardState extends State<HomeClassCard> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: widget.height * 0.008, horizontal: widget.width * 0.03),
      child: Material(
        elevation: 1,
        borderRadius: BorderRadius.circular(8),
        child: Stack(
          alignment: widget.isEn ? Alignment.centerLeft : Alignment.centerRight,
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(8),
              child: FadeInImage.memoryNetwork(
                fit: BoxFit.cover,
                width: double.infinity,
                height: widget.height * 0.32,
                placeholder: kTransparentImage,
                image: widget.poster ?? placeHolderUrl,
              ),
            ),
            Positioned.fill(
                child: DecoratedBox(
                    decoration:
                        BoxDecoration(color: Colors.black.withOpacity(0.25), borderRadius: BorderRadius.circular(8)))),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: widget.width * 0.03),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                      padding: const EdgeInsets.all(10),
                      margin: const EdgeInsets.only(bottom: 10),
                      decoration: BoxDecoration(
                          color: Color(widget.color), borderRadius: const BorderRadius.all(Radius.circular(30))),
                      child: Text(widget.category == null ? "notAvailable".tr() : widget.category!.toUpperCase(),
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: 12,
                              color: Colors.white,
                              fontFamily: widget.isEn ? 'Montserrat' : 'Tajawal',
                              fontWeight: FontWeight.w600))),
                  Padding(
                    padding: EdgeInsetsDirectional.only(end: widget.width * 0.4),
                    child: Text(
                      widget.authorName ?? "notAvailable".tr(),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(color: Colors.white, fontFamily: widget.isEn ? 'Inter-Medium' : 'Tajawal'),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsetsDirectional.only(top: 10, bottom: 8, end: widget.width * 0.4),
                    child: Text(
                      widget.courseName ?? "notAvailable".tr(),
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      style: Theme.of(context).textTheme.headline4!.copyWith(
                          fontFamily: AppConst.isEn ? 'BebasNeue' : 'Tajawal', color: Colors.white, height: 1),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsetsDirectional.only(bottom: 10, end: widget.width * 0.4),
                    child: Text(
                      widget.details ?? "notAvailable".tr(),
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          fontSize: 12, color: Colors.white, fontFamily: widget.isEn ? 'Inter-Medium' : 'Tajawal'),
                    ),
                  ),
                  CustomButton(
                    color: Colors.white,
                    width: widget.width * 0.3,
                    onTap: () => MagicRouter.navigateTo(ClassScreen(classId: widget.masterClassId)),
                    child: Text(
                      widget.released ? 'joinNow'.tr() : 'comingSoon'.tr(),
                      style: Theme.of(context).textTheme.caption!.copyWith(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontFamily: widget.isEn ? 'Montserrat' : 'Tajawal'),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
