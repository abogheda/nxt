// ignore_for_file: must_be_immutable
import 'package:flutter/material.dart';
import 'package:nxt/utils/constants/app_const.dart';
import 'package:transparent_image/transparent_image.dart';
import '../../../../utils/constants/app_text_style.dart';

class HomePostCard extends StatelessWidget {
  final String image;
  final int color;
  final String postType;
  final String postText;
  const HomePostCard({
    Key? key,
    required this.image,
    required this.color,
    required this.postType,
    required this.postText,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(color: Colors.black, borderRadius: BorderRadius.circular(8)),
      margin: EdgeInsets.symmetric(horizontal: width * 0.03, vertical: height * 0.008),
      child: Material(
        elevation: 2,
        borderRadius: BorderRadius.circular(8),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Stack(
              alignment: Alignment.bottomRight,
              children: [
                ClipRRect(
                    borderRadius: BorderRadius.circular(8),
                    child: FadeInImage.memoryNetwork(
                        image: image,
                        placeholder: kTransparentImage,
                        height: height * 0.25,
                        width: double.infinity,
                        fit: BoxFit.cover)),
                Container(
                  height: 7,
                  width: width * 0.22,
                  decoration: BoxDecoration(
                      color: Color(color),
                      borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(8),
                        bottomLeft: Radius.circular(8),
                      )),
                )
              ],
            ),
            SizedBox(height: height * 0.01),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: width * 0.03),
              child: RichText(
                textAlign: TextAlign.start,
                text: TextSpan(
                  text: "@${postType}BYNXT ",
                  style: (AppTextStyles.bold14),
                  children: [
                    TextSpan(
                        text: postText,
                        style: TextStyle(
                            fontSize: 14, fontWeight: FontWeight.w400, fontFamily: isEn ? 'Montserrat' : 'Tajawal')),
                    TextSpan(
                      text: " #POWEREDBYNXT",
                      style: (AppTextStyles.bold14),
                    )
                  ],
                ),
              ),
            ),
            const SizedBox(height: 10),
          ],
        ),
      ),
    );
  }
}
