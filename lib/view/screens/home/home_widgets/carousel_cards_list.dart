// ignore_for_file: must_be_immutable
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:nxt/config/router/router.dart';
import 'package:nxt/utils/constants/app_const.dart';
import 'package:nxt/utils/constants/app_images.dart';
import 'package:nxt/utils/helpers/popup_helper.dart';
import 'package:nxt/view/screens/carousel/carousel_screen.dart';
import 'package:nxt/view/screens/home/cubit/home_cubit.dart';
import 'package:transparent_image/transparent_image.dart';

import '../../../widgets/shimmer_widgets/custom_shimmer.dart';

class CarouselCardsList extends StatelessWidget {
  const CarouselCardsList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<HomeCubit, HomeState>(
      listener: (context, state) {
        if (state is GetHomeCarouselsFailed) PopupHelper.showBasicSnack(msg: 'errorOccurred'.tr(), color: Colors.red);
      },
      builder: (context, state) {
        final cubit = HomeCubit.get(context);
        final carouselList = cubit.carouselModelList;
        if (state is GetHomeCarouselsLoading || carouselList == null) return const CarouselCardShimmer();
        if (carouselList.isEmpty) return const SizedBox();
        return Container(
          height: 270.h,
          width: double.infinity,
          margin: EdgeInsets.symmetric(vertical: height * 0.01),
          child: ListView.separated(
              physics: const BouncingScrollPhysics(),
              scrollDirection: Axis.horizontal,
              itemCount: carouselList.length,
              padding: EdgeInsets.symmetric(horizontal: width * 0.03),
              separatorBuilder: (context, index) => SizedBox(width: width * 0.03),
              itemBuilder: (_, index) {
                return InkWell(
                  onTap: () => MagicRouter.navigateTo(BlocProvider.value(
                    value: cubit..getHomeCarouselById(id: carouselList[index].id!),
                    child: CarouselScreen(id: carouselList[index].id!),
                  )),
                  splashColor: Colors.white.withOpacity(0.2),
                  borderRadius: BorderRadius.circular(8),
                  child: Ink(
                    width: width * 0.45,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      image: DecorationImage(
                        fit: BoxFit.cover,
                        image: FadeInImage.memoryNetwork(
                          placeholder: kTransparentImage,
                          image: carouselList[index].image ?? placeHolderUrl,
                        ).image,
                      ),
                    ),
                  ),
                );
              }),
        );
      },
    );
  }
}

class CarouselCardShimmer extends StatelessWidget {
  const CarouselCardShimmer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 270.h,
      width: double.infinity,
      margin: EdgeInsets.symmetric(vertical: height * 0.01),
      child: ListView.separated(
        itemCount: 3,
        shrinkWrap: true,
        scrollDirection: Axis.horizontal,
        physics: const BouncingScrollPhysics(),
        padding: EdgeInsets.symmetric(horizontal: width * 0.03),
        separatorBuilder: (context, index) => SizedBox(width: width * 0.03),
        itemBuilder: (context, index) {
          return CustomShimmer(
            child: Container(
              width: width * 0.45,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(8),
              ),
            ),
          );
        },
      ),
    );
  }
}
