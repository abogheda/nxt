import 'package:flutter/material.dart';
import '../../../../utils/constants/app_const.dart';
import '../../../widgets/shimmer_widgets/custom_shimmer.dart';

class HomeClassCardShimmer extends StatelessWidget {
  const HomeClassCardShimmer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: height * 0.008, horizontal: width * 0.03),
      child: CustomShimmer(
        child: Container(
          height: height * 0.29,
          decoration: ShapeDecoration(
            color: Colors.grey[400]!,
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)),
          ),
        ),
      ),
    );
  }
}
