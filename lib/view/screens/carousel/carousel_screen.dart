import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nxt/utils/constants/app_const.dart';
import 'package:nxt/utils/constants/app_images.dart';
import 'package:nxt/view/screens/home/cubit/home_cubit.dart';
import 'package:transparent_image/transparent_image.dart';

import '../../../utils/constants/app_colors.dart';
import '../../widgets/custom_html_widget.dart';
import '../../widgets/error_state_widget.dart';
import '../../widgets/loading_state_widget.dart';
import '../navigation_and_appbar/import_widget.dart';

class CarouselScreen extends StatelessWidget {
  const CarouselScreen({Key? key, required this.id}) : super(key: key);
  final String id;
  @override
  Widget build(BuildContext context) {
    return CustomScaffoldWidget(
      extendBodyBehindAppBar: false,
      isBackArrow: true,
      isThereLogo: true,
      isThereActions: true,
      body: BlocConsumer<HomeCubit, HomeState>(
        listener: (context, state) {},
        builder: (context, state) {
          final cubit = HomeCubit.get(context);
          if (state is GetHomeCarouselByIdLoading) return const LoadingStateWidget();
          if (state is GetHomeCarouselByIdFailed || HomeCubit.get(context).carouselModel == null) {
            return ErrorStateWidget(
              hasRefresh: true,
              onRefresh: () async => await cubit.getHomeCarouselById(id: id),
            );
          }
          final carouselModel = cubit.carouselModel!;
          return RefreshIndicator(
            onRefresh: () async => cubit.getHomeCarouselById(id: carouselModel.id!),
            child: ListView(
              shrinkWrap: true,
              children: [
                Padding(
                  padding: EdgeInsets.symmetric(vertical: height * 0.0),
                  child: InkWell(
                    onTap: () {},
                    child: Material(
                      color: Colors.white,
                      elevation: 2,
                      child: Container(
                        decoration: BoxDecoration(border: Border.all(color: AppColors.greyOutText)),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              width: double.infinity,
                              child: FadeInImage.memoryNetwork(
                                fit: BoxFit.cover,
                                height: height * 0.5,
                                width: double.infinity,
                                placeholder: kTransparentImage,
                                image: carouselModel.media ?? placeHolderUrl,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
                const SizedBox(height: 12),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: width * 0.025),
                  child: Text("${'details'.tr()}:",
                      style: TextStyle(
                          fontSize: 15, fontFamily: isEn ? 'Montserrat' : 'Tajawal', fontWeight: FontWeight.bold)),
                ),
                Padding(
                    padding: EdgeInsets.symmetric(horizontal: width * 0.025),
                    child: const Divider(color: Colors.black, thickness: 1.5)),
                CustomHtmlWidget(data: carouselModel.content)
              ],
            ),
          );
        },
      ),
    );
  }
}
