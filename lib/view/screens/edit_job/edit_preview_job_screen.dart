// ignore_for_file: use_build_context_synchronously

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../config/router/router.dart';
import '../../../data/service/hive_services.dart';
import 'package:transparent_image/transparent_image.dart';
import '../../../../../../utils/constants/resources.dart';
import '../../../../data/enum/talent_type.dart';
import '../../../../utils/helpers/popup_helper.dart';
import '../../widgets/app_loader.dart';
import '../../widgets/custom_button.dart';
import '../connect/connect_screens/experts_and_jobs/screens/widgets/job_description_widget.dart';
import '../navigation_and_appbar/import_widget.dart';
import '../organization_connect/controller/organization_connect_cubit.dart';
import 'controller/edit_job_cubit.dart';

class EditPreviewJobScreen extends StatefulWidget {
  const EditPreviewJobScreen({Key? key, required this.jobId}) : super(key: key);
  final String jobId;
  @override
  State<EditPreviewJobScreen> createState() => _EditPreviewJobScreenState();
}

class _EditPreviewJobScreenState extends State<EditPreviewJobScreen> {
  final now = DateTime.now();
  final hiveUser = HiveHelper.getUserInfo!.user!;
  String getUserName() {
    String name = '';
    if (HiveHelper.getUserInfo!.user!.role == 'Organization') {
      name = HiveHelper.getUserInfo!.user!.organization!.name!;
    } else if (HiveHelper.getUserInfo!.user!.role == 'Expert') {
      name = HiveHelper.getUserInfo!.user!.expert!.name!;
    }
    return name;
  }

  String getUserAvatar() {
    String avatar = '';
    if (HiveHelper.getUserInfo!.user!.role == 'Organization') {
      avatar = HiveHelper.getUserInfo!.user!.organization!.logo!;
    } else if (HiveHelper.getUserInfo!.user!.role == 'Expert') {
      avatar = HiveHelper.getUserInfo!.user!.expert!.logo!;
    }
    return avatar;
  }

  @override
  Widget build(BuildContext context) {
    final cubit = EditJobCubit.get(context);
    return CustomScaffoldWidget(
        backgroundColor: Colors.white,
        extendBodyBehindAppBar: false,
        isBackArrow: true,
        isInProfile: false,
        isLogoClickable: true,
        body: BlocConsumer<EditJobCubit, EditJobState>(
          listener: (context, state) {
            if (state is EditJobFailed) {
              PopupHelper.showBasicSnack(msg: state.msg.toString(), color: Colors.red);
            }
          },
          builder: (context, state) {
            return Column(
              children: [
                Expanded(
                  child: ListView(
                    children: [
                      Stack(
                        alignment: widget.isEn ? Alignment.bottomRight : Alignment.bottomLeft,
                        children: [
                          Container(
                            width: double.infinity,
                            height: widget.height * 0.27,
                            color: AppColors.greyOutText,
                          ),
                          cubit.coverImage == null
                              ? Image.network(
                                  cubit.coverImageUrl!,
                                  fit: BoxFit.cover,
                                  width: double.infinity,
                                  height: widget.height * 0.27,
                                )
                              : Image.file(
                                  cubit.coverImage!,
                                  fit: BoxFit.cover,
                                  width: double.infinity,
                                  height: widget.height * 0.27,
                                ),
                          Container(
                            height: 7,
                            width: widget.width * 0.22,
                            decoration: BoxDecoration(
                              color: cubit.verticalId == TalentCategoryType.music.id
                                  ? AppColors.blueColor
                                  : cubit.verticalId == TalentCategoryType.filmMaking.id
                                      ? AppColors.redColor
                                      : AppColors.yellowColor,
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(widget.isEn ? 8 : 0),
                                bottomLeft: Radius.circular(widget.isEn ? 8 : 0),
                                topRight: Radius.circular(widget.isEn ? 0 : 8),
                                bottomRight: Radius.circular(widget.isEn ? 0 : 8),
                              ),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: widget.height * 0.015),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: widget.width * 0.03),
                        child: Row(
                          children: [
                            ClipRRect(
                              borderRadius: BorderRadius.circular(50),
                              child: FadeInImage.memoryNetwork(
                                  image: getUserAvatar() == '' ? userAvatarPlaceHolderUrl : getUserAvatar(),
                                  fit: BoxFit.cover,
                                  placeholder: kTransparentImage,
                                  width: 45,
                                  height: 45),
                            ),
                            const SizedBox(width: 8),
                            Expanded(
                              child: Text(
                                getUserName() == '' ? "notAvailable".tr() : getUserName(),
                                style: Theme.of(context).textTheme.titleLarge!.copyWith(
                                      fontFamily: widget.isEn ? 'BebasNeue' : 'Tajawal',
                                      fontWeight: widget.isEn ? FontWeight.normal : FontWeight.bold,
                                      overflow: TextOverflow.ellipsis,
                                    ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      JobDescriptionWidget(
                        jobTitle:
                            widget.isEn ? cubit.englishJobTitleController.text : cubit.arabicJobTitleController.text,
                        jobSubTitle: widget.isEn
                            ? cubit.englishJobSubTitleController.text
                            : cubit.arabicJobSubTitleController.text,
                        jobDescription: widget.isEn
                            ? cubit.englishJobDescriptionController.text
                            : cubit.arabicJobDescriptionController.text,
                        area: widget.isEn ? cubit.englishJobAreaController.text : cubit.arabicJobAreaController.text,
                        gender: cubit.gender!,
                        ageTo: cubit.ageToController.text,
                        ageFrom: cubit.ageFromController.text,
                        jobStartDate: cubit.jobStartDate!.toIso8601String(),
                        jobEndDate: cubit.jobEndDate!.toIso8601String(),
                      )
                    ],
                  ),
                ),
                ColoredBox(
                  color: Colors.black,
                  child: state is EditJobLoading
                      ? SizedBox(
                          height: widget.height * 0.08,
                          width: double.infinity,
                          child: const AppLoader(color: Colors.white))
                      : CustomButton(
                          onTap: () async {
                            await cubit.editJob(jobId: widget.jobId);
                            MagicRouter.navigateAndPopAll(Navigation(navigationIndex: 3));
                            await OrganizationConnectCubit.get(context).getAllJobsForOwner();
                          },
                          height: widget.height * 0.08,
                          width: double.infinity,
                          textColor: Colors.white,
                          child: Text(
                            'updateJob'.tr().toUpperCase(),
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 18,
                              color: Colors.white,
                              fontWeight: FontWeight.w100,
                              fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal',
                            ),
                          ),
                        ),
                ),
              ],
            );
          },
        ));
  }
}
