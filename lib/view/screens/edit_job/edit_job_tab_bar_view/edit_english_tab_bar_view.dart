import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import '../../../../../utils/constants/resources.dart';
import '../../../../utils/helpers/validators.dart';
import '../../../widgets/custom_text_field.dart';
import '../../organization_connect/component/create_job_header_text.dart';
import '../controller/edit_job_cubit.dart';

class EditEnglishTabBarView extends StatelessWidget {
  const EditEnglishTabBarView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final cubit = EditJobCubit.get(context);
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: width * 0.03),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox(height: height * 0.01),
              CreateJobHeaderText(text: 'jobTitleInEnglish'.tr(), horizontalPadding: 0),
              CustomTextField(
                hint: 'addJobTitle'.tr(),
                validator: Validators.generalField,
                type: TextInputType.text,
                controller: cubit.englishJobTitleController,
                onFieldSubmitted: (_) {
                  FocusScope.of(context).requestFocus(cubit.englishSubTitleFocusNode);
                },
              ),
              SizedBox(height: height * 0.01),
              CreateJobHeaderText(text: 'subTitleInEnglish'.tr(), horizontalPadding: 0),
              CustomTextField(
                hint: 'addJobSubTitle'.tr(),
                validator: Validators.generalField,
                type: TextInputType.text,
                controller: cubit.englishJobSubTitleController,
                focusNode: cubit.englishSubTitleFocusNode,
                onFieldSubmitted: (_) {
                  FocusScope.of(context).requestFocus(cubit.englishDescriptionFocusNode);
                },
              ),
              SizedBox(height: height * 0.01),
              CreateJobHeaderText(text: 'jobDescriptionInEnglish'.tr(), horizontalPadding: 0),
              CustomTextField(
                hint: 'addJobDescription'.tr(),
                validator: Validators.generalField,
                type: TextInputType.text,
                controller: cubit.englishJobDescriptionController,
                focusNode: cubit.englishDescriptionFocusNode,
                onFieldSubmitted: (_) {
                  FocusScope.of(context).requestFocus(cubit.englishAreaFocusNode);
                },
              ),
              SizedBox(height: height * 0.01),
              CreateJobHeaderText(text: 'areaInEnglish'.tr(), horizontalPadding: 0),
              CustomTextField(
                hint: 'addJobArea'.tr(),
                validator: Validators.generalField,
                type: TextInputType.text,
                controller: cubit.englishJobAreaController,
                focusNode: cubit.englishAreaFocusNode,
                onFieldSubmitted: (_) {},
              ),
            ],
          ),
        ],
      ),
    );
  }
}
