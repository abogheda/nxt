import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import '../../../../../utils/constants/resources.dart';
import '../../../../utils/helpers/validators.dart';
import '../../../widgets/custom_text_field.dart';
import '../../organization_connect/component/create_job_header_text.dart';
import '../controller/edit_job_cubit.dart';

class EditArabicTabBarView extends StatelessWidget {
  const EditArabicTabBarView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final cubit = EditJobCubit.get(context);
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: width * 0.03),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox(height: height * 0.01),
              CreateJobHeaderText(text: 'jobTitleInArabic'.tr(), horizontalPadding: 0),
              CustomTextField(
                hint: 'addJobTitle'.tr(),
                validator: Validators.generalField,
                type: TextInputType.text,
                controller: cubit.arabicJobTitleController,
                onFieldSubmitted: (_) {
                  FocusScope.of(context).requestFocus(cubit.arabicSubTitleFocusNode);
                },
              ),
              SizedBox(height: height * 0.01),
              CreateJobHeaderText(text: 'subTitleInArabic'.tr(), horizontalPadding: 0),
              CustomTextField(
                hint: 'addJobSubTitle'.tr(),
                validator: Validators.generalField,
                type: TextInputType.text,
                controller: cubit.arabicJobSubTitleController,
                focusNode: cubit.arabicSubTitleFocusNode,
                onFieldSubmitted: (_) {
                  FocusScope.of(context).requestFocus(cubit.arabicDescriptionFocusNode);
                },
              ),
              SizedBox(height: height * 0.01),
              CreateJobHeaderText(text: 'jobDescriptionInArabic'.tr(), horizontalPadding: 0),
              CustomTextField(
                hint: 'addJobDescription'.tr(),
                validator: Validators.generalField,
                type: TextInputType.text,
                controller: cubit.arabicJobDescriptionController,
                focusNode: cubit.arabicDescriptionFocusNode,
                onFieldSubmitted: (_) {
                  FocusScope.of(context).requestFocus(cubit.arabicAreaFocusNode);
                },
              ),
              SizedBox(height: height * 0.01),
              CreateJobHeaderText(text: 'areaInArabic'.tr(), horizontalPadding: 0),
              CustomTextField(
                hint: 'addJobArea'.tr(),
                validator: Validators.generalField,
                type: TextInputType.text,
                controller: cubit.arabicJobAreaController,
                focusNode: cubit.arabicAreaFocusNode,
                onFieldSubmitted: (_) {},
              ),
            ],
          ),
        ],
      ),
    );
  }
}
