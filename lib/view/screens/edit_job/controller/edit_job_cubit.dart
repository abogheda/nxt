// ignore_for_file: unnecessary_import, depend_on_referenced_packages

import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';

import '../../../../data/models/community/countries_model.dart';
import '../../../../data/models/community/create_job_model.dart';
import '../../../../data/models/jobs/all_job_model.dart';
import '../../../../data/repos/general_repo.dart';
import '../../../../data/repos/imports.dart';
import '../../../../utils/constants/app_images.dart';
import '../../../../utils/helpers/picker_helper.dart';

part 'edit_job_state.dart';

class EditJobCubit extends Cubit<EditJobState> {
  EditJobCubit() : super(CreateJobInitial());
  static EditJobCubit get(context) => BlocProvider.of(context);
  File? coverImage;
  CreateJobModel? postJobModel;
  List<CountriesModel>? countriesList;
  String? coverImageUrl;
  String? verticalId;
  String? gender;
  String? jobType;
  List<String> selectedCountryList = [];
  List<String> selectedCountryIdList = [];
  AllJobModel? allJobModel;
  DateTime? jobStartDate, jobEndDate;
  GlobalKey<FormState> ageFormKey = GlobalKey<FormState>(), datesFormKey = GlobalKey<FormState>();
  TextEditingController englishJobTitleController = TextEditingController(),
      englishJobSubTitleController = TextEditingController(),
      englishJobDescriptionController = TextEditingController(),
      englishJobAreaController = TextEditingController(),
      arabicJobTitleController = TextEditingController(),
      arabicJobSubTitleController = TextEditingController(),
      arabicJobDescriptionController = TextEditingController(),
      arabicJobAreaController = TextEditingController(),
      ageFromController = TextEditingController(),
      ageToController = TextEditingController();
  FocusNode englishSubTitleFocusNode = FocusNode(),
      englishDescriptionFocusNode = FocusNode(),
      englishAreaFocusNode = FocusNode(),
      arabicTitleFocusNode = FocusNode(),
      arabicSubTitleFocusNode = FocusNode(),
      arabicDescriptionFocusNode = FocusNode(),
      arabicAreaFocusNode = FocusNode(),
      ageToFocusNode = FocusNode();
  @override
  Future<void> close() {
    englishJobTitleController.dispose();
    englishJobSubTitleController.dispose();
    englishJobDescriptionController.dispose();
    englishJobAreaController.dispose();
    englishSubTitleFocusNode.dispose();
    englishDescriptionFocusNode.dispose();
    englishAreaFocusNode.dispose();
    arabicJobTitleController.dispose();
    arabicJobSubTitleController.dispose();
    arabicJobDescriptionController.dispose();
    arabicJobAreaController.dispose();
    arabicSubTitleFocusNode.dispose();
    arabicDescriptionFocusNode.dispose();
    arabicAreaFocusNode.dispose();
    return super.close();
  }

  Future pickCoverImage() async {
    coverImage = await PickerHelper.pickGalleryImage();
    emit(UploadCoverImageLoading());
  }

  void createPostJobModel() {
    postJobModel = CreateJobModel(
      poster: coverImageUrl ?? placeHolderUrl,
      category: verticalId,
      gender: gender,
      uploadType: jobType,
      country: selectedCountryIdList,
      ageFrom: ageFromController.text.toString().trim(),
      ageTo: ageToController.text.toString().trim(),
      jobStartDate: jobStartDate!.toIso8601String(),
      jobEndDate: jobEndDate!.toIso8601String(),
      lang: [
        PostJobModelLang(
          title: englishJobTitleController.text.trim(),
          supTitle: englishJobSubTitleController.text.trim(),
          description: englishJobDescriptionController.text.trim(),
          area: englishJobAreaController.text.trim(),
        ),
        PostJobModelLang(
          title: arabicJobTitleController.text.trim(),
          supTitle: arabicJobSubTitleController.text.trim(),
          description: arabicJobDescriptionController.text.trim(),
          area: arabicJobAreaController.text.trim(),
        ),
      ],
    );
  }

  Future<void> editJob({required String jobId}) async {
    emit(EditJobLoading());
    if (coverImage != null) {
      final response = await GeneralRepo.uploadMedia([coverImage!]);
      response.fold(
        (url) async {
          coverImageUrl = url.first;
          emit(UploadCoverImageSuccess());
        },
        (errorMsg) => emit(UploadCoverImageFailed(errorMsg)),
      );
    }
    createPostJobModel();
    final res = await CommunityRepo.editJob(jobId: jobId, postJobModel: postJobModel!);
    if (res.data != null) {
      emit(EditJobSuccess());
    } else {
      emit(EditJobFailed(res.message ?? ""));
    }
  }

  Future<void> getCountries() async {
    emit(GetCountriesLoading());
    final res = await CommunityRepo.getCountries();
    if (res.data != null) {
      countriesList = res.data;
      emit(GetCountriesSuccess());
    } else {
      emit(GetCountriesFailed(res.message ?? ""));
    }
  }

  Future<void> getJobByIdAllLanguage({required String jobId}) async {
    emit(GetJobWithLanguagesLoading());
    final res = await CommunityRepo.getJobByIdAllLanguage(jobId: jobId);
    if (res.data != null) {
      allJobModel = res.data;
      coverImageUrl = allJobModel!.poster;
      verticalId = allJobModel!.category!.id;
      gender = allJobModel!.gender;
      jobType = allJobModel!.uploadType;
      jobStartDate = DateTime.parse(allJobModel!.jobStartDate ?? DateTime.now().toString());
      jobEndDate = DateTime.parse(allJobModel!.jobEndDate ?? DateTime.now().toString());
      ageFromController.text = allJobModel!.ageFrom!;
      ageToController.text = allJobModel!.ageTo!;
      englishJobTitleController.text = allJobModel!.lang![0]!.title!;
      englishJobSubTitleController.text = allJobModel!.lang![0]!.supTitle!;
      englishJobDescriptionController.text = allJobModel!.lang![0]!.description!;
      englishJobAreaController.text = allJobModel!.lang![0]!.area!;
      arabicJobTitleController.text = allJobModel!.lang![1]!.title!;
      arabicJobSubTitleController.text = allJobModel!.lang![1]!.supTitle!;
      arabicJobDescriptionController.text = allJobModel!.lang![1]!.description!;
      arabicJobAreaController.text = allJobModel!.lang![1]!.area!;
      for (var country in allJobModel!.country!) {
        selectedCountryIdList.add(country!.id!);
        selectedCountryList.add(country.name!);
      }
      emit(GetJobWithLanguagesSuccess());
    } else {
      emit(GetJobWithLanguagesFailed(res.message ?? ""));
    }
  }

  void updateCountryList() {
    emit(UpdateCountryListState());
  }

  void addToSelectedTagsList({
    required String name,
    required String id,
  }) {
    emit(CreateJobInitial());
    if (!selectedCountryList.contains(name)) {
      selectedCountryList.add(name);
      selectedCountryIdList.add(id);
    }
    emit(AddToSelectedCountryListState());
  }

  void removeFromSelectedTagsList({
    required String name,
    required String id,
  }) {
    emit(CreateJobInitial());
    if (selectedCountryList.contains(name)) {
      selectedCountryList.remove(name);
      selectedCountryIdList.remove(id);
    }
    emit(RemoveFromSelectedCountryListState());
  }
}
