part of 'edit_job_cubit.dart';

@immutable
abstract class EditJobState {}

class CreateJobInitial extends EditJobState {}

class UpdateCountryListState extends EditJobState {}

class AddToSelectedCountryListState extends EditJobState {}

class RemoveFromSelectedCountryListState extends EditJobState {}

class GetCountriesSuccess extends EditJobState {}

class GetCountriesLoading extends EditJobState {}

class GetCountriesFailed extends EditJobState {
  final String msg;
  GetCountriesFailed(this.msg);
}

class EditJobSuccess extends EditJobState {}

class EditJobLoading extends EditJobState {}

class EditJobFailed extends EditJobState {
  final String msg;
  EditJobFailed(this.msg);
}

class UploadCoverImageSuccess extends EditJobState {}

class UploadCoverImageLoading extends EditJobState {}

class UploadCoverImageFailed extends EditJobState {
  final String msg;
  UploadCoverImageFailed(this.msg);
}

class GetJobWithLanguagesSuccess extends EditJobState {}

class GetJobWithLanguagesLoading extends EditJobState {}

class GetJobWithLanguagesFailed extends EditJobState {
  final String msg;
  GetJobWithLanguagesFailed(this.msg);
}
