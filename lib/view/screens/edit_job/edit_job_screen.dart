// ignore_for_file: deprecated_member_use

import 'package:dotted_border/dotted_border.dart';
import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:easy_image_viewer/easy_image_viewer.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:nxt/config/router/router.dart';
import 'package:nxt/utils/constants/resources.dart';
import 'package:nxt/view/screens/organization_connect/component/create_job_header_text.dart';
import 'package:nxt/view/widgets/loading_state_widget.dart';

import '../../../data/enum/talent_type.dart';
import '../../../data/enum/upload_type.dart';
import '../../../utils/helpers/picker_helper.dart';
import '../../../utils/helpers/popup_helper.dart';
import '../../../utils/helpers/validators.dart';
import '../../widgets/custom_button.dart';
import '../../widgets/custom_drop_down_field.dart';
import '../../widgets/custom_text_field.dart';
import '../../widgets/error_state_widget.dart';
import '../../widgets/shimmer_widgets/custom_shimmer.dart';
import '../navigation_and_appbar/import_widget.dart';
import 'controller/edit_job_cubit.dart';
import 'edit_job_tab_bar_view/edit_language_tab_bar_view.dart';
import 'edit_preview_job_screen.dart';

class EditJobScreen extends StatefulWidget {
  final String jobId;
  const EditJobScreen({Key? key, required this.jobId}) : super(key: key);

  @override
  State<EditJobScreen> createState() => _EditJobScreenState();
}

class _EditJobScreenState extends State<EditJobScreen> {
  @override
  Widget build(BuildContext context) {
    return CustomScaffoldWidget(
      extendBodyBehindAppBar: false,
      isBackArrow: true,
      isThereLogo: true,
      isThereActions: true,
      body: BlocProvider(
        create: (context) => EditJobCubit()
          ..getJobByIdAllLanguage(jobId: widget.jobId)
          ..getCountries(),
        child: BlocConsumer<EditJobCubit, EditJobState>(
          listener: (context, state) {},
          builder: (context, state) {
            final cubit = EditJobCubit.get(context);
            if (state is GetJobWithLanguagesLoading || cubit.allJobModel == null) return const LoadingStateWidget();
            if (state is GetCountriesFailed) {
              return ErrorStateWidget(hasRefresh: true, onRefresh: () async => await cubit.getCountries());
            }
            if (state is GetJobWithLanguagesFailed) {
              return ErrorStateWidget(hasRefresh: true, onRefresh: () async => await cubit.getCountries());
            }
            return Stack(
              children: [
                Positioned(
                  top: 0,
                  bottom: 0,
                  right: 0,
                  left: 0,
                  child: ListView(
                    shrinkWrap: true,
                    children: [
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: widget.width * 0.03),
                        child: InkWell(
                          onTap: () async {
                            FocusManager.instance.primaryFocus?.unfocus();
                            await cubit.pickCoverImage();
                            setState(() {});
                          },
                          borderRadius: BorderRadius.circular(6),
                          splashColor: Colors.black26,
                          child: DottedBorder(
                            borderType: BorderType.RRect,
                            radius: const Radius.circular(6),
                            dashPattern: const [15, 10],
                            child: ClipRRect(
                              borderRadius: const BorderRadius.all(Radius.circular(6.0)),
                              child: Center(
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(6.0),
                                  child: SizedBox(
                                    height: widget.height * 0.2,
                                    child: Stack(
                                      children: <Widget>[
                                        cubit.coverImage == null
                                            ? GestureDetector(
                                                onTap: () async => await showImageViewer(
                                                    context, NetworkImage(cubit.allJobModel!.poster!),
                                                    swipeDismissible: true, doubleTapZoomable: true),
                                                child: Image.network(cubit.allJobModel!.poster!,
                                                    fit: BoxFit.cover, width: widget.width),
                                              )
                                            : GestureDetector(
                                                onTap: () async => await showImageViewer(
                                                    context, FileImage(cubit.coverImage!),
                                                    swipeDismissible: true, doubleTapZoomable: true),
                                                child: Image.file(cubit.coverImage!,
                                                    fit: BoxFit.cover, width: widget.width),
                                              ),
                                        Positioned(
                                          left: 0,
                                          right: 0,
                                          bottom: 0,
                                          top: 0,
                                          child: Align(
                                            child: CircleAvatar(
                                              backgroundColor: Colors.black38,
                                              radius: 32.0,
                                              child: cubit.coverImage == null
                                                  ? InkWell(
                                                      onTap: () => cubit.pickCoverImage(),
                                                      child: const Icon(Icons.change_circle_outlined,
                                                          size: 36, color: Colors.white),
                                                    )
                                                  : InkWell(
                                                      onTap: () => setState(() => cubit.coverImage = null),
                                                      child:
                                                          const Icon(Icons.delete_rounded, size: 36, color: Colors.red),
                                                    ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: widget.height * 0.015),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: widget.width * 0.03),
                        child: CustomDropDownTextField<String>(
                          hint: 'chooseVertical'.tr(),
                          value: cubit.verticalId,
                          hintTextStyle: Theme.of(context).textTheme.bodyText1!.copyWith(
                              fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal', color: const Color(0xFFA5A5A5)),
                          items: [
                            DropdownMenuItem(
                              value: TalentCategoryType.music.id,
                              child: Text(
                                'music'.tr(),
                                style: Theme.of(context).textTheme.titleMedium!.copyWith(
                                      fontFamily: AppConst.isEn ? 'BebasNeue' : 'Tajawal',
                                      color: AppColors.blueColor,
                                    ),
                              ),
                            ),
                            DropdownMenuItem(
                              value: TalentCategoryType.filmMaking.id,
                              child: Text(
                                'filmMaking'.tr(),
                                style: Theme.of(context).textTheme.titleMedium!.copyWith(
                                      fontFamily: AppConst.isEn ? 'BebasNeue' : 'Tajawal',
                                      color: AppColors.redColor,
                                    ),
                              ),
                            ),
                            DropdownMenuItem(
                              value: TalentCategoryType.acting.id,
                              child: Text(
                                'acting'.tr(),
                                style: Theme.of(context).textTheme.titleMedium!.copyWith(
                                      fontFamily: AppConst.isEn ? 'BebasNeue' : 'Tajawal',
                                      color: AppColors.yellowColor,
                                    ),
                              ),
                            )
                          ],
                          prefix: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: SvgPicture.asset('assets/images/dropdown_logo_icon.svg'),
                          ),
                          onChanged: (val) {
                            setState(() {
                              cubit.verticalId = val;
                            });
                          },
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: widget.width * 0.03),
                        child: CustomDropDownTextField<String>(
                          value: cubit.gender,
                          hintTextStyle: Theme.of(context).textTheme.bodyText1!.copyWith(
                              fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal', color: const Color(0xFFA5A5A5)),
                          items: [
                            DropdownMenuItem(
                              value: 'Male',
                              child: Text(
                                'Male'.tr(),
                                style: TextStyle(fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal'),
                              ),
                            ),
                            DropdownMenuItem(
                              value: 'Female',
                              child: Text(
                                'Female'.tr(),
                                style: TextStyle(fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal'),
                              ),
                            ),
                            DropdownMenuItem(
                              value: 'general',
                              child: Text(
                                'general'.tr(),
                                style: TextStyle(fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal'),
                              ),
                            )
                          ],
                          hint: 'chooseGender'.tr(),
                          prefix: const Icon(Icons.person_outline_rounded),
                          onChanged: (val) => cubit.gender = val,
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: widget.width * 0.03),
                        child: CustomDropDownTextField<String>(
                          value: cubit.jobType,
                          hintTextStyle: Theme.of(context).textTheme.bodyText1!.copyWith(
                              fontFamily: widget.isEn ? 'Montserrat' : 'Tajawal', color: const Color(0xFFA5A5A5)),
                          items: [
                            DropdownMenuItem(
                              value: UploadType.video.type,
                              child: Text(
                                UploadType.video.type.tr(),
                                style: TextStyle(fontFamily: widget.isEn ? 'Montserrat' : 'Tajawal'),
                              ),
                            ),
                            DropdownMenuItem(
                              value: UploadType.audio.type,
                              child: Text(
                                UploadType.audio.type.tr(),
                                style: TextStyle(fontFamily: widget.isEn ? 'Montserrat' : 'Tajawal'),
                              ),
                            ),
                            DropdownMenuItem(
                              value: UploadType.text.type,
                              child: Text(
                                UploadType.text.type.tr(),
                                style: TextStyle(fontFamily: widget.isEn ? 'Montserrat' : 'Tajawal'),
                              ),
                            )
                          ],
                          hint: 'chooseType'.tr(),
                          prefix: const Icon(Icons.description_outlined),
                          onChanged: (val) => cubit.jobType = val,
                        ),
                      ),
                      BlocBuilder<EditJobCubit, EditJobState>(
                        builder: (context, state) {
                          final cubit = EditJobCubit.get(context);
                          if (state is GetCountriesLoading || cubit.countriesList == null) {
                            return CustomShimmer(
                              child: Padding(
                                padding: EdgeInsets.symmetric(horizontal: widget.width * 0.03),
                                child: Container(
                                  height: widget.height * 0.066,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(8),
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                            );
                          }
                          final countriesList = cubit.countriesList ?? [];
                          List selectedCountry = cubit.selectedCountryList;
                          return Container(
                            margin: EdgeInsets.symmetric(horizontal: widget.width * 0.03),
                            padding: EdgeInsets.symmetric(horizontal: widget.width * 0.03),
                            decoration: BoxDecoration(
                              border: Border.all(color: Colors.black),
                              borderRadius: BorderRadius.circular(8),
                            ),
                            child: Row(
                              children: [
                                const Icon(Icons.flag_outlined),
                                SizedBox(width: widget.width * 0.02),
                                Expanded(
                                  child: DropdownButtonHideUnderline(
                                    child: DropdownButton2(
                                      isDense: false,
                                      isExpanded: true,
                                      itemPadding: EdgeInsets.zero,
                                      dropdownPadding: EdgeInsets.zero,
                                      buttonWidth: widget.width * 0.65,
                                      dropdownWidth: widget.width * 0.65,
                                      dropdownMaxHeight: widget.height * 0.55,
                                      scrollbarRadius: const Radius.circular(50),
                                      onChanged: (value) => cubit.updateCountryList(),
                                      icon: const Icon(Icons.keyboard_arrow_down_rounded),
                                      hint: Text('chooseCountry'.tr(), textAlign: TextAlign.start),
                                      value: selectedCountry.isEmpty ? null : selectedCountry.last,
                                      buttonPadding: EdgeInsets.symmetric(vertical: widget.height * 0.003),
                                      style: TextStyle(
                                          fontFamily: widget.isEn ? 'Montserrat' : 'Tajawal',
                                          color: selectedCountry.isEmpty ? const Color(0xFFA5A5A5) : Colors.black),
                                      selectedItemBuilder: (context) {
                                        return countriesList.map(
                                          (item) {
                                            return Container(
                                                alignment: AlignmentDirectional.centerStart,
                                                child: Text(selectedCountry.join(', ')));
                                          },
                                        ).toList();
                                      },
                                      items: countriesList.map((item) {
                                        return DropdownMenuItem<String>(
                                          value: item.name ?? "notAvailable".tr(),
                                          child: StatefulBuilder(
                                            builder: (context, menuSetState) {
                                              bool isSelected = selectedCountry.contains(item.name);
                                              return InkWell(
                                                onTap: () {
                                                  isSelected
                                                      ? cubit.removeFromSelectedTagsList(name: item.name!, id: item.id!)
                                                      : cubit.addToSelectedTagsList(name: item.name!, id: item.id!);
                                                  setState(() {});
                                                  menuSetState(() {});
                                                  cubit.updateCountryList();
                                                },
                                                child: Container(
                                                  height: double.infinity,
                                                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                                                  child: Row(
                                                    children: [
                                                      isSelected
                                                          ? const Icon(Icons.check_box_outlined)
                                                          : const Icon(Icons.check_box_outline_blank),
                                                      SizedBox(width: widget.width * 0.01),
                                                      Text(item.name!, style: const TextStyle(fontSize: 12)),
                                                    ],
                                                  ),
                                                ),
                                              );
                                            },
                                          ),
                                        );
                                      }).toList(),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          );
                        },
                      ),
                      CreateJobHeaderText(text: 'age'.tr()),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: widget.width * 0.03),
                        child: Form(
                          key: cubit.ageFormKey,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Expanded(
                                child: StatefulBuilder(builder: (_, stState) {
                                  return CustomTextField(
                                    key: UniqueKey(),
                                    hint: 'ageFrom'.tr(),
                                    controller: cubit.ageFromController,
                                    type: TextInputType.number,
                                    prefix: const Icon(Icons.calendar_today_outlined),
                                    onFieldSubmitted: (_) {
                                      cubit.ageFormKey.currentState!.save();
                                      FocusScope.of(context).requestFocus(cubit.ageToFocusNode);
                                    },
                                    onChange: (_) => cubit.ageFormKey.currentState!.save(),
                                    validator: (value) {
                                      if (value!.isEmpty) {
                                        return 'empty_field'.tr();
                                      } else if (int.parse(value) < 13 || int.parse(value) > 90) {
                                        return 'minAge'.tr();
                                      } else {
                                        return null;
                                      }
                                    },
                                  );
                                }),
                              ),
                              const SizedBox(width: 8),
                              Expanded(
                                child: StatefulBuilder(builder: (_, stState) {
                                  return CustomTextField(
                                    key: UniqueKey(),
                                    hint: 'ageTo'.tr(),
                                    onFieldSubmitted: (_) {
                                      cubit.ageFormKey.currentState!.save();
                                    },
                                    onChange: (_) => cubit.ageFormKey.currentState!.save(),
                                    focusNode: cubit.ageToFocusNode,
                                    validator: (value) {
                                      if (value!.isEmpty) {
                                        return 'empty_field'.tr();
                                      } else if (int.parse(value) < 13 || int.parse(value) > 90) {
                                        return 'maxAge'.tr();
                                      } else {
                                        return null;
                                      }
                                    },
                                    prefix: const Icon(Icons.calendar_today_outlined),
                                    controller: cubit.ageToController,
                                    type: TextInputType.number,
                                  );
                                }),
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(height: widget.height * 0.01),
                      CreateJobHeaderText(text: 'jobDates'.tr()),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: widget.width * 0.03),
                        child: Form(
                          key: cubit.datesFormKey,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Expanded(
                                child: StatefulBuilder(builder: (_, stState) {
                                  return CustomTextField(
                                    key: UniqueKey(),
                                    hint: 'jobStartDate'.tr(),
                                    prefix: const Icon(Icons.calendar_today_outlined),
                                    validator: Validators.generalField,
                                    initialValue: cubit.jobStartDate == null
                                        ? null
                                        : DateFormat("dd/MM/yyyy").format(cubit.jobStartDate!),
                                    onFieldSubmitted: (_) {
                                      cubit.datesFormKey.currentState!.save();
                                    },
                                    onChange: (_) => cubit.datesFormKey.currentState!.save(),
                                    onTap: () async {
                                      FocusManager.instance.primaryFocus?.unfocus();
                                      final date = await PickerHelper.pickDate(
                                          currentTime: DateTime.now(),
                                          minTime: DateTime.now(),
                                          maxTime: DateTime(DateTime.now().year + 2));
                                      if (date != null) {
                                        cubit.jobStartDate = date;
                                        stState(() {});
                                      }
                                    },
                                  );
                                }),
                              ),
                              const SizedBox(width: 8),
                              Expanded(
                                child: StatefulBuilder(builder: (_, stState) {
                                  return CustomTextField(
                                    key: UniqueKey(),
                                    hint: 'jobEndDate'.tr(),
                                    onFieldSubmitted: (_) => cubit.datesFormKey.currentState!.save(),
                                    onChange: (_) => cubit.datesFormKey.currentState!.save(),
                                    validator: Validators.generalField,
                                    prefix: const Icon(Icons.calendar_today_outlined),
                                    initialValue: cubit.jobEndDate == null
                                        ? null
                                        : DateFormat("dd/MM/yyyy").format(cubit.jobEndDate!),
                                    onTap: () async {
                                      if (cubit.jobStartDate == null) {
                                        PopupHelper.showBasicSnack(msg: 'addMinAge'.tr());
                                        return;
                                      }
                                      FocusManager.instance.primaryFocus?.unfocus();
                                      final date = await PickerHelper.pickDate(
                                        currentTime: cubit.jobEndDate ?? cubit.jobStartDate!,
                                        minTime: cubit.jobStartDate!,
                                        maxTime: DateTime(DateTime.now().year + 2),
                                      );
                                      if (date != null) {
                                        cubit.jobEndDate = date;
                                        stState(() {});
                                      }
                                    },
                                  );
                                }),
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(height: widget.height * 0.9, child: const EditLanguageTabBarView())
                    ],
                  ),
                ),
                Positioned(
                  right: 0,
                  left: 0,
                  bottom: 0,
                  child: Container(
                    color: Colors.black,
                    child: CustomButton(
                      height: widget.height * 0.08,
                      width: double.infinity,
                      onTap: () {
                        if (cubit.verticalId == null) {
                          PopupHelper.showBasicSnack(msg: 'chooseVerticalError'.tr());
                          return;
                        }
                        if (cubit.gender == null) {
                          PopupHelper.showBasicSnack(msg: 'chooseGenderError'.tr());
                          return;
                        }
                        if (cubit.jobType == null) {
                          PopupHelper.showBasicSnack(msg: 'chooseTypeError'.tr());
                          return;
                        }
                        if (cubit.selectedCountryIdList.isEmpty) {
                          PopupHelper.showBasicSnack(msg: 'chooseCountryError'.tr());
                          return;
                        }
                        if (cubit.ageToController.text.isNotEmpty &&
                            int.tryParse(cubit.ageToController.text).runtimeType != int) {
                          PopupHelper.showBasicSnack(msg: "enterValidAgeTo".tr());
                          return;
                        }
                        if (cubit.ageFromController.text.isNotEmpty &&
                            int.tryParse(cubit.ageFromController.text).runtimeType != int) {
                          PopupHelper.showBasicSnack(msg: "enterValidAgeFrom".tr());
                          return;
                        }
                        if (int.parse(cubit.ageFromController.value.text) >
                            int.parse(cubit.ageToController.value.text)) {
                          PopupHelper.showBasicSnack(msg: 'ageRangeError'.tr());
                          return;
                        }
                        if (cubit.englishJobTitleController.text.isEmpty ||
                            cubit.englishJobSubTitleController.text.isEmpty ||
                            cubit.englishJobDescriptionController.text.isEmpty ||
                            cubit.englishJobAreaController.text.isEmpty) {
                          PopupHelper.showBasicSnack(msg: 'fillEnglish'.tr());
                          return;
                        }
                        if (cubit.arabicJobTitleController.text.isEmpty ||
                            cubit.arabicJobSubTitleController.text.isEmpty ||
                            cubit.arabicJobDescriptionController.text.isEmpty ||
                            cubit.arabicJobAreaController.text.isEmpty) {
                          PopupHelper.showBasicSnack(msg: 'fillArabic'.tr());
                          return;
                        }
                        if (cubit.ageFormKey.currentState!.validate() && cubit.datesFormKey.currentState!.validate()) {
                          MagicRouter.navigateTo(
                              BlocProvider.value(value: cubit, child: EditPreviewJobScreen(jobId: widget.jobId)));
                        }
                      },
                      child: Text(
                        'previewJob'.tr().toUpperCase(),
                        style: TextStyle(
                          fontSize: 18,
                          color: Colors.white,
                          fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal',
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ),
                )
              ],
            );
          },
        ),
      ),
    );
  }
}
