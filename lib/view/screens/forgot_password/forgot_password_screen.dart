import 'package:custom_timer/custom_timer.dart';
import 'package:easy_localization/easy_localization.dart' as local;
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../config/router/router.dart';
import '../../../utils/constants/app_const.dart';
import '../sign_up/talent/custom_intl_phone_field.dart';
import 'o_t_p_continue_button.dart';
import '../otp/o_t_p_screen.dart';

import '../../../utils/helpers/popup_helper.dart';
import '../../widgets/app_loader.dart';
import 'controller/forgot_password_cubit.dart';

class ForgotPasswordScreen extends StatefulWidget {
  const ForgotPasswordScreen({Key? key}) : super(key: key);

  @override
  State<ForgotPasswordScreen> createState() => _ForgotPasswordScreenState();
}

class _ForgotPasswordScreenState extends State<ForgotPasswordScreen> {
  bool doOnce = true;
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => ForgotPasswordCubit(),
      child: BlocConsumer<ForgotPasswordCubit, ForgotPasswordState>(
        listener: (context, state) {
          if (state is ConfirmPhoneSuccess) {
            if (state.confirmPhoneModel.status != null) {
              PopupHelper.showBasicSnack(
                  msg: state.confirmPhoneModel.status.toString().substring(6), color: Colors.red);
              ForgotPasswordCubit.get(context).changeCanContinue(value: false);
              ForgotPasswordCubit.get(context).controller.start();
              return;
            }
            if (state.confirmPhoneModel.phone != null || state.confirmPhoneModel.remaining != null) {
              state.confirmPhoneModel.remaining = null;
              MagicRouter.navigateTo(BlocProvider.value(
                value: ForgotPasswordCubit.get(context),
                child: const OTPScreen(),
              ));
              return;
            }
            if (state.confirmPhoneModel.error != null) {
              PopupHelper.showBasicSnack(msg: state.confirmPhoneModel.message.toString(), color: Colors.red);
            }
          } else if (state is ConfirmPhoneFailed) {
            PopupHelper.showBasicSnack(msg: "normalErrorMsg".tr(), color: Colors.red);
          }
        },
        builder: (context, state) {
          final cubit = ForgotPasswordCubit.get(context);
          return Scaffold(
            appBar: AppBar(
              elevation: 0.0,
              backgroundColor: Colors.transparent,
              foregroundColor: Colors.black,
            ),
            body: ListView(
              padding:
                  EdgeInsets.only(left: widget.width * 0.07, right: widget.width * 0.07, top: widget.height * 0.02),
              shrinkWrap: true,
              children: [
                Wrap(
                  children: [
                    Text('forgot.enter_num'.tr(),
                        style: Theme.of(context)
                            .textTheme
                            .titleLarge!
                            .copyWith(fontFamily: widget.isEn ? 'Montserrat' : 'Tajawal', fontWeight: FontWeight.bold))
                  ],
                ),
                const SizedBox(height: 16),
                Form(
                  key: cubit.forgotFormKey,
                  child: Directionality(
                    textDirection: TextDirection.ltr,
                    child: CustomIntlPhoneField(
                      hintText: 'phoneNumber'.tr(),
                      invalidNumberMessage: "validation.enter_valid_phone".tr(),
                      controller: cubit.phoneController,
                      onSaved: (phone) => cubit.countryCode = phone!.countryCode,
                      onSubmitted: (val) async {
                        cubit.forgotFormKey.currentState!.save();
                        if (cubit.forgotFormKey.currentState!.validate()) await cubit.confirmPhone();
                      },
                    ),
                  ),
                ),
                const SizedBox(height: 16),
                (state is ConfirmPhoneLoading)
                    ? const AppLoader()
                    : OTPContinueButton(
                        onPressed: cubit.canContinue
                            ? () async {
                                if (cubit.forgotFormKey.currentState!.validate()) await cubit.confirmPhone();
                              }
                            : null,
                      ),
                const SizedBox(height: 24),
                (cubit.confirmPhoneModel != null && cubit.confirmPhoneModel!.remaining != null)
                    ? CustomTimer(
                        controller: cubit.controller,
                        begin: Duration(seconds: cubit.confirmPhoneModel!.remaining!.toInt()),
                        end: const Duration(),
                        builder: (time) {
                          if (time.duration == const Duration()) {
                            if (doOnce) {
                              doOnce = false;
                              cubit.confirmPhoneModel!.remaining = null;
                              cubit.changeCanContinue(value: true);
                            }
                          }
                          return Column(
                            children: <Widget>[
                              Text("${time.minutes}${'m'.tr()} ${time.seconds}${'s'.tr()}",
                                  style: TextStyle(fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal')),
                              const SizedBox(height: 4),
                              Text('waitForTimer'.tr(),
                                  textAlign: TextAlign.center,
                                  style: TextStyle(fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal')),
                            ],
                          );
                        })
                    : const SizedBox()
              ],
            ),
          );
        },
      ),
    );
  }
}
