// ignore_for_file: depend_on_referenced_packages, unnecessary_import

import 'package:bloc/bloc.dart';
import 'package:custom_timer/custom_timer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';

import '../../../../data/models/forgot_password/reset_pass_model.dart';
import '../../../../data/models/forgot_password/verify_phone_model.dart';
import '../../../../data/repos/auth/auth_repo.dart';
import '../../../../data/models/forgot_password/confirm_phone_model.dart';

part 'forgot_password_state.dart';

class ForgotPasswordCubit extends Cubit<ForgotPasswordState> {
  ForgotPasswordCubit() : super(ForgotPasswordInitial());
  static ForgotPasswordCubit get(context) => BlocProvider.of(context);
  GlobalKey<FormState> forgotFormKey = GlobalKey<FormState>();
  GlobalKey<FormState> resetFormKey = GlobalKey<FormState>();
  TextEditingController phoneController = TextEditingController();
  String countryCode = '';
  TextEditingController passwordController = TextEditingController();
  ConfirmPhoneModel? confirmPhoneModel;
  VerifyPhoneModel? verifyPhoneModel;
  ResetPassModel? resetPassModel;
  CustomTimerController controller = CustomTimerController();
  bool canContinue = true;

  Future<void> confirmPhone() async {
    emit(ConfirmPhoneLoading());
    final res = await AuthRepo.confirmPhone(phone: countryCode + phoneController.text.trim().toString());
    if (res.data != null) {
      confirmPhoneModel = res.data;
      if (confirmPhoneModel.toString() == 'ThrottlerException: Too Many Requests') {
        emit(ConfirmPhoneFailed(error: confirmPhoneModel.toString()));
      }
      emit(ConfirmPhoneSuccess(confirmPhoneModel: confirmPhoneModel!));
    } else {
      emit(ConfirmPhoneFailed(error: res.message!));
    }
  }

  Future<void> verifyPhone({required String text}) async {
    emit(VerifyPhoneLoading());
    final res = await AuthRepo.verifyPhoneWithCodeAndNumber(
        text: text, phone: countryCode + phoneController.text.trim().toString());
    if (res.data != null) {
      verifyPhoneModel = res.data;
      emit(VerifyPhoneSuccess(verifyPhoneModel: verifyPhoneModel!));
    } else {
      emit(VerifyPhoneFailed(error: res.message!));
    }
  }

  Future<void> resetPass() async {
    emit(ResetPassLoading());
    final res = await AuthRepo.resetPass(
        password: passwordController.text.trim().toString(),
        phone: countryCode + phoneController.text.trim().toString());
    if (res.data != null) {
      resetPassModel = res.data;
      emit(ResetPassSuccess(resetPassModel: resetPassModel!));
    } else {
      emit(ResetPassFailed(error: res.message!));
    }
  }

  void changeCanContinue({required bool value}) {
    canContinue = value;
    emit(ChangeContinueState());
  }

  @override
  Future<void> close() {
    phoneController.dispose();
    passwordController.dispose();
    controller.dispose();
    return super.close();
  }
}
