part of 'forgot_password_cubit.dart';

@immutable
abstract class ForgotPasswordState {}

class ForgotPasswordInitial extends ForgotPasswordState {}

class ConfirmPhoneLoading extends ForgotPasswordState {}

class ConfirmPhoneSuccess extends ForgotPasswordState {
  final ConfirmPhoneModel confirmPhoneModel;

  ConfirmPhoneSuccess({required this.confirmPhoneModel});
}

class ConfirmPhoneFailed extends ForgotPasswordState {
  final String error;

  ConfirmPhoneFailed({required this.error});
}
//===============================================================

class VerifyPhoneLoading extends ForgotPasswordState {}

class VerifyPhoneSuccess extends ForgotPasswordState {
  final VerifyPhoneModel verifyPhoneModel;

  VerifyPhoneSuccess({required this.verifyPhoneModel});
}

class VerifyPhoneFailed extends ForgotPasswordState {
  final String error;

  VerifyPhoneFailed({required this.error});
}
//===============================================================

class ResetPassLoading extends ForgotPasswordState {}

class ResetPassSuccess extends ForgotPasswordState {
  final ResetPassModel resetPassModel;

  ResetPassSuccess({required this.resetPassModel});
}

class ResetPassFailed extends ForgotPasswordState {
  final String error;

  ResetPassFailed({required this.error});
}

class ChangeContinueState extends ForgotPasswordState {}

class ChangeDelayedContinueState extends ForgotPasswordState {}
