import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import '../../../utils/constants/app_const.dart';

class OTPContinueButton extends StatelessWidget {
  const OTPContinueButton({
    Key? key,
    this.onPressed,
  }) : super(key: key);

  final Function()? onPressed;
  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: onPressed,
      style: ButtonStyle(
        padding: MaterialStateProperty.resolveWith<EdgeInsetsGeometry>(
          (Set<MaterialState> states) {
            return EdgeInsets.symmetric(vertical: height * 0.016);
          },
        ),
      ),
      child: Text(
        'forgot.continue'.tr(),
        style: isEn
            ? Theme.of(context)
                .textTheme
                .titleMedium!
                .copyWith(fontWeight: FontWeight.w400, color: Colors.white, fontFamily: 'Montserrat')
            : Theme.of(context).textTheme.titleMedium!.copyWith(
                  fontSize: 17.0,
                  fontWeight: FontWeight.w400,
                  color: Colors.white,
                  fontFamily: 'Tajawal',
                ),
      ),
    );
  }
}
