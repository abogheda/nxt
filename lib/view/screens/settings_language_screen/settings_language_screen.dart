import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../config/router/router.dart';
import '../../../data/service/remote_service.dart';
import '../../../utils/constants/app_const.dart';
import '../home/cubit/home_cubit.dart';

import '../Language/widgets/rounded_corner_button.dart';
import '../navigation_and_appbar/import_widget.dart';
import '../otp/widgets/loader_hud.dart';

class SettingsLanguageScreen extends StatefulWidget {
  const SettingsLanguageScreen({Key? key}) : super(key: key);

  @override
  State<SettingsLanguageScreen> createState() => _SettingsLanguageScreenState();
}

class _SettingsLanguageScreenState extends State<SettingsLanguageScreen> {
  bool isEnSelected = AppConst.isEn;
  var currentLocal = MagicRouter.currentContext!.locale;
  Future<void> method() async {
    await MagicRouter.currentContext!.setLocale(currentLocal);
    APIService.dio!.options.headers['accept-language'] = currentLocal.languageCode;
    await HomeCubit.get(MagicRouter.currentContext!).editUserLanguage(language: currentLocal.languageCode);
    await HomeCubit.get(MagicRouter.currentContext!).getAllHomeData();
    await HomeCubit.get(MagicRouter.currentContext!).getHomeCarouselsData();
    await MagicRouter.navigateAndPopAll(Navigation());
  }

  @override
  void dispose() {
    method();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<HomeCubit, HomeState>(
      listener: (context, state) {},
      builder: (context, state) {
        return LoaderHUD(
          inAsyncCall: state is HomeLoading,
          child: CustomScaffoldWidget(
            extendBodyBehindAppBar: false,
            isBackArrow: true,
            isThereLogo: true,
            isThereActions: true,
            body: ListView(
              shrinkWrap: true,
              children: [
                Container(color: Colors.black, width: double.infinity, height: 1.5),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 24),
                  child: Text(
                    'chooseLang'.tr(),
                    textAlign: TextAlign.center,
                    style: Theme.of(context)
                        .textTheme
                        .headline3!
                        .copyWith(fontFamily: widget.isEn ? 'BebasNeue' : 'Tajawal', color: Colors.black),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: widget.width * 0.07, vertical: 12),
                  child: RoundedCornerButton(
                    isSelected: AppConst.isEn,
                    child: Text(
                      'English',
                      softWrap: true,
                      style: Theme.of(context).textTheme.titleMedium!.copyWith(
                          fontSize: 18.0,
                          fontWeight: FontWeight.w400,
                          color: AppConst.isEn ? Colors.white : Colors.black,
                          fontFamily: 'Montserrat'),
                    ),
                    onPressed: () async {
                      await context.setLocale(const Locale('en', 'US'));
                      setState(() {
                        currentLocal = const Locale('en', 'US');
                      });
                    },
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: widget.width * 0.07, vertical: 12),
                  child: RoundedCornerButton(
                    isSelected: !AppConst.isEn,
                    child: Text(
                      'العربية',
                      softWrap: true,
                      style: Theme.of(context).textTheme.titleMedium!.copyWith(
                            fontSize: 18.0,
                            fontWeight: FontWeight.w400,
                            fontFamily: 'Tajawal',
                            color: AppConst.isEn ? Colors.black : Colors.white,
                          ),
                    ),
                    onPressed: () async {
                      await context.setLocale(const Locale('ar', 'EG'));
                      setState(() {
                        currentLocal = const Locale('ar', 'EG');
                      });
                    },
                  ),
                ),
                SizedBox(height: widget.height * 0.4),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: widget.width * 0.05, vertical: 12),
                  child: RoundedCornerButton(
                    isSelected: true,
                    onPressed: () async {
                      await method();
                    },
                    child: Text(
                      'save'.tr(),
                      style: Theme.of(context).textTheme.titleMedium!.copyWith(
                            fontSize: 18.0,
                            fontWeight: FontWeight.w400,
                            color: Colors.white,
                            fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal',
                          ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
