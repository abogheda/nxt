// import 'package:flutter/material.dart';
// import 'package:flutter_svg/svg.dart';
// import 'package:nxt/utils/constants/app_colors.dart';
// import 'package:nxt/utils/constants/app_images.dart';
// import 'package:nxt/utils/constants/app_text_style.dart';
//
// import '../../../../../utils/constants/app_const.dart';
//
// class LiveStream extends StatelessWidget {
//   final String liveTitleText;
//   final String liveVideo;
//   final String liveStreamActorProfile;
//   final String liveStreamActorName;
//   const LiveStream(
//       {Key? key,
//       required this.liveTitleText,
//       required this.liveVideo,
//       required this.liveStreamActorProfile,
//       required this.liveStreamActorName})
//       : super(key: key);
//
//   @override
//   Widget build(BuildContext context) {
//     return Material(
//       elevation: 2,
//       color: Colors.white,
//       child: Container(
//         decoration: BoxDecoration(
//           border: Border.all(
//             color: AppColors.greyOutText,
//           ),
//         ),
//         child: Column(
//           children: [
//             const SizedBox(
//               height: 20,
//             ),
//             Row(
//               children: [
//                 Padding(
//                   padding: const EdgeInsets.fromLTRB(10, 0, 0, 0),
//                   child: Align(
//                     alignment: isEn ? Alignment.centerLeft : Alignment.centerRight,
//                     child: Text(
//                       liveTitleText,
//                       style: AppTextStyles.huge30,
//                     ),
//                   ),
//                 ),
//                 const SizedBox(
//                   width: 20,
//                 ),
//                 Container(
//                   width: 15,
//                   height: 15,
//                   decoration: const BoxDecoration(
//                     shape: BoxShape.circle,
//                     color: AppColors.redColor,
//                   ),
//                 ),
//               ],
//             ),
//             const SizedBox(
//               height: 10,
//             ),
//             Stack(
//               alignment: Alignment.center,
//               children: [
//                 Image.asset(AppImages.actorSayedClassesPng),
//                 SvgPicture.asset(
//                   AppImages.playIconSvg,
//                 ),
//               ],
//             ),
//             const SizedBox(
//               height: 10,
//             ),
//             Row(
//               //mainAxisAlignment: MainAxisAlignment.spaceBetween,
//               children: [
//                 Padding(
//                   padding: const EdgeInsets.fromLTRB(10, 0, 0, 0),
//                   child: Image.asset(liveStreamActorProfile, height: 45),
//                 ),
//                 const SizedBox(width: 8),
//                 Text(
//                   liveStreamActorName,
//                   style: const TextStyle(
//                     fontSize: 18,
//                     fontWeight: FontWeight.bold,
//                     fontFamily: 'BebasNeue',
//                   ),
//                 ),
//               ],
//             ),
//             const SizedBox(
//               height: 15,
//             )
//           ],
//         ),
//       ),
//     );
//   }
// }
