import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:transparent_image/transparent_image.dart';
import '../../../../../config/router/router.dart';
import '../../../../../utils/constants/resources.dart';
import '../../../../../../data/models/classes/master_class.dart';
import '../screens/actor_profile_screen.dart';

class ActorData extends StatelessWidget {
  final Author? author;
  final String date;
  const ActorData({Key? key, required this.date, required this.author}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: width * 0.03, vertical: 12.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          InkWell(
            onTap: () => MagicRouter.navigateTo(ActorProfileScreen(author: author)),
            borderRadius: BorderRadius.circular(8),
            child: Row(
              children: <Widget>[
                Container(
                  width: 30,
                  height: 30,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    border: Border.all(width: 1.0, color: Colors.black),
                    image: DecorationImage(
                      fit: BoxFit.cover,
                      image: FadeInImage.memoryNetwork(
                        image: author?.avatar == ''
                            ? 'https://static.thenounproject.com/png/504708-200.png'
                            : author?.avatar ?? 'https://static.thenounproject.com/png/504708-200.png',
                        placeholder: kTransparentImage,
                      ).image,
                    ),
                  ),
                ),
                const SizedBox(width: 8),
                Text(
                  author?.name ?? "notAvailable".tr(),
                  overflow: TextOverflow.ellipsis,
                  style: Theme.of(context)
                      .textTheme
                      .bodyText1!
                      .copyWith(fontFamily: AppConst.isEn ? 'BebasNeue' : 'Tajawal', fontSize: 18),
                ),
              ],
            ),
          ),
          Text(
            "${'uploadedOn'.tr()} $date",
            style: TextStyle(
                fontSize: 10,
                color: const Color(0xff707070),
                fontWeight: FontWeight.bold,
                fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal'),
          ),
        ],
      ),
    );
  }
}
