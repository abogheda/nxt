import 'package:easy_localization/easy_localization.dart' as local;
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:intl/intl.dart' as intl;
import '../../../../../utils/constants/resources.dart';
import '../../../../../data/models/classes/master_class.dart';
import 'actor_data_card.dart';
import 'poster_image.dart';
import 'hash_tag_container.dart';

class ClassCard extends StatefulWidget {
  final bool isOnTapRequired;
  final Function()? onTap;
  final String? title;
  final String? description;
  final String? poster;
  final String? updatedAt;
  final Author? author;
  final String? color;
  final List<Tags>? tags;
  const ClassCard({
    Key? key,
    required this.isOnTapRequired,
    required this.onTap,
    required this.title,
    required this.description,
    required this.poster,
    required this.updatedAt,
    required this.author,
    required this.color,
    required this.tags,
  }) : super(key: key);

  @override
  State<ClassCard> createState() => _ClassCardState();
}

class _ClassCardState extends State<ClassCard> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: widget.height * 0.009),
      child: InkWell(
        onTap: widget.isOnTapRequired ? widget.onTap : null,
        child: Material(
          color: Colors.white,
          elevation: 2,
          child: DecoratedBox(
            decoration: BoxDecoration(border: Border.all(color: AppColors.greyOutText)),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: widget.height * 0.017),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: widget.width * 0.03),
                  child: Text(
                    widget.title ?? "notAvailable".tr(),
                    style: Theme.of(context)
                        .textTheme
                        .headline5!
                        .copyWith(fontFamily: AppConst.isEn ? 'BebasNeue' : 'Tajawal', fontSize: 28),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: widget.width * 0.03),
                  child: Text(
                    widget.description ?? "notAvailable".tr(),
                    style: Theme.of(context)
                        .textTheme
                        .bodyText1!
                        .copyWith(fontFamily: widget.isEn ? 'Montserrat' : 'Tajawal'),
                  ),
                ),
                Padding(
                  padding: const EdgeInsetsDirectional.only(start: 7),
                  child: Wrap(
                      textDirection: TextDirection.ltr,
                      crossAxisAlignment: WrapCrossAlignment.start,
                      alignment: WrapAlignment.start,
                      children:
                          widget.tags!.map((tag) => HashTagContainer(tags: tag.text ?? "notAvailable".tr())).toList()),
                ),
                const SizedBox(height: 8),
                PosterImage(
                  posterUrl: widget.poster ?? placeHolderUrl,
                  showPlayIcon: true,
                  nxtColor: widget.color == null ? AppColors.redColor : Color(widget.color!.toHex()),
                  child: Positioned(
                    left: 0,
                    right: 0,
                    bottom: 0,
                    top: 0,
                    child: Align(
                      alignment: Alignment.center,
                      child: SvgPicture.asset(
                        'assets/images/play_icon.svg',
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
                ActorData(
                    author: widget.author,
                    date: intl.DateFormat("MMMM d, yyyy")
                        .format(DateTime.parse(widget.updatedAt ?? DateTime.now().toString()))),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
