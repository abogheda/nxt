import 'package:flutter/material.dart';
import '../../../../../utils/constants/resources.dart';

class HashTagContainer extends StatelessWidget {
  final String tags;
  const HashTagContainer({Key? key, required this.tags}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return tags.isNotEmpty
        ? Container(
            margin: const EdgeInsets.symmetric(horizontal: 3, vertical: 4),
            decoration: const BoxDecoration(
              color: Colors.black,
              borderRadius: BorderRadius.all(
                Radius.circular(4),
              ),
            ),
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 6, vertical: 6),
              child: Text(
                "#$tags",
                textAlign: TextAlign.center,
                style:
                    AppTextStyles.regular12.copyWith(color: Colors.white, fontFamily: isEn ? 'Montserrat' : 'Tajawal'),
              ),
            ),
          )
        : const SizedBox();
  }
}
