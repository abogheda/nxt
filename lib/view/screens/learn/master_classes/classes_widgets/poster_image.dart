import 'package:flutter/material.dart';
import 'package:transparent_image/transparent_image.dart';
import '../../../../../utils/constants/app_const.dart';

class PosterImage extends StatelessWidget {
  final String posterUrl;
  final Color nxtColor;
  final bool showPlayIcon;
  final Widget? child;
  const PosterImage({Key? key, required this.posterUrl, required this.nxtColor, this.child, this.showPlayIcon = true})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: Stack(
        alignment: Alignment.bottomRight,
        children: [
          Center(
            child: FadeInImage.memoryNetwork(
              image: posterUrl,
              height: height * 0.22,
              width: double.infinity,
              fit: BoxFit.cover,
              placeholder: kTransparentImage,
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Container(
                decoration: BoxDecoration(color: nxtColor, borderRadius: BorderRadius.circular(8)),
                height: 7,
                width: width * 0.22,
              ),
            ],
          ),
          showPlayIcon ? child! : const SizedBox()
        ],
      ),
    );
  }
}
