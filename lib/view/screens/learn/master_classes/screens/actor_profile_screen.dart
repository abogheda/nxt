import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:transparent_image/transparent_image.dart';
import '../../../../../utils/constants/resources.dart';
import '../../../navigation_and_appbar/import_widget.dart';
import '../../../../../data/models/classes/master_class.dart';

class ActorProfileScreen extends StatelessWidget {
  final Author? author;
  const ActorProfileScreen({Key? key, required this.author}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomScaffoldWidget(
      extendBodyBehindAppBar: false,
      isBackArrow: true,
      isThereLogo: true,
      isThereActions: true,
      body: ListView(
        children: [
          const Divider(color: Colors.black, thickness: 0.9),
          Container(
            height: width * 0.4,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              border: Border.all(width: 1.0, color: Colors.black),
              image: DecorationImage(
                fit: BoxFit.contain,
                alignment: Alignment.center,
                image: FadeInImage.memoryNetwork(
                  image: author!.avatar == '' ? userAvatarPlaceHolderUrl : author!.avatar ?? userAvatarPlaceHolderUrl,
                  placeholder: kTransparentImage,
                ).image,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 12.0),
            child: Text(
              author!.name ?? "notAvailable".tr(),
              textAlign: TextAlign.center,
              style: Theme.of(context)
                  .textTheme
                  .headline5!
                  .copyWith(fontFamily: AppConst.isEn ? 'BebasNeue' : 'Tajawal', fontSize: 44),
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: width * 0.03),
            child: Text(
                "${author!.nationality ?? "notAvailable".tr()}"
                " "
                "${author!.position ?? "notAvailable".tr()}",
                textAlign: TextAlign.center,
                style: Theme.of(context)
                    .textTheme
                    .bodyText2!
                    .copyWith(fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal')),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: width * 0.03),
            child: const Divider(color: Colors.black, thickness: 0.9),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: width * 0.03),
            child: Text(author!.description ?? "notAvailable".tr(),
                style: Theme.of(context).textTheme.bodyText1!.copyWith(
                    fontWeight: FontWeight.w400,
                    color: const Color(0xFFA5A5A5),
                    fontFamily: AppConst.isEn ? 'Inter-Medium' : 'Tajawal')),
          ),
          const SizedBox(height: 12)
        ],
      ),
    );
  }
}
