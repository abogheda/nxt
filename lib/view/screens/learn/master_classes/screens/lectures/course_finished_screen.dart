import 'package:flutter/material.dart';

import '../../../../navigation_and_appbar/import_widget.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:easy_localization/easy_localization.dart';
import '../../../../../../config/router/router.dart';
import '../../../../../../utils/constants/resources.dart';

class CourseFinishedScreen extends StatelessWidget {
  const CourseFinishedScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomScaffoldWidget(
      extendBodyBehindAppBar: false,
      isBackArrow: true,
      isThereLogo: true,
      isThereActions: true,
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: width * 0.1),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Center(
              child: AutoSizeText(
                'courseCompleted'.tr(),
                maxLines: 3,
                style: AppTextStyles.huge44,
                textAlign: TextAlign.center,
              ),
            ),
            SizedBox(height: height * 0.02),
            SizedBox(
              width: width,
              child: OutlinedButton(
                  onPressed: () async {
                    // await Utils.clearSecureFlag();
                    MagicRouter.navigateAndPopAll(Navigation(navigationIndex: 1, librarySelectedPage: 1));
                  },
                  child: Text('goToCourses'.tr(),
                      style: TextStyle(fontFamily: isEn ? 'Montserrat' : 'Tajawal', fontWeight: FontWeight.bold))),
            ),
          ],
        ),
      ),
    );
  }
}
