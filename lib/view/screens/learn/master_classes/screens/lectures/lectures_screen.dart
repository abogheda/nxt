// ignore_for_file: use_build_context_synchronously

import 'package:flick_video_player/flick_video_player.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import '../../../../../../config/router/router.dart';
import 'package:visibility_detector/visibility_detector.dart';
import 'package:video_player/video_player.dart';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../../../../data/models/lectures/lectures.dart';
import '../../../../../../utils/constants/app_const.dart';
import 'poster_aspect_ratio.dart';
import '../../../../navigation_and_appbar/import_widget.dart';
import 'package:intl/intl.dart' as intl;
import '../../../../../../data/models/classes/master_class.dart';
import '../../../../../../utils/constants/app_colors.dart';
import '../../../../../../utils/helpers/utils.dart';
import '../../../../../widgets/error_state_widget.dart';
import '../../../../../widgets/loading_state_widget.dart';
import '../../../../../widgets/no_state_widget.dart';
import '../../classes_widgets/actor_data_card.dart';
import '../../cubit/class_cubit.dart';
import 'controls.dart';
import 'course_finished_screen.dart';
import 'data_manager.dart';

class LecturesScreen extends StatefulWidget {
  final MasterClass classData;
  final List<Lectures> lecturesList;
  const LecturesScreen({
    Key? key,
    required this.classData,
    required this.lecturesList,
  }) : super(key: key);
  @override
  State<LecturesScreen> createState() => _LecturesScreenState();
}

class _LecturesScreenState extends State<LecturesScreen> {
  late FlickManager flickManager;
  late DataManager dataManager;
  // int currentLectureIndex = 0;
  bool showPlayer = false;
  List<String> lecturesUrlList = [];

  @override
  void initState() {
    for (var lecture in widget.lecturesList) {
      lecturesUrlList.add(lecture.media!);
    }
    if (lecturesUrlList.isNotEmpty) {
      initPlayer();
    }
    super.initState();
  }

  void initPlayer() {
    playVideo(init: true);
    setState(() {});
  }

  playVideo({int index = 0, bool init = false}) {
    if (index < 0 || index >= lecturesUrlList.length) return;
    if (!init) flickManager.flickVideoManager!.videoPlayerController!.pause();
    setState(() => ClassCubit.get(context).currentIndex = index);
    flickManager = FlickManager(
      videoPlayerController: VideoPlayerController.network(lecturesUrlList[ClassCubit.get(context).currentIndex]),
      autoInitialize: true,
      autoPlay: true,
    );
    dataManager = DataManager(flickManager: flickManager, urls: lecturesUrlList);
  }

  @override
  didChangeDependencies() {
    ClassCubit.get(context).currentLectureDuration = null;
    ClassCubit.get(context).currentLectureTitle = null;
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    if (widget.lecturesList.isNotEmpty) flickManager.dispose();
    showPlayer = false;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final cubit = context.read<ClassCubit>();
    if (widget.lecturesList.isNotEmpty) {
      flickManager.flickVideoManager!.videoPlayerController!.addListener(() async {
        flickManager.onVideoEnd = () async {
          if (!widget.lecturesList[ClassCubit.get(context).currentIndex].watched!) {
            await cubit.markLectureWatched(
                lectureId: widget.lecturesList[ClassCubit.get(context).currentIndex].id!,
                classId: widget.classData.id!);
            await cubit.getLecturesForClassById(classId: widget.classData.id!);
          }
          if (ClassCubit.get(context).currentIndex == lecturesUrlList.length - 1) {
            if (flickManager.flickControlManager!.isFullscreen) {
              flickManager.flickControlManager!.toggleFullscreen();
            }
            MagicRouter.navigateTo(const CourseFinishedScreen());
          } else {
            dataManager.skipToNextVideo();
            ClassCubit.get(context).currentIndex++;
            setState(() {});
          }
          setState(() {});
        };
      });
    }

    return CustomScaffoldWidget(
      extendBodyBehindAppBar: false,
      isBackArrow: true,
      isThereLogo: true,
      isThereActions: true,
      body: BlocBuilder<ClassCubit, ClassState>(
        builder: (context, state) {
          final cubit = ClassCubit.get(context);
          return ListView(
            children: [
              Padding(
                padding: EdgeInsets.symmetric(vertical: widget.height * 0.009),
                child: Material(
                  color: Colors.white,
                  elevation: 2,
                  child: DecoratedBox(
                    decoration: BoxDecoration(border: Border.all(color: AppColors.greyOutText)),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(height: widget.height * 0.017),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: widget.width * 0.03),
                          child: Text(
                            widget.classData.title ?? "notAvailable".tr(),
                            style: Theme.of(context)
                                .textTheme
                                .headline5!
                                .copyWith(fontFamily: AppConst.isEn ? 'BebasNeue' : 'Tajawal', fontSize: 28),
                          ),
                        ),
                        cubit.lecturesList!.isEmpty
                            ? PosterAspectRatio(
                                nxtColor: widget.classData.category!.color,
                                posterUrl: widget.classData.poster,
                                onPressed: () => setState(() => showPlayer = true),
                              )
                            : showPlayer
                                ? AspectRatio(
                                    aspectRatio: 16 / 9,
                                    child: VisibilityDetector(
                                      key: ObjectKey(flickManager),
                                      onVisibilityChanged: (visibility) {
                                        if (visibility.visibleFraction == 0 && mounted) {
                                          flickManager.flickControlManager?.autoPause();
                                        } else if (visibility.visibleFraction == 1) {
                                          flickManager.flickControlManager?.autoResume();
                                        }
                                      },
                                      child: FlickVideoPlayer(
                                        flickManager: flickManager,
                                        preferredDeviceOrientationFullscreen: const [DeviceOrientation.landscapeLeft],
                                        flickVideoWithControls: FlickVideoWithControls(
                                          closedCaptionTextStyle: const TextStyle(fontSize: 8),
                                          controls: CustomOrientationControls(dataManager: dataManager),
                                          aspectRatioWhenLoading: 16 / 9,
                                          videoFit: BoxFit.fitHeight,
                                        ),
                                        flickVideoWithControlsFullscreen: FlickVideoWithControls(
                                          controls: CustomOrientationControls(dataManager: dataManager),
                                          aspectRatioWhenLoading: 16 / 9,
                                          videoFit: BoxFit.fitHeight,
                                        ),
                                      ),
                                    ),
                                  )
                                : PosterAspectRatio(
                                    nxtColor: widget.classData.category!.color,
                                    posterUrl: widget.classData.poster,
                                    onPressed: () => setState(() {
                                      showPlayer = true;
                                      flickManager.flickVideoManager!.videoPlayerController!.play();
                                    }),
                                  ),
                        // !showPlayer
                        //     ?
                        ActorData(
                            author: widget.classData.author,
                            date: intl.DateFormat("MMMM d, yyyy")
                                .format(DateTime.parse(widget.classData.updatedAt ?? DateTime.now().toString())))
                        //     :
                        // Padding(
                        //         padding: EdgeInsets.symmetric(horizontal: widget.width * 0.03),
                        //         child: Row(
                        //           children: <Widget>[
                        //             Text(
                        //               // currentLectureIndex.toString(),
                        //               cubit.currentIndex > 10 ? "${cubit.currentIndex}" : "0${cubit.currentIndex + 1}",
                        //               style: Theme.of(context).textTheme.headline3!.copyWith(
                        //                     color: AppColors.iconsColor,
                        //                     fontFamily: AppConst.isEn ? 'BebasNeue' : 'Tajawal',
                        //                   ),
                        //             ),
                        //             SizedBox(width: widget.width * 0.025),
                        //             Text(
                        //               cubit.currentLectureTitle ?? '',
                        //               maxLines: 2,
                        //               style: Theme.of(context).textTheme.bodyText1!.copyWith(
                        //                     overflow: TextOverflow.ellipsis,
                        //                     fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal',
                        //                     fontWeight: FontWeight.bold,
                        //                   ),
                        //             ),
                        //             const Spacer(),
                        //             Text(
                        //               cubit.currentLectureDuration ?? '0',
                        //               style: Theme.of(context).textTheme.bodyText1!.copyWith(
                        //                     color: AppColors.iconsColor,
                        //                     fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal',
                        //                   ),
                        //             ),
                        //           ],
                        //         ),
                        //       ),
                      ],
                    ),
                  ),
                ),
              ),
              SizedBox(height: widget.height * 0.02),
              Padding(
                  padding: EdgeInsets.symmetric(horizontal: widget.width * 0.03),
                  child: Text('lectures'.tr().toUpperCase(),
                      style: TextStyle(fontSize: 16, fontFamily: AppConst.isEn ? 'Inter-Bold' : 'Tajawal'))),
              Padding(
                  padding: EdgeInsets.only(
                      right: widget.width * 0.03,
                      left: widget.width * 0.03,
                      top: widget.height * 0.01,
                      bottom: widget.height * 0.015),
                  child: Container(color: Colors.black, height: 1.5, width: widget.width)),
              BlocBuilder<ClassCubit, ClassState>(
                builder: (context, state) {
                  if (state is LecturesFailed) {
                    return ErrorStateWidget(
                        hasRefresh: true,
                        onRefresh: () async => await cubit.getLecturesForClassById(classId: widget.classData.id!));
                  }
                  if (state is LecturesLoading) return const LoadingStateWidget();
                  final List<Lectures> allLectures = cubit.lecturesList!;
                  if (allLectures.isEmpty) {
                    return NoStateWidget(
                      isDefaultText: false,
                      newText: "${'noStateText'.tr()} 👀",
                      newTextStyle: Theme.of(context)
                          .textTheme
                          .titleMedium!
                          .copyWith(fontFamily: AppConst.isEn ? 'BebasNeue' : 'Tajawal'),
                    );
                  } else {
                    return ListView.builder(
                      shrinkWrap: true,
                      physics: const NeverScrollableScrollPhysics(),
                      itemCount: allLectures.length,
                      itemBuilder: (context, index) {
                        final lecture = allLectures[index];
                        return Padding(
                          padding: EdgeInsets.symmetric(horizontal: widget.width * 0.03),
                          child: Row(
                            children: <Widget>[
                              Text(
                                allLectures.indexOf(lecture) > 10 ? "$index" : "0${index + 1}",
                                style: Theme.of(context).textTheme.headline2!.copyWith(
                                      color: AppColors.iconsColor,
                                      fontFamily: AppConst.isEn ? 'BebasNeue' : 'Tajawal',
                                    ),
                              ),
                              SizedBox(width: widget.width * 0.025),
                              Expanded(
                                child: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Flexible(
                                      child: Text(
                                        lecture.title ?? "notAvailable".tr(),
                                        maxLines: 2,
                                        style: Theme.of(context).textTheme.bodyText1!.copyWith(
                                              overflow: TextOverflow.ellipsis,
                                              fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal',
                                              fontWeight: FontWeight.bold,
                                            ),
                                      ),
                                    ),
                                    Row(
                                      children: [
                                        Icon(
                                          lecture.watched ?? false ? Icons.check_circle : Icons.not_interested_rounded,
                                          color: lecture.watched! ? Colors.black : Colors.transparent,
                                        ),
                                        SizedBox(width: widget.width * 0.015),
                                        Padding(
                                          padding: const EdgeInsets.symmetric(vertical: 8),
                                          child: Text(
                                            lecture.duration == null
                                                ? "notAvailable".tr()
                                                : Utils.parseDuration(durationInMinutes: lecture.duration!),
                                            style: Theme.of(context).textTheme.bodyText1!.copyWith(
                                                  fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal',
                                                  color: AppColors.iconsColor,
                                                ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(width: widget.width * 0.02),
                              ElevatedButton(
                                onPressed: () {
                                  showPlayer = true;
                                  playVideo(index: index);
                                  cubit.currentIndex = index;
                                  cubit.updateTheLectureDateHeader(
                                    lectureTitle: lecture.title ?? "notAvailable".tr(),
                                    lectureDuration: Utils.parseDuration(durationInMinutes: lecture.duration!),
                                  );
                                  setState(() {});
                                },
                                style: ElevatedButton.styleFrom(
                                    padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 4),
                                    minimumSize: Size.zero),
                                child: const Icon(Icons.play_arrow, color: Colors.white),
                              )
                            ],
                          ),
                        );
                      },
                    );
                  }
                },
              )
            ],
          );
        },
      ),
    );
  }
}
