import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:transparent_image/transparent_image.dart';
import '../../../../../../utils/constants/app_const.dart';

import '../../../../../../utils/constants/app_colors.dart';

class PosterAspectRatio extends StatelessWidget {
  final String? posterUrl;
  final String? nxtColor;
  final Function()? onPressed;
  const PosterAspectRatio({Key? key, required this.posterUrl, required this.nxtColor, this.onPressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AspectRatio(
      aspectRatio: 16 / 9,
      child: SizedBox(
        width: double.infinity,
        child: Stack(
          alignment: Alignment.bottomRight,
          children: [
            Center(
                child: FadeInImage.memoryNetwork(
                    image: posterUrl!, placeholder: kTransparentImage, fit: BoxFit.cover, height: height)),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Container(
                  height: 7,
                  width: width * 0.22,
                  decoration: BoxDecoration(
                      color: nxtColor == null ? AppColors.redColor : Color(nxtColor!.toHex()),
                      borderRadius: BorderRadius.circular(8)),
                ),
              ],
            ),
            Positioned(
              left: 0,
              right: 0,
              top: 0,
              bottom: 0,
              child: IconButton(
                  onPressed: onPressed, icon: SvgPicture.asset('assets/images/play_icon.svg', color: Colors.white)),
            ),
          ],
        ),
      ),
    );
  }
}
