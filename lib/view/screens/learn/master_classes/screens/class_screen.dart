import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../../../data/models/choose_bundle/plan_model.dart';
import '../../../../../data/service/hive_services.dart';
import '../../../../../utils/constants/app_const.dart';
import '../../../../../utils/helpers/utils.dart';
import '../../../choose_your_bundle/choose_your_bundle.dart';
import '../cubit/class_cubit.dart';
import '../../../navigation_and_appbar/import_widget.dart';
import 'package:easy_localization/easy_localization.dart';
import '../../../../widgets/save_button.dart';
import '../../../../../../utils/constants/app_colors.dart';
import '../../../../../config/router/router.dart';
import '../../../../widgets/error_state_widget.dart';
import '../../../../widgets/loading_state_widget.dart';
import '../classes_widgets/class_card.dart';
import 'lectures/lectures_screen.dart';

class ClassScreen extends StatefulWidget {
  const ClassScreen({Key? key, required this.classId}) : super(key: key);
  final String classId;
  @override
  State<ClassScreen> createState() => _ClassScreenState();
}

class _ClassScreenState extends State<ClassScreen> {
  late ClassCubit cubit;
  initMethod({required ClassCubit cubit}) async {
    // await Utils.addSecureFlag();
    await cubit.getClassById(classId: widget.classId);
    await cubit.getLecturesForClassById(classId: widget.classId);
  }

  @override
  void initState() {
    cubit = context.read<ClassCubit>();
    initMethod(cubit: cubit);
    super.initState();
  }

  @override
  void dispose() {
    // Utils.clearSecureFlag();
    cubit.masterClass = null;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return CustomScaffoldWidget(
      backgroundColor: Colors.white,
      extendBodyBehindAppBar: false,
      isBackArrow: true,
      isInProfile: false,
      isLogoClickable: true,
      body: BlocConsumer<ClassCubit, ClassState>(
        listener: (context, state) {},
        builder: (context, state) {
          final cubit = ClassCubit.get(context);
          if (state is GetMasterClassByIdLoading || cubit.masterClass == null) {
            return const LoadingStateWidget();
          } else if (state is GetMasterClassByIdFailed) {
            return ErrorStateWidget(hasRefresh: true, onRefresh: () async => await initMethod(cubit: cubit));
          }
          final masterClass = cubit.masterClass!;
          return Stack(
            children: [
              Positioned(
                top: 0,
                right: 0,
                left: 0,
                bottom: 0,
                child: RefreshIndicator(
                  onRefresh: () async => cubit.getClassById(classId: masterClass.id!),
                  child: ListView(
                    children: [
                      ClassCard(
                        isOnTapRequired: false,
                        onTap: null,
                        color: masterClass.category!.color,
                        title: masterClass.title,
                        author: masterClass.author,
                        poster: masterClass.poster,
                        description: masterClass.description,
                        tags: masterClass.tags,
                        updatedAt: masterClass.createdAt,
                      ),
                      ListView(
                        shrinkWrap: true,
                        physics: const NeverScrollableScrollPhysics(),
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 12),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  "${'details'.tr()}:",
                                  style: Theme.of(context)
                                      .textTheme
                                      .bodyText1!
                                      .copyWith(fontSize: 16, fontFamily: AppConst.isEn ? 'Inter-Bold' : 'Tajawal'),
                                ),
                                Row(
                                  children: [
                                    SaveButton(
                                      onPressed: () async {
                                        if (masterClass.userSaved == true) {
                                          await cubit.deleteFromSavedItems(item: masterClass.id!);
                                          setState(() {
                                            masterClass.userSaved = !masterClass.userSaved!;
                                          });
                                        } else {
                                          await cubit.addToSavedItems(item: masterClass.id!);
                                          setState(() {
                                            masterClass.userSaved = !masterClass.userSaved!;
                                          });
                                        }
                                      },
                                      isSaved: masterClass.userSaved ?? false,
                                      saveColor: '#000000',
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: widget.width * 0.03),
                            child: Container(color: Colors.black, height: 1.5, width: widget.width),
                          ),
                          const SizedBox(height: 8),
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: widget.width * 0.03),
                            child: Text(
                              masterClass.details ?? "notAvailable".tr(),
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyText1!
                                  .copyWith(fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal'),
                            ),
                          ),
                          const SizedBox(height: 10),
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: widget.width * 0.03),
                            child: Row(
                              children: [
                                const Icon(Icons.access_time_rounded, color: Colors.black),
                                SizedBox(width: widget.width * 0.01),
                                Text(
                                    masterClass.duration == null
                                        ? "notAvailable".tr()
                                        : Utils.parseDuration(durationInMinutes: masterClass.duration!),
                                    style: TextStyle(
                                      fontFamily: widget.isEn ? 'Montserrat' : 'Tajawal',
                                      fontWeight: FontWeight.bold,
                                    )),
                                SizedBox(width: widget.width * 0.04),
                                const Icon(Icons.language_rounded, color: Colors.black),
                                SizedBox(width: widget.width * 0.01),
                                Text(
                                  masterClass.language ?? "notAvailable".tr(),
                                  style: TextStyle(
                                    fontFamily: widget.isEn ? 'Montserrat' : 'Tajawal',
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(width: widget.width, height: widget.height * 0.08 + 12),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              Positioned(
                right: 0,
                left: 0,
                bottom: 0,
                child: Column(
                  children: <Widget>[
                    Container(
                        width: widget.width,
                        height: 8.0,
                        color: masterClass.category?.color == null
                            ? AppColors.redColor
                            : Color(masterClass.category!.color!.toHex())),
                    MaterialButton(
                      height: widget.height * 0.08,
                      minWidth: double.infinity,
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(0)),
                      color: Colors.black,
                      onPressed: masterClass.released ?? false
                          ? (() {
                            if(HiveHelper.getUserInfo?.user?.subscriptionEnd == false){
                              MagicRouter.navigateTo(
                                  BlocProvider.value(
                                    value: cubit..getLecturesForClassById(classId: masterClass.id!),
                                    child: LecturesScreen(classData: masterClass, lecturesList: cubit.lecturesList!),
                                  ));
                            }else{
                              var model =  PlanModel(id: "255",title: "test",features: ["dddd","sssss"],price: 20);
                              MagicRouter.navigateTo( const ChooseYourBundle(fromHome: true,));
                            }
                            })
                          : () {},
                      child: Text(
                        masterClass.released ?? false ? "play".tr().toUpperCase() : "comingSoon".tr().toUpperCase(),
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                          fontFamily: widget.isEn ? 'Montserrat' : 'Tajawal',
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          );
        },
      ),
    );
  }
}
