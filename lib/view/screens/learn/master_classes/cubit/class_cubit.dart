// ignore_for_file: unnecessary_import, depend_on_referenced_packages
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../../../data/enum/save.dart';
import '../../../../../data/enum/talent_type.dart';
import '../../../../../data/models/classes/master_class.dart';
import '../../../../../data/models/lectures/lectures.dart';
import '../../../../../data/models/save/save_model.dart';
import '../../../../../data/repos/classes/classes_repo.dart';
import '../../../../../data/repos/imports.dart';

part 'class_state.dart';

class ClassCubit extends Cubit<ClassState> {
  ClassCubit() : super(ClassCubitInitial());
  static ClassCubit get(context) => BlocProvider.of(context);
  List<MasterClass>? classesList;
  MasterClass? masterClass;
  List<Lectures>? lecturesList;
  late String firstLecture;
  String? currentLectureTitle;
  String? currentLectureDuration;
  int currentIndex = 0;
  void updateTheLectureDateHeader({required String lectureTitle, required String lectureDuration}) {
    currentLectureTitle = lectureTitle;
    currentLectureDuration = lectureDuration;
    emit(UpdateHeaderDataState());
  }

  Future<void> addToSavedItems({String? item}) async {
    emit(SaveClassLoading());
    final res = await SaveRepo.addToSavedItems(
      SaveModel(
        item: item,
        type: SavedItemsEnum.nxtclasses,
      ),
    );
    if (res.data != null) {
      emit(SaveClassSuccess());
    } else {
      emit(SaveClassFailed(msg: res.message ?? ""));
    }
  }

  Future<void> deleteFromSavedItems({String? item}) async {
    emit(SaveClassLoading());
    final res = await SaveRepo.deleteFromSavedItems(
      SaveModel(
        item: item,
        type: SavedItemsEnum.nxtclasses,
      ),
    );
    if (res.data != null) {
      emit(SaveClassSuccess());
      return;
    } else {
      emit(SaveClassFailed(msg: res.message ?? ""));
    }
  }

  Future<void> getAllClasses({TalentCategoryType? filter}) async {
    emit(MasterClassesLoading());
    final res = await ClassesRepo.getClasses(filter?.id);
    if (res.data != null) {
      classesList = res.data!;
      emit(MasterClassesSuccess());
    } else {
      emit(MasterClassesFailed(res.message ?? ""));
    }
  }

  Future<void> getClassById({required String classId}) async {
    emit(GetMasterClassByIdLoading());
    final res = await ClassesRepo.getClassById(id: classId);
    if (res.data != null) {
      masterClass = res.data!;
      emit(GetMasterClassByIdSuccess());
    } else {
      emit(GetMasterClassByIdFailed(res.message ?? ""));
    }
  }

  Future<void> getLecturesForClassById({required String classId}) async {
    emit(LecturesLoading());
    final res = await ClassesRepo.getLectureById(id: classId);
    if (res.data != null) {
      lecturesList = res.data!;
      emit(LecturesSuccess());
    } else {
      emit(LecturesFailed(res.message ?? ""));
    }
  }

  Future<void> markLectureWatched({required String lectureId, required String classId}) async {
    emit(MarkLectureWatchedLoading());
    final res = await ClassesRepo.markLectureWatched(lectureId: lectureId, classId: classId);
    if (res.data != null) {
      emit(MarkLectureWatchedSuccess());
    } else {
      emit(MarkLectureWatchedFailed(msg: res.message ?? ""));
    }
  }
}
