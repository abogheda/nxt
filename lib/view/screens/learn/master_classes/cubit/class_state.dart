// ignore_for_file: public_member_api_docs, sort_constructors_first
part of 'class_cubit.dart';

abstract class ClassState extends Equatable {
  const ClassState();

  @override
  List<Object> get props => [];
}

class ClassCubitInitial extends ClassState {}

class UpdateHeaderDataState extends ClassState {}

class MasterClassesSuccess extends ClassState {}

class MasterClassesLoading extends ClassState {}

class MasterClassesFailed extends ClassState {
  final String msg;
  const MasterClassesFailed(this.msg);
}

class GetMasterClassByIdSuccess extends ClassState {}

class GetMasterClassByIdLoading extends ClassState {}

class GetMasterClassByIdFailed extends ClassState {
  final String msg;
  const GetMasterClassByIdFailed(this.msg);
}

class LecturesSuccess extends ClassState {}

class LecturesLoading extends ClassState {}

class LecturesFailed extends ClassState {
  final String msg;
  const LecturesFailed(this.msg);
}

class SaveClassSuccess extends ClassState {}

class SaveClassLoading extends ClassState {}

class SaveClassFailed extends ClassState {
  final String msg;

  const SaveClassFailed({required this.msg});
}

class MarkLectureWatchedSuccess extends ClassState {}

class MarkLectureWatchedLoading extends ClassState {}

class MarkLectureWatchedFailed extends ClassState {
  final String msg;

  const MarkLectureWatchedFailed({required this.msg});
}
