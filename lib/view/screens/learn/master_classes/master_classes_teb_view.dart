// ignore_for_file: avoid_types_as_parameter_names, non_constant_identifier_names

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nxt/utils/constants/app_const.dart';
import 'package:nxt/view/screens/learn/master_classes/screens/class_screen.dart';
import 'package:nxt/view/widgets/error_state_widget.dart';
import 'package:nxt/view/widgets/no_state_widget.dart';
import 'package:nxt/view/screens/home/cubit/home_cubit.dart';
import '../../../../config/router/router.dart';
import '../../../widgets/shimmer_widgets/class_card_shimmer.dart';
import 'classes_widgets/class_card.dart';
import 'cubit/class_cubit.dart';

class MasterClassesTabView extends StatelessWidget {
  const MasterClassesTabView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<ClassCubit, ClassState>(
      listener: (context, state) {},
      builder: (_, state) {
        final cubit = ClassCubit.get(context);
        if (state is MasterClassesLoading) return const ClassCardShimmer();
        if (state is MasterClassesFailed) {
          return ErrorStateWidget(hasRefresh: true, onRefresh: () async => await cubit.getAllClasses());
        }
        final allClasses = cubit.classesList!;
        return RefreshIndicator(
          onRefresh: () async => cubit.getAllClasses(filter: context.read<HomeCubit>().categoryType),
          child: allClasses.isEmpty
              ? ListView(children: [SizedBox(height: height * 0.35), NoStateWidget()])
              : ListView.builder(
                  itemCount: allClasses.length,
                  itemBuilder: (_, index) {
                    final masterClass = allClasses[index];
                    return ClassCard(
                      isOnTapRequired: true,
                      color: masterClass.category!.color,
                      title: masterClass.title,
                      author: masterClass.author,
                      poster: masterClass.poster,
                      description: masterClass.description,
                      tags: masterClass.tags,
                      updatedAt: masterClass.createdAt,
                      onTap: () => MagicRouter.navigateTo(
                        BlocProvider.value(
                          value: cubit..getClassById(classId: masterClass.id!),
                          child: ClassScreen(classId: masterClass.id!),
                        ),
                      ),
                    );
                  },
                ),
        );
      },
    );
  }
}
