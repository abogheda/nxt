import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import '../../../utils/constants/app_const.dart';
import 'classes_coming_soon_classes/classes_coming_soon_screen.dart';
import 'reads/reads_tab_view.dart';

import '../../../utils/constants/app_colors.dart';

class LearnScreen extends StatefulWidget {
  const LearnScreen({Key? key, this.librarySelectedPage = 0}) : super(key: key);
  final int librarySelectedPage;

  @override
  State<LearnScreen> createState() => _LearnScreenState();
}

class _LearnScreenState extends State<LearnScreen> with SingleTickerProviderStateMixin {
  late TabController _tabController;
  List<Tab> tabTitleList = [
    Tab(child: Text('masterClasses'.tr())),
    Tab(child: Text('reads'.tr())),
  ];
  List<Widget> tabViewList = [
    // const MasterClassesTabView(),
    const ClassesComingSoonScreen(),
    const ReadsTabView(),
  ];

  @override
  void initState() {
    _tabController = TabController(length: tabTitleList.length, vsync: this, initialIndex: widget.librarySelectedPage);
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    _tabController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          height: widget.height * 0.06,
          color: Colors.black,
          child: TabBar(
            physics: const NeverScrollableScrollPhysics(),
            controller: _tabController,
            indicator: const BoxDecoration(color: Colors.black),
            labelColor: Colors.white,
            labelStyle:
                Theme.of(context).textTheme.headline6!.copyWith(fontFamily: AppConst.isEn ? 'BebasNeue' : 'Tajawal'),
            unselectedLabelColor: AppColors.greyOutText,
            tabs: tabTitleList,
          ),
        ),
        Expanded(
          child: TabBarView(
            physics: const NeverScrollableScrollPhysics(),
            controller: _tabController,
            children: tabViewList,
          ),
        )
      ],
    );
  }
}
