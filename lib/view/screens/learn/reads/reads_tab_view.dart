import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../../utils/constants/resources.dart';
import '../../../widgets/error_state_widget.dart';
import '../../../widgets/no_state_widget.dart';
import '../../../widgets/shimmer_widgets/reads_card_shimmer.dart';
import 'cubit/reads_cubit.dart';
import 'read_screen.dart';
import 'reads_card.dart';

import '../../../../config/router/router.dart';

class ReadsTabView extends StatelessWidget {
  const ReadsTabView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => ReadsCubit()..getAllReads(),
      child: BlocConsumer<ReadsCubit, ReadsState>(
        listener: (context, state) {},
        builder: (context, state) {
          final cubit = ReadsCubit.get(context);
          if (state is GetAllReadsLoading) return const ReadsCardShimmer();
          if (state is GetAllReadsFailed || ReadsCubit.get(context).readsList == null) {
            return ErrorStateWidget(hasRefresh: true, onRefresh: () async => await cubit.getAllReads());
          }
          final reads = cubit.readsList!;
          return RefreshIndicator(
            onRefresh: () async => cubit.getAllReads(),
            child: reads.isEmpty
                ? ListView(children: [SizedBox(height: height * 0.35), NoStateWidget()])
                : ListView.builder(
                    itemCount: reads.length,
                    itemBuilder: (_, index) {
                      return ReadsCard(
                        title: reads[index].title!,
                        description: reads[index].description!,
                        media: reads[index].media!,
                        tags: reads[index].tags!,
                        showCategoryColor: false,
                        titlePadding: EdgeInsetsDirectional.only(start: width * 0.01 + 3, end: width * 0.01 + 3),
                        tagsPadding: EdgeInsetsDirectional.only(start: width * 0.01, end: width * 0.01),
                        onTap: () => MagicRouter.navigateTo(ReadScreen(readId: reads[index].id!)),
                      );
                    },
                  ),
          );
        },
      ),
    );
  }
}
