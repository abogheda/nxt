import 'package:flutter/material.dart';
import '../../../../utils/constants/app_const.dart';

import '../../../../utils/constants/app_colors.dart';
import '../master_classes/classes_widgets/poster_image.dart';
import '../master_classes/classes_widgets/hash_tag_container.dart';

class ReadsCard extends StatelessWidget {
  const ReadsCard({
    Key? key,
    this.showDescription = true,
    this.showCategoryColor = true,
    this.onTap,
    required this.titlePadding,
    required this.tagsPadding,
    required this.title,
    required this.tags,
    required this.media,
    required this.description,
  }) : super(key: key);
  final bool showDescription;
  final bool showCategoryColor;
  final EdgeInsetsGeometry titlePadding;
  final EdgeInsetsGeometry tagsPadding;
  final Function()? onTap;
  final String title;
  final List<String?> tags;
  final String media;
  final String description;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: height * 0.012),
      child: InkWell(
        onTap: onTap,
        child: Material(
          color: Colors.white,
          elevation: 2,
          child: Container(
            decoration: BoxDecoration(border: Border.all(color: AppColors.greyOutText)),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: height * 0.017),
                Padding(
                  padding: titlePadding,
                  child: Text(
                    title,
                    style: Theme.of(context)
                        .textTheme
                        .headline5!
                        .copyWith(fontFamily: AppConst.isEn ? 'BebasNeue' : 'Tajawal', fontSize: 28),
                  ),
                ),
                Padding(
                  padding: tagsPadding,
                  child: Wrap(
                    textDirection: TextDirection.ltr,
                    crossAxisAlignment: WrapCrossAlignment.start,
                    alignment: WrapAlignment.start,
                    children: tags.map((tag) => HashTagContainer(tags: tag.toString())).toList(),
                  ),
                ),
                const SizedBox(height: 7),
                PosterImage(
                  posterUrl: media,
                  nxtColor: showCategoryColor ? AppColors.redColor : Colors.transparent,
                  showPlayIcon: false,
                ),
                showDescription
                    ? Padding(
                        padding: const EdgeInsets.symmetric(vertical: 4, horizontal: 6),
                        child: Text(
                          description,
                          style: Theme.of(context).textTheme.caption!.copyWith(
                                fontFamily: isEn ? 'Montserrat' : 'Tajawal',
                              ),
                        ),
                      )
                    : const SizedBox(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
