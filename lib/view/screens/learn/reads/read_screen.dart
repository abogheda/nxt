import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../../utils/constants/resources.dart';
import '../../../widgets/custom_html_widget.dart';
import 'cubit/reads_cubit.dart';
import 'reads_card.dart';

import '../../../widgets/error_state_widget.dart';
import '../../../widgets/loading_state_widget.dart';
import '../../../widgets/save_button.dart';
import '../../navigation_and_appbar/import_widget.dart';

class ReadScreen extends StatefulWidget {
  const ReadScreen({Key? key, required this.readId}) : super(key: key);
  final String readId;
  @override
  State<ReadScreen> createState() => _ReadScreenState();
}

class _ReadScreenState extends State<ReadScreen> {
  @override
  Widget build(BuildContext context) {
    return CustomScaffoldWidget(
      extendBodyBehindAppBar: false,
      isBackArrow: true,
      isThereLogo: true,
      isThereActions: true,
      body: BlocProvider(
        create: (context) => ReadsCubit()..getRead(readId: widget.readId),
        child: BlocConsumer<ReadsCubit, ReadsState>(
          listener: (context, state) {},
          builder: (context, state) {
            final cubit = ReadsCubit.get(context);
            if (state is GetReadLoading) {
              return const LoadingStateWidget();
            }
            if (state is GetReadFailed || ReadsCubit.get(context).readModel == null) {
              return ErrorStateWidget(
                hasRefresh: true,
                onRefresh: () async {
                  await cubit.getRead(readId: widget.readId);
                },
              );
            }
            final readModel = cubit.readModel!;
            return RefreshIndicator(
              onRefresh: () async => cubit.getRead(readId: readModel.id!),
              child: ListView(
                shrinkWrap: true,
                children: [
                  ReadsCard(
                    title: readModel.title ?? "notAvailable".tr(),
                    description: readModel.description ?? "notAvailable".tr(),
                    media: readModel.media ?? placeHolderUrl,
                    tags: readModel.tags ?? [],
                    showDescription: false,
                    showCategoryColor: false,
                    titlePadding:
                        EdgeInsetsDirectional.only(start: widget.width * 0.025 + 3, end: widget.width * 0.025 + 3),
                    tagsPadding: EdgeInsetsDirectional.only(start: widget.width * 0.025, end: widget.width * 0.025),
                  ),
                  const SizedBox(height: 12),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: widget.width * 0.025),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text("${'details'.tr()}:",
                            style: TextStyle(
                                fontSize: 15,
                                fontFamily: widget.isEn ? 'Montserrat' : 'Tajawal',
                                fontWeight: FontWeight.bold)),
                        Row(
                          children: [
                            SaveButton(
                              onPressed: () async {
                                if (readModel.userSaved == true) {
                                  await cubit.deleteReadFromSavedItems(readId: readModel.id!);
                                  setState(() {
                                    readModel.userSaved = !readModel.userSaved!;
                                  });
                                } else {
                                  await cubit.addReadToSavedItems(readId: readModel.id!);
                                  setState(() {
                                    readModel.userSaved = !readModel.userSaved!;
                                  });
                                }
                              },
                              isSaved: readModel.userSaved ?? false,
                              saveColor: '#000000',
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: widget.width * 0.025),
                    child: const Divider(color: Colors.black, thickness: 1.5),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: widget.width * 0.025),
                    child: Text(readModel.details ?? '',
                        style: Theme.of(context)
                            .textTheme
                            .bodyText1!
                            .copyWith(fontFamily: widget.isEn ? 'Montserrat' : 'Tajawal')),
                  ),
                  CustomHtmlWidget(data: readModel.content)
                ],
              ),
            );
          },
        ),
      ),
    );
  }
}
