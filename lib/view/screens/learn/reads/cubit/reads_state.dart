part of 'reads_cubit.dart';

@immutable
abstract class ReadsState {}

class ReadsInitial extends ReadsState {}

class GetAllReadsSuccess extends ReadsState {}

class GetAllReadsLoading extends ReadsState {}

class GetAllReadsFailed extends ReadsState {
  final String msg;
  GetAllReadsFailed(this.msg);
}

class GetReadSuccess extends ReadsState {}

class GetReadLoading extends ReadsState {}

class GetReadFailed extends ReadsState {
  final String msg;
  GetReadFailed(this.msg);
}

class SaveReadSuccess extends ReadsState {}

class SaveReadLoading extends ReadsState {}

class SaveReadFailed extends ReadsState {
  final String msg;
  SaveReadFailed({required this.msg});
}
