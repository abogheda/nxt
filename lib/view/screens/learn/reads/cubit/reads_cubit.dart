// ignore_for_file: unnecessary_import, depend_on_referenced_packages

import 'package:bloc/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';
import 'package:nxt/data/repos/reads/reads_repo.dart';

import '../../../../../data/enum/save.dart';
import '../../../../../data/models/reads/read_model.dart';
import '../../../../../data/models/reads/reads_model.dart';
import '../../../../../data/models/save/save_model.dart';
import '../../../../../data/repos/imports.dart';

part 'reads_state.dart';

class ReadsCubit extends Cubit<ReadsState> {
  ReadsCubit() : super(ReadsInitial());
  static ReadsCubit get(context) => BlocProvider.of(context);

  List<ReadsModel>? readsList;
  ReadModel? readModel;

  Future<void> getAllReads() async {
    emit(GetAllReadsLoading());
    final res = await ReadsRepo.getAllReads();
    if (res.data != null) {
      readsList = res.data!;
      emit(GetAllReadsSuccess());
    } else {
      emit(GetAllReadsFailed(res.message ?? ""));
    }
  }

  Future<void> getRead({required String readId}) async {
    emit(GetReadLoading());
    final res = await ReadsRepo.getRead(reaId: readId);
    if (res.data != null) {
      readModel = res.data!;
      emit(GetReadSuccess());
    } else {
      emit(GetReadFailed(res.message ?? ""));
    }
  }

  Future<void> addReadToSavedItems({String? readId}) async {
    emit(SaveReadLoading());
    final res = await SaveRepo.addToSavedItems(
      SaveModel(
        item: readId,
        type: SavedItemsEnum.reads,
      ),
    );
    if (res.data != null) {
      emit(SaveReadSuccess());
    } else {
      emit(SaveReadFailed(msg: res.message ?? ""));
    }
  }

  Future<void> deleteReadFromSavedItems({String? readId}) async {
    emit(SaveReadLoading());
    final res = await SaveRepo.deleteFromSavedItems(
      SaveModel(
        item: readId,
        type: SavedItemsEnum.reads,
      ),
    );
    if (res.data != null) {
      emit(SaveReadSuccess());
      return;
    } else {
      emit(SaveReadFailed(msg: res.message ?? ""));
    }
  }
}
