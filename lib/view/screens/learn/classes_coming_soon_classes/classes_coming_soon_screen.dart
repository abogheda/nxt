import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

import '../../../../utils/constants/app_const.dart';

class ClassesComingSoonScreen extends StatelessWidget {
  const ClassesComingSoonScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.black.withOpacity(0.5),
      padding: EdgeInsets.symmetric(horizontal: width * 0.03),
      child: ListView(
        children: [
          SizedBox(height: height * 0.05),
          Center(
              child: Stack(
            children: [
              Column(
                children: [
                  SizedBox(height: height * 0.01),
                  Image.asset('assets/images/classes_coming_soon.png'),
                ],
              ),
              Column(
                children: [
                  Opacity(
                    opacity: 0,
                    child: Image.asset('assets/images/classes_coming_soon.png'),
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: width * 0.02),
                    decoration: const BoxDecoration(
                        color: Colors.black,
                        borderRadius:
                            BorderRadius.only(bottomRight: Radius.circular(24), bottomLeft: Radius.circular(24))),
                    child: Column(
                      children: <Widget>[
                        Text(
                          'learnFromTheBest'.tr(),
                          style: Theme.of(context).textTheme.headlineSmall!.copyWith(
                                color: Colors.white,
                                fontFamily: isEn ? 'BebasNeue' : 'Tajawal',
                              ),
                        ),
                        SizedBox(height: height * 0.01),
                        Text(
                          'getReady'.tr(),
                          textAlign: TextAlign.center,
                          style: Theme.of(context).textTheme.titleSmall!.copyWith(
                                color: Colors.white,
                                fontFamily: isEn ? 'Montserrat' : 'Tajawal',
                              ),
                        ),
                        SizedBox(height: height * 0.01),
                        Text(
                          'offers'.tr(),
                          textAlign: TextAlign.center,
                          style: Theme.of(context).textTheme.titleSmall!.copyWith(
                                color: Colors.white,
                                fontFamily: isEn ? 'Montserrat' : 'Tajawal',
                              ),
                        ),
                        SizedBox(height: height * 0.01),
                        Text(
                          'Filmmaking/Music/Acting'.tr(),
                          textAlign: TextAlign.center,
                          style: Theme.of(context).textTheme.titleMedium!.copyWith(
                                color: Colors.white,
                                fontFamily: isEn ? 'Montserrat' : 'Tajawal',
                                fontWeight: FontWeight.bold,
                              ),
                        ),
                        SizedBox(height: height * 0.06),
                      ],
                    ),
                  ),
                ],
              )
            ],
          )),
          SizedBox(height: height * 0.05),
        ],
      ),
    );
  }
}
