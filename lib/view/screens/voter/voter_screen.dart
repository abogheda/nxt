import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import '../../../utils/constants/resources.dart';
import '../../../utils/helpers/utils.dart';
import '../../widgets/logo_app_bar.dart';
import '../../../utils/constants/app_const.dart';

class VoterScreen extends StatelessWidget {
  const VoterScreen({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          const LogoAppBar(),
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 36, horizontal: width * 0.03),
                  child: Text(
                    'voter'.tr(),
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.headline4!.copyWith(
                          fontFamily: isEn ? 'BebasNeue' : 'Tajawal',
                          color: Colors.black,
                        ),
                  ),
                ),
                const SizedBox(height: 16.0),
                SizedBox(
                  width: width * 0.6,
                  height: 48,
                  child: ElevatedButton(
                    child: Text(
                      'goToWebsite'.tr(),
                      style: Theme.of(context).textTheme.titleMedium!.copyWith(
                            fontFamily: isEn ? 'Montserrat' : 'Tajawal',
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                    ),
                    onPressed: () async => await Utils.launchAppUrl(
                        url: AppConst.isProduction
                            ? "${AppConst.productionUrl}/signup/"
                            : "${AppConst.developmentUrl}/signup/"),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
