import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import '../../../../utils/constants/app_const.dart';
import '../../../../config/router/router.dart';
import 'arabic_tab_bar_view.dart';
import 'english_tab_bar_view.dart';

class LanguageTabBarView extends StatefulWidget {
  const LanguageTabBarView({Key? key}) : super(key: key);

  @override
  State<LanguageTabBarView> createState() => _LanguageTabBarViewState();
}

class _LanguageTabBarViewState extends State<LanguageTabBarView> with SingleTickerProviderStateMixin {
  late TabController _tabController;
  List<Tab> tabTitleList = [Tab(child: Text('english'.tr())), Tab(child: Text('arabic'.tr()))];
  List<Widget> tabViewList = [const EnglishTabBarView(), const ArabicTabBarView()];

  @override
  void initState() {
    _tabController = TabController(length: tabTitleList.length, vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    _tabController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(height: widget.height * 0.01),
        SizedBox(
          height: widget.height * 0.06,
          child: TabBar(
            controller: _tabController,
            indicatorColor: Colors.black,
            labelColor: Colors.black,
            labelStyle: Theme.of(MagicRouter.currentContext!)
                .textTheme
                .headline6!
                .copyWith(fontFamily: AppConst.isEn ? 'BebasNeue' : 'Tajawal'),
            unselectedLabelColor: const Color(0xFF989898),
            tabs: tabTitleList,
          ),
        ),
        Expanded(
          child: TabBarView(
            controller: _tabController,
            children: tabViewList,
          ),
        )
      ],
    );
  }
}
