import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../utils/constants/resources.dart';
import 'controller/organization_connect_cubit.dart';
import 'organization_all_jobs_screen.dart';
import 'organization_my_jobs_screen.dart';

class OrganizationConnectScreen extends StatefulWidget {
  const OrganizationConnectScreen({
    Key? key,
  }) : super(key: key);

  @override
  State<OrganizationConnectScreen> createState() => _OrganizationConnectScreenState();
}

class _OrganizationConnectScreenState extends State<OrganizationConnectScreen> with SingleTickerProviderStateMixin {
  late TabController _tabController;

  List<Tab> tabTitleList = [
    Tab(child: Text('allJobs'.tr().toUpperCase())),
    Tab(child: Text('myJobs'.tr().toUpperCase())),
  ];
  List<Widget> tabViewList = [
    const OrganizationAllJobsScreen(),
    const OrganizationMyJobsScreen(),
  ];
  @override
  void initState() {
    _tabController = TabController(length: tabTitleList.length, vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          height: widget.height * 0.06,
          color: Colors.black,
          child: TabBar(
            controller: _tabController,
            indicator: const BoxDecoration(color: Colors.black),
            labelColor: Colors.white,
            labelStyle:
                Theme.of(context).textTheme.headline6!.copyWith(fontFamily: AppConst.isEn ? 'BebasNeue' : 'Tajawal'),
            unselectedLabelColor: AppColors.greyOutText,
            tabs: tabTitleList,
          ),
        ),
        Expanded(
          child: BlocProvider(
              create: (context) => OrganizationConnectCubit()
                ..getAllJobs()
                ..getAllJobsForOwner(),
              child: TabBarView(controller: _tabController, children: tabViewList)),
        )
      ],
    );
  }
}
