// ignore_for_file: depend_on_referenced_packages, unnecessary_import

import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';
import '../../../../data/models/jobs/applicants_model.dart';
import '../../../../data/models/jobs/jobs_model.dart';
import '../../../../data/models/jobs/update_applicant_model.dart';
import '../../../../data/repos/imports.dart';

part 'organization_connect_state.dart';

class OrganizationConnectCubit extends Cubit<OrganizationConnectState> {
  OrganizationConnectCubit() : super(OrganizationConnectInitial());
  static OrganizationConnectCubit get(context) => BlocProvider.of(context);
  List<JobsModel>? allJobsList, allJobsForOrganizationList;
  List<ApplicantsModel>? applicantsModel;
  UpdateApplicantModel? updateApplicantModel;
  int selectedButtonIndex = 0;
  // int? index;
  Future<void> getAllJobs() async {
    emit(GetAllJobsLoading());
    final res = await CommunityRepo.getAllJobs();
    if (res.data != null) {
      allJobsList = res.data;
      emit(GetAllJobsSuccess());
    } else {
      emit(GetAllJobsFailed(res.message ?? ""));
    }
  }

  Future<void> getAllJobsForOwner() async {
    emit(GetAllJobsForOwnerLoading());
    final res = await CommunityRepo.getAllJobsForOwner();
    if (res.data != null) {
      allJobsForOrganizationList = res.data;
      emit(GetAllJobsForOwnerSuccess());
    } else {
      emit(GetAllJobsForOwnerFailed(res.message ?? ""));
    }
  }

  Future<void> getApplicants({required String jobId, String? status}) async {
    emit(GetAllApplicantsLoading());
    final res = await CommunityRepo.getApplicants(jobId: jobId, status: status);
    if (res.data != null) {
      applicantsModel = res.data;
      emit(GetAllApplicantsSuccess());
    } else {
      emit(GetAllApplicantsFailed(res.message ?? ""));
    }
  }

  Future<void> updateJobApplicationStatus({required String applicantId, required String status}) async {
    emit(UpdateJobApplicationLoading());
    final res = await CommunityRepo.updateJobApplicationStatus(status: status, applicantId: applicantId);
    if (res.data != null) {
      updateApplicantModel = res.data;
      emit(UpdateJobApplicationSuccess());
    } else {
      emit(UpdateJobApplicationFailed(res.message ?? ""));
    }
  }

  void changeButtonIndex({required int index}) {
    selectedButtonIndex = index;
    emit(ChangeButtonIndexState());
  }

  // void changeIndex({required int newIndex}) {
  //   index = newIndex;
  //   emit(ChangeIndexState());
  // }
}
