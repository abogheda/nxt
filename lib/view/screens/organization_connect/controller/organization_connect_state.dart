part of 'organization_connect_cubit.dart';

@immutable
abstract class OrganizationConnectState {}

class OrganizationConnectInitial extends OrganizationConnectState {}

class ChangeButtonIndexState extends OrganizationConnectState {}

class ChangeIndexState extends OrganizationConnectState {}

class GetAllJobsSuccess extends OrganizationConnectState {}

class GetAllJobsLoading extends OrganizationConnectState {}

class GetAllJobsFailed extends OrganizationConnectState {
  final String msg;
  GetAllJobsFailed(this.msg);
}

class GetAllJobsForOwnerSuccess extends OrganizationConnectState {}

class GetAllJobsForOwnerLoading extends OrganizationConnectState {}

class GetAllJobsForOwnerFailed extends OrganizationConnectState {
  final String msg;
  GetAllJobsForOwnerFailed(this.msg);
}

class GetAllApplicantsSuccess extends OrganizationConnectState {}

class GetAllApplicantsLoading extends OrganizationConnectState {}

class GetAllApplicantsFailed extends OrganizationConnectState {
  final String msg;
  GetAllApplicantsFailed(this.msg);
}

class UpdateJobApplicationSuccess extends OrganizationConnectState {}

class UpdateJobApplicationLoading extends OrganizationConnectState {}

class UpdateJobApplicationFailed extends OrganizationConnectState {
  final String msg;
  UpdateJobApplicationFailed(this.msg);
}
