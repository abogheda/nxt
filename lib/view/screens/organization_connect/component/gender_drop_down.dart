import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import '../../../../utils/constants/resources.dart';

import '../../../widgets/custom_drop_down_field.dart';
import '../../create_job/controller/create_job_cubit.dart';

class GenderDropDown extends StatelessWidget {
  const GenderDropDown({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final cubit = CreateJobCubit.get(context);
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: width * 0.03),
      child: CustomDropDownTextField<String>(
        value: cubit.gender,
        hintTextStyle: Theme.of(context)
            .textTheme
            .bodyText1!
            .copyWith(fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal', color: const Color(0xFFA5A5A5)),
        items: [
          DropdownMenuItem(
            value: 'Male',
            child: Text(
              'Male'.tr(),
              style: TextStyle(fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal'),
            ),
          ),
          DropdownMenuItem(
            value: 'Female',
            child: Text(
              'Female'.tr(),
              style: TextStyle(fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal'),
            ),
          ),
          DropdownMenuItem(
            value: 'general',
            child: Text(
              'general'.tr(),
              style: TextStyle(fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal'),
            ),
          )
        ],
        hint: 'chooseGender'.tr(),
        prefix: const Icon(Icons.person_outline_rounded),
        onChanged: (val) => cubit.gender = val,
      ),
    );
  }
}
