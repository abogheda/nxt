import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import '../../../../data/enum/talent_type.dart';
import '../../../../utils/constants/resources.dart';

import '../../../widgets/custom_drop_down_field.dart';
import '../../create_job/controller/create_job_cubit.dart';

class VerticalDropDown extends StatefulWidget {
  const VerticalDropDown({Key? key}) : super(key: key);

  @override
  State<VerticalDropDown> createState() => _VerticalDropDownState();
}

class _VerticalDropDownState extends State<VerticalDropDown> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: widget.width * 0.03),
      child: BlocBuilder<CreateJobCubit, CreateJobState>(
        builder: (context, state) {
          final cubit = CreateJobCubit.get(context);
          return CustomDropDownTextField<String>(
            value: cubit.verticalId,
            hint: 'chooseVertical'.tr(),
            hintTextStyle: Theme.of(context)
                .textTheme
                .bodyText1!
                .copyWith(fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal', color: const Color(0xFFA5A5A5)),
            items: [
              DropdownMenuItem(
                value: TalentCategoryType.music.id,
                child: Text(
                  'music'.tr(),
                  style: Theme.of(context).textTheme.titleMedium!.copyWith(
                        fontFamily: AppConst.isEn ? 'BebasNeue' : 'Tajawal',
                        color: AppColors.blueColor,
                      ),
                ),
              ),
              DropdownMenuItem(
                value: TalentCategoryType.filmMaking.id,
                child: Text(
                  'filmMaking'.tr(),
                  style: Theme.of(context).textTheme.titleMedium!.copyWith(
                        fontFamily: AppConst.isEn ? 'BebasNeue' : 'Tajawal',
                        color: AppColors.redColor,
                      ),
                ),
              ),
              DropdownMenuItem(
                value: TalentCategoryType.acting.id,
                child: Text(
                  'acting'.tr(),
                  style: Theme.of(context).textTheme.titleMedium!.copyWith(
                        fontFamily: AppConst.isEn ? 'BebasNeue' : 'Tajawal',
                        color: AppColors.yellowColor,
                      ),
                ),
              )
            ],
            prefix: Padding(
              padding: const EdgeInsets.all(8.0),
              child: SvgPicture.asset('assets/images/dropdown_logo_icon.svg'),
            ),
            onChanged: (val) {
              setState(() {
                cubit.verticalId = val;
              });
            },
          );
        },
      ),
    );
  }
}
