import 'package:flutter/material.dart';
import '../../../../utils/constants/resources.dart';

class CreateJobHeaderText extends StatelessWidget {
  const CreateJobHeaderText({Key? key, required this.text, this.horizontalPadding}) : super(key: key);
  final String text;
  final double? horizontalPadding;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: horizontalPadding ?? width * 0.03, vertical: height * 0.005),
      child: Text(
        text,
        textAlign: TextAlign.start,
        style: Theme.of(context).textTheme.titleLarge!.copyWith(
              fontFamily: isEn ? 'BebasNeue' : 'Tajawal',
              fontWeight: isEn ? FontWeight.normal : FontWeight.bold,
              overflow: TextOverflow.ellipsis,
            ),
      ),
    );
  }
}
