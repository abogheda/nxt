import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import '../../../../utils/constants/app_const.dart';
import '../../create_job/controller/create_job_cubit.dart';
import '../../../widgets/custom_text_field.dart';

class AgeTextFieldsRow extends StatefulWidget {
  const AgeTextFieldsRow({Key? key}) : super(key: key);

  @override
  State<AgeTextFieldsRow> createState() => _AgeTextFieldsRowState();
}

class _AgeTextFieldsRowState extends State<AgeTextFieldsRow> {
  @override
  Widget build(BuildContext context) {
    final cubit = CreateJobCubit.get(context);
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: widget.width * 0.03),
      child: Form(
        key: cubit.ageFormKey,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              child: StatefulBuilder(builder: (_, stState) {
                return CustomTextField(
                  key: UniqueKey(),
                  hint: 'ageFrom'.tr(),
                  controller: cubit.ageFromController,
                  type: TextInputType.number,
                  prefix: const Icon(Icons.calendar_today_outlined),
                  onFieldSubmitted: (_) {
                    cubit.ageFormKey.currentState!.save();
                    FocusScope.of(context).requestFocus(cubit.ageToFocusNode);
                  },
                  onChange: (_) => cubit.ageFormKey.currentState!.save(),
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'empty_field'.tr();
                    } else if (int.parse(value) < 13 || int.parse(value) > 90) {
                      return 'minAge'.tr();
                    } else {
                      return null;
                    }
                  },
                );
              }),
            ),
            const SizedBox(width: 8),
            Expanded(
              child: StatefulBuilder(builder: (_, stState) {
                return CustomTextField(
                  key: UniqueKey(),
                  hint: 'ageTo'.tr(),
                  onFieldSubmitted: (_) {
                    cubit.ageFormKey.currentState!.save();
                  },
                  onChange: (_) => cubit.ageFormKey.currentState!.save(),
                  focusNode: cubit.ageToFocusNode,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'empty_field'.tr();
                    } else if (int.parse(value) < 13 || int.parse(value) > 90) {
                      return 'maxAge'.tr();
                    } else {
                      return null;
                    }
                  },
                  prefix: const Icon(Icons.calendar_today_outlined),
                  controller: cubit.ageToController,
                  type: TextInputType.number,
                );
              }),
            ),
          ],
        ),
      ),
    );
  }
}
