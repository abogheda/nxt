import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../../utils/constants/resources.dart';
import '../../create_job/controller/create_job_cubit.dart';
import '../../../widgets/shimmer_widgets/custom_shimmer.dart';

class CountryDropDown extends StatefulWidget {
  const CountryDropDown({Key? key}) : super(key: key);

  @override
  State<CountryDropDown> createState() => _CountryDropDownState();
}

class _CountryDropDownState extends State<CountryDropDown> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CreateJobCubit, CreateJobState>(
      builder: (context, state) {
        final cubit = CreateJobCubit.get(context);
        if (state is GetCountriesLoading || cubit.countriesList == null) {
          return CustomShimmer(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: widget.width * 0.03),
              child: Container(
                height: widget.height * 0.066,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  color: Colors.white,
                ),
              ),
            ),
          );
        }
        final countriesList = cubit.countriesList ?? [];
        List selectedCountry = cubit.selectedCountryList;
        return Container(
          margin: EdgeInsets.symmetric(horizontal: widget.width * 0.03),
          padding: EdgeInsets.symmetric(horizontal: widget.width * 0.03),
          decoration: BoxDecoration(
            border: Border.all(color: Colors.black),
            borderRadius: BorderRadius.circular(8),
          ),
          child: Row(
            children: [
              const Icon(Icons.flag_outlined),
              SizedBox(width: widget.width * 0.02),
              Expanded(
                child: DropdownButtonHideUnderline(
                  child: DropdownButton2(
                    isDense: false,
                    isExpanded: true,
                    itemPadding: EdgeInsets.zero,
                    dropdownPadding: EdgeInsets.zero,
                    buttonWidth: widget.width * 0.65,
                    dropdownWidth: widget.width * 0.65,
                    dropdownMaxHeight: widget.height * 0.55,
                    scrollbarRadius: const Radius.circular(50),
                    onChanged: (value) => cubit.updateCountryList(),
                    icon: const Icon(Icons.keyboard_arrow_down_rounded),
                    hint: Text('chooseCountry'.tr(), textAlign: TextAlign.start),
                    value: selectedCountry.isEmpty ? null : selectedCountry.last,
                    buttonPadding: EdgeInsets.symmetric(vertical: widget.height * 0.003),
                    style: TextStyle(
                        fontFamily: widget.isEn ? 'Montserrat' : 'Tajawal',
                        color: selectedCountry.isEmpty ? const Color(0xFFA5A5A5) : Colors.black),
                    selectedItemBuilder: (context) {
                      return countriesList.map(
                        (item) {
                          return Container(
                              alignment: AlignmentDirectional.centerStart, child: Text(selectedCountry.join(', ')));
                        },
                      ).toList();
                    },
                    items: countriesList.map((item) {
                      return DropdownMenuItem<String>(
                        value: item.name ?? "notAvailable".tr(),
                        child: StatefulBuilder(
                          builder: (context, menuSetState) {
                            bool isSelected = selectedCountry.contains(item.name);
                            return InkWell(
                              onTap: () {
                                isSelected
                                    ? cubit.removeFromSelectedTagsList(name: item.name!, id: item.id!)
                                    : cubit.addToSelectedTagsList(name: item.name!, id: item.id!);
                                setState(() {});
                                menuSetState(() {});
                                cubit.updateCountryList();
                              },
                              child: Container(
                                height: double.infinity,
                                padding: const EdgeInsets.symmetric(horizontal: 16.0),
                                child: Row(
                                  children: [
                                    isSelected
                                        ? const Icon(Icons.check_box_outlined)
                                        : const Icon(Icons.check_box_outline_blank),
                                    SizedBox(width: widget.width * 0.01),
                                    Text(item.name!, style: const TextStyle(fontSize: 12)),
                                  ],
                                ),
                              ),
                            );
                          },
                        ),
                      );
                    }).toList(),
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
