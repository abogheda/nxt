import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import '../../../../utils/constants/resources.dart';

import '../../../../data/enum/upload_type.dart';
import '../../../widgets/custom_drop_down_field.dart';
import '../../create_job/controller/create_job_cubit.dart';

class JobTypeDropDown extends StatelessWidget {
  const JobTypeDropDown({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final cubit = CreateJobCubit.get(context);
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: width * 0.03),
      child: CustomDropDownTextField<String>(
        value: cubit.jobType,
        hintTextStyle: Theme.of(context)
            .textTheme
            .bodyText1!
            .copyWith(fontFamily: isEn ? 'Montserrat' : 'Tajawal', color: const Color(0xFFA5A5A5)),
        items: [
          DropdownMenuItem(
            value: UploadType.video.type,
            child: Text(
              UploadType.video.type.tr(),
              style: TextStyle(fontFamily: isEn ? 'Montserrat' : 'Tajawal'),
            ),
          ),
          DropdownMenuItem(
            value: UploadType.audio.type,
            child: Text(
              UploadType.audio.type.tr(),
              style: TextStyle(fontFamily: isEn ? 'Montserrat' : 'Tajawal'),
            ),
          ),
          DropdownMenuItem(
            value: UploadType.text.type,
            child: Text(
              UploadType.text.type.tr(),
              style: TextStyle(fontFamily: isEn ? 'Montserrat' : 'Tajawal'),
            ),
          )
        ],
        hint: 'chooseType'.tr(),
        prefix: const Icon(Icons.description_outlined),
        onChanged: (val) => cubit.jobType = val,
      ),
    );
  }
}
