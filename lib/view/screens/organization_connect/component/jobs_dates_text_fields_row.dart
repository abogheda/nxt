import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import '../../../../utils/constants/app_const.dart';

import '../../../../utils/helpers/picker_helper.dart';
import '../../../../utils/helpers/popup_helper.dart';
import '../../../../utils/helpers/validators.dart';
import '../../../widgets/custom_text_field.dart';
import '../../create_job/controller/create_job_cubit.dart';

class JobsDatesTextFieldsRow extends StatefulWidget {
  const JobsDatesTextFieldsRow({Key? key}) : super(key: key);

  @override
  State<JobsDatesTextFieldsRow> createState() => _JobsDatesTextFieldsRowState();
}

class _JobsDatesTextFieldsRowState extends State<JobsDatesTextFieldsRow> {
  @override
  Widget build(BuildContext context) {
    final cubit = CreateJobCubit.get(context);
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: widget.width * 0.03),
      child: Form(
        key: cubit.datesFormKey,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              child: StatefulBuilder(builder: (_, stState) {
                return CustomTextField(
                  key: UniqueKey(),
                  hint: 'jobStartDate'.tr(),
                  prefix: const Icon(Icons.calendar_today_outlined),
                  validator: Validators.generalField,
                  initialValue:
                      cubit.jobStartDate == null ? null : DateFormat("dd/MM/yyyy").format(cubit.jobStartDate!),
                  onFieldSubmitted: (_) {
                    cubit.datesFormKey.currentState!.save();
                  },
                  onChange: (_) => cubit.datesFormKey.currentState!.save(),
                  onTap: () async {
                    FocusManager.instance.primaryFocus?.unfocus();
                    final date = await PickerHelper.pickDate(
                        currentTime: DateTime.now(),
                        minTime: DateTime.now(),
                        maxTime: DateTime(DateTime.now().year + 2));
                    if (date != null) {
                      cubit.jobStartDate = date;
                      stState(() {});
                    }
                  },
                );
              }),
            ),
            const SizedBox(width: 8),
            Expanded(
              child: StatefulBuilder(builder: (_, stState) {
                return CustomTextField(
                  key: UniqueKey(),
                  hint: 'jobEndDate'.tr(),
                  onFieldSubmitted: (_) {
                    cubit.datesFormKey.currentState!.save();
                  },
                  onChange: (_) => cubit.datesFormKey.currentState!.save(),
                  validator: Validators.generalField,
                  prefix: const Icon(Icons.calendar_today_outlined),
                  initialValue: cubit.jobEndDate == null ? null : DateFormat("dd/MM/yyyy").format(cubit.jobEndDate!),
                  onTap: () async {
                    if (cubit.jobStartDate == null) {
                      PopupHelper.showBasicSnack(msg: 'addMinAge'.tr());
                      return;
                    }
                    FocusManager.instance.primaryFocus?.unfocus();
                    final date = await PickerHelper.pickDate(
                      currentTime: cubit.jobEndDate ?? cubit.jobStartDate!,
                      minTime: cubit.jobStartDate!,
                      maxTime: DateTime(DateTime.now().year + 2),
                    );
                    if (date != null) {
                      cubit.jobEndDate = date;
                      stState(() {});
                    }
                  },
                );
              }),
            ),
          ],
        ),
      ),
    );
  }
}
