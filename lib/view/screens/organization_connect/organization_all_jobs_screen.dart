import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../utils/helpers/popup_helper.dart';
import '../../widgets/error_state_widget.dart';
import '../../widgets/no_state_widget.dart';
import '../../widgets/shimmer_widgets/my_job_card_shimmer.dart';
import '../my_jobs/widgets/my_job_card.dart';
import 'controller/organization_connect_cubit.dart';
import '../../../utils/constants/app_const.dart';

class OrganizationAllJobsScreen extends StatelessWidget {
  const OrganizationAllJobsScreen({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return BlocConsumer<OrganizationConnectCubit, OrganizationConnectState>(
      listener: (context, state) {
        if (state is GetAllJobsFailed) {
          PopupHelper.showBasicSnack(msg: state.msg, color: Colors.red);
        }
      },
      builder: (context, state) {
        var cubit = OrganizationConnectCubit.get(context);
        final allJobsList = cubit.allJobsList;
        if (state is GetAllJobsLoading || allJobsList == null) {
          return ListView.builder(
            itemCount: 3,
            padding: const EdgeInsets.all(10),
            itemBuilder: (context, index) => const MyJobCardShimmer(),
          );
        } else if (state is GetAllJobsFailed) {
          return ErrorStateWidget(hasRefresh: true, onRefresh: () async => await cubit.getAllJobs());
        }
        return RefreshIndicator(
            onRefresh: () async => await cubit.getAllJobs(),
            child: allJobsList.isEmpty
                ? ListView(children: [SizedBox(height: height * 0.35), NoStateWidget()])
                : ListView.builder(
                    itemCount: allJobsList.length,
                    padding: const EdgeInsets.all(10),
                    itemBuilder: (context, index) {
                      final job = allJobsList[index];
                      String getName() {
                        String name = '';
                        if (job.organization != null) {
                          name = job.organization!.name!;
                        } else {
                          name = job.expert!.expert!.name!;
                        }
                        return name;
                      }

                      String getLogo() {
                        String logo = '';
                        if (job.organization != null) {
                          logo = job.organization!.logo!;
                        } else {
                          logo = job.expert!.expert!.logo!;
                        }
                        return logo;
                      }

                      return MyJobCard(
                        id: job.id!,
                        title: job.title,
                        createdAt: job.createdAt,
                        color: job.category!.color,
                        name: getName(),
                        logo: getLogo(),
                      );
                    },
                  ));
      },
    );
  }
}
