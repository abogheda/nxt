import 'package:flutter/material.dart';
import '../../../utils/constants/resources.dart';
import '../../widgets/logo_app_bar.dart';
import 'component/get_ready_text.dart';
import 'component/page_view_buttons_row.dart';
import 'widgets/first_on_boarding_screen.dart';
import 'widgets/second_on_boarding_screen.dart';
import 'widgets/third_on_boarding_screen.dart';

class OnBoardingScreen extends StatefulWidget {
  const OnBoardingScreen({Key? key}) : super(key: key);

  @override
  State<OnBoardingScreen> createState() => _OnBoardingScreenState();
}

class _OnBoardingScreenState extends State<OnBoardingScreen> {
  late final PageController _pageController;
  int index = 0;

  @override
  void initState() {
    _pageController = PageController();
    super.initState();
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Column(
            children: [
              const LogoAppBar(),
              const GetReadyText(),
              Container(
                height: 1,
                margin: const EdgeInsets.only(top: 8),
                decoration: const BoxDecoration(color: Colors.black),
              ),
              Expanded(
                child: SizedBox(
                  child: PageView.builder(
                    reverse: true,
                    controller: _pageController,
                    itemCount: 3,
                    pageSnapping: true,
                    onPageChanged: (int page) => setState(() => index = page),
                    itemBuilder: (context, index) {
                      if (index == 0) return const FirstOnBoardingScreen();
                      if (index == 1) return const SecondOnBoardingScreen();
                      if (index == 2) return const ThirdOnBoardingScreen();
                      return const SizedBox();
                    },
                  ),
                ),
              ),
            ],
          ),
          PageViewButtonsRow(index: index, pageController: _pageController),
          Positioned(
            top: MediaQuery.of(context).padding.top,
            right: widget.isEn ? null : 0,
            left: widget.isEn ? 0 : null,
            child: const BackButton(),
          ),
        ],
      ),
    );
  }
}
