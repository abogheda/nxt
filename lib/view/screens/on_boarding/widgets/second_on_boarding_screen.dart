import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

import '../../../../utils/constants/resources.dart';

class SecondOnBoardingScreen extends StatelessWidget {
  const SecondOnBoardingScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          width: double.infinity,
          height: height * 0.15,
          child: ColoredBox(
            color: AppColors.redColor,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 4, vertical: 4.0),
              child: FittedBox(
                fit: BoxFit.contain,
                child: Align(
                  alignment: isEn ? Alignment.center : Alignment.bottomCenter,
                  child: Text(
                    'filmMaking'.tr() + 'qMark'.tr(),
                    maxLines: 1,
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.bodyLarge!.copyWith(
                          color: Colors.white,
                        ),
                  ),
                ),
              ),
            ),
          ),
        ),
        SizedBox(
          width: double.infinity,
          height: height * 0.15,
          child: ColoredBox(
            color: AppColors.yellowColor,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 4, vertical: 4.0),
              child: FittedBox(
                fit: BoxFit.contain,
                child: Align(
                  alignment: isEn ? Alignment.center : Alignment.bottomCenter,
                  child: Column(
                    children: [
                      SizedBox(height: isEn ? 0 : height * 0.005),
                      Text(
                        'acting'.tr() + 'qMark'.tr(),
                        maxLines: 1,
                        textAlign: TextAlign.center,
                        style: Theme.of(context).textTheme.bodyLarge!.copyWith(
                              color: Colors.white,
                            ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
        SizedBox(
          width: double.infinity,
          height: height * 0.15,
          child: ColoredBox(
            color: AppColors.blueColor,
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 4, vertical: isEn ? 4 : 16),
              child: FittedBox(
                fit: BoxFit.contain,
                child: Align(
                  alignment: isEn ? Alignment.center : Alignment.bottomCenter,
                  child: Text(
                    'musicOnBoarding'.tr() + 'qMark'.tr(),
                    maxLines: 1,
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.bodyLarge!.copyWith(
                          color: Colors.white,
                        ),
                  ),
                ),
              ),
            ),
          ),
        ),
        Expanded(
          child: SizedBox.expand(
            child: ColoredBox(
              color: Colors.black,
              child: Padding(
                padding: EdgeInsets.only(
                    right: width * 0.05, left: width * 0.05, bottom: height * 0.085, top: height * 0.02),
                child: Center(
                  child: Text(
                    'onboarding2'.tr(),
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w500,
                      fontFamily: isEn ? 'Montserrat' : 'Tajawal',
                    ),
                  ),
                ),
              ),
            ),
          ),
        )
      ],
    );
  }
}
