import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

import '../../../../utils/constants/resources.dart';

class ThirdOnBoardingScreen extends StatelessWidget {
  const ThirdOnBoardingScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(height: isEn ? 0 : height * 0.005),
        SizedBox(
          width: double.infinity,
          height: height * 0.15,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 4, vertical: 4.0),
            child: FittedBox(
              fit: BoxFit.contain,
              child: Align(
                alignment: isEn ? Alignment.center : Alignment.bottomCenter,
                child: Text(
                  'learnOnBoarding'.tr(),
                  maxLines: 1,
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.bodyLarge!.copyWith(),
                ),
              ),
            ),
          ),
        ),
        SizedBox(
          width: double.infinity,
          height: height * 0.15,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 4, vertical: 4.0),
            child: FittedBox(
              fit: BoxFit.contain,
              child: Align(
                alignment: isEn ? Alignment.center : Alignment.bottomCenter,
                child: Text(
                  'competeOnBoarding'.tr(),
                  maxLines: 1,
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.bodyLarge!.copyWith(),
                ),
              ),
            ),
          ),
        ),
        SizedBox(
          width: double.infinity,
          height: height * 0.15,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 4, vertical: 4.0),
            child: FittedBox(
              fit: BoxFit.contain,
              child: Align(
                alignment: isEn ? Alignment.center : Alignment.bottomCenter,
                child: Text(
                  'connectOnBoarding'.tr(),
                  maxLines: 1,
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.bodyLarge!.copyWith(),
                ),
              ),
            ),
          ),
        ),
        Expanded(
          child: SizedBox.expand(
            child: ColoredBox(
              color: Colors.black,
              child: Padding(
                padding: EdgeInsets.only(
                    right: width * 0.05, left: width * 0.05, bottom: height * 0.085, top: height * 0.02),
                child: Center(
                  child: Text(
                    'onboarding3'.tr(),
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w500,
                      fontFamily: isEn ? 'Montserrat' : 'Tajawal',
                    ),
                  ),
                ),
              ),
            ),
          ),
        )
      ],
    );
  }
}
