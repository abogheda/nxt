import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

import '../../../../utils/constants/resources.dart';

class FirstOnBoardingScreen extends StatelessWidget {
  const FirstOnBoardingScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(height: isEn ? 0 : height * 0.005),
        SizedBox(
          width: double.infinity,
          height: height * 0.15,
          child: Padding(
            padding: const EdgeInsets.only(left: 4, right: 4, top: 9, bottom: 2),
            child: FittedBox(
              fit: BoxFit.contain,
              child: Align(
                alignment: isEn ? Alignment.center : Alignment.bottomCenter,
                child: Text(
                  'areUTheNxt1'.tr(),
                  maxLines: 1,
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.bodyLarge!.copyWith(),
                ),
              ),
            ),
          ),
        ),
        SizedBox(
          width: double.infinity,
          height: height * 0.15,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 4, vertical: 4.0),
            child: FittedBox(
              fit: BoxFit.contain,
              child: Align(
                alignment: isEn ? Alignment.center : Alignment.bottomCenter,
                child: Text(
                  'areUTheNxt2'.tr(),
                  maxLines: 1,
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.bodyLarge!.copyWith(),
                ),
              ),
            ),
          ),
        ),
        SizedBox(
          width: double.infinity,
          height: height * 0.15,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 4, vertical: 4.0),
            child: FittedBox(
              fit: BoxFit.contain,
              child: Align(
                alignment: isEn ? Alignment.center : Alignment.bottomCenter,
                child: Text(
                  'talentOnBoarding'.tr() + 'qMark'.tr(),
                  maxLines: 1,
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.bodyLarge!.copyWith(),
                ),
              ),
            ),
          ),
        ),
        Expanded(
          child: SizedBox.expand(
            child: ColoredBox(
              color: Colors.black,
              child: Padding(
                padding: EdgeInsets.only(
                    right: width * 0.05, left: width * 0.05, bottom: height * 0.085, top: height * 0.02),
                child: Center(
                  child: Text(
                    'onboarding1'.tr(),
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w500,
                      fontFamily: isEn ? 'Montserrat' : 'Tajawal',
                    ),
                  ),
                ),
              ),
            ),
          ),
        )
      ],
    );
  }
}
