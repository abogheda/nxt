import 'package:flutter/material.dart';

class DotContainer extends StatelessWidget {
  const DotContainer({
    Key? key,
    required this.index,
    required this.condition,
    required this.color,
  }) : super(key: key);

  final int index;
  final int condition;
  final Color color;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(6),
      width: 12.0,
      height: 12.0,
      decoration: BoxDecoration(
        color: index == condition ? color : Colors.white,
        shape: BoxShape.circle,
      ),
    );
  }
}
