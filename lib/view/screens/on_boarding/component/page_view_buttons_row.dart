import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import '../../../../config/router/router.dart';
import '../../../../data/service/hive_services.dart';

import '../../../../utils/constants/resources.dart';

import '../../welcome_to_project/welcome_to_project_imports.dart';
import 'dot_container.dart';

class PageViewButtonsRow extends StatelessWidget {
  const PageViewButtonsRow({Key? key, required this.index, required this.pageController}) : super(key: key);
  final int index;
  final PageController pageController;
  Future<void> onComplete() async {
    await HiveHelper.cacheOnBoarding(value: false);
    MagicRouter.navigateAndPopAll(const WelcomeToProject());
  }

  @override
  Widget build(BuildContext context) {
    return Positioned(
      left: 0,
      right: 0,
      bottom: height * 0.015,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          TextButton(
            onPressed: index == 2 ? null : () async => await onComplete(),
            child: Text(
              'skip'.tr(),
              style: Theme.of(context).textTheme.titleMedium!.copyWith(
                    color: index == 2 ? Colors.transparent : Colors.white,
                    fontFamily: isEn ? 'Montserrat' : 'Tajawal',
                  ),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              DotContainer(
                index: index,
                condition: 0,
                color: AppColors.redColor,
              ),
              DotContainer(
                index: index,
                condition: 1,
                color: AppColors.yellowColor,
              ),
              DotContainer(
                index: index,
                condition: 2,
                color: AppColors.blueColor,
              ),
            ],
          ),
          TextButton(
            onPressed: () async {
              if (index == 2) {
                await onComplete();
              } else {
                pageController.nextPage(
                  duration: const Duration(milliseconds: 200),
                  curve: Curves.easeInOutSine,
                );
              }
            },
            child: Text(
              index == 2 ? 'start'.tr() : 'next'.tr(),
              style: Theme.of(context).textTheme.titleMedium!.copyWith(
                    color: Colors.white,
                    fontFamily: isEn ? 'Montserrat' : 'Tajawal',
                  ),
            ),
          ),
        ],
      ),
    );
  }
}
