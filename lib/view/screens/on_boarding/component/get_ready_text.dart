import 'package:auto_size_text/auto_size_text.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import '../../../../utils/constants/resources.dart';

class GetReadyText extends StatelessWidget {
  const GetReadyText({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        SizedBox(height: height * 0.049),
        const Divider(color: Colors.black, thickness: 1.1),
        AutoSizeText(
          'getReadyTxt'.tr(),
          style: TextStyle(
            fontSize: 14,
            color: Theme.of(context).primaryColor,
            fontWeight: FontWeight.bold,
            fontFamily: isEn ? 'Montserrat' : 'Tajawal',
          ),
          softWrap: true,
          wrapWords: true,
        ),
      ],
    );
  }
}
