part of 'welcome_to_project_imports.dart';

class WelcomeToProject extends StatelessWidget {
  const WelcomeToProject({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: BlocProvider(
        create: (context) => LoginCubit()..onInit(),
        child: const WelcomeBody(),
      ),
    );
  }
}
