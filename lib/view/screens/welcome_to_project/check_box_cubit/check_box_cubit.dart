import 'package:flutter_bloc/flutter_bloc.dart';
part 'check_box_state.dart';

class CheckboxCubit extends Cubit<CheckboxState> {
  CheckboxCubit() : super(CheckboxState(isChecked: false));

  void changeValue(bool value) {
    emit(state.copyWith(changeState: value));
  }
}