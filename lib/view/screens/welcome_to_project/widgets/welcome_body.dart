part of 'welcome_to_project_widgets_imports.dart';

class WelcomeBody extends StatelessWidget {
  const WelcomeBody({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocListener<LoginCubit, LoginState>(
      listener: (context, state) async {
        //==== Google login ====
        if (state is GoogleSignInSuccessState) {
          if (state.userModel.token == null || state.userModel.token == '') {
            PopupHelper.showBasicSnack(msg: state.userModel.message.toString(), color: Colors.red);
            return;
          }
          if (state.userModel.user!.verify == false) {
            PopupHelper.showBasicSnack(msg: 'notVerified'.tr(), color: Colors.red);
            await HiveHelper.logout();
            return;
          }
          if (state.userModel.token != null) {
            if (HiveHelper.getUserInfo!.user!.profileStatus == "NotCompleted") {
              MagicRouter.navigateAndPopAll(const SelectVerticalScreen());
              // MagicRouter.navigateAndPopAll(const CompleteProfileScreen());
            } else {
              MagicRouter.navigateAndPopAll(Navigation());
            }
          }
        }
        if (state is GoogleSignInErrorState) {
          PopupHelper.showBasicSnack(msg: state.error.toString(), color: Colors.red);
        }
        //==== Facebook login ====
        if (state is FacebookSignInSuccessState) {
          if (state.userModel.token == null || state.userModel.token == '') {
            PopupHelper.showBasicSnack(msg: state.userModel.message.toString(), color: Colors.red);
            return;
          }
          if (state.userModel.user!.verify == false) {
            PopupHelper.showBasicSnack(msg: 'notVerified'.tr(), color: Colors.red);
            await HiveHelper.logout();
            return;
          }
          if (state.userModel.token != null) {
            if (HiveHelper.getUserInfo!.user!.profileStatus == "NotCompleted") {
              MagicRouter.navigateAndPopAll(const SelectVerticalScreen());
              // MagicRouter.navigateAndPopAll(const CompleteProfileScreen());
            } else {
              MagicRouter.navigateAndPopAll(Navigation());
            }
          }
          if (state is FacebookSignInErrorState) {
            PopupHelper.showBasicSnack(msg: state.userModel.message.toString(), color: Colors.red);
          }
        }
        if (state is FacebookSignInErrorState) {
          PopupHelper.showBasicSnack(msg: state.error.toString(), color: Colors.red);
        }
      },
      child: ListView(
        children: [
          SizedBox(height: height * 0.08),
          SvgPicture.asset('assets/images/fab_logo.svg', width: width * 0.7),
          SizedBox(height: height * 0.02),
          Text(
            'welcome'.tr(),
            maxLines: 1,
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 100.sp,
              fontWeight: FontWeight.normal,
              fontFamily: isEn ? 'BebasNeue' : 'Tajawal',
            ),
          ),
          Align(
            alignment: isEn ? Alignment.center : Alignment.bottomCenter,
            child: Text(
              'toProjectNXT'.tr(),
              maxLines: 1,
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 65.sp,
                fontWeight: FontWeight.normal,
                fontFamily: isEn ? 'BebasNeue' : 'Tajawal',
              ),
            ),
          ),
          SizedBox(height: height * 0.12),
          Align(
            alignment: isEn ? Alignment.center : Alignment.bottomCenter,
            child: Text(
              'continueWith'.tr(),
              maxLines: 1,
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 14.sp,
                fontWeight: FontWeight.w600,
                fontFamily: 'Montserrat',
              ),
            ),
          ),
          SizedBox(height: height * 0.01),
          const SocialButtonsRow(),
          SizedBox(height: height * 0.03),
          Align(
            alignment: isEn ? Alignment.center : Alignment.bottomCenter,
            child: Text(
              'or'.tr().toUpperCase(),
              maxLines: 1,
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 14.sp,
                fontWeight: FontWeight.w600,
                fontFamily: 'Montserrat',
              ),
            ),
          ),
          SizedBox(height: height * 0.03),
          const WelcomeButton(),
          SizedBox(height: height * 0.015),
          BlocBuilder<CheckboxCubit, CheckboxState>(
            builder: (context, state) {
              return Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Checkbox(
                      value: state.isChecked,
                      onChanged: (value) {
                        context.read<CheckboxCubit>().changeValue(value!);
                      }),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Align(
                        alignment: isEn ? Alignment.center : Alignment.bottomCenter,
                        child: Text(
                          'agreeToText'.tr(),
                          maxLines: 1,
                          textAlign: TextAlign.center,
                          style: Theme.of(context).textTheme.bodySmall!.copyWith(
                                color: AppColors.greyTxtColor,
                                fontWeight: FontWeight.w300,
                                fontFamily: 'Montserrat',
                              ),
                        ),
                      ),
                      SizedBox(height: height * 0.005),
                      Align(
                        alignment: isEn ? Alignment.center : Alignment.bottomCenter,
                        child: InkWell(
                          onTap: () async => await Utils.launchAppUrl(
                              url: AppConst.isProduction
                                  ? "${AppConst.productionUrl}/rules/"
                                  : "${AppConst.developmentUrl}/rules/"),
                          child: FittedBox(
                            fit: BoxFit.contain,
                            child: Text(
                              'welcomeRules'.tr(),
                              maxLines: 2,
                              textAlign: TextAlign.center,
                              style: Theme.of(context).textTheme.bodySmall!.copyWith(
                                    color: Colors.black,
                                    fontSize: 11.5,
                                    decoration: TextDecoration.underline,
                                    fontWeight: FontWeight.w600,
                                    fontFamily: 'Montserrat',
                                  ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  )
                ],
              );
            },
          ),
        ],
      ),
    );
  }
}
