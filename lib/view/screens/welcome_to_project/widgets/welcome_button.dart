part of 'welcome_to_project_widgets_imports.dart';

class WelcomeButton extends StatelessWidget {
  const WelcomeButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 50,
      width: double.infinity,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 25.0),
        child: OutlinedButton(
          style: ButtonStyle(
            elevation: MaterialStateProperty.all(0),
            alignment: Alignment.center,
            side: MaterialStateProperty.all(BorderSide(
                width: 1, color: context.watch<CheckboxCubit>().state.isChecked == false ? Colors.grey : Colors.black)),
            backgroundColor: MaterialStateProperty.all(
                context.watch<CheckboxCubit>().state.isChecked == false ? Colors.grey : Colors.black),
            shape: MaterialStateProperty.all(RoundedRectangleBorder(borderRadius: BorderRadius.circular(6.0))),
          ),
          onPressed: () {
            if (context.read<CheckboxCubit>().state.isChecked == true) {
              MagicRouter.navigateAndPopAll(BlocProvider.value(
                value: context.read<LoginCubit>(),
                child: const LoginScreen(),
              ));
            }
            // print("test :${context.read<CheckboxCubit>().state.isChecked}");
          },
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              SvgPicture.asset('assets/images/phone_login.svg'),
              Text(
                'continueWithPhoneNumber'.tr(),
                softWrap: true,
                style: isEn
                    ? TextStyle(
                        fontSize: 12.sp, fontWeight: FontWeight.w600, fontFamily: "Montserrat", color: Colors.white)
                    : Theme.of(context).textTheme.titleMedium!.copyWith(
                          fontSize: 12.0.sp,
                          fontWeight: FontWeight.w600,
                          color: Colors.white,
                          fontFamily: 'Tajawal',
                        ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
