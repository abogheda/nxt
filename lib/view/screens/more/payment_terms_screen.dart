import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'legal_scrollable_screen.dart';

import 'cubit/more_cubit.dart';

class PaymentTermsScreen extends StatelessWidget {
  const PaymentTermsScreen({Key? key, this.showActions = true, this.isLogoClickable = true}) : super(key: key);
  final bool showActions;
  final bool isLogoClickable;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MoreCubit, MoreState>(
      builder: (context, state) {
        final legalModel = context.read<MoreCubit>().legalModel;
        return LegalScrollableScreen(
          legalIndex: 2,
          title: 'paymentTerms'.tr(),
          subTitle: "read".tr(),
          legalModel: legalModel,
          showActions: showActions,
          isLogoClickable: isLogoClickable,
          onRefresh: () async => await context.read<MoreCubit>().getLegal(),
          loadingCondition: state is GetLegalLoading || legalModel == null,
        );
      },
    );
  }
}
