import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import '../../../utils/constants/app_const.dart';
import '../../../../../utils/constants/app_colors.dart';
import '../../widgets/custom_button.dart';
import '../../widgets/custom_text_field.dart';
import '../navigation_and_appbar/import_widget.dart';

final formKey = GlobalKey<FormState>();
final formKey2 = GlobalKey<FormState>();

class ReportIssueScreen extends StatelessWidget {
  const ReportIssueScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomScaffoldWidget(
      extendBodyBehindAppBar: false,
      backgroundColor: Colors.white,
      isBackArrow: true,
      isThereLogo: true,
      isThereActions: true,
      body: Stack(
        children: [
          Positioned(
            top: 0,
            bottom: 0,
            right: 0,
            left: 0,
            child: ListView(
              children: [
                Container(color: Colors.black, width: double.infinity, height: 1.5),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: width * 0.04, vertical: 24),
                  child: Text(
                    'havingTrouble'.tr(),
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.headline3!.copyWith(
                          fontFamily: isEn ? 'BebasNeue' : 'Tajawal',
                          color: Colors.black,
                        ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: width * 0.04),
                  child: Text(
                    'howCanHelp'.tr(),
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.bodyText2!.copyWith(
                          fontFamily: isEn ? 'Montserrat' : 'Tajawal',
                          color: AppColors.iconsColor,
                          fontWeight: FontWeight.bold,
                        ),
                  ),
                ),
                const SizedBox(height: 24),
                // Padding(
                //   padding: const EdgeInsets.symmetric(horizontal: 20),
                //   child: Form(
                //     key: formKey,
                //     child: Column(
                //       children: [
                //         CustomTextField(
                //           hint: 'login.email'.tr(),
                //           validator: Validators.email,
                //           prefix: const Icon(
                //             Icons.email_outlined,
                //             color: AppColors.iconsColor,
                //           ),
                //         ),
                //         const SizedBox(height: 12),
                //         CustomTextField(
                //           prefix: const Icon(
                //             Icons.local_phone_outlined,
                //             color: AppColors.iconsColor,
                //           ),
                //           hint: 'phoneNumber'.tr(),
                //           validator: Validators.phone,
                //           type: TextInputType.phone,
                //           onFieldSubmitted: (val) {},
                //         ),
                //         const SizedBox(height: 12),
                //         CustomTextField(
                //           validator: Validators.generalField,
                //           type: TextInputType.name,
                //           hint: 'nameHint'.tr(),
                //           prefix: Image.asset('assets/images/profile_icon.webp'),
                //         ),
                //       ],
                //     ),
                //   ),
                // ),
                // const SizedBox(height: 12),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: Form(
                    key: formKey2,
                    child: CustomTextField(
                      validator: (value) {
                        if (value!.isEmpty) {
                          return ('Please provide your issue / suggestion');
                        }
                        if (value.length < 20) {
                          return ("Minimum 20 Characters in order to help !!");
                        } else {
                          return null;
                        }
                      },
                      hint: 'Your Message...',
                      lines: 12,
                    ),
                  ),
                ),
              ],
            ),
          ),
          Positioned(
            right: 0,
            left: 0,
            bottom: 0,
            child: Container(
              color: Colors.black,
              child: CustomButton(
                height: height * 0.08,
                width: double.infinity,
                onTap: () {
                  if (formKey.currentState!.validate() && formKey2.currentState!.validate()) {}
                },
                child: Text(
                  'sendNow'.tr().toUpperCase(),
                  style: TextStyle(
                    fontSize: 18,
                    color: Colors.white,
                    fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal',
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
