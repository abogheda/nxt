// ignore_for_file: unnecessary_import, depend_on_referenced_packages

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nxt/data/models/save/saved_nxt_classes.dart';
import 'package:nxt/data/repos/imports.dart';

import '../../../../data/models/jobs/my_jobs_model.dart';
import '../../../../data/models/more/legal_model.dart';
import '../../../../data/models/more/plan_model.dart';
import '../../../../data/models/save/saved_challenges.dart';
import '../../../../data/models/save/saved_reads.dart';
import '../../../../data/repos/more/more_repo.dart';
import '../../../../data/models/save/saved_jobs.dart';

part 'more_state.dart';

class MoreCubit extends Cubit<MoreState> {
  MoreCubit() : super(MoreInitial());
  static MoreCubit get(context) => BlocProvider.of(context);
  late bool isSaved;
  late bool isSavedReel;
  SavedChallenges? savedChallenges;
  SavedNxtClasses? savedNxtClasses;
  SavedJobs? savedJobs;
  SavedReads? savedReads;
  LegalModel? legalModel;
  PlanModel? planModel;
  List<MyJobsModel?>? myJobsList;

  Future<void> deleteUserAccount() async {
    emit(DeleteAccountLoading());
    final res = await MoreRepo.deleteUserAccount();
    if (res.data != null) {
      emit(DeleteAccountSuccess());
    } else {
      emit(DeleteAccountFailed(msg: res.message ?? ""));
    }
  }

  Future<void> getSavedChallenges() async {
    emit(GetSavedItemsLoading());
    final res = await SaveRepo.getSavedChallenges();
    if (res.data != null) {
      savedChallenges = res.data;
      emit(GetSavedItemsSuccess());
    } else {
      emit(GetSavedItemsFailed(msg: res.message ?? ""));
    }
  }

  Future<void> getSavedClasses() async {
    emit(GetSavedItemsLoading());
    final res = await SaveRepo.getSavedClasses();
    if (res.data != null) {
      savedNxtClasses = res.data;
      emit(GetSavedItemsSuccess());
    } else {
      emit(GetSavedItemsFailed(msg: res.message ?? ""));
    }
  }

  Future<void> getSavedJobs() async {
    emit(GetSavedItemsLoading());
    final res = await SaveRepo.getSavedJobs();
    if (res.data != null) {
      savedJobs = res.data;
      emit(GetSavedItemsSuccess());
    } else {
      emit(GetSavedItemsFailed(msg: res.message ?? ""));
    }
  }

  Future<void> getSavedReads() async {
    emit(GetSavedItemsLoading());
    final res = await SaveRepo.getSavedReads();
    if (res.data != null) {
      savedReads = res.data;
      emit(GetSavedItemsSuccess());
    } else {
      emit(GetSavedItemsFailed(msg: res.message ?? ""));
    }
  }

  Future<void> getLegal() async {
    emit(GetLegalLoading());
    final res = await MoreRepo.getLegal();
    if (res.data != null) {
      legalModel = res.data;
      emit(GetLegalSuccess());
    } else {
      emit(GetLegalFailed(msg: res.message ?? ""));
    }
  }

  Future<void> getCurrentPlan() async {
    emit(GetPlanLoading());
    final res = await MoreRepo.getCurrentPlan();
    if (res.data != null) {
      planModel = res.data;
      emit(GetPlanSuccess());
    } else {
      emit(GetPlanFailed(msg: res.message ?? ""));
    }
  }

  Future<void> getMyJobs() async {
    emit(GetMyJobsLoading());
    final res = await MoreRepo.getMyJobs();
    if (res.data != null) {
      myJobsList = res.data;
      emit(GetMyJobsSuccess());
    } else {
      emit(GetMyJobsFailed(msg: res.message ?? ""));
    }
  }
}
