part of 'more_cubit.dart';

abstract class MoreState extends Equatable {
  const MoreState();

  @override
  List<Object> get props => [];
}

class MoreInitial extends MoreState {}

class ChangeIsSavedState extends MoreState {}

class DeleteAccountSuccess extends MoreState {}

class DeleteAccountLoading extends MoreState {}

class DeleteAccountFailed extends MoreState {
  final String msg;
  const DeleteAccountFailed({required this.msg});
}

class SavedSuccess extends MoreState {}

class SavedLoading extends MoreState {}

class SavedFailed extends MoreState {
  final String msg;
  const SavedFailed({required this.msg});
}

class GetSavedItemsSuccess extends MoreState {}

class GetSavedItemsLoading extends MoreState {}

class GetSavedItemsFailed extends MoreState {
  final String msg;
  const GetSavedItemsFailed({required this.msg});
}

class GetLegalSuccess extends MoreState {}

class GetLegalLoading extends MoreState {}

class GetLegalFailed extends MoreState {
  final String msg;
  const GetLegalFailed({required this.msg});
}

class GetPlanSuccess extends MoreState {}

class GetPlanLoading extends MoreState {}

class GetPlanFailed extends MoreState {
  final String msg;
  const GetPlanFailed({required this.msg});
}

class GetMyJobsSuccess extends MoreState {}

class GetMyJobsLoading extends MoreState {}

class GetMyJobsFailed extends MoreState {
  final String msg;
  const GetMyJobsFailed({required this.msg});
}
