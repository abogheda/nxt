import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import '../../../utils/constants/resources.dart';

import '../navigation_and_appbar/import_widget.dart';

class FAQScreen extends StatelessWidget {
  const FAQScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomScaffoldWidget(
      extendBodyBehindAppBar: false,
      backgroundColor: Colors.white,
      isBackArrow: true,
      isThereLogo: true,
      isThereActions: true,
      body: Column(
        children: [
          Container(color: Colors.black, width: double.infinity, height: 1.5),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: width * 0.04, vertical: 24),
            child: Text(
              'faq'.tr(),
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.headline3!.copyWith(
                    fontFamily: isEn ? 'BebasNeue' : 'Tajawal',
                    color: Colors.black,
                  ),
            ),
          ),
          Expanded(
            child: ListView.builder(
              itemCount: 10,
              physics: const ScrollPhysics(),
              shrinkWrap: true,
              itemBuilder: (context, index) {
                return const DocumentTile();
              },
            ),
          ),
        ],
      ),
    );
  }
}

class DocumentTile extends StatelessWidget {
  const DocumentTile({
    Key? key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.symmetric(horizontal: width * 0.03, vertical: 6),
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8.0),
          side: const BorderSide(
            color: Color(0xFF707070),
          )),
      child: ExpansionTile(
        tilePadding: EdgeInsets.symmetric(vertical: 12, horizontal: width * 0.04),
        title: Text(
          "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy ? tempor invidunt ut labore et dolore.",
          style: Theme.of(context)
              .textTheme
              .bodyText2!
              .copyWith(fontFamily: isEn ? 'Montserrat' : 'Tajawal', color: Colors.black, fontWeight: FontWeight.bold),
        ),
        children: [
          Container(
            width: double.maxFinite,
            padding: const EdgeInsets.symmetric(vertical: 16, horizontal: 16.0),
            child: Text(
              "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.",
              style: Theme.of(context)
                  .textTheme
                  .bodyText2!
                  .copyWith(fontFamily: isEn ? 'Montserrat' : 'Tajawal', color: const Color(0xFF727272)),
            ),
          )
        ],
      ),
    );
  }
}
