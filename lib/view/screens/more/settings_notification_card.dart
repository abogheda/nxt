import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../../utils/constants/app_colors.dart';
import '../../../utils/constants/app_const.dart';

class SettingsNotificationCard extends StatefulWidget {
  const SettingsNotificationCard({Key? key}) : super(key: key);

  @override
  State<SettingsNotificationCard> createState() => _SettingsNotificationCardState();
}

class _SettingsNotificationCardState extends State<SettingsNotificationCard> {
  bool state = true;
  bool notifyValue = true;
  @override
  Widget build(BuildContext context) {
    return DecoratedBox(
      decoration: BoxDecoration(border: Border.all(color: const Color(0xFFEDEDED))),
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 4, horizontal: 16),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              "notifications".tr().toUpperCase(),
              textAlign: TextAlign.start,
              style: Theme.of(context).textTheme.bodyText1!.copyWith(
                    fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal',
                    fontWeight: FontWeight.bold,
                  ),
            ),
            CupertinoSwitch(
              value: state,
              activeColor: Colors.black,
              trackColor: AppColors.greyNavbar,
              onChanged: ((notifyValue) {
                setState(() {
                  state = notifyValue;
                });
              }),
            )
          ],
        ),
      ),
    );
  }
}
