import 'package:flutter/material.dart';
import '../../../utils/constants/resources.dart';

class MoreCard extends StatelessWidget {
  const MoreCard({
    Key? key,
    required this.text,
    required this.onPressed,
    this.padding,
    this.textColor,
  }) : super(key: key);
  final String text;
  final Function()? onPressed;
  final EdgeInsetsGeometry? padding;
  final Color? textColor;
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: padding,
      decoration: BoxDecoration(border: Border.all(color: const Color(0xFFEDEDED))),
      child: MaterialButton(
          onPressed: onPressed,
          child: Row(
            children: [
              Expanded(
                child: Text(
                  text,
                  textAlign: TextAlign.start,
                  style: Theme.of(context).textTheme.bodyText1!.copyWith(
                        fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal',
                        fontWeight: FontWeight.bold,
                        color: textColor,
                      ),
                ),
              ),
            ],
          )),
    );
  }
}
