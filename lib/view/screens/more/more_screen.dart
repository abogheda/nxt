import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import '../../../utils/helpers/utils.dart';
import '../../../config/router/router.dart';
import '../../../data/service/hive_services.dart';
import '../../../utils/constants/resources.dart';
import '../../../utils/helpers/popup_helper.dart';
import '../login/login_screen.dart';
import 'e_u_l_a_screen.dart';
import 'more_card.dart';
import 'more_expanded_tile.dart';
import 'payment_terms_screen.dart';
import '../my_jobs/my_jobs_screen.dart';
import '../settings_language_screen/settings_language_screen.dart';
import 'challenges_terms_screen.dart';
import 'community_guide_lines_screen.dart';
import 'cubit/more_cubit.dart';
import 'privacy_screen.dart';
import 'more_screens/saved_items/saved_items_screen.dart';
import 'terms_and_condition_screen.dart';
import 'more_widgets/custom_logout_dialog.dart';

class MoreScreen extends StatelessWidget {
  const MoreScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      lazy: false,
      create: (context) => MoreCubit(),
      child: BlocConsumer<MoreCubit, MoreState>(
        listener: (context, state) {
          if (state is DeleteAccountFailed) PopupHelper.showBasicSnack(msg: state.msg.toString(), color: Colors.red);
        },
        builder: (context, state) {
          final cubit = MoreCubit.get(context);
          return ListView(
            children: [
              const Divider(color: Colors.black, thickness: 0.9),
              const SizedBox(height: 12),
              MoreExpandedTile(
                title: "account".tr(),
                contentList: [
                  // HiveHelper.isRegularUser && Platform.isAndroid
                  //     ? MoreCard(
                  //         text: "subscription".tr(),
                  //         onPressed: () {
                  //           print(hiveUser.toJson());
                  //           print(hiveUser.invitationExpiryDate ?? 'asdfasdf');
                  //           // final bool isInvitation =
                  //           //     DateTime.now().isBefore(DateTime.parse(hiveUser.invitationExpiryDate!));
                  //           // print(isInvitation);
                  //           // if (isInvitation) {
                  //           //   MagicRouter.navigateTo(const SubscriptionInvitationScreen());
                  //           // } else {
                  //           //   MagicRouter.navigateTo(BlocProvider.value(
                  //           //       value: cubit..getCurrentPlan(), child: const SubscriptionScreen()));
                  //           // }
                  //         })
                  //     : const SizedBox(),
                  MoreCard(
                      text: "mySaved".tr(),
                      onPressed: () => MagicRouter.navigateTo(BlocProvider.value(
                          value: cubit
                            ..getSavedChallenges()
                            // ..getSavedClasses()
                            ..getSavedJobs()
                            ..getSavedReads(),
                          child: const SavedItemsScreen()))),
                  MoreCard(
                      text: "myJobs".tr(),
                      onPressed: () => MagicRouter.navigateTo(
                          BlocProvider.value(value: cubit..getMyJobs(), child: const MyJobsScreen()))),
                  MoreCard(
                      text: "deleteAccount".tr().toUpperCase(),
                      textColor: Colors.red,
                      onPressed: () => Utils.showAlertDialog(
                            context: context,
                            headerText: 'sureDeleteAccount'.tr(),
                            onPressed: () async {
                              await cubit.deleteUserAccount();
                              await HiveHelper.logout();
                              if (HiveHelper.getUserInfo?.user?.facebookId != null) {
                                await FacebookAuth.instance.logOut().then((value) {});
                              }
                              if (HiveHelper.getUserInfo?.user?.googleId != null) {
                                await GoogleSignIn().signOut().then((value) {});
                              }
                              MagicRouter.navigateAndPopAll(const LoginScreen());
                            },
                          )),
                  // MoreCard(text: "myAgenda".tr(), onPressed: () => MagicRouter.navigateTo(const AgendaWidget())),
                ],
              ),
              MoreExpandedTile(
                title: "settings".tr(),
                contentList: [
                  // const SettingsNotificationCard(),
                  MoreCard(
                      text: "language".tr(), onPressed: () => MagicRouter.navigateTo(const SettingsLanguageScreen())),
                ],
              ),
              MoreExpandedTile(
                title: "legal".tr(),
                contentList: [
                  MoreCard(
                      text: "terms".tr(),
                      onPressed: () => MagicRouter.navigateTo(
                          BlocProvider.value(value: cubit..getLegal(), child: const TermsAndConditionsScreen()))),
                  MoreCard(
                      text: "privacyAndCookies".tr(),
                      onPressed: () => MagicRouter.navigateTo(
                          BlocProvider.value(value: cubit..getLegal(), child: const PrivacyScreen()))),
                  MoreCard(
                      text: "paymentTerms".tr(),
                      onPressed: () => MagicRouter.navigateTo(
                          BlocProvider.value(value: cubit..getLegal(), child: const PaymentTermsScreen()))),
                  MoreCard(
                      text: "challengeTerms".tr(),
                      onPressed: () => MagicRouter.navigateTo(
                          BlocProvider.value(value: cubit..getLegal(), child: const ChallengesTermsScreen()))),
                  MoreCard(
                      text: "communityGuidelines".tr(),
                      onPressed: () => MagicRouter.navigateTo(
                          BlocProvider.value(value: cubit..getLegal(), child: const CommunityGuideLinesScreen()))),
                  MoreCard(
                      text: "eula".tr().toUpperCase(),
                      onPressed: () => MagicRouter.navigateTo(
                          BlocProvider.value(value: cubit..getLegal(), child: const EULAScreen()))),
                ],
              ),
              // MoreExpandedTile(
              //   title: "help".tr(),
              //   contentList: [
              //     MoreCard(text: "faq".tr(), onPressed: () => MagicRouter.navigateTo(const FAQScreen())),
              //     MoreCard(text: "report".tr(), onPressed: () => MagicRouter.navigateTo(const ReportIssueScreen())),
              //     MoreCard(text: "contact".tr(), onPressed: () => MagicRouter.navigateTo(ContactUsScreen())),
              //   ],
              // ),
              MoreExpandedTile(
                title: "about".tr(),
                contentList: [
                  MoreCard(
                    onPressed: null,
                    text: "welcomeToNxt".tr(),
                    padding: const EdgeInsets.symmetric(vertical: 8),
                  ),
                ],
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: width * 0.03),
                child: InkWell(
                  borderRadius: BorderRadius.circular(6),
                  onTap: () => showLogoutDialog(context),
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: width * 0.03, vertical: 6),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text("logout".tr(),
                            style: Theme.of(context)
                                .textTheme
                                .headline6!
                                .copyWith(fontFamily: AppConst.isEn ? 'BebasNeue' : 'Tajawal')),
                        RotatedBox(
                            quarterTurns: isEn ? 0 : 2,
                            child: Image.asset('assets/images/logout_icon.png', height: 36.0, width: 36.0))
                      ],
                    ),
                  ),
                ),
              ),
              // GoogleButtonWidget(),
              // OutlinedButton(
              //     onPressed: () async {
              //       await FlutterLocalNotificationUtils.instance.showInstantNotification(
              //         id: Utils.createUniqueId(),
              //         title: 'This is test title',
              //         body: 'and this is the body',
              //       );
              //     },
              //     child: const Text('Send basic notification')),
              SizedBox(height: height * 0.01)
            ],
          );
        },
      ),
    );
  }
}
