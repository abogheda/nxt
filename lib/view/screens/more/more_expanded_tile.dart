import 'package:flutter/material.dart';
import 'package:flutter_expanded_tile/flutter_expanded_tile.dart';
import '../../../utils/constants/app_const.dart';

class MoreExpandedTile extends StatefulWidget {
  const MoreExpandedTile({Key? key, required this.title, required this.contentList}) : super(key: key);
  final String title;
  final List<Widget> contentList;

  @override
  State<MoreExpandedTile> createState() => _MoreExpandedTileState();
}

class _MoreExpandedTileState extends State<MoreExpandedTile> {
  late ExpandedTileController _controller;
  @override
  void initState() {
    _controller = ExpandedTileController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: widget.width * 0.03, vertical: 4),
      child: ExpandedTile(
        controller: _controller,
        trailing: const Icon(Icons.arrow_forward_ios_rounded, color: Colors.white, size: 16),
        content: Column(children: widget.contentList),
        title: Text(widget.title,
            style: Theme.of(context)
                .textTheme
                .headline6!
                .copyWith(fontFamily: AppConst.isEn ? 'BebasNeue' : 'Tajawal', color: Colors.white)),
        theme: const ExpandedTileThemeData(
          headerRadius: 4,
          contentRadius: 0,
          headerColor: Colors.black,
          headerSplashColor: Colors.grey,
          contentPadding: EdgeInsets.zero,
          contentBackgroundColor: Colors.white,
          headerPadding: EdgeInsets.symmetric(vertical: 12, horizontal: 8),
        ),
      ),
    );
  }
}
