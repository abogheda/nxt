// // ignore_for_file: library_private_types_in_public_api
//
// import 'package:flutter/material.dart';
// import 'package:pay/pay.dart';
//
// class GoogleButtonWidget extends StatefulWidget {
//   const GoogleButtonWidget({Key? key}) : super(key: key);
//
//   @override
//   _GoogleButtonWidgetState createState() => _GoogleButtonWidgetState();
// }
//
// class _GoogleButtonWidgetState extends State<GoogleButtonWidget> {
//   late final Future<PaymentConfiguration> _googlePayConfigFuture;
//
//   @override
//   void initState() {
//     super.initState();
//     _googlePayConfigFuture = PaymentConfiguration.fromAsset('images/default_google_pay_config.json');
//   }
//
//   void onGooglePayResult(paymentResult) {
//     debugPrint(paymentResult.toString());
//   }
//
//   void onApplePayResult(paymentResult) {
//     debugPrint(paymentResult.toString());
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     const paymentItems = [
//       PaymentItem(
//         label: 'Total',
//         amount: '99.99',
//         status: PaymentItemStatus.final_price,
//       )
//     ];
//     return Column(
//       children: <Widget>[
//         // Example pay button configured using an asset
//         FutureBuilder<PaymentConfiguration>(
//             future: _googlePayConfigFuture,
//             builder: (context, snapshot) => snapshot.hasData
//                 ? GooglePayButton(
//                     paymentConfiguration: snapshot.data!,
//                     paymentItems: paymentItems,
//                     type: GooglePayButtonType.buy,
//                     margin: const EdgeInsets.only(top: 15.0),
//                     onPaymentResult: onGooglePayResult,
//                     loadingIndicator: const Center(child: CircularProgressIndicator()),
//                   )
//                 : const SizedBox.shrink()),
//         // Example pay button configured using a string
//         // ApplePayButton(
//         //   paymentConfiguration: PaymentConfiguration.fromJsonString(payment_configurations.defaultApplePay),
//         //   paymentItems: _paymentItems,
//         //   style: ApplePayButtonStyle.black,
//         //   type: ApplePayButtonType.buy,
//         //   margin: const EdgeInsets.only(top: 15.0),
//         //   onPaymentResult: onApplePayResult,
//         //   loadingIndicator: const Center(
//         //     child: CircularProgressIndicator(),
//         //   ),
//         // ),
//       ],
//     );
//   }
// }
