import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../../../utils/constants/app_const.dart';
import '../../cubit/more_cubit.dart';

import '../../../../../config/router/router.dart';
import '../../../../../data/models/classes/master_class.dart';
import '../../../../widgets/error_state_widget.dart';
import '../../../../widgets/loading_state_widget.dart';
import '../../../../widgets/no_state_widget.dart';
import '../../../learn/master_classes/classes_widgets/class_card.dart';
import '../../../learn/master_classes/screens/class_screen.dart';

class SavedClassesTabBarView extends StatelessWidget {
  const SavedClassesTabBarView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MoreCubit, MoreState>(
      builder: (context, state) {
        final cubit = MoreCubit.get(context);
        if (state is GetSavedItemsLoading) {
          return const LoadingStateWidget();
        } else if (state is GetSavedItemsFailed) {
          return ErrorStateWidget(
            hasRefresh: true,
            onRefresh: () async => await cubit.getSavedClasses(),
          );
        } else {
          final classesList = cubit.savedNxtClasses!.nxtclasses ?? [];
          if (classesList.isEmpty) {
            return NoStateWidget();
          } else {
            return RefreshIndicator(
              onRefresh: () async => await cubit.getSavedClasses(),
              child: ListView.builder(
                itemCount: classesList.length,
                padding: EdgeInsets.only(top: height * 0.016, right: width * 0.03, left: width * 0.03),
                itemBuilder: (context, index) {
                  final masterClass = classesList[index]!;
                  return ClassCard(
                    isOnTapRequired: true,
                    color: Colors.transparent.value.toString(),
                    title: isEn
                        ? masterClass.lang![0]!.title ?? "notAvailable".tr()
                        : masterClass.lang![1]!.title ?? "notAvailable".tr(),
                    description: isEn
                        ? masterClass.lang![0]!.description ?? "notAvailable".tr()
                        : masterClass.lang![1]!.description ?? "notAvailable".tr(),
                    author: Author(
                      description: isEn
                          ? masterClass.author!.lang![0]!.description ?? "notAvailable".tr()
                          : masterClass.author!.lang![1]!.description ?? "notAvailable".tr(),
                      name: isEn
                          ? masterClass.author!.lang![0]!.name ?? "notAvailable".tr()
                          : masterClass.author!.lang![1]!.name ?? "notAvailable".tr(),
                      avatar: masterClass.author!.avatar,
                      nationality: isEn
                          ? masterClass.author!.lang![0]!.nationality ?? "notAvailable".tr()
                          : masterClass.author!.lang![1]!.nationality ?? "notAvailable".tr(),
                      position: isEn
                          ? masterClass.author!.lang![0]!.position ?? "notAvailable".tr()
                          : masterClass.author!.lang![1]!.position ?? "notAvailable".tr(),
                    ),
                    poster: masterClass.poster,
                    tags: masterClass.tags!
                        .map((tag) => Tags(id: tag!.id!, text: tag.text!, removed: tag.removed!))
                        .toList(),
                    updatedAt: masterClass.createdAt,
                    onTap: () => MagicRouter.navigateTo(ClassScreen(classId: masterClass.id!)),
                  );
                },
              ),
            );
          }
        }
      },
    );
  }
}
