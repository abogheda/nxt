import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../../../utils/constants/app_const.dart';
import '../../cubit/more_cubit.dart';

import '../../../../../config/router/router.dart';
import '../../../../../utils/constants/app_images.dart';
import '../../../../widgets/error_state_widget.dart';
import '../../../../widgets/loading_state_widget.dart';
import '../../../../widgets/no_state_widget.dart';
import '../../../challenges/cubit/challenge_cubit.dart';
import '../../../challenges/screens/view_challenge/view_challenge_screen.dart';
import '../../../challenges/widgets/challenge_card.dart';

class SavedChallengesTabBarView extends StatelessWidget {
  const SavedChallengesTabBarView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MoreCubit, MoreState>(
      builder: (context, state) {
        final cubit = MoreCubit.get(context);
        if (state is GetSavedItemsLoading || cubit.savedChallenges == null) return const LoadingStateWidget();
        if (state is GetSavedItemsFailed) {
          return ErrorStateWidget(
            hasRefresh: true,
            onRefresh: () async => await cubit.getSavedChallenges(),
          );
        }
        final challengesList = cubit.savedChallenges!.challenges ?? [];
        if (challengesList.isEmpty) return NoStateWidget();
        return RefreshIndicator(
          onRefresh: () async => await cubit.getSavedChallenges(),
          child: ListView.builder(
            itemCount: challengesList.length,
            padding: EdgeInsets.only(top: height * 0.016),
            itemBuilder: (context, index) {
              final challenge = challengesList[index]!;
              return ChallengeCard(
                image: challenge.media ?? placeHolderUrl,
                color: Colors.transparent.value,
                title: isEn
                    ? challenge.lang![0]!.title ?? "notAvailable".tr()
                    : challenge.lang![1]!.title ?? "notAvailable".tr(),
                supTitle: isEn
                    ? challenge.lang![0]!.supTitle ?? "notAvailable".tr()
                    : challenge.lang![1]!.supTitle ?? "notAvailable".tr(),
                padding: EdgeInsets.only(bottom: height * 0.016, left: width * 0.03, right: width * 0.03),
                onTap: () => MagicRouter.navigateTo(
                  BlocProvider.value(
                    value: ChallengeCubit()..onInit(challengeId: challenge.id!),
                    child: ViewChallengeScreen(challengeId: challenge.id!),
                  ),
                ),
              );
            },
          ),
        );
      },
    );
  }
}
