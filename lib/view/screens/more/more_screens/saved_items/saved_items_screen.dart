import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import '../../../../../utils/constants/app_const.dart';
import 'saved_news_tab_bar_view.dart';

import '../../../navigation_and_appbar/import_widget.dart';

import 'saved_challenges_tab_bar_view.dart';
import 'saved_jobs_tab_bar_view.dart';

class SavedItemsScreen extends StatefulWidget {
  const SavedItemsScreen({Key? key}) : super(key: key);

  @override
  State<SavedItemsScreen> createState() => _SavedItemsScreenState();
}

class _SavedItemsScreenState extends State<SavedItemsScreen> with SingleTickerProviderStateMixin {
  late TabController _tabController;
  List<Tab> tabTitleList = [
    Tab(child: Text('challenges'.tr())),
    // Tab(child: Text('classes'.tr())),
    Tab(child: Text('jobs'.tr())),
    Tab(child: Text('reads'.tr())),
  ];

  List<Widget> tabViewList = const [
    SavedChallengesTabBarView(),
    // SavedClassesTabBarView(),
    SavedJobsTabBarView(),
    SavedNewsTabBarView(),
  ];
  @override
  void initState() {
    _tabController = TabController(length: tabTitleList.length, vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    _tabController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return CustomScaffoldWidget(
      extendBodyBehindAppBar: false,
      isBackArrow: true,
      isThereLogo: true,
      isThereActions: true,
      body: Column(
        children: [
          Container(color: Colors.black, width: double.infinity, height: 1.5),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 24),
            child: Text('mySaved'.tr(),
                style: Theme.of(context).textTheme.headline3!.copyWith(
                      fontFamily: widget.isEn ? 'BebasNeue' : 'Tajawal',
                      color: Colors.black,
                    )),
          ),
          Container(
            height: widget.height * 0.06,
            color: Colors.black,
            child: TabBar(
              labelPadding: const EdgeInsets.all(4),
              physics: const NeverScrollableScrollPhysics(),
              controller: _tabController,
              labelColor: Colors.white,
              unselectedLabelColor: const Color(0xFF989898),
              labelStyle:
                  Theme.of(context).textTheme.headline6!.copyWith(fontFamily: AppConst.isEn ? 'BebasNeue' : 'Tajawal'),
              indicatorColor: Colors.black,
              tabs: tabTitleList,
            ),
          ),
          Expanded(
            child: TabBarView(
              controller: _tabController,
              children: tabViewList,
            ),
          )
        ],
      ),
    );
  }
}
