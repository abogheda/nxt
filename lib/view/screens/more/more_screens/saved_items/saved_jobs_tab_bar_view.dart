import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../../../utils/constants/app_const.dart';
import '../../cubit/more_cubit.dart';

import '../../../../widgets/error_state_widget.dart';
import '../../../../widgets/loading_state_widget.dart';
import '../../../../widgets/no_state_widget.dart';
import '../../../my_jobs/widgets/my_job_card.dart';

class SavedJobsTabBarView extends StatelessWidget {
  const SavedJobsTabBarView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MoreCubit, MoreState>(
      builder: (context, state) {
        final cubit = MoreCubit.get(context);
        if (state is GetSavedItemsLoading) return const LoadingStateWidget();
        if (state is GetSavedItemsFailed) {
          return ErrorStateWidget(hasRefresh: true, onRefresh: () async => await cubit.getSavedJobs());
        }
        final jobsList = cubit.savedJobs!.jobs ?? [];
        if (jobsList.isEmpty) return NoStateWidget();
        return RefreshIndicator(
          onRefresh: () async => await cubit.getSavedJobs(),
          child: ListView.builder(
            itemCount: jobsList.length,
            padding: EdgeInsets.only(top: height * 0.016, right: width * 0.03, left: width * 0.03),
            itemBuilder: (context, index) {
              final job = jobsList[index]!;
              return MyJobCard(
                id: job.id!,
                name: job.organization!.name,
                color: job.category!.color,
                logo: job.organization!.logo,
                title: isEn ? job.lang![0]!.title : job.lang![1]!.title,
                createdAt: job.createdAt,
              );
            },
          ),
        );
      },
    );
  }
}
