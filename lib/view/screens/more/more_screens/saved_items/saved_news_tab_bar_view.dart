import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../../../utils/constants/app_const.dart';
import '../../cubit/more_cubit.dart';

import '../../../../../config/router/router.dart';
import '../../../../../utils/constants/app_images.dart';
import '../../../../widgets/error_state_widget.dart';
import '../../../../widgets/loading_state_widget.dart';
import '../../../../widgets/no_state_widget.dart';
import '../../../learn/reads/read_screen.dart';
import '../../../learn/reads/reads_card.dart';

class SavedNewsTabBarView extends StatelessWidget {
  const SavedNewsTabBarView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MoreCubit, MoreState>(
      builder: (context, state) {
        final cubit = MoreCubit.get(context);
        if (state is GetSavedItemsLoading) return const LoadingStateWidget();
        if (state is GetSavedItemsFailed) {
          return ErrorStateWidget(
            hasRefresh: true,
            onRefresh: () async => await cubit.getSavedReads(),
          );
        }
        final readsList = cubit.savedReads!.reads ?? [];
        if (readsList.isEmpty) return NoStateWidget();
        return RefreshIndicator(
          onRefresh: () async => await cubit.getSavedReads(),
          child: ListView.builder(
            itemCount: readsList.length,
            padding: EdgeInsets.only(top: height * 0.016, right: width * 0.03, left: width * 0.03),
            itemBuilder: (context, index) {
              final readModel = readsList[index]!;
              return ReadsCard(
                onTap: () => MagicRouter.navigateTo(ReadScreen(readId: readModel.id!)),
                title: isEn
                    ? readModel.lang![0]!.title ?? "notAvailable".tr()
                    : readModel.lang![1]!.title ?? "notAvailable".tr(),
                description: isEn
                    ? readModel.lang![0]!.description ?? "notAvailable".tr()
                    : readModel.lang![1]!.description ?? "notAvailable".tr(),
                media: readModel.media ?? placeHolderUrl,
                tags: isEn ? readModel.lang![0]!.tags ?? [] : readModel.lang![1]!.tags ?? [],
                showDescription: false,
                showCategoryColor: false,
                titlePadding: EdgeInsetsDirectional.only(start: width * 0.025 + 3, end: width * 0.025 + 3),
                tagsPadding: EdgeInsetsDirectional.only(start: width * 0.025, end: width * 0.025),
              );
            },
          ),
        );
      },
    );
  }
}
