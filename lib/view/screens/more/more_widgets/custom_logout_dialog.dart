import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:nxt/view/screens/welcome_to_project/welcome_to_project_imports.dart';
import '../../../../config/router/router.dart';
import '../../../../utils/constants/app_const.dart';

import '../../../../data/service/hive_services.dart';
import '../../../../utils/constants/app_colors.dart';

void showLogoutDialog(BuildContext context) {
  showDialog(
    context: context,
    builder: (context) {
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          CustomLogOutDialog(
            onLogOutPressed: () async {
              await HiveHelper.logout();
              if (HiveHelper.getUserInfo?.user?.facebookId != null) {
                await FacebookAuth.instance.logOut().then((value) {});
              }
              if (HiveHelper.getUserInfo?.user?.googleId != null) {
                await GoogleSignIn().signOut().then((value) {});
              }
              MagicRouter.navigateAndPopAll(const WelcomeToProject());
            },
          ),
        ],
      );
    },
  );
}

class CustomLogOutDialog extends StatelessWidget {
  const CustomLogOutDialog({Key? key, required this.onLogOutPressed}) : super(key: key);
  final primaryColor = Colors.black;
  final accentColor = const Color(0xffffffff);
  final Function() onLogOutPressed;
  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
      child: Container(
        margin: const EdgeInsets.symmetric(vertical: 16, horizontal: 12),
        decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(15.0)),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              "logout".tr(),
              style: Theme.of(context).textTheme.titleLarge!.copyWith(
                    color: Colors.black,
                    fontFamily: isEn ? 'BebasNeue' : 'Tajawal',
                  ),
            ),
            const SizedBox(height: 3.5),
            Text(
              "logoutSure".tr(),
              style: Theme.of(context).textTheme.titleSmall!.copyWith(
                    color: AppColors.greyOutText,
                    fontFamily: isEn ? 'Montserrat' : 'Tajawal',
                  ),
            ),
            const SizedBox(height: 15),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                SimpleButton(
                  text: "cancel".tr(),
                  onPressed: () => MagicRouter.pop(),
                ),
                SimpleButton(
                  text: "logout".tr(),
                  onPressed: onLogOutPressed,
                  invertedColors: true,
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}

class SimpleButton extends StatelessWidget {
  final String text;
  final Function() onPressed;
  final bool invertedColors;
  const SimpleButton({required this.text, required this.onPressed, this.invertedColors = false, Key? key})
      : super(key: key);
  final primaryColor = Colors.black;
  final accentColor = const Color(0xffffffff);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
        style: ButtonStyle(
            elevation: MaterialStateProperty.all(0),
            alignment: Alignment.center,
            side: MaterialStateProperty.all(BorderSide(width: 1, color: primaryColor)),
            padding: MaterialStateProperty.all(const EdgeInsets.only(right: 25, left: 25, top: 0, bottom: 0)),
            backgroundColor: MaterialStateProperty.all(invertedColors ? accentColor : primaryColor),
            shape: MaterialStateProperty.all(
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
            )),
        onPressed: onPressed,
        child: Text(
          text,
          style: TextStyle(
            fontSize: 16,
            fontFamily: isEn ? 'BebasNeue' : 'Tajawal',
            color: invertedColors ? primaryColor : accentColor,
          ),
        ));
  }
}

class CustomAskPermissionDialog extends StatelessWidget {
  const CustomAskPermissionDialog({Key? key, required this.onPressed}) : super(key: key);
  final primaryColor = Colors.black;
  final Function() onPressed;
  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
      child: Container(
        margin: const EdgeInsets.symmetric(vertical: 16, horizontal: 12),
        decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(15.0)),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              "deniedPermission".tr(),
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.titleMedium!.copyWith(fontFamily: isEn ? 'Montserrat' : 'Tajawal'),
            ),
            const SizedBox(height: 15),
            SimpleButton(
              text: "appSettings".tr(),
              onPressed: onPressed,
              invertedColors: false,
            ),
          ],
        ),
      ),
    );
  }
}
