import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'legal_scrollable_screen.dart';

import 'cubit/more_cubit.dart';

class CommunityGuideLinesScreen extends StatelessWidget {
  const CommunityGuideLinesScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MoreCubit, MoreState>(
      builder: (context, state) {
        final legalModel = context.read<MoreCubit>().legalModel;
        return LegalScrollableScreen(
          legalIndex: 4,
          title: 'communityGuidelines'.tr(),
          subTitle: "read".tr(),
          onRefresh: () async => await context.read<MoreCubit>().getLegal(),
          loadingCondition: state is GetLegalLoading || legalModel == null,
          legalModel: legalModel,
        );
      },
    );
  }
}
