import 'dart:developer';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import '../../../data/models/more/legal_model.dart';
import '../../../utils/constants/app_const.dart';
import '../../widgets/app_loader.dart';
import '../navigation_and_appbar/import_widget.dart';

import '../../../../../utils/constants/app_colors.dart';
import '../../widgets/custom_html_widget.dart';

class LegalScrollableScreen extends StatelessWidget {
  const LegalScrollableScreen({
    Key? key,
    required this.title,
    required this.subTitle,
    required this.legalModel,
    this.loadingCondition,
    required this.onRefresh,
    required this.legalIndex,
    this.showActions = true,
    this.isLogoClickable = true,
  }) : super(key: key);
  final String title;
  final String subTitle;
  final LegalModel? legalModel;
  final bool? loadingCondition;
  final bool showActions;
  final bool isLogoClickable;
  final int legalIndex;
  final Future<void> Function() onRefresh;
  @override
  Widget build(BuildContext context) {
    String getLegalData() {
      String legalData = "notAvailable".tr();
      try {
        switch (legalIndex) {
          case 0:
            legalData = legalModel!.terms ?? "notAvailable".tr();
            break;
          case 1:
            legalData = legalModel!.privacy ?? "notAvailable".tr();
            break;
          case 2:
            legalData = legalModel!.paymentTerms ?? "notAvailable".tr();
            break;
          case 3:
            legalData = legalModel!.challengesTerms ?? "notAvailable".tr();
            break;
          case 4:
            legalData = legalModel!.communityGuidelines ?? "notAvailable".tr();
            break;
          case 5:
            legalData = legalModel!.eula ?? "notAvailable".tr();
            break;
          default:
        }
      } catch (e) {
        log('!=!=!=' * 22);
        log('LegalScrollableScreen.getLegalData');
        log(e.toString());
        log('!=!=!=' * 22);
        return legalData;
      }
      return legalData;
    }

    return CustomScaffoldWidget(
      extendBodyBehindAppBar: false,
      backgroundColor: Colors.white,
      isBackArrow: true,
      isThereLogo: true,
      isThereActions: showActions,
      isLogoClickable: isLogoClickable,
      body: Theme(
        data: Theme.of(context).copyWith(
            scrollbarTheme: ScrollbarThemeData(
                trackBorderColor: MaterialStateProperty.all(Colors.black),
                thumbColor: MaterialStateProperty.all(Colors.black))),
        child: Column(
          children: [
            Container(color: Colors.black, width: double.infinity, height: 1.5),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: width * 0.04, vertical: 24),
              child: Text(
                title,
                textAlign: TextAlign.center,
                style: Theme.of(context)
                    .textTheme
                    .headline3!
                    .copyWith(fontFamily: isEn ? 'BebasNeue' : 'Tajawal', color: Colors.black),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: width * 0.04),
              child: Text(
                subTitle,
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.bodyText2!.copyWith(
                      fontFamily: isEn ? 'Montserrat' : 'Tajawal',
                      color: AppColors.iconsColor,
                      fontWeight: FontWeight.bold,
                    ),
              ),
            ),
            const SizedBox(height: 24),
            loadingCondition == true
                ? Column(children: [SizedBox(height: height * 0.25), const AppLoader()])
                : Expanded(
                    child: RefreshIndicator(
                      onRefresh: onRefresh,
                      child: ListView(
                        padding: EdgeInsets.symmetric(horizontal: width * 0.01),
                        children: [CustomHtmlWidget(data: getLegalData())],
                      ),
                    ),
                  ),
          ],
        ),
      ),
    );
  }
}
