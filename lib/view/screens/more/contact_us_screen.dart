import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import '../../../utils/constants/app_const.dart';

import '../../../config/router/router.dart';
import '../../../utils/constants/app_colors.dart';
import '../../../utils/constants/app_text_style.dart';
import '../../widgets/custom_button.dart';
import '../../widgets/custom_text_field.dart';
import '../navigation_and_appbar/import_widget.dart';

class ContactUsScreen extends StatelessWidget {
  ContactUsScreen({Key? key}) : super(key: key);
  final formKey = GlobalKey<FormState>();
  final formKey2 = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return CustomScaffoldWidget(
      extendBodyBehindAppBar: false,
      backgroundColor: Colors.white,
      isBackArrow: true,
      isThereLogo: true,
      isThereActions: true,
      body: Stack(
        children: [
          Positioned(
            top: 0,
            bottom: 0,
            right: 0,
            left: 0,
            child: ListView(
              children: [
                Container(color: Colors.black, width: double.infinity, height: 1.5),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: width * 0.04, vertical: 24),
                  child: Text(
                    'getTouch'.tr(),
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.headline3!.copyWith(
                          fontFamily: isEn ? 'BebasNeue' : 'Tajawal',
                          color: Colors.black,
                        ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: width * 0.04),
                  child: Text(
                    'howCanHelp'.tr(),
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.bodyText2!.copyWith(
                          fontFamily: isEn ? 'Montserrat' : 'Tajawal',
                          color: AppColors.iconsColor,
                          fontWeight: FontWeight.bold,
                        ),
                  ),
                ),
                const SizedBox(height: 24),
                // Padding(
                //   padding: const EdgeInsets.symmetric(horizontal: 20),
                //   child: Form(
                //     key: formKey,
                //     child: CustomTextField(
                //       hint: 'login.email'.tr(),
                //       validator: Validators.email,
                //       prefix: const Icon(
                //         Icons.email_outlined,
                //         color: AppColors.iconsColor,
                //       ),
                //     ),
                //   ),
                // ),
                // const SizedBox(height: 25),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: Form(
                    key: formKey2,
                    child: CustomTextField(
                      validator: (value) {
                        if (value!.isEmpty) {
                          return ('Please provide your issue / suggestion');
                        }
                        if (value.length < 20) {
                          return ("Minimum 20 Characters in order to help !!");
                        } else {
                          return null;
                        }
                      },
                      hint: 'Your Message...',
                      lines: 12,
                    ),
                  ),
                ),
              ],
            ),
          ),
          Positioned(
            right: 0,
            left: 0,
            bottom: 0,
            child: Container(
              color: Colors.black,
              child: CustomButton(
                height: height * 0.08,
                width: double.infinity,
                onTap: () {
                  if (formKey.currentState!.validate() && formKey2.currentState!.validate()) {
                    return reportDialog(context);
                  }
                },
                child: Text(
                  'sendNow'.tr().toUpperCase(),
                  style: TextStyle(
                    fontSize: 18,
                    color: Colors.white,
                    fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal',
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  void reportDialog(context) {
    showDialog(
      context: context,
      builder: (context) {
        return Material(
          color: Colors.transparent,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                padding: EdgeInsets.only(right: width * 0.06, left: width * 0.06, bottom: 20, top: 10),
                margin: EdgeInsets.symmetric(horizontal: width * 0.03, vertical: 20),
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.black),
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(8)),
                child: Column(
                  children: [
                    Align(
                      alignment: Alignment.topRight,
                      child: GestureDetector(
                        onTap: () => Navigator.of(context).pop(),
                        child: const Icon(Icons.clear_outlined, size: 50, color: Colors.black),
                      ),
                    ),
                    Container(
                      height: 45,
                      width: 45,
                      decoration: BoxDecoration(color: Colors.black, borderRadius: BorderRadius.circular(80)),
                      child: const Icon(
                        Icons.done,
                        size: 30,
                        color: Colors.white,
                      ),
                    ),
                    const SizedBox(height: 25),
                    Text('ApplySuccess'.tr().toUpperCase(), style: AppTextStyles.huge36),
                    const SizedBox(height: 10),
                    Text(
                      "thanksTouch".tr(),
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: 13,
                          fontFamily: isEn ? 'Montserrat' : 'Tajawal',
                          fontWeight: FontWeight.bold,
                          color: AppColors.iconsColor),
                    ),
                    const SizedBox(height: 15),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16),
                      child: MaterialButton(
                        height: 42,
                        minWidth: double.infinity,
                        shape: RoundedRectangleBorder(
                          side: const BorderSide(color: Colors.black, width: 1),
                          borderRadius: BorderRadius.circular(4),
                        ),
                        onPressed: () {
                          MagicRouter.navigateAndPopUntilFirstPage(Navigation());
                        },
                        child: Text(
                          'home'.tr().toUpperCase(),
                          style: TextStyle(
                            fontSize: 13,
                            fontFamily: isEn ? 'Montserrat' : 'Tajawal',
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
