import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import '../../../utils/constants/resources.dart';
import '../../../data/service/remote_service.dart';
import '../../../utils/helpers/utils.dart';
import '../../widgets/logo_app_bar.dart';
import '../login/login_screen.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import '../../../config/router/router.dart';
import '../../../utils/constants/app_const.dart';
import '../../../../data/service/hive_services.dart';

class InvitationEndScreen extends StatelessWidget {
  const InvitationEndScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          const LogoAppBar(),
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: width * 0.03),
                  child: Text(
                    'invitationEnded'.tr(),
                    textAlign: TextAlign.center,
                    style: Theme.of(context)
                        .textTheme
                        .headline4!
                        .copyWith(color: Colors.black, fontFamily: isEn ? 'BebasNeue' : 'Tajawal'),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: width * 0.03),
                  child: Text(
                    '${'invitationEndDate'.tr()}: ${HiveHelper.getUserInfo!.user!.invitationExpiryDate == null ? DateFormat("dd/MM/yyyy").format(DateTime.now()) : DateFormat("dd/MM/yyyy").format(DateTime.parse(HiveHelper.getUserInfo!.user!.invitationExpiryDate!))}',
                    textAlign: TextAlign.center,
                    style: Theme.of(context)
                        .textTheme
                        .headline4!
                        .copyWith(color: Colors.black, fontFamily: isEn ? 'BebasNeue' : 'Tajawal'),
                  ),
                ),
                const SizedBox(height: 24.0),
                SizedBox(
                  width: width * 0.6,
                  height: 48,
                  child: ElevatedButton(
                    child: Text(
                      'goToWebsite'.tr(),
                      style: Theme.of(context).textTheme.titleMedium!.copyWith(
                          fontFamily: isEn ? 'Montserrat' : 'Tajawal',
                          fontWeight: FontWeight.bold,
                          color: Colors.white),
                    ),
                    onPressed: () async => await Utils.launchAppUrl(
                        url: AppConst.isProduction ? AppConst.productionUrl : AppConst.developmentUrl),
                  ),
                ),
                const SizedBox(height: 12),
                SizedBox(
                  width: width * 0.6,
                  height: 48,
                  child: ElevatedButton(
                    child: Text(
                      'logout'.tr(),
                      style: Theme.of(context).textTheme.titleMedium!.copyWith(
                            fontFamily: isEn ? 'Montserrat' : 'Tajawal',
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                    ),
                    onPressed: () async {
                      await HiveHelper.logout().then((_) async {
                        if (APIService.dio != null) {
                          if (HiveHelper.getUserInfo?.user?.facebookId != null) {
                            await FacebookAuth.instance.logOut().then((value) {});
                          }
                          if (HiveHelper.getUserInfo?.user?.googleId != null) {
                            await GoogleSignIn().signOut().then((value) {});
                          }
                        }
                        MagicRouter.navigateAndPopAll(const LoginScreen());
                      });
                    },
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
