// ignore_for_file: deprecated_member_use

import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nxt/utils/helpers/popup_helper.dart';
import '../../../utils/constants/resources.dart';
import '../../../utils/helpers/utils.dart';
import '../../widgets/app_loader.dart';
import '../../widgets/logo_app_bar.dart';
import '../home/cubit/home_cubit.dart';
import '../login/login_screen.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import '../../../config/router/router.dart';
import '../../../utils/constants/app_const.dart';
import '../../../data/service/remote_service.dart';

import '../../../../data/service/hive_services.dart';
import '../navigation_and_appbar/import_widget.dart';

class SubscribeScreen extends StatelessWidget {
  const SubscribeScreen({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          const LogoAppBar(),
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 36, horizontal: width * 0.03),
                  child: Text(
                    'notSubscribed'.tr(),
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.headline4!.copyWith(
                          fontFamily: isEn ? 'BebasNeue' : 'Tajawal',
                          color: Colors.black,
                        ),
                  ),
                ),
                const SizedBox(height: 16.0),
                SizedBox(
                  width: width * 0.6,
                  height: 48,
                  child: ElevatedButton(
                    child: Text(
                      'goToWebsite'.tr(),
                      style: Theme.of(context).textTheme.titleMedium!.copyWith(
                            fontFamily: isEn ? 'Montserrat' : 'Tajawal',
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                    ),
                    onPressed: () async => await Utils.launchAppUrl(
                        url: AppConst.isProduction
                            ? "${AppConst.productionUrl}/upgrade-package/"
                            : "${AppConst.developmentUrl}/upgrade-package/"),
                  ),
                ),
                const SizedBox(height: 12),
                SizedBox(
                  width: width * 0.6,
                  height: 48,
                  child: ElevatedButton(
                    child: Text(
                      'logout'.tr(),
                      style: Theme.of(context).textTheme.titleMedium!.copyWith(
                            fontFamily: isEn ? 'Montserrat' : 'Tajawal',
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                    ),
                    onPressed: () async {
                      await HiveHelper.logout().then((_) async {
                        if (APIService.dio != null) {
                          if (HiveHelper.getUserInfo?.user?.facebookId != null) {
                            await FacebookAuth.instance.logOut().then((value) {});
                          }
                          if (HiveHelper.getUserInfo?.user?.googleId != null) {
                            await GoogleSignIn().signOut().then((value) {});
                          }
                        }
                        MagicRouter.navigateAndPopAll(const LoginScreen());
                      });
                    },
                  ),
                ),
                const SizedBox(height: 12),
                SizedBox(
                  width: width * 0.6,
                  height: 48,
                  child: BlocConsumer<HomeCubit, HomeState>(
                    listener: (context, state) {
                      if (state is HomeProfileFailed) {
                        PopupHelper.showBasicSnack(msg: state.message ?? 'normalErrorMsg'.tr(), color: Colors.red);
                      }
                    },
                    builder: (context, state) {
                      return OutlinedButton(
                        child: state is HomeProfileLoading
                            ? const Center(child: AppLoader())
                            : Text(
                                'restoreSubscription'.tr(),
                                style: Theme.of(context)
                                    .textTheme
                                    .titleMedium!
                                    .copyWith(fontWeight: FontWeight.bold, fontFamily: isEn ? 'Montserrat' : 'Tajawal'),
                              ),
                        onPressed: () async {
                          final cubit = HomeCubit.get(context);
                          await cubit.getUserDataValidate();
                          await cubit.getAllHomeData();
                          MagicRouter.navigateAndPopAll(Navigation());
                        },
                      );
                    },
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
