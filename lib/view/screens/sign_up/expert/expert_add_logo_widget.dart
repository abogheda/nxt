import 'package:dotted_border/dotted_border.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

import '../../../../data/service/hive_services.dart';
import '../../../../utils/constants/app_colors.dart';
import '../../../../utils/constants/app_const.dart';
import 'controller/expert_sign_up_cubit.dart';

class ExpertAddLogoWidget extends StatelessWidget {
  const ExpertAddLogoWidget({Key? key, required this.cubit}) : super(key: key);
  final ExpertSignUpCubit cubit;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: width * 0.06),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          DottedBorder(
            borderType: BorderType.RRect,
            radius: const Radius.circular(6),
            dashPattern: const [12, 12],
            color: AppColors.iconsColor,
            child: SizedBox(
              height: height * 0.16,
              width: width * 0.27,
              child: cubit.expertLogoImage == null
                  ? IconButton(
                      padding: EdgeInsets.zero,
                      onPressed: () async {
                        await cubit.pickImageForProfile();
                      },
                      icon: HiveHelper.getUserAvatar == '' || HiveHelper.getUserAvatar == null
                          ? const Icon(Icons.add, color: Colors.black)
                          : SizedBox.expand(
                              child: Image.network(HiveHelper.getUserAvatar.toString(), fit: BoxFit.cover)),
                    )
                  : ClipRRect(
                      borderRadius: const BorderRadius.all(Radius.circular(6.0)),
                      child: Stack(
                        children: [
                          SizedBox.expand(
                            child: InkWell(
                              radius: 6,
                              onTap: () async => await cubit.pickImageForProfile(),
                              child: Image.file(cubit.expertLogoImage!, fit: BoxFit.cover),
                            ),
                          ),
                          Align(
                            alignment: Alignment.topRight,
                            child: IconButton(
                              padding: EdgeInsets.zero,
                              constraints: const BoxConstraints(),
                              onPressed: () => cubit.deleteExpertProfileImage(),
                              icon: DecoratedBox(
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  border: Border.all(color: Colors.red),
                                ),
                                child: const Padding(
                                  padding: EdgeInsets.all(2),
                                  child: Icon(Icons.clear_rounded, color: Colors.red, size: 12),
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
            ),
          ),
          SizedBox(width: width * 0.03),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                'addYourLogo'.tr(),
                style: Theme.of(context).textTheme.titleMedium!.copyWith(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal',
                    ),
              ),
              SizedBox(height: height * 0.01),
              Text(
                'png'.tr(),
                style: TextStyle(
                  fontSize: 12,
                  color: AppColors.greyOutText,
                  fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal',
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
