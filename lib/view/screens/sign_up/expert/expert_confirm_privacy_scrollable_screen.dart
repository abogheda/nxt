import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import '../../../../config/router/router.dart';
import '../../../../utils/constants/app_const.dart';
import '../../../../utils/helpers/popup_helper.dart';
import '../../../../utils/constants/app_colors.dart';
import '../../../widgets/app_loader.dart';
import '../../../widgets/custom_html_widget.dart';
import 'controller/expert_sign_up_cubit.dart';
import 'expert_confirm_eula_scrollable_screen.dart';

class ExpertConfirmPrivacyScrollableScreen extends StatelessWidget {
  const ExpertConfirmPrivacyScrollableScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ExpertSignUpCubit, ExpertSignUpState>(
      builder: (context, state) {
        final cubit = ExpertSignUpCubit.get(context);
        return Scaffold(
          body: Column(
            children: [
              SizedBox(height: MediaQuery.of(context).padding.top + height * 0.05),
              Center(child: SvgPicture.asset('assets/images/fab_logo.svg', height: height * 0.09)),
              SizedBox(height: height * 0.035),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: width * 0.04),
                child: Text(
                  'privacy'.tr(),
                  textAlign: TextAlign.center,
                  style: Theme.of(context)
                      .textTheme
                      .headline3!
                      .copyWith(fontFamily: isEn ? 'BebasNeue' : 'Tajawal', color: Colors.black),
                ),
              ),
              SizedBox(height: height * 0.006),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: width * 0.04),
                child: Text(
                  'read'.tr(),
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.bodyText2!.copyWith(
                        fontFamily: isEn ? 'Montserrat' : 'Tajawal',
                        color: AppColors.iconsColor,
                        fontWeight: FontWeight.bold,
                      ),
                ),
              ),
              SizedBox(height: height * 0.05),
              Expanded(
                child: RefreshIndicator(
                  onRefresh: () async => await cubit.getLegal(),
                  child: ListView(
                    padding: EdgeInsets.symmetric(horizontal: width * 0.03),
                    children: [
                      if (state is GetLegalLoadingState)
                        Column(children: [SizedBox(height: height * 0.2), const AppLoader()])
                      else
                        CustomHtmlWidget(data: cubit.legalModel!.privacy)
                    ],
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: width * 0.03),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Checkbox(
                      value: cubit.isPrivacyChecked,
                      materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                      onChanged: (value) async {
                        cubit.isPrivacyChecked = value!;
                        cubit.changePrivacyCheckBox();
                      },
                    ),
                    Text(
                      'agreePrivacy'.tr(),
                      softWrap: true,
                      style: Theme.of(context).textTheme.bodySmall!.copyWith(
                            fontFamily: isEn ? 'Montserrat' : 'Tajawal',
                            fontWeight: FontWeight.bold,
                            color: const Color(0xFF727272),
                          ),
                    ),
                  ],
                ),
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: ElevatedButton(
                  onPressed: cubit.isPrivacyChecked
                      ? () async {
                          await cubit.confirmPrivacy();
                          await cubit.userValidateAndCacheUserInfo();
                          MagicRouter.navigateTo(
                              BlocProvider.value(value: cubit, child: const ExpertConfirmEULAScrollableScreen()));
                        }
                      : () => PopupHelper.showBasicSnack(msg: 'makeSurePrivacy'.tr(), color: Colors.red),
                  style: ElevatedButton.styleFrom(shape: const RoundedRectangleBorder(borderRadius: BorderRadius.zero)),
                  child: SizedBox(
                    height: height * 0.08,
                    child: Center(
                      child: state is ConfirmPrivacyLoadingState || state is ExpertValidateAndCacheLoadingState
                          ? const AppLoader(color: Colors.white)
                          : Text(
                              'confirm'.tr().toUpperCase(),
                              style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.w900,
                                fontFamily: isEn ? 'Montserrat' : 'Tajawal',
                              ),
                            ),
                    ),
                  ),
                ),
              )
            ],
          ),
        );
      },
    );
  }
}
