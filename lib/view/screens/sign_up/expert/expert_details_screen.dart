import 'dart:io';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:nxt/view/widgets/app_loader.dart';
import '../../../../utils/constants/app_const.dart';
import '../../../../config/router/router.dart';
import '../../../../utils/constants/app_colors.dart';
import '../../../../utils/helpers/popup_helper.dart';
import '../../../../utils/helpers/validators.dart';
import '../../../widgets/custom_text_field.dart';
import 'controller/expert_sign_up_cubit.dart';
import 'expert_add_logo_widget.dart';
import 'preview_expert_screen.dart';

class ExpertDetailsScreen extends StatelessWidget {
  const ExpertDetailsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: Platform.isAndroid ? false : true,
      body: BlocConsumer<ExpertSignUpCubit, ExpertSignUpState>(
        listener: (context, state) async {
          final cubit = ExpertSignUpCubit.get(context);
          if (state is ExpertPostInfoErrorState) {
            PopupHelper.showBasicSnack(msg: state.error.toString(), color: Colors.red);
            return;
          }
          if (state is UploadProfileImageFailed) {
            PopupHelper.showBasicSnack(msg: state.msg.toString(), color: Colors.red);
            return;
          }
          if (state is ExpertPostInfoSuccessState) {
            MagicRouter.navigateTo(
              BlocProvider.value(value: cubit, child: const PreviewExpertScreen()),
            );
          }
        },
        builder: (context, state) {
          final cubit = ExpertSignUpCubit.get(context);
          return Stack(
            children: [
              ListView(
                children: [
                  SizedBox(height: MediaQuery.of(context).padding.top),
                  Center(child: SvgPicture.asset('assets/images/fab_logo.svg', height: height * 0.09)),
                  SizedBox(height: height * 0.04),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: width * 0.03),
                    child: Text('enterYourOrganizationInformation'.tr(),
                        textAlign: TextAlign.center,
                        style: Theme.of(context)
                            .textTheme
                            .headlineMedium!
                            .copyWith(fontFamily: isEn ? 'BebasNeue' : 'Tajawal', color: Colors.black)),
                  ),
                  SizedBox(height: height * 0.03),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: width * 0.05),
                    child: Form(
                      key: cubit.expertDetailsFormKey,
                      child: Column(
                        children: [
                          CustomTextField(
                            hint: 'expertName'.tr(),
                            type: TextInputType.name,
                            validator: Validators.generalField,
                            controller: cubit.expertNameController,
                            prefix: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: SvgPicture.asset('assets/images/profile_icon.svg', color: AppColors.iconsColor),
                            ),
                            onFieldSubmitted: (val) => FocusScope.of(context).requestFocus(cubit.expertLinkFocusNode),
                          ),
                          SizedBox(height: height * 0.015),
                          CustomTextField(
                            type: TextInputType.url,
                            validator: Validators.generalField,
                            hint: 'organizationLink'.tr(),
                            focusNode: cubit.expertLinkFocusNode,
                            controller: cubit.expertLinkController,
                            prefix: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: SvgPicture.asset('assets/images/world_icon.svg', color: AppColors.iconsColor),
                            ),
                            onFieldSubmitted: (val) => FocusScope.of(context).requestFocus(cubit.expertScopeFocusNode),
                          ),
                          SizedBox(height: height * 0.015),
                          CustomTextField(
                            hint: 'scope'.tr(),
                            type: TextInputType.name,
                            validator: Validators.generalField,
                            focusNode: cubit.expertScopeFocusNode,
                            controller: cubit.expertScopeController,
                            prefix: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: SvgPicture.asset('assets/images/light_icon.svg', color: AppColors.iconsColor),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(height: height * 0.02),
                  ExpertAddLogoWidget(cubit: cubit),
                  SizedBox(height: height * 0.02),
                ],
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: ElevatedButton(
                  onPressed: () async {
                    FocusScope.of(context).requestFocus(FocusNode());
                    if (cubit.expertDetailsFormKey.currentState!.validate()) {
                      String pattern =
                          r'[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@%_\+.~#?&//=]*)';
                      RegExp regExp = RegExp(pattern);
                      if (!regExp.hasMatch(cubit.expertLinkController.text)) {
                        PopupHelper.showBasicSnack(msg: 'enterValidUrl'.tr());
                        return;
                      }
                      if (cubit.expertLogoImage == null) {
                        PopupHelper.showBasicSnack(msg: 'pleaseMakeSureImage'.tr());
                        return;
                      }
                      await cubit.addExpertInfo();
                    }
                  },
                  style: ElevatedButton.styleFrom(shape: const RoundedRectangleBorder(borderRadius: BorderRadius.zero)),
                  child: SizedBox(
                    height: height * 0.08,
                    child: Center(
                      child: state is ExpertPostInfoLoadingState || state is UploadExpertLogoLoading
                          ? const AppLoader(color: Colors.white)
                          : Text(
                              'createAccount'.tr().toUpperCase(),
                              style: TextStyle(
                                fontSize: 18,
                                color: Colors.white,
                                fontWeight: FontWeight.w900,
                                fontFamily: isEn ? 'Montserrat' : 'Tajawal',
                              ),
                            ),
                    ),
                  ),
                ),
              )
            ],
          );
        },
      ),
    );
  }
}
