import 'dart:io';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:nxt/view/widgets/app_loader.dart';
import '../../../../data/service/hive_services.dart';
import '../../../../utils/constants/app_const.dart';
import '../../../../config/router/router.dart';
import '../../../../utils/constants/app_colors.dart';
import '../../../../utils/helpers/popup_helper.dart';
import '../../../../utils/helpers/validators.dart';
import '../../../widgets/custom_text_field.dart';
import '../../production_house_profile/controller/organization_profile_cubit.dart';
import 'controller/expert_sign_up_cubit.dart';
import 'expert_add_logo_widget.dart';

class EditExpertDetailsScreen extends StatefulWidget {
  const EditExpertDetailsScreen({Key? key, required this.organizationCubit}) : super(key: key);
  final OrganizationProfileCubit organizationCubit;
  @override
  State<EditExpertDetailsScreen> createState() => _EditExpertDetailsScreenState();
}

class _EditExpertDetailsScreenState extends State<EditExpertDetailsScreen> {
  @override
  void initState() {
    final cubit = context.read<ExpertSignUpCubit>();
    if (widget.organizationCubit.expertProfileModel!.expert != null) {
      cubit.expertNameController =
          TextEditingController(text: widget.organizationCubit.expertProfileModel!.expert!.name!);
      cubit.expertLinkController =
          TextEditingController(text: widget.organizationCubit.expertProfileModel!.expert!.website!);
      cubit.expertScopeController =
          TextEditingController(text: widget.organizationCubit.expertProfileModel!.expert!.scope!);
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: Platform.isAndroid ? false : true,
      body: BlocConsumer<ExpertSignUpCubit, ExpertSignUpState>(
        listener: (context, state) async {
          if (state is ExpertPostInfoErrorState) {
            PopupHelper.showBasicSnack(msg: state.error.toString(), color: Colors.red);
            return;
          }
          if (state is UploadProfileImageFailed) {
            PopupHelper.showBasicSnack(msg: state.msg.toString(), color: Colors.red);
            return;
          }
          if (state is ExpertPostInfoSuccessState) {
            if (state.user.expert != null) {
              await HiveHelper.cacheProfileInfo(
                name: state.user.expert!.name,
                avatar: state.user.expert!.logo,
                birthDate: state.user.updatedAt,
              );
            }
            MagicRouter.pop();
            await widget.organizationCubit.onInit();
          }
        },
        builder: (context, state) {
          final cubit = ExpertSignUpCubit.get(context);
          return Stack(
            children: [
              ListView(
                children: [
                  SizedBox(height: MediaQuery.of(context).padding.top),
                  Center(child: SvgPicture.asset('assets/images/fab_logo.svg', height: widget.height * 0.09)),
                  SizedBox(height: widget.height * 0.04),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: widget.width * 0.03),
                    child: Text('enterYourOrganizationInformation'.tr(),
                        textAlign: TextAlign.center,
                        style: Theme.of(context)
                            .textTheme
                            .headlineMedium!
                            .copyWith(fontFamily: widget.isEn ? 'BebasNeue' : 'Tajawal', color: Colors.black)),
                  ),
                  SizedBox(height: widget.height * 0.03),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: widget.width * 0.05),
                    child: Form(
                      key: cubit.expertDetailsFormKey,
                      child: Column(
                        children: [
                          CustomTextField(
                            hint: 'expertName'.tr(),
                            type: TextInputType.name,
                            validator: Validators.generalField,
                            controller: cubit.expertNameController,
                            prefix: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: SvgPicture.asset('assets/images/profile_icon.svg', color: AppColors.iconsColor),
                            ),
                            onFieldSubmitted: (val) => FocusScope.of(context).requestFocus(cubit.expertLinkFocusNode),
                          ),
                          SizedBox(height: widget.height * 0.015),
                          CustomTextField(
                            type: TextInputType.url,
                            validator: Validators.generalField,
                            hint: 'organizationLink'.tr(),
                            focusNode: cubit.expertLinkFocusNode,
                            controller: cubit.expertLinkController,
                            prefix: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: SvgPicture.asset('assets/images/world_icon.svg', color: AppColors.iconsColor),
                            ),
                            onFieldSubmitted: (val) => FocusScope.of(context).requestFocus(cubit.expertScopeFocusNode),
                          ),
                          SizedBox(height: widget.height * 0.015),
                          CustomTextField(
                            hint: 'scope'.tr(),
                            type: TextInputType.text,
                            validator: Validators.generalField,
                            focusNode: cubit.expertScopeFocusNode,
                            controller: cubit.expertScopeController,
                            prefix: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: SvgPicture.asset('assets/images/light_icon.svg', color: AppColors.iconsColor),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(height: widget.height * 0.02),
                  ExpertAddLogoWidget(cubit: cubit),
                  SizedBox(height: widget.height * 0.02),
                ],
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: ElevatedButton(
                  onPressed: () async {
                    FocusScope.of(context).requestFocus(FocusNode());
                    if (cubit.expertDetailsFormKey.currentState!.validate()) {
                      String pattern =
                          r'[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@%_\+.~#?&//=]*)';
                      RegExp regExp = RegExp(pattern);
                      if (!regExp.hasMatch(cubit.expertLinkController.text)) {
                        PopupHelper.showBasicSnack(msg: 'enterValidUrl'.tr());
                        return;
                      }
                      if (cubit.expertLogoImage == null &&
                          (HiveHelper.getUserAvatar == '' || HiveHelper.getUserAvatar == null)) {
                        PopupHelper.showBasicSnack(msg: 'pleaseMakeSureImage'.tr());
                        return;
                      }
                      await cubit.editExpertInfo();
                    }
                  },
                  style: ElevatedButton.styleFrom(shape: const RoundedRectangleBorder(borderRadius: BorderRadius.zero)),
                  child: SizedBox(
                    height: widget.height * 0.08,
                    child: Center(
                      child: state is ExpertPostInfoLoadingState || state is UploadExpertLogoLoading
                          ? const AppLoader(color: Colors.white)
                          : Text(
                              'update'.tr().toUpperCase(),
                              style: TextStyle(
                                fontSize: 18,
                                color: Colors.white,
                                fontWeight: FontWeight.w900,
                                fontFamily: widget.isEn ? 'Montserrat' : 'Tajawal',
                              ),
                            ),
                    ),
                  ),
                ),
              )
            ],
          );
        },
      ),
    );
  }
}
