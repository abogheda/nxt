import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../../config/router/router.dart';
import '../../../../utils/constants/app_colors.dart';
import '../../../../utils/constants/app_const.dart';

import '../../../widgets/logo_app_bar.dart';
import '../../congratulation/congratulation_screen.dart';
import 'controller/expert_sign_up_cubit.dart';

class PreviewExpertScreen extends StatelessWidget {
  const PreviewExpertScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<ExpertSignUpCubit, ExpertSignUpState>(
      listener: (context, state) {},
      builder: (context, state) {
        final cubit = ExpertSignUpCubit.get(context);
        return Scaffold(
          body: ListView(
            shrinkWrap: true,
            children: [
              const LogoAppBar(),
              SizedBox(height: height * 0.03),
              Card(
                elevation: 0,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8.0), side: const BorderSide(color: Color(0xFFC6C6C6))),
                margin: EdgeInsets.symmetric(vertical: 8, horizontal: width * 0.04),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(height: height * 0.03),
                    SizedBox(
                      height: width * 0.2,
                      width: width * 0.2,
                      child: Image.file(
                        cubit.expertLogoImage!,
                        fit: BoxFit.cover,
                      ),
                    ),
                    SizedBox(height: height * 0.03),
                    Text(
                      'expertName'.tr(),
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.titleSmall!.copyWith(
                            color: AppColors.greyTxtColor,
                            fontFamily: isEn ? 'Montserrat' : 'Tajawal',
                          ),
                    ),
                    SizedBox(height: height * 0.01),
                    Text(
                      cubit.expertNameController.value.text,
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.headlineSmall!.copyWith(
                            fontFamily: isEn ? 'BebasNeue' : 'Tajawal',
                          ),
                    ),
                    SizedBox(height: height * 0.03),
                    Text(
                      'organizationLink'.tr(),
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.titleSmall!.copyWith(
                            color: AppColors.greyTxtColor,
                            fontFamily: isEn ? 'Montserrat' : 'Tajawal',
                          ),
                    ),
                    SizedBox(height: height * 0.01),
                    Text(
                      cubit.expertLinkController.value.text,
                      textAlign: TextAlign.center,
                      style:
                          Theme.of(context).textTheme.titleLarge!.copyWith(fontFamily: isEn ? 'BebasNeue' : 'Tajawal'),
                    ),
                    SizedBox(height: height * 0.1),
                    SizedBox(
                      width: width * 0.8,
                      height: height * 0.055,
                      child: OutlinedButton(
                        onPressed: () {
                          cubit.changeIsEditClicked();
                          MagicRouter.pop();
                        },
                        style: OutlinedButton.styleFrom(side: const BorderSide(color: Colors.black)),
                        child: Text(
                          'editData'.tr().toUpperCase(),
                          style: Theme.of(context).textTheme.titleSmall!.copyWith(
                                fontWeight: FontWeight.bold,
                                fontFamily: isEn ? 'Montserrat' : 'Tajawal',
                              ),
                        ),
                      ),
                    ),
                    SizedBox(height: height * 0.015),
                    SizedBox(
                      width: width * 0.8,
                      height: height * 0.055,
                      child: ElevatedButton(
                        onPressed: () async {
                          await cubit.expertLogin();
                          await MagicRouter.navigateAndPopAll(const CongratulationScreen());
                        },
                        child: Text(
                          'confirmPage'.tr().toUpperCase(),
                          style: Theme.of(context).textTheme.titleSmall!.copyWith(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontFamily: isEn ? 'Montserrat' : 'Tajawal',
                              ),
                        ),
                      ),
                    ),
                    SizedBox(height: height * 0.07),
                  ],
                ),
              )
            ],
          ),
        );
      },
    );
  }
}
