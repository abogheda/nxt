import 'dart:io';

import 'package:easy_localization/easy_localization.dart' as local;
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import '../../../../utils/constants/resources.dart';

import '../../../../config/router/router.dart';
import '../../../../utils/helpers/popup_helper.dart';
import '../../../../utils/helpers/validators.dart';
import '../../../widgets/app_loader.dart';
import '../../../widgets/custom_button.dart';
import '../../../widgets/custom_text_field.dart';
import '../talent/custom_intl_phone_field.dart';
import 'controller/expert_sign_up_cubit.dart';
import 'expert_verification_screen.dart';

class ExpertSignUpScreen extends StatelessWidget {
  const ExpertSignUpScreen({Key? key}) : super(key: key);
  Future<void> validateAndSignUp({required ExpertSignUpCubit cubit}) async {
    cubit.expertSignUpFormKey.currentState!.save();
    FocusManager.instance.primaryFocus?.unfocus();
    if (cubit.expertSignUpFormKey.currentState!.validate()) {
      if (cubit.expertPasswordController.text != cubit.expertConfirmPasswordController.text) {
        PopupHelper.showBasicSnack(msg: 'reset_password.make_sure'.tr(), color: Colors.red);
        return;
      }
      await cubit.expertSignUp();
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => ExpertSignUpCubit(),
      child: Scaffold(
        resizeToAvoidBottomInset: Platform.isAndroid ? false : true,
        body: BlocConsumer<ExpertSignUpCubit, ExpertSignUpState>(
          listener: (context, state) async {
            final cubit = ExpertSignUpCubit.get(context);
            if (state is ExpertSignUpErrorState) {
              PopupHelper.showBasicSnack(msg: state.error.toString(), color: Colors.red);
            }
            if (state is ExpertSignUpSuccessState) {
              if (state.signUpModel.token == null || state.signUpModel.token == '') {
                PopupHelper.showBasicSnack(msg: state.signUpModel.message.toString(), color: Colors.red);
                return;
              }
              if (state.signUpModel.token != null) {
                await cubit.sendCodeAndStartTimer();
                MagicRouter.navigateTo(BlocProvider.value(value: cubit, child: const ExpertVerificationScreen()));
              }
            }
          },
          builder: (context, state) {
            final cubit = ExpertSignUpCubit.get(context);
            return ListView(
              children: [
                SizedBox(height: MediaQuery.of(context).padding.top),
                Center(child: SvgPicture.asset('assets/images/fab_logo.svg', height: height * 0.09)),
                SizedBox(height: height * 0.05),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: width * 0.3),
                  child: FittedBox(
                    child: Text('hello'.tr(),
                        textAlign: TextAlign.center,
                        style: Theme.of(context)
                            .textTheme
                            .displayLarge!
                            .copyWith(fontFamily: isEn ? 'BebasNeue' : 'Tajawal', color: Colors.black)),
                  ),
                ),
                SizedBox(height: height * 0.03),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: width * 0.05),
                  child: Form(
                    key: cubit.expertSignUpFormKey,
                    child: Column(
                      children: [
                        Directionality(
                          textDirection: TextDirection.ltr,
                          child: CustomTextField(
                            hint: 'emailHint'.tr(),
                            validator: Validators.email,
                            type: TextInputType.emailAddress,
                            focusNode: cubit.expertEmailFocusNode,
                            controller: cubit.expertEmailController,
                            prefix: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: SvgPicture.asset('assets/images/email_icon.svg', color: AppColors.iconsColor),
                            ),
                            onFieldSubmitted: (val) => FocusScope.of(context).requestFocus(cubit.expertPhoneFocusNode),
                          ),
                        ),
                        SizedBox(height: height * 0.015),
                        Directionality(
                          textDirection: TextDirection.ltr,
                          child: CustomIntlPhoneField(
                            invalidNumberMessage: "validation.enter_valid_phone".tr(),
                            focusNode: cubit.expertPhoneFocusNode,
                            controller: cubit.expertPhoneController,
                            onSaved: (phone) => cubit.countryCode = phone!.countryCode,
                            onSubmitted: (val) {
                              FocusScope.of(context).requestFocus(cubit.expertPasswordFocusNode);
                              cubit.expertSignUpFormKey.currentState!.save();
                            },
                          ),
                        ),
                        SizedBox(height: height * 0.015),
                        Directionality(
                          textDirection: TextDirection.ltr,
                          child: CustomTextField(
                            hint: 'passwordHint'.tr(),
                            validator: Validators.password,
                            type: TextInputType.visiblePassword,
                            focusNode: cubit.expertPasswordFocusNode,
                            controller: cubit.expertPasswordController,
                            onFieldSubmitted: (val) =>
                                FocusScope.of(context).requestFocus(cubit.expertConfirmPasswordFocusNode),
                            prefix: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: SvgPicture.asset('assets/images/lock.svg', color: AppColors.iconsColor, width: 24),
                            ),
                          ),
                        ),
                        SizedBox(height: height * 0.015),
                        Directionality(
                          textDirection: TextDirection.ltr,
                          child: CustomTextField(
                            hint: 'confirmPasswordHint'.tr(),
                            validator: Validators.password,
                            type: TextInputType.visiblePassword,
                            focusNode: cubit.expertConfirmPasswordFocusNode,
                            controller: cubit.expertConfirmPasswordController,
                            prefix: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: SvgPicture.asset('assets/images/lock.svg', color: AppColors.iconsColor, width: 24),
                            ),
                            onFieldSubmitted: (val) =>
                                FocusScope.of(context).requestFocus(cubit.expertActivationCodeFocusNode),
                          ),
                        ),
                        SizedBox(height: height * 0.015),
                        Directionality(
                          textDirection: TextDirection.ltr,
                          child: CustomTextField(
                            hint: 'enterActivationCode'.tr(),
                            validator: Validators.generalField,
                            type: TextInputType.text,
                            focusNode: cubit.expertActivationCodeFocusNode,
                            controller: cubit.expertActivationCodeController,
                            prefix: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: SvgPicture.asset('assets/images/email_icon.svg', color: AppColors.iconsColor),
                            ),
                            onFieldSubmitted: (_) async => validateAndSignUp(cubit: cubit),
                          ),
                        ),
                        SizedBox(height: height * 0.025),
                        SizedBox(
                          height: height * 0.055,
                          child: (state is ExpertSignUpLoadingState)
                              ? const AppLoader()
                              : CustomButton(
                                  onTap: () async => validateAndSignUp(cubit: cubit),
                                  width: double.infinity,
                                  height: double.infinity,
                                  child: Text('singUp'.tr().toUpperCase(),
                                      style: TextStyle(
                                          fontFamily: isEn ? 'Montserrat' : 'Tajawal', fontWeight: FontWeight.bold))),
                        ),
                        SizedBox(height: height * 0.1 + MediaQuery.of(context).viewInsets.bottom * 0.8),
                      ],
                    ),
                  ),
                ),
              ],
            );
          },
        ),
      ),
    );
  }
}
