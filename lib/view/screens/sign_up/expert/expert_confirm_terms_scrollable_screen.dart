import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:flutter_svg/svg.dart';

import '../../../../config/router/router.dart';
import '../../../../utils/constants/app_colors.dart';
import '../../../../utils/constants/app_const.dart';
import '../../../../utils/helpers/popup_helper.dart';
import '../../../widgets/app_loader.dart';
import '../../../widgets/error_state_widget.dart';
import '../../../widgets/loading_state_widget.dart';
import '../../../widgets/custom_html_widget.dart';
import 'controller/expert_sign_up_cubit.dart';
import 'expert_confirm_privacy_scrollable_screen.dart';

final htmlThemeData = Theme.of(MagicRouter.currentContext!).copyWith(
    textTheme:
        Theme.of(MagicRouter.currentContext!).textTheme.apply(fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal'));

class ExpertConfirmTermsScrollableScreen extends StatelessWidget {
  const ExpertConfirmTermsScrollableScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocBuilder<ExpertSignUpCubit, ExpertSignUpState>(
        builder: (context, state) {
          final cubit = ExpertSignUpCubit.get(context);
          if (state is GetLegalLoadingState || cubit.legalModel == null) {
            return const LoadingStateWidget();
          }
          if (state is GetLegalErrorState) {
            return ErrorStateWidget(hasRefresh: true, onRefresh: () async => await cubit.getLegal());
          }
          return Column(
            children: [
              SizedBox(height: MediaQuery.of(context).padding.top + height * 0.05),
              Center(child: SvgPicture.asset('assets/images/fab_logo.svg', height: height * 0.09)),
              SizedBox(height: height * 0.035),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: width * 0.04),
                child: Text(
                  'terms'.tr(),
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.headline3!.copyWith(
                        fontFamily: isEn ? 'BebasNeue' : 'Tajawal',
                        color: Colors.black,
                      ),
                ),
              ),
              SizedBox(height: height * 0.006),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: width * 0.04),
                child: Text(
                  'read'.tr(),
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.bodyText2!.copyWith(
                        fontFamily: isEn ? 'Montserrat' : 'Tajawal',
                        color: AppColors.iconsColor,
                        fontWeight: FontWeight.bold,
                      ),
                ),
              ),
              SizedBox(height: height * 0.05),
              Expanded(
                child: RefreshIndicator(
                  onRefresh: () async => await cubit.getLegal(),
                  child: ListView(
                    padding: EdgeInsets.symmetric(horizontal: width * 0.03),
                    children: [
                      if (state is GetLegalLoadingState)
                        Column(children: [SizedBox(height: height * 0.2), const AppLoader()])
                      else
                        CustomHtmlWidget(data: cubit.legalModel!.terms)
                    ],
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: width * 0.03),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Checkbox(
                      value: cubit.isTermsChecked,
                      materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                      onChanged: (value) async {
                        cubit.isTermsChecked = value!;
                        cubit.changeTermsCheckBox();
                      },
                    ),
                    Text(
                      'agreeTerms'.tr(),
                      softWrap: true,
                      style: Theme.of(context).textTheme.bodySmall!.copyWith(
                            fontFamily: isEn ? 'Montserrat' : 'Tajawal',
                            fontWeight: FontWeight.bold,
                            color: const Color(0xFF727272),
                          ),
                    ),
                  ],
                ),
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: ElevatedButton(
                  onPressed: cubit.isTermsChecked
                      ? () async {
                          await cubit.confirmTerms();
                          await cubit.userValidateAndCacheUserInfo();
                          MagicRouter.navigateTo(
                              BlocProvider.value(value: cubit, child: const ExpertConfirmPrivacyScrollableScreen()));
                        }
                      : () => PopupHelper.showBasicSnack(msg: 'makeSureTerms'.tr(), color: Colors.red),
                  style: ElevatedButton.styleFrom(shape: const RoundedRectangleBorder(borderRadius: BorderRadius.zero)),
                  child: SizedBox(
                    height: height * 0.08,
                    child: Center(
                      child: state is ConfirmTermsLoadingState || state is ExpertValidateAndCacheLoadingState
                          ? const AppLoader(color: Colors.white)
                          : Text(
                              'confirm'.tr().toUpperCase(),
                              style: TextStyle(
                                fontSize: 18,
                                color: Colors.white,
                                fontWeight: FontWeight.w900,
                                fontFamily: isEn ? 'Montserrat' : 'Tajawal',
                              ),
                            ),
                    ),
                  ),
                ),
              )
            ],
          );
        },
      ),
    );
  }
}
