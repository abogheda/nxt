part of 'expert_sign_up_cubit.dart';

@immutable
abstract class ExpertSignUpState {}

class SignUpInitial extends ExpertSignUpState {}

class ChangeContinueState extends ExpertSignUpState {}

class UpdateExpertProfileImageState extends ExpertSignUpState {}

class ChangeTermsCheckBoxState extends ExpertSignUpState {}

class ChangePrivacyCheckBoxState extends ExpertSignUpState {}

class ChangeEULACheckBoxState extends ExpertSignUpState {}

class ChangeIsEditClickedState extends ExpertSignUpState {}

class ExpertSignUpLoadingState extends ExpertSignUpState {}

class ExpertSignUpSuccessState extends ExpertSignUpState {
  final SignUpModel signUpModel;
  ExpertSignUpSuccessState({required this.signUpModel});
}

class ExpertSignUpErrorState extends ExpertSignUpState {
  final String error;
  ExpertSignUpErrorState({required this.error});
}

class ExpertPostInfoLoadingState extends ExpertSignUpState {}

class ExpertPostInfoSuccessState extends ExpertSignUpState {
  final User user;
  ExpertPostInfoSuccessState({required this.user});
}

class ExpertPostInfoErrorState extends ExpertSignUpState {
  final String error;
  ExpertPostInfoErrorState({required this.error});
}

class SendCodeLoadingState extends ExpertSignUpState {}

class SendCodeSuccessState extends ExpertSignUpState {}

class SendCodeErrorState extends ExpertSignUpState {
  final String error;
  SendCodeErrorState({required this.error});
}

class VerifyPhoneWithCodeLoadingState extends ExpertSignUpState {}

class VerifyPhoneWithCodeSuccessState extends ExpertSignUpState {}

class VerifyPhoneWithCodeErrorState extends ExpertSignUpState {
  final String error;
  VerifyPhoneWithCodeErrorState({required this.error});
}

class ChangeNumberLoadingState extends ExpertSignUpState {}

class ChangeNumberSuccessState extends ExpertSignUpState {
  final ChangeNumberModel user;
  ChangeNumberSuccessState({required this.user});
}

class ChangeNumberErrorState extends ExpertSignUpState {
  final String error;
  ChangeNumberErrorState({required this.error});
}

class GetLegalLoadingState extends ExpertSignUpState {}

class GetLegalSuccessState extends ExpertSignUpState {}

class GetLegalErrorState extends ExpertSignUpState {
  final String error;
  GetLegalErrorState({required this.error});
}

class ConfirmTermsLoadingState extends ExpertSignUpState {}

class ConfirmTermsSuccessState extends ExpertSignUpState {}

class ConfirmTermsErrorState extends ExpertSignUpState {
  final String error;
  ConfirmTermsErrorState({required this.error});
}

class ConfirmPrivacyLoadingState extends ExpertSignUpState {}

class ConfirmPrivacySuccessState extends ExpertSignUpState {}

class ConfirmPrivacyErrorState extends ExpertSignUpState {
  final String error;
  ConfirmPrivacyErrorState({required this.error});
}

class ConfirmEULALoadingState extends ExpertSignUpState {}

class ConfirmEULASuccessState extends ExpertSignUpState {}

class ConfirmEULAErrorState extends ExpertSignUpState {
  final String error;
  ConfirmEULAErrorState({required this.error});
}

class ExpertValidateAndCacheLoadingState extends ExpertSignUpState {}

class ExpertValidateAndCacheSuccessState extends ExpertSignUpState {
  final User user;
  ExpertValidateAndCacheSuccessState({required this.user});
}

class ExpertValidateAndCacheErrorState extends ExpertSignUpState {
  final String error;
  ExpertValidateAndCacheErrorState({required this.error});
}

class ExpertLoginLoadingState extends ExpertSignUpState {}

class ExpertLoginSuccessState extends ExpertSignUpState {
  final UserModel userModel;
  ExpertLoginSuccessState({required this.userModel});
}

class ExpertLoginErrorState extends ExpertSignUpState {
  final String error;
  ExpertLoginErrorState({required this.error});
}

class UploadExpertLogoLoading extends ExpertSignUpState {}

class UploadExpertLogoSuccess extends ExpertSignUpState {}

class UploadProfileImageFailed extends ExpertSignUpState {
  final String msg;
  UploadProfileImageFailed(this.msg);
}
