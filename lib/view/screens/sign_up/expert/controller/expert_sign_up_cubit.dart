// ignore_for_file: unnecessary_import, depend_on_referenced_packages

import 'dart:io';
import 'package:bloc/bloc.dart';
import 'package:custom_timer/custom_timer.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';
import 'package:nxt/data/models/sign_up/create_user_model.dart';
import 'package:nxt/utils/constants/app_images.dart';

import '../../../../../data/models/more/legal_model.dart';
import '../../../../../data/models/sign_up/change_number_model.dart';
import '../../../../../data/models/sign_up/confirm_privacy_model.dart';
import '../../../../../data/models/sign_up/confirm_terms_model.dart';
import '../../../../../data/models/sign_up/send_code_response_model.dart';
import '../../../../../data/models/sign_up/sign_up_model.dart';
import '../../../../../data/models/sign_up/use_invitation_model.dart';
import '../../../../../data/models/sign_up/verify_code_model.dart';
import '../../../../../data/models/user_model.dart';
import '../../../../../data/repos/auth/auth_repo.dart';
import '../../../../../data/repos/general_repo.dart';
import '../../../../../data/repos/imports.dart';
import '../../../../../data/repos/more/more_repo.dart';
import '../../../../../data/service/hive_services.dart';
import '../../../../../data/service/remote_service.dart';
import '../../../../../utils/helpers/picker_helper.dart';

part 'expert_sign_up_state.dart';

class ExpertSignUpCubit extends Cubit<ExpertSignUpState> {
  ExpertSignUpCubit() : super(SignUpInitial());
  static ExpertSignUpCubit get(context) => BlocProvider.of(context);
  num? codeTimer;
  bool canContinue = true;
  bool isEULAChecked = false;
  bool isTermsChecked = false;
  bool isPrivacyChecked = false;
  bool isEditClicked = false;
  File? expertLogoImage;
  String? expertLogoUrl;
  String countryCode = '';
  UserModel? userModel;
  User? user;
  User? postExpertInfoUser;
  SignUpModel? signUpModel;
  LegalModel? legalModel;
  VerifyCodeModel? verificationResponse;
  ChangeNumberModel? changeNumberResponse;
  ConfirmTermsModel? confirmTermsResponse;
  ConfirmPrivacyModel? confirmEULAResponse;
  ConfirmPrivacyModel? confirmPrivacyResponse;
  UseInvitationModel? useInvitationResponse;
  SendCodeResponseModel? sendCodeResponseModel;
  CustomTimerController timerController = CustomTimerController();
  TextEditingController expertNameController = TextEditingController(),
      expertEmailController = TextEditingController(),
      expertBirthController = TextEditingController(),
      expertPhoneController = TextEditingController(),
      expertPasswordController = TextEditingController(),
      expertConfirmPasswordController = TextEditingController(),
      expertVerificationCodeController = TextEditingController(),
      expertActivationCodeController = TextEditingController(),
      expertLinkController = TextEditingController(),
      expertScopeController = TextEditingController(),
      expertChangeNumberController = TextEditingController();
  FocusNode expertEmailFocusNode = FocusNode(),
      expertBirthFocusNode = FocusNode(),
      expertPhoneFocusNode = FocusNode(),
      expertActivationCodeFocusNode = FocusNode(),
      expertPasswordFocusNode = FocusNode(),
      expertLinkFocusNode = FocusNode(),
      expertScopeFocusNode = FocusNode(),
      expertConfirmPasswordFocusNode = FocusNode();
  final expertSignUpFormKey = GlobalKey<FormState>(),
      expertDetailsFormKey = GlobalKey<FormState>(),
      expertChangeNumberFormKey = GlobalKey<FormState>(),
      expertActivationFormKey = GlobalKey<FormState>(),
      expertVerifyCodeFormKey = GlobalKey<FormState>();

  @override
  Future<void> close() {
    expertNameController.dispose();
    expertEmailController.dispose();
    expertBirthController.dispose();
    expertPhoneController.dispose();
    expertPasswordController.dispose();
    expertConfirmPasswordController.dispose();
    expertVerificationCodeController.dispose();
    expertActivationCodeController.dispose();
    expertEmailFocusNode.dispose();
    expertBirthFocusNode.dispose();
    expertPhoneFocusNode.dispose();
    expertPasswordFocusNode.dispose();
    expertConfirmPasswordFocusNode.dispose();
    timerController.dispose();
    return super.close();
  }

  void changeTermsCheckBox() => emit(ChangeTermsCheckBoxState());

  void changePrivacyCheckBox() => emit(ChangePrivacyCheckBoxState());

  void changeEULACheckBox() => emit(ChangeEULACheckBoxState());

  void changeIsEditClicked() {
    isEditClicked = true;
    emit(ChangeIsEditClickedState());
  }

  Future<void> sendCodeAndStartTimer() async {
    await sendCode();
    changeCanContinue(value: false);
    timerController.start();
  }

  Future<void> expertSignUp() async {
    signUpModel = null;
    await HiveHelper.logout();
    emit(ExpertSignUpLoadingState());
    try {
      final res = await AuthRepo.expertSignUp(
        createUserModel: CreateUserModel(
          email: expertEmailController.text.trim(),
          password: expertPasswordController.text.trim(),
          phone: countryCode + expertPhoneController.text.trim(),
          activationCode: expertActivationCodeController.text.trim(),
        ),
      );
      if (res.data != null) {
        signUpModel = res.data;
        if (signUpModel!.message != null) {
          emit(ExpertSignUpErrorState(error: signUpModel!.message ?? 'normalErrorMsg'.tr()));
          return;
        }
        if (signUpModel!.token != null) APIService.setHeaderToken(token: signUpModel!.token);
        await HiveHelper.cacheKeepMeLoggedIn(value: true);
        await HiveHelper.cacheUserInfo(
            token: signUpModel!.token!.toString(),
            userModel: UserModel(user: signUpModel!.newUser, token: signUpModel!.token!.toString()));
        HiveHelper.cacheProfileInfo(
          name: signUpModel!.newUser!.name,
          avatar: signUpModel!.newUser!.avatar,
          birthDate: signUpModel!.newUser!.birthDate,
        );
        await HiveHelper.checkAndCacheCountry(userPhone: signUpModel!.newUser!.phone);
        emit(ExpertSignUpSuccessState(signUpModel: signUpModel!));
      } else {
        emit(ExpertSignUpErrorState(error: res.message!));
      }
    } catch (e) {
      debugPrint(e.toString());
      emit(ExpertSignUpErrorState(error: 'normalErrorMsg'.tr()));
    }
  }

  Future<void> addExpertInfo() async {
    emit(ExpertPostInfoLoadingState());
    if (expertLogoImage != null) {
      emit(UploadExpertLogoLoading());
      final response = await GeneralRepo.uploadMedia([expertLogoImage!]);
      response.fold(
        (url) async {
          expertLogoUrl = url.first;
          emit(UploadExpertLogoSuccess());
        },
        (errorMsg) => emit(UploadProfileImageFailed(errorMsg)),
      );
    }
    final res = isEditClicked
        ? await AuthRepo.patchExpertInfo(
            createUserModel: CreateUserModel(
              name: expertNameController.text.trim(),
              website: expertLinkController.text.trim(),
              scope: expertScopeController.text.trim(),
              logo: HiveHelper.getUserAvatar ?? userAvatarPlaceHolderUrl,
            ),
          )
        : await AuthRepo.postExpertInfo(
            createUserModel: CreateUserModel(
              name: expertNameController.text.trim(),
              website: expertLinkController.text.trim(),
              scope: expertScopeController.text.trim(),
              logo: expertLogoUrl ?? userAvatarPlaceHolderUrl,
            ),
          );
    if (res.data != null) {
      postExpertInfoUser = res.data;
      await HiveHelper.checkAndCacheCountry(userPhone: postExpertInfoUser!.phone);
      emit(ExpertPostInfoSuccessState(user: postExpertInfoUser!));
    } else {
      emit(ExpertPostInfoErrorState(error: res.message!));
    }
  }

  Future<void> editExpertInfo() async {
    emit(ExpertPostInfoLoadingState());
    if (expertLogoImage != null) {
      emit(UploadExpertLogoLoading());
      final response = await GeneralRepo.uploadMedia([expertLogoImage!]);
      response.fold(
        (url) async {
          expertLogoUrl = url.first;
          emit(UploadExpertLogoSuccess());
        },
        (errorMsg) => emit(UploadProfileImageFailed(errorMsg)),
      );
    }
    final res = await AuthRepo.patchExpertInfo(
      createUserModel: CreateUserModel(
        name: expertNameController.text.trim(),
        website: expertLinkController.text.trim(),
        scope: expertScopeController.text.trim(),
        logo: expertLogoUrl ?? HiveHelper.getUserAvatar ?? userAvatarPlaceHolderUrl,
      ),
    );
    if (res.data != null) {
      postExpertInfoUser = res.data;
      await HiveHelper.checkAndCacheCountry(userPhone: postExpertInfoUser!.phone);
      emit(ExpertPostInfoSuccessState(user: postExpertInfoUser!));
    } else {
      emit(ExpertPostInfoErrorState(error: res.message!));
    }
  }

  Future<void> sendCode() async {
    sendCodeResponseModel = null;
    emit(SendCodeLoadingState());
    final res = await AuthRepo.sendCode();
    if (res.data != null) {
      sendCodeResponseModel = res.data;
      if (sendCodeResponseModel.toString() == 'ThrottlerException: Too Many Requests') {
        emit(SendCodeErrorState(error: sendCodeResponseModel.toString()));
      }
      if (sendCodeResponseModel != null) {
        codeTimer = sendCodeResponseModel!.remaining;
        emit(SendCodeSuccessState());
      }
      emit(SendCodeSuccessState());
    } else {
      emit(SendCodeErrorState(error: res.message!));
    }
  }

  Future<void> verifyPhoneWithCode({required String code}) async {
    emit(VerifyPhoneWithCodeLoadingState());
    final res = await AuthRepo.verifyPhoneWithCode(
      code: int.tryParse(code)!,
    );
    if (res.message == null) {
      verificationResponse = res.data;
      emit(VerifyPhoneWithCodeSuccessState());
    } else {
      emit(VerifyPhoneWithCodeErrorState(error: res.message!));
    }
  }

  Future<void> changeNumber() async {
    changeNumberResponse = null;
    emit(ChangeNumberLoadingState());
    final res = await AuthRepo.changeNumber(phone: countryCode + expertChangeNumberController.text.trim());
    if (res.data != null) {
      changeNumberResponse = res.data;
      emit(ChangeNumberSuccessState(user: changeNumberResponse!));
    } else {
      emit(ChangeNumberErrorState(error: res.message!));
    }
  }

  Future<void> confirmTerms() async {
    emit(ConfirmTermsLoadingState());
    final res = await AuthRepo.confirmTerms();
    if (res.data != null) {
      confirmTermsResponse = res.data;
      emit(ConfirmTermsSuccessState());
    } else {
      emit(ConfirmTermsErrorState(error: res.message!));
    }
  }

  Future<void> confirmPrivacy() async {
    emit(ConfirmPrivacyLoadingState());
    final res = await AuthRepo.confirmPrivacy();
    if (res.data != null) {
      confirmPrivacyResponse = res.data;
      emit(ConfirmPrivacySuccessState());
    } else {
      emit(ConfirmPrivacyErrorState(error: res.message!));
    }
  }

  Future<void> confirmEULA() async {
    emit(ConfirmEULALoadingState());
    final res = await AuthRepo.confirmEULA();
    if (res.data != null) {
      confirmEULAResponse = res.data;
      emit(ConfirmEULASuccessState());
    } else {
      emit(ConfirmEULAErrorState(error: res.message!));
    }
  }

  Future<void> getLegal() async {
    emit(GetLegalLoadingState());
    final res = await MoreRepo.getLegal();
    if (res.data != null) {
      legalModel = res.data;
      emit(GetLegalSuccessState());
    } else {
      emit(GetLegalErrorState(error: res.message ?? ""));
    }
  }

  void changeCanContinue({required bool value}) {
    canContinue = value;
    emit(ChangeContinueState());
  }

  Future pickImageForProfile() async {
    expertLogoImage = await PickerHelper.pickGalleryImage();
    emit(UpdateExpertProfileImageState());
  }

  void deleteExpertProfileImage() {
    expertLogoImage = null;
    emit(UpdateExpertProfileImageState());
  }

  Future<void> expertLogin() async {
    emit(ExpertLoginLoadingState());
    if (expertPasswordController.value.text.isNotEmpty) {
      await HiveHelper.logout();
      final res = await AuthRepo.userLogin(
        phone: countryCode + expertPhoneController.text.trim().toString(),
        password: expertPasswordController.text.trim(),
      );
      if (res.data != null) {
        userModel = res.data;
        if (userModel!.token != null) {
          APIService.setHeaderToken(token: userModel!.token);
          await HiveHelper.cacheUserInfo(token: userModel!.token!.toString(), userModel: userModel!);
          await HiveHelper.checkAndCacheCountry(userPhone: userModel!.user!.phone);
          await HiveHelper.cacheProfileInfo(
            name: userModel!.user!.expert!.name,
            avatar: userModel!.user!.expert!.logo,
            birthDate: DateTime.now().toIso8601String(),
          );
        }
        emit(ExpertLoginSuccessState(userModel: userModel!));
      } else {
        emit(ExpertLoginErrorState(error: res.message!));
      }
    } else {
      await userValidateAndCacheUserInfo();
    }
  }

  Future<void> userValidateAndCacheUserInfo() async {
    emit(ExpertValidateAndCacheLoadingState());
    final res = await ProfileRepo.getUserValidate();
    if (res.data != null) {
      user = res.data;
      if (user!.phone != null) {
        await HiveHelper.cacheUserModel(userModel: HiveHelper.getUserInfo!.copyWith(user: user));
      }
      emit(ExpertValidateAndCacheSuccessState(user: user!));
    } else {
      emit(ExpertValidateAndCacheErrorState(error: res.message!));
    }
  }
}
