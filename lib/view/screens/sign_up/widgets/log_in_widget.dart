import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '../../../../utils/constants/resources.dart';
import '../../login/login_screen.dart';

import '../../../../config/router/router.dart';

class LogInWidget extends StatelessWidget {
  const LogInWidget({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.bottomCenter,
      child: ElevatedButton(
        onPressed: () => MagicRouter.navigateAndPopAll(const LoginScreen()),
        style: ElevatedButton.styleFrom(shape: const RoundedRectangleBorder(borderRadius: BorderRadius.zero)),
        child: SizedBox(
          height: height * 0.08,
          child: Center(
            child: RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                text: '${'haveAnAccount'.tr()} ',
                style: TextStyle(
                  fontSize: 16.sp,
                  color: Colors.white,
                  fontWeight: FontWeight.w100,
                  fontFamily: isEn ? 'Montserrat' : 'Tajawal',
                ),
                children: [
                  TextSpan(
                    text: 'login.login'.tr().toUpperCase(),
                    style: TextStyle(
                      fontSize: 16.sp,
                      color: Colors.white,
                      fontWeight: FontWeight.w900,
                      fontFamily: isEn ? 'Montserrat' : 'Tajawal',
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
