import 'dart:io';

import 'package:easy_localization/easy_localization.dart' as local;
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../../utils/constants/resources.dart';
import '../../../../config/router/router.dart';
import '../../../../utils/helpers/popup_helper.dart';
import '../../../widgets/app_loader.dart';
import '../../../widgets/custom_button.dart';
import '../../../widgets/logo_app_bar.dart';
import 'controller/talent_sign_up_cubit.dart';
import 'custom_intl_phone_field.dart';

class TalentChangeNumberScreen extends StatelessWidget {
  const TalentChangeNumberScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: Platform.isAndroid ? false : true,
      body: BlocConsumer<TalentSignUpCubit, TalentSignUpState>(
        listener: (context, state) async {
          final cubit = TalentSignUpCubit.get(context);
          if (state is ChangeNumberErrorState) {
            PopupHelper.showBasicSnack(msg: state.error, color: Colors.red);
          }
          if (state is ChangeNumberSuccessState) {
            if (state.user.message != null) {
              PopupHelper.showBasicSnack(msg: state.user.message!, color: Colors.red);
              return;
            }
            cubit.talentPhoneController.text = cubit.talentChangeNumberController.text;
            await cubit.sendCode();
            cubit.talentChangeNumberController.clear();
            MagicRouter.pop();
          }
        },
        builder: (context, state) {
          final cubit = TalentSignUpCubit.get(context);
          Future<void> saveAndChangeNumber() async {
            cubit.talentChangeNumberFormKey.currentState!.save();
            FocusManager.instance.primaryFocus?.unfocus();
            if (cubit.talentChangeNumberFormKey.currentState!.validate()) {
              await cubit.changeNumber();
            }
          }

          return ListView(
            shrinkWrap: true,
            children: [
              const LogoAppBar(),
              SizedBox(height: height * 0.03),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: width * 0.15),
                child: Text(
                  'pleaseEnterDifferentNumber'.tr(),
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.titleMedium!.copyWith(
                        fontFamily: isEn ? 'Montserrat' : 'Tajawal',
                        color: const Color(0xFF727272),
                        fontWeight: FontWeight.w400,
                      ),
                ),
              ),
              SizedBox(height: height * 0.05),
              Form(
                key: cubit.talentChangeNumberFormKey,
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: width * 0.05),
                  child: Directionality(
                    textDirection: TextDirection.ltr,
                    child: CustomIntlPhoneField(
                      invalidNumberMessage: "validation.enter_valid_phone".tr(),
                      controller: cubit.talentChangeNumberController,
                      hintText: 'enterNewNumberHere'.tr(),
                      onSaved: (phone) => cubit.countryCode = phone!.countryCode,
                      onSubmitted: (_) async => await saveAndChangeNumber(),
                    ),
                  ),
                  //   controller: cubit.talentChangeNumberController,
                  //   onFieldSubmitted: (_) async {
                  //     FocusManager.instance.primaryFocus?.unfocus();
                  //     if (cubit.talentChangeNumberFormKey.currentState!.validate()) {
                  //       await cubit.changeNumber();
                  //     }
                  //   },
                  // ),
                ),
              ),
              SizedBox(height: height * 0.03),
              Center(
                child: SizedBox(
                  height: height * 0.055,
                  width: width * 0.9,
                  child: (state is ChangeNumberLoadingState)
                      ? const AppLoader()
                      : CustomButton(
                          onTap: () async => await saveAndChangeNumber(),
                          child: Text('confirm'.tr().toUpperCase(),
                              style:
                                  TextStyle(fontFamily: isEn ? 'Montserrat' : 'Tajawal', fontWeight: FontWeight.bold))),
                ),
              ),
            ],
          );
        },
      ),
    );
  }
}
