// ignore_for_file: unnecessary_import, depend_on_referenced_packages
import 'package:bloc/bloc.dart';
import 'package:custom_timer/custom_timer.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';
import 'package:nxt/data/models/sign_up/create_user_model.dart';

import '../../../../../data/models/more/legal_model.dart';
import '../../../../../data/models/sign_up/change_number_model.dart';
import '../../../../../data/models/sign_up/confirm_privacy_model.dart';
import '../../../../../data/models/sign_up/confirm_terms_model.dart';
import '../../../../../data/models/sign_up/request_invitation_model.dart';
import '../../../../../data/models/sign_up/send_code_response_model.dart';
import '../../../../../data/models/sign_up/sign_up_model.dart';
import '../../../../../data/models/sign_up/use_invitation_model.dart';
import '../../../../../data/models/sign_up/verify_code_model.dart';
import '../../../../../data/models/user_model.dart';
import '../../../../../data/repos/auth/auth_repo.dart';
import '../../../../../data/repos/imports.dart';
import '../../../../../data/repos/more/more_repo.dart';
import '../../../../../data/service/hive_services.dart';
import '../../../../../data/service/remote_service.dart';
part 'talent_sign_up_state.dart';

class TalentSignUpCubit extends Cubit<TalentSignUpState> {
  TalentSignUpCubit() : super(SignUpInitial());
  static TalentSignUpCubit get(context) => BlocProvider.of(context);
  num? codeTimer;
  bool canContinue = true;
  bool isEULAChecked = false;
  bool isTermsChecked = false;
  bool isPrivacyChecked = false;
  String countryCode = '';
  UserModel? userModel;
  User? user;
  LegalModel? legalModel;
  SignUpModel? signUpModel;
  VerifyCodeModel? verificationResponse;
  ChangeNumberModel? changeNumberResponse;
  ConfirmTermsModel? confirmTermsResponse;
  ConfirmPrivacyModel? confirmEULAResponse;
  ConfirmPrivacyModel? confirmPrivacyResponse;
  UseInvitationModel? useInvitationResponse;
  SendCodeResponseModel? sendCodeResponseModel;
  RequestInvitationModel? requestInvitationModel;
  CustomTimerController timerController = CustomTimerController();
  TextEditingController talentNameController = TextEditingController(),
      talentEmailController = TextEditingController(),
      talentBirthController = TextEditingController(),
      talentPhoneController = TextEditingController(),
      talentPasswordController = TextEditingController(),
      talentConfirmPasswordController = TextEditingController(),
      talentVerificationCodeController = TextEditingController(),
      talentActivationCodeController = TextEditingController(),
      talentChangeNumberController = TextEditingController();
  FocusNode talentEmailFocusNode = FocusNode(),
      talentBirthFocusNode = FocusNode(),
      talentPhoneFocusNode = FocusNode(),
      talentPasswordFocusNode = FocusNode(),
      talentConfirmPasswordFocusNode = FocusNode();

  final talentSignUpFormKey = GlobalKey<FormState>(),
      talentChangeNumberFormKey = GlobalKey<FormState>(),
      talentActivationFormKey = GlobalKey<FormState>(),
      talentVerifyCodeFormKey = GlobalKey<FormState>();

  @override
  Future<void> close() {
    talentNameController.dispose();
    talentEmailController.dispose();
    talentBirthController.dispose();
    talentPhoneController.dispose();
    talentPasswordController.dispose();
    talentConfirmPasswordController.dispose();
    talentVerificationCodeController.dispose();
    talentActivationCodeController.dispose();
    talentEmailFocusNode.dispose();
    talentBirthFocusNode.dispose();
    talentPhoneFocusNode.dispose();
    talentPasswordFocusNode.dispose();
    talentConfirmPasswordFocusNode.dispose();
    timerController.dispose();
    return super.close();
  }

  void changeTermsCheckBox() => emit(ChangeTermsCheckBoxState());

  void changePrivacyCheckBox() => emit(ChangePrivacyCheckBoxState());

  void changeEULACheckBox() => emit(ChangeEULACheckBoxState());

  // Future<void> cacheCountry() async {
  //   bool startsWith({required String firstThreeDigits}) => signUpModel!.newUser!.phone!.startsWith(firstThreeDigits);
  //   if (startsWith(firstThreeDigits: '+2010') ||
  //       startsWith(firstThreeDigits: '+2011') ||
  //       startsWith(firstThreeDigits: '+2012') ||
  //       startsWith(firstThreeDigits: '+2015')) {
  //     await HiveHelper.cacheCountry(country: 'Egypt');
  //   } else {
  //     await HiveHelper.userBox.delete('country');
  //   }
  // }

  Future<void> talentSignUp() async {
    signUpModel = null;
    await HiveHelper.logout();
    emit(TalentSignUpLoadingState());
    try {
      final res = await AuthRepo.talentSignUp(
        createUserModel: CreateUserModel(
          name: talentNameController.text.trim(),
          password: talentPasswordController.text.trim(),
          email: talentEmailController.text.trim(),
          phone: countryCode + talentPhoneController.text.trim(),
          birthDate: talentBirthController.text.isEmpty
              ? null
              : DateFormat('dd/MM/yyyy').parse(talentBirthController.text).toIso8601String(),
        ),
      );
      if (res.data != null) {
        signUpModel = res.data;
        if (signUpModel!.message != null) {
          emit(TalentSignUpErrorState(error: signUpModel!.message ?? 'normalErrorMsg'.tr()));
          return;
        }
        if (signUpModel!.token != null) APIService.setHeaderToken(token: signUpModel!.token);
        await HiveHelper.cacheKeepMeLoggedIn(value: true);
        await HiveHelper.cacheUserInfo(
            token: signUpModel!.token!.toString(),
            userModel: UserModel(user: signUpModel!.newUser, token: signUpModel!.token!.toString()));
        HiveHelper.cacheProfileInfo(
          name: signUpModel!.newUser!.name,
          avatar: signUpModel!.newUser!.avatar,
          birthDate: signUpModel!.newUser!.birthDate,
        );
        await HiveHelper.checkAndCacheCountry(userPhone: signUpModel!.newUser!.phone);
        emit(TalentSignUpSuccessState(signUpModel: signUpModel!));
      } else {
        emit(TalentSignUpErrorState(error: res.message ?? 'normalErrorMsg'.tr()));
      }
    } catch (e) {
      debugPrint(e.toString());
      emit(TalentSignUpErrorState(error: 'normalErrorMsg'.tr()));
    }
  }

  Future<void> sendCode() async {
    sendCodeResponseModel = null;
    emit(SendCodeLoadingState());
    final res = await AuthRepo.sendCode();
    if (res.data != null) {
      sendCodeResponseModel = res.data;
      if (sendCodeResponseModel.toString() == 'ThrottlerException: Too Many Requests') {
        emit(SendCodeErrorState(error: sendCodeResponseModel.toString()));
      }
      if (sendCodeResponseModel != null) {
        codeTimer = sendCodeResponseModel!.remaining;
        emit(SendCodeSuccessState());
      }
      emit(SendCodeSuccessState());
    } else {
      emit(SendCodeErrorState(error: res.message!));
    }
  }

  Future<void> sendCodeAndStartTimer() async {
    await sendCode();
    changeCanContinue(value: false);
    timerController.start();
  }

  Future<void> verifyPhoneWithCode({required String code}) async {
    emit(VerifyPhoneWithCodeLoadingState());
    try {
      final res = await AuthRepo.verifyPhoneWithCode(
        code: int.tryParse(code)!,
      );
      if (res.message == null) {
        verificationResponse = res.data;
        emit(VerifyPhoneWithCodeSuccessState());
      } else {
        emit(VerifyPhoneWithCodeErrorState(error: res.message!));
      }
    } catch (e) {
      emit(VerifyPhoneWithCodeErrorState(error: 'makeSureToEnterNumbers'.tr()));
    }
  }

  Future<void> changeNumber() async {
    changeNumberResponse = null;
    emit(ChangeNumberLoadingState());
    final res = await AuthRepo.changeNumber(phone: countryCode + talentChangeNumberController.text.trim());
    if (res.data != null) {
      changeNumberResponse = res.data;
      emit(ChangeNumberSuccessState(user: changeNumberResponse!));
    } else {
      emit(ChangeNumberErrorState(error: res.message!));
    }
  }

  Future<void> useInvitation() async {
    useInvitationResponse = null;
    emit(UseInvitationLoadingState());
    final res = await AuthRepo.useInvitation(invitationCode: talentActivationCodeController.text.trim());
    if (res.data != null) {
      useInvitationResponse = res.data;
      if (useInvitationResponse!.message != null) {
        emit(UseInvitationErrorState(error: useInvitationResponse!.message!));
        return;
      }
      emit(UseInvitationSuccessState());
    } else {
      emit(UseInvitationErrorState(error: res.message!));
    }
  }

  Future<void> requestNewInvitation() async {
    requestInvitationModel = null;
    emit(RequestNewInvitationLoadingState());
    final res = await AuthRepo.requestNewInvitation();
    if (res.data != null) {
      requestInvitationModel = res.data;
      if (requestInvitationModel!.message != null) {
        emit(RequestNewInvitationErrorState(error: requestInvitationModel!.message!));
        return;
      }
      emit(RequestNewInvitationSuccessState());
    } else {
      emit(RequestNewInvitationErrorState(error: res.message!));
    }
  }

  Future<void> confirmTerms() async {
    emit(ConfirmTermsLoadingState());
    final res = await AuthRepo.confirmTerms();
    if (res.data != null) {
      confirmTermsResponse = res.data;
      emit(ConfirmTermsSuccessState());
    } else {
      emit(ConfirmTermsErrorState(error: res.message!));
    }
  }

  Future<void> confirmPrivacy() async {
    emit(ConfirmPrivacyLoadingState());
    final res = await AuthRepo.confirmPrivacy();
    if (res.data != null) {
      confirmPrivacyResponse = res.data;
      emit(ConfirmPrivacySuccessState());
    } else {
      emit(ConfirmPrivacyErrorState(error: res.message!));
    }
  }

  Future<void> confirmEULA() async {
    emit(ConfirmEULALoadingState());
    final res = await AuthRepo.confirmEULA();
    if (res.data != null) {
      confirmEULAResponse = res.data;
      emit(ConfirmEULASuccessState());
    } else {
      emit(ConfirmEULAErrorState(error: res.message!));
    }
  }

  Future<void> getLegal() async {
    emit(GetLegalLoadingState());
    final res = await MoreRepo.getLegal();
    if (res.data != null) {
      legalModel = res.data;
      emit(GetLegalSuccessState());
    } else {
      emit(GetLegalErrorState(error: res.message ?? ""));
    }
  }

  void changeCanContinue({required bool value}) {
    canContinue = value;
    emit(ChangeContinueState());
  }

  Future<void> talentLogin() async {
    emit(TalentLoginLoadingState());
    if (talentPasswordController.value.text.isNotEmpty) {
      await HiveHelper.logout();
      final res = await AuthRepo.userLogin(
        phone: countryCode + talentPhoneController.text.trim().toString(),
        password: talentPasswordController.text.trim(),
      );
      if (res.data != null) {
        userModel = res.data;
        if (userModel!.token != null) {
          APIService.setHeaderToken(token: userModel!.token);
          await HiveHelper.cacheUserInfo(token: userModel!.token!.toString(), userModel: userModel!);
          await HiveHelper.checkAndCacheCountry(userPhone: userModel!.user!.phone);
          await HiveHelper.cacheProfileInfo(
            name: userModel!.user!.name,
            avatar: userModel!.user!.avatar,
            birthDate: userModel!.user!.birthDate,
          );
        }
        emit(TalentLoginSuccessState(userModel: userModel!));
      } else {
        emit(TalentLoginErrorState(error: res.message!));
      }
    } else {
      await userValidateAndCacheUserInfo();
    }
  }

  Future<void> userValidateAndCacheUserInfo() async {
    emit(TalentValidateAndCacheLoadingState());
    final res = await ProfileRepo.getUserValidate();
    if (res.data != null) {
      user = res.data;
      if (user!.name != null) {
        await HiveHelper.cacheUserModel(userModel: HiveHelper.getUserInfo!.copyWith(user: user));
        await HiveHelper.cacheProfileInfo(
          name: user!.name,
          avatar: user!.avatar,
          birthDate: user!.birthDate,
        );
      }
      emit(TalentValidateAndCacheSuccessState(user: user!));
    } else {
      emit(TalentValidateAndCacheErrorState(error: res.message!));
    }
  }
}
