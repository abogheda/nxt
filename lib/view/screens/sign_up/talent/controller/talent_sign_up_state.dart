part of 'talent_sign_up_cubit.dart';

@immutable
abstract class TalentSignUpState {}

class SignUpInitial extends TalentSignUpState {}

class ChangeContinueState extends TalentSignUpState {}

class ChangeTermsCheckBoxState extends TalentSignUpState {}

class ChangePrivacyCheckBoxState extends TalentSignUpState {}

class ChangeEULACheckBoxState extends TalentSignUpState {}

class TalentSignUpLoadingState extends TalentSignUpState {}

class TalentSignUpSuccessState extends TalentSignUpState {
  final SignUpModel signUpModel;
  TalentSignUpSuccessState({required this.signUpModel});
}

class TalentSignUpErrorState extends TalentSignUpState {
  final String error;
  TalentSignUpErrorState({required this.error});
}

class SendCodeLoadingState extends TalentSignUpState {}

class SendCodeSuccessState extends TalentSignUpState {}

class SendCodeErrorState extends TalentSignUpState {
  final String error;
  SendCodeErrorState({required this.error});
}

class VerifyPhoneWithCodeLoadingState extends TalentSignUpState {}

class VerifyPhoneWithCodeSuccessState extends TalentSignUpState {}

class VerifyPhoneWithCodeErrorState extends TalentSignUpState {
  final String error;
  VerifyPhoneWithCodeErrorState({required this.error});
}

class ChangeNumberLoadingState extends TalentSignUpState {}

class ChangeNumberSuccessState extends TalentSignUpState {
  final ChangeNumberModel user;
  ChangeNumberSuccessState({required this.user});
}

class ChangeNumberErrorState extends TalentSignUpState {
  final String error;
  ChangeNumberErrorState({required this.error});
}

class UseInvitationLoadingState extends TalentSignUpState {}

class UseInvitationSuccessState extends TalentSignUpState {}

class UseInvitationErrorState extends TalentSignUpState {
  final String error;
  UseInvitationErrorState({required this.error});
}

class RequestNewInvitationLoadingState extends TalentSignUpState {}

class RequestNewInvitationSuccessState extends TalentSignUpState {}

class RequestNewInvitationErrorState extends TalentSignUpState {
  final String error;
  RequestNewInvitationErrorState({required this.error});
}

class GetLegalLoadingState extends TalentSignUpState {}

class GetLegalSuccessState extends TalentSignUpState {}

class GetLegalErrorState extends TalentSignUpState {
  final String error;
  GetLegalErrorState({required this.error});
}

class ConfirmTermsLoadingState extends TalentSignUpState {}

class ConfirmTermsSuccessState extends TalentSignUpState {}

class ConfirmTermsErrorState extends TalentSignUpState {
  final String error;
  ConfirmTermsErrorState({required this.error});
}

class ConfirmPrivacyLoadingState extends TalentSignUpState {}

class ConfirmPrivacySuccessState extends TalentSignUpState {}

class ConfirmPrivacyErrorState extends TalentSignUpState {
  final String error;
  ConfirmPrivacyErrorState({required this.error});
}

class ConfirmEULALoadingState extends TalentSignUpState {}

class ConfirmEULASuccessState extends TalentSignUpState {}

class ConfirmEULAErrorState extends TalentSignUpState {
  final String error;
  ConfirmEULAErrorState({required this.error});
}

class TalentLoginLoadingState extends TalentSignUpState {}

class TalentLoginSuccessState extends TalentSignUpState {
  final UserModel userModel;
  TalentLoginSuccessState({required this.userModel});
}

class TalentLoginErrorState extends TalentSignUpState {
  final String error;
  TalentLoginErrorState({required this.error});
}

class TalentValidateAndCacheLoadingState extends TalentSignUpState {}

class TalentValidateAndCacheSuccessState extends TalentSignUpState {
  final User user;
  TalentValidateAndCacheSuccessState({required this.user});
}

class TalentValidateAndCacheErrorState extends TalentSignUpState {
  final String error;
  TalentValidateAndCacheErrorState({required this.error});
}
