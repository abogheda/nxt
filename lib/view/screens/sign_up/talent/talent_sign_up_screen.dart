import 'dart:io';

import 'package:easy_localization/easy_localization.dart' as local;
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import '../../../../utils/constants/resources.dart';
import 'custom_intl_phone_field.dart';
import '../widgets/log_in_widget.dart';

import '../../../../config/router/router.dart';
import '../../../../utils/helpers/popup_helper.dart';
import '../../../../utils/helpers/validators.dart';
import '../../../widgets/app_loader.dart';
import '../../../widgets/custom_button.dart';
import '../../../widgets/custom_text_field.dart';
import '../../talent_profile/screens/edit_profile/profile_date_text_form_field.dart';
import '../../talent_profile/screens/edit_profile/profile_text_form_field.dart';
import 'controller/talent_sign_up_cubit.dart';
import 'talent_verification_screen.dart';

class TalentSignUpScreen extends StatelessWidget {
  const TalentSignUpScreen({Key? key}) : super(key: key);
  Future<void> validateAndSignUp({required TalentSignUpCubit cubit}) async {
    cubit.talentSignUpFormKey.currentState!.save();
    FocusManager.instance.primaryFocus?.unfocus();
    if (cubit.talentSignUpFormKey.currentState!.validate()) {
      if (cubit.talentPasswordController.text != cubit.talentConfirmPasswordController.text) {
        PopupHelper.showBasicSnack(msg: 'reset_password.make_sure'.tr(), color: Colors.red);
        return;
      }
      await cubit.talentSignUp();
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => TalentSignUpCubit(),
      child: Scaffold(
        resizeToAvoidBottomInset: Platform.isAndroid ? false : true,
        body: BlocConsumer<TalentSignUpCubit, TalentSignUpState>(
          listener: (context, state) async {
            final cubit = TalentSignUpCubit.get(context);
            if (state is TalentSignUpErrorState) {
              PopupHelper.showBasicSnack(msg: state.error.toString(), color: Colors.red);
            }
            if (state is TalentSignUpSuccessState) {
              if (state.signUpModel.token == null || state.signUpModel.token == '') {
                PopupHelper.showBasicSnack(msg: state.signUpModel.message.toString(), color: Colors.red);
                return;
              }
              if (state.signUpModel.token != null) {
                await cubit.sendCodeAndStartTimer();
                MagicRouter.navigateTo(BlocProvider.value(value: cubit, child: const TalentVerificationScreen()));
              }
            }
          },
          builder: (context, state) {
            final cubit = TalentSignUpCubit.get(context);
            return Stack(
              children: [
                ListView(
                  children: [
                    SizedBox(height: 10.h),
                    Center(child: SvgPicture.asset('assets/images/fab_logo.svg', height: height * 0.09)),
                    Text('singUp'.tr().toUpperCase(),
                        textAlign: TextAlign.center,
                        style: Theme.of(context)
                            .textTheme
                            .headline3!
                            .copyWith(fontFamily: isEn ? 'BebasNeue' : 'Tajawal', color: Colors.black)),
                    SizedBox(height: height * 0.03),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: width * 0.05),
                      child: Form(
                        key: cubit.talentSignUpFormKey,
                        child: Column(
                          children: [
                            Directionality(
                              textDirection: TextDirection.ltr,
                              child: ProfileTextFormField(
                                margin: EdgeInsets.zero,
                                inputType: TextInputType.name,
                                hint: 'nameHint'.tr(),
                                controller: cubit.talentNameController,
                                validator: Validators.generalField,
                                onFieldSubmitted: (_) =>
                                    FocusScope.of(context).requestFocus(cubit.talentEmailFocusNode),
                              ),
                            ),
                            SizedBox(height: height * 0.015),
                            Directionality(
                              textDirection: TextDirection.ltr,
                              child: ProfileTextFormField(
                                margin: EdgeInsets.zero,
                                inputType: TextInputType.emailAddress,
                                hint: 'emailHint'.tr(),
                                controller: cubit.talentEmailController,
                                validator: Validators.email,
                                focusNode: cubit.talentEmailFocusNode,
                              ),
                            ),
                            SizedBox(height: height * 0.015),
                            Directionality(
                              textDirection: TextDirection.ltr,
                              child: ProfileDateTextFormField(
                                margin: EdgeInsets.zero,
                                controller: cubit.talentBirthController,
                                initialValue: DateTime(DateTime.now().year - 12),
                                validator: Validators.generalField,
                              ),
                            ),
                            SizedBox(height: height * 0.015),
                            Directionality(
                              textDirection: TextDirection.ltr,
                              child: CustomIntlPhoneField(
                                invalidNumberMessage: "validation.enter_valid_phone".tr(),
                                focusNode: cubit.talentPhoneFocusNode,
                                controller: cubit.talentPhoneController,
                                onSaved: (phone) => cubit.countryCode = phone!.countryCode,
                                onSubmitted: (val) {
                                  FocusScope.of(context).requestFocus(cubit.talentPasswordFocusNode);
                                  cubit.talentSignUpFormKey.currentState!.save();
                                },
                              ),
                            ),
                            SizedBox(height: height * 0.015),
                            Directionality(
                              textDirection: TextDirection.ltr,
                              child: CustomTextField(
                                hint: 'passwordHint'.tr(),
                                validator: Validators.password,
                                type: TextInputType.visiblePassword,
                                focusNode: cubit.talentPasswordFocusNode,
                                controller: cubit.talentPasswordController,
                                onFieldSubmitted: (val) =>
                                    FocusScope.of(context).requestFocus(cubit.talentConfirmPasswordFocusNode),
                                prefix: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: SvgPicture.asset('assets/images/lock.svg',
                                        color: AppColors.iconsColor, width: 24)),
                              ),
                            ),
                            SizedBox(height: height * 0.015),
                            Directionality(
                              textDirection: TextDirection.ltr,
                              child: CustomTextField(
                                validator: Validators.password,
                                hint: 'confirmPasswordHint'.tr(),
                                type: TextInputType.visiblePassword,
                                focusNode: cubit.talentConfirmPasswordFocusNode,
                                controller: cubit.talentConfirmPasswordController,
                                onFieldSubmitted: (_) async => validateAndSignUp(cubit: cubit),
                                prefix: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: SvgPicture.asset('assets/images/lock.svg',
                                        color: AppColors.iconsColor, width: 24)),
                              ),
                            ),
                            SizedBox(height: height * 0.025),
                            SizedBox(
                              height: height * 0.055,
                              child: (state is TalentSignUpLoadingState)
                                  ? const AppLoader()
                                  : CustomButton(
                                      onTap: () async => validateAndSignUp(cubit: cubit),
                                      width: double.infinity,
                                      height: double.infinity,
                                      child: Text('singUp'.tr().toUpperCase(),
                                          style: TextStyle(
                                              fontFamily: isEn ? 'Montserrat' : 'Tajawal',
                                              fontWeight: FontWeight.bold))),
                            )
                          ],
                        ),
                      ),
                    ),
                    SizedBox(height: height * 0.1 + MediaQuery.of(context).viewInsets.bottom * 0.8),
                  ],
                ),
                const LogInWidget()
              ],
            );
          },
        ),
      ),
    );
  }
}
