// // ignore_for_file: use_build_context_synchronously
//
// import 'package:easy_localization/easy_localization.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:flutter_svg/svg.dart';
// import 'package:nxt/utils/constants/app_const.dart';
// import 'package:nxt/utils/helpers/popup_helper.dart';
// import 'package:nxt/data/service/hive_services.dart';
//
// import '../../../../config/router/router.dart';
// import '../../../../utils/helpers/update_dialog.dart';
// import '../../../../utils/helpers/validators.dart';
// import '../../../widgets/app_loader.dart';
// import '../../../widgets/custom_button.dart';
// import '../../../widgets/custom_text_field.dart';
// import '../../../widgets/verified_successfully_dialog.dart';
// import '../../complete_profile_screen/complete_profile_screen.dart';
// import '../../navigation_and_appbar/import_widget.dart';
// import 'controller/talent_sign_up_cubit.dart';
//
// class TalentInvitationScreen extends StatefulWidget {
//   const TalentInvitationScreen({Key? key, required this.isFirstTime}) : super(key: key);
//   final bool isFirstTime;
//   @override
//   State<TalentInvitationScreen> createState() => _TalentInvitationScreenState();
// }
//
// class _TalentInvitationScreenState extends State<TalentInvitationScreen> {
//   @override
//   void initState() {
//     checkNewVersion(context);
//     super.initState();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       body: BlocConsumer<TalentSignUpCubit, TalentSignUpState>(
//         listener: (context, state) async {
//           if (state is UseInvitationErrorState) {
//             PopupHelper.showBasicSnack(msg: state.error, color: Colors.red);
//             return;
//           }
//           if (state is RequestNewInvitationErrorState) {
//             PopupHelper.showBasicSnack(msg: state.error, color: Colors.red);
//             return;
//           }
//           if (state is UseInvitationSuccessState) {
//             if (widget.isFirstTime) {
//               await TalentSignUpCubit.get(context).talentLogin();
//               return;
//             } else {
//               await TalentSignUpCubit.get(context).userValidateAndCacheUserInfo();
//               MagicRouter.navigateAndPopAll(const CompleteProfileScreen());
//               return;
//             }
//           }
//           if (state is RequestNewInvitationSuccessState) {
//             showDialog(
//               context: context,
//               barrierDismissible: false,
//               builder: (context) => VerifiedSuccessfullyDialog(
//                 text: 'invitationCodeRequestSent'.tr(),
//                 onPressed: () => MagicRouter.pop(),
//                 buttonText: 'ok'.tr().toUpperCase(),
//                 contentPadding: EdgeInsets.only(
//                   right: widget.width * 0.07,
//                   left: widget.width * 0.07,
//                   top: widget.height * 0.03,
//                   bottom: widget.height * 0.03,
//                 ),
//                 textStyle: Theme.of(context)
//                     .textTheme
//                     .titleMedium!
//                     .copyWith(fontFamily: widget.isEn ? 'Montserrat' : 'Tajawal'),
//               ),
//             );
//             return;
//           }
//           if (state is TalentLoginSuccessState) {
//             if (state.userModel.token == null || state.userModel.token == '') {
//               PopupHelper.showBasicSnack(msg: state.userModel.message.toString(), color: Colors.red);
//               return;
//             }
//             if (state.userModel.token != null) {
//               if (state.userModel.user!.verify == false) {
//                 PopupHelper.showBasicSnack(msg: 'notVerified'.tr(), color: Colors.red);
//                 await HiveHelper.logout();
//                 return;
//               }
//               // if (HiveHelper.isRegularUser) {
//               //   if (state.userModel.user!.invitationExpiryDate != null) {
//               //     if (state.userModel.user!.invitation ??
//               //         false == true &&
//               //             DateTime.now().isAfter(DateTime.parse(state.userModel.user!.invitationExpiryDate!))) {
//               //       MagicRouter.navigateAndPopAll(const InvitationEndScreen());
//               //     }
//               //   } else if (state.userModel.user!.subscriptionEnd ?? false == true) {
//               //     MagicRouter.navigateAndPopAll(const SubscriptionEndedScreen());
//               //   } else {
//               //     MagicRouter.navigateAndPopAll(Navigation());
//               //     return;
//               //   }
//               // }
//               else {
//                 MagicRouter.navigateAndPopAll(Navigation());
//                 return;
//               }
//               if (HiveHelper.getUserInfo!.user!.profileStatus == "NotCompleted") {
//                 if (HiveHelper.isRegularUser) {
//                   MagicRouter.navigateAndPopAll(const CompleteProfileScreen());
//                 } else {
//                   MagicRouter.navigateAndPopAll(Navigation());
//                 }
//               } else {
//                 MagicRouter.navigateAndPopAll(Navigation());
//               }
//             }
//           }
//           if (state is TalentLoginErrorState) {
//             PopupHelper.showBasicSnack(msg: state.error.toString(), color: Colors.red);
//           }
//         },
//         builder: (context, state) {
//           final cubit = TalentSignUpCubit.get(context);
//           Future<void> useInvitationAndLogin(TalentSignUpCubit cubit, TalentSignUpState state) async {
//             {
//               FocusManager.instance.primaryFocus?.unfocus();
//               if (cubit.talentActivationFormKey.currentState!.validate()) {
//                 await cubit.useInvitation();
//               }
//             }
//           }
//
//           return ListView(
//             shrinkWrap: true,
//             children: [
//               SizedBox(height: widget.height * 0.2),
//               Center(child: SvgPicture.asset('assets/images/fab_logo.svg', height: widget.height * 0.15)),
//               SizedBox(height: widget.height * 0.03),
//               Padding(
//                 padding: EdgeInsets.symmetric(horizontal: widget.width * 0.2),
//                 child: Text(
//                   'enterYourActivationCode'.tr(),
//                   textAlign: TextAlign.center,
//                   style: Theme.of(context).textTheme.headline5!.copyWith(
//                         fontFamily: widget.isEn ? 'Montserrat' : 'Tajawal',
//                         fontWeight: FontWeight.bold,
//                       ),
//                 ),
//               ),
//               SizedBox(height: widget.height * 0.03),
//               Padding(
//                 padding: EdgeInsets.symmetric(horizontal: widget.width * 0.15),
//                 child: Text(
//                   'addActivationCode'.tr(),
//                   textAlign: TextAlign.center,
//                   style: Theme.of(context).textTheme.titleSmall!.copyWith(
//                         fontFamily: widget.isEn ? 'Montserrat' : 'Tajawal',
//                         fontWeight: FontWeight.bold,
//                       ),
//                 ),
//               ),
//               SizedBox(height: widget.height * 0.03),
//               Form(
//                 key: cubit.talentActivationFormKey,
//                 child: Padding(
//                   padding: EdgeInsets.symmetric(horizontal: widget.width * 0.05),
//                   child: CustomTextField(
//                     hint: 'doYouHaveActivationCode'.tr(),
//                     validator: Validators.generalField,
//                     type: TextInputType.text,
//                     controller: cubit.talentActivationCodeController,
//                     onFieldSubmitted: (_) async => useInvitationAndLogin(cubit, state),
//                   ),
//                 ),
//               ),
//               SizedBox(height: widget.height * 0.03),
//               Center(
//                 child: SizedBox(
//                   height: widget.height * 0.055,
//                   width: widget.width * 0.9,
//                   child: (state is UseInvitationLoadingState ||
//                           state is TalentLoginLoadingState ||
//                           state is TalentValidateAndCacheLoadingState)
//                       ? const AppLoader()
//                       : CustomButton(
//                           onTap: () async => useInvitationAndLogin(cubit, state),
//                           child: Text('apply'.tr().toUpperCase(),
//                               style: TextStyle(
//                                   fontFamily: widget.isEn ? 'Montserrat' : 'Tajawal', fontWeight: FontWeight.bold))),
//                 ),
//               ),
//               SizedBox(height: widget.height * 0.02),
//               Center(
//                 child: SizedBox(
//                   height: widget.height * 0.055,
//                   width: widget.width * 0.9,
//                   child: (state is RequestNewInvitationLoadingState)
//                       ? const AppLoader()
//                       : OutlinedButton(
//                           onPressed: () async {
//                             FocusManager.instance.primaryFocus?.unfocus();
//                             await cubit.requestNewInvitation();
//                           },
//                           child: FittedBox(
//                             child: Text('requestInvitationCode'.tr().toUpperCase(),
//                                 style: TextStyle(
//                                     fontFamily: widget.isEn ? 'Montserrat' : 'Tajawal', fontWeight: FontWeight.bold)),
//                           ),
//                         ),
//                 ),
//               ),
//               SizedBox(height: widget.height * 0.05 + MediaQuery.of(context).viewInsets.bottom * 0.8),
//             ],
//           );
//         },
//       ),
//     );
//   }
// }
