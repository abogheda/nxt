import 'package:easy_localization/easy_localization.dart' as local;
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:intl_phone_field/country_picker_dialog.dart';
import 'package:intl_phone_field/intl_phone_field.dart';
import 'package:intl_phone_field/phone_number.dart';
import '../../../../utils/constants/resources.dart';

class CustomIntlPhoneField extends StatelessWidget {
  const CustomIntlPhoneField({
    super.key,
    this.onSaved,
    this.hintText,
    this.focusNode,
    this.onChanged,
    this.controller,
    this.onSubmitted,
    this.initialCountryCode,
    this.invalidNumberMessage,
  });
  final String? hintText;
  final FocusNode? focusNode;
  final String? initialCountryCode;
  final String? invalidNumberMessage;
  final Function(String)? onSubmitted;
  final Function(PhoneNumber?)? onSaved;
  final Function(PhoneNumber)? onChanged;
  final TextEditingController? controller;
  @override
  Widget build(BuildContext context) {
    final defaultStyle = Theme.of(context).textTheme.bodySmall!.copyWith(fontFamily: isEn ? 'Montserrat' : 'Tajawal');
    return IntlPhoneField(
      controller: controller,
      focusNode: focusNode,
      onSaved: onSaved,
      onSubmitted: onSubmitted,
      onChanged: onChanged,
      textAlign: TextAlign.left,
      dropdownTextStyle: defaultStyle,
      keyboardType: TextInputType.phone,
      invalidNumberMessage: invalidNumberMessage,
      initialCountryCode: initialCountryCode ?? 'EG',
      style: Theme.of(context).textTheme.bodyText1!.copyWith(
            fontSize: 12,
            fontWeight: FontWeight.w500,
            color: Theme.of(context).primaryColor,
            fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal',
          ),
      pickerDialogStyle: PickerDialogStyle(
        countryCodeStyle: defaultStyle,
        countryNameStyle: defaultStyle,
        searchFieldInputDecoration: InputDecoration(
          hintText: 'searchCountry'.tr(),
          hintStyle: defaultStyle,
          labelStyle: defaultStyle,
          errorStyle: defaultStyle,
          helperStyle: defaultStyle,
          prefixStyle: defaultStyle,
          suffixStyle: defaultStyle,
          counterStyle: defaultStyle,
          floatingLabelStyle: defaultStyle,
        ),
      ),
      decoration: InputDecoration(
        hintStyle: defaultStyle,
        labelStyle: defaultStyle,
        helperStyle: defaultStyle,
        suffixStyle: defaultStyle,
        prefixStyle: defaultStyle,
        counterStyle: defaultStyle,
        floatingLabelStyle: defaultStyle,
        prefixIcon: SvgPicture.asset('assets/images/phone.svg', color: AppColors.iconsColor),
        filled: true,
        hintText: hintText ?? 'phoneNumberHint'.tr(),
        errorStyle: TextStyle(fontFamily: isEn ? 'Montserrat' : 'Tajawal', fontSize: 12, height: .9),
        border: const OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(8.0))),
        enabledBorder: const OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(8.0)), borderSide: BorderSide(width: .75)),
        focusedBorder: const OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(8.0)),
            borderSide: BorderSide(color: AppColors.greyTxtColor)),
      ),
    );
  }
}
