// ignore_for_file: use_build_context_synchronously

import 'dart:io';

import 'package:custom_timer/custom_timer.dart';
import 'package:easy_localization/easy_localization.dart' as local;
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:nxt/data/service/hive_services.dart';
import '../../../../utils/constants/resources.dart';
import '../../../../config/router/router.dart';
import '../../../../utils/helpers/popup_helper.dart';
import '../../../../utils/helpers/validators.dart';
import '../../../widgets/app_loader.dart';
import '../../../widgets/custom_button.dart';
import '../../../widgets/custom_text_field.dart';
import '../../../widgets/logo_app_bar.dart';
import '../../../widgets/verified_successfully_dialog.dart';
import '../organization/organization_change_number_screen.dart';
import 'controller/organization_sign_up_cubit.dart';
import 'organization_confirm_terms_scrollable_screen.dart';

class OrganizationVerificationScreen extends StatefulWidget {
  const OrganizationVerificationScreen({Key? key}) : super(key: key);

  @override
  State<OrganizationVerificationScreen> createState() => _OrganizationVerificationScreenState();
}

class _OrganizationVerificationScreenState extends State<OrganizationVerificationScreen> {
  bool doOnce = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: Platform.isAndroid ? false : true,
      body: BlocConsumer<OrganizationSignUpCubit, OrganizationSignUpState>(
        listener: (context, state) async {
          final cubit = OrganizationSignUpCubit.get(context);
          if (state is VerifyPhoneWithCodeErrorState) {
            PopupHelper.showBasicSnack(msg: state.error, color: Colors.red);
            return;
          }
          if (state is VerifyPhoneWithCodeSuccessState) {
            await cubit.userValidateAndCacheUserInfo();
            showDialog(
              context: context,
              barrierDismissible: false,
              builder: (context) => VerifiedSuccessfullyDialog(
                onPressed: () => MagicRouter.navigateTo(BlocProvider.value(
                    value: cubit..getLegal(), child: const OrganizationConfirmTermsScrollableScreen())),
              ),
            );
          }
        },
        builder: (context, state) {
          final cubit = OrganizationSignUpCubit.get(context);
          Future<void> organizationVerifyCode() async {
            FocusManager.instance.primaryFocus?.unfocus();
            if (cubit.organizationVerifyCodeFormKey.currentState!.validate()) {
              await cubit.verifyPhoneWithCode(code: cubit.organizationVerificationCodeController.text);
            }
          }

          String getPhoneNumber() {
            String number = "notAvailable".tr();
            if (cubit.organizationPhoneController.text.isNotEmpty) {
              number = '${cubit.countryCode} ${cubit.organizationPhoneController.text.trim()}';
            } else {
              number = HiveHelper.getUserInfo!.user!.phone!.toString();
            }
            return number;
          }

          return ListView(
            shrinkWrap: true,
            children: [
              const LogoAppBar(),
              SizedBox(height: widget.height * 0.1),
              Text('welcomeToProjectNXT'.tr(),
                  textAlign: TextAlign.center,
                  style: Theme.of(context)
                      .textTheme
                      .displaySmall!
                      .copyWith(fontFamily: widget.isEn ? 'BebasNeue' : 'Tajawal', color: Colors.black)),
              SizedBox(height: widget.height * 0.05),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: widget.width * 0.15),
                child: Text(
                  'codeSent'.tr(),
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.titleMedium!.copyWith(
                        fontFamily: widget.isEn ? 'Montserrat' : 'Tajawal',
                        color: const Color(0xFF727272),
                        fontWeight: FontWeight.w400,
                      ),
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: widget.width * 0.15, vertical: widget.height * 0.03),
                child: Text(
                  getPhoneNumber(),
                  textAlign: TextAlign.center,
                  textDirection: TextDirection.ltr,
                  style: Theme.of(context).textTheme.titleLarge!.copyWith(
                        color: const Color(0xFF727272),
                        fontFamily: widget.isEn ? 'Montserrat' : 'Tajawal',
                      ),
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: widget.width * 0.3),
                child: TextButton(
                  onPressed: () => MagicRouter.navigateTo(BlocProvider.value(
                    value: cubit,
                    child: const OrganizationChangeNumberScreen(),
                  )),
                  child: Text(
                    'changeNumber'.tr(),
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.bodyText1!.copyWith(
                          fontFamily: widget.isEn ? 'Montserrat' : 'Tajawal',
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          decoration: TextDecoration.underline,
                        ),
                  ),
                ),
              ),
              SizedBox(height: widget.height * 0.01),
              Form(
                key: cubit.organizationVerifyCodeFormKey,
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: widget.width * 0.05),
                  child: CustomTextField(
                    prefix: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: SvgPicture.asset('assets/images/phone_iphone.svg', color: AppColors.iconsColor),
                    ),
                    hint: 'verificationCode'.tr(),
                    validator: Validators.generalField,
                    type: TextInputType.phone,
                    controller: cubit.organizationVerificationCodeController,
                    onFieldSubmitted: (_) async => organizationVerifyCode(),
                  ),
                ),
              ),
              SizedBox(height: widget.height * 0.03),
              Center(
                child: SizedBox(
                  height: widget.height * 0.055,
                  width: widget.width * 0.9,
                  child: (state is VerifyPhoneWithCodeLoadingState)
                      ? const AppLoader()
                      : CustomButton(
                          onTap: () async => organizationVerifyCode(),
                          child: Text('confirm'.tr().toUpperCase(),
                              style: TextStyle(
                                  fontFamily: widget.isEn ? 'Montserrat' : 'Tajawal', fontWeight: FontWeight.bold))),
                ),
              ),
              cubit.canContinue == false
                  ? Center(
                      child: CustomTimer(
                          controller: cubit.timerController,
                          begin: Duration(seconds: cubit.codeTimer == null ? 60 : cubit.codeTimer!.toInt()),
                          end: const Duration(),
                          builder: (time) {
                            if (time.duration == const Duration()) {
                              if (doOnce) {
                                doOnce = false;
                                cubit.codeTimer = null;
                                cubit.changeCanContinue(value: true);
                              }
                            }
                            return Padding(
                              padding:
                                  EdgeInsets.symmetric(horizontal: widget.width * 0.05, vertical: widget.height * 0.01),
                              child: Row(
                                children: <Widget>[
                                  SizedBox(height: widget.height * 0.01),
                                  Text('resendCodeTimer'.tr(),
                                      textAlign: TextAlign.center,
                                      style: TextStyle(fontFamily: widget.isEn ? 'Montserrat' : 'Tajawal')),
                                  const SizedBox(width: 4),
                                  Text("${time.minutes}${'m'.tr()} ${time.seconds}${'s'.tr()}",
                                      style: TextStyle(fontFamily: widget.isEn ? 'Montserrat' : 'Tajawal')),
                                ],
                              ),
                            );
                          }),
                    )
                  : const SizedBox(),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: widget.width * 0.3),
                child: TextButton(
                  onPressed: cubit.canContinue
                      ? () async => await cubit.sendCode()
                      : () => PopupHelper.showBasicSnack(msg: 'pleaseWaitForTimer'.tr(), color: Colors.red),
                  child: Text(
                    'resendCode'.tr(),
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.bodyText1!.copyWith(
                          fontFamily: widget.isEn ? 'Montserrat' : 'Tajawal',
                          decoration: TextDecoration.underline,
                        ),
                  ),
                ),
              ),
            ],
          );
        },
      ),
    );
  }
}
