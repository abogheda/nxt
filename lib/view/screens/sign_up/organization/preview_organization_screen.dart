import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nxt/utils/constants/app_const.dart';
import 'package:nxt/utils/constants/app_images.dart';
import 'package:nxt/view/widgets/app_loader.dart';

import '../../../../config/router/router.dart';
import '../../../../utils/constants/app_colors.dart';
import '../../../widgets/logo_app_bar.dart';
import '../../congratulation/congratulation_screen.dart';
import 'controller/organization_sign_up_cubit.dart';

class PreviewOrganizationScreen extends StatelessWidget {
  const PreviewOrganizationScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<OrganizationSignUpCubit, OrganizationSignUpState>(
      builder: (context, state) {
        final cubit = OrganizationSignUpCubit.get(context);
        return Scaffold(
          body: ListView(
            shrinkWrap: true,
            children: [
              const LogoAppBar(),
              SizedBox(height: height * 0.03),
              Card(
                elevation: 0,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8.0), side: const BorderSide(color: Color(0xFFC6C6C6))),
                margin: EdgeInsets.symmetric(vertical: 8, horizontal: width * 0.04),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(height: height * 0.03),
                    SizedBox(
                      height: width * 0.2,
                      width: width * 0.2,
                      child: Image.network(
                        cubit.user!.organization!.logo ?? userAvatarPlaceHolderUrl,
                        fit: BoxFit.fitWidth,
                      ),
                    ),
                    SizedBox(height: height * 0.03),
                    Text(
                      'nameOfOrganization'.tr(),
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.titleSmall!.copyWith(
                            color: AppColors.greyTxtColor,
                            fontFamily: isEn ? 'Montserrat' : 'Tajawal',
                          ),
                    ),
                    SizedBox(height: height * 0.01),
                    Text(
                      cubit.user!.organization!.name ?? "notAvailable".tr(),
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.headlineSmall!.copyWith(
                            fontFamily: isEn ? 'BebasNeue' : 'Tajawal',
                          ),
                    ),
                    SizedBox(height: height * 0.03),
                    Text(
                      'website'.tr(),
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.titleSmall!.copyWith(
                            color: AppColors.greyTxtColor,
                            fontFamily: isEn ? 'Montserrat' : 'Tajawal',
                          ),
                    ),
                    SizedBox(height: height * 0.01),
                    Text(
                      cubit.user!.organization!.website ?? "notAvailable".tr(),
                      textAlign: TextAlign.center,
                      style:
                          Theme.of(context).textTheme.titleLarge!.copyWith(fontFamily: isEn ? 'BebasNeue' : 'Tajawal'),
                    ),
                    SizedBox(height: height * 0.03),
                    Text(
                      'industry'.tr(),
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.titleSmall!.copyWith(
                            color: AppColors.greyTxtColor,
                            fontFamily: isEn ? 'Montserrat' : 'Tajawal',
                          ),
                    ),
                    SizedBox(height: height * 0.01),
                    Text(
                      cubit.user!.organization!.industry ?? "notAvailable".tr(),
                      textAlign: TextAlign.center,
                      style:
                          Theme.of(context).textTheme.titleLarge!.copyWith(fontFamily: isEn ? 'BebasNeue' : 'Tajawal'),
                    ),
                    SizedBox(height: height * 0.03),
                    Text(
                      'organizationSize'.tr(),
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.titleSmall!.copyWith(
                            color: AppColors.greyTxtColor,
                            fontFamily: isEn ? 'Montserrat' : 'Tajawal',
                          ),
                    ),
                    SizedBox(height: height * 0.01),
                    Text(
                      cubit.user!.organization!.size == null
                          ? "notAvailable".tr()
                          : '${cubit.user!.organization!.size!} ${'employees'.tr()}',
                      textAlign: TextAlign.center,
                      style:
                          Theme.of(context).textTheme.titleLarge!.copyWith(fontFamily: isEn ? 'BebasNeue' : 'Tajawal'),
                    ),
                    SizedBox(height: height * 0.03),
                    Text(
                      'organizationType'.tr(),
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.titleSmall!.copyWith(
                            color: AppColors.greyTxtColor,
                            fontFamily: isEn ? 'Montserrat' : 'Tajawal',
                          ),
                    ),
                    SizedBox(height: height * 0.01),
                    Text(
                      cubit.user!.organization!.type ?? "notAvailable".tr(),
                      textAlign: TextAlign.center,
                      style:
                          Theme.of(context).textTheme.titleLarge!.copyWith(fontFamily: isEn ? 'BebasNeue' : 'Tajawal'),
                    ),
                    SizedBox(height: height * 0.02),
                    SizedBox(
                      width: width * 0.8,
                      height: height * 0.055,
                      child: ElevatedButton(
                        onPressed: () async {
                          await cubit.organizationLogin();
                          await MagicRouter.navigateAndPopAll(const CongratulationScreen());
                        },
                        child:
                            state is OrganizationLoginLoadingState || state is OrganizationValidateAndCacheLoadingState
                                ? const AppLoader(color: Colors.white)
                                : Text(
                                    'confirmPage'.tr().toUpperCase(),
                                    style: Theme.of(context).textTheme.titleSmall!.copyWith(
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold,
                                          fontFamily: isEn ? 'Montserrat' : 'Tajawal',
                                        ),
                                  ),
                      ),
                    ),
                    SizedBox(height: height * 0.03),
                  ],
                ),
              )
            ],
          ),
        );
      },
    );
  }
}
