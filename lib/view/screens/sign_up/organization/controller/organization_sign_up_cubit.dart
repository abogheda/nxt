// ignore_for_file: unnecessary_import, depend_on_referenced_packages

import 'package:bloc/bloc.dart';
import 'package:custom_timer/custom_timer.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';
import 'package:nxt/data/models/sign_up/create_user_model.dart';

import '../../../../../data/models/more/legal_model.dart';
import '../../../../../data/models/sign_up/change_number_model.dart';
import '../../../../../data/models/sign_up/confirm_privacy_model.dart';
import '../../../../../data/models/sign_up/confirm_terms_model.dart';
import '../../../../../data/models/sign_up/send_code_response_model.dart';
import '../../../../../data/models/sign_up/sign_up_model.dart';
import '../../../../../data/models/sign_up/verify_code_model.dart';
import '../../../../../data/models/user_model.dart';
import '../../../../../data/repos/auth/auth_repo.dart';
import '../../../../../data/repos/imports.dart';
import '../../../../../data/repos/more/more_repo.dart';
import '../../../../../data/service/hive_services.dart';
import '../../../../../data/service/remote_service.dart';

part 'organization_sign_up_state.dart';

class OrganizationSignUpCubit extends Cubit<OrganizationSignUpState> {
  OrganizationSignUpCubit() : super(SignUpInitial());
  static OrganizationSignUpCubit get(context) => BlocProvider.of(context);
  num? codeTimer;
  bool canContinue = true;
  bool isEULAChecked = false;
  bool isTermsChecked = false;
  bool isPrivacyChecked = false;
  String countryCode = '';
  UserModel? userModel;
  User? user;
  User? postOrganizationInfoUser;
  SignUpModel? signUpModel;
  LegalModel? legalModel;
  VerifyCodeModel? verificationResponse;
  ChangeNumberModel? changeNumberResponse;
  ConfirmTermsModel? confirmTermsResponse;
  ConfirmPrivacyModel? confirmEULAResponse;
  ConfirmPrivacyModel? confirmPrivacyResponse;
  SendCodeResponseModel? sendCodeResponseModel;
  CustomTimerController timerController = CustomTimerController();
  TextEditingController organizationNameController = TextEditingController(),
      organizationEmailController = TextEditingController(),
      organizationPhoneController = TextEditingController(),
      organizationPasswordController = TextEditingController(),
      organizationConfirmPasswordController = TextEditingController(),
      organizationVerificationCodeController = TextEditingController(),
      organizationActivationCodeController = TextEditingController(),
      organizationLinkController = TextEditingController(),
      organizationScopeController = TextEditingController(),
      organizationChangeNumberController = TextEditingController();
  FocusNode organizationEmailFocusNode = FocusNode(),
      organizationBirthFocusNode = FocusNode(),
      organizationPhoneFocusNode = FocusNode(),
      organizationActivationCodeFocusNode = FocusNode(),
      organizationPasswordFocusNode = FocusNode(),
      organizationLinkFocusNode = FocusNode(),
      organizationScopeFocusNode = FocusNode(),
      organizationConfirmPasswordFocusNode = FocusNode();
  final organizationSignUpFormKey = GlobalKey<FormState>(),
      organizationDetailsFormKey = GlobalKey<FormState>(),
      organizationChangeNumberFormKey = GlobalKey<FormState>(),
      organizationActivationFormKey = GlobalKey<FormState>(),
      organizationVerifyCodeFormKey = GlobalKey<FormState>();

  @override
  Future<void> close() {
    organizationNameController.dispose();
    organizationEmailController.dispose();
    organizationPhoneController.dispose();
    organizationPasswordController.dispose();
    organizationConfirmPasswordController.dispose();
    organizationVerificationCodeController.dispose();
    organizationActivationCodeController.dispose();
    organizationLinkController.dispose();
    organizationScopeController.dispose();
    organizationChangeNumberController.dispose();
    organizationEmailFocusNode.dispose();
    organizationBirthFocusNode.dispose();
    organizationPhoneFocusNode.dispose();
    organizationActivationCodeFocusNode.dispose();
    organizationPasswordFocusNode.dispose();
    organizationLinkFocusNode.dispose();
    organizationScopeFocusNode.dispose();
    organizationConfirmPasswordFocusNode.dispose();
    timerController.dispose();
    return super.close();
  }

  void changeTermsCheckBox() => emit(ChangeTermsCheckBoxState());

  void changePrivacyCheckBox() => emit(ChangePrivacyCheckBoxState());

  void changeEULACheckBox() => emit(ChangeEULACheckBoxState());

  Future<void> sendCodeAndStartTimer() async {
    await sendCode();
    changeCanContinue(value: false);
    timerController.start();
  }

  Future<void> organizationSignUp() async {
    signUpModel = null;
    await HiveHelper.logout();
    emit(OrganizationSignUpLoadingState());
    try {
      final res = await AuthRepo.organizationSignUp(
        createUserModel: CreateUserModel(
          email: organizationEmailController.text.trim(),
          password: organizationPasswordController.text.trim(),
          phone: countryCode + organizationPhoneController.text.trim(),
          activationCode: organizationActivationCodeController.text.trim(),
        ),
      );
      if (res.data != null) {
        signUpModel = res.data;
        if (signUpModel!.message != null) {
          emit(OrganizationSignUpErrorState(error: signUpModel!.message ?? 'normalErrorMsg'.tr()));
          return;
        }
        if (signUpModel!.token != null) APIService.setHeaderToken(token: signUpModel!.token);
        await HiveHelper.cacheKeepMeLoggedIn(value: true);
        await HiveHelper.cacheUserInfo(
            token: signUpModel!.token!.toString(),
            userModel: UserModel(user: signUpModel!.newUser, token: signUpModel!.token!.toString()));
        HiveHelper.cacheProfileInfo(
          name: signUpModel!.newUser!.name,
          avatar: signUpModel!.newUser!.avatar,
          birthDate: signUpModel!.newUser!.birthDate,
        );
        await HiveHelper.checkAndCacheCountry(userPhone: signUpModel!.newUser!.phone);
        emit(OrganizationSignUpSuccessState(signUpModel: signUpModel!));
      } else {
        emit(OrganizationSignUpErrorState(error: res.message!));
      }
    } catch (e) {
      debugPrint(e.toString());
      emit(OrganizationSignUpErrorState(error: 'normalErrorMsg'.tr()));
    }
  }

  Future<void> sendCode() async {
    sendCodeResponseModel = null;
    emit(SendCodeLoadingState());
    final res = await AuthRepo.sendCode();
    if (res.data != null) {
      sendCodeResponseModel = res.data;
      if (sendCodeResponseModel.toString() == 'ThrottlerException: Too Many Requests') {
        emit(SendCodeErrorState(error: sendCodeResponseModel.toString()));
      }
      if (sendCodeResponseModel != null) {
        codeTimer = sendCodeResponseModel!.remaining;
        emit(SendCodeSuccessState());
      }
      emit(SendCodeSuccessState());
    } else {
      emit(SendCodeErrorState(error: res.message!));
    }
  }

  Future<void> verifyPhoneWithCode({required String code}) async {
    emit(VerifyPhoneWithCodeLoadingState());
    final res = await AuthRepo.verifyPhoneWithCode(
      code: int.tryParse(code)!,
    );
    if (res.message == null) {
      verificationResponse = res.data;
      emit(VerifyPhoneWithCodeSuccessState());
    } else {
      emit(VerifyPhoneWithCodeErrorState(error: res.message!));
    }
  }

  Future<void> changeNumber() async {
    changeNumberResponse = null;
    emit(ChangeNumberLoadingState());
    final res = await AuthRepo.changeNumber(phone: countryCode + organizationChangeNumberController.text.trim());
    if (res.data != null) {
      changeNumberResponse = res.data;
      emit(ChangeNumberSuccessState(user: changeNumberResponse!));
    } else {
      emit(ChangeNumberErrorState(error: res.message!));
    }
  }

  Future<void> confirmTerms() async {
    emit(ConfirmTermsLoadingState());
    final res = await AuthRepo.confirmTerms();
    if (res.data != null) {
      confirmTermsResponse = res.data;
      emit(ConfirmTermsSuccessState());
    } else {
      emit(ConfirmTermsErrorState(error: res.message!));
    }
  }

  Future<void> confirmPrivacy() async {
    emit(ConfirmPrivacyLoadingState());
    final res = await AuthRepo.confirmPrivacy();
    if (res.data != null) {
      confirmPrivacyResponse = res.data;
      emit(ConfirmPrivacySuccessState());
    } else {
      emit(ConfirmPrivacyErrorState(error: res.message!));
    }
  }

  Future<void> confirmEULA() async {
    emit(ConfirmEULALoadingState());
    final res = await AuthRepo.confirmEULA();
    if (res.data != null) {
      confirmEULAResponse = res.data;
      emit(ConfirmEULASuccessState());
    } else {
      emit(ConfirmEULAErrorState(error: res.message!));
    }
  }

  Future<void> getLegal() async {
    emit(GetLegalLoadingState());
    final res = await MoreRepo.getLegal();
    if (res.data != null) {
      legalModel = res.data;
      emit(GetLegalSuccessState());
    } else {
      emit(GetLegalErrorState(error: res.message ?? ""));
    }
  }

  void changeCanContinue({required bool value}) {
    canContinue = value;
    emit(ChangeContinueState());
  }

  Future<void> organizationLogin() async {
    emit(OrganizationLoginLoadingState());
    if (organizationPasswordController.value.text.isNotEmpty) {
      await HiveHelper.logout();
      final res = await AuthRepo.userLogin(
        phone: countryCode + organizationPhoneController.text.trim().toString(),
        password: organizationPasswordController.text.trim(),
      );
      if (res.data != null) {
        userModel = res.data;
        if (userModel!.token != null) {
          APIService.setHeaderToken(token: userModel!.token);
          await HiveHelper.cacheUserInfo(token: userModel!.token!.toString(), userModel: userModel!);
          await HiveHelper.checkAndCacheCountry(userPhone: userModel!.user!.phone);
          await HiveHelper.cacheProfileInfo(
            name: userModel!.user!.organization!.name,
            avatar: userModel!.user!.organization!.logo,
            birthDate: DateTime.now().toIso8601String(),
          );
        }
        emit(OrganizationLoginSuccessState(userModel: userModel!));
      } else {
        emit(OrganizationLoginErrorState(error: res.message!));
      }
    } else {
      await userValidateAndCacheUserInfo();
    }
  }

  Future<void> userValidateAndCacheUserInfo() async {
    emit(OrganizationValidateAndCacheLoadingState());
    final res = await ProfileRepo.getUserValidate();
    if (res.data != null) {
      user = res.data;
      if (user!.phone != null) {
          await HiveHelper.cacheUserModel(userModel: HiveHelper.getUserInfo!.copyWith(user: user));
      }
      emit(OrganizationValidateAndCacheSuccessState(user: user!));
    } else {
      emit(OrganizationValidateAndCacheErrorState(error: res.message!));
    }
  }
}
