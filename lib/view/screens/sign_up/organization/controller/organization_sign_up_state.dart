part of 'organization_sign_up_cubit.dart';

@immutable
abstract class OrganizationSignUpState {}

class SignUpInitial extends OrganizationSignUpState {}

class ChangeContinueState extends OrganizationSignUpState {}

class ChangeTermsCheckBoxState extends OrganizationSignUpState {}

class ChangePrivacyCheckBoxState extends OrganizationSignUpState {}

class ChangeEULACheckBoxState extends OrganizationSignUpState {}

class SignUpLoadingState extends OrganizationSignUpState {}

class SignUpSuccessState extends OrganizationSignUpState {
  final SignUpModel signUpModel;
  SignUpSuccessState({required this.signUpModel});
}

class SignUpErrorState extends OrganizationSignUpState {
  final String error;
  SignUpErrorState({required this.error});
}

class SendCodeLoadingState extends OrganizationSignUpState {}

class SendCodeSuccessState extends OrganizationSignUpState {}

class SendCodeErrorState extends OrganizationSignUpState {
  final String error;
  SendCodeErrorState({required this.error});
}

class VerifyPhoneWithCodeLoadingState extends OrganizationSignUpState {}

class VerifyPhoneWithCodeSuccessState extends OrganizationSignUpState {}

class VerifyPhoneWithCodeErrorState extends OrganizationSignUpState {
  final String error;
  VerifyPhoneWithCodeErrorState({required this.error});
}

class ChangeNumberLoadingState extends OrganizationSignUpState {}

class ChangeNumberSuccessState extends OrganizationSignUpState {
  final ChangeNumberModel user;
  ChangeNumberSuccessState({required this.user});
}

class ChangeNumberErrorState extends OrganizationSignUpState {
  final String error;
  ChangeNumberErrorState({required this.error});
}

class GetLegalLoadingState extends OrganizationSignUpState {}

class GetLegalSuccessState extends OrganizationSignUpState {}

class GetLegalErrorState extends OrganizationSignUpState {
  final String error;
  GetLegalErrorState({required this.error});
}

class ConfirmTermsLoadingState extends OrganizationSignUpState {}

class ConfirmTermsSuccessState extends OrganizationSignUpState {}

class ConfirmTermsErrorState extends OrganizationSignUpState {
  final String error;
  ConfirmTermsErrorState({required this.error});
}

class ConfirmPrivacyLoadingState extends OrganizationSignUpState {}

class ConfirmPrivacySuccessState extends OrganizationSignUpState {}

class ConfirmPrivacyErrorState extends OrganizationSignUpState {
  final String error;
  ConfirmPrivacyErrorState({required this.error});
}

class ConfirmEULALoadingState extends OrganizationSignUpState {}

class ConfirmEULASuccessState extends OrganizationSignUpState {}

class ConfirmEULAErrorState extends OrganizationSignUpState {
  final String error;
  ConfirmEULAErrorState({required this.error});
}

class OrganizationSignUpLoadingState extends OrganizationSignUpState {}

class OrganizationSignUpSuccessState extends OrganizationSignUpState {
  final SignUpModel signUpModel;
  OrganizationSignUpSuccessState({required this.signUpModel});
}

class OrganizationSignUpErrorState extends OrganizationSignUpState {
  final String error;
  OrganizationSignUpErrorState({required this.error});
}

class OrganizationPostInfoLoadingState extends OrganizationSignUpState {}

class OrganizationPostInfoSuccessState extends OrganizationSignUpState {
  final User user;
  OrganizationPostInfoSuccessState({required this.user});
}

class OrganizationPostInfoErrorState extends OrganizationSignUpState {
  final String error;
  OrganizationPostInfoErrorState({required this.error});
}

class UploadOrganizationLogoLoading extends OrganizationSignUpState {}

class UploadOrganizationLogoSuccess extends OrganizationSignUpState {}

class UploadOrganizationImageFailed extends OrganizationSignUpState {
  final String msg;
  UploadOrganizationImageFailed(this.msg);
}

class OrganizationLoginLoadingState extends OrganizationSignUpState {}

class OrganizationLoginSuccessState extends OrganizationSignUpState {
  final UserModel userModel;
  OrganizationLoginSuccessState({required this.userModel});
}

class OrganizationLoginErrorState extends OrganizationSignUpState {
  final String error;
  OrganizationLoginErrorState({required this.error});
}

class OrganizationValidateAndCacheLoadingState extends OrganizationSignUpState {}

class OrganizationValidateAndCacheSuccessState extends OrganizationSignUpState {
  final User user;
  OrganizationValidateAndCacheSuccessState({required this.user});
}

class OrganizationValidateAndCacheErrorState extends OrganizationSignUpState {
  final String error;
  OrganizationValidateAndCacheErrorState({required this.error});
}
