import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import '../../../utils/constants/resources.dart';
import 'expert/expert_sign_up_screen.dart';
import 'organization/organization_sign_up_screen.dart';
import 'talent/talent_sign_up_screen.dart';
import '../../../config/router/router.dart';
import '../../widgets/custom_button.dart';
import '../../widgets/logo_app_bar.dart';
import '../login/widgets/header_text.dart';

class SignUpScreen extends StatelessWidget {
  const SignUpScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        shrinkWrap: true,
        children: [
          const LogoAppBar(),
          SizedBox(height: height * 0.1),
          const HeaderText(),
          Padding(
            padding: EdgeInsets.only(top: height * 0.015, bottom: height * 0.02),
            child: Text(
              'singUp'.tr().toUpperCase(),
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                    fontFamily: isEn ? 'BebasNeue' : 'Tajawal',
                    color: AppColors.greyTxtColor,
                  ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(bottom: height * 0.02),
            child: Text(
              'as'.tr().toUpperCase(),
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.titleMedium!.copyWith(
                    fontFamily: isEn ? 'Montserrat' : 'Tajawal',
                    color: AppColors.greyTxtColor,
                    fontWeight: FontWeight.bold,
                  ),
            ),
          ),
          Container(
              padding: EdgeInsets.symmetric(horizontal: width * 0.06),
              height: height * 0.055,
              child: CustomButton(
                  onTap: () => MagicRouter.navigateTo(const TalentSignUpScreen()),
                  width: double.infinity,
                  height: double.infinity,
                  child: Text('talent'.tr().toUpperCase(),
                      style: TextStyle(fontFamily: isEn ? 'Montserrat' : 'Tajawal', fontWeight: FontWeight.bold)))),
          SizedBox(height: width * 0.07),
          Container(
              padding: EdgeInsets.symmetric(horizontal: width * 0.06),
              height: height * 0.055,
              child: CustomButton(
                  onTap: () => MagicRouter.navigateTo(const OrganizationSignUpScreen()),
                  width: double.infinity,
                  height: double.infinity,
                  child: Text('organization'.tr().toUpperCase(),
                      style: TextStyle(fontFamily: isEn ? 'Montserrat' : 'Tajawal', fontWeight: FontWeight.bold)))),
          SizedBox(height: width * 0.07),
          Container(
              padding: EdgeInsets.symmetric(horizontal: width * 0.06),
              height: height * 0.055,
              child: CustomButton(
                  onTap: () => MagicRouter.navigateTo(const ExpertSignUpScreen()),
                  width: double.infinity,
                  height: double.infinity,
                  child: Text('industryExperts'.tr().toUpperCase(),
                      style: TextStyle(fontFamily: isEn ? 'Montserrat' : 'Tajawal', fontWeight: FontWeight.bold)))),
        ],
      ),
    );
  }
}
