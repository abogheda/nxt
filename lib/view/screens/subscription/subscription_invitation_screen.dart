// ignore_for_file: use_build_context_synchronously

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:nxt/utils/constants/resources.dart';
import '../../../utils/helpers/utils.dart';
import '../../widgets/custom_button.dart';
import '../navigation_and_appbar/import_widget.dart';

class SubscriptionInvitationScreen extends StatelessWidget {
  const SubscriptionInvitationScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomScaffoldWidget(
      extendBodyBehindAppBar: false,
      isBackArrow: true,
      isThereLogo: true,
      isThereActions: true,
      body: Stack(
        children: <Widget>[
          const Positioned(
            top: 0,
            bottom: 0,
            right: 0,
            left: 0,
            child: Text('text', style: TextStyle()),
          ),
          Positioned(
            right: 0,
            left: 0,
            bottom: 0,
            child: Container(
              color: Colors.black,
              child: CustomButton(
                height: height * 0.08,
                width: double.infinity,
                onTap: () async => await Utils.launchAppUrl(
                    url: AppConst.isProduction
                        ? "${AppConst.productionUrl}/upgrade-package/"
                        : "${AppConst.developmentUrl}/upgrade-package/"),
                child: Text(
                  'upgrade'.tr().toUpperCase(),
                  style: TextStyle(
                    fontSize: 18,
                    color: Colors.white,
                    fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal',
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
