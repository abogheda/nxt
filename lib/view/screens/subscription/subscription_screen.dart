// ignore_for_file: use_build_context_synchronously

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:nxt/utils/constants/resources.dart';
import '../../../utils/helpers/utils.dart';
import '../../widgets/custom_button.dart';
import '../navigation_and_appbar/import_widget.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nxt/view/screens/home/cubit/home_cubit.dart';
import 'package:nxt/view/screens/more/cubit/more_cubit.dart';
import 'package:nxt/view/screens/subscription/widgets/benefit_text.dart';

import '../../widgets/error_state_widget.dart';
import '../../widgets/loading_state_widget.dart';

class SubscriptionScreen extends StatelessWidget {
  const SubscriptionScreen({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return CustomScaffoldWidget(
      extendBodyBehindAppBar: false,
      isBackArrow: true,
      isThereLogo: true,
      isThereActions: true,
      body: Stack(
        children: <Widget>[
          Positioned(
            top: 0,
            bottom: 0,
            right: 0,
            left: 0,
            child: BlocBuilder<MoreCubit, MoreState>(
              builder: (context, state) {
                final cubit = MoreCubit.get(context);
                if (state is GetPlanFailed) {
                  return ErrorStateWidget(
                    hasRefresh: true,
                    onRefresh: () async => await cubit.getCurrentPlan(),
                  );
                } else if (state is GetPlanLoading) {
                  return const LoadingStateWidget();
                }
                final planModel = cubit.planModel!;
                return RefreshIndicator(
                  onRefresh: () async {
                    await cubit.getCurrentPlan();
                    await context.read<HomeCubit>().getUserData();
                  },
                  child: ListView(
                    children: [
                      Container(color: Colors.black, width: double.infinity, height: 1.5),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 36),
                        child: Text(
                          'mySubscription'.tr(),
                          textAlign: TextAlign.center,
                          style: Theme.of(context).textTheme.headline3!.copyWith(
                                fontFamily: isEn ? 'BebasNeue' : 'Tajawal',
                                color: Colors.black,
                              ),
                        ),
                      ),
                      Card(
                        elevation: 6,
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
                        margin: EdgeInsets.symmetric(vertical: 8, horizontal: width * 0.15),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            SizedBox(height: height * 0.05),
                            Text(
                              planModel.package!.plan!.title ?? "notAvailable".tr(),
                              textAlign: TextAlign.center,
                              style: Theme.of(context).textTheme.headline5!.copyWith(
                                    fontFamily: isEn ? 'Montserrat' : 'Tajawal',
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold,
                                  ),
                            ),
                            const SizedBox(height: 12),
                            Text(
                              planModel.package!.plan!.supTitle ?? "notAvailable".tr(),
                              textAlign: TextAlign.center,
                              style: Theme.of(context).textTheme.bodyText1!.copyWith(
                                    fontFamily: isEn ? 'Montserrat' : 'Tajawal',
                                    color: Colors.black,
                                    fontWeight: FontWeight.normal,
                                  ),
                            ),
                            const SizedBox(height: 24),
                            Row(
                              mainAxisSize: MainAxisSize.min,
                              crossAxisAlignment: CrossAxisAlignment.baseline,
                              textBaseline: TextBaseline.alphabetic,
                              children: [
                                Column(
                                  mainAxisSize: MainAxisSize.min,
                                  verticalDirection: VerticalDirection.up,
                                  children: [
                                    Text(
                                      planModel.package!.plan!.price == null
                                          ? "notAvailable".tr()
                                          : "${planModel.package!.plan!.price} ",
                                      textAlign: TextAlign.center,
                                      style: Theme.of(context)
                                          .textTheme
                                          .headline3!
                                          .copyWith(fontFamily: isEn ? 'BebasNeue' : 'Tajawal', color: Colors.black),
                                    ),
                                  ],
                                ),
                                Text(
                                  'egp'.tr(),
                                  textAlign: TextAlign.center,
                                  style: Theme.of(context)
                                      .textTheme
                                      .headline6!
                                      .copyWith(fontFamily: isEn ? 'BebasNeue' : 'Tajawal', color: Colors.black),
                                ),
                              ],
                            ),
                            Container(
                              margin: EdgeInsets.symmetric(vertical: 8, horizontal: width * 0.04),
                              padding: EdgeInsets.symmetric(vertical: height * 0.02, horizontal: width * 0.04),
                              decoration: BoxDecoration(
                                  color: const Color(0xFFF7F7F7), borderRadius: BorderRadius.circular(10)),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: planModel.package!.plan!.features == null
                                    ? [BenefitText(text: "notAvailable".tr())]
                                    : planModel.package!.plan!.features!
                                        .map((feature) => BenefitText(text: feature))
                                        .toList(),
                              ),
                            ),
                            SizedBox(height: height * 0.02),
                          ],
                        ),
                      ),
                      SizedBox(height: height * 0.08),
                    ],
                  ),
                );
              },
            ),
          ),
          Positioned(
            right: 0,
            left: 0,
            bottom: 0,
            child: Container(
              color: Colors.black,
              child: CustomButton(
                height: height * 0.08,
                width: double.infinity,
                onTap: () async => await Utils.launchAppUrl(
                    url: AppConst.isProduction
                        ? "${AppConst.productionUrl}/upgrade-package/"
                        : "${AppConst.developmentUrl}/upgrade-package/"),
                child: Text(
                  'upgrade'.tr().toUpperCase(),
                  style: TextStyle(
                    fontSize: 18,
                    color: Colors.white,
                    fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal',
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
