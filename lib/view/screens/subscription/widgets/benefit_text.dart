import 'package:flutter/material.dart';
import '../../../../utils/constants/resources.dart';

class BenefitText extends StatelessWidget {
  const BenefitText({Key? key, required this.text}) : super(key: key);
  final String text;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 4.0, horizontal: width * 0.03),
      child: Center(
        child: Row(
          children: <Widget>[
            const Icon(Icons.check_circle_rounded, color: Color(0xFF54BD95)),
            SizedBox(width: width * 0.03),
            Expanded(
              child: Text(text,
                  style: Theme.of(context).textTheme.bodyText1!.copyWith(
                        color: Colors.black,
                        fontWeight: FontWeight.w600,
                        fontFamily: isEn ? 'Montserrat' : 'Tajawal',
                      )),
            )
          ],
        ),
      ),
    );
  }
}
