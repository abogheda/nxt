import 'package:flutter/material.dart';
import 'animated_column.dart';

import '../../../utils/constants/app_const.dart';
import '../../widgets/animated_logo.dart';

class ClickToStartScreen extends StatefulWidget {
  const ClickToStartScreen({Key? key}) : super(key: key);

  @override
  State<ClickToStartScreen> createState() => _ClickToStartScreenState();
}

class _ClickToStartScreenState extends State<ClickToStartScreen> {
  int index = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(children: [
        const AnimatedColumn(),
        Positioned(
          top: 0,
          left: 0,
          right: 0,
          bottom: widget.height * 0.1,
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: widget.width * 0.1),
            child: const AnimatedLogo(),
          ),
        ),
      ]),
    );
  }
}
