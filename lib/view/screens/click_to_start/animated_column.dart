import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:nxt/config/router/router.dart';
import '../../../utils/constants/resources.dart';
import '../on_boarding/on_boarding_screen.dart';

class AnimatedColumn extends StatefulWidget {
  const AnimatedColumn({
    Key? key,
  }) : super(key: key);

  @override
  State<AnimatedColumn> createState() => _AnimatedColumnState();
}

class _AnimatedColumnState extends State<AnimatedColumn> with SingleTickerProviderStateMixin {
  var _opacity = 0.0;
  bool canClick = false;
  @override
  void initState() {
    super.initState();
    Future.delayed(
      const Duration(milliseconds: 2000),
      () {
        setState(() {
          _opacity = 1.0;
          canClick = true;
        });
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: GestureDetector(
        onTap: () {
          if (canClick) MagicRouter.navigateTo(const OnBoardingScreen());
        },
        child: AnimatedOpacity(
          opacity: _opacity,
          duration: const Duration(milliseconds: 1000),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              SvgPicture.asset('assets/images/three_arrow.svg', height: widget.height * 0.035),
              SizedBox(height: widget.height * 0.015),
              Text(
                'clickToStart'.tr(),
                style: widget.isEn
                    ? Theme.of(context).textTheme.titleMedium!.copyWith(
                        letterSpacing: 1.5,
                        fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal',
                        fontWeight: FontWeight.w900)
                    : Theme.of(context).textTheme.titleMedium!.copyWith(
                          fontSize: 18.0,
                          fontWeight: FontWeight.w600,
                          fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal',
                        ),
              ),
              const SizedBox(height: 4),
              Text('all_rights'.tr(),
                  style: widget.isEn
                      ? Theme.of(context).textTheme.titleSmall!.copyWith(
                            fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal',
                            fontWeight: FontWeight.w900,
                            fontSize: 8,
                          )
                      : Theme.of(context)
                          .textTheme
                          .titleSmall!
                          .copyWith(fontWeight: FontWeight.w400, fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal')),
              SizedBox(height: widget.height * 0.03),
            ],
          ),
        ),
      ),
    );
  }
}
