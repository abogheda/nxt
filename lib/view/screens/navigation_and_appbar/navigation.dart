// ignore_for_file: must_be_immutable
part of 'import_widget.dart';

class Navigation extends StatefulWidget {
  Navigation({
    Key? key,
    this.navigationIndex = 0,
    this.librarySelectedPage = 0,
    // this.onLogoTap,
  }) : super(key: key);
  int navigationIndex;
  int librarySelectedPage;
  // final Function()? onLogoTap;
  @override
  State<Navigation> createState() => _NavigationState();
}

class _NavigationState extends State<Navigation> {
  final DynamicLinkService dynamicLinkService = DynamicLinkService();
  @override
  void initState() {
    checkNewVersion(context);
    FacebookEventsHelper.logEvent(eventName: "home_init");
    // Utils.checkSafeDevice(context);
    handleDynamicLinks();
    FlutterLocalNotificationUtils.instance.checkIsAllowed(context);
    super.initState();
  }

  Future<void> handleDynamicLinks() async {
    if (HiveHelper.isLogged) await dynamicLinkService.handleDynamicLinks();
  }

  @override
  Widget build(BuildContext context) {
    final hiveUser = HiveHelper.getUserInfo!.user!;
    if (HiveHelper.isRegularUser) {
      //==== talent handle sign up steps ====
      // if (hiveUser.verify == false) {
      //   return BlocProvider(
      //     create: (context) => TalentSignUpCubit()..sendCodeAndStartTimer(),
      //     child: const TalentVerificationScreen(),
      //   );
      // }
      // if (hiveUser.terms == false) {
      //   return BlocProvider(
      //     create: (context) => TalentSignUpCubit()..getLegal(),
      //     child: const TalentConfirmTermsScrollableScreen(),
      //   );
      // }
      // if (hiveUser.privacy == false) {
      //   return BlocProvider(
      //     create: (context) => TalentSignUpCubit()..getLegal(),
      //     child: const TalentConfirmPrivacyScrollableScreen(),
      //   );
      // }
      // if (hiveUser.eula == false) {
      //   return BlocProvider(
      //     create: (context) => TalentSignUpCubit()..getLegal(),
      //     child: const TalentConfirmEULAScrollableScreen(),
      //   );
      // }
      // if ((hiveUser.invitation ?? false) == false) {
      //   return BlocProvider(
      //       create: (context) => TalentSignUpCubit(), child: const TalentInvitationScreen(isFirstTime: false));
      // }
      // if (hiveUser.invitationExpiryDate != null) {
      //   if (((hiveUser.invitation ?? false) == true) &&
      //       DateTime.now().isAfter(DateTime.parse(hiveUser.invitationExpiryDate!))) {
      //     return const InvitationEndScreen();
      //   }
      // }
      // if (hiveUser.subscriptionEnd ?? false == true) {
      //   return const SubscriptionEndedScreen();
      // }
    }
    //==== expert handle sign up steps ====
    else if (hiveUser.role == 'Expert') {
      if (hiveUser.verify == false) {
        return BlocProvider(
          create: (context) => ExpertSignUpCubit()..sendCodeAndStartTimer(),
          child: const ExpertVerificationScreen(),
        );
      }
      if (hiveUser.terms == false) {
        return BlocProvider(
          create: (context) => ExpertSignUpCubit()..getLegal(),
          child: const ExpertConfirmTermsScrollableScreen(),
        );
      }
      if (hiveUser.privacy == false) {
        return BlocProvider(
          create: (context) => ExpertSignUpCubit()..getLegal(),
          child: const ExpertConfirmPrivacyScrollableScreen(),
        );
      }
      if (hiveUser.eula == false) {
        return BlocProvider(
          create: (context) => ExpertSignUpCubit()..getLegal(),
          child: const ExpertConfirmEULAScrollableScreen(),
        );
      }
      if (hiveUser.expert == null) {
        return BlocProvider(
          create: (context) => ExpertSignUpCubit(),
          child: const ExpertDetailsScreen(),
        );
      }
    }
    //==== organization handle sign up steps ====
    else if (hiveUser.role == 'Organization') {
      if (hiveUser.verify == false) {
        return BlocProvider(
          create: (context) => OrganizationSignUpCubit()..sendCodeAndStartTimer(),
          child: const OrganizationVerificationScreen(),
        );
      }
      if (hiveUser.terms == false) {
        return BlocProvider(
          create: (context) => OrganizationSignUpCubit()..getLegal(),
          child: const OrganizationConfirmTermsScrollableScreen(),
        );
      }
      if (hiveUser.privacy == false) {
        return BlocProvider(
          create: (context) => OrganizationSignUpCubit()..getLegal(),
          child: const OrganizationConfirmPrivacyScrollableScreen(),
        );
      }
      if (hiveUser.eula == false) {
        return BlocProvider(
          create: (context) => OrganizationSignUpCubit()..getLegal(),
          child: const OrganizationConfirmEULAScrollableScreen(),
        );
      }
    }
    return CustomScaffoldWidget(
      isThereLogo: true,
      isBackArrow: false,
      isInProfile: false,
      isThereActions: true,
      isLogoClickable: false,
      extendBodyBehindAppBar: false,
      callOnWillPop: true,
      backgroundColor: Colors.white,
      body: Scaffold(
        body: [
          const HomeScreen(),
          HiveHelper.isRegularUser ? LearnScreen(librarySelectedPage: widget.librarySelectedPage) : const ScoutScreen(),
          const ChallengesScreen(),
          HiveHelper.isRegularUser ? const ConnectScreen() : const OrganizationConnectScreen(),
          const MoreScreen(),
        ][widget.navigationIndex],
        bottomNavigationBar: BottomNavigationBar(
          selectedItemColor: Colors.black,
          type: BottomNavigationBarType.fixed,
          currentIndex: widget.navigationIndex,
          unselectedItemColor: AppColors.greyOutText,
          onTap: (index) => setState(() => widget.navigationIndex = index),
          selectedLabelStyle: TextStyle(fontFamily: widget.isEn ? 'Inter-Medium' : 'Tajawal', fontSize: 10),
          unselectedLabelStyle: TextStyle(fontFamily: widget.isEn ? 'Inter-Medium' : 'Tajawal', fontSize: 10),
          items: [
            buildBottomNavigationBarItem(
              iconPath: 'assets/images/home_outlined.svg',
              activeIconPath: 'assets/images/home.svg',
              label: 'home',
            ),
            HiveHelper.isRegularUser
                ? buildBottomNavigationBarItem(
                    iconPath: 'assets/images/library_outlined.svg',
                    activeIconPath: 'assets/images/library_filled.svg',
                    label: 'learn',
                  )
                : buildBottomNavigationBarItem(
                    iconPath: 'assets/images/scout.svg',
                    activeIconPath: 'assets/images/scout.svg',
                    label: 'scout',
                  ),
            buildBottomNavigationBarItem(
              iconPath: 'assets/images/challenges_outlined.svg',
              activeIconPath: 'assets/images/challenges.svg',
              label: 'compete',
            ),
            buildBottomNavigationBarItem(
              iconPath: 'assets/images/community_outlined.svg',
              activeIconPath: 'assets/images/community.svg',
              label: 'connect',
            ),
            buildBottomNavigationBarItem(
              iconPath: 'assets/images/more.svg',
              activeIconPath: 'assets/images/more.svg',
              label: 'more',
            ),
          ],
        ),
      ),
    );
  }

  BottomNavigationBarItem buildBottomNavigationBarItem(
      {required String iconPath, required String activeIconPath, required String label}) {
    return BottomNavigationBarItem(
      backgroundColor: Colors.white,
      icon: SizedBox(
          height: widget.height * 0.03,
          child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 2),
              child: SvgPicture.asset(iconPath, color: AppColors.greyNavbar))),
      activeIcon: SizedBox(
          height: widget.height * 0.03,
          child: Padding(padding: const EdgeInsets.symmetric(vertical: 2), child: SvgPicture.asset(activeIconPath))),
      label: label.tr().toUpperCase(),
    );
  }
}
