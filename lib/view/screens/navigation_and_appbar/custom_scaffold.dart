// ignore_for_file: must_be_immutable

part of 'import_widget.dart';

class CustomScaffoldWidget extends StatefulWidget {
  Widget? body;
  Color? backgroundColor;
  bool isBackArrow;
  bool isThereLogo;
  bool isThereActions;
  bool extendBodyBehindAppBar;
  bool isLogoClickable;
  bool isInProfile;
  bool isInNotification;
  bool callOnWillPop;
  CustomScaffoldWidget({
    Key? key,
    this.body,
    this.backgroundColor,
    this.isBackArrow = false,
    this.isThereLogo = true,
    this.extendBodyBehindAppBar = false,
    this.isThereActions = true,
    this.isLogoClickable = true,
    this.isInProfile = false,
    this.isInNotification = false,
    this.callOnWillPop = false,
  }) : super(key: key);

  @override
  State<CustomScaffoldWidget> createState() => _CustomScaffoldWidgetState();
}

class _CustomScaffoldWidgetState extends State<CustomScaffoldWidget> {
  String? filterValue;
  late String selectedItem;
  Color? filterColor;

  @override
  void initState() {
    final currentFilter = context.read<HomeCubit>().categoryType;
    selectedItem = currentFilter?.toString() ?? 'all'.tr().toUpperCase();
    filterColor = currentFilter?.color();
    super.initState();
  }

  void onFilterChange({required BuildContext context, TalentCategoryType? filter}) {
    context.read<HomeCubit>().categoryType = filter;
    context.read<HomeCubit>().getAllHomeData(filter: filter);
    context.read<ClassCubit>().getAllClasses(filter: filter);
    context.read<ChallengeCubit>().getAllChallenges(filter: filter);
    context.read<ChallengeCubit>().getJoinedChallenges(filter: filter);
    context.read<ChallengeCubit>().getWonChallenges(filter: filter);
  }

  @override
  Widget build(BuildContext context) {
    TextStyle returnStyle({required BuildContext context, required Color color}) =>
        Theme.of(context).textTheme.subtitle1!.copyWith(
            color: color,
            fontFamily: AppConst.isEn ? 'BebasNeue' : 'Tajawal',
            fontWeight: AppConst.isEn ? null : FontWeight.bold);
    return WillPopScope(
      onWillPop: () async => widget.callOnWillPop ? showExitPopup(context) : pop(context),
      child: Scaffold(
        body: widget.body,
        extendBodyBehindAppBar: widget.extendBodyBehindAppBar,
        backgroundColor: widget.backgroundColor,
        appBar: AppBar(
          elevation: 0.0,
          centerTitle: true,
          backgroundColor: Colors.white,
          automaticallyImplyLeading: false,
          toolbarHeight: widget.height * 0.1,
          title: widget.isThereLogo ? AppbarNXTLogo(isNeedLogoNavigate: widget.isLogoClickable) : const SizedBox(),
          actions: widget.isThereActions
              ? [AppbarActions(isInProfile: widget.isInProfile, isInNotification: widget.isInNotification)]
              : const [SizedBox()],
          flexibleSpace: widget.isBackArrow
              ? Center(
                  child: Row(children: [
                  Padding(padding: EdgeInsets.only(top: widget.height * 0.02), child: const BackButton())
                ]))
              : BlocBuilder<HomeCubit, HomeState>(
                  builder: (context, state) {
                    return Padding(
                      padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
                      child: FilterWidget(
                        onNXTTap: () => setState(() => selectedItem = filterValue!),
                        text: Text(
                          selectedItem,
                          style: widget.isEn
                              ? Theme.of(context).textTheme.titleLarge!.copyWith(
                                    color: filterColor,
                                    fontFamily: AppConst.isEn ? 'BebasNeue' : 'Tajawal',
                                    fontWeight: AppConst.isEn ? null : FontWeight.bold,
                                    height: .88,
                                  )
                              : Theme.of(context).textTheme.subtitle1!.copyWith(
                                    color: filterColor,
                                    fontFamily: AppConst.isEn ? 'BebasNeue' : 'Tajawal',
                                    fontWeight: AppConst.isEn ? null : FontWeight.bold,
                                    height: .88,
                                  ),
                        ),
                        allTextStyle: returnStyle(context: context, color: Colors.black),
                        actingTextStyle: returnStyle(context: context, color: AppColors.yellowColor),
                        filmMakingTextStyle: returnStyle(context: context, color: AppColors.redColor),
                        soundTextStyle: returnStyle(context: context, color: AppColors.blueColor),
                        onSelectedTap: ((filterValue) => setState(() => selectedItem = filterValue)),
                        allTap: () {
                          setState(
                            () {
                              filterColor = Colors.black;
                              selectedItem = 'all'.tr();
                            },
                          );
                          onFilterChange(context: context);
                        },
                        soundTap: () {
                          setState(
                            () {
                              filterColor = AppColors.blueColor;
                              selectedItem = 'music'.tr();
                            },
                          );
                          onFilterChange(context: context, filter: TalentCategoryType.music);
                        },
                        fimMakingTap: () {
                          setState(
                            () {
                              filterColor = AppColors.redColor;
                              selectedItem = 'filmMaking'.tr();
                            },
                          );
                          onFilterChange(context: context, filter: TalentCategoryType.filmMaking);
                        },
                        actingTap: () {
                          setState(
                            () {
                              filterColor = AppColors.yellowColor;
                              selectedItem = 'acting'.tr();
                            },
                          );
                          onFilterChange(context: context, filter: TalentCategoryType.acting);
                        },
                        selectedFilter: selectedItem,
                      ),
                    );
                  },
                ),
        ),
      ),
    );
  }
}

Future<bool> pop(context) async {
  Navigator.of(context).pop();
  return false;
}

Future<bool> showExitPopup(context) async {
  return await showDialog(
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
        actionsAlignment: MainAxisAlignment.center,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15.0)),
        contentPadding: const EdgeInsets.only(bottom: 8, top: 24),
        actions: [
          SimpleButton(
            text: "yes".tr(),
            onPressed: () => SystemNavigator.pop(animated: true),
          ),
          const SizedBox(width: 4),
          SimpleButton(
            text: "no".tr(),
            invertedColors: true,
            onPressed: () => Navigator.of(context).pop(),
          ),
        ],
        content: Text(
          "sureExit".tr(),
          textAlign: TextAlign.center,
          style:
              Theme.of(context).textTheme.titleMedium!.copyWith(fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal'),
        ),
      );
    },
  );
}
