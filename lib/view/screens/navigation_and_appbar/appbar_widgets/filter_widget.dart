// ignore_for_file: public_member_api_docs, sort_constructors_first, must_be_immutable
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:nxt/utils/constants/app_colors.dart';
import 'package:nxt/utils/constants/app_const.dart';
import 'package:nxt/utils/constants/app_text_style.dart';

class FilterWidget extends StatefulWidget {
  String selectedFilter;
  final void Function()? allTap;
  final void Function()? soundTap;
  final void Function()? fimMakingTap;
  final void Function()? actingTap;
  final void Function()? onNXTTap;
  final void Function(String)? onSelectedTap;
  final double? index;
  final TextStyle soundTextStyle, allTextStyle, actingTextStyle, filmMakingTextStyle;
  final Widget text;
  FilterWidget({
    Key? key,
    required this.selectedFilter,
    required this.allTap,
    required this.soundTap,
    required this.fimMakingTap,
    required this.actingTap,
    required this.onSelectedTap,
    this.index,
    required this.soundTextStyle,
    required this.allTextStyle,
    required this.actingTextStyle,
    required this.filmMakingTextStyle,
    required this.text,
    this.onNXTTap,
  }) : super(key: key);

  @override
  State<FilterWidget> createState() => _FilterWidgetState();
}

class _FilterWidgetState extends State<FilterWidget> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          PopupMenuButton<String>(
            shape: ContinuousRectangleBorder(borderRadius: BorderRadius.circular(18)),
            position: PopupMenuPosition.under,
            offset: const Offset(0, 35),
            onSelected: widget.onSelectedTap,
            initialValue: widget.selectedFilter,
            itemBuilder: (BuildContext context) => [
              PopupMenuItem(
                textStyle: widget.allTextStyle,
                onTap: widget.allTap,
                value: 'all'.tr().toUpperCase(),
                child: Text(
                  'all'.tr().toUpperCase(),
                  style: AppTextStyles.huge20,
                ),
              ),
              PopupMenuItem(
                textStyle: widget.soundTextStyle,
                onTap: widget.soundTap,
                value: 'music'.tr().toUpperCase(),
                child: Text(
                  'music'.tr().toUpperCase(),
                  style: AppTextStyles.huge20.apply(color: AppColors.blueColor),
                ),
              ),
              PopupMenuItem(
                textStyle: widget.filmMakingTextStyle,
                onTap: widget.fimMakingTap,
                value: 'filmMaking'.tr().toUpperCase(),
                child: Text(
                  'filmMaking'.tr().toUpperCase(),
                  style: AppTextStyles.huge20.apply(color: AppColors.redColor),
                ),
              ),
              PopupMenuItem(
                textStyle: widget.filmMakingTextStyle,
                onTap: widget.actingTap,
                value: 'acting'.tr().toUpperCase(),
                child: Text(
                  'acting'.tr().toUpperCase(),
                  style: AppTextStyles.huge20.apply(color: AppColors.yellowColor),
                ),
              ),
            ],
            child: Row(
              children: [
                const SizedBox(width: 9.5),
                Text('nxt'.tr().toUpperCase(),
                    style: widget.isEn
                        ? Theme.of(context).textTheme.titleLarge!.copyWith(
                              fontFamily: widget.isEn ? 'BebasNeue' : 'Tajawal',
                              fontWeight: widget.isEn ? null : FontWeight.bold,
                              height: .88,
                            )
                        : Theme.of(context).textTheme.subtitle1!.copyWith(
                              fontFamily: widget.isEn ? 'BebasNeue' : 'Tajawal',
                              fontWeight: widget.isEn ? null : FontWeight.bold,
                              height: .88,
                            )),
                const SizedBox(width: 3),
                widget.text,
                const SizedBox(width: 5),
                Padding(
                  padding: const EdgeInsets.only(bottom: 7),
                  child: SvgPicture.asset('assets/images/arrow_down.svg'),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
