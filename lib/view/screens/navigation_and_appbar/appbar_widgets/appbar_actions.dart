import 'package:flutter/material.dart';
import 'package:nxt/utils/constants/resources.dart';
import 'package:nxt/view/screens/navigation_and_appbar/appbar_widgets/invitation_icon_button.dart';
import 'package:transparent_image/transparent_image.dart';

import '../../../../config/router/router.dart';
import '../../../../data/service/hive_services.dart';
import '../../production_house_profile/expert_profile_screen.dart';
import '../../production_house_profile/organization_profile_screen.dart';
import '../../talent_profile/talent_profile_screen.dart';

class AppbarActions extends StatelessWidget {
  final bool isInProfile;
  final bool isInNotification;
  const AppbarActions({Key? key, this.isInProfile = false, this.isInNotification = false}) : super(key: key);

  Widget getProfileScreen() {
    Widget profileWidget = const SizedBox();
    switch (HiveHelper.getUserInfo!.user!.role) {
      case 'Organization':
        profileWidget = const OrganizationProfileScreen();
        break;
      case 'Expert':
        profileWidget = const ExpertProfileScreen();
        break;
      case 'RegularUser':
        profileWidget = const TalentProfileScreen();
        break;
      default:
        profileWidget = const TalentProfileScreen();
    }
    return profileWidget;
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        if (HiveHelper.getUserInfo?.user?.myInvitationCode != null &&
            HiveHelper.getUserInfo?.user?.myInvitationCode != '')
          const InvitationIconButton(),
        // IconButton(
        //   splashRadius: 18,
        //   onPressed: () => isInNotification ? null : MagicRouter.navigateTo(const NotificationsScreen()),
        //   icon: SvgPicture.asset(AppImages.notificationIconSvg),
        // ),
        IconButton(
          splashRadius: 24,
          onPressed: () => isInProfile ? null : MagicRouter.navigateTo(getProfileScreen()),
          icon: Container(
            height: 28.0,
            width: 28.0,
            decoration: BoxDecoration(
              color: AppColors.greyOutText,
              borderRadius: BorderRadius.circular(4.0),
              image: DecorationImage(
                fit: BoxFit.cover,
                image: HiveHelper.getUserAvatar != null && HiveHelper.getUserAvatar != ''
                    ? FadeInImage.memoryNetwork(
                        image: HiveHelper.getUserAvatar!,
                        placeholder: kTransparentImage,
                      ).image
                    : Image.asset('assets/images/profile_icon.png').image,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
