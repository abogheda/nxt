import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import '../../../../data/service/hive_services.dart';
import '../../../../utils/constants/resources.dart';

import '../../../../config/router/router.dart';
import '../import_widget.dart';

class AppbarNXTLogo extends StatelessWidget {
  final bool isNeedLogoNavigate;
  const AppbarNXTLogo({Key? key, this.isNeedLogoNavigate = true}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: isNeedLogoNavigate ? () => MagicRouter.navigateAndPopAll(Navigation(navigationIndex: 0)) : null,
      child: SvgPicture.asset(
        HiveHelper.isEgypt ? 'assets/images/fab_logo.svg' : 'assets/images/logo.svg',
        width: (MediaQuery.of(context).orientation == DeviceOrientation.landscapeLeft ||
                MediaQuery.of(context).orientation == DeviceOrientation.landscapeRight)
            ? 60
            : HiveHelper.isEgypt
                ? width * 0.285
                : width * 0.15,
      ),
    );
  }
}
