import 'package:dotted_border/dotted_border.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:nxt/utils/constants/resources.dart';
import 'package:nxt/utils/helpers/popup_helper.dart';
import 'package:nxt/utils/helpers/utils.dart';
import 'package:share_plus/share_plus.dart';

import '../../../../data/service/hive_services.dart';

class InvitationIconButton extends StatelessWidget {
  const InvitationIconButton({super.key});

  @override
  Widget build(BuildContext context) {
    return IconButton(
      splashRadius: 18,
      onPressed: () async {
        await FirebaseAnalytics.instance.logEvent(name: 'invitation_button', parameters: {
          'invitationCode': HiveHelper.getUserInfo?.user?.myInvitationCode.toString(),
        });
        String? code = HiveHelper.getUserInfo?.user?.myInvitationCode;
        if (code != null && code != '') {
          Utils.showAlertDialog(
            context: context,
            headerText: "",
            yesButtonText: "share".tr(),
            noButtonText: "dismiss".tr(),
            useLoading: false,
            onPressed: () {
              Navigator.pop(context);
              Share.share('${'joinProjectNxt'.tr()}\n$code');
            },
            customContent: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: width * 0.03),
                  child: Center(
                    child: Text(
                      "shareYourInvitationCode".tr(),
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w600,
                        fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal',
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(10).copyWith(top: 20),
                  child: DottedBorder(
                    color: AppColors.greyOutText,
                    padding: const EdgeInsets.all(8),
                    borderType: BorderType.RRect,
                    radius: const Radius.circular(10),
                    dashPattern: const [4, 4],
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Expanded(
                          child: Text(
                            code,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 16,
                              fontWeight: FontWeight.bold,
                              fontFamily: isEn ? 'Montserrat' : 'Tajawal',
                            ),
                          ),
                        ),
                        IconButton(
                          splashRadius: 18,
                          onPressed: () async {
                            if (code.trim().isEmpty) {
                              PopupHelper.showBasicSnack(msg: 'noInvitationCodeToCopy'.tr());
                              return;
                            } else {
                              Clipboard.setData(ClipboardData(text: code)).then(
                                (value) {
                                  Navigator.pop(context);
                                  PopupHelper.showBasicSnack(
                                    msg: "invitationCodeCopiedSuccessfully".tr(),
                                  );
                                },
                              );
                            }
                          },
                          icon: const Icon(Icons.copy, color: Colors.black),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          );
        } else {
          PopupHelper.showBasicSnack(msg: 'youDoNotHaveAnInvitationCode'.tr());
        }
      },
      icon: Icon(
        Icons.adaptive.share,
        color: AppColors.greyOutText,
        size: 27.0,
      ),
    );
  }
}
