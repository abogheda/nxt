
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../config/router/router.dart';
import '../../../utils/constants/app_const.dart';
import '../../../utils/helpers/popup_helper.dart';
import '../../widgets/app_loader.dart';
import '../../widgets/custom_text_field.dart';
import '../navigation_and_appbar/import_widget.dart';
import '../talent_profile/controller/profile_cubit.dart';

class EditBioScreen extends StatelessWidget {
  const EditBioScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return CustomScaffoldWidget(
      backgroundColor: Colors.white,
      extendBodyBehindAppBar: false,
      isBackArrow: true,
      isInProfile: true,
      isLogoClickable: true,
      body: BlocBuilder<ProfileCubit, ProfileState>(
        builder: (context, state) {
          final cubit = ProfileCubit.get(context);
          return Stack(
            children: [
              Positioned(
                top: 0,
                right: 0,
                left: 0,
                bottom: 0,
                child: Column(
                  children: [
                    Center(
                      child: Text("bio".tr(),style:Theme.of(context)
                          .textTheme
                          .titleLarge!
                          .copyWith(fontFamily: isEn ? 'BebasNeue' : 'Tajawal',fontSize: 30)),
                    ),
                    Padding(
                      padding:  EdgeInsets.all(25.0.h),
                      child: CustomTextField(
                        lines: 7,
                        prefix: const Padding(
                          padding: EdgeInsets.all(8.0),
                        ),
                        hint: '',
                        // validator: Validators.password,
                        type: TextInputType.text,
                        controller: cubit.bioController,
                        focusNode: cubit.bioFocusNode,
                        onFieldSubmitted: (_) async {
                          if (cubit.bioController.text.isNotEmpty) {
                            FocusManager.instance.primaryFocus?.unfocus();
                            await cubit.editBio();
                            await cubit.getUserData();
                            MagicRouter.pop();
                          }
                        },
                      ),
                    ),
                  ],
                ),
              ),
              Positioned(
                bottom: 0,
                right: 0,
                left: 0,
                child: MaterialButton(
                  height: 100.h,
                  minWidth: double.infinity,
                  color: Colors.black,
                  onPressed: () async {
                    if (cubit.bioController.text.isEmpty) {
                      PopupHelper.showBasicSnack(msg: 'dontLeftBioEmpty'.tr(), color: Colors.red);
                      return;
                    }
                    else{
                      await cubit.editBio();
                      await cubit.getUserData();
                      MagicRouter.pop();
                    }
                  },
                  child: state is GetProfileLoading || state is GetBioLoading
                      ? const AppLoader(color: Colors.white)
                      : Text(
                    'save'.tr().toUpperCase(),
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 18,
                      color: Colors.white,
                      fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal',
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              )
            ],
          );
        },
      ),
    );
  }
}
