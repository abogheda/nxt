import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../../../utils/constants/app_const.dart';
import '../../../../widgets/no_state_widget.dart';
import 'widgets/single_job_card.dart';
import '../../cubit/connect_cubit.dart';

import '../../../../../utils/helpers/popup_helper.dart';
import '../../../../widgets/app_loader.dart';
import '../../../../widgets/error_state_widget.dart';

class ExpertsAndJobsScreen extends StatelessWidget {
  const ExpertsAndJobsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<ConnectCubit, ConnectState>(
      listener: (context, state) {
        if (state is GetAllJobsFailed) {
          PopupHelper.showBasicSnack(msg: state.msg, color: Colors.red);
        }
      },
      builder: (context, state) {
        var cubit = ConnectCubit.get(context);
        final allRecommendedJobs = cubit.allJobsList;
        if (state is GetAllJobsLoading || allRecommendedJobs == null) {
          return const AppLoader();
        } else if (state is GetAllJobsFailed) {
          return ErrorStateWidget(
            hasRefresh: true,
            onRefresh: () async => await cubit.getAllJobs(),
          );
        }
        return RefreshIndicator(
          onRefresh: () => cubit.getAllJobs(),
          child: allRecommendedJobs.isEmpty
              ? ListView(children: [SizedBox(height: height * 0.35), NoStateWidget()])
              : ListView.builder(
                  itemCount: allRecommendedJobs.length,
                  padding: const EdgeInsets.all(10),
                  itemBuilder: (context, index) {
                    final job = allRecommendedJobs[index];
                    return SingleJobCard(job: job);
                  },
                ),
        );
      },
    );
  }
}
