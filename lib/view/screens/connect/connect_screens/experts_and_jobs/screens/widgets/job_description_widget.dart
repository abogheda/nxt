import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import '../../../../../../../utils/constants/resources.dart';
import '../../../../../../../utils/helpers/utils.dart';

class JobDescriptionWidget extends StatelessWidget {
  final String jobDescription;
  final String jobTitle;
  final String jobSubTitle;
  final String gender;
  final String ageTo;
  final String ageFrom;
  final String area;
  final String jobStartDate;
  final String jobEndDate;
  const JobDescriptionWidget({
    Key? key,
    required this.jobDescription,
    required this.jobTitle,
    required this.jobSubTitle,
    required this.gender,
    required this.ageTo,
    required this.ageFrom,
    required this.area,
    required this.jobStartDate,
    required this.jobEndDate,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final dateFormat = DateFormat.yMMMMEEEEd(isEn ? 'en_US' : 'ar_EG');
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: height * 0.015),
        Padding(
          padding: EdgeInsetsDirectional.only(start: width * 0.03, end: width * 0.06),
          child: RichText(
            text: TextSpan(
              children: [
                TextSpan(
                    text: '$jobTitle  ',
                    style: Theme.of(context).textTheme.titleMedium!.copyWith(
                        fontFamily: isEn ? 'Inter-Medium' : 'Tajawal', fontWeight: FontWeight.bold, height: 1.3)),
                TextSpan(
                    text: jobSubTitle,
                    style: Theme.of(context)
                        .textTheme
                        .titleMedium!
                        .copyWith(fontFamily: isEn ? 'Inter-Medium' : 'Tajawal', height: 1.3)),
              ],
            ),
          ),
        ),
        SizedBox(height: height * 0.015),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: width * 0.03),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text('${"jobDescription".tr().toUpperCase()}: ',
                  style: Theme.of(context)
                      .textTheme
                      .headline5!
                      .copyWith(fontFamily: isEn ? 'BebasNeue' : 'Tajawal', fontWeight: isEn ? null : FontWeight.bold)),
              Text(jobDescription, style: TextStyle(fontFamily: isEn ? 'Inter-Medium' : 'Tajawal', height: 1.3)),
            ],
          ),
        ),
        SizedBox(height: height * 0.015),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: width * 0.03),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                '${"req".tr().toUpperCase()}: ',
                textAlign: TextAlign.start,
                style: Theme.of(context).textTheme.headline5!.copyWith(
                      fontFamily: isEn ? 'BebasNeue' : 'Tajawal',
                      fontWeight: isEn ? null : FontWeight.bold,
                      color: Colors.black,
                    ),
              ),
              TextRow(boldText: "gender".tr(), normalText: gender),
              TextRow(boldText: "age".tr(), normalText: '${'between'.tr()} $ageFrom - $ageTo.'),
              TextRow(boldText: "area".tr(), normalText: area),
              TextRow(
                  boldText: "jobStartDate".tr(),
                  normalText: dateFormat.format(Utils.localDateFromIsoUTC(jobStartDate)!)),
              TextRow(
                  boldText: "jobEndDate".tr(), normalText: dateFormat.format(Utils.localDateFromIsoUTC(jobEndDate)!)),
              SizedBox(height: height * 0.015),
            ],
          ),
        ),
      ],
    );
  }
}

class TextRow extends StatelessWidget {
  const TextRow({
    Key? key,
    required this.boldText,
    required this.normalText,
  }) : super(key: key);
  final String boldText;
  final String normalText;

  @override
  Widget build(BuildContext context) {
    final boldStyle = TextStyle(
        fontSize: 16, color: Colors.black, fontFamily: isEn ? 'Inter-Medium' : 'Tajawal', fontWeight: FontWeight.bold);
    final normalStyle = TextStyle(fontFamily: isEn ? 'Inter-Medium' : 'Tajawal', color: Colors.black);
    return Column(
      children: <Widget>[
        SizedBox(height: height * 0.006),
        RichText(
          text: TextSpan(
            children: [
              TextSpan(text: '$boldText: ', style: boldStyle),
              TextSpan(text: normalText, style: normalStyle),
            ],
          ),
        ),
      ],
    );
  }
}
