import 'package:flutter/material.dart';
import '../../../../../../../utils/constants/app_const.dart';
import '../../../../../../../utils/helpers/picker_helper.dart';

class FileItem extends StatelessWidget {
  const FileItem({Key? key, required this.filePath, required this.iconData}) : super(key: key);
  final String filePath;
  final IconData iconData;
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => PickerHelper.openFile(path: filePath),
      child: Container(
          height: height * 0.15,
          width: width * 0.28,
          decoration: BoxDecoration(color: Colors.black26, borderRadius: BorderRadius.circular(16.0)),
          child: Icon(iconData, color: Colors.white)),
    );
  }
}
