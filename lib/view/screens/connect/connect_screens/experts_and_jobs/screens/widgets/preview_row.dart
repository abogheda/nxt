import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

import '../../../../../../../utils/constants/app_const.dart';

class PreviewRow extends StatelessWidget {
  const PreviewRow({Key? key, required this.previewWidget, required this.onPressed}) : super(key: key);
  final Widget previewWidget;
  final Function() onPressed;
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        previewWidget,
        OutlinedButton(
            onPressed: onPressed,
            child: Text('delete'.tr(), style: TextStyle(fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal')))
      ],
    );
  }
}
