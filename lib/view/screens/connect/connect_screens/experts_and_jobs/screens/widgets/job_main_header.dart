import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:transparent_image/transparent_image.dart';
import '../../../../../../../utils/constants/app_images.dart';
import '../../../../cubit/connect_cubit.dart';

import '../../../../../../../utils/constants/app_const.dart';
import '../../../../../../widgets/save_button.dart';

class JobMainHeader extends StatelessWidget {
  const JobMainHeader({
    Key? key,
    this.onSaveTap,
    this.onDeleteTap,
    this.loadingWidget,
    required this.logo,
    required this.jobColor,
    this.isLoading = false,
    this.isTheSameUser = false,
    required this.organizationName,
  }) : super(key: key);
  final String? logo;
  final String jobColor;
  final bool isTheSameUser;
  final bool isLoading;
  final String organizationName;
  final Function()? onSaveTap;
  final Function()? onDeleteTap;
  final Widget? loadingWidget;
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ConnectCubit, ConnectState>(
      builder: (context, state) {
        final cubit = ConnectCubit.get(context);
        return Padding(
          padding: EdgeInsets.symmetric(horizontal: width * 0.03),
          child: Row(
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(50),
                child: FadeInImage.memoryNetwork(
                    placeholder: kTransparentImage,
                    image: logo == '' ? placeHolderUrl : logo ?? placeHolderUrl,
                    fit: BoxFit.cover,
                    width: 45,
                    height: 45),
              ),
              const SizedBox(width: 8),
              Expanded(
                child: Text(
                  organizationName,
                  style: Theme.of(context).textTheme.titleLarge!.copyWith(
                        fontFamily: isEn ? 'BebasNeue' : 'Tajawal',
                        fontWeight: isEn ? FontWeight.normal : FontWeight.bold,
                        overflow: TextOverflow.ellipsis,
                      ),
                ),
              ),
              SaveButton(
                onPressed: onSaveTap,
                isSaved: cubit.isJobSaved,
                saveColor: '#000000',
              ),
              isLoading
                  ? loadingWidget!
                  : Offstage(
                      offstage: !isTheSameUser,
                      child: TextButton.icon(
                        onPressed: onDeleteTap,
                        icon: const Icon(Icons.delete_rounded),
                        label: Text(
                          'delete'.tr(),
                          style: TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.w900,
                            fontFamily: isEn ? 'Montserrat' : 'Tajawal',
                          ),
                        ),
                      ),
                    ),
            ],
          ),
        );
      },
    );
  }
}
