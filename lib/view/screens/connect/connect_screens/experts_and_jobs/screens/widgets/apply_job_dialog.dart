// ignore_for_file: use_build_context_synchronously

import 'dart:io';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nxt/utils/constants/app_const.dart';
import 'package:nxt/utils/helpers/popup_helper.dart';
import 'package:nxt/view/screens/connect/connect_screens/experts_and_jobs/screens/widgets/preview_row.dart';

import '../../../../../../../data/enum/upload_type.dart';
import '../../../../../../../utils/constants/app_colors.dart';
import '../../../../../../widgets/app_loader.dart';
import '../../../../../complete_profile_screen/widgets/imports.dart';
import '../../../../cubit/connect_cubit.dart';
import 'file_item.dart';
import 'job_thanks_for_apply_dialog.dart';

class ApplyJobDialog extends StatefulWidget {
  const ApplyJobDialog({Key? key}) : super(key: key);
  @override
  State<ApplyJobDialog> createState() => _ApplyJobDialogState();
}

class _ApplyJobDialogState extends State<ApplyJobDialog> {
  bool isFileChoose = false;
  File? videoFile;
  void deleteMethod({required File? file}) {
    setState(() => isFileChoose = false);
    file = null;
  }

  void dependOnInheritedWidgetOfExactType() {
    final cubit = ConnectCubit.get(context);
    cubit.applyCubitVideo = null;
    cubit.applyCubitAudio = null;
    cubit.applyCubitText = null;
    isFileChoose = false;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<ConnectCubit, ConnectState>(
      listener: (context, state) {},
      builder: (context, state) {
        final cubit = ConnectCubit.get(context);
        return AlertDialog(
          actionsAlignment: MainAxisAlignment.center,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15.0)),
          titlePadding: EdgeInsets.only(top: widget.height * 0.045),
          contentPadding: EdgeInsets.only(top: widget.height * 0.02),
          actionsPadding: EdgeInsets.only(
              bottom: widget.height * 0.015,
              top: widget.height * 0.01,
              right: widget.width * 0.09,
              left: widget.width * 0.09),
          title: Text(
            "uploadFile".tr(),
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 36, fontFamily: AppConst.isEn ? 'BebasNeue' : 'Tajawal', height: .88),
          ),
          actions: [
            state is UploadVideoForJobLoading || state is ApplyForJobLoading
                ? Stack(
                    children: [
                      Opacity(
                        opacity: 0.0,
                        child: ElevatedButton(
                          onPressed: null,
                          child: Text(
                            "applyNow".tr(),
                            style: TextStyle(
                                fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal', fontWeight: FontWeight.bold),
                          ),
                        ),
                      ),
                      const AppLoader(),
                    ],
                  )
                : ElevatedButton(
                    onPressed: state is UploadVideoForJobLoading || state is ApplyForJobLoading
                        ? null
                        : () async {
                            if (cubit.applyCubitVideo == null && cubit.jobModel!.uploadType! == UploadType.video.type) {
                              PopupHelper.showBasicSnack(msg: 'pleaseSelectFile'.tr(), color: Colors.red);
                            } else if (cubit.applyCubitAudio == null &&
                                cubit.jobModel!.uploadType! == UploadType.audio.type) {
                              PopupHelper.showBasicSnack(msg: 'pleaseSelectFile'.tr(), color: Colors.red);
                            } else if (cubit.applyCubitText == null &&
                                cubit.jobModel!.uploadType! == UploadType.text.type) {
                              PopupHelper.showBasicSnack(msg: 'pleaseSelectFile'.tr(), color: Colors.red);
                            } else {
                              await cubit.applyForJob(
                                jobId: cubit.jobModel!.id!,
                                uploadType: cubit.jobModel!.uploadType!,
                              );
                              showDialog(
                                context: context,
                                builder: (context) => const JobThanksForApplyDialog(),
                              ).then((_) {
                                Navigator.pop(context);
                                cubit.setFilesToNull();
                              });
                            }
                          },
                    child: Text(
                      "applyNow".tr(),
                      style:
                          TextStyle(fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal', fontWeight: FontWeight.bold),
                    ),
                  ),
          ],
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              isFileChoose
                  ? Container(
                      child: cubit.jobModel!.uploadType == UploadType.video.type
                          ? PreviewRow(
                              previewWidget: VideoItem(videoPath: videoFile!),
                              onPressed: () {
                                videoFile = null;
                                deleteMethod(file: cubit.applyCubitVideo);
                              },
                            )
                          : cubit.jobModel!.uploadType == UploadType.audio.type
                              ? PreviewRow(
                                  previewWidget: FileItem(
                                      filePath: cubit.applyCubitAudio!.path, iconData: Icons.music_note_outlined),
                                  onPressed: () => deleteMethod(file: cubit.applyCubitAudio))
                              : PreviewRow(
                                  previewWidget: FileItem(
                                      filePath: cubit.applyCubitText!.path, iconData: Icons.description_rounded),
                                  onPressed: () => deleteMethod(file: cubit.applyCubitText)),
                    )
                  : Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16),
                      child: Align(
                        alignment: AppConst.isEn ? Alignment.centerLeft : Alignment.centerRight,
                        child: Text(
                          "selectFile".tr(),
                          style: TextStyle(
                              fontSize: 12,
                              color: AppColors.iconsColor,
                              fontWeight: FontWeight.bold,
                              fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal'),
                        ),
                      ),
                    ),
              const SizedBox(height: 15),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16),
                child: GestureDetector(
                  onTap: () async {
                    if (cubit.jobModel!.uploadType == UploadType.video.type) {
                      cubit.applyCubitVideo = null;
                      isFileChoose = false;
                      setState(() => videoFile = null);
                      await cubit.pickVideo().then((_) {
                        if (cubit.applyCubitVideo != null) {
                          videoFile = cubit.applyCubitVideo;
                          setState(() => isFileChoose = true);
                        }
                      });
                    } else if (cubit.jobModel!.uploadType == UploadType.audio.type) {
                      cubit.applyCubitAudio = null;
                      isFileChoose = false;
                      await cubit.pickAudio().then((_) {
                        if (cubit.applyCubitAudio != null) {
                          setState(() => isFileChoose = true);
                        }
                      });
                    } else if (cubit.jobModel!.uploadType == UploadType.text.type) {
                      cubit.applyCubitText = null;
                      isFileChoose = false;
                      await cubit.pickText().then((_) {
                        if (cubit.applyCubitText != null) {
                          setState(() => isFileChoose = true);
                        }
                      });
                    }
                  },
                  child: Container(
                    decoration: BoxDecoration(
                        border: Border.all(color: AppColors.iconsColor),
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(8)),
                    height: 50,
                    child: Row(
                      children: [
                        const SizedBox(width: 10),
                        const Icon(Icons.file_upload_outlined, size: 25, color: AppColors.iconsColor),
                        const SizedBox(width: 5),
                        Text(
                          "enter".tr(),
                          style: TextStyle(
                            fontSize: 13,
                            color: AppColors.iconsColor,
                            fontWeight: FontWeight.w600,
                            fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal',
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
