import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import '../../../../../../../config/router/router.dart';
import '../../../../../../../utils/constants/resources.dart';
import '../../../../../navigation_and_appbar/import_widget.dart';

class JobThanksForApplyDialog extends StatelessWidget {
  const JobThanksForApplyDialog({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final buttonTextStyle = Theme.of(context)
        .textTheme
        .bodyText2!
        .copyWith(fontFamily: isEn ? 'Montserrat' : 'Tajawal', fontWeight: FontWeight.bold);
    return AlertDialog(
      actionsAlignment: MainAxisAlignment.center,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15.0)),
      contentPadding: EdgeInsets.symmetric(horizontal: width * 0.05, vertical: height * 0.01),
      actionsPadding:
          EdgeInsets.only(top: height * 0.015, bottom: height * 0.025, right: width * 0.09, left: width * 0.09),
      title: Column(
        children: [
          Container(
            height: width * 0.11,
            width: width * 0.11,
            decoration: BoxDecoration(color: Colors.black, borderRadius: BorderRadius.circular(100)),
            child: Icon(Icons.done_rounded, size: width * 0.07, color: Colors.white),
          ),
          SizedBox(height: height * 0.02),
          Text(
            'thanksApply'.tr().toUpperCase(),
            style: AppTextStyles.huge36,
            textAlign: TextAlign.center,
          ),
        ],
      ),
      content: Text(
        "applicationReviewd".tr(),
        textAlign: TextAlign.center,
        style: Theme.of(context).textTheme.bodySmall!.copyWith(
            fontFamily: isEn ? 'Montserrat' : 'Tajawal', fontWeight: FontWeight.bold, color: AppColors.iconsColor),
      ),
      actions: [
        MaterialButton(
          shape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(4))),
          height: height * 0.05,
          minWidth: double.infinity,
          color: Colors.black,
          onPressed: () {
            Navigator.pop(context);
            Navigator.pop(context);
          },
          child: Text(
            "backJobs".tr(),
            style: buttonTextStyle.copyWith(color: Colors.white),
            textAlign: TextAlign.center,
          ),
        ),
        SizedBox(height: height * 0.01),
        MaterialButton(
          height: height * 0.05,
          minWidth: double.infinity,
          shape: RoundedRectangleBorder(
              side: const BorderSide(color: Colors.black, width: 1), borderRadius: BorderRadius.circular(4)),
          onPressed: () => MagicRouter.navigateAndPopUntilFirstPage(Navigation()),
          child: Text(
            'home'.tr().toUpperCase(),
            style: buttonTextStyle,
            textAlign: TextAlign.center,
          ),
        )
      ],
    );
  }
}
