import 'package:flutter/material.dart';
import 'package:transparent_image/transparent_image.dart';

import '../../../../../../../utils/constants/resources.dart';

class JobImage extends StatelessWidget {
  const JobImage({
    Key? key,
    required this.poster,
    required this.categoryColor,
  }) : super(key: key);
  final String poster;
  final String categoryColor;
  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: isEn ? Alignment.bottomRight : Alignment.bottomLeft,
      children: [
        Container(
          width: double.infinity,
          height: height * 0.27,
          color: AppColors.greyOutText,
        ),
        FadeInImage.memoryNetwork(
          image: poster,
          fit: BoxFit.cover,
          width: double.infinity,
          height: height * 0.27,
          placeholder: kTransparentImage,
        ),
        Container(
          height: 7,
          width: width * 0.22,
          decoration: BoxDecoration(
            color: Color(categoryColor.toHex()),
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(isEn ? 8 : 0),
              bottomLeft: Radius.circular(isEn ? 8 : 0),
              topRight: Radius.circular(isEn ? 0 : 8),
              bottomRight: Radius.circular(isEn ? 0 : 8),
            ),
          ),
        ),
      ],
    );
  }
}
