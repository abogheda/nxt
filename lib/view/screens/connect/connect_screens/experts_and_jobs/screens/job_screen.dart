import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../../../../config/router/router.dart';
import '../../../../../../data/service/hive_services.dart';
import 'widgets/apply_job_dialog.dart';
import '../../../../../../utils/helpers/utils.dart';
import '../../../../edit_job/edit_job_screen.dart';
import '../../../cubit/connect_cubit.dart';
import '../../../../navigation_and_appbar/import_widget.dart';

import '../../../../../../utils/helpers/popup_helper.dart';
import '../../../../../widgets/app_loader.dart';
import '../../../../../widgets/custom_button.dart';
import '../../../../../widgets/error_state_widget.dart';
import 'widgets/job_description_widget.dart';
import 'widgets/job_image.dart';
import 'widgets/job_main_header.dart';
import '../../../../../../utils/constants/resources.dart';

class JobScreen extends StatefulWidget {
  const JobScreen({Key? key, required this.jobId}) : super(key: key);
  final String jobId;

  @override
  State<JobScreen> createState() => _JobScreenState();
}

class _JobScreenState extends State<JobScreen> {
  final now = DateTime.now();
  String buildText({required bool userApply, required String jobStartDate, required String jobEndDate}) {
    final startDate = Utils.localDateFromIsoUTC(jobStartDate)!;
    final endDate = Utils.localDateFromIsoUTC(jobEndDate)!;
    if (userApply) {
      return "alreadyApplied".tr().toUpperCase();
    } else if (now.isBefore(startDate)) {
      return "joinDateNotCome".tr().toUpperCase();
    } else if (now.isAfter(endDate)) {
      return "joinDatePassed".tr().toUpperCase();
    } else {
      return "applyNow".tr().toUpperCase();
    }
  }

  @override
  Widget build(BuildContext context) {
    return CustomScaffoldWidget(
      backgroundColor: Colors.white,
      extendBodyBehindAppBar: false,
      isBackArrow: true,
      isInProfile: false,
      isLogoClickable: true,
      body: BlocProvider(
        create: (context) => ConnectCubit()..getJobById(jobId: widget.jobId),
        child: BlocConsumer<ConnectCubit, ConnectState>(
          listener: (context, state) {
            if (state is GetJobFailed) PopupHelper.showBasicSnack(msg: state.msg, color: Colors.red);
            if (state is ApplyForJobFailed) PopupHelper.showBasicSnack(msg: state.msg, color: Colors.red);
            if (state is UploadVideoForJobFailed) PopupHelper.showBasicSnack(msg: state.msg, color: Colors.red);
          },
          builder: (context, state) {
            final cubit = ConnectCubit.get(context);
            final job = cubit.jobModel;
            if (state is GetJobLoading || job == null) {
              return const AppLoader();
            } else if (state is GetJobFailed) {
              return ErrorStateWidget(
                hasRefresh: true,
                onRefresh: () async => await cubit.getJobById(jobId: widget.jobId),
              );
            }
            String getName() {
              String name = '';
              if (job.organization != null) {
                name = job.organization!.name ?? "notAvailable".tr();
                if (name == '') name = "notAvailable".tr();
                return name;
              }
              if (job.expert != null) {
                name = job.expert!.expert!.name ?? "notAvailable".tr();
                if (name == '') name = "notAvailable".tr();
                return name;
              }
              return name;
            }

            String getLogo() {
              String logo = '';
              if (job.organization != null) {
                logo = job.organization!.logo ?? userAvatarPlaceHolderUrl;
                if (logo == '') logo = userAvatarPlaceHolderUrl;
                return logo;
              }
              if (job.expert != null) {
                logo = job.expert!.expert!.logo ?? userAvatarPlaceHolderUrl;
                if (logo == '') logo = userAvatarPlaceHolderUrl;
                return logo;
              }
              return logo;
            }

            bool isTheSameUser() {
              bool isSameUser = false;
              try {
                if (HiveHelper.getUserInfo!.user!.role == 'Organization') {
                  isSameUser = HiveHelper.getUserInfo!.user!.organization!.id! == job.organization!.id;
                  return isSameUser;
                }
                if (HiveHelper.getUserInfo!.user!.role == 'Expert') {
                  isSameUser = HiveHelper.getUserInfo!.user!.id! == job.expert!.id!;
                  return isSameUser;
                }
              } catch (e) {
                debugPrint(e.toString());
              }
              return false;
            }

            return Column(
              children: [
                Expanded(
                  child: RefreshIndicator(
                    onRefresh: () async => cubit.getJobById(jobId: widget.jobId),
                    child: ListView(
                      children: [
                        JobImage(
                          poster: job.poster ?? placeHolderUrl,
                          categoryColor: job.category!.color ?? '#f5ed05',
                        ),
                        SizedBox(height: widget.height * 0.015),
                        JobMainHeader(
                          logo: getLogo(),
                          organizationName: getName(),
                          jobColor: job.category!.color ?? '#f5ed05',
                          isTheSameUser: false,
                          // isTheSameUser: isTheSameUser(),
                          onSaveTap: () async {
                            if (cubit.isJobSaved == true) {
                              await cubit.deleteSaveJob(jobId: job.id!, inJobsScreen: false);
                            } else {
                              await cubit.saveJob(item: job.id!, inJobsScreen: false);
                            }
                            await cubit.getAllJobs();
                          },
                          // onDeleteTap: () async {
                          //   await cubit.getAllJobs();
                          // },
                        ),
                        JobDescriptionWidget(
                          jobTitle: job.title ?? "notAvailable".tr(),
                          jobSubTitle: job.supTitle ?? "notAvailable".tr(),
                          jobDescription: job.description ?? "notAvailable".tr(),
                          gender: job.gender ?? "notAvailable".tr(),
                          ageTo: job.ageTo ?? "notAvailable".tr(),
                          ageFrom: job.ageFrom ?? "notAvailable".tr(),
                          area: job.area ?? "notAvailable".tr(),
                          jobStartDate: job.jobStartDate ?? "notAvailable".tr(),
                          jobEndDate: job.jobEndDate ?? "notAvailable".tr(),
                        )
                      ],
                    ),
                  ),
                ),
                ColoredBox(
                  color: Colors.black,
                  child: HiveHelper.isRegularUser
                      ? CustomButton(
                          onTap: job.userApply! ||
                                  now.isBefore(Utils.localDateFromIsoUTC(job.jobStartDate)!) ||
                                  now.isAfter(Utils.localDateFromIsoUTC(job.jobEndDate)!)
                              ? null
                              : () => showDialog(
                                    context: context,
                                    builder: (context) =>
                                        BlocProvider.value(value: cubit, child: const ApplyJobDialog()),
                                  ).then((_) => cubit.setFilesToNull()),
                          height: widget.height * 0.08,
                          width: double.infinity,
                          textColor: Colors.white,
                          child: Text(
                            buildText(
                                userApply: job.userApply!,
                                jobEndDate: job.jobEndDate!,
                                jobStartDate: job.jobStartDate!),
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 18,
                              color: Colors.white,
                              fontWeight: FontWeight.w100,
                              fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal',
                            ),
                          ),
                        )
                      : isTheSameUser()
                          ? CustomButton(
                              onTap: () => MagicRouter.navigateTo(EditJobScreen(jobId: job.id!)),
                              height: widget.height * 0.08,
                              width: double.infinity,
                              textColor: Colors.white,
                              child: Text(
                                'editJob'.tr(),
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontSize: 18,
                                  color: Colors.white,
                                  fontWeight: FontWeight.w100,
                                  fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal',
                                ),
                              ),
                            )
                          : const SizedBox(),
                ),
              ],
            );
          },
        ),
      ),
    );
  }
}
