// ignore_for_file: depend_on_referenced_packages

import 'package:easy_localization/easy_localization.dart' as local;
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:nxt/utils/constants/app_const.dart';
import 'package:nxt/view/screens/connect/cubit/connect_cubit.dart';
import 'package:timeago/timeago.dart' as timeago;
import 'package:transparent_image/transparent_image.dart';

import '../../../../../../config/router/router.dart';
import '../../../../../../data/models/jobs/jobs_model.dart';
import '../../../../../../utils/constants/app_colors.dart';
import '../../../../../../utils/constants/app_images.dart';
import '../../../../connect/Connect_screens/experts_and_jobs/screens/job_screen.dart';

const colorRadius = Radius.circular(10);
final borderRadius = BorderRadius.circular(10);

class SingleJobCard extends StatefulWidget {
  const SingleJobCard({Key? key, required this.job}) : super(key: key);
  final JobsModel job;
  @override
  State<SingleJobCard> createState() => _SingleJobCardState();
}

class _SingleJobCardState extends State<SingleJobCard> {
  @override
  void initState() {
    ConnectCubit.get(context).inJobsInit(isSavedFormModel: widget.job.userSaved ?? false);
    super.initState();
  }

  String getLogo() {
    String logo = '';
    if (widget.job.organization != null) {
      logo = widget.job.organization!.logo ?? userAvatarPlaceHolderUrl;
      if (logo == '') logo = userAvatarPlaceHolderUrl;
      return logo;
    }
    if (widget.job.expert != null) {
      logo = widget.job.expert!.expert!.logo ?? userAvatarPlaceHolderUrl;
      if (logo == '') logo = userAvatarPlaceHolderUrl;
      return logo;
    }
    return logo;
  }

  String getName() {
    String name = '';
    if (widget.job.organization != null) {
      name = widget.job.organization!.name ?? "notAvailable".tr();
      if (name == '') name = "notAvailable".tr();
      return name;
    }
    if (widget.job.expert != null) {
      name = widget.job.expert!.expert!.name ?? "notAvailable".tr();
      if (name == '') name = "notAvailable".tr();
      return name;
    }
    return name;
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ConnectCubit, ConnectState>(
      builder: (context, state) {
        final cubit = ConnectCubit.get(context);
        return Container(
          margin: const EdgeInsets.only(bottom: 10.0),
          decoration: BoxDecoration(borderRadius: borderRadius, border: Border.all(color: Colors.black, width: 0.2)),
          child: InkWell(
            onTap: () => MagicRouter.navigateTo(JobScreen(jobId: widget.job.id!)),
            child: Material(
              elevation: 2,
              borderRadius: borderRadius,
              child: IntrinsicHeight(
                child: Stack(
                  children: [
                    Container(
                      width: 10,
                      decoration: BoxDecoration(
                        color: Color(widget.job.category!.color!.toHex()),
                        borderRadius: BorderRadius.only(
                          topLeft: widget.isEn ? colorRadius : Radius.zero,
                          bottomLeft: widget.isEn ? colorRadius : Radius.zero,
                          topRight: widget.isEn ? Radius.zero : colorRadius,
                          bottomRight: widget.isEn ? Radius.zero : colorRadius,
                        ),
                      ),
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const SizedBox(height: 12),
                        Padding(
                          padding: EdgeInsetsDirectional.only(start: 16, end: widget.width * 0.006),
                          child: Row(
                            children: [
                              SizedBox(
                                width: 36,
                                height: 36,
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(50),
                                  child: FadeInImage.memoryNetwork(
                                      image: getLogo(), placeholder: kTransparentImage, fit: BoxFit.cover),
                                ),
                              ),
                              SizedBox(width: widget.width * 0.015),
                              SizedBox(
                                width: widget.width * 0.55,
                                height: widget.height * 0.06,
                                child: Align(
                                  alignment: AppConst.isEn ? Alignment.centerLeft : Alignment.centerRight,
                                  child: Text(
                                    widget.job.title ?? '',
                                    maxLines: 2,
                                    textAlign: TextAlign.start,
                                    overflow: TextOverflow.ellipsis,
                                    style: Theme.of(context).textTheme.headline6!.copyWith(
                                          fontFamily: AppConst.isEn ? 'BebasNeue' : 'Tajawal',
                                          fontWeight: FontWeight.w500,
                                        ),
                                  ),
                                ),
                              ),
                              const Spacer(),
                              TextButton.icon(
                                onPressed: () async {
                                  if (widget.job.userSaved! == true) {
                                    await cubit.deleteSaveJob(jobId: widget.job.id!, inJobsScreen: true);
                                  } else {
                                    await cubit.saveJob(item: widget.job.id!, inJobsScreen: true);
                                  }
                                  setState(() => widget.job.userSaved = !widget.job.userSaved!);
                                },
                                icon: SvgPicture.asset(
                                  widget.job.userSaved! ? 'assets/images/saved.svg' : 'assets/images/save.svg',
                                  height: 16,
                                  width: 16,
                                  color: widget.job.userSaved!
                                      ? Color((widget.job.category!.color ?? '#f5ed05').toHex())
                                      : Colors.black,
                                ),
                                label: Text(
                                  widget.job.userSaved! ? 'saved'.tr() : 'save'.tr(),
                                  style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w900,
                                    color: widget.job.userSaved!
                                        ? Color((widget.job.category!.color ?? '#f5ed05').toHex())
                                        : Colors.black,
                                    fontFamily: widget.isEn ? 'Montserrat' : 'Tajawal',
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          padding: EdgeInsetsDirectional.only(start: widget.width * 0.15),
                          child: Text(
                            getName(),
                            style: Theme.of(context).textTheme.bodyText2!.copyWith(
                                  color: const Color(0xFF555555),
                                  fontFamily: widget.isEn ? 'Inter-Medium' : 'Tajawal',
                                ),
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              padding: EdgeInsetsDirectional.only(start: widget.width * 0.15),
                              child: Text(
                                timeago.format(DateTime.parse(widget.job.createdAt ?? ''),
                                    locale: widget.isEn ? 'en' : 'ar'),
                                style: TextStyle(
                                  fontSize: 12,
                                  color: AppColors.iconsColor,
                                  fontFamily: widget.isEn ? 'Inter-Medium' : 'Tajawal',
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(bottom: 12.0, left: 12.0, right: 12.0),
                              child: Icon(
                                Icons.arrow_forward_ios_rounded,
                                textDirection: widget.isEn ? TextDirection.ltr : TextDirection.rtl,
                              ),
                            )
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
