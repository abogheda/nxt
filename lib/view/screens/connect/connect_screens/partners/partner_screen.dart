import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:nxt/utils/constants/app_const.dart';
import 'package:transparent_image/transparent_image.dart';

import '../../../../../data/models/community/partner_model.dart';
import '../../../../../utils/constants/app_images.dart';
import '../../../navigation_and_appbar/import_widget.dart';
import '../../../../widgets/custom_html_widget.dart';

class PartnerScreen extends StatelessWidget {
  const PartnerScreen({Key? key, required this.partner}) : super(key: key);
  final PartnerModel partner;
  @override
  Widget build(BuildContext context) {
    return CustomScaffoldWidget(
      extendBodyBehindAppBar: false,
      isBackArrow: true,
      isThereLogo: true,
      isThereActions: true,
      body: ListView(
        children: [
          const Divider(color: Colors.black, thickness: 0.9),
          SizedBox(height: height * 0.01),
          Container(
            height: width * 0.35,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              border: Border.all(width: 1.0, color: Colors.black),
              image: DecorationImage(
                fit: BoxFit.contain,
                alignment: Alignment.center,
                image: FadeInImage.memoryNetwork(
                  image: partner.logo == '' ? userAvatarPlaceHolderUrl : partner.logo ?? userAvatarPlaceHolderUrl,
                  placeholder: kTransparentImage,
                ).image,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 12.0, bottom: 0.0),
            child: Text(
              partner.name ?? "notAvailable".tr(),
              textAlign: TextAlign.center,
              style: Theme.of(context)
                  .textTheme
                  .headline5!
                  .copyWith(fontFamily: AppConst.isEn ? 'BebasNeue' : 'Tajawal', fontSize: 44),
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: width * 0.03),
            child: Text(partner.industry ?? "notAvailable".tr(),
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.bodyText2!.copyWith(
                    fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal', color: Colors.black.withOpacity(0.7))),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: width * 0.03),
            child: const Divider(color: Colors.black, thickness: 0.9),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: width * 0.03),
            child: CustomHtmlWidget(data: partner.description),
          ),
          const SizedBox(height: 12)
        ],
      ),
    );
  }
}
