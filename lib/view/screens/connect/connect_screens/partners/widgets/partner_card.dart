import 'package:flutter/material.dart';
import 'package:nxt/config/router/router.dart';
import 'package:nxt/data/models/community/partner_model.dart';
import 'package:nxt/view/screens/connect/connect_screens/partners/partner_screen.dart';
import 'package:transparent_image/transparent_image.dart';

import '../../../../../../utils/constants/resources.dart';

class PartnerCard extends StatelessWidget {
  const PartnerCard({Key? key, required this.partner}) : super(key: key);
  final PartnerModel partner;
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Padding(
          padding: EdgeInsets.symmetric(vertical: height * 0.005, horizontal: width * 0.03),
          child: InkWell(
            onTap: () => MagicRouter.navigateTo(PartnerScreen(partner: partner)),
            splashColor: Colors.black12,
            borderRadius: BorderRadius.circular(6),
            child: Container(
              width: width,
              decoration: BoxDecoration(
                  border: Border.all(color: const Color(0xFF707070), width: 0.2),
                  borderRadius: BorderRadius.circular(6)),
              child: Container(
                height: height * 0.06,
                margin: EdgeInsets.symmetric(vertical: height * 0.01),
                child: partner.logo != null && partner.logo != ''
                    ? FadeInImage.memoryNetwork(
                        image: partner.logo ?? userAvatarPlaceHolderUrl,
                        placeholder: kTransparentImage,
                      )
                    : Image.asset('assets/images/profile_icon.png'),
              ),
            ),
          ),
        ),
        Positioned(
          top: 0,
          bottom: 0,
          left: isEn ? null : width * 0.05,
          right: isEn ? width * 0.05 : null,
          child: const Icon(Icons.arrow_forward_ios_rounded, size: 20),
        )
      ],
    );
  }
}
