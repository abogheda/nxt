import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nxt/utils/constants/app_const.dart';
import 'package:nxt/view/screens/connect/connect_screens/partners/widgets/partner_card.dart';
import 'package:nxt/view/screens/connect/cubit/connect_cubit.dart';
import 'package:nxt/view/widgets/app_loader.dart';

import '../../../../widgets/error_state_widget.dart';
import '../../../../widgets/no_state_widget.dart';

class PartnersScreen extends StatelessWidget {
  const PartnersScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ConnectCubit, ConnectState>(
      builder: (context, state) {
        final cubit = ConnectCubit.get(context);
        if (state is GetAllPartnerLoading || cubit.partnerList == null) {
          return const AppLoader();
        }
        if (state is GetAllPartnerFailed) {
          return ErrorStateWidget(
            hasRefresh: true,
            onRefresh: () async => await cubit.getAllPartners(),
          );
        }
        if (cubit.partnerList!.isEmpty) return NoStateWidget();
        return RefreshIndicator(
          onRefresh: () async => await cubit.getAllPartners(),
          child: ListView.builder(
            itemCount: cubit.partnerList!.length,
            padding: EdgeInsets.only(top: height * 0.01),
            itemBuilder: (context, index) {
              final partner = cubit.partnerList![index];
              if (partner.display == true) return PartnerCard(partner: partner);
              return const SizedBox();
            },
          ),
        );
      },
    );
  }
}
