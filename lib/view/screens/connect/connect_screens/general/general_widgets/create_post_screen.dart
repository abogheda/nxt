import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:easy_image_viewer/easy_image_viewer.dart';
import 'package:easy_localization/easy_localization.dart' as local;
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:transparent_image/transparent_image.dart';
import '../../../../../../config/router/router.dart';
import '../../../../../../utils/constants/app_const.dart';
import '../../../../../../utils/helpers/popup_helper.dart';
import '../../../../../widgets/error_state_widget.dart';
import '../../../../../widgets/loading_state_widget.dart';
import 'tag_card.dart';

import '../../../../../../data/service/hive_services.dart';
import '../../../../../../utils/constants/app_text_style.dart';
import '../../../../../widgets/custom_text_field.dart';
import '../../../../navigation_and_appbar/import_widget.dart';
import '../../../../otp/widgets/loader_hud.dart';
import '../../../cubit/connect_cubit.dart';

class CreatePostScreen extends StatefulWidget {
  const CreatePostScreen({Key? key}) : super(key: key);

  @override
  State<CreatePostScreen> createState() => _CreatePostPageState();
}

class _CreatePostPageState extends State<CreatePostScreen> {
  String text = '';
  late ConnectCubit cubit;
  @override
  void initState() {
    cubit = context.read<ConnectCubit>();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    cubit.postController.clear();
    cubit.postImageUrl = null;
    cubit.postImage = null;
  }

  @override
  Widget build(BuildContext context) {
    return CustomScaffoldWidget(
      extendBodyBehindAppBar: false,
      isBackArrow: true,
      isThereLogo: true,
      isThereActions: true,
      body: BlocProvider(
        create: (context) => cubit..getTags(),
        child: BlocConsumer<ConnectCubit, ConnectState>(
          listener: (context, state) {
            if (state is CreatePostFailed) PopupHelper.showBasicSnack(msg: state.msg, color: Colors.red);
          },
          builder: (context, state) {
            final cubit = ConnectCubit.get(context);
            final tags = cubit.tagsList ?? [];
            List selectedTags = cubit.selectedTagsList;
            if (state is TagLoading) return const LoadingStateWidget();
            if (state is TagFailed) {
              return ErrorStateWidget(hasRefresh: true, onRefresh: () async => await cubit.getTags());
            }
            return LoaderHUD(
              inAsyncCall: state is CreatePostLoading,
              child: ListView(
                children: [
                  Column(
                    children: [
                      const Divider(color: Colors.black, thickness: 0.9),
                      Container(
                        width: double.infinity,
                        height: widget.width * 0.56,
                        margin: EdgeInsets.symmetric(horizontal: widget.width * 0.03),
                        decoration: BoxDecoration(
                            border: Border.all(color: Colors.black, width: 0.7),
                            borderRadius: const BorderRadius.all(Radius.circular(6.0))),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            SizedBox(
                              height: widget.height * 0.1,
                              child: Row(
                                children: [
                                  Padding(
                                    padding: EdgeInsets.symmetric(horizontal: widget.width * 0.02),
                                    child: Container(
                                      width: 36,
                                      height: 36,
                                      decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        image: DecorationImage(
                                          fit: BoxFit.cover,
                                          image: HiveHelper.getUserAvatar != '' || HiveHelper.getUserAvatar == null
                                              ? FadeInImage.memoryNetwork(
                                                  image: HiveHelper.getUserAvatar!,
                                                  placeholder: kTransparentImage,
                                                ).image
                                              : Image.asset('assets/images/profile_icon.png').image,
                                        ),
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                    child: Form(
                                      key: cubit.postFormKey,
                                      child: CustomTextField(
                                        showBorder: false,
                                        hint: 'mind'.tr(),
                                        type: TextInputType.multiline,
                                        controller: cubit.postController,
                                        onChange: (value) => setState(() => text = value),
                                        validator: (value) {
                                          if (value!.isEmpty) return 'post_empty'.tr();
                                          if (value.length < 4) return 'validation.post_valid'.tr();
                                          return null;
                                        },
                                        suffix: cubit.postController.text.isEmpty
                                            ? const SizedBox()
                                            : IconButton(
                                                splashRadius: 1,
                                                onPressed: () => cubit.postController.clear(),
                                                icon: const Icon(Icons.clear, color: Colors.black)),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: widget.width * 0.03),
                              child: Row(
                                children: [
                                  InkWell(
                                    onTap: () async {
                                      FocusManager.instance.primaryFocus?.unfocus();
                                      await cubit.pickImageForPost();
                                      setState(() {});
                                    },
                                    child: Row(
                                      children: <Widget>[
                                        SvgPicture.asset('assets/images/gallery.svg'),
                                        SizedBox(width: widget.width * 0.02),
                                        Text("upload".tr(), style: AppTextStyles.bold12)
                                      ],
                                    ),
                                  ),
                                  SizedBox(width: widget.width * 0.02),
                                  DropdownButtonHideUnderline(
                                    child: DropdownButton2(
                                      iconSize: 0,
                                      isDense: false,
                                      isExpanded: true,
                                      buttonHeight: 40,
                                      itemPadding: EdgeInsets.zero,
                                      buttonPadding: EdgeInsets.zero,
                                      dropdownPadding: EdgeInsets.zero,
                                      buttonWidth: widget.width * 0.27,
                                      dropdownWidth: widget.width * 0.65,
                                      dropdownMaxHeight: widget.height * 0.55,
                                      onChanged: (value) => cubit.emitFunction(),
                                      style: TextStyle(
                                          fontFamily: widget.isEn ? 'Montserrat' : 'Tajawal', color: Colors.black),
                                      hint: Row(
                                        children: <Widget>[
                                          SvgPicture.asset('assets/images/tag.svg'),
                                          const SizedBox(width: 5),
                                          Text("tagPeople".tr(), style: AppTextStyles.bold12),
                                        ],
                                      ),
                                      items: tags.map((item) {
                                        return DropdownMenuItem<String>(
                                          value: item.text,
                                          onTap: () {},
                                          child: StatefulBuilder(
                                            builder: (context, menuSetState) {
                                              bool isSelected = selectedTags.contains(item.text);
                                              return InkWell(
                                                onTap: () {
                                                  isSelected
                                                      ? cubit.removeFromSelectedTagsList(tag: item.text!, id: item.id!)
                                                      : cubit.addToSelectedTagsList(tag: item.text!, id: item.id!);
                                                  setState(() {});
                                                  menuSetState(() {});
                                                  cubit.emitFunction();
                                                },
                                                child: Container(
                                                  height: double.infinity,
                                                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                                                  child: Row(
                                                    children: [
                                                      isSelected
                                                          ? const Icon(Icons.check_box_outlined)
                                                          : const Icon(Icons.check_box_outline_blank),
                                                      SizedBox(width: widget.width * 0.01),
                                                      Text(item.text!, style: const TextStyle(fontSize: 12)),
                                                    ],
                                                  ),
                                                ),
                                              );
                                            },
                                          ),
                                        );
                                      }).toList(),
                                    ),
                                  ),
                                  const Spacer(),
                                  Expanded(
                                    flex: 3,
                                    child: MaterialButton(
                                      minWidth: 120,
                                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
                                      color: Colors.black,
                                      onPressed: () async {
                                        if (cubit.selectedTagsIdList.isEmpty || cubit.selectedTagsIdList == []) {
                                          PopupHelper.showBasicSnack(msg: 'chooseTag'.tr());
                                          return;
                                        }
                                        if (cubit.postFormKey.currentState!.validate()) {
                                          if (cubit.postController.text.isNotEmpty) {
                                            await cubit.addPost(postText: cubit.postController.text.trim());
                                            cubit.postController.clear();
                                            setState(() {
                                              text = '';
                                              cubit.postImage = null;
                                              cubit.postImageUrl = null;
                                            });
                                            MagicRouter.pop();
                                            PopupHelper.showBasicSnack(msg: "postReview".tr(), color: Colors.green);
                                          }
                                        }
                                      },
                                      child: Text(
                                        "post".tr(),
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontWeight: FontWeight.bold,
                                            fontFamily: widget.isEn ? 'Montserrat' : 'Tajawal'),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.all(10),
                    child: Wrap(
                      spacing: 6,
                      runSpacing: 6,
                      textDirection: widget.isEn ? TextDirection.ltr : TextDirection.rtl,
                      children: selectedTags
                          .map((item) => TagCard(
                              text: item,
                              onTap: () => cubit.removeFromSelectedTagsList(
                                  tag: item, id: cubit.tagsList!.firstWhere((element) => element.text! == item).id!)))
                          .toList(),
                    ),
                  ),
                  cubit.postImage == null
                      ? const SizedBox()
                      : Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 8),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(12.0),
                            child: Stack(
                              children: <Widget>[
                                GestureDetector(
                                  onTap: () async => await showImageViewer(
                                      context,
                                      Image.file(
                                        cubit.postImage!,
                                        fit: BoxFit.cover,
                                        width: widget.width,
                                        height: widget.height * 0.3,
                                      ).image,
                                      swipeDismissible: true,
                                      doubleTapZoomable: true),
                                  child: Image.file(cubit.postImage!, fit: BoxFit.cover, width: widget.width),
                                ),
                                Positioned(
                                  left: 0,
                                  right: 0,
                                  bottom: 0,
                                  top: 0,
                                  child: Align(
                                    child: CircleAvatar(
                                      backgroundColor: Colors.black38,
                                      radius: 32.0,
                                      child: InkWell(
                                        onTap: () {
                                          cubit.postImage = null;
                                          setState(() {});
                                        },
                                        child: const Icon(Icons.delete, color: Colors.red, size: 36),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        )
                ],
              ),
            );
          },
        ),
      ),
    );
  }
}
