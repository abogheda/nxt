// ignore_for_file: depend_on_referenced_packages

import 'dart:io';
import 'package:easy_image_viewer/easy_image_viewer.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:nxt/config/router/router.dart';
import 'package:nxt/data/models/community/post_model.dart';
import 'package:nxt/data/service/hive_services.dart';
import 'package:nxt/utils/constants/resources.dart';
import 'package:nxt/view/widgets/custom_text_field.dart';
import 'package:nxt/view/screens/connect/connect_screens/general/general_widgets/create_post_screen.dart';
import 'package:nxt/view/screens/connect/cubit/connect_cubit.dart';
import 'package:nxt/view/screens/navigation_and_appbar/import_widget.dart';
import 'package:nxt/view/widgets/error_state_widget.dart';
import 'package:nxt/view/widgets/loading_state_widget.dart';
import 'package:nxt/view/widgets/no_state_widget.dart';
import 'package:flutter_svg/svg.dart';
import 'package:transparent_image/transparent_image.dart';
import '../../../../../../utils/helpers/picker_helper.dart';

import '../../../../../../utils/helpers/popup_helper.dart';
import '../../../../../../utils/helpers/utils.dart';
import '../../../../../widgets/delete_dialog.dart';
import '../../../../community_user_profile/community_user_profile.dart';
import '../../../../talent_profile/talent_profile_screen.dart';
import 'post_header.dart';
import 'package:timeago/timeago.dart' as timeago;
part 'comment_page.dart';
part 'create_post_card.dart';
part 'comment_card.dart';
part 'community_post.dart';
