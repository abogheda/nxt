part of 'imports.dart';

class CommentsPage extends StatefulWidget {
  final PostModel userPost;
  const CommentsPage({Key? key, required this.userPost}) : super(key: key);

  @override
  State<CommentsPage> createState() => _CommentsPageState();
}

class _CommentsPageState extends State<CommentsPage> {
  File? commentImage;
  String? commentImageUrl;
  late ConnectCubit cubit;
  final commentController = TextEditingController();

  @override
  void dispose() {
    commentController.clear();
    commentImageUrl = null;
    commentImage = null;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return CustomScaffoldWidget(
      extendBodyBehindAppBar: false,
      isBackArrow: true,
      isThereLogo: true,
      isThereActions: true,
      body: BlocConsumer<ConnectCubit, ConnectState>(
        listener: (context, state) {
          if (state is ReportPostCommentSuccess) {
            Navigator.pop(context);
            if (state.reportModel.statusCode == 404 || state.reportModel.message != null) {
              PopupHelper.showBasicSnack(msg: 'commentAlreadyReported'.tr());
              return;
            }
            PopupHelper.showBasicSnack(msg: 'commentReported'.tr());
          }
          if (state is ReportPostCommentFailed) {
            Navigator.pop(context);
            PopupHelper.showBasicSnack(msg: state.msg);
          }
          if (state is ReportPostSuccess) {
            Navigator.pop(context);
            if (state.reportModel.statusCode == 404 || state.reportModel.message != null) {
              PopupHelper.showBasicSnack(msg: 'postAlreadyReported'.tr());
              return;
            }
            PopupHelper.showBasicSnack(msg: 'postReported'.tr());
          }
          if (state is ReportPostFailed) {
            Navigator.pop(context);
            PopupHelper.showBasicSnack(msg: state.msg);
          }
        },
        builder: (context, state) {
          var cubit = ConnectCubit.get(context);
          if (state is CommentFailed) {
            return ErrorStateWidget(
                hasRefresh: true, onRefresh: () async => await cubit.getAllComments(id: widget.userPost.id!));
          }
          return Column(
            children: [
              Expanded(
                child: RefreshIndicator(
                  onRefresh: () async => await cubit.getAllComments(id: widget.userPost.id!),
                  child: ListView(
                    shrinkWrap: true,
                    children: [
                      const Divider(color: Colors.black, thickness: 0.9),
                      SizedBox(height: widget.height * 0.01),
                      CommunityPost(userPost: widget.userPost),
                      SizedBox(height: widget.height * 0.03),
                      state is CommentLoading
                          ? Column(
                              children: <Widget>[SizedBox(height: widget.height * 0.2), const LoadingStateWidget()])
                          : const CommentCard(),
                      const SizedBox(height: 10),
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 8),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    commentImage == null
                        ? const SizedBox()
                        : Padding(
                            padding: const EdgeInsetsDirectional.only(start: 12, end: 12, top: 4, bottom: 8),
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(12.0),
                              child: SizedBox(
                                width: 90,
                                height: 90,
                                child: Stack(
                                  children: <Widget>[
                                    GestureDetector(
                                      onTap: () async {
                                        await showImageViewer(
                                          context,
                                          Image.file(commentImage!, width: 90, height: 90, fit: BoxFit.cover).image,
                                          swipeDismissible: true,
                                          doubleTapZoomable: true,
                                        );
                                      },
                                      child: Image.file(commentImage!, width: 90, height: 90, fit: BoxFit.cover),
                                    ),
                                    Align(
                                      child: IconButton(
                                        onPressed: () => setState(() => commentImage = null),
                                        icon: const Icon(Icons.delete, color: Colors.red),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ),
                    Row(
                      children: [
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: widget.width * 0.03),
                          child: SizedBox(
                            width: 36,
                            height: 36,
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(50),
                              child: FadeInImage.memoryNetwork(
                                image: HiveHelper.getUserAvatar == ''
                                    ? userAvatarPlaceHolderUrl
                                    : HiveHelper.getUserAvatar ?? userAvatarPlaceHolderUrl,
                                fit: BoxFit.cover,
                                placeholder: kTransparentImage,
                              ),
                            ),
                          ),
                        ),
                        Expanded(
                          child: Form(
                            key: cubit.commentFormKey,
                            child: Container(
                              margin: EdgeInsets.only(left: widget.isEn ? 0 : 12, right: widget.isEn ? 12 : 0),
                              child: CustomTextField(
                                controller: commentController,
                                type: TextInputType.multiline,
                                hint: 'writeComment'.tr(),
                                suffix: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    IconButton(
                                      splashRadius: 12,
                                      icon: const Icon(Icons.camera_alt_outlined, color: Colors.black),
                                      padding: EdgeInsets.zero,
                                      constraints: const BoxConstraints(),
                                      onPressed: () async {
                                        FocusManager.instance.primaryFocus?.unfocus();
                                        commentImage = await PickerHelper.pickGalleryImage();
                                        setState(() {});
                                      },
                                    ),
                                    IconButton(
                                      splashRadius: 12,
                                      icon: state is AddCommentLoading
                                          ? const SizedBox(
                                              width: 20,
                                              height: 20,
                                              child: CircularProgressIndicator.adaptive(strokeWidth: 2))
                                          : const Icon(Icons.send_outlined, color: Colors.black),
                                      onPressed: () async {
                                        if (commentController.text.isNotEmpty || commentImage != null) {
                                          await cubit.addComment(
                                            commentText: commentController.text.trim(),
                                            commentPostId: widget.userPost.id,
                                            commentImage: commentImage,
                                            commentImageUrl: commentImageUrl,
                                          );
                                          commentController.clear();
                                          setState(() {
                                            commentImage = null;
                                            commentImageUrl = null;
                                          });
                                          await cubit.getAllComments(id: widget.userPost.id!);
                                        }
                                      },
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          );
        },
      ),
    );
  }
}
