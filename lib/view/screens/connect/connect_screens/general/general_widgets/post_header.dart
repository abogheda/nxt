import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import '../../../../../../config/router/router.dart';
import '../../../../../../data/service/hive_services.dart';
import 'package:transparent_image/transparent_image.dart';
import '../../../../../../utils/constants/resources.dart';
import '../../../../community_user_profile/community_user_profile.dart';
import '../../../../talent_profile/talent_profile_screen.dart';

enum MenuItems {
  save,
  delete,
  report;
}

class PostHeader extends StatelessWidget {
  final String? userProfile;
  final String userName;
  final String userId;
  final String createdPostDate;
  final String savePopupText;
  // final Function() onSaveTap;
  final Function() onReportTap;
  final Function() onDeleteTap;
  final bool showReport;
  const PostHeader({
    Key? key,
    required this.userProfile,
    required this.userName,
    required this.userId,
    required this.createdPostDate,
    required this.savePopupText,
    // required this.onSaveTap,
    required this.onReportTap,
    required this.showReport,
    required this.onDeleteTap,
  }) : super(key: key);
  void navigateToUserProfile() => MagicRouter.navigateTo(
      HiveHelper.getUserInfo!.user!.id == userId ? const TalentProfileScreen() : CommunityUserProfile(userId: userId));

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        GestureDetector(
          onTap: () => navigateToUserProfile(),
          child: Container(
            width: width * 0.11,
            height: width * 0.11,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              image: DecorationImage(
                  image: userProfile != null && userProfile != ''
                      ? FadeInImage.memoryNetwork(
                          image: userProfile!,
                          placeholder: kTransparentImage,
                        ).image
                      : Image.asset('assets/images/profile_icon.png').image,
                  fit: BoxFit.cover),
            ),
          ),
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: height * 0.012),
            InkWell(
              onTap: () => navigateToUserProfile(),
              child: Padding(
                padding: EdgeInsetsDirectional.only(start: width * 0.025),
                child: Text(
                  userName,
                  style: Theme.of(context)
                      .textTheme
                      .titleLarge!
                      .copyWith(color: Colors.black, fontFamily: isEn ? 'BebasNeue' : 'Tajawal'),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsetsDirectional.only(start: width * 0.025),
              child: Text(
                createdPostDate,
                style: Theme.of(context)
                    .textTheme
                    .titleSmall!
                    .copyWith(color: Colors.black, fontFamily: isEn ? 'Montserrat' : 'Tajawal'),
              ),
            ),
            SizedBox(height: height * 0.012),
          ],
        ),
        const Spacer(),
        PopupMenuButton<MenuItems>(
          onSelected: (value) {
            if (showReport) {
              switch (value) {
                // case MenuItems.save:
                //   onSaveTap();
                //   break;
                case MenuItems.delete:
                  onDeleteTap();
                  break;
                default:
              }
            } else {
              switch (value) {
                case MenuItems.save:
                // onSaveTap();
                // break;
                case MenuItems.report:
                  onReportTap();
                  break;
                default:
              }
            }
          },
          itemBuilder: (BuildContext context) {
            final allList = [
              // PopupMenuItem(
              //   value: MenuItems.save,
              //   child: Text(savePopupText, style: TextStyle(fontFamily: isEn ? 'BebasNeue' : 'Tajawal')),
              // ),
              PopupMenuItem(
                value: MenuItems.report,
                child: Text('report'.tr(), style: TextStyle(fontFamily: isEn ? 'BebasNeue' : 'Tajawal')),
              ),
            ];
            final userOnlyList = [
              // PopupMenuItem(
              //   value: MenuItems.save,
              //   child: Text(savePopupText, style: TextStyle(fontFamily: isEn ? 'BebasNeue' : 'Tajawal')),
              // ),
              PopupMenuItem(
                value: MenuItems.delete,
                child: Text('delete'.tr(), style: TextStyle(fontFamily: isEn ? 'BebasNeue' : 'Tajawal')),
              ),
            ];
            return showReport ? userOnlyList : allList;
          },
        )
      ],
    );
  }
}
