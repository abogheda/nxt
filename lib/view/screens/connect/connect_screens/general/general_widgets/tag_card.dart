import 'package:flutter/material.dart';
import '../../../../../../utils/constants/app_const.dart';

import '../../../../../../utils/constants/app_colors.dart';

class TagCard extends StatelessWidget {
  const TagCard({Key? key, required this.text, this.onTap}) : super(key: key);
  final String text;
  final Function()? onTap;
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      borderRadius: const BorderRadius.all(Radius.circular(6.0)),
      child: Container(
        decoration: BoxDecoration(
            border: Border.all(color: AppColors.borderGreyColor),
            borderRadius: const BorderRadius.all(Radius.circular(6.0)),
            color: Colors.black),
        padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 6),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Text(
              text,
              style: Theme.of(context).textTheme.bodyText1!.copyWith(
                    color: Colors.white,
                    fontFamily: isEn ? 'Montserrat' : 'Tajawal',
                  ),
            ),
            const Icon(
              Icons.clear,
              color: Colors.white,
            ),
          ],
        ),
      ),
    );
  }
}
