part of 'imports.dart';

class CommentCard extends StatefulWidget {
  const CommentCard({Key? key}) : super(key: key);

  @override
  State<CommentCard> createState() => _CommentCardState();
}

class _CommentCardState extends State<CommentCard> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ConnectCubit, ConnectState>(
      buildWhen: (previous, current) => current is CommentDone,
      builder: (context, state) {
        final cubit = ConnectCubit.get(context);
        if (state is CommentLoading) {
          return const LoadingStateWidget();
        } else if (state is CommentFailed) {
          return const ErrorStateWidget(hasRefresh: false);
        } else {
          final comments = cubit.comments!;
          if (comments.isEmpty) {
            return NoStateWidget(
              isDefaultText: false,
              newTextStyle: AppTextStyles.bold13,
              newText: '${'noComment'.tr()}.. \n ${'beTheFirstToComment'.tr()} !',
            );
          }
          void navigateToUserProfile({required String userId}) {
            MagicRouter.navigateTo(HiveHelper.getUserInfo!.user!.id == userId
                ? const TalentProfileScreen()
                : CommunityUserProfile(userId: userId));
          }

          return ListView.builder(
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            itemCount: comments.length,
            itemBuilder: (context, index) {
              final comment = comments[index];
              return Column(
                children: [
                  IntrinsicHeight(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 12.0),
                      child: Row(
                        children: [
                          GestureDetector(
                            onTap: () => navigateToUserProfile(userId: comment.user!),
                            child: Container(
                              width: 40,
                              height: 40,
                              decoration: BoxDecoration(
                                color: AppColors.greyOutText,
                                borderRadius: BorderRadius.circular(50),
                                image: DecorationImage(
                                  fit: BoxFit.cover,
                                  image: FadeInImage.memoryNetwork(
                                    image: comment.author?.avatar == ''
                                        ? userAvatarPlaceHolderUrl
                                        : comment.author?.avatar ?? userAvatarPlaceHolderUrl,
                                    placeholder: kTransparentImage,
                                  ).image,
                                ),
                              ),
                            ),
                          ),
                          SizedBox(width: widget.width * 0.02),
                          Expanded(
                            child: Column(
                              children: <Widget>[
                                Container(
                                  margin: EdgeInsetsDirectional.only(end: widget.width * 0.02),
                                  width: double.infinity,
                                  decoration: BoxDecoration(
                                      color: const Color(0xFFF8F8F8), borderRadius: BorderRadius.circular(12)),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      GestureDetector(
                                        onTap: () => navigateToUserProfile(userId: comment.user!),
                                        child: Padding(
                                          padding: EdgeInsets.all(widget.width * 0.025),
                                          child: Text(comment.author?.name ?? 'noUsername'.tr(),
                                              style: AppTextStyles.huge20),
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.symmetric(horizontal: widget.width * 0.03),
                                        child: Text(
                                          comment.comment!.trim(),
                                          style: Theme.of(context).textTheme.bodyText1!.copyWith(
                                              color: const Color(0xFF555555),
                                              fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal'),
                                        ),
                                      ),
                                      comment.media != null
                                          ? Padding(
                                              padding: EdgeInsets.symmetric(horizontal: widget.width * 0.03),
                                              child: Container(
                                                margin: EdgeInsets.symmetric(vertical: widget.height * 0.01),
                                                child: ClipRRect(
                                                  borderRadius: BorderRadius.circular(12),
                                                  child: Container(
                                                    width: double.infinity,
                                                    height: widget.height * 0.25,
                                                    color: const Color(0xFFA5A5A5),
                                                    child: GestureDetector(
                                                        onTap: () async {
                                                          await showImageViewer(
                                                            context,
                                                            FadeInImage.memoryNetwork(
                                                              image: comment.media == 'string'
                                                                  ? placeHolderUrl
                                                                  : comment.media!,
                                                              placeholder: kTransparentImage,
                                                            ).image,
                                                            swipeDismissible: true,
                                                            doubleTapZoomable: true,
                                                          );
                                                        },
                                                        child: FadeInImage.memoryNetwork(
                                                            image: comment.media == 'string'
                                                                ? placeHolderUrl
                                                                : comment.media!,
                                                            placeholder: kTransparentImage,
                                                            fit: BoxFit.cover)),
                                                  ),
                                                ),
                                              ),
                                            )
                                          : const SizedBox(height: 8),
                                    ],
                                  ),
                                ),
                                const SizedBox(height: 8),
                                Row(
                                  children: [
                                    Text(
                                      timeago.format(comment.createdAt!, locale: widget.isEn ? 'en_short' : 'ar_short'),
                                      style: TextStyle(
                                        fontSize: 12,
                                        color: const Color(0xff555555),
                                        fontFamily: widget.isEn ? 'Montserrat' : 'Tajawal',
                                      ),
                                    ),
                                    SizedBox(width: widget.width * 0.005),
                                    TextButton(
                                      style: TextButton.styleFrom(
                                        minimumSize: Size.zero,
                                        tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                                        padding: const EdgeInsets.fromLTRB(8.0, 4.0, 8.0, 4.0),
                                      ),
                                      onPressed: () async {
                                        await cubit.addOrRemoveLikeOnComment(id: comment.id!);
                                        setState(() {
                                          if (comment.userLiked! == false) {
                                            comment.likesCount = comment.likesCount! + 1;
                                          }
                                          if (comment.userLiked! == true) {
                                            if (comment.likesCount == 0) {
                                              return;
                                            }
                                            comment.likesCount = comment.likesCount! - 1;
                                          }
                                          comment.userLiked = !comment.userLiked!;
                                        });
                                      },
                                      child: Text(
                                        comment.userLiked! ? 'liked'.tr() : 'like'.tr(),
                                        style: comment.userLiked!
                                            ? TextStyle(
                                                fontSize: 12,
                                                fontWeight: FontWeight.bold,
                                                color: Colors.black,
                                                fontFamily: widget.isEn ? 'Montserrat' : 'Tajawal',
                                              )
                                            : TextStyle(
                                                fontSize: 12,
                                                color: const Color(0xff555555),
                                                fontFamily: widget.isEn ? 'Montserrat' : 'Tajawal',
                                              ),
                                      ),
                                    ),
                                    SizedBox(width: widget.width * 0.005),
                                    comment.user == HiveHelper.getUserInfo!.user!.id!
                                        ? const SizedBox()
                                        : TextButton(
                                            style: TextButton.styleFrom(
                                              minimumSize: Size.zero,
                                              tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                                              padding: const EdgeInsets.fromLTRB(8.0, 4.0, 8.0, 4.0),
                                            ),
                                            onPressed: () async {
                                              Utils.showAlertDialog(
                                                context: context,
                                                headerText: "reportComment".tr(),
                                                yesButtonText: "report".tr(),
                                                noButtonText: "cancel".tr(),
                                                onPressed: () async =>
                                                    await cubit.reportPostComment(commentId: comment.id!),
                                              );
                                            },
                                            child: Text(
                                              'report'.tr(),
                                              style: TextStyle(
                                                fontSize: 12,
                                                fontWeight: FontWeight.bold,
                                                color: Colors.black,
                                                fontFamily: widget.isEn ? 'Montserrat' : 'Tajawal',
                                              ),
                                            ),
                                          ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(height: widget.width * 0.01),
                  const SizedBox(height: 20)
                ],
              );
            },
          );
        }
      },
    );
  }
}
