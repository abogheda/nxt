part of 'imports.dart';

class CommunityPost extends StatefulWidget {
  final PostModel userPost;
  const CommunityPost({
    Key? key,
    required this.userPost,
  }) : super(key: key);

  @override
  State<CommunityPost> createState() => _CommunityPostState();
}

class _CommunityPostState extends State<CommunityPost> {
  @override
  Widget build(BuildContext context) {
    return BlocConsumer<ConnectCubit, ConnectState>(
      listener: (context, state) {
        if (state is ReportPostSuccess) {
          if (state.reportModel.statusCode == 404 || state.reportModel.message != null) {
            PopupHelper.showBasicSnack(msg: 'postAlreadyReported'.tr());
            return;
          }
          PopupHelper.showBasicSnack(msg: 'postReported'.tr());
        }
        if (state is ReportPostFailed) {
          PopupHelper.showBasicSnack(msg: state.msg);
        }
      },
      builder: (context, state) {
        final cubit = ConnectCubit.get(context);
        return Container(
          decoration: BoxDecoration(border: Border.all(color: Colors.black, width: 0.1), color: Colors.white),
          child: Column(
            children: [
              Padding(
                  padding: EdgeInsets.symmetric(horizontal: widget.width * 0.03),
                  child: PostHeader(
                      userId: widget.userPost.author!.id!,
                      savePopupText: widget.userPost.userSaved! ? "unsave".tr() : "save".tr(),
                      showReport: widget.userPost.author!.id == HiveHelper.getUserInfo!.user!.id,
                      userName: widget.userPost.author != null ? widget.userPost.author!.name! : "Unknown User",
                      createdPostDate: timeago.format(
                          DateTime.parse(widget.userPost.updatedAt ?? DateTime.now().toString()),
                          locale: widget.isEn ? 'en_short' : 'ar_short'),
                      userProfile:
                          widget.userPost.author != null ? widget.userPost.author!.avatar : userAvatarPlaceHolderUrl,
                      // onSaveTap: () async {
                      //   if (widget.userPost.userSaved! == true) {
                      //     await cubit.deleteSavePost(postId: widget.userPost.id!);
                      //   } else {
                      //     await cubit.addSavePost(postId: widget.userPost.id!);
                      //   }
                      //   setState(() {
                      //     widget.userPost.userSaved = !widget.userPost.userSaved!;
                      //   });
                      // },
                      onReportTap: () async {
                        Utils.showAlertDialog(
                          context: context,
                          headerText: "reportPost".tr(),
                          yesButtonText: "report".tr(),
                          noButtonText: "cancel".tr(),
                          onPressed: () async => await cubit.reportPost(postId: widget.userPost.id!),
                        );
                      },
                      onDeleteTap: () async => showDialog(
                          context: context,
                          builder: (context) => DeleteDialog(
                              headerText: "deletePost".tr(),
                              isLoading: state is DeletePostLoading,
                              onPressed: () async =>
                                  await cubit.deletePost(postId: widget.userPost.id!).then((_) async {
                                    Navigator.pop(context);
                                    Navigator.pop(context);
                                    cubit.getAllPosts();
                                  }))))),
              Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: widget.width * 0.04,
                  vertical: widget.height * 0.01,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Expanded(
                      child: Text(
                        widget.userPost.text ?? "notAvailable".tr(),
                        style: Theme.of(context).textTheme.bodyText1!.copyWith(
                              color: Theme.of(context).primaryColor,
                              fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal',
                            ),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: widget.width * 0.03),
                child: widget.userPost.media != null
                    ? widget.userPost.media!.length == 1
                        ? ClipRRect(
                            borderRadius: BorderRadius.circular(8),
                            child: GestureDetector(
                              onTap: () async {
                                await showImageViewer(
                                  context,
                                  FadeInImage.memoryNetwork(
                                    image: widget.userPost.media!.first ?? placeHolderUrl,
                                    height: widget.height * 0.35,
                                    width: double.infinity,
                                    fit: BoxFit.cover,
                                    placeholder: kTransparentImage,
                                  ).image,
                                  swipeDismissible: true,
                                  doubleTapZoomable: true,
                                );
                              },
                              child: FadeInImage.memoryNetwork(
                                image: widget.userPost.media!.first ?? placeHolderUrl,
                                height: widget.height * 0.35,
                                width: double.infinity,
                                placeholder: kTransparentImage,
                                fit: BoxFit.cover,
                              ),
                            ),
                          ) //todo use grid view for multi photos
                        : const SizedBox()
                    : null,
              ),
              const SizedBox(height: 10),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Row(
                  children: [
                    SvgPicture.asset('assets/images/blackroundlike.svg'),
                    const SizedBox(width: 5),
                    Text(
                      widget.userPost.likedCount != null ? widget.userPost.likedCount.toString() : "0",
                      style: widget.userPost.userLiked!
                          ? AppTextStyles.bold13.apply(color: Colors.black)
                          : AppTextStyles.bold13.apply(color: AppColors.iconsColor),
                    ),
                  ],
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  InkWell(
                    onTap: () async {
                      if (widget.userPost.likedCount == null) {
                        await cubit.addOrRemoveLikeOnPost(id: widget.userPost.id!).then((value) {
                          cubit.postsList = null;
                          cubit.getAllPosts();
                        });
                        widget.userPost.likedCount = 0;
                        setState(() {
                          if (widget.userPost.userLiked! == false) {
                            widget.userPost.likedCount = widget.userPost.likedCount! + 1;
                          }
                          if (widget.userPost.userLiked! == true) {
                            if (widget.userPost.likedCount! == 0) {
                              return;
                            }
                            widget.userPost.likedCount = widget.userPost.likedCount! - 1;
                          }
                          widget.userPost.userLiked = !widget.userPost.userLiked!;
                        });
                      }

                      await cubit.addOrRemoveLikeOnPost(id: widget.userPost.id!).then((value) {
                        cubit.postsList = null;
                        cubit.getAllPosts();
                      });
                      setState(() {
                        if (widget.userPost.userLiked! == false) {
                          widget.userPost.likedCount = widget.userPost.likedCount! + 1;
                        }
                        if (widget.userPost.userLiked! == true) {
                          if (widget.userPost.likedCount! == 0) {
                            return;
                          }
                          widget.userPost.likedCount = widget.userPost.likedCount! - 1;
                        }
                        widget.userPost.userLiked = !widget.userPost.userLiked!;
                      });
                    },
                    child: SizedBox(
                      height: 60,
                      width: 125,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          SvgPicture.asset(
                            widget.userPost.userLiked! ? 'assets/images/liked.svg' : 'assets/images/like.svg',
                            width: 24,
                            height: 24,
                            color: widget.userPost.userLiked! ? Colors.black : AppColors.iconsColor,
                          ),
                          const SizedBox(width: 5),
                          Text(
                            widget.userPost.userLiked! ? 'liked'.tr() : 'like'.tr(),
                            style: Theme.of(context).textTheme.bodyText1!.copyWith(
                                  fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal',
                                  fontWeight: widget.userPost.userLiked! ? FontWeight.bold : null,
                                  color: widget.userPost.userLiked! ? Colors.black : AppColors.iconsColor,
                                ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 60,
                    width: 125,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SvgPicture.asset(
                          'assets/images/comment.svg',
                          width: 24,
                          height: 20,
                        ),
                        const SizedBox(width: 5),
                        Text(
                          'comment'.tr(),
                          style: Theme.of(context).textTheme.bodyText1!.copyWith(
                                fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal',
                                color: AppColors.iconsColor,
                              ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ],
          ),
        );
      },
    );
  }
}
