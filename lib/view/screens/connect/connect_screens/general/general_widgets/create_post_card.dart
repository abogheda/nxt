part of 'imports.dart';

class CreatePostCard extends StatelessWidget {
  const CreatePostCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () =>
          MagicRouter.navigateTo(BlocProvider(create: (context) => ConnectCubit(), child: const CreatePostScreen())),
      child: Container(
        height: height * 0.1,
        margin:
            EdgeInsets.only(top: height * 0.012, right: width * 0.03, left: width * 0.03, bottom: height * 0.012 - 6),
        decoration: BoxDecoration(
            color: Colors.white,
            border: Border.all(color: Colors.black, width: 0.8),
            borderRadius: const BorderRadius.all(Radius.circular(6.0))),
        child: Row(
          children: [
            Padding(
              padding: EdgeInsets.symmetric(horizontal: width * 0.03),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(50),
                child: Container(
                  width: 32,
                  height: 32,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    image: DecorationImage(
                      fit: BoxFit.cover,
                      image: HiveHelper.getUserAvatar != '' || HiveHelper.getUserAvatar == null
                          ? FadeInImage.memoryNetwork(
                              image: HiveHelper.getUserAvatar!,
                              placeholder: kTransparentImage,
                            ).image
                          : Image.asset('assets/images/profile_icon.png').image,
                    ),
                  ),
                ),
              ),
            ),
            Text("mind".tr(),
                style: AppTextStyles.black13
                    .apply(color: AppColors.iconsColor, fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal')),
          ],
        ),
      ),
    );
  }
}
