// ignore_for_file: depend_on_referenced_packages
import 'package:transparent_image/transparent_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:nxt/config/router/router.dart';
import 'package:nxt/utils/constants/app_const.dart';
import 'package:nxt/utils/helpers/popup_helper.dart';
import 'package:nxt/view/screens/connect/cubit/connect_cubit.dart';
import 'package:nxt/view/widgets/app_loader.dart';
import 'package:nxt/view/widgets/error_state_widget.dart';
import 'package:nxt/view/widgets/no_state_widget.dart';
import 'package:timeago/timeago.dart' as timeago;

import '../../../../../data/service/hive_services.dart';
import '../../../../../utils/constants/app_colors.dart';
import '../../../../../utils/constants/app_images.dart';
import '../../../../../utils/constants/app_text_style.dart';
import '../../../../../utils/helpers/utils.dart';
import '../../../../widgets/delete_dialog.dart';
import 'general_widgets/imports.dart';
import 'general_widgets/post_header.dart';

class GeneralScreen extends StatefulWidget {
  const GeneralScreen({Key? key}) : super(key: key);

  @override
  State<GeneralScreen> createState() => _GeneralScreenState();
}

class _GeneralScreenState extends State<GeneralScreen> {
  bool isLoading = false;
  @override
  Widget build(BuildContext context) {
    return BlocConsumer<ConnectCubit, ConnectState>(
      listener: (context, state) {
        if (state is CommentFailed) {
          PopupHelper.showBasicSnack(msg: state.msg, color: Colors.red);
        }
        if (state is LikeOnPostFailed) {
          PopupHelper.showBasicSnack(msg: state.msg, color: Colors.red);
        }
        if (state is SavePostFailed) {
          PopupHelper.showBasicSnack(msg: state.msg, color: Colors.red);
        }
        if (state is ReportPostSuccess) {
          if (state.reportModel.statusCode == 404 || state.reportModel.message != null) {
            PopupHelper.showBasicSnack(msg: 'postAlreadyReported'.tr());
            return;
          }
          PopupHelper.showBasicSnack(msg: 'postReported'.tr());
        }
        if (state is ReportPostFailed) {
          PopupHelper.showBasicSnack(msg: state.msg, color: Colors.red);
        }
        if (state is DeletePostFailed) {
          PopupHelper.showBasicSnack(msg: state.msg, color: Colors.red);
        }
      },
      builder: (context, state) {
        var cubit = ConnectCubit.get(context);
        if (state is GetAllPostsLoading || cubit.postsList == null) {
          return const AppLoader();
        } else if (state is GetAllPostsFailed) {
          return ErrorStateWidget(hasRefresh: true, onRefresh: () async => await cubit.getAllPosts());
        }
        return RefreshIndicator(
          onRefresh: () async => await cubit.getAllPosts(),
          child: ListView(
            shrinkWrap: true,
            children: [
              const CreatePostCard(),
              cubit.postsList!.isEmpty
                  ? Padding(padding: EdgeInsets.symmetric(vertical: widget.height * 0.28), child: NoStateWidget())
                  : ListView.builder(
                      shrinkWrap: true,
                      physics: const ClampingScrollPhysics(),
                      itemCount: cubit.postsList!.length,
                      itemBuilder: (context, index) {
                        final post = cubit.postsList![index];
                        return Container(
                          margin: const EdgeInsets.symmetric(vertical: 6),
                          padding: EdgeInsets.symmetric(horizontal: widget.width * 0.03),
                          decoration:
                              BoxDecoration(border: Border.all(color: Colors.black, width: 0.1), color: Colors.white),
                          child: Column(
                            children: [
                              post.author == null
                                  ? const SizedBox()
                                  : PostHeader(
                                      userId: post.author!.id ?? '',
                                      showReport: post.author!.id == HiveHelper.getUserInfo!.user!.id,
                                      savePopupText: post.userSaved! ? "unsave".tr() : "save".tr(),
                                      createdPostDate: timeago.format(
                                          DateTime.parse(post.updatedAt ?? DateTime.now().toString()),
                                          locale: widget.isEn ? 'en_short' : 'ar_short'),
                                      userName: post.author != null ? post.author!.name! : "unknownUser".tr(),
                                      userProfile: post.author != null ? post.author!.avatar : userAvatarPlaceHolderUrl,
                                      onReportTap: () {
                                        Utils.showAlertDialog(
                                          context: context,
                                          noButtonText: "cancel".tr(),
                                          yesButtonText: "report".tr(),
                                          headerText: "reportPost".tr(),
                                          onPressed: () async {
                                            setState(() {});
                                            await cubit
                                                .reportPost(postId: post.id!)
                                                .then((_) => Navigator.pop(context));
                                          },
                                        );
                                      },
                                      // onSaveTap: () async {
                                      //   if (post.userSaved! == true) {
                                      //     await cubit.deleteSavePost(postId: post.id!);
                                      //   } else {
                                      //     await cubit.addSavePost(postId: post.id!);
                                      //   }
                                      //   setState(() {
                                      //     post.userSaved = !post.userSaved!;
                                      //   });
                                      // },
                                      onDeleteTap: () async => showDialog(
                                          context: context,
                                          builder: (context) => DeleteDialog(
                                              headerText: "deletePost".tr(),
                                              isLoading: state is DeletePostLoading,
                                              onPressed: () async =>
                                                  await cubit.deletePost(postId: post.id!).then((_) async {
                                                    Navigator.pop(context);
                                                    cubit.getAllPosts();
                                                  })))),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Expanded(
                                    child: Text(post.text ?? "notAvailable".tr(),
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyText1!
                                            .copyWith(fontFamily: widget.isEn ? 'Montserrat' : 'Tajawal')),
                                  )
                                ],
                              ),
                              const SizedBox(height: 10),
                              SizedBox(
                                child: post.media != null
                                    ? post.media!.length == 1
                                        ? ClipRRect(
                                            borderRadius: BorderRadius.circular(8),
                                            child: GestureDetector(
                                              onTap: () => MagicRouter.navigateTo(BlocProvider.value(
                                                  value: cubit..getAllComments(id: post.id!),
                                                  child: CommentsPage(userPost: post))),
                                              child: FadeInImage.memoryNetwork(
                                                fit: BoxFit.cover,
                                                width: double.infinity,
                                                height: widget.height * 0.35,
                                                placeholder: kTransparentImage,
                                                image: post.media!.first ?? placeHolderUrl,
                                              ),
                                            ),
                                          )
                                        : const SizedBox()
                                    : null,
                              ),
                              const SizedBox(height: 6),
                              Padding(
                                padding: const EdgeInsetsDirectional.only(start: 10),
                                child: Row(
                                  children: [
                                    SvgPicture.asset('assets/images/blackroundlike.svg'),
                                    const SizedBox(width: 5),
                                    Text(
                                      post.likedCount != null ? post.likedCount.toString() : "0",
                                      style: post.userLiked!
                                          ? AppTextStyles.bold13.apply(color: Colors.black)
                                          : AppTextStyles.bold13.apply(color: AppColors.iconsColor),
                                    ),
                                  ],
                                ),
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Expanded(
                                    child: InkWell(
                                      onTap: () async {
                                        if (post.likedCount == null) {
                                          await cubit.addOrRemoveLikeOnPost(id: post.id!);
                                          post.likedCount = 0;
                                          setState(() {
                                            if (post.userLiked! == false) {
                                              post.likedCount = post.likedCount! + 1;
                                            }
                                            if (post.userLiked! == true) {
                                              if (post.likedCount! == 0) {
                                                return;
                                              }
                                              post.likedCount = post.likedCount! - 1;
                                            }
                                            post.userLiked = !post.userLiked!;
                                          });
                                          return;
                                        }
                                        setState(() {
                                          if (post.userLiked! == false) {
                                            post.likedCount = post.likedCount! + 1;
                                          }
                                          if (post.userLiked! == true) {
                                            if (post.likedCount! == 0) {
                                              return;
                                            }
                                            post.likedCount = post.likedCount! - 1;
                                          }
                                          post.userLiked = !post.userLiked!;
                                        });
                                        await cubit.addOrRemoveLikeOnPost(id: post.id!);
                                      },
                                      child: SizedBox(
                                        height: 60,
                                        width: 125,
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children: [
                                            SvgPicture.asset(
                                              post.userLiked! ? 'assets/images/liked.svg' : 'assets/images/like.svg',
                                              width: 24,
                                              height: 24,
                                              color: post.userLiked! ? Colors.black : AppColors.iconsColor,
                                            ),
                                            const SizedBox(width: 5),
                                            Text(
                                              post.userLiked! ? 'liked'.tr() : 'like'.tr(),
                                              style: post.userLiked!
                                                  ? TextStyle(
                                                      fontSize: 12,
                                                      fontWeight: FontWeight.bold,
                                                      color: Colors.black,
                                                      fontFamily: widget.isEn ? 'Montserrat' : 'Tajawal',
                                                    )
                                                  : TextStyle(
                                                      fontSize: 12,
                                                      color: const Color(0xff555555),
                                                      fontFamily: widget.isEn ? 'Montserrat' : 'Tajawal',
                                                    ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                    child: IconButton(
                                      onPressed: () => MagicRouter.navigateTo(BlocProvider.value(
                                          value: cubit..getAllComments(id: post.id!),
                                          child: CommentsPage(userPost: post))),
                                      icon: Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: [
                                          SvgPicture.asset('assets/images/comment.svg', width: 24, height: 20),
                                          const SizedBox(width: 5),
                                          Text('comment'.tr(),
                                              style: Theme.of(context).textTheme.bodyText1!.copyWith(
                                                  fontFamily: widget.isEn ? 'Montserrat' : 'Tajawal',
                                                  color: AppColors.iconsColor)),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              )
                            ],
                          ),
                        );
                      },
                    ),
            ],
          ),
        );
      },
    );
  }
}
