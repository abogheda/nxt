// ignore_for_file: unnecessary_import, depend_on_referenced_packages
import 'dart:io';
import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nxt/data/models/community/create_post_model.dart';
import 'package:nxt/data/models/community/comments_model.dart';
import 'package:nxt/data/models/community/partner_model.dart';
import 'package:nxt/data/models/community/post_model.dart';
import 'package:nxt/data/models/jobs/jobs_model.dart';
import 'package:nxt/data/models/tags/tag_model.dart';
import 'package:nxt/data/repos/imports.dart';
import 'package:nxt/utils/helpers/picker_helper.dart';

import '../../../../data/enum/report_type.dart';
import '../../../../data/enum/save.dart';
import '../../../../data/enum/upload_type.dart';
import '../../../../data/models/base_models/base_single_res.dart';
import '../../../../data/models/community_user/community_user.dart';
import '../../../../data/models/jobs/job_model.dart';
import '../../../../data/models/report/report_model.dart';
import '../../../../data/models/save/save_model.dart';
import '../../../../data/repos/general_repo.dart';
import '../../../../data/repos/report/report_repo.dart';

part 'connect_state.dart';

class ConnectCubit extends Cubit<ConnectState> {
  ConnectCubit() : super(CommunityInitial());

  static ConnectCubit get(context) => BlocProvider.of(context);
  late bool isJobSaved;
  late bool isJobSavedInJobsScreen;
  //==== General Tab Variables ====
  final postController = TextEditingController();
  final commentFormKey = GlobalKey<FormState>();
  final postFormKey = GlobalKey<FormState>();
  String? commentID;
  File? postImage;
  String? postImageUrl;
  List<PostModel>? postsList;
  List<PartnerModel>? partnerList;
  List<CommentModel>? comments;
  List<TagModel>? tagsList;
  List<String> selectedTagsList = [];
  List<String> selectedTagsIdList = [];
  CommunityUser? communityUser;
  //==== Jobs Tab Variables ====
  List<JobsModel>? allJobsList;
  JobModel? jobModel;
  File? applyCubitVideo;
  File? applyCubitAudio;
  File? applyCubitText;
  //==== Save Method ====
  void inJobsInit({required bool isSavedFormModel}) => isJobSavedInJobsScreen = isSavedFormModel;
  @override
  Future<void> close() {
    postController.dispose();
    return super.close();
  }

  Future<void> saveJob({String? item, required bool inJobsScreen}) async {
    emit(SaveJobLoading());
    final res = await SaveRepo.addToSavedItems(
      SaveModel(item: item, type: SavedItemsEnum.jobs),
    );
    if (res.data != null) {
      if (inJobsScreen) {
        isJobSavedInJobsScreen = true;
        emit(SaveJobSuccess());
        return;
      } else {
        isJobSaved = true;
        emit(SaveJobSuccess());
        return;
      }
    } else {
      emit(SaveJobFailed(msg: res.message ?? ""));
    }
  }

  Future<void> deleteSaveJob({String? jobId, required bool inJobsScreen}) async {
    emit(SaveJobLoading());
    final res = await SaveRepo.deleteFromSavedItems(
      SaveModel(
        item: jobId,
        type: SavedItemsEnum.jobs,
      ),
    );
    if (res.data != null) {
      if (inJobsScreen) {
        isJobSavedInJobsScreen = false;
        emit(SaveJobSuccess());
        return;
      } else {
        isJobSaved = false;
        emit(SaveJobSuccess());
        return;
      }
    } else {
      emit(SaveJobFailed(msg: res.message ?? ""));
    }
  }

  // Future<void> addSavePost({String? postId}) async {
  //   emit(SavePostLoading());
  //   final res = await SaveRepo.addToSavedItems(
  //     SaveModel(
  //       item: postId,
  //       type: SavedItemsEnum.posts,
  //     ),
  //   );
  //   if (res.data != null) {
  //     emit(SavePostSuccess());
  //   } else {
  //     emit(SavePostFailed(msg: res.message ?? ""));
  //   }
  // }

  // Future<void> deleteSavePost({String? postId}) async {
  //   emit(SavePostLoading());
  //   final res = await SaveRepo.deleteFromSavedItems(
  //     SaveModel(
  //       item: postId,
  //       type: SavedItemsEnum.posts,
  //     ),
  //   );
  //   if (res.data != null) {
  //     emit(SavePostSuccess());
  //   } else {
  //     emit(SavePostFailed(msg: res.message ?? ""));
  //   }
  // }

  Future<void> deletePost({String? postId}) async {
    emit(DeletePostLoading());
    final res = await CommunityRepo.deletePost(postId: postId);
    if (res.data != null) {
      emit(DeletePostSuccess());
    } else {
      emit(DeletePostFailed(msg: res.message ?? ""));
    }
  }

  // ==== Report Methods ====
  Future<void> reportPost({required String postId}) async {
    emit(ReportPostLoading());
    final res = await ReportRepo.reportItem(
      contentId: postId,
      contentType: ReportType.post.type,
    );
    if (res.data != null) {
      emit(ReportPostSuccess(reportModel: res));
    } else {
      emit(ReportPostFailed(res.message ?? ""));
    }
  }

  Future<void> reportPostComment({required String commentId}) async {
    emit(ReportPostCommentLoading());
    final res = await ReportRepo.reportItem(
      contentId: commentId,
      contentType: ReportType.postcomment.type,
    );
    if (res.data != null) {
      emit(ReportPostCommentSuccess(reportModel: res));
    } else {
      emit(ReportPostCommentFailed(res.message ?? ""));
    }
  }

  // ==== General Tab Methods ====
  Future<void> getAllPosts() async {
    emit(GetAllPostsLoading());
    final res = await CommunityRepo.getAllPosts();
    if (res.data != null) {
      postsList = res.data;
      emit(GetAllPostsSuccess());
    } else {
      emit(GetAllPostsFailed(res.message ?? ""));
    }
  }

  void addToSelectedTagsList({
    required String tag,
    required String id,
  }) {
    emit(CommunityInitial());
    if (!selectedTagsList.contains(tag)) {
      selectedTagsList.add(tag);
      selectedTagsIdList.add(id);
    }
    emit(AddToSelectedTagsListState());
  }

  void emitFunction() {
    emit(CommunityTestInitial());
  }

  void removeFromSelectedTagsList({
    required String tag,
    required String id,
  }) {
    emit(CommunityInitial());
    if (selectedTagsList.contains(tag)) {
      selectedTagsList.remove(tag);
      selectedTagsIdList.remove(id);
    }
    emit(RemoveFromSelectedTagsListState());
  }

  Future<void> getTags() async {
    emit(TagLoading());
    final res = await CommunityRepo.getTags();
    if (res.data != null) {
      tagsList = res.data;
      emit(TagSuccess());
    } else {
      emit(TagFailed(res.message ?? ""));
    }
  }

  Future<void> addOrRemoveLikeOnPost({String? id}) async {
    emit(LikeOnPostLoading());
    final res = await CommunityRepo.addOrRemoveLikeOnPost(postId: id!);
    if (res.data != null) {
      emit(LikeOnPostDone());
    } else {
      emit(LikeOnPostFailed(res.message ?? ""));
    }
  }

  Future<void> addOrRemoveLikeOnComment({String? id}) async {
    emit(LikeOnCommentLoading());
    final res = await CommunityRepo.addOrRemoveLikeOnComment(id: id!);
    if (res.data != null) {
      emit(LikeOnCommentDone());
    } else {
      emit(LikeOnCommentFailed(res.message ?? ""));
    }
  }

  Future pickImageForPost() async {
    postImage = await PickerHelper.pickGalleryImage();
    emit(UploadPostImageLoading());
  }

  void setFilesToNull() {
    applyCubitVideo = null;
    applyCubitAudio = null;
    applyCubitText = null;
  }

  Future<void> addPost({
    String? postText,
  }) async {
    emit(CreatePostLoading());
    if (postImage != null) {
      final response = await GeneralRepo.uploadMedia([postImage!]);
      response.fold(
        (url) async {
          postImageUrl = url.first;
          emit(UploadPostImageSuccess());
        },
        (errorMsg) => emit(UploadPostImageFailed(errorMsg)),
      );
    }

    final res = await CommunityRepo.createPost(
      text: postText,
      media: postImageUrl == null ? null : [postImageUrl!],
      tags: selectedTagsIdList,
    );
    if (res.data != null) {
      emit(CreatePostDone(userPost: res.data!));
    } else {
      emit(CreatePostFailed(res.message ?? ""));
    }
  }

  Future<void> addComment({
    String? commentText,
    String? commentPostId,
    required File? commentImage,
    required String? commentImageUrl,
  }) async {
    emit(AddCommentLoading());
    if (commentImage != null) {
      final response = await GeneralRepo.uploadMedia([commentImage]);
      response.fold(
        (url) async {
          commentImageUrl = url.first;
          emit(UploadCommentImageSuccess());
        },
        (errorMsg) => emit(UploadCommentImageFailed(errorMsg)),
      );
    }
    final res = await CommunityRepo.addComment(CommentModel(
      comment: commentText,
      media: commentImageUrl,
      post: commentPostId,
    ));
    if (res.data != null) {
      emit(AddCommentDone(addComment: res.data!));
    } else {
      emit(AddCommentFailed(res.message ?? ""));
    }
  }

  Future<void> removeComment({
    String? commentId,
  }) async {
    emit(RemoveCommentLoading());
    commentID = commentId;
    final res = await CommunityRepo.removeComment(CommentModel(
      id: commentId,
    ));
    if (res.data != null) {
      emit(RemoveCommentDone(removeComment: res.data!));
    } else {
      emit(RemoveCommentFailed(res.message ?? ""));
    }
  }

  Future<void> getAllComments({required String id}) async {
    emit(CommentLoading());
    final res = await CommunityRepo.getAllComments(id: id);
    if (res.data != null) {
      comments = res.data;
      emit(CommentDone());
    } else {
      emit(CommentFailed(res.message ?? ""));
    }
  }

  Future<void> getUserDataById({required String userId}) async {
    emit(GetCommunityUserLoading());
    final res = await ProfileRepo.getUserDataById(userId: userId);
    if (res.data != null) {
      communityUser = res.data;
      emit(GetCommunityUserSuccess());
    } else {
      emit(GetCommunityUserFailed(res.message ?? ""));
    }
  }

  //==== Jobs Tab Methods ====
  Future<void> getAllJobs() async {
    emit(GetAllJobsLoading());
    final res = await CommunityRepo.getAllJobs();
    if (res.data != null) {
      allJobsList = res.data;
      emit(GetAllJobsSuccess());
    } else {
      emit(GetAllJobsFailed(res.message ?? ""));
    }
  }

  Future<void> getJobById({required String jobId}) async {
    emit(GetJobLoading());
    final res = await CommunityRepo.getJobById(jobId: jobId);
    if (res.data != null) {
      jobModel = res.data;
      isJobSaved = jobModel!.userSaved ?? false;
      emit(GetJobSuccess());
    } else {
      emit(GetJobFailed(res.message ?? ""));
    }
  }

  Future<bool> pickVideo() async {
    final video = await PickerHelper.pickVideoFile();
    if (video != null) {
      applyCubitVideo = video;
      emit(VideoPicked());
      return true;
    }
    emit(VideoPicked());
    return false;
  }

  Future<bool> pickAudio() async {
    final audio = await PickerHelper.pickAudioFile();
    if (audio != null) {
      applyCubitAudio = File(audio.path);
      emit(AudioPicked());
      return true;
    }
    emit(AudioPicked());
    return false;
  }

  Future<bool> pickText() async {
    final text = await PickerHelper.pickTextFile();
    if (text != null) {
      applyCubitText = File(text.path);
      emit(TextPicked());
      return true;
    }
    emit(TextPicked());
    return false;
  }

  Future<void> applyForJob({
    required String jobId,
    required String uploadType,
  }) async {
    emit(UploadVideoForJobLoading());
    Either<List<String>, String>? res;
    if (applyCubitVideo != null && uploadType == UploadType.video.type) {
      res = await GeneralRepo.uploadMedia([applyCubitVideo!]);
    } else if (applyCubitAudio != null && uploadType == UploadType.audio.type) {
      res = await GeneralRepo.uploadMedia([applyCubitAudio!]);
    } else if (applyCubitText != null && uploadType == UploadType.text.type) {
      res = await GeneralRepo.uploadMedia([applyCubitText!]);
    }
    if (applyCubitVideo != null || applyCubitAudio != null || applyCubitText != null) {
      res!.fold(
        (urls) async {
          emit(UploadVideoForJobSuccess());
          await applyForJobWithoutMedia(url: urls.first, jobId: jobId);
        },
        (errorMsg) {
          emit(UploadVideoForJobFailed(errorMsg));
        },
      );
    } else {
      await applyForJobWithoutMedia(jobId: jobId);
    }
  }

  Future<void> applyForJobWithoutMedia({String? url, required String jobId}) async {
    emit(ApplyForJobLoading());
    final response = await CommunityRepo.applyForJob(media: url, jobId: jobId);
    if (response.data != null) {
      emit(ApplyForJobSuccess());
    } else {
      emit(ApplyForJobFailed(response.message ?? ""));
    }
  }

  Future<void> getAllPartners() async {
    emit(GetAllPartnerLoading());
    final res = await CommunityRepo.getAllPartners();
    if (res.data != null) {
      partnerList = res.data;
      emit(GetAllPartnerSuccess());
    } else {
      emit(GetAllPartnerFailed(res.message ?? ""));
    }
  }
}
