part of 'connect_cubit.dart';

abstract class ConnectState extends Equatable {
  const ConnectState();

  @override
  List<Object> get props => [];
}

class CommunityInitial extends ConnectState {}

class ChangeIsSavedState extends ConnectState {}

class CommunityTestInitial extends ConnectState {}

class SaveJobSuccess extends ConnectState {}

class SaveJobLoading extends ConnectState {}

class SaveJobFailed extends ConnectState {
  final String msg;

  const SaveJobFailed({required this.msg});
}

class SavePostSuccess extends ConnectState {}

class SavePostLoading extends ConnectState {}

class SavePostFailed extends ConnectState {
  final String msg;

  const SavePostFailed({required this.msg});
}

class DeletePostSuccess extends ConnectState {}

class DeletePostLoading extends ConnectState {}

class DeletePostFailed extends ConnectState {
  final String msg;

  const DeletePostFailed({required this.msg});
}

//==== General States ====

class GetAllPostsSuccess extends ConnectState {}

class GetAllPostsLoading extends ConnectState {}

class GetAllPostsFailed extends ConnectState {
  final String msg;

  const GetAllPostsFailed(this.msg);
}

class ReportPostSuccess extends ConnectState {
  final BaseSingleResponse<ReportModel> reportModel;

  const ReportPostSuccess({required this.reportModel});
}

class ReportPostLoading extends ConnectState {}

class ReportPostFailed extends ConnectState {
  final String msg;

  const ReportPostFailed(this.msg);
}

class ReportPostCommentSuccess extends ConnectState {
  final BaseSingleResponse<ReportModel> reportModel;
  const ReportPostCommentSuccess({required this.reportModel});
}

class ReportPostCommentLoading extends ConnectState {}

class ReportPostCommentFailed extends ConnectState {
  final String msg;

  const ReportPostCommentFailed(this.msg);
}

class AddToSelectedTagsListState extends ConnectState {}

class RemoveFromSelectedTagsListState extends ConnectState {}

class TagSuccess extends ConnectState {}

class TagLoading extends ConnectState {}

class TagFailed extends ConnectState {
  final String msg;

  const TagFailed(this.msg);
}

class CreatePostDone extends ConnectState {
  final CreatePostModel userPost;

  const CreatePostDone({required this.userPost});
}

class CreatePostLoading extends ConnectState {}

class CreatePostFailed extends ConnectState {
  final String msg;

  const CreatePostFailed(this.msg);
}

class LikeOnPostDone extends ConnectState {}

class LikeOnPostLoading extends ConnectState {}

class LikeOnPostFailed extends ConnectState {
  final String msg;

  const LikeOnPostFailed(this.msg);
}

class LikeOnCommentDone extends ConnectState {}

class LikeOnCommentLoading extends ConnectState {}

class LikeOnCommentFailed extends ConnectState {
  final String msg;

  const LikeOnCommentFailed(this.msg);
}

class UploadCommentImageSuccess extends ConnectState {}

class UploadCommentImageLoading extends ConnectState {}

class UploadCommentImageFailed extends ConnectState {
  final String msg;

  const UploadCommentImageFailed(this.msg);
}

class UploadPostImageSuccess extends ConnectState {}

class UploadPostImageLoading extends ConnectState {}

class UploadPostImageFailed extends ConnectState {
  final String msg;

  const UploadPostImageFailed(this.msg);
}

class CommentDone extends ConnectState {}

class CommentLoading extends ConnectState {}

class CommentFailed extends ConnectState {
  final String msg;

  const CommentFailed(this.msg);
}

class AddCommentDone extends ConnectState {
  final CommentModel addComment;

  const AddCommentDone({required this.addComment});
}

class AddCommentLoading extends ConnectState {}

class AddCommentFailed extends ConnectState {
  final String msg;

  const AddCommentFailed(this.msg);
}

class RemoveCommentDone extends ConnectState {
  final CommentModel removeComment;

  const RemoveCommentDone({required this.removeComment});
}

class RemoveCommentLoading extends ConnectState {}

class RemoveCommentFailed extends ConnectState {
  final String msg;

  const RemoveCommentFailed(this.msg);
}
//==== Jobs States ====

class GetAllJobsSuccess extends ConnectState {}

class GetAllJobsLoading extends ConnectState {}

class GetAllJobsFailed extends ConnectState {
  final String msg;

  const GetAllJobsFailed(this.msg);
}

class GetJobSuccess extends ConnectState {}

class GetJobLoading extends ConnectState {}

class GetJobFailed extends ConnectState {
  final String msg;

  const GetJobFailed(this.msg);
}

class UploadVideoForJobSuccess extends ConnectState {}

class UploadVideoForJobLoading extends ConnectState {}

class UploadVideoForJobFailed extends ConnectState {
  final String msg;

  const UploadVideoForJobFailed(this.msg);
}

class ApplyForJobSuccess extends ConnectState {}

class ApplyForJobLoading extends ConnectState {}

class ApplyForJobFailed extends ConnectState {
  final String msg;

  const ApplyForJobFailed(this.msg);
}

class VideoPicked extends ConnectState {}

class AudioPicked extends ConnectState {}

class TextPicked extends ConnectState {}

class GetCommunityUserSuccess extends ConnectState {}

class GetCommunityUserLoading extends ConnectState {}

class GetCommunityUserFailed extends ConnectState {
  final String msg;

  const GetCommunityUserFailed(this.msg);
}

class GetAllPartnerSuccess extends ConnectState {}

class GetAllPartnerLoading extends ConnectState {}

class GetAllPartnerFailed extends ConnectState {
  final String msg;

  const GetAllPartnerFailed(this.msg);
}
