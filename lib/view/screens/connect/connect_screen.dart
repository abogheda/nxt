import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nxt/view/screens/connect/connect_screens/jobs_and_casting_calls/jobs_and_casting_calls_screen.dart';
import 'package:nxt/view/screens/connect/connect_screens/partners/partners_screen.dart';
import '../../../config/router/router.dart';
import '../../../utils/constants/app_const.dart';
import 'cubit/connect_cubit.dart';
import '../../../utils/constants/app_colors.dart';
import 'connect_screens/general/general_screen.dart';

class ConnectScreen extends StatefulWidget {
  const ConnectScreen({
    Key? key,
  }) : super(key: key);

  @override
  State<ConnectScreen> createState() => _ConnectScreenState();
}

class _ConnectScreenState extends State<ConnectScreen> with SingleTickerProviderStateMixin {
  late TabController _tabController;

  List<Widget> tabTitleList = [
    Tab(
        child: Text(
      'partners'.tr().toUpperCase(),
      // style: Theme.of(MagicRouter.currentContext!).textTheme.titleMedium!.copyWith(color: Colors.white),
    )),
    Tab(
        child: Text(
      'jobsCastingCalls'.tr().toUpperCase(),
      style: AppConst.isEn
          ? null
          : Theme.of(MagicRouter.currentContext!).textTheme.titleMedium!.copyWith(color: Colors.white),
      textAlign: TextAlign.center,
    )),
    Tab(
        child: Text(
      'community'.tr().toUpperCase(),
      // style: Theme.of(MagicRouter.currentContext!).textTheme.titleMedium!.copyWith(color: Colors.white),
    )),
  ];

  // List<Tab> tabTitleList = [
  //   Tab(child: Text('general'.tr().toUpperCase())),
  //   Tab(child: Text('experts'.tr().toUpperCase())),
  // ];
  List<Widget> tabViewList = [
    const PartnersScreen(),
    const JobsAndCastingCallsScreen(),
    const GeneralScreen(),
  ];
  // List<Widget> tabViewList = [
  //   const GeneralScreen(),
  //   const ExpertsAndJobsScreen(),
  // ];
  @override
  void initState() {
    _tabController = TabController(length: tabTitleList.length, vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          height: widget.height * 0.06,
          color: Colors.black,
          child: TabBar(
            physics: const NeverScrollableScrollPhysics(),
            controller: _tabController,
            indicator: const BoxDecoration(color: Colors.black),
            labelColor: Colors.white,
            labelStyle:
                Theme.of(context).textTheme.headline6!.copyWith(fontFamily: AppConst.isEn ? 'BebasNeue' : 'Tajawal'),
            unselectedLabelColor: AppColors.greyOutText,
            tabs: tabTitleList,
          ),
        ),
        Expanded(
          child: BlocProvider(
            create: (context) => ConnectCubit()
              ..getAllPosts()
              ..getAllPartners()
            // ..getAllJobs()
            ,
            child: TabBarView(
              physics: const NeverScrollableScrollPhysics(),
              controller: _tabController,
              children: tabViewList,
            ),
          ),
        )
      ],
    );
  }
}
