import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import '../../../utils/constants/app_const.dart';
import '../login/login_screen.dart';

import '../../../config/router/router.dart';
import '../../../utils/constants/app_colors.dart';
import '../../../utils/helpers/popup_helper.dart';
import '../../../utils/helpers/validators.dart';
import '../../widgets/app_loader.dart';
import '../../widgets/custom_text_field.dart';
import '../forgot_password/controller/forgot_password_cubit.dart';
import '../forgot_password/o_t_p_continue_button.dart';

class ResetScreen extends StatelessWidget {
  const ResetScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<ForgotPasswordCubit, ForgotPasswordState>(
      listener: (context, state) {
        if (state is ResetPassSuccess) {
          if (state.resetPassModel.password != null) {
            PopupHelper.showBasicSnack(msg: 'forgot.pass_reset'.tr(), color: Colors.green);
            MagicRouter.navigateAndPopAll(const LoginScreen());
          } else if (state.resetPassModel.statusCode != 200 ||
              state.resetPassModel.statusCode != 201 ||
              state.resetPassModel.message != null) {
            PopupHelper.showBasicSnack(msg: state.resetPassModel.message!.first.toString(), color: Colors.red);
          }
        } else if (state is ResetPassFailed) {
          PopupHelper.showBasicSnack(msg: "normalErrorMsg".tr(), color: Colors.red);
        }
      },
      builder: (context, state) {
        final cubit = ForgotPasswordCubit.get(context);
        return Scaffold(
          appBar: AppBar(
            elevation: 0.0,
            backgroundColor: Colors.transparent,
            foregroundColor: Colors.black,
          ),
          body: ListView(
            padding: EdgeInsets.only(left: width * 0.07, right: width * 0.07, top: height * 0.02),
            shrinkWrap: true,
            children: [
              Wrap(
                children: [
                  Text('forgot.enter_new'.tr(),
                      style: Theme.of(context)
                          .textTheme
                          .titleLarge!
                          .copyWith(fontFamily: isEn ? 'Montserrat' : 'Tajawal', fontWeight: FontWeight.bold))
                ],
              ),
              const SizedBox(height: 16),
              Form(
                key: cubit.resetFormKey,
                child: CustomTextField(
                  prefix: SvgPicture.asset(
                    'assets/images/lock.svg',
                    color: AppColors.iconsColor,
                    width: 24,
                  ),
                  hint: 'password'.tr(),
                  validator: Validators.password,
                  type: TextInputType.visiblePassword,
                  controller: cubit.passwordController,
                ),
              ),
              const SizedBox(height: 16),
              (state is ResetPassLoading)
                  ? const AppLoader()
                  : OTPContinueButton(
                      onPressed: () async {
                        if (!(await hasInternetConnection)) {
                          PopupHelper.showBasicSnack(msg: "internet_error".tr(), color: Colors.red);
                          return;
                        }
                        if (cubit.resetFormKey.currentState!.validate()) {
                          await cubit.resetPass();
                        }
                      },
                    ),
            ],
          ),
        );
      },
    );
  }
}
