import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:nxt/data/service/hive_services.dart';
import 'package:nxt/view/screens/choose_your_bundle/payment_details.dart';
import 'package:nxt/view/screens/complete_profile_screen/cubit/complete_profile_cubit.dart';

import '../../../data/models/choose_bundle/plan_model.dart';
import '../../../utils/constants/app_const.dart';
import '../../widgets/app_loader.dart';
import '../complete_profile_screen/base_complete_profile_screen.dart';
import '../complete_profile_screen/widgets/bundle_expanded_tile.dart';
import 'cubits/plan_cubit/plan_cubit.dart';
import 'cubits/select_plan_cubit/select_plan_cubit.dart';

class ChooseYourBundle extends StatefulWidget {
  final PlanModel? planModel;
  final bool fromHome;
  const ChooseYourBundle(
      {Key? key, required this.fromHome, this.planModel})
      : super(key: key);

  @override
  State<ChooseYourBundle> createState() => _ChooseYourBundleState();
}

class _ChooseYourBundleState extends State<ChooseYourBundle> {
  @override
  void initState() {
    HiveHelper.cacheFromHome(fromHome: widget.fromHome);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BaseCompleteProfileScreen(
      // showBackButton: true,
      showSkipButton: false,
      sizedBoxHeight: 0,
      child: BlocBuilder<CompleteProfileCubit, CompleteProfileState>(
        builder: (context, state) {
          // final cubit = CompleteProfileCubit.get(context);
          int verticalLength = HiveHelper.getCacheVerticals ?? 0;
          return BlocProvider(
            create: (context) => PlanCubit()..getPlans(model: widget.planModel),
            child: BlocBuilder<PlanCubit, PlanState>(
              builder: (context, state) {
                var planList = PlanCubit.get(context).planList;
                if (state is PlanSuccess) {
                  return BlocProvider(
                    create: (context) => SelectPlanCubit(),
                    child: BlocBuilder<SelectPlanCubit, SelectPlanState>(
                      builder: (context, selectPlanState) {
                        var selectPlan = SelectPlanCubit.get(context);
                        return ListView(
                          shrinkWrap: true,
                          children: [
                            Padding(
                              padding:
                                  EdgeInsets.only(bottom: widget.height * 0.06),
                              child: Text(
                                'chooseBundle'.tr(),
                                style: Theme.of(context)
                                    .textTheme
                                    .headlineMedium!
                                    .copyWith(
                                        color: Colors.black,
                                        fontFamily: AppConst.isEn
                                            ? 'BebasNeue'
                                            : 'Tajawal'),
                                textAlign: TextAlign.center,
                              ),
                            ),
                            Column(
                              children: List.generate(
                                planList?.length ?? 0,
                                (index) => InkWell(
                                  onTap: (){
                                    selectPlan.onUpdateData(planList![index].id ?? '',planList[index].price ?? 0,planList[index].title ?? '');
                                  },
                                  child: BundleExpandedTile(
                                    bundleName: planList![index].title ?? "",
                                    showBorder: selectPlan.state.id == planList[index].id ? true : false,
                                    price: planList[index].price ?? 0,
                                    features: planList[index].features,
                                  ),
                                ),
                              ),
                            ),
                            const SizedBox(
                              height: 15,
                            ),
                            SizedBox(
                              height: 350.h,
                              child: PaymentDetails(
                                bundleName: selectPlanState.title,
                                price: selectPlanState.price,
                                planId: selectPlanState.id,
                              ),
                            ),
                          ],
                        );
                      },
                    ),
                  );
                }
                return const AppLoader();
              },
            ),
          );
        },
      ),
    );
  }
}
