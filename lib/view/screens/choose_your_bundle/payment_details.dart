import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:nxt/config/router/router.dart';
import 'package:nxt/data/service/hive_services.dart';
import 'package:nxt/utils/constants/app_colors.dart';
import 'package:nxt/view/screens/choose_your_bundle/cubits/check_box_cubit/check_box_cubit.dart';
import 'package:nxt/view/screens/complete_profile_screen/cubit/complete_profile_cubit.dart';
import 'package:nxt/view/screens/more/cubit/more_cubit.dart';
import 'package:nxt/view/widgets/custom_button.dart';
import 'package:nxt/view/widgets/custom_text_field.dart';

import '../../../utils/constants/app_const.dart';
import '../../../utils/helpers/popup_helper.dart';
import '../choose_your_payment/choose_your_payment.dart';
import '../more/payment_terms_screen.dart';
import '../navigation_and_appbar/import_widget.dart';
import 'cubits/coupon_cubit/coupon_cubit.dart';

class PaymentDetails extends StatelessWidget {
  final String bundleName;
  final String planId;
  final num price;
  const PaymentDetails(
      {Key? key,
      required this.bundleName,
      required this.price,
      required this.planId})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CompleteProfileCubit, CompleteProfileState>(
      builder: (context, state) {
        final cubit = CompleteProfileCubit.get(context);
        return BlocProvider(
          create: (context) => CouponCubit(),
          child: BlocConsumer<CouponCubit, CouponState>(
            listener: (context, state) {
              if (state is CouponSuccess) {
                PopupHelper.showBasicSnack(
                    msg: "Coupon Added", color: Colors.green);
              }
              if (state is CouponFailed) {
                PopupHelper.showBasicSnack(
                    msg: "Something wrong please try again", color: Colors.red);
              }
            },
            builder: (context, state) {
              var couponCubit = CouponCubit.get(context);
              return ColoredBox(
                color: Colors.white,
                child: BlocProvider(
                  create: (context) => CheckboxCubit(),
                  child: BlocBuilder<CheckboxCubit, CheckboxState>(
                    builder: (context, checkState) {
                      return Column(
                        children: <Widget>[
                          Align(
                            alignment: Alignment.topLeft,
                            child: Padding(
                              padding: EdgeInsets.symmetric(
                                  horizontal: width * 0.06, vertical: 5.h),
                              child: RichText(
                                text: TextSpan(
                                  children: [
                                    TextSpan(
                                      text: bundleName,
                                      style: Theme.of(context)
                                          .textTheme
                                          .headline4!
                                          .copyWith(
                                              color: Colors.black,
                                              fontFamily: AppConst.isEn
                                                  ? 'BebasNeue-Book'
                                                  : 'Tajawal'),
                                    ),
                                    TextSpan(
                                      text: price.toString(),
                                      style: Theme.of(context)
                                          .textTheme
                                          .headline4!
                                          .copyWith(
                                              color: Colors.black,
                                              fontFamily: AppConst.isEn
                                                  ? 'BebasNeue'
                                                  : 'Tajawal',
                                              decoration:
                                                  TextDecoration.lineThrough),
                                    ),
                                    WidgetSpan(
                                      baseline: TextBaseline.ideographic,
                                      alignment:
                                          PlaceholderAlignment.aboveBaseline,
                                      child: Text(
                                        'EGP',
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyText1!
                                            .copyWith(
                                              color: Colors.black,
                                              fontWeight: FontWeight.bold,
                                              fontFamily: AppConst.isEn
                                                  ? 'BebasNeue-Book'
                                                  : 'Tajawal',
                                            ),
                                      ),
                                    ),
                                    if (state is CouponSuccess)
                                      TextSpan(
                                        text: couponCubit.newPrice.toString(),
                                        style: Theme.of(context)
                                            .textTheme
                                            .headline4!
                                            .copyWith(
                                              color: Colors.green,
                                              fontFamily: AppConst.isEn
                                                  ? 'BebasNeue'
                                                  : 'Tajawal',
                                            ),
                                      ),
                                    // if (state is CouponSuccess)
                                    //   WidgetSpan(
                                    //     baseline: TextBaseline.ideographic,
                                    //     alignment: PlaceholderAlignment
                                    //         .aboveBaseline,
                                    //     child: Text(
                                    //       'EGP',
                                    //       style: Theme.of(context)
                                    //           .textTheme
                                    //           .bodyText1!
                                    //           .copyWith(
                                    //             color: Colors.black,
                                    //             fontWeight: FontWeight.bold,
                                    //             fontFamily: AppConst.isEn
                                    //                 ? 'BebasNeue-Book'
                                    //                 : 'Tajawal',
                                    //           ),
                                    //     ),
                                    //   ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              SizedBox(
                                width: width * 0.67,
                                height: height * 0.05,
                                child: CustomTextField(
                                  hint: "Coupon",
                                  controller: couponCubit.couponController,
                                ),
                              ),
                              SizedBox(width: 10.w),
                              CustomButton(
                                onTap: () {
                                  couponCubit.getCoupon(
                                      planId: planId, price: price);
                                },
                                child: const Text("APPLY"),
                              )
                            ],
                          ),
                          Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal: width * 0.03),
                            child: Row(
                              children: [
                                Checkbox(
                                    value: checkState.isChecked,
                                    onChanged: (value) {
                                      context
                                          .read<CheckboxCubit>()
                                          .changeValue(value!);
                                    }),
                                SizedBox(width: 1.w),
                                Text(
                                  "iAgreeTo".tr(),
                                  style: TextStyle(
                                      color: AppColors.greyTxtColor,
                                      fontSize: 12.sp,
                                      fontFamily: AppConst.isEn
                                          ? "Montserrat"
                                          : "Tajawal"),
                                ),
                                SizedBox(width: 1.w),
                                TextButton(
                                    onPressed: () => MagicRouter.navigateTo(
                                        BlocProvider.value(
                                            value: MoreCubit()..getLegal(),
                                            child: const PaymentTermsScreen(
                                              showActions: false,
                                              isLogoClickable: false,
                                            ))),
                                    child: Text(
                                      "paymentTermsButton".tr(),
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 12.sp,
                                          decoration:
                                              TextDecoration.underline,
                                          fontWeight: FontWeight.w700,
                                          fontFamily: AppConst.isEn
                                              ? "Montserrat"
                                              : "Tajawal"),
                                    )),
                              ],
                            ),
                          ),
                          CustomButton(
                            width: width * 0.87,
                            child: Text("select".tr()),
                            onTap: () {
                              if (checkState.isChecked == true) {
                                if (state is CouponSuccess) {
                                  MagicRouter.navigateTo(BlocProvider.value(
                                    value: cubit,
                                    child: ChooseYourPayment(
                                      price: couponCubit.newPrice ?? 0,
                                      planId: planId,
                                    ),
                                  ));
                                  cubit.increaseCompleteProfileIndex();
                                } else {
                                  MagicRouter.navigateTo(BlocProvider.value(
                                    value: cubit,
                                    child: ChooseYourPayment(
                                      price: price,
                                      planId: planId,
                                    ),
                                  ));
                                  cubit.increaseCompleteProfileIndex();
                                }
                              } else {
                                PopupHelper.showBasicSnack(
                                    msg: 'makeSureAll'.tr(),
                                    color: Colors.red);
                              }
                            },
                          ),
                          HiveHelper.fromHome == false ? TextButton(
                            onPressed: () async{
                             await cubit.skipProfile();
                             MagicRouter.navigateAndPopAll(Navigation());
                            },
                            child: Text(
                              "payLater".tr(),
                              style: TextStyle(
                                  fontSize: 13.sp,
                                  color: Colors.black,
                                  fontWeight: FontWeight.w800,
                                  decoration: TextDecoration.underline,
                                  fontFamily: AppConst.isEn
                                      ? "Montserrat"
                                      : "Tajawal"),
                            ),
                          ):Container(),
                        ],
                      );
                    },
                  ),
                ),
              );
            },
          ),
        );
      },
    );
  }
}
