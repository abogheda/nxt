import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nxt/data/repos/bundles/bundle_repo.dart';

import '../../../../../data/models/choose_bundle/plan_model.dart';

part 'plan_state.dart';

class PlanCubit extends Cubit<PlanState> {
  PlanCubit() : super(PlanInitial());
  static PlanCubit get(context) => BlocProvider.of(context);
  List<PlanModel>? planList;
  Future<void> getPlans({PlanModel? model}) async {
    emit(PlanLoading());
    final res = await BundleRepo.plansList();
    if (res.data != null) {
      if(model?.id != ''){
        print("model done : $model");

        planList = res.data;
        planList?.add(model!);
      }else{
        planList = res.data;
      }

      emit(PlanSuccess());
    } else {
      emit(PlanFailed());
    }
  }
}
