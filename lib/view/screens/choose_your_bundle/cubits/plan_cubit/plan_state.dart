part of 'plan_cubit.dart';

abstract class PlanState extends Equatable {
  const PlanState();

  @override
  List<Object> get props => [];
}

class PlanInitial extends PlanState {}
class PlanLoading extends PlanState {}
class PlanSuccess extends PlanState {}
class PlanFailed extends PlanState {}
