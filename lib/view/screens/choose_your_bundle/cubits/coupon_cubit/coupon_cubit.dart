import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../../data/models/choose_bundle/coupon_model.dart';
import '../../../../../data/repos/bundles/bundle_repo.dart';
import '../../../../../utils/helpers/popup_helper.dart';

part 'coupon_state.dart';

class CouponCubit extends Cubit<CouponState> {
  CouponCubit() : super(CouponInitial());
  static CouponCubit get(context) => BlocProvider.of(context);

  @override
  Future<void> close() {
    couponController.dispose();
    return super.close();
  }
  CouponModel? couponModel;
  TextEditingController couponController = TextEditingController();
  num? price;
  num? newPrice;
  Future<void> getCoupon({required String planId,required price}) async{
    if(couponController.text.isNotEmpty) {
      emit(CouponLoading());
      final res = await BundleRepo.getCoupon(
          planId: planId, code: couponController.text);
      if (res.statusCode != 400 && res.data?.limit != 0) {
        couponModel = res.data;
        int discount = res.data!.discount ?? 0;
        num discountPercentage = discount / 100;
        newPrice = price - (price * discountPercentage);
        couponController.clear();
        emit(CouponSuccess());
      } else {
        emit(CouponFailed());
      }
    }else{
      PopupHelper.showBasicSnack(msg: "Please,add the code", color: Colors.red);
    }
  }
}
