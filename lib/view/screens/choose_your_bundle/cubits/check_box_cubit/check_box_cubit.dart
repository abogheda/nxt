import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../../../data/repos/auth/auth_repo.dart';
part 'check_box_state.dart';

class CheckboxCubit extends Cubit<CheckboxState> {
  CheckboxCubit() : super(CheckboxState(isChecked: false));

  void changeValue(bool value) async{
    final res = await AuthRepo.confirmTerms();
    if (res.data != null) {
    emit(state.copyWith(changeState: value));
  }
  }
}