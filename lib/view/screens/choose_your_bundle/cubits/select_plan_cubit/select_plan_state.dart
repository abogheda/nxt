part of 'select_plan_cubit.dart';

abstract class SelectPlanState extends Equatable{
  final String id;
  final num price;
  final String title;
  final bool changed;

  const SelectPlanState(this.id, this.changed, this.price, this.title);
}

class SelectPlanInitial extends SelectPlanState {
  const SelectPlanInitial(super.id, super.changed, super.price, super.title);


  @override
  // TODO: implement props
  List<Object?> get props => [changed];
}
class SelectPlanUpdated extends SelectPlanState {
  const SelectPlanUpdated(super.id, super.changed, super.price, super.title);


  @override
  // TODO: implement props
  List<Object?> get props => [changed];
}
