import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'select_plan_state.dart';

class SelectPlanCubit extends Cubit<SelectPlanState> {
  SelectPlanCubit() : super(const SelectPlanInitial("0",false,0,''));
  static SelectPlanCubit get(context) => BlocProvider.of(context);

  onUpdateData(String id,num price,String title){
    emit(SelectPlanUpdated(id, !state.changed,price,title,));
  }
}
