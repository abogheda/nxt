// ignore_for_file: depend_on_referenced_packages

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nxt/utils/helpers/popup_helper.dart';
import 'package:nxt/view/screens/challenges/screens/challenge_video/view_submission_video_player.dart';
import 'package:nxt/view/screens/job_applicants/applicant_expanded_tile.dart';
import 'package:nxt/view/screens/job_applicants/colored_button.dart';
import 'package:nxt/view/screens/job_applicants/expanded_button_tile.dart';
import 'package:nxt/view/screens/talent_profile/screens/reels/reel_widgets/reels_thumbnail.dart';
import 'package:timeago/timeago.dart' as timeago;
import 'package:transparent_image/transparent_image.dart';

import '../../../config/router/router.dart';
import '../../../data/enum/applicant_status_enum.dart';
import '../../../data/models/jobs/applicants_model.dart';
import '../../../utils/constants/app_colors.dart';
import '../../../utils/constants/app_const.dart';
import '../../../utils/constants/app_images.dart';
import '../../widgets/shimmer_widgets/custom_shimmer.dart';
import '../community_user_profile/community_user_profile.dart';
import '../navigation_and_appbar/import_widget.dart';
import '../organization_connect/controller/organization_connect_cubit.dart';

class ViewJobApplicantScreen extends StatelessWidget {
  const ViewJobApplicantScreen({
    Key? key,
    required this.applicant,
    required this.jobId,
  }) : super(key: key);
  final ApplicantsModel applicant;
  final String jobId;

  void navigateToProfile() => MagicRouter.navigateTo(CommunityUserProfile(userId: applicant.user!.id!));

  @override
  Widget build(BuildContext context) {
    return CustomScaffoldWidget(
      isInProfile: true,
      extendBodyBehindAppBar: false,
      isBackArrow: true,
      isThereLogo: true,
      isThereActions: true,
      backgroundColor: Colors.white,
      body: BlocConsumer<OrganizationConnectCubit, OrganizationConnectState>(
        listener: (context, state) {
          if (state is UpdateJobApplicationFailed) {
            PopupHelper.showBasicSnack(msg: state.msg, color: Colors.red);
          }
        },
        builder: (context, state) {
          final cubit = OrganizationConnectCubit.get(context);
          bool isApproved = applicant.status == ApplicantStatusEnum.approved.type;
          Future<void> updateStatus({required String status}) async {
            await cubit.updateJobApplicationStatus(applicantId: applicant.id!, status: status);
            cubit.changeButtonIndex(index: 0);
            await cubit.getApplicants(jobId: jobId).then((_) => MagicRouter.pop());
          }

          return ListView(
            children: [
              Container(height: 1.5, color: Colors.black, width: width, margin: EdgeInsets.only(bottom: height * 0.01)),
              Card(
                elevation: 2.5,
                margin: EdgeInsets.symmetric(horizontal: width * 0.03, vertical: height * 0.01),
                shape: RoundedRectangleBorder(
                    side: const BorderSide(color: Color(0xFF707070), width: 0.2),
                    borderRadius: BorderRadius.circular(6)),
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: width * 0.03, vertical: height * 0.01),
                  child: Column(
                    children: <Widget>[
                      InkWell(
                        onTap: navigateToProfile,
                        splashColor: Colors.black12,
                        child: Row(
                          children: <Widget>[
                            Container(
                              height: width * 0.13,
                              width: width * 0.13,
                              decoration: BoxDecoration(
                                color: AppColors.greyOutText,
                                image: DecorationImage(
                                    fit: BoxFit.cover,
                                    image: applicant.user!.avatar != null && applicant.user!.avatar != ''
                                        ? FadeInImage.memoryNetwork(
                                                image: applicant.user!.avatar ?? userAvatarPlaceHolderUrl,
                                                placeholder: kTransparentImage)
                                            .image
                                        : Image.asset('assets/images/profile_icon.png').image),
                              ),
                            ),
                            Padding(
                              padding: EdgeInsetsDirectional.only(start: width * 0.03),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Padding(
                                      padding: EdgeInsets.symmetric(vertical: height * 0.005),
                                      child: Text(applicant.user!.name ?? "notAvailable".tr(),
                                          style: Theme.of(context).textTheme.headline6)),
                                  Text(
                                    timeago.format(DateTime.parse(applicant.createdAt ?? DateTime.now().toString()),
                                        locale: isEn ? 'en_short' : 'ar_short'),
                                    style: Theme.of(context)
                                        .textTheme
                                        .bodyText1!
                                        .copyWith(fontFamily: isEn ? 'Montserrat' : 'Tajawal'),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(height: height * 0.015),
                      SizedBox(
                        width: width,
                        height: height * 0.5,
                        child: ReelsThumbnail(
                          reelThumbnail: applicant.media,
                          isAudio: applicant.media!.endsWith('.mp3') ||
                              applicant.media!.endsWith('.wav') ||
                              applicant.media!.endsWith('.aac'),
                          onPressed: null,
                          widgetOnThumbnail: IconButton(
                            onPressed: () =>
                                MagicRouter.navigateTo(ViewSubmissionVideoPlayer(videoUrl: applicant.media!)),
                            icon: Icon(Icons.play_circle, color: Colors.white, size: width * 0.15),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              state is UpdateJobApplicationLoading || state is GetAllApplicantsLoading
                  ? CustomShimmer(
                      highlightColor: Colors.black26,
                      baseColor: Colors.black54,
                      child: Padding(
                        padding: EdgeInsets.symmetric(horizontal: width * 0.03),
                        child: ElevatedButton(
                          onPressed: null,
                          style: ElevatedButton.styleFrom(
                              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6))),
                          child: const SizedBox(),
                        ),
                      ),
                    )
                  : Padding(
                      padding: EdgeInsets.symmetric(horizontal: width * 0.03),
                      child: (applicant.status == ApplicantStatusEnum.pending.type)
                          ? Row(
                              children: [
                                ColoredButton(
                                  text: 'reject'.tr(),
                                  color: AppColors.rejectRedColor,
                                  onPressed: () async => await updateStatus(status: ApplicantStatusEnum.rejected.type),
                                ),
                                SizedBox(width: width * 0.035),
                                ColoredButton(
                                  text: 'accept'.tr(),
                                  color: AppColors.agreeGreenColor,
                                  onPressed: () async => await updateStatus(status: ApplicantStatusEnum.approved.type),
                                ),
                              ],
                            )
                          : ApplicantExpandedTile(
                              title: applicant.status!.toLowerCase().tr().toUpperCase(),
                              headerColor: isApproved ? AppColors.agreeGreenColor : AppColors.rejectRedColor,
                              contentList: [
                                ExpandedButtonTile(
                                  onPressed: isApproved
                                      ? () async => await updateStatus(status: ApplicantStatusEnum.rejected.type)
                                      : () async => await updateStatus(status: ApplicantStatusEnum.approved.type),
                                  color: isApproved ? AppColors.rejectRedColor : AppColors.agreeGreenColor,
                                  text: isApproved ? 'reject'.tr().toUpperCase() : 'accept'.tr().toUpperCase(),
                                ),
                                ExpandedButtonTile(
                                  onPressed: () async => await updateStatus(status: ApplicantStatusEnum.pending.type),
                                  color: Colors.black,
                                  text: 'pending'.tr().toUpperCase(),
                                ),
                              ],
                            ),
                    ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: width * 0.03),
                child: OutlinedButton(
                  onPressed: navigateToProfile,
                  style: OutlinedButton.styleFrom(
                      side: const BorderSide(color: Colors.black),
                      padding: EdgeInsets.symmetric(vertical: height * 0.01),
                      textStyle: Theme.of(context)
                          .textTheme
                          .titleMedium!
                          .copyWith(fontFamily: isEn ? 'Montserrat' : 'Tajawal', fontWeight: FontWeight.bold)),
                  child: Text('viewProfile'.tr().toUpperCase()),
                ),
              )
            ],
          );
        },
      ),
    );
  }
}
