// ignore_for_file: deprecated_member_use

import 'package:flutter/material.dart';
import '../../../utils/constants/app_const.dart';

class ColoredButton extends StatelessWidget {
  const ColoredButton({Key? key, this.onPressed, required this.text, required this.color}) : super(key: key);
  final Function()? onPressed;
  final String text;
  final Color color;
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: ElevatedButton(
          onPressed: onPressed,
          style: ElevatedButton.styleFrom(
              primary: color,
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
              textStyle: Theme.of(context).textTheme.titleSmall!.copyWith(
                  color: Colors.white, fontFamily: isEn ? 'Montserrat' : 'Tajawal', fontWeight: FontWeight.bold)),
          child: FittedBox(child: Text(text.toUpperCase()))),
    );
  }
}
