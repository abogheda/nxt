import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import '../../../data/enum/applicant_status_enum.dart';
import '../../../utils/constants/resources.dart';
import 'applicants_button.dart';

import '../organization_connect/controller/organization_connect_cubit.dart';

class FilterApplicantsButtonsRow extends StatelessWidget {
  const FilterApplicantsButtonsRow({Key? key, required this.jobId}) : super(key: key);

  final String jobId;

  @override
  Widget build(BuildContext context) {
    final cubit = OrganizationConnectCubit.get(context);
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: width * 0.03),
      child: Row(
        children: <Widget>[
          ApplicantsButton(
            flex: 2,
            text: 'all'.tr(),
            isSelected: cubit.selectedButtonIndex == 0,
            onPressed: () async {
              cubit.changeButtonIndex(index: 0);
              await cubit.getApplicants(jobId: jobId);
            },
          ),
          SizedBox(width: width * 0.01),
          ApplicantsButton(
            text: 'approved'.tr(),
            isSelected: cubit.selectedButtonIndex == 1,
            onPressed: () async {
              cubit.changeButtonIndex(index: 1);
              await cubit.getApplicants(jobId: jobId, status: ApplicantStatusEnum.approved.type);
            },
          ),
          SizedBox(width: width * 0.01),
          ApplicantsButton(
            text: 'rejected'.tr(),
            isSelected: cubit.selectedButtonIndex == 2,
            onPressed: () async {
              cubit.changeButtonIndex(index: 2);
              await cubit.getApplicants(jobId: jobId, status: ApplicantStatusEnum.rejected.type);
            },
          ),
          SizedBox(width: width * 0.01),
          ApplicantsButton(
            text: 'pending'.tr(),
            isSelected: cubit.selectedButtonIndex == 3,
            onPressed: () async {
              cubit.changeButtonIndex(index: 3);
              await cubit.getApplicants(jobId: jobId, status: ApplicantStatusEnum.pending.type);
            },
          ),
          SizedBox(width: width * 0.01),
        ],
      ),
    );
  }
}
