// ignore_for_file: depend_on_referenced_packages

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:nxt/utils/constants/resources.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:timeago/timeago.dart' as timeago;

import '../../../data/models/jobs/applicants_model.dart';

class ApplicantUserCard extends StatelessWidget {
  const ApplicantUserCard({
    Key? key,
    required this.applicant,
    required this.jobId,
    this.onTap,
  }) : super(key: key);
  final ApplicantsModel applicant;
  final String jobId;
  final Function()? onTap;
  Color getBadgeColor() {
    final badge = applicant.status!.toLowerCase().trim().tr();
    if (badge == 'approved'.tr()) {
      return AppColors.agreeGreenColor;
    } else if (badge == 'rejected'.tr()) {
      return AppColors.rejectRedColor;
    } else if (badge == 'pending'.tr()) {
      return Colors.black;
    } else {
      return Colors.black;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: height * 0.01),
      child: InkWell(
        splashColor: Colors.black12,
        borderRadius: BorderRadius.circular(6),
        onTap: onTap,
        child: Stack(
          children: [
            Container(
              decoration: BoxDecoration(
                  border: Border.all(color: const Color(0xFF707070), width: 0.2),
                  borderRadius: BorderRadius.circular(6)),
              child: Padding(
                padding: EdgeInsets.symmetric(vertical: height * 0.025, horizontal: height * 0.02),
                child: Row(
                  children: <Widget>[
                    Container(
                      height: width * 0.13,
                      width: width * 0.13,
                      decoration: BoxDecoration(
                        color: AppColors.greyOutText,
                        image: DecorationImage(
                            fit: BoxFit.cover,
                            image: applicant.user!.avatar != null && applicant.user!.avatar != ''
                                ? FadeInImage.memoryNetwork(
                                        image: applicant.user!.avatar ?? userAvatarPlaceHolderUrl,
                                        placeholder: kTransparentImage)
                                    .image
                                : Image.asset('assets/images/profile_icon.png').image),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsetsDirectional.only(start: width * 0.03),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                              padding: EdgeInsets.symmetric(vertical: height * 0.005),
                              child: Text(applicant.user!.name ?? "notAvailable".tr(),
                                  style: Theme.of(context).textTheme.headline6)),
                          Text(
                            timeago.format(DateTime.parse(applicant.createdAt ?? DateTime.now().toString()),
                                locale: isEn ? 'en_short' : 'ar_short'),
                            style: Theme.of(context)
                                .textTheme
                                .bodyText1!
                                .copyWith(fontFamily: isEn ? 'Montserrat' : 'Tajawal'),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Positioned(
              top: height * 0.02,
              right: isEn ? 0 : null,
              left: isEn ? null : 0,
              child: Container(
                padding: EdgeInsets.symmetric(vertical: height * 0.01, horizontal: width * 0.03),
                decoration: BoxDecoration(
                    color: getBadgeColor(),
                    borderRadius: BorderRadius.only(
                        topLeft: isEn ? const Radius.circular(50) : Radius.zero,
                        bottomLeft: isEn ? const Radius.circular(50) : Radius.zero,
                        bottomRight: isEn ? Radius.zero : const Radius.circular(50),
                        topRight: isEn ? Radius.zero : const Radius.circular(50))),
                child: FittedBox(
                  child: Text(
                    applicant.status != null ? applicant.status!.toLowerCase().trim().tr() : "notAvailable".tr(),
                    style: Theme.of(context).textTheme.caption!.copyWith(
                          color: Colors.white,
                          fontFamily: isEn ? 'Montserrat' : 'Tajawal',
                        ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
