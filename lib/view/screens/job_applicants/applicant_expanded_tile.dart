import 'package:flutter/material.dart';
import 'package:flutter_expanded_tile/flutter_expanded_tile.dart';
import '../../../utils/constants/app_const.dart';

class ApplicantExpandedTile extends StatefulWidget {
  const ApplicantExpandedTile({Key? key, required this.title, required this.contentList, required this.headerColor})
      : super(key: key);
  final String title;
  final List<Widget> contentList;
  final Color headerColor;
  @override
  State<ApplicantExpandedTile> createState() => _ApplicantExpandedTileState();
}

class _ApplicantExpandedTileState extends State<ApplicantExpandedTile> {
  late ExpandedTileController _controller;
  @override
  void initState() {
    _controller = ExpandedTileController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ExpandedTile(
      contentseparator: 0,
      controller: _controller,
      theme: ExpandedTileThemeData(
        headerRadius: 4,
        contentRadius: 0,
        headerSplashColor: Colors.grey,
        contentPadding: EdgeInsets.zero,
        headerColor: widget.headerColor,
        contentBackgroundColor: Colors.white,
        headerPadding: EdgeInsets.symmetric(vertical: 6, horizontal: widget.width * 0.01),
      ),
      title: Text(
        widget.title,
        style: Theme.of(context).textTheme.titleMedium!.copyWith(
              color: Colors.white,
              fontFamily: widget.isEn ? 'Montserrat' : 'Tajawal',
            ),
      ),
      content: Column(
          mainAxisSize: MainAxisSize.min, mainAxisAlignment: MainAxisAlignment.start, children: widget.contentList),
      trailing: const Icon(Icons.arrow_forward_ios_rounded, color: Colors.white, size: 16),
    );
  }
}
