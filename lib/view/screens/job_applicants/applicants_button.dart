// ignore_for_file: deprecated_member_use

import 'package:flutter/material.dart';
import '../../../utils/constants/resources.dart';

class ApplicantsButton extends StatelessWidget {
  const ApplicantsButton({Key? key, required this.isSelected, this.flex, this.onPressed, required this.text})
      : super(key: key);
  final bool isSelected;
  final int? flex;
  final Function()? onPressed;
  final String text;
  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: flex ?? 3,
      child: ElevatedButton(
          onPressed: onPressed,
          style: ElevatedButton.styleFrom(
              elevation: isSelected ? null : 0,
              shadowColor: isSelected ? null : Colors.black12,
              primary: isSelected ? null : Colors.transparent,
              shape: RoundedRectangleBorder(
                side: const BorderSide(color: Colors.black),
                borderRadius: BorderRadius.circular(6),
              ),
              textStyle: Theme.of(context).textTheme.titleSmall!.copyWith(
                  color: Colors.white, fontFamily: isEn ? 'Montserrat' : 'Tajawal', fontWeight: FontWeight.bold)),
          child: FittedBox(child: Text(text, style: isSelected ? null : const TextStyle(color: Colors.black)))),
    );
  }
}
