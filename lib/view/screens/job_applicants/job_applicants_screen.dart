import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../data/models/jobs/jobs_model.dart';
import '../../../utils/constants/resources.dart';
import 'applicant_user_card.dart';
import 'filter_applicants_buttons_row.dart';
import 'view_job_applicant_screen.dart';
import '../my_jobs/widgets/my_job_card.dart';

import '../../../config/router/router.dart';
import '../../../utils/helpers/popup_helper.dart';
import '../../widgets/no_state_widget.dart';
import '../../widgets/shimmer_widgets/user_result_card_shimmer.dart';
import '../navigation_and_appbar/import_widget.dart';
import '../organization_connect/controller/organization_connect_cubit.dart';

class JobApplicantsScreen extends StatelessWidget {
  const JobApplicantsScreen({Key? key, required this.job}) : super(key: key);
  final JobsModel job;
  String getName() {
    String name = '';
    if (job.organization != null) {
      name = job.organization!.name!;
    } else {
      name = job.expert!.expert!.name!;
    }
    return name;
  }

  String getLogo() {
    String logo = '';
    if (job.organization != null) {
      logo = job.organization!.logo!;
    } else {
      logo = job.expert!.expert!.logo!;
    }
    return logo;
  }

  @override
  Widget build(BuildContext context) {
    return CustomScaffoldWidget(
      isInProfile: true,
      extendBodyBehindAppBar: false,
      isBackArrow: true,
      isThereLogo: true,
      isThereActions: true,
      backgroundColor: Colors.white,
      body: BlocConsumer<OrganizationConnectCubit, OrganizationConnectState>(
        listener: (context, state) {
          if (state is GetAllApplicantsFailed) {
            PopupHelper.showBasicSnack(msg: state.msg, color: Colors.red);
          }
        },
        builder: (context, state) {
          final cubit = OrganizationConnectCubit.get(context);
          return RefreshIndicator(
            onRefresh: () async {
              cubit.changeButtonIndex(index: 0);
              await cubit.getApplicants(jobId: job.id!);
            },
            child: ListView(
              children: [
                Container(
                    color: Colors.black, height: 1.5, width: width, margin: EdgeInsets.only(bottom: height * 0.01)),
                Text('jobDetails'.tr(),
                    textAlign: TextAlign.center,
                    style: Theme.of(context)
                        .textTheme
                        .headlineSmall!
                        .copyWith(fontFamily: isEn ? 'BebasNeue' : 'Tajawal')),
                SizedBox(height: height * 0.02),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: width * 0.03),
                  child: MyJobCard(
                    id: job.id!,
                    title: job.title,
                    createdAt: job.createdAt,
                    color: job.category!.color,
                    name: getName(),
                    logo: getLogo(),
                  ),
                ),
                SizedBox(height: height * 0.015),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: width * 0.03),
                  child: Text('applicants'.tr(),
                      style: Theme.of(context)
                          .textTheme
                          .headlineSmall!
                          .copyWith(fontFamily: isEn ? 'BebasNeue' : 'Tajawal')),
                ),
                SizedBox(height: height * 0.01),
                FilterApplicantsButtonsRow(jobId: job.id!),
                state is GetAllApplicantsLoading
                    ? const UserResultShimmerListView()
                    : cubit.applicantsModel!.isEmpty
                        ? NoStateWidget(height: height * 0.2)
                        : ListView.builder(
                            shrinkWrap: true,
                            physics: const NeverScrollableScrollPhysics(),
                            itemCount: cubit.applicantsModel!.length,
                            padding: EdgeInsets.symmetric(horizontal: width * 0.03),
                            itemBuilder: (context, index) {
                              final applicant = cubit.applicantsModel![index];
                              if (cubit.applicantsModel!.isEmpty) return NoStateWidget();
                              return ApplicantUserCard(
                                applicant: applicant,
                                jobId: job.id!,
                                onTap: () => MagicRouter.navigateTo(BlocProvider.value(
                                    value: cubit, child: ViewJobApplicantScreen(applicant: applicant, jobId: job.id!))),
                              );
                            },
                          ),
              ],
            ),
          );
        },
      ),
    );
  }
}
