import 'package:flutter/material.dart';

import '../../../utils/constants/app_const.dart';

class ExpandedButtonTile extends StatelessWidget {
  const ExpandedButtonTile({
    Key? key,
    required this.text,
    this.onPressed,
    required this.color,
  }) : super(key: key);
  final String text;
  final Function()? onPressed;
  final Color color;
  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: Container(
        width: width,
        decoration: BoxDecoration(border: Border.all(color: const Color(0xFFEDEDED))),
        child: InkWell(
          onTap: onPressed,
          splashColor: Colors.black38,
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: height * 0.015, horizontal: width * 0.04),
            child: Text(
              text,
              textAlign: TextAlign.start,
              style: Theme.of(context).textTheme.bodyText1!.copyWith(
                    fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal',
                    fontWeight: FontWeight.bold,
                    color: color,
                  ),
            ),
          ),
        ),
      ),
    );
  }
}
