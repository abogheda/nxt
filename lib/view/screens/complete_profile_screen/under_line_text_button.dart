import 'package:flutter/material.dart';
import '../../../utils/constants/resources.dart';

class UnderLineTextButton extends StatelessWidget {
  const UnderLineTextButton({Key? key, this.onPressed, required this.text, required this.offstage}) : super(key: key);

  final bool offstage;
  final void Function()? onPressed;
  final String text;
  @override
  Widget build(BuildContext context) {
    return Offstage(
      offstage: offstage,
      child: Padding(
        padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top + height * 0.02),
        child: TextButton(
          onPressed: onPressed,
          child: Text(
            text.toUpperCase(),
            style: Theme.of(context).textTheme.bodyText1!.copyWith(
                  fontWeight: FontWeight.bold,
                  fontFamily: isEn ? 'Montserrat' : 'Tajawal',
                  decoration: TextDecoration.underline,
                ),
          ),
        ),
      ),
    );
  }
}
