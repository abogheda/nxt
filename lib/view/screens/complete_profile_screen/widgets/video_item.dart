part of 'imports.dart';

class VideoItem extends StatefulWidget {
  final File videoPath;
  final bool showDeleteIcon;
  final Function()? onDeletePressed;
  const VideoItem({
    Key? key,
    required this.videoPath,
    this.showDeleteIcon = false,
    this.onDeletePressed,
  }) : super(key: key);

  @override
  State<VideoItem> createState() => _VideoItemState();
}

class _VideoItemState extends State<VideoItem> {
  String? thumbnailFile;

  Future<File> copyAssetFile(String assetFileName) async {
    Directory tempDir = await getTemporaryDirectory();
    final byteData = await rootBundle.load(assetFileName);

    File videoThumbnailFile = File("${tempDir.path}/$assetFileName")
      ..createSync(recursive: true)
      ..writeAsBytesSync(byteData.buffer.asUint8List(byteData.offsetInBytes, byteData.lengthInBytes));
    return videoThumbnailFile;
  }

  void generateThumbnail() async {
    thumbnailFile = await VideoThumbnail.thumbnailFile(
        video: widget.videoPath.path,
        thumbnailPath: (await getTemporaryDirectory()).path,
        imageFormat: ImageFormat.PNG);
    if (mounted) {
      setState(() {});
    }
  }

  @override
  void initState() {
    super.initState();
    generateThumbnail();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: widget.height * 0.15,
      width: widget.width * 0.28,
      child: Stack(
        children: [
          thumbnailFile != null
              ? ClipRRect(
                  borderRadius: BorderRadius.circular(12.0),
                  child: SizedBox(
                    height: widget.height * 0.15,
                    width: widget.width * 0.28,
                    child: Image.file(
                      File(thumbnailFile!),
                      fit: BoxFit.cover,
                    ),
                  ),
                )
              : SizedBox(
                  height: widget.height * 0.15,
                  width: widget.width * 0.28,
                  child: const Center(
                      child:
                          SizedBox(width: 20, height: 20, child: CircularProgressIndicator.adaptive(strokeWidth: 2))),
                ),
          widget.showDeleteIcon
              ? Align(
                  alignment: Alignment.topRight,
                  child: IconButton(
                    padding: EdgeInsets.zero,
                    constraints: const BoxConstraints(),
                    onPressed: widget.onDeletePressed,
                    icon: DecoratedBox(
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        border: Border.all(color: Colors.red),
                      ),
                      child: const Padding(
                        padding: EdgeInsets.all(2),
                        child: Icon(Icons.clear_rounded, color: Colors.red, size: 12),
                      ),
                    ),
                  ),
                )
              : const SizedBox(),
          Align(
            alignment: Alignment.center,
            child: IconButton(
              padding: EdgeInsets.zero,
              constraints: const BoxConstraints(),
              onPressed: () => MagicRouter.navigateTo(VideoPlayerScreen(videoPath: widget.videoPath)),
              icon: SvgPicture.asset('assets/images/play_arrow.svg', color: Colors.white),
            ),
          )
        ],
      ),
    );
  }
}
// part of 'imports.dart';
//
// class VideoItem extends StatefulWidget {
//   final File videoThumb;
//   final File videoPath;
//   final bool showDeleteIcon;
//   final Function()? onDeletePressed;
//   const VideoItem({
//     Key? key,
//     required this.videoThumb,
//     required this.videoPath,
//     this.showDeleteIcon = false,
//     this.onDeletePressed,
//   }) : super(key: key);
//
//   @override
//   State<VideoItem> createState() => _VideoItemState();
// }
//
// class _VideoItemState extends State<VideoItem> {
//   late final String uint8list;
//   late VideoPlayerController _controller;
//
//   @override
//   void initState() {
//     super.initState();
//     _controller = VideoPlayerController.network(widget.videoPath.path)
//       ..initialize().then((_) {
//         setState(() {});
//       });
//   }
//
//   @override
//   void dispose() {
//     super.dispose();
//     _controller.dispose();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return SizedBox(
//       height: widget.height * 0.15,
//       width: widget.width * 0.28,
//       child: Stack(
//         children: [
//           _controller.value.isInitialized
//               ? ClipRRect(
//             borderRadius: BorderRadius.circular(12.0),
//             child: SizedBox(
//               height: widget.height * 0.15,
//               width: widget.width * 0.28,
//               child: VideoPlayer(_controller),
//             ),
//           )
//               : SizedBox(
//               height: widget.height * 0.15,
//               width: widget.width * 0.28,
//               child: const Center(
//                   child:
//                   SizedBox(width: 20, height: 20, child: CircularProgressIndicator.adaptive(strokeWidth: 2)))),
//           widget.showDeleteIcon
//               ? Align(
//             alignment: Alignment.topRight,
//             child: IconButton(
//               padding: EdgeInsets.zero,
//               constraints: const BoxConstraints(),
//               onPressed: widget.onDeletePressed,
//               icon: DecoratedBox(
//                 decoration: BoxDecoration(
//                   shape: BoxShape.circle,
//                   border: Border.all(color: Colors.red),
//                 ),
//                 child: const Padding(
//                   padding: EdgeInsets.all(2),
//                   child: Icon(Icons.clear_rounded, color: Colors.red, size: 12),
//                 ),
//               ),
//             ),
//           )
//               : const SizedBox(),
//           Align(
//             alignment: Alignment.center,
//             child: IconButton(
//               padding: EdgeInsets.zero,
//               constraints: const BoxConstraints(),
//               onPressed: () => MagicRouter.navigateTo(VideoPlayerScreen(videoPath: widget.videoPath)),
//               icon: SvgPicture.asset('assets/images/play_arrow.svg', color: Colors.white),
//             ),
//           )
//         ],
//       ),
//     );
//   }
// }
