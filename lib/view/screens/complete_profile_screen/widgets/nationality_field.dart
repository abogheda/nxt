import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:nxt/utils/constants/app_const.dart';
import 'package:nxt/view/screens/complete_profile_screen/cubit/complete_profile_cubit.dart';

import '../../../../utils/constants/app_colors.dart';
import '../../../../utils/helpers/validators.dart';
import '../../../widgets/custom_text_field.dart';

class NationalityField extends StatelessWidget {
  const NationalityField({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CompleteProfileCubit, CompleteProfileState>(
      builder: (context, state) {
        final cubit = CompleteProfileCubit.get(context);
        return CustomTextField(
          prefix: Padding(
            padding: const EdgeInsets.all(4.0),
            child: SvgPicture.asset('assets/images/nationality.svg', color: AppColors.iconsColor, width: 24),
          ),
          hint: 'nationality'.tr(),
          validator: Validators.generalField,
          type: TextInputType.text,
          textStyle: Theme.of(context).textTheme.bodyText1!.copyWith(
                fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal',
                color: Colors.black,
                fontWeight: cubit.nationalityController.value.text.isEmpty ? null : FontWeight.bold,
              ),
          hintStyle: Theme.of(context).textTheme.bodyText1!.copyWith(
                fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal',
                color: AppColors.iconsColor,
              ),
          controller: cubit.nationalityController,
        );
      },
    );
  }
}
