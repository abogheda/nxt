import 'package:flutter/material.dart';
import 'package:flutter_expanded_tile/flutter_expanded_tile.dart';
import 'package:nxt/utils/constants/app_const.dart';
import 'package:nxt/view/screens/subscription/widgets/benefit_text.dart';

class BundleExpandedTile extends StatefulWidget {
  const BundleExpandedTile({Key? key, required this.bundleName, required this.showBorder,this.features, required this.price}) : super(key: key);
  final String bundleName;
  final bool showBorder;
  final List<String>? features;
  final num price;

  @override
  State<BundleExpandedTile> createState() => _BundleExpandedTileState();
}

class _BundleExpandedTileState extends State<BundleExpandedTile> {
  late ExpandedTileController _controller;
  @override
  void initState() {
    _controller = ExpandedTileController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // final cubit = CompleteProfileCubit.get(context);
    return Padding(
      padding: EdgeInsets.only(left: widget.width * 0.05, right: widget.width * 0.05, bottom: widget.height * 0.01),
      child: Container(
        decoration: BoxDecoration(
          borderRadius: widget.showBorder ? BorderRadius.circular(6) : null,
          border: widget.showBorder ? Border.all(color: Colors.black, width: 2) : null,
          boxShadow: [
            BoxShadow(
              blurRadius: 8,
              spreadRadius: 0,
              offset: const Offset(0, 3),
              color: Colors.grey.withOpacity(0.4),
            ),
          ],
        ),
        child: ExpandedTile(
          contentseparator: 0,
          controller: _controller,
          trailing: const Icon(Icons.arrow_forward_ios_rounded, size: 24),
          title: Text(
            widget.bundleName,
            style: Theme.of(context).textTheme.headline5!.copyWith(
                  fontSize: 32,
                  letterSpacing: 1.5,
                  color: Colors.black,
                  fontFamily: AppConst.isEn ? 'BebasNeue' : 'Tajawal',
                ),
          ),
          theme: const ExpandedTileThemeData(
            headerRadius: 6,
            contentRadius: 6,
            headerColor: Colors.white,
            headerSplashColor: Colors.grey,
            contentPadding: EdgeInsets.zero,
            contentBackgroundColor: Colors.white,
            titlePadding: EdgeInsets.zero,
            trailingPadding: EdgeInsets.zero,
            headerPadding: EdgeInsetsDirectional.only(top: 12, bottom: 12, start: 12, end: 4),
            leadingPadding: EdgeInsets.zero,
          ),
          content: Padding(
            padding: EdgeInsets.symmetric(vertical: widget.height * 0.01),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: widget.width * 0.03),
                  child: Text(
                    'EGP ${widget.price}',
                    style: Theme.of(context).textTheme.titleMedium!.copyWith(
                          color: const Color(0xFF3E3E3E),
                          fontFamily: widget.isEn ? 'Inter-Bold' : 'Tajawal',
                        ),
                  ),
                ),
                const SizedBox(height: 4),
                ...List.generate(widget.features?.length ??0,
                      (index) =>
                          BenefitText(text: widget.features![index]),)

              ],
            ),
          ),
        ),
      ),
    );
  }
}
