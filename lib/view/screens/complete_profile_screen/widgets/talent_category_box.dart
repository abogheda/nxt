part of 'imports.dart';

class TalentCategoryBox extends StatefulWidget {
  final TalentCategoryType talent;
  final String title;
  const TalentCategoryBox({Key? key, required this.talent, required this.title}) : super(key: key);

  @override
  State<TalentCategoryBox> createState() => _TalentCategoryBoxState();
}

class _TalentCategoryBoxState extends State<TalentCategoryBox> {
  @override
  Widget build(BuildContext context) {
    final cubit = CompleteProfileCubit.get(context);
    bool isSelected = cubit.verticalCategories.contains(widget.talent.id);
    return InkWell(
      onTap: () async {
        setState(() => isSelected = !isSelected);
        if (isSelected) {
          cubit.verticalCategories.add(widget.talent.id);
        } else {
          cubit.verticalCategories.remove(widget.talent.id);
        }
        cubit.emitState(state: CompleteProfileInitial());
      },
      borderRadius: const BorderRadius.all(Radius.circular(8.0)),
      child: Container(
        height: 120,
        decoration: BoxDecoration(
            border: Border.all(width: 1.4),
            borderRadius: const BorderRadius.all(Radius.circular(8.0)),
            color: isSelected ? widget.talent.color() : null),
        alignment: Alignment.center,
        child: Padding(
          padding: const EdgeInsets.all(4.0),
          child: Text(
            widget.title,
            textAlign: TextAlign.center,
            style: AppTextStyles.huge30.copyWith(color: isSelected ? Colors.white : null),
          ),
        ),
      ),
    );
  }
}
