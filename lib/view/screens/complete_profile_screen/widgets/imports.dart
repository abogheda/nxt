// ignore_for_file: depend_on_referenced_packages

import 'dart:io';
import 'package:video_thumbnail/video_thumbnail.dart';
import 'package:path_provider/path_provider.dart';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import '../../../../data/enum/talent_type.dart';
import '../../../../utils/constants/resources.dart';
import '../component/video_player_screen.dart';
import '../cubit/complete_profile_cubit.dart';
import 'package:dotted_border/dotted_border.dart';
import '../../../../utils/helpers/picker_helper.dart';
import '../../../../config/router/router.dart';
import '../../../widgets/pick_image_dialog.dart';

part 'save_continue_button.dart';
part 'shot_item.dart';
part 'talent_list_view.dart';
part 'talent_category_box.dart';
part 'video_item.dart';
