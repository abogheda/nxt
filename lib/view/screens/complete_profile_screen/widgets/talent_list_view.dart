part of 'imports.dart';

class TalentListView extends StatefulWidget {
  const TalentListView({Key? key}) : super(key: key);

  @override
  State<TalentListView> createState() => _TalentListViewState();
}

class _TalentListViewState extends State<TalentListView> {
  bool isSelected = false;
  @override
  Widget build(BuildContext context) {
    final cubit = CompleteProfileCubit.get(context);
    final chipsList = cubit.talentsList!;
    return Expanded(
      child: RefreshIndicator(
        onRefresh: () async => cubit.getTalents(),
        child: ListView(
          padding: EdgeInsets.only(top: widget.height * 0.01, left: widget.width * 0.02, right: widget.width * 0.02),
          children: [
            Wrap(
              spacing: 6,
              children: chipsList.map((chip) {
                bool isSelected = cubit.talents.contains(chip.id!);
                return FilterChip(
                  label: Text(chip.title ?? "notAvailable".tr()),
                  selected: isSelected,
                  padding: EdgeInsets.symmetric(horizontal: widget.width * 0.03, vertical: widget.height * 0.012),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(4),
                      side: BorderSide(color: isSelected ? Colors.transparent : AppColors.borderGreyColor)),
                  selectedColor: Colors.black,
                  backgroundColor: Colors.white,
                  showCheckmark: false,
                  labelStyle: Theme.of(context).textTheme.bodyText1!.copyWith(
                        fontFamily: widget.isEn ? 'Montserrat' : 'Tajawal',
                        fontWeight: FontWeight.bold,
                        color: isSelected ? Colors.white : Colors.black,
                      ),
                  onSelected: (bool selected) {
                    setState(() {
                      if (selected) {
                        if (!isSelected) cubit.talents.add(chip.id!);
                      } else {
                        cubit.talents.remove(chip.id!);
                      }
                      cubit.emitState(state: ChangeScreenState());
                    });
                  },
                );
              }).toList(),
            ),
          ],
        ),
      ),
    );
  }
}
