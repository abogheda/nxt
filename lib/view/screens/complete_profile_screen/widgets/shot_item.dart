part of 'imports.dart';

class ShotItem extends StatefulWidget {
  final int index;
  const ShotItem({Key? key, required this.index}) : super(key: key);

  @override
  State<ShotItem> createState() => _ShotItemState();
}

class _ShotItemState extends State<ShotItem> {
  late CompleteProfileCubit cubit;

  Future<void> _captureImage() async {
    final photo = await PickerHelper.pickFrontCameraImage();
    if (photo != null) {
      _image = photo;
      cubit.setShotPath(_image!.path, widget.index);
      cubit.emitState(state: UpdateShotsState());
      setState(() {});
    }
  }

  Future<void> _pickImage() async {
    final photo = await PickerHelper.pickGalleryImage();
    if (photo != null) {
      _image = photo;
      cubit.setShotPath(_image!.path, widget.index);
      cubit.emitState(state: UpdateShotsState());
      setState(() {});
    }
  }

  @override
  void initState() {
    cubit = context.read<CompleteProfileCubit>();
    super.initState();
  }

  File? _image;
  @override
  Widget build(BuildContext context) {
    return DottedBorder(
      borderType: BorderType.RRect,
      radius: const Radius.circular(6),
      dashPattern: const [20, 10],
      child: SizedBox(
        height: 116,
        width: double.infinity,
        child: _image != null
            ? ClipRRect(
                borderRadius: const BorderRadius.all(Radius.circular(6.0)),
                child: InkWell(
                  onTap: () => showDialog(
                      context: context,
                      builder: (context) => PickImageDialog(
                            pickFromCamera: () async {
                              await _captureImage();
                              MagicRouter.pop();
                            },
                            pickFromGallery: () async {
                              await _pickImage();
                              MagicRouter.pop();
                            },
                          )),
                  child: Image.file(_image!, fit: BoxFit.cover),
                ),
              )
            : IconButton(
                onPressed: () => showDialog(
                  context: context,
                  builder: (context) => PickImageDialog(
                    pickFromCamera: () async {
                      await _captureImage();
                      MagicRouter.pop();
                    },
                    pickFromGallery: () async {
                      await _pickImage();
                      MagicRouter.pop();
                    },
                  ),
                ),
                icon: const Icon(Icons.add, color: Colors.black),
              ),
      ),
    );
  }
}
