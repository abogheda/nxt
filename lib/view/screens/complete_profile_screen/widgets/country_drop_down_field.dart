import 'package:country_picker/country_picker.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:nxt/utils/constants/app_colors.dart';
import 'package:nxt/utils/constants/app_const.dart';

import '../cubit/complete_profile_cubit.dart';

class CountryDropDownField extends StatefulWidget {
  const CountryDropDownField({Key? key}) : super(key: key);

  @override
  State<CountryDropDownField> createState() => _CountryDropDownFieldState();
}

class _CountryDropDownFieldState extends State<CountryDropDownField> {
  @override
  Widget build(BuildContext context) {
    final defaultStyle = TextStyle(fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal');
    final cubit = CompleteProfileCubit.get(context);
    return BlocBuilder<CompleteProfileCubit, CompleteProfileState>(
      builder: (context, state) {
        return Container(
          padding: EdgeInsets.symmetric(horizontal: widget.width * 0.03),
          decoration: BoxDecoration(
            border: Border.all(color: Colors.black),
            borderRadius: BorderRadius.circular(8),
          ),
          child: Row(
            children: [
              SvgPicture.asset('assets/images/countryOfResidence.svg', color: AppColors.iconsColor, width: 24),
              SizedBox(width: widget.width * 0.01),
              Expanded(
                child: TextButton(
                  child: Align(
                    alignment: AlignmentDirectional.centerStart,
                    child: Text(
                      cubit.countryOfResidence ?? 'countryOfResidence'.tr(),
                      style: Theme.of(context).textTheme.bodyText1!.copyWith(
                            fontFamily: widget.isEn ? 'Montserrat' : 'Tajawal',
                            color: cubit.countryOfResidence == null ? AppColors.iconsColor : Colors.black,
                            fontWeight: cubit.countryOfResidence == null ? null : FontWeight.w600,
                          ),
                    ),
                  ),
                  onPressed: () {
                    showCountryPicker(
                      showSearch: true,
                      context: context,
                      showPhoneCode: false,
                      exclude: <String>['IL'],
                      onSelect: (Country country) => cubit.assignCountryOfResidence(countryName: country.name),
                      countryListTheme: CountryListThemeData(
                        textStyle: defaultStyle,
                        borderRadius: const BorderRadius.only(
                          topLeft: Radius.circular(24.0),
                          topRight: Radius.circular(24.0),
                        ),
                        inputDecoration: InputDecoration(
                          labelText: 'Search',
                          hintText: 'Start typing to search',
                          counterStyle: defaultStyle,
                          helperStyle: defaultStyle,
                          labelStyle: defaultStyle,
                          hintStyle: defaultStyle,
                          suffixStyle: defaultStyle,
                          prefixStyle: defaultStyle,
                          prefixIcon: const Icon(Icons.search),
                          border: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: const Color(0xFF8C98A8).withOpacity(0.2),
                            ),
                          ),
                        ),
                        searchTextStyle: TextStyle(
                          fontSize: 18,
                          fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal',
                        ),
                      ),
                    );
                  },
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
