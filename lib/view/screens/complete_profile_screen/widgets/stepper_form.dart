// part of 'imports.dart';
//
// class StepperForm extends StatefulWidget {
//   const StepperForm({Key? key}) : super(key: key);
//
//   @override
//   State<StepperForm> createState() => _StepperFormState();
// }
//
// class _StepperFormState extends State<StepperForm> {
//   Function()? nextStepMethod({required CompleteProfileCubit cubit}) {
//     Function()? onPressed;
//     switch (cubit.stepperIndex) {
//       case 0:
//         if (cubit.isActing) {
//           onPressed = () {
//             if (cubit.weightController.text.isNotEmpty &&
//                 int.tryParse(cubit.weightController.text).runtimeType != int) {
//               PopupHelper.showBasicSnack(msg: "enterValidWeight".tr(), color: Colors.red);
//               return;
//             }
//             if (cubit.weightController.text.isNotEmpty && (int.parse(cubit.weightController.text) > 300)) {
//               PopupHelper.showBasicSnack(msg: "weight_max".tr(), color: Colors.red);
//               return;
//             }
//             if (cubit.weightController.text.isNotEmpty && (int.parse(cubit.weightController.text) <= 0)) {
//               PopupHelper.showBasicSnack(msg: "weight0".tr(), color: Colors.red);
//               return;
//             }
//             if (cubit.heightController.text.isNotEmpty &&
//                 int.tryParse(cubit.heightController.text).runtimeType != int) {
//               PopupHelper.showBasicSnack(msg: "enterValidHeight".tr(), color: Colors.red);
//               return;
//             }
//             if (cubit.heightController.text.isNotEmpty && (int.parse(cubit.heightController.text) > 250)) {
//               PopupHelper.showBasicSnack(msg: "height_max".tr(), color: Colors.red);
//               return;
//             }
//             if (cubit.heightController.text.isNotEmpty && (int.parse(cubit.heightController.text) <= 0)) {
//               PopupHelper.showBasicSnack(msg: "height0".tr(), color: Colors.red);
//               return;
//             }
//             cubit.userPhysicalFormKey.currentState!.save();
//             cubit.stepperIndex++;
//             cubit.emitState(state: UpdateUserPhysicalState());
//           };
//         } else {
//           onPressed = () {
//             cubit.portfolioFormKey.currentState!.save();
//             String pattern = r'[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@%_\+.~#?&//=]*)';
//             RegExp regExp = RegExp(pattern);
//             if (cubit.portfolioController.text.isNotEmpty && !regExp.hasMatch(cubit.portfolioController.text)) {
//               PopupHelper.showBasicSnack(msg: 'enterValidUrl'.tr());
//             } else {
//               cubit.stepperIndex++;
//               cubit.emitState(state: UpdateUserPhysicalState());
//             }
//           };
//         }
//         break;
//       case 1:
//         if (cubit.isActing) {
//           onPressed = () {
//             cubit.stepperIndex++;
//             cubit.emitState(state: UpdateUserPhysicalState());
//           };
//         } else {
//           onPressed = () async {
//             if (cubit.reels.isNotEmpty) await cubit.uploadReels();
//             await cubit.onFinished();
//           };
//         }
//         break;
//       case 2:
//         onPressed = () {
//           cubit.portfolioFormKey.currentState!.save();
//           String pattern = r'[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@%_\+.~#?&//=]*)';
//           RegExp regExp = RegExp(pattern);
//           if (cubit.portfolioController.text.isNotEmpty && !regExp.hasMatch(cubit.portfolioController.text)) {
//             PopupHelper.showBasicSnack(msg: 'enterValidUrl'.tr());
//           } else {
//             cubit.stepperIndex++;
//             cubit.emitState(state: UpdateUserPhysicalState());
//           }
//         };
//         break;
//       case 3:
//         onPressed = () async {
//           if (cubit.reels.isNotEmpty) await cubit.uploadReels();
//           cubit.stepperIndex++;
//           cubit.nextStepButtonLoading = false;
//           cubit.isUploadingReelsFinished = false;
//           cubit.emitState(state: UpdateUserPhysicalState());
//         };
//         break;
//       case 4:
//         onPressed = () async {
//           if (cubit.isAllShotsSelected) await cubit.uploadShots();
//           cubit.onFinished();
//         };
//         break;
//       default:
//         PopupHelper.showBasicSnack(msg: 'normalErrorMsg'.tr());
//     }
//     return onPressed;
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return BlocConsumer<CompleteProfileCubit, CompleteProfileState>(
//       listener: (context, state) {
//         if (state is UploadingFailed) PopupHelper.showBasicSnack(msg: state.errorMsg.toString(), color: Colors.red);
//         if (state is CompleteProfilePatchFailed) {
//           PopupHelper.showBasicSnack(msg: state.errorMsg.toString(), color: Colors.red);
//         }
//         if (state is CompleteProfilePostFailed) {
//           PopupHelper.showBasicSnack(msg: state.errorMsg.toString(), color: Colors.red);
//         }
//       },
//       builder: (context, state) {
//         final cubit = CompleteProfileCubit.get(context);
//         return Column(
//           children: [
//             FittedBox(
//               child: Padding(
//                 padding: EdgeInsets.symmetric(horizontal: widget.width * 0.03),
//                 child: Text(
//                   'completeYourProfile'.tr(),
//                   textAlign: TextAlign.center,
//                   style: TextStyle(fontSize: 44, fontFamily: widget.isEn ? 'BebasNeue' : 'Tajawal', height: .88),
//                 ),
//               ),
//             ),
//             Expanded(
//               child: Column(
//                 children: [
//                   SizedBox(
//                     height: widget.height * 0.11,
//                     child: LayoutBuilder(builder: (_, cons) {
//                       return Timeline.tileBuilder(
//                         scrollDirection: Axis.horizontal,
//                         builder: TimelineTileBuilder.connected(
//                             itemCount: cubit.isActing ? cubit.processes.length : cubit.miniProcesses.length,
//                             itemExtentBuilder: (_, __) =>
//                                 cons.maxWidth / (cubit.isActing ? cubit.processes.length : cubit.miniProcesses.length),
//                             indicatorBuilder: (_, i) {
//                               if (i == cubit.stepperIndex) {
//                                 return const CurrentPoint();
//                               } else if (i < cubit.stepperIndex) {
//                                 return const DonePoint();
//                               } else {
//                                 return const NotDonePoint();
//                               }
//                             },
//                             connectorBuilder: (_, i, type) => Container(
//                                 height: 2, color: i < cubit.stepperIndex ? Colors.black : const Color(0xFFD1D5DB)),
//                             contentsBuilder: (_, i) {
//                               if (cubit.stepperIndex != i) return const SizedBox();
//                               return FittedBox(
//                                 child: Column(
//                                   crossAxisAlignment: CrossAxisAlignment.center,
//                                   mainAxisAlignment: MainAxisAlignment.center,
//                                   children: [
//                                     Padding(
//                                       padding: const EdgeInsets.all(2),
//                                       child: Text(
//                                         "${'step'.tr()} ${i + 1}",
//                                         style: TextStyle(
//                                             fontSize: 12,
//                                             color: const Color(0xFF8B8B8B),
//                                             fontFamily: widget.isEn ? 'Montserrat' : 'Tajawal'),
//                                       ),
//                                     ),
//                                     Text(
//                                       cubit.isActing
//                                           ? cubit.processKeysList[cubit.stepperIndex].tr()
//                                           : cubit.miniProcessKeysList[cubit.stepperIndex].tr(),
//                                       textAlign: TextAlign.center,
//                                       style: TextStyle(
//                                           fontSize: 14,
//                                           fontWeight: FontWeight.bold,
//                                           fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal'),
//                                     )
//                                   ],
//                                 ),
//                               );
//                             }),
//                       );
//                     }),
//                   ),
//                   Container(color: Colors.black, height: 1.1, margin: EdgeInsets.only(top: widget.height * 0.015)),
//                   Expanded(
//                     child: cubit.isActing
//                         ? cubit.processValuesList[cubit.stepperIndex]
//                         : cubit.miniProcessValuesList[cubit.stepperIndex],
//                   ),
//                   SaveAndContinueButton(
//                     text: 'nextStep'.tr().toUpperCase(),
//                     onPressed: state is UploadLoading ? null : nextStepMethod(cubit: cubit),
//                     child: cubit.nextStepButtonLoading ? const AppLoader(color: Colors.white) : null,
//                   ),
//                 ],
//               ),
//             ),
//           ],
//         );
//       },
//     );
//   }
// }
