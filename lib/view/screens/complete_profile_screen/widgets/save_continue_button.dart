part of 'imports.dart';

class SaveAndContinueButton extends StatelessWidget {
  final String? text;
  final void Function()? onPressed;
  final Widget? child;
  const SaveAndContinueButton({Key? key, this.onPressed, this.text, this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: width,
      height: (height * 0.07) + MediaQuery.of(context).padding.bottom,
      child: ElevatedButton(
        onPressed: onPressed,
        style: ElevatedButton.styleFrom(
          shape: const RoundedRectangleBorder(borderRadius: BorderRadius.zero),
        ),
        child: child ??
            Text(
              text ?? 'saveContinue'.tr().toUpperCase(),
              style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.w900,
                      color: Colors.white.withOpacity(onPressed == null ? 0.5 : 1),
                      fontFamily: isEn ? 'Montserrat' : 'Tajawal')
                  .copyWith(),
            ),
      ),
    );
  }
}
