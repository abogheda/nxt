import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../../utils/helpers/picker_helper.dart';
import '../../../../utils/helpers/popup_helper.dart';

import '../../../../data/models/user_models/job_experience_model.dart';
import '../../../../utils/constants/app_colors.dart';
import '../../../../utils/constants/app_text_style.dart';
import '../../../widgets/custom_text_field.dart';
import '../cubit/complete_profile_cubit.dart';

class JobExperiencesWidget extends StatefulWidget {
  const JobExperiencesWidget({Key? key}) : super(key: key);

  @override
  State<JobExperiencesWidget> createState() => _JobExperiencesWidgetState();
}

class _JobExperiencesWidgetState extends State<JobExperiencesWidget> {
  late CompleteProfileCubit _bloc;
  @override
  void initState() {
    _bloc = context.read<CompleteProfileCubit>();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        for (var item in _bloc.jobExperiences)
          Container(
            key: ValueKey(_bloc.jobExperiences.indexOf(item)),
            decoration: BoxDecoration(
              border: Border.all(width: 1),
              borderRadius: const BorderRadius.all(Radius.circular(6.0)),
            ),
            margin: const EdgeInsets.only(bottom: 8),
            padding: const EdgeInsets.all(12),
            child: Column(
              children: [
                CustomTextField(
                  hint: 'jobTitle'.tr(),
                  onChange: (val) => item.jobTitle = val,
                ),
                const SizedBox(height: 12),
                CustomTextField(
                  hint: 'roleDescription'.tr(),
                  height: 56,
                  onChange: (val) => item.roleDescription = val,
                ),
                const SizedBox(height: 8),
                Row(
                  children: [
                    Expanded(
                      child: StatefulBuilder(builder: (_, stState) {
                        return CustomTextField(
                          key: ValueKey(item.workFrom),
                          hint: 'from'.tr(),
                          initialValue: item.workFrom == null ? null : DateFormat("dd/MM/yyyy").format(item.workFrom!),
                          onTap: () async {
                            FocusManager.instance.primaryFocus?.unfocus();
                            final date = await PickerHelper.pickDate();
                            if (date != null) {
                              item.workFrom = date;
                              stState(() {});
                            }
                          },
                        );
                      }),
                    ),
                    const SizedBox(width: 8),
                    Expanded(
                      child: StatefulBuilder(builder: (_, stState) {
                        return CustomTextField(
                          key: ValueKey(item.workTo),
                          hint: 'to'.tr(),
                          initialValue: item.workTo == null ? null : DateFormat("dd/MM/yyyy").format(item.workTo!),
                          onTap: () async {
                            if (item.workFrom == null) {
                              PopupHelper.showBasicSnack(msg: 'pleaseInsertWorkFrom'.tr());
                              return;
                            }
                            FocusManager.instance.primaryFocus?.unfocus();
                            final date = await PickerHelper.pickDate(
                              currentTime: DateTime.now(),
                              maxTime: DateTime(2200),
                              minTime: item.workFrom ?? DateTime.now(),
                            );
                            if (date != null) {
                              item.workTo = date;
                              stState(() {});
                            }
                          },
                        );
                      }),
                    ),
                  ],
                )
              ],
            ),
          ),
        TextButton.icon(
          onPressed: () {
            _bloc.jobExperiences.add(JobExperienceModel());
            setState(() {});
          },
          icon: const Icon(
            Icons.add_box_rounded,
            color: Colors.black,
          ),
          label: Text(
            'addMore'.tr(),
            style: AppTextStyles.medium14.copyWith(color: AppColors.greyTxtColor),
          ),
        ),
        SizedBox(height: MediaQuery.of(context).viewInsets.bottom),
      ],
    );
  }
}
