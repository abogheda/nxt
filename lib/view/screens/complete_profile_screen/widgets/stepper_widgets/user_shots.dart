import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import '../../../../../utils/constants/resources.dart';
import '../../cubit/complete_profile_cubit.dart';
import '../imports.dart';

class UserShots extends StatefulWidget {
  const UserShots({Key? key}) : super(key: key);

  @override
  State<UserShots> createState() => _UserShotsState();
}

class _UserShotsState extends State<UserShots> {
  @override
  Widget build(BuildContext context) {
    return ListView(
      padding: EdgeInsets.symmetric(horizontal: widget.width * 0.06),
      children: [
        SizedBox(height: widget.height * 0.02),
        Text('need3Shots'.tr(), style: AppTextStyles.medium14),
        SizedBox(height: widget.height * 0.015),
        Text('withDiffAngel'.tr(), style: AppTextStyles.bold14),
        SizedBox(height: widget.height * 0.01),
        DecoratedBox(
            decoration: BoxDecoration(border: Border.all(), borderRadius: const BorderRadius.all(Radius.circular(6.0))),
            child: ClipRRect(
                borderRadius: const BorderRadius.all(Radius.circular(6.0)),
                child: Image.asset('assets/images/shots_guide.png', width: double.infinity, fit: BoxFit.cover))),
        const SizedBox(height: 16),
        Row(
          children: const [
            Expanded(child: ShotItem(index: 0)),
            SizedBox(width: 12),
            Expanded(child: ShotItem(index: 1)),
            SizedBox(width: 12),
            Expanded(child: ShotItem(index: 2)),
          ],
        ),
        SizedBox(height: widget.height * 0.02),
        BlocBuilder<CompleteProfileCubit, CompleteProfileState>(
          builder: (context, state) {
            final cubit = CompleteProfileCubit.get(context);
            if (state is! UploadLoading) return const SizedBox();
            if (cubit.isUploadingReelsFinished) {
              return Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Icon(Icons.check_circle_rounded, color: Colors.green),
                  Text('done'.tr(),
                      style: TextStyle(fontFamily: widget.isEn ? 'Montserrat' : 'Tajawal', color: Colors.green)),
                ],
              );
            }
            return StreamBuilder(
                initialData: cubit.shotsUploadingProgress,
                stream: cubit.shotsStreamController.stream,
                builder: (context, snapshot) {
                  return LinearPercentIndicator(
                    animation: true,
                    lineHeight: 20.0,
                    animationDuration: 2500,
                    curve: Curves.easeOut,
                    percent: snapshot.data as double,
                    clipLinearGradient: true,
                    alignment: MainAxisAlignment.start,
                    leading: Text('${'progress'.tr()} :',
                        style: TextStyle(fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal')),
                    trailing: Text('${cubit.uploadingShotFileNumber ?? 1}/${cubit.shots.length}',
                        style: TextStyle(fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal')),
                    addAutomaticKeepAlive: false,
                    animateFromLastPercent: true,
                    barRadius: const Radius.circular(12.0),
                    center: Text('${(cubit.shotsUploadingProgress * 100).toStringAsFixed(1)}%',
                        style: const TextStyle(color: Colors.white)),
                    progressColor: Colors.black,
                    backgroundColor: Colors.black12,
                  );
                });
          },
        )
      ],
    );
  }
}
