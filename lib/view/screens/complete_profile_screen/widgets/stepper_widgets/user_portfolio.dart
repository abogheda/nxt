// import 'package:easy_localization/easy_localization.dart';
// import 'package:flutter/material.dart';
// import '../../../../../utils/constants/resources.dart';
//
// import '../../../../widgets/custom_text_field.dart';
// import '../../cubit/complete_profile_cubit.dart';
// import '../job_experiences_widget.dart';
//
// class UserPortfolio extends StatefulWidget {
//   const UserPortfolio({Key? key}) : super(key: key);
//
//   @override
//   State<UserPortfolio> createState() => _UserPortfolioState();
// }
//
// class _UserPortfolioState extends State<UserPortfolio> {
//   @override
//   Widget build(BuildContext context) {
//     final cubit = CompleteProfileCubit.get(context);
//     return SingleChildScrollView(
//       padding: const EdgeInsets.symmetric(horizontal: 24),
//       child: Form(
//         key: cubit.portfolioFormKey,
//         child: Column(
//           crossAxisAlignment: CrossAxisAlignment.start,
//           children: [
//             const SizedBox(height: 12),
//             Text('havePortfolio'.tr().toUpperCase(), style: AppTextStyles.bold13),
//             const SizedBox(height: 12),
//             CustomTextField(
//               controller: cubit.portfolioController,
//               type: TextInputType.url,
//               hint: 'portfolioLink'.tr(),
//               prefix: const Icon(Icons.link_rounded),
//             ),
//             const SizedBox(height: 16),
//             Text(
//               'prevExperience'.tr().toUpperCase(),
//               style: AppTextStyles.bold13,
//             ),
//             const SizedBox(height: 4),
//             Text('fillInInfo'.tr().toUpperCase(),
//                 style: AppTextStyles.bold12.copyWith(
//                   color: AppColors.greyTxtColor,
//                 )),
//             const SizedBox(height: 8),
//             const JobExperiencesWidget(),
//           ],
//         ),
//       ),
//     );
//   }
// }
