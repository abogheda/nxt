import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

import '../../../../../utils/constants/resources.dart';

class CompleteProfileHeader extends StatelessWidget {
  const CompleteProfileHeader({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        FittedBox(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: width * 0.03),
            child: Text(
              'completeYourProfile'.tr(),
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 44, fontFamily: isEn ? 'BebasNeue' : 'Tajawal', height: .88),
            ),
          ),
        ),
        Container(color: Colors.black, height: 1.1, margin: EdgeInsets.only(top: height * 0.015)),
      ],
    );
  }
}
