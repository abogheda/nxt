import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nxt/view/screens/complete_profile_screen/widgets/country_drop_down_field.dart';
import 'package:nxt/view/screens/complete_profile_screen/widgets/stepper_widgets/user_body_info_screen.dart';
import 'package:nxt/view/screens/navigation_and_appbar/import_widget.dart';

import '../../../../../config/router/router.dart';
import '../../../../../utils/constants/resources.dart';
import '../../../../../utils/helpers/popup_helper.dart';
import '../../../../widgets/app_loader.dart';
import '../../base_complete_profile_screen.dart';
import '../../cubit/complete_profile_cubit.dart';
import '../imports.dart';
import '../nationality_field.dart';
import 'gender_drop_down_field.dart';

class UserInfoScreen extends StatefulWidget {
  const UserInfoScreen({Key? key}) : super(key: key);

  @override
  State<UserInfoScreen> createState() => _UserInfoScreenState();
}

class _UserInfoScreenState extends State<UserInfoScreen> {
  @override
  Widget build(BuildContext context) {
    return BaseCompleteProfileScreen(
      showHeader: true,
      child: BlocBuilder<CompleteProfileCubit, CompleteProfileState>(
        builder: (context, state) {
          final cubit = CompleteProfileCubit.get(context);
          return Stack(
            children: <Widget>[
              Form(
                key: cubit.userInfoFormKey,
                child: ListView(
                  shrinkWrap: true,
                  padding: EdgeInsets.symmetric(horizontal: widget.width * 0.05),
                  children: [
                    SizedBox(height: widget.height * 0.025),
                    const GenderDropDownField(),
                    const NationalityField(),
                    SizedBox(height: widget.height * 0.01),
                    const CountryDropDownField(),
                    SizedBox(height: widget.height * 0.01),
                    SizedBox(height: MediaQuery.of(context).viewInsets.bottom),
                  ],
                ),
              ),
              Positioned(
                bottom: 0,
                child: SaveAndContinueButton(
                    text: 'next'.tr().toUpperCase(),
                    onPressed: state is CompleteProfilePatchLoading
                        ? () {}
                        : () async {
                            if (cubit.nationalityController.text.isNotEmpty &&
                                (cubit.nationalityController.text.length < 4)) {
                              PopupHelper.showBasicSnack(msg: "enterValidNationality".tr(), color: Colors.red);
                              return;
                            }
                            cubit.userInfoFormKey.currentState!.save();
                            if (cubit.nationalityController.text.isNotEmpty ||
                                cubit.gender != null ||
                                cubit.countryOfResidence != null) {
                              await cubit.completeProfilePatch();
                            }
                            if (cubit.isActing) {
                              MagicRouter.navigateTo(BlocProvider.value(
                                value: cubit,
                                child: const UserBodyInfoScreen(),
                              ));
                            } else {
                              MagicRouter.navigateAndPopAll(Navigation());
                            }
                          },
                    child: state is CompleteProfilePatchLoading
                        ? const Center(child: AppLoader(color: Colors.white))
                        : null),
              )
            ],
          );
        },
      ),
    );
  }
}
