import 'dart:io';

import 'package:percent_indicator/percent_indicator.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:easy_localization/easy_localization.dart' as local;
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../../../utils/constants/resources.dart';
import '../../../../../utils/helpers/picker_helper.dart';
import '../../cubit/complete_profile_cubit.dart';
import '../imports.dart';

class UserReels extends StatefulWidget {
  const UserReels({Key? key}) : super(key: key);

  @override
  State<UserReels> createState() => _UserReelsState();
}

class _UserReelsState extends State<UserReels> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CompleteProfileCubit, CompleteProfileState>(
      builder: (context, state) {
        final cubit = CompleteProfileCubit.get(context);
        return ListView(
          padding: EdgeInsets.only(right: widget.width * 0.05, left: widget.width * 0.05, top: widget.height * 0.012),
          shrinkWrap: true,
          children: [
            Text('haveReel'.tr(), style: AppTextStyles.bold14.copyWith(fontWeight: FontWeight.bold)),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 8.0),
              child: Text(
                'maxSec'.tr(),
                textAlign: TextAlign.center,
                style:
                    Theme.of(context).textTheme.caption!.copyWith(fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal'),
              ),
            ),
            // SizedBox(height: 8),
            SizedBox(height: widget.height * 0.01),
            Wrap(
              direction: Axis.horizontal,
              runSpacing: 10,
              spacing: 10,
              children: [
                for (var item in cubit.reelsAll)
                  VideoItem(
                    key: UniqueKey(),
                    videoPath: item!,
                    showDeleteIcon: true,
                    onDeletePressed: state is UploadLoading
                        ? null
                        : () {
                            cubit.removeReelAll(item);
                            cubit.removeReel(item);
                            cubit.removeReelThumb(item);
                            setState(() {});
                          },
                  ),
                cubit.reelsAll.length == 2
                    ? const SizedBox()
                    : DottedBorder(
                        borderType: BorderType.RRect,
                        radius: const Radius.circular(6),
                        dashPattern: const [20, 6],
                        padding: EdgeInsets.zero,
                        child: SizedBox(
                          height: widget.height * 0.15 - 3,
                          width: widget.width * 0.28 - 4,
                          child: IconButton(
                            onPressed: state is UploadLoading
                                ? null
                                : () async {
                                    File? videoAll = await PickerHelper.pickVideoFile();
                                    if (videoAll != null) {
                                      final videoFilePath = videoAll;
                                      final videoFileThumb = videoAll;
                                      cubit.addReelAll(videoAll);
                                      cubit.addReel(videoFilePath);
                                      cubit.addReelThumb(videoFileThumb);
                                      setState(() {});
                                    }
                                  },
                            padding: EdgeInsets.zero,
                            icon: const Icon(Icons.add, color: Colors.black),
                          ),
                        ),
                      ),
              ],
            ),
            SizedBox(height: widget.height * 0.02),
            BlocBuilder<CompleteProfileCubit, CompleteProfileState>(
              builder: (context, state) {
                if (state is! UploadLoading) return const SizedBox();
                if (cubit.isUploadingReelsFinished) {
                  return Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const Icon(Icons.check_circle_rounded, color: Colors.green),
                      Text('done'.tr(),
                          style: TextStyle(fontFamily: widget.isEn ? 'Montserrat' : 'Tajawal', color: Colors.green)),
                    ],
                  );
                }
                return StreamBuilder(
                    initialData: cubit.reelUploadingProgress,
                    stream: cubit.reelStreamController.stream,
                    builder: (context, snapshot) {
                      return LinearPercentIndicator(
                        animation: true,
                        lineHeight: 20.0,
                        animationDuration: 2500,
                        curve: Curves.easeOut,
                        // percent: double.parse(snapshot.data.toString()),
                        percent: snapshot.data as double,
                        clipLinearGradient: true,
                        alignment: MainAxisAlignment.start,
                        leading: Text('${'progress'.tr()} :',
                            style: TextStyle(fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal')),
                        trailing: Text('${cubit.uploadingReelFileNumber ?? 1}/${cubit.reels.length}',
                            style: TextStyle(fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal')),
                        addAutomaticKeepAlive: false,
                        animateFromLastPercent: true,
                        barRadius: const Radius.circular(12.0),
                        center: Text('${(cubit.reelUploadingProgress * 100).toStringAsFixed(1)}%',
                            style: const TextStyle(color: Colors.white)),
                        progressColor: Colors.black,
                        backgroundColor: Colors.black12,
                      );
                    });
              },
            ),
          ],
        );
      },
    );
  }
}
