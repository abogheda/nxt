import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import '../../../../../utils/constants/resources.dart';
import '../../../../widgets/custom_drop_down_field.dart';
import '../../cubit/complete_profile_cubit.dart';

class GenderDropDownField extends StatelessWidget {
  const GenderDropDownField({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final cubit = CompleteProfileCubit.get(context);
    return CustomDropDownTextField<String>(
      key: UniqueKey(),
      value: cubit.gender,
      hint: 'chooseGender'.tr(),
      hintTextStyle: Theme.of(context)
          .textTheme
          .bodyText1!
          .copyWith(fontFamily: isEn ? 'Montserrat' : 'Tajawal', color: AppColors.iconsColor),
      prefix: Padding(padding: const EdgeInsets.all(4), child: SvgPicture.asset('assets/images/gender.svg')),
      items: [
        DropdownMenuItem(
          value: 'Male',
          child: Text('Male'.tr(), style: TextStyle(fontFamily: isEn ? 'Montserrat' : 'Tajawal')),
        ),
        DropdownMenuItem(
          value: 'Female',
          child: Text('Female'.tr(), style: TextStyle(fontFamily: isEn ? 'Montserrat' : 'Tajawal')),
        )
      ],
      onChanged: (value) {
        cubit.gender = value!.capitalize;
        cubit.userInfoFormKey.currentState!.save();
        cubit.emitState(state: UpdateUserPhysicalState());
      },
      validator: (value) {
        if (value == null) {
          return 'empty_field'.tr();
        } else if (value.isEmpty) {
          return 'empty_field'.tr();
        } else {
          return null;
        }
      },
    );
  }
}
