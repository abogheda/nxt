import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:nxt/utils/constants/app_const.dart';
import 'package:nxt/view/screens/complete_profile_screen/widgets/unit_container.dart';
import '../../../../../config/router/router.dart';
import '../../../../../utils/constants/app_colors.dart';
import '../../../../../utils/constants/app_images.dart';
import '../../../../../utils/constants/app_text_style.dart';
import '../../../../../utils/helpers/popup_helper.dart';
import '../../../../../utils/helpers/validators.dart';
import '../../../../widgets/app_loader.dart';
import '../../../../widgets/custom_text_field.dart';
import '../../../navigation_and_appbar/import_widget.dart';
import '../../base_complete_profile_screen.dart';
import '../../component/body_label.dart';
import '../../cubit/complete_profile_cubit.dart';
import '../imports.dart';

class UserBodyInfoScreen extends StatefulWidget {
  const UserBodyInfoScreen({Key? key}) : super(key: key);

  @override
  State<UserBodyInfoScreen> createState() => _UserBodyInfoScreenState();
}

class _UserBodyInfoScreenState extends State<UserBodyInfoScreen> {
  @override
  Widget build(BuildContext context) {
    return BaseCompleteProfileScreen(
      showHeader: true,
      child: BlocBuilder<CompleteProfileCubit, CompleteProfileState>(
        builder: (context, state) {
          final cubit = CompleteProfileCubit.get(context);
          return Stack(
            fit: StackFit.loose,
            children: <Widget>[
              ListView(
                shrinkWrap: true,
                children: [
                  Form(
                    key: cubit.userInfoBodyFormKey,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              const BodyLabel(text: 'weight', customHeight: 0),
                              Padding(
                                padding: EdgeInsetsDirectional.only(
                                    start: widget.width * 0.03,
                                    end: widget.width * 0.015,
                                    bottom: widget.height * 0.015,
                                    top: widget.height * 0.015),
                                child: CustomTextField(
                                  hint: 'chooseWeight'.tr(),
                                  type: TextInputType.number,
                                  suffix:
                                      cubit.weightController.value.text.isEmpty ? null : UnitContainer(unit: 'kg'.tr()),
                                  controller: cubit.weightController,
                                  prefix: SvgPicture.asset('assets/images/weight.svg'),
                                  validator: Validators.weight,
                                  onChange: (value) {
                                    cubit.userInfoFormKey.currentState!.save();
                                    cubit.emitState(state: UpdateUserPhysicalState());
                                  },
                                  onFieldSubmitted: (_) => cubit.heightFocusNode.requestFocus(),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              const BodyLabel(text: 'height', customHeight: 0),
                              Padding(
                                padding: EdgeInsetsDirectional.only(
                                    start: widget.width * 0.03,
                                    end: widget.width * 0.03,
                                    bottom: widget.height * 0.015,
                                    top: widget.height * 0.015),
                                child: CustomTextField(
                                  hint: 'chooseHeight'.tr(),
                                  type: TextInputType.number,
                                  validator: Validators.height,
                                  focusNode: cubit.heightFocusNode,
                                  controller: cubit.heightController,
                                  suffix:
                                      cubit.heightController.value.text.isEmpty ? null : UnitContainer(unit: 'cm'.tr()),
                                  prefix: SvgPicture.asset('assets/images/height.svg'),
                                  onChange: (value) {
                                    cubit.userInfoFormKey.currentState!.save();
                                    cubit.emitState(state: UpdateUserPhysicalState());
                                  },
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  const BodyLabel(text: 'skinColor'),
                  Container(
                    width: widget.width,
                    height: widget.height * 0.12,
                    margin: const EdgeInsets.symmetric(vertical: 8),
                    child: ListView.builder(
                      shrinkWrap: true,
                      itemCount: AppConst.skinColors.length,
                      scrollDirection: Axis.horizontal,
                      padding: const EdgeInsets.symmetric(horizontal: 12.0),
                      itemBuilder: (context, index) {
                        var item = AppConst.skinColors[index];
                        return AnimatedContainer(
                          duration: const Duration(milliseconds: 200),
                          curve: Curves.easeInCubic,
                          padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 2),
                          decoration: BoxDecoration(
                              border: Border.all(
                                width: 1.5,
                                color: cubit.skin == item ? Colors.black : Colors.transparent,
                              ),
                              borderRadius: const BorderRadius.all(Radius.circular(6.0))),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Padding(
                                padding: EdgeInsets.symmetric(vertical: widget.height * 0.003),
                                child: GestureDetector(
                                  onTap: () => setState(() => cubit.skin = item),
                                  child: Image.asset(AppImages.getSkinImage(item)),
                                ),
                              ),
                              Text(
                                item.toLowerCase().tr(),
                                style: AppTextStyles.bold12.copyWith(color: AppColors.iconsColor),
                              ),
                            ],
                          ),
                        );
                      },
                    ),
                  ),
                  const BodyLabel(text: 'hairColor'),
                  Container(
                    width: widget.width,
                    height: widget.height * 0.12,
                    margin: const EdgeInsets.symmetric(vertical: 8),
                    child: ListView.builder(
                      shrinkWrap: true,
                      itemCount: AppConst.hairColors.length,
                      scrollDirection: Axis.horizontal,
                      padding: const EdgeInsets.symmetric(horizontal: 12.0),
                      itemBuilder: (context, index) {
                        var item = AppConst.hairColors[index];
                        return AnimatedContainer(
                          duration: const Duration(milliseconds: 200),
                          curve: Curves.easeInCubic,
                          padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 2),
                          decoration: BoxDecoration(
                              border: Border.all(
                                width: 1.5,
                                color: cubit.hair == item ? Colors.black : Colors.transparent,
                              ),
                              borderRadius: const BorderRadius.all(Radius.circular(6.0))),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Padding(
                                padding: EdgeInsets.symmetric(vertical: widget.height * 0.003),
                                child: GestureDetector(
                                  onTap: () => setState(() => cubit.hair = item),
                                  child: Image.asset(AppImages.getHairImage(item)),
                                ),
                              ),
                              Text(
                                item.toLowerCase().tr(),
                                style: AppTextStyles.bold12.copyWith(color: AppColors.iconsColor),
                              ),
                            ],
                          ),
                        );
                      },
                    ),
                  ),
                  const BodyLabel(text: 'eyeColor'),
                  Container(
                    width: widget.width,
                    height: widget.height * 0.12,
                    margin: const EdgeInsets.symmetric(vertical: 8),
                    child: ListView.builder(
                      shrinkWrap: true,
                      itemCount: AppConst.eyeColors.length,
                      scrollDirection: Axis.horizontal,
                      padding: const EdgeInsets.symmetric(horizontal: 12.0),
                      itemBuilder: (context, index) {
                        var item = AppConst.eyeColors[index];
                        return AnimatedContainer(
                          duration: const Duration(milliseconds: 200),
                          curve: Curves.easeInCubic,
                          padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 2),
                          decoration: BoxDecoration(
                              border: Border.all(
                                width: 1.5,
                                color: cubit.eye == item ? Colors.black : Colors.transparent,
                              ),
                              borderRadius: const BorderRadius.all(Radius.circular(6.0))),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Padding(
                                padding: EdgeInsets.symmetric(vertical: widget.height * 0.003),
                                child: GestureDetector(
                                  onTap: () => setState(() => cubit.eye = item),
                                  child: Image.asset(AppImages.getEyeImage(item)),
                                ),
                              ),
                              Text(
                                item.toLowerCase().tr(),
                                style: AppTextStyles.bold12.copyWith(color: AppColors.iconsColor),
                              ),
                            ],
                          ),
                        );
                      },
                    ),
                  ),
                  SizedBox(height: widget.height * 0.09 + MediaQuery.of(context).viewInsets.bottom),
                ],
              ),
              Positioned(
                bottom: 0,
                child: SaveAndContinueButton(
                  text: 'next'.tr().toUpperCase(),
                  onPressed: state is CompleteProfilePatchLoading
                      ? () {}
                      : () async {
                          if (cubit.weightController.text.isNotEmpty &&
                              int.tryParse(cubit.weightController.text).runtimeType != int) {
                            PopupHelper.showBasicSnack(msg: "enterValidWeight".tr(), color: Colors.red);
                            return;
                          }
                          if (cubit.weightController.text.isNotEmpty &&
                              (int.parse(cubit.weightController.text) > 300)) {
                            PopupHelper.showBasicSnack(msg: "weight_max".tr(), color: Colors.red);
                            return;
                          }
                          if (cubit.weightController.text.isNotEmpty && (int.parse(cubit.weightController.text) <= 0)) {
                            PopupHelper.showBasicSnack(msg: "weight0".tr(), color: Colors.red);
                            return;
                          }
                          if (cubit.heightController.text.isNotEmpty &&
                              int.tryParse(cubit.heightController.text).runtimeType != int) {
                            PopupHelper.showBasicSnack(msg: "enterValidHeight".tr(), color: Colors.red);
                            return;
                          }
                          if (cubit.heightController.text.isNotEmpty &&
                              (int.parse(cubit.heightController.text) > 250)) {
                            PopupHelper.showBasicSnack(msg: "height_max".tr(), color: Colors.red);
                            return;
                          }
                          if (cubit.heightController.text.isNotEmpty && (int.parse(cubit.heightController.text) <= 0)) {
                            PopupHelper.showBasicSnack(msg: "height0".tr(), color: Colors.red);
                            return;
                          }
                          cubit.userInfoBodyFormKey.currentState!.save();
                          await cubit.completeProfilePatch();
                          MagicRouter.navigateAndPopAll(Navigation());
                        },
                  child:
                      state is CompleteProfilePatchLoading ? const Center(child: AppLoader(color: Colors.white)) : null,
                ),
              )
            ],
          );
        },
      ),
    );
  }
}
