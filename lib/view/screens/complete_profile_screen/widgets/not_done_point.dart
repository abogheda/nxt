import 'package:flutter/material.dart';
import '../../../../utils/constants/app_const.dart';

class NotDonePoint extends StatelessWidget {
  const NotDonePoint({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width * 0.085,
      height: width * 0.085,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        border: Border.all(width: 3, color: const Color(0xFFD1D5DB)),
      ),
    );
  }
}
