import 'package:flutter/material.dart';
import '../../../../utils/constants/app_const.dart';

class DonePoint extends StatelessWidget {
  const DonePoint({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width * 0.085,
      height: width * 0.085,
      decoration: const BoxDecoration(shape: BoxShape.circle, color: Colors.black),
      alignment: Alignment.center,
      child: Icon(Icons.check, size: width * 0.045, color: Colors.white),
    );
  }
}
