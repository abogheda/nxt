import 'package:flutter/material.dart';
import '../../../../utils/constants/resources.dart';

class UnitContainer extends StatelessWidget {
  const UnitContainer({Key? key, required this.unit}) : super(key: key);
  final String unit;
  @override
  Widget build(BuildContext context) {
    return Container(
      width: width * 0.12,
      alignment: Alignment.center,
      decoration: const BoxDecoration(
          color: Colors.black,
          borderRadius: BorderRadiusDirectional.only(
              bottomStart: Radius.zero,
              topStart: Radius.zero,
              bottomEnd: Radius.circular(8.0),
              topEnd: Radius.circular(8.0))),
      child: Text(unit, style: AppTextStyles.bold14.copyWith(color: Colors.white)),
    );
  }
}
