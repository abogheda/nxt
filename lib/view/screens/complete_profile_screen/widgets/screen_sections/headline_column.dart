import 'package:flutter/material.dart';
import '../../../../../utils/constants/app_const.dart';

class HeadlineColumn extends StatelessWidget {
  const HeadlineColumn({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Flex(
      direction: Axis.vertical,
      children: const [
        CustomHeadlineText(text: 'YOU'),
        CustomHeadlineText(text: 'ARE'),
        CustomHeadlineText(text: 'THE'),
        CustomHeadlineText(text: 'NXT'),
      ],
    );
  }
}

class CustomHeadlineText extends StatelessWidget {
  const CustomHeadlineText({Key? key, required this.text}) : super(key: key);
  final String text;
  @override
  Widget build(BuildContext context) {
    return FittedBox(
      fit: BoxFit.fitHeight,
      child: Text(
        text,
        // style: TextStyle(height: height * 0.00123, color: Colors.black, fontFamily: 'BebasNeue'),
        style: Theme.of(context).textTheme.headline1!.copyWith(height: height * 0.0013, color: Colors.black),
      ),
    );
  }
}
