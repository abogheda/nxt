import 'package:auto_size_text/auto_size_text.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import '../../../../../data/enum/talent_type.dart';
import '../../../../../utils/constants/resources.dart';
import '../../../../../utils/helpers/popup_helper.dart';
import '../../cubit/complete_profile_cubit.dart';
import '../imports.dart';

class TalentCategoryWidget extends StatelessWidget {
  const TalentCategoryWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 40),
      child: Column(
        children: [
          Text('whatIsYourTalent'.tr(), style: AppTextStyles.huge44, textAlign: TextAlign.center),
          Text('selectTalent'.tr(),
              textAlign: TextAlign.center, style: AppTextStyles.bold13.copyWith(color: AppColors.greyTxtColor)),
          const SizedBox(height: 32),
          Flexible(
            flex: 7,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  child: Column(
                    children: [
                      const Spacer(),
                      Expanded(
                        flex: 10,
                        child: TalentCategoryBox(
                          talent: TalentCategoryType.music,
                          title: 'musicians'.tr().toUpperCase(),
                        ),
                      ),
                      const Spacer(flex: 5),
                      Expanded(
                        flex: 10,
                        child: TalentCategoryBox(
                          talent: TalentCategoryType.filmMaking,
                          title: 'filmmakers'.tr().toUpperCase(),
                        ),
                      ),
                      const Spacer(flex: 5),
                      Expanded(
                        flex: 10,
                        child: TalentCategoryBox(
                          talent: TalentCategoryType.acting,
                          title: 'actors'.tr().toUpperCase(),
                        ),
                      ),
                      const Spacer(),
                    ],
                  ),
                ),
                const SizedBox(width: 24),
                Expanded(
                  child: Column(
                    children: [
                      const Spacer(),
                      Expanded(
                        flex: 160,
                        child: AutoSizeText(
                          'youAreNxt'.tr(),
                          maxLines: 4,
                          style: TextStyle(
                              fontSize: 108, fontFamily: AppConst.isEn ? 'BebasNeue' : 'Tajawal', height: .99),
                        ),
                      ),
                      const Spacer(),
                      GestureDetector(
                        onTap: () async {
                          final bloc = BlocProvider.of<CompleteProfileCubit>(context);
                          if (bloc.isCategorySelected) {
                            await bloc.getTalents();
                          } else {
                            PopupHelper.showAppSnack(
                              Text(
                                'noTalentSelected'.tr(),
                                textAlign: TextAlign.center,
                                style: AppTextStyles.huge30.copyWith(color: Colors.black),
                              ),
                              Colors.black,
                              Colors.white,
                              isDefaultBackGroundColor: false,
                              isShapeDefaultColor: false,
                            );
                          }
                        },
                        child: RotatedBox(
                          quarterTurns: context.locale.languageCode == "ar" ? 2 : 0,
                          child: SvgPicture.asset('assets/images/arrow.svg'),
                        ),
                      ),
                      const Spacer(),
                    ],
                  ),
                )
              ],
            ),
          ),
          const Spacer(),
        ],
      ),
    );
  }
}
