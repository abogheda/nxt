import 'package:nxt/config/router/router.dart';

import '../../base_complete_profile_screen.dart';
import '../../cubit/complete_profile_cubit.dart';
import '../stepper_widgets/user_info_screen.dart';
import 'know_more_about_you_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../imports.dart';
import '../interest_header.dart';
import '../../../../widgets/app_loader.dart';

class TalentsWidget extends StatelessWidget {
  const TalentsWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BaseCompleteProfileScreen(
      child: Column(
        children: [
          const InterestHeader(),
          const KnowMoreAboutYouText(hasBottomPadding: false),
          Expanded(
            child: BlocBuilder<CompleteProfileCubit, CompleteProfileState>(
              builder: (context, state) {
                final cubit = CompleteProfileCubit.get(context);
                if (state is TalentsFailed) {
                  return RefreshIndicator(
                      onRefresh: () async => cubit.getTalents(),
                      child: ListView(children: [Center(child: Text(state.errorMsg))]));
                }
                if (state is TalentsLoading || state is SkipProfileLoading) return const AppLoader();
                return Column(
                  children: [
                    const TalentListView(),
                    SaveAndContinueButton(
                      onPressed: state is CompleteProfilePatchLoading
                          ? () {}
                          : () async {
                              if (cubit.talents.isNotEmpty) await cubit.completeProfilePatch();
                              MagicRouter.navigateTo(BlocProvider.value(
                                value: cubit,
                                child: const UserInfoScreen(),
                              ));
                            },
                      child: state is CompleteProfilePatchLoading
                          ? const Center(child: AppLoader(color: Colors.white))
                          : null,
                    ),
                    // SaveAndContinueButton(onPressed: () => cubit.increaseCompleteProfileIndex())
                  ],
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
