// import 'package:easy_localization/easy_localization.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:nxt/utils/helpers/popup_helper.dart';
// import '../../../../../utils/constants/resources.dart';
// import '../../cubit/complete_profile_cubit.dart';
// import '../imports.dart';
// import '../interest_header.dart';
// import 'know_more_about_you_text.dart';
//
// class SpecialSkillsWidget extends StatelessWidget {
//   const SpecialSkillsWidget({Key? key}) : super(key: key);
//
//   @override
//   Widget build(BuildContext context) {
//     var cubit = CompleteProfileCubit.get(context);
//     return Column(
//       children: [
//         const InterestHeader(),
//         const KnowMoreAboutYouText(hasBottomPadding: false),
//         Container(
//           width: double.infinity,
//           color: Colors.black,
//           padding: EdgeInsets.symmetric(vertical: height * 0.012),
//           child: Column(
//             children: [
//               Text(
//                 'gotSpecialSkills'.tr().toUpperCase(),
//                 textAlign: TextAlign.center,
//                 style: Theme.of(context).textTheme.headline3!.copyWith(
//                       height: .88,
//                       color: Colors.white,
//                       fontFamily: isEn ? 'BebasNeue' : 'Tajawal',
//                     ),
//               ),
//               Text(
//                 'tellUsMoreAboutYou'.tr().toUpperCase(),
//                 style: Theme.of(context).textTheme.caption!.copyWith(
//                       fontWeight: FontWeight.bold,
//                       fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal',
//                       color: Colors.white,
//                     ),
//               ),
//             ],
//           ),
//         ),
//         // SizedBox(height: height * 0.03),
//         Expanded(
//           child: ListView(
//             shrinkWrap: true,
//             padding: EdgeInsets.only(
//                 right: width * 0.07,
//                 left: width * 0.07,
//                 top: height * 0.02,
//                 bottom: MediaQuery.of(context).viewInsets.bottom),
//             children: [
//               Text(
//                 'fillBox'.tr().toUpperCase(),
//                 style: Theme.of(context).textTheme.caption!.copyWith(
//                       fontWeight: FontWeight.bold,
//                       fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal',
//                       color: const Color(0xFFBEBEBE),
//                     ),
//               ),
//               SizedBox(height: height * 0.01),
//               TextFormField(
//                 maxLines: 12,
//                 keyboardType: TextInputType.multiline,
//                 controller: cubit.skillsController,
//                 onChanged: (_) => cubit.emitState(state: UpdateTextField()),
//                 style: TextStyle(
//                   fontSize: 12,
//                   fontWeight: FontWeight.w500,
//                   fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal',
//                 ),
//                 decoration: InputDecoration(
//                   filled: true,
//                   enabled: true,
//                   hintText: 'writeDownYourSkills'.tr(),
//                   border: const OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(8.0))),
//                   errorStyle: const TextStyle(fontFamily: 'Montserrat', fontSize: 12, height: .9),
//                   enabledBorder: const OutlineInputBorder(
//                       borderRadius: BorderRadius.all(Radius.circular(8.0)), borderSide: BorderSide(width: .75)),
//                   focusedBorder: const OutlineInputBorder(
//                       borderRadius: BorderRadius.all(Radius.circular(8.0)),
//                       borderSide: BorderSide(color: AppColors.greyTxtColor)),
//                   hintStyle: const TextStyle(fontSize: 12, color: AppColors.iconsColor, fontWeight: FontWeight.w500),
//                 ),
//               ),
//             ],
//           ),
//         ),
//         BlocBuilder<CompleteProfileCubit, CompleteProfileState>(
//           builder: (context, state) {
//             return SaveAndContinueButton(
//               onPressed: () {
//                 if (cubit.skillsController.text.isNotEmpty && cubit.skillsController.text.length < 4) {
//                   PopupHelper.showBasicSnack(msg: 'pleaseEnterValidSkills'.tr(), color: Colors.red);
//                   return;
//                 }
//                 cubit.increaseCompleteProfileIndex();
//               },
//             );
//           },
//         ), // const Spacer(),
//       ],
//     );
//   }
// }
