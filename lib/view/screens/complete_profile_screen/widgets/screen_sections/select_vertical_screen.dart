import 'package:easy_localization/easy_localization.dart' as local;
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:nxt/config/router/router.dart';
import 'package:nxt/data/service/hive_services.dart';
import '../../../../../data/enum/talent_type.dart';
import '../../../../../utils/constants/resources.dart';
import '../../../choose_your_bundle/choose_your_bundle.dart';
import '../../../otp/widgets/loader_hud.dart';
import '../../base_complete_profile_screen.dart';
import '../../cubit/complete_profile_cubit.dart';
import '../imports.dart';
import '../interest_header.dart';
import 'headline_column.dart';

import '../../../../../utils/helpers/popup_helper.dart';

class SelectVerticalScreen extends StatelessWidget {
  const SelectVerticalScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => CompleteProfileCubit(),
      child: BaseCompleteProfileScreen(
        showBackButton: false,
        showSkipButton: false,
        child: BlocBuilder<CompleteProfileCubit, CompleteProfileState>(
          builder: (context, state) {
            final cubit = CompleteProfileCubit.get(context);
            return LoaderHUD(
              inAsyncCall: state is CompleteProfilePostLoading,
              child: Padding(
                padding: EdgeInsets.only(left: width * 0.07, right: width * 0.07, bottom: height * 0.05),
                child: Column(
                  children: [
                    const InterestHeader(),
                    Expanded(
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        textDirection: TextDirection.ltr,
                        children: [
                          Expanded(
                            child: Padding(
                              padding: EdgeInsets.symmetric(vertical: height * 0.01),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Expanded(
                                      child: TalentCategoryBox(
                                    talent: TalentCategoryType.music,
                                    title: 'musicians'.tr().toUpperCase(),
                                  )),
                                  SizedBox(height: height * 0.05),
                                  Expanded(
                                      child: TalentCategoryBox(
                                    talent: TalentCategoryType.filmMaking,
                                    title: 'filmmakers'.tr().toUpperCase(),
                                  )),
                                  SizedBox(height: height * 0.05),
                                  Expanded(
                                      child: TalentCategoryBox(
                                    talent: TalentCategoryType.acting,
                                    title: 'actors'.tr().toUpperCase(),
                                  )),
                                ],
                              ),
                            ),
                          ),
                          Expanded(
                            child: GestureDetector(
                              onTap: () async {
                                if (kDebugMode) {
                                  print("index : ${cubit.completeProfileIndex}");
                                }
                                if (cubit.isCategorySelected) {
                                  cubit.skipAllButtonOffStage = false;
                                  cubit.updateIsActing();
                                  cubit.increaseCompleteProfileIndex();
                                  await cubit.completeProfilePost();
                                  HiveHelper.cacheFromHome(fromHome: false);
                                  MagicRouter.navigateTo(BlocProvider.value(
                                    value: cubit,
                                    child: const ChooseYourBundle(fromHome: false,),
                                  ));
                                } else if (cubit.verticalCategories.isEmpty) {
                                  PopupHelper.showBasicSnack(msg: 'select_talent'.tr(), color: Colors.red);
                                }
                              },
                              child: Column(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  const HeadlineColumn(),
                                  RotatedBox(
                                    quarterTurns: context.locale.languageCode == "ar" ? 2 : 0,
                                    child: SvgPicture.asset('assets/images/arrow.svg'),
                                  ),
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
