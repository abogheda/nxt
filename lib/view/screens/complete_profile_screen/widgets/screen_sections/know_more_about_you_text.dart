import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import '../../../../../utils/constants/resources.dart';

class KnowMoreAboutYouText extends StatelessWidget {
  const KnowMoreAboutYouText({Key? key, this.hasBottomPadding = true}) : super(key: key);
  final bool hasBottomPadding;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(color: Colors.black, width: double.infinity, height: 1.5),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 8),
          child: Text(
            'knowMoreAboutYou'.tr().toUpperCase(),
            style: AppTextStyles.bold13,
          ),
        ),
        Container(
          color: Colors.black,
          width: double.infinity,
          height: 1.5,
        ),
        SizedBox(height: hasBottomPadding ? height * 0.03 : 0.0),
      ],
    );
  }
}
