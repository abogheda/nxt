import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

import '../../../../utils/constants/app_colors.dart';
import '../../../../utils/constants/app_const.dart';
import '../../../../utils/constants/app_text_style.dart';

class InterestHeader extends StatelessWidget {
  const InterestHeader({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.symmetric(horizontal: width * 0.015),
          child: Text(
            'whatIsYourTalent'.tr(),
            style: Theme.of(context)
                .textTheme
                .headlineMedium!
                .copyWith(color: Colors.black, fontFamily: AppConst.isEn ? 'BebasNeue' : 'Tajawal'),
            // style: AppTextStyles.huge44.copyWith(fontFamily: AppConst.isEn ? 'BebasNeue' : 'Tajawal'),
            textAlign: TextAlign.center,
          ),
        ),
        const SizedBox(height: 4),
        Text(
          'selectTalent',
          style: AppTextStyles.bold12
              .copyWith(color: AppColors.greyTxtColor, fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal'),
          textAlign: TextAlign.center,
        ).tr(),
        SizedBox(height: height * 0.04),
      ],
    );
  }
}
