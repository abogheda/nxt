import 'package:flutter/material.dart';
import '../../../../utils/constants/app_const.dart';

class CurrentPoint extends StatelessWidget {
  const CurrentPoint({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width * 0.085,
      height: width * 0.085,
      alignment: Alignment.center,
      decoration: BoxDecoration(shape: BoxShape.circle, border: Border.all(width: 3)),
      child: Container(
          width: width * 0.03,
          height: width * 0.03,
          decoration: const BoxDecoration(shape: BoxShape.circle, color: Colors.black)),
    );
  }
}
