import 'package:easy_localization/easy_localization.dart' as local;
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nxt/config/router/router.dart';
import 'package:nxt/view/screens/complete_profile_screen/under_line_text_button.dart';
import 'package:nxt/view/screens/complete_profile_screen/widgets/stepper_widgets/complete_profile_header.dart';
import '../../../../../utils/constants/resources.dart';

import '../../widgets/logo_app_bar.dart';
import '../navigation_and_appbar/import_widget.dart';
import 'complete_profile_loading_widget.dart';
import 'cubit/complete_profile_cubit.dart';

class BaseCompleteProfileScreen extends StatelessWidget {
  const BaseCompleteProfileScreen(
      {Key? key,
      required this.child,
      this.showBackButton = true,
      this.showSkipButton = true,
      this.showHeader = false,
      this.sizedBoxHeight})
      : super(key: key);
  final Widget child;
  final bool showBackButton;
  final bool showSkipButton;
  final bool showHeader;
  final double? sizedBoxHeight;
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CompleteProfileCubit, CompleteProfileState>(
      builder: (context, state) {
        final cubit = CompleteProfileCubit.get(context);
        return Scaffold(
          body: Column(
            children: [
              Stack(
                children: [
                  const LogoAppBar(),
                  SizedBox(height: height * 0.05),
                  showBackButton
                      ? Row(
                          children: [
                            UnderLineTextButton(
                              text: 'back'.tr(),
                              offstage: cubit.completeProfileIndex == 0 || state is UploadLoading,
                              onPressed: () => MagicRouter.pop(),
                            ),
                            const Spacer(),
                            Offstage(
                              offstage: !showSkipButton,
                              child: cubit.showSkipLoadingIndicator
                                  ? const CompleteProfileLoadingWidget()
                                  : UnderLineTextButton(
                                      text: 'skip'.tr(),
                                      offstage: !showSkipButton,
                                      onPressed: () async {
                                        cubit.showSkipLoadingIndicator = true;
                                        cubit.emitState(state: ChangeScreenState());
                                        await cubit.skipProfile();
                                        MagicRouter.navigateAndPopAll(Navigation());
                                      },
                                    ),
                            ),
                          ],
                        )
                      : const SizedBox()
                ],
              ),
              SizedBox(height: sizedBoxHeight ?? height * 0.025),
              showHeader ? const CompleteProfileHeader() : const SizedBox(),
              Expanded(child: child),
            ],
          ),
        );
      },
    );
  }
}
