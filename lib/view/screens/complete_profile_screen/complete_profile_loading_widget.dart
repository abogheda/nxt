import 'package:flutter/material.dart';
import '../../../utils/constants/resources.dart';

class CompleteProfileLoadingWidget extends StatelessWidget {
  const CompleteProfileLoadingWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsetsDirectional.only(top: MediaQuery.of(context).padding.top + height * 0.02, end: width * 0.03),
      child: const Center(
        heightFactor: 1,
        widthFactor: 1,
        child: SizedBox(
          height: 16,
          width: 16,
          child: CircularProgressIndicator.adaptive(strokeWidth: 2),
        ),
      ),
    );
  }
}
