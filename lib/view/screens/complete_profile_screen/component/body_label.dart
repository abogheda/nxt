import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

import '../../../../utils/constants/resources.dart';

class BodyLabel extends StatelessWidget {
  const BodyLabel({Key? key, required this.text, this.customHeight}) : super(key: key);
  final String text;
  final double? customHeight;
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: customHeight ?? height * 0.01),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: Text(
            text.tr().toUpperCase(),
            style: AppTextStyles.bold13,
          ),
        ),
      ],
    );
  }
}
