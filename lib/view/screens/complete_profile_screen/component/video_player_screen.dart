import 'dart:io';

import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';

class VideoPlayerScreen extends StatefulWidget {
  const VideoPlayerScreen({Key? key, required this.videoPath}) : super(key: key);
  final File videoPath;

  @override
  State<VideoPlayerScreen> createState() => _VideoPlayerScreenState();
}

class _VideoPlayerScreenState extends State<VideoPlayerScreen> {
  late VideoPlayerController _controller;
  late Future<void> _initializeVideoPlayerFuture;
  late IconData icon;
  @override
  void initState() {
    _controller = VideoPlayerController.file(widget.videoPath);
    _initializeVideoPlayerFuture = _controller.initialize();
    _controller.setVolume(50.0);
    _controller.play();
    icon = Icons.pause;
    super.initState();
  }

  @override
  void dispose() {
    _controller.pause();
    _controller.setVolume(0.0);
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      backgroundColor: Colors.black,
      body: FutureBuilder(
        future: _initializeVideoPlayerFuture,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            return Center(
              child: AspectRatio(
                aspectRatio: _controller.value.aspectRatio,
                child: VideoPlayer(_controller),
              ),
            );
          } else {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          setState(() {
            if (_controller.value.isPlaying) {
              icon = Icons.play_arrow;
              _controller.pause();
            } else {
              _controller.play();
              icon = Icons.pause;
            }
          });
        },
        child: Icon(icon),
      ),
    );
  }
}
