// // import 'package:easy_localization/easy_localization.dart';
// // import 'package:flutter/material.dart';
// // import 'package:flutter_bloc/flutter_bloc.dart';
// // import 'package:nxt/view/screens/complete_profile_screen/widgets/imports.dart';
// // import '../../../utils/helpers/update_dialog.dart';
// // import '../choose_your_bundle/choose_your_bundle.dart';
// // import '../choose_your_payment/choose_your_payment.dart';
// // import 'under_line_text_button.dart';
// // import '../../../data/service/hive_services.dart';
// // import '../../../utils/constants/resources.dart';
// // import '../../widgets/logo_app_bar.dart';
// // import '../sign_up/talent/controller/talent_sign_up_cubit.dart';
// // import '../sign_up/talent/talent_invitation_screen.dart';
// // import 'cubit/complete_profile_cubit.dart';
// // import 'widgets/screen_sections/select_vertical_screen.dart';
// // import 'widgets/screen_sections/talents_widget.dart';
// //
// // class CompleteProfileScreen extends StatefulWidget {
// //   const CompleteProfileScreen({Key? key}) : super(key: key);
// //
// //   @override
// //   State<CompleteProfileScreen> createState() => _CompleteProfileScreenState();
// // }
// //
// // class _CompleteProfileScreenState extends State<CompleteProfileScreen> {
// //   @override
// //   void initState() {
// //     checkNewVersion(context);
// //     super.initState();
// //   }
// //
// //   @override
// //   Widget build(BuildContext context) {
// //     // try {
// //     //   final user = HiveHelper.getUserInfo!.user!;
// //     //   if ((user.invitation ?? false) == false) {
// //     //     return BlocProvider(
// //     //       create: (context) => TalentSignUpCubit(),
// //     //       child: const TalentInvitationScreen(isFirstTime: false),
// //     //     );
// //     //   }
// //     // } catch (e) {
// //     //   debugPrint(e.toString());
// //     // }
// //     return BlocProvider(
// //       create: (context) => CompleteProfileCubit(),
// //       child: BlocBuilder<CompleteProfileCubit, CompleteProfileState>(
// //         builder: (context, state) {
// //           final cubit = CompleteProfileCubit.get(context);
// //           return Scaffold(
// //             body: Column(
// //               children: [
// //                 Stack(
// //                   children: [
// //                     const LogoAppBar(),
// //                     SizedBox(height: widget.height * 0.05),
// //                     Row(
// //                       children: [
// //                         UnderLineTextButton(
// //                           text: 'back'.tr(),
// //                           offstage: cubit.completeProfileIndex == 0 || state is UploadLoading,
// //                           onPressed: () {
// //                             if (cubit.stepperIndex != 0) {
// //                               cubit.stepperIndex--;
// //                               cubit.emitState(state: ChangeScreenState());
// //                               return;
// //                             }
// //                             if (cubit.completeProfileIndex != 0) {
// //                               cubit.completeProfileIndex--;
// //                               cubit.emitState(state: ChangeScreenState());
// //                             } else {
// //                               cubit.skipAllButtonOffStage = true;
// //                               cubit.emitState(state: ChangeScreenState());
// //                             }
// //                           },
// //                         ),
// //                         const Spacer(),
// //                         // Offstage(
// //                         //   offstage: cubit.skipAllButtonOffStage,
// //                         //   child: cubit.showSkipLoadingIndicator
// //                         //       ? const CompleteProfileLoadingWidget()
// //                         //       : UnderLineTextButton(
// //                         //           text: 'skipAll'.tr(),
// //                         //           offstage: cubit.completeProfileIndex == 0,
// //                         //           onPressed: () async {
// //                         //             cubit.showSkipLoadingIndicator = true;
// //                         //             cubit.emitState(state: ChangeScreenState());
// //                         //             await cubit.skipProfile();
// //                         //             MagicRouter.navigateAndPopAll(Navigation());
// //                         //           },
// //                         //         ),
// //                         // ),
// //                       ],
// //                     )
// //                   ],
// //                 ),
// //                 SizedBox(height: widget.height * 0.025),
// //                 Expanded(
// //                   child: BlocBuilder<CompleteProfileCubit, CompleteProfileState>(
// //                     builder: (_, state) {
// //                       if (cubit.completeProfileIndex == 0) {
// //                         return const SelectVerticalScreen();
// //                       } else if (cubit.completeProfileIndex == 1) {
// //                         return const ChooseYourBundle();
// //                       } else if (cubit.completeProfileIndex == 2) {
// //                         return const ChooseYourPayment();
// //                       } else if (cubit.completeProfileIndex == 3) {
// //                         return const TalentsWidget();
// //                       } else if (cubit.completeProfileIndex == 4) {
// //                         return const StepperForm();
// //                       }
// //                       return Text(
// //                         'whatIsYourTalent'.tr(),
// //                         textAlign: TextAlign.center,
// //                         style: AppTextStyles.huge44,
// //                       );
// //                     },
// //                   ),
// //                 ),
// //               ],
// //             ),
// //           );
// //         },
// //       ),
// //     );
// //   }
// // }
// import 'package:easy_localization/easy_localization.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:nxt/view/screens/complete_profile_screen/widgets/imports.dart';
//
// import '../../../data/service/hive_services.dart';
// import '../../../utils/constants/resources.dart';
// import '../../../utils/helpers/update_dialog.dart';
// import '../../widgets/logo_app_bar.dart';
// import '../sign_up/talent/controller/talent_sign_up_cubit.dart';
// import '../sign_up/talent/talent_invitation_screen.dart';
// import 'cubit/complete_profile_cubit.dart';
// import 'under_line_text_button.dart';
// import 'widgets/screen_sections/select_vertical_screen.dart';
// import 'widgets/screen_sections/special_skills_widget.dart';
// import 'widgets/screen_sections/talents_widget.dart';
//
// class CompleteProfileScreen extends StatefulWidget {
//   const CompleteProfileScreen({Key? key}) : super(key: key);
//
//   @override
//   State<CompleteProfileScreen> createState() => _CompleteProfileScreenState();
// }
//
// class _CompleteProfileScreenState extends State<CompleteProfileScreen> {
//   @override
//   void initState() {
//     checkNewVersion(context);
//     super.initState();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     try {
//       final user = HiveHelper.getUserInfo!.user!;
//       if ((user.invitation ?? false) == false) {
//         return BlocProvider(
//           create: (context) => TalentSignUpCubit(),
//           child: const TalentInvitationScreen(isFirstTime: false),
//         );
//       }
//     } catch (e) {
//       debugPrint(e.toString());
//     }
//     return BlocProvider(
//       create: (context) => CompleteProfileCubit(),
//       child: BlocBuilder<CompleteProfileCubit, CompleteProfileState>(
//         builder: (context, state) {
//           final cubit = CompleteProfileCubit.get(context);
//           return Scaffold(
//             body: Column(
//               children: [
//                 Stack(
//                   children: [
//                     const LogoAppBar(),
//                     SizedBox(height: widget.height * 0.05),
//                     Row(
//                       children: [
//                         UnderLineTextButton(
//                           text: 'back'.tr(),
//                           offstage: cubit.completeProfileIndex == 0 || state is UploadLoading,
//                           onPressed: () {
//                             if (cubit.stepperIndex != 0) {
//                               cubit.stepperIndex--;
//                               cubit.emitState(state: ChangeScreenState());
//                               return;
//                             }
//                             if (cubit.completeProfileIndex != 0) {
//                               cubit.completeProfileIndex--;
//                               cubit.emitState(state: ChangeScreenState());
//                             } else {
//                               cubit.skipAllButtonOffStage = true;
//                               cubit.emitState(state: ChangeScreenState());
//                             }
//                           },
//                         ),
//                         const Spacer(),
//                         // Offstage(
//                         //   offstage: cubit.skipAllButtonOffStage,
//                         //   child: cubit.showSkipLoadingIndicator
//                         //       ? const CompleteProfileLoadingWidget()
//                         //       : UnderLineTextButton(
//                         //           text: 'skipAll'.tr(),
//                         //           offstage: cubit.completeProfileIndex == 0,
//                         //           onPressed: () async {
//                         //             cubit.showSkipLoadingIndicator = true;
//                         //             cubit.emitState(state: ChangeScreenState());
//                         //             await cubit.skipProfile();
//                         //             MagicRouter.navigateAndPopAll(Navigation());
//                         //           },
//                         //         ),
//                         // ),
//                       ],
//                     )
//                   ],
//                 ),
//                 SizedBox(height: widget.height * 0.025),
//                 Expanded(
//                   child: BlocBuilder<CompleteProfileCubit, CompleteProfileState>(
//                     builder: (_, state) {
//                       if (cubit.completeProfileIndex == 0) {
//                         return const SelectVerticalScreen();
//                       } else if (cubit.completeProfileIndex == 1) {
//                         return const TalentsWidget();
//                       } else if (cubit.completeProfileIndex == 2) {
//                         return const SpecialSkillsWidget();
//                       } else if (cubit.completeProfileIndex == 3) {
//                         return const StepperForm();
//                       }
//                       return Text(
//                         'whatIsYourTalent'.tr(),
//                         textAlign: TextAlign.center,
//                         style: AppTextStyles.huge44,
//                       );
//                     },
//                   ),
//                 ),
//               ],
//             ),
//           );
//         },
//       ),
//     );
//   }
// }
