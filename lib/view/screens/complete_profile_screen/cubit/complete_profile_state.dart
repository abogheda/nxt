part of 'complete_profile_cubit.dart';

@immutable
abstract class CompleteProfileState {}

class CompleteProfileInitial extends CompleteProfileState {}

class ChangeScreenState extends CompleteProfileState {}

class UpdateTextField extends CompleteProfileState {}

class UpdateIsActingState extends CompleteProfileState {}

class UpdateUserPhysicalState extends CompleteProfileState {}

class UpdateShotsState extends CompleteProfileState {}

class ChangeStepperScreenState extends CompleteProfileState {}
//
// class SkillsUpdateAndCannotContinue extends CompleteProfileState {}

// class SaveAndContinueBtnState extends CompleteProfileState {
//   final DateTime time;
//   final bool canContinue;
//   SaveAndContinueBtnState({required this.time, required this.canContinue});
// }

abstract class TalentsState extends CompleteProfileState {}

class TalentsLoading extends TalentsState {}

class TalentsSuccess extends TalentsState {
  // final List<TalentModel> talents;
  // const TalentsSuccess(this.talents);
}

class TalentsFailed extends TalentsState {
  final String errorMsg;
  TalentsFailed(this.errorMsg);
}

// class SpecialSkillsState extends CompleteProfileState {}

abstract class UploadingState extends CompleteProfileState {
  UploadingState();
}

class UploadLoading extends UploadingState {}

class UploadSuccess extends UploadingState {}

class UploadingFailed extends UploadingState {
  final String errorMsg;
  UploadingFailed(this.errorMsg);
}

class CompleteProfilePatchLoading extends UploadingState {}

class CompleteProfilePatchSuccess extends UploadingState {}

class CompleteProfilePatchFailed extends UploadingState {
  final String errorMsg;
  CompleteProfilePatchFailed(this.errorMsg);
}

class CompleteProfilePostLoading extends UploadingState {}

class CompleteProfilePostSuccess extends UploadingState {}

class CompleteProfilePostFailed extends UploadingState {
  final String errorMsg;
  CompleteProfilePostFailed(this.errorMsg);
}

class SkipProfileLoading extends CompleteProfileState {}

class SkipProfileSuccess extends CompleteProfileState {}

class SkipProfileError extends CompleteProfileState {
  final String error;
  SkipProfileError({required this.error});
}

class AssignCountryName extends CompleteProfileState {}
