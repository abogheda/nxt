// ignore_for_file: depend_on_referenced_packages, unnecessary_import
import 'dart:async';
import 'dart:io';
import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';
import '../../../../config/router/router.dart';
import '../../../../data/enum/talent_type.dart';
import '../../../../data/models/talent_model.dart';
import '../../../../data/models/user_models/complete_prof_model.dart';
import '../../../../data/models/user_models/job_experience_model.dart';
import '../../../../data/repos/imports.dart';
import '../../../../data/service/hive_services.dart';
import '../../congratulation/congratulation_screen.dart';

part 'complete_profile_state.dart';

class CompleteProfileCubit extends Cubit<CompleteProfileState> {
  CompleteProfileCubit() : super(CompleteProfileInitial());
  static CompleteProfileCubit get(context) => BlocProvider.of(context);
  bool skipAllButtonOffStage = true;
  bool showSkipLoadingIndicator = false;
  bool isUploadingReelsFinished = false;
  bool isUploadingShotsFinished = false;
  bool nextStepButtonLoading = false;
  int stepperIndex = 0;
  int completeProfileIndex = 0;
  List<String> verticalCategories = [];
  int? uploadingReelFileNumber;
  double reelUploadingProgress = 0;
  StreamController<double> reelStreamController = StreamController<double>();
  int? uploadingShotFileNumber;
  double shotsUploadingProgress = 0;
  StreamController<double> shotsStreamController = StreamController<double>();
  bool get isCategorySelected => verticalCategories.isNotEmpty;
  List<String> talents = [];
  List<TalentModel>? talentsList;
  final List<String> _shotsPaths = List.filled(3, "");
  List get shots => [..._shotsPaths];
  bool get isAllShotsSelected => !_shotsPaths.any((e) => e.isEmpty);
  void setShotPath(String path, int index) => _shotsPaths[index] = path;
  TextEditingController
      // skillsController = TextEditingController(),
      heightController = TextEditingController(),
      weightController = TextEditingController(),
      // portfolioController = TextEditingController(),
      nationalityController = TextEditingController();
  FocusNode heightFocusNode = FocusNode();
  //==== stepper attributes ====
  late bool isActing = verticalCategories.contains(TalentCategoryType.acting.id);
  void updateIsActing() {
    isActing = verticalCategories.contains(TalentCategoryType.acting.id);
    model = CompleteProfileModel();
    emit(UpdateIsActingState());
  }

  bool doOnce = true;
  final userInfoFormKey = GlobalKey<FormState>(), userInfoBodyFormKey = GlobalKey<FormState>();
  // final portfolioFormKey = GlobalKey<FormState>();
  // final processes = {
  //   'physical': const UserPhysicalScreen(),
  //   'look': const UserBodyInfoScreen(),
  //   'experience': const UserPortfolio(),
  //   'reels': const UserReels(),
  //   'shots': const UserShots(),
  // };
  // late List<String> processKeysList = processes.keys.toList();
  // late List processValuesList = processes.values.toList();
  // final miniProcesses = {
  //   'experience': const UserPortfolio(),
  //   'reels': const UserReels(),
  // };
  // late List<String> miniProcessKeysList = miniProcesses.keys.toList();
  // late List miniProcessValuesList = miniProcesses.values.toList();
  bool isContinue = true;
  Future<void> onFinished() async {
    if (isContinue) {
      isContinue = false;
      await completeProfilePost();
      MagicRouter.navigateAndPopAll(const CongratulationScreen());
    }
  }

  String? gender;
  String? skin;
  String? hair;
  String? eye;
  String? countryOfResidence;
  CompleteProfileModel model = CompleteProfileModel();
  final List<File?> _reelsAll = [];
  final List<File> _reels = [];
  final List<File> _reelsThumb = [];

  List<File?> get reelsAll => [..._reelsAll];
  List<File> get reels => [..._reels];
  List<File> get reelsThumb => [..._reelsThumb];

  void addReelAll(File? file) => _reelsAll.add(file);
  void addReel(File file) => _reels.add(file);
  void addReelThumb(File file) => _reelsThumb.add(file);

  void removeReelAll(File? file) => _reelsAll.remove(file);
  void removeReel(File file) => _reels.remove(file);
  void removeReelThumb(File file) => _reelsThumb.remove(file);

  final List<JobExperienceModel> _jobExperiences = [];
  List<JobExperienceModel> get jobExperiences => _jobExperiences;

  void emitState({required CompleteProfileState state}) => emit(state);

  void assignCountryOfResidence({required String countryName}) {
    countryOfResidence = countryName;
    emit(AssignCountryName());
  }

  void increaseCompleteProfileIndex() {
    completeProfileIndex++;
    emit(ChangeScreenState());
  }

  // Future<void> uploadShots() async {
  //   emit(UploadLoading());
  //   final res =
  //       await GeneralRepo.uploadMedia(_shotsPaths.map((e) => File(e)).toList(), onProgress: (fileNum, progress, total) {
  //     uploadingShotFileNumber = fileNum + 1;
  //     shotsUploadingProgress = progress / total;
  //     shotsStreamController.sink.add(shotsUploadingProgress);
  //     if (uploadingReelFileNumber == _shotsPaths.length && shotsUploadingProgress == 1.0) {
  //       isUploadingShotsFinished = true;
  //       shotsStreamController.close();
  //       shotsStreamController = StreamController<double>();
  //     }
  //   });
  //   res.fold(
  //     (urls) {
  //       nextStepButtonLoading = true;
  //       _shotsPaths.setAll(0, urls);
  //       emit(FinishCompleteProfileLoading());
  //     },
  //     (errorMsg) => emit(UploadingFailed(errorMsg)),
  //   );
  // }

  // Future<void> uploadReels() async {
  //   emit(UploadLoading());
  //   final res = await GeneralRepo.uploadMedia(
  //     _reels,
  //     onProgress: (fileNum, progress, total) {
  //       uploadingReelFileNumber = fileNum + 1;
  //       reelUploadingProgress = progress / total;
  //       reelStreamController.sink.add(reelUploadingProgress);
  //       if (uploadingReelFileNumber == _reels.length && reelUploadingProgress == 1.0) {
  //         isUploadingReelsFinished = true;
  //         reelStreamController.close();
  //         reelStreamController = StreamController<double>();
  //       }
  //     },
  //   );
  //   res.fold(
  //     (urls) async {
  //       for (var url in urls) {
  //         await MediaRepo.uploadReels(url: url);
  //       }
  //       emit(FinishCompleteProfileLoading());
  //     },
  //     (errorMsg) => emit(UploadingFailed(errorMsg)),
  //   );
  // }

  Future<void> getTalents() async {
    talents.clear();
    verticalCategories = verticalCategories.toSet().toList();
    String categoriesIds = verticalCategories[0];
    if (verticalCategories.length == 2) {
      categoriesIds = '$categoriesIds%2C${verticalCategories[1]}';
    }
    if (verticalCategories.length == 3) {
      categoriesIds = '$categoriesIds%2C${verticalCategories[1]}%2C${verticalCategories[2]}';
    }
    emit(TalentsLoading());
    final res = await ProfileRepo.getTalents(categories: categoriesIds);
    if (res.data != null) {
      talentsList = res.data!;
      emit(TalentsSuccess());
    } else {
      emit(TalentsFailed(res.message ?? ""));
    }
  }

  void assignModel() {
    model = CompleteProfileModel(
      categories: verticalCategories,
      talents: talents,
      gender: gender,
      weight: double.tryParse(weightController.text.trim()),
      height: double.tryParse(heightController.text.trim()),
      skinColor: skin,
      hairColor: hair,
      eyeColor: eye,
      country: countryOfResidence,
      nationality: nationalityController.text.trim(),
    );
  }

  Future<void> skipProfile() async {
    emit(SkipProfileLoading());
    final CompleteProfileModel skipModel = CompleteProfileModel(categories: verticalCategories);
    final res = await ProfileRepo.completeProfilePost(skipModel);
    if (res.data != null) {
      final hiveUser = HiveHelper.getUserInfo!;
      if (res.data!.profileStatus != null) {
        await HiveHelper.cacheUserModel(
          userModel: hiveUser.copyWith(
            user: hiveUser.user!.copyWith(profileStatus: res.data!.profileStatus!.status),
          ),
        );
      }
      await ProfileRepo.skipCompleteProfile();
      emit(SkipProfileSuccess());
    } else {
      emit(SkipProfileError(error: res.message ?? ""));
    }
  }

  Future<void> completeProfilePatch() async {
    emit(CompleteProfilePatchLoading());
    assignModel();
    final res = await ProfileRepo.completeProfilePatch(model);
    if (res.data != null) {
      emit(CompleteProfilePatchSuccess());
    } else {
      emit(CompleteProfilePatchFailed(res.message ?? ""));
    }
  }

  Future<void> completeProfilePost() async {
    emit(CompleteProfilePostLoading());
    assignModel();
    final res = await ProfileRepo.completeProfilePost(model);
    if (res.data != null) {
      HiveHelper.cacheVerticals(length: model.categories?.length ?? 0);
      emit(CompleteProfilePostSuccess());
    } else {
      emit(CompleteProfilePostFailed(res.message ?? ""));
    }
  }

  @override
  Future<void> close() {
    // skillsController.dispose();
    return super.close();
  }
}
