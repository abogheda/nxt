// ignore_for_file: library_private_types_in_public_api, file_names

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:nxt/view/widgets/custom_button.dart';
import '../../../config/router/router.dart';
import '../../../data/repos/imports.dart';
import '../../../data/service/hive_services.dart';
import '../../../utils/helpers/popup_helper.dart';
import '../complete_profile_screen/cubit/complete_profile_cubit.dart';
import '../complete_profile_screen/widgets/screen_sections/talents_widget.dart';
import '../congratulation/congratulation_screen.dart';
import '../navigation_and_appbar/import_widget.dart';
import 'PaymentOlineData.dart';

// ignore: must_be_immutable
class PaymentOnline extends StatefulWidget {
  final String url;
  const PaymentOnline({
    Key? key,
    required this.url,
  }) : super(key: key);

  @override
  _PaymentOnlineState createState() => _PaymentOnlineState();
}

class _PaymentOnlineState extends State<PaymentOnline> with PaymentOnlineData {
  @override
  void initState() {
    flutterWebviewPlugin.close();
    // onChangeUrl(context);
    super.initState();
  }

  @override
  void dispose() {
    urlState.cancel();
    flutterWebviewPlugin.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CompleteProfileCubit, CompleteProfileState>(
  builder: (context, state) {
    final cubit = CompleteProfileCubit.get(context);
    return WebviewScaffold(
      key: scaffold,
      withZoom: true,
      url: widget.url,
      debuggingEnabled: true,
      persistentFooterButtons: [
        CustomButton(
            onTap: () async{
              final res = await ProfileRepo.getUserValidate();
              if (res.data != null) {
                await HiveHelper.cacheUserModel(userModel: HiveHelper.getUserInfo!.copyWith(user: res.data));
                if(res.data?.subscriptionEnd == false){
                  flutterWebviewPlugin.close();
                  // print("data : ${cubit..getTalents()..eye..toString()}");
                  MagicRouter.navigateTo(
                    CongratulationScreen(
                      onPressed: () async =>  HiveHelper.fromHome == false ?

                      MagicRouter.navigateTo(
                              BlocProvider.value(
                                value: cubit..getTalents(),
                                child: const TalentsWidget(),
                              ),
                          ):MagicRouter.navigateAndPopAll(Navigation()),
                    ),
                  );
                }
                else{
                  MagicRouter.pop();
                  PopupHelper.showBasicSnack(msg: "something error, please try again", color: Colors.red);

                }
              }

            },
            child: const Text("back to app")),
      ],
    );
  },
);
  }
}
