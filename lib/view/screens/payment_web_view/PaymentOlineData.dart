// ignore_for_file: file_names

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:nxt/view/screens/choose_your_payment/cubits/create_order/create_order_cubit.dart';

import '../complete_profile_screen/cubit/complete_profile_cubit.dart';

class PaymentOnlineData {
  final CreateOrderCubit createOrderCubit = CreateOrderCubit();
  final CompleteProfileCubit completeProfileCubit = CompleteProfileCubit();
  late StreamSubscription<String> urlState;
  final FlutterWebviewPlugin flutterWebviewPlugin = FlutterWebviewPlugin();
  final GlobalKey<ScaffoldState> scaffold = GlobalKey();

  onChangeUrl(BuildContext context) {
    urlState = flutterWebviewPlugin.onUrlChanged.listen((String url) {
      // if ("https://projectnxt.app/signup/?redirect=%2Fpayment%2F".contains("projectnxt")) {

      // } else if (url.contains('Fail')) {
      //   flutterWebviewPlugin.close();
      //   Navigator.pop(context);
      // }
    });
  }
}
