import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../utils/constants/app_const.dart';
import '../../widgets/no_state_widget.dart';
import '../more/cubit/more_cubit.dart';
import 'widgets/my_job_card.dart';
import '../../widgets/error_state_widget.dart';
import '../../widgets/loading_state_widget.dart';
import '../navigation_and_appbar/import_widget.dart';

class MyJobsScreen extends StatelessWidget {
  const MyJobsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomScaffoldWidget(
      extendBodyBehindAppBar: false,
      isBackArrow: true,
      isThereLogo: true,
      isThereActions: true,
      body: BlocBuilder<MoreCubit, MoreState>(
        builder: (context, state) {
          final cubit = MoreCubit.get(context);
          if (state is GetMyJobsFailed) {
            return ErrorStateWidget(
              hasRefresh: true,
              onRefresh: () async => await cubit.getMyJobs(),
            );
          } else if (state is GetMyJobsLoading) {
            return const LoadingStateWidget();
          }
          final myJobsList = cubit.myJobsList ?? [];
          return RefreshIndicator(
            onRefresh: () async => await cubit.getMyJobs(),
            child: ListView(
              children: [
                Container(color: Colors.black, width: double.infinity, height: 1.5),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 24),
                  child: Text(
                    'myJobs'.tr(),
                    textAlign: TextAlign.center,
                    style: Theme.of(context)
                        .textTheme
                        .headline3!
                        .copyWith(fontFamily: isEn ? 'BebasNeue' : 'Tajawal', color: Colors.black),
                  ),
                ),
                if (myJobsList.isEmpty)
                  SizedBox(height: height * 0.5, child: NoStateWidget())
                else
                  ListView.builder(
                    itemCount: myJobsList.length,
                    shrinkWrap: true,
                    padding: const EdgeInsets.all(10),
                    physics: const ClampingScrollPhysics(),
                    itemBuilder: (context, index) {
                      final myJob = myJobsList[index]!.job!;
                      return MyJobCard(
                        id: myJob.id!,
                        name: myJob.organization!.name,
                        color: Colors.transparent.value.toString(),
                        logo: myJob.organization!.logo,
                        title: isEn ? myJob.lang![0]!.title : myJob.lang![1]!.title,
                        createdAt: myJob.createdAt,
                      );
                    },
                  )
              ],
            ),
          );
        },
      ),
    );
  }
}
