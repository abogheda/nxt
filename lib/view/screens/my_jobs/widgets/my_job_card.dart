// ignore_for_file: depend_on_referenced_packages
import 'package:easy_localization/easy_localization.dart' as local;
import 'package:flutter/material.dart';
import 'package:nxt/utils/constants/app_const.dart';
import 'package:timeago/timeago.dart' as timeago;
import 'package:transparent_image/transparent_image.dart';
import '../../../../../../utils/constants/app_colors.dart';
import '../../../../config/router/router.dart';
import '../../../../utils/constants/app_images.dart';
import '../../connect/Connect_screens/experts_and_jobs/screens/job_screen.dart';

const colorRadius = Radius.circular(10);
final borderRadius = BorderRadius.circular(10);

class MyJobCard extends StatefulWidget {
  final String? color;
  final String? logo;
  final String id;
  final String? title;
  final String? name;
  final String? createdAt;
  final double? elevation;
  final EdgeInsetsGeometry? padding;
  final Function()? onPressed;
  const MyJobCard({
    Key? key,
    required this.color,
    required this.logo,
    required this.title,
    required this.name,
    required this.createdAt,
    required this.id,
    this.padding,
    this.elevation,
    this.onPressed,
  }) : super(key: key);
  @override
  State<MyJobCard> createState() => _MyJobCardState();
}

class _MyJobCardState extends State<MyJobCard> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: widget.padding ?? const EdgeInsets.only(bottom: 10.0),
      decoration: BoxDecoration(borderRadius: borderRadius, border: Border.all(color: Colors.black, width: 0.2)),
      child: InkWell(
        radius: 10,
        onTap: widget.onPressed ?? () => MagicRouter.navigateTo(JobScreen(jobId: widget.id)),
        child: Material(
          elevation: widget.elevation ?? 2,
          borderRadius: borderRadius,
          child: IntrinsicHeight(
            child: Stack(
              children: [
                Container(
                  width: 10,
                  decoration: BoxDecoration(
                    color: widget.color == null ? AppColors.redColor : Color(widget.color!.toHex()),
                    borderRadius: BorderRadius.only(
                      topLeft: widget.isEn ? colorRadius : Radius.zero,
                      bottomLeft: widget.isEn ? colorRadius : Radius.zero,
                      topRight: widget.isEn ? Radius.zero : colorRadius,
                      bottomRight: widget.isEn ? Radius.zero : colorRadius,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: EdgeInsetsDirectional.only(start: 16, end: widget.width * 0.006),
                        child: Row(
                          children: [
                            SizedBox(
                              width: 36,
                              height: 36,
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(50),
                                child: FadeInImage.memoryNetwork(
                                  image: widget.logo ?? userAvatarPlaceHolderUrl,
                                  fit: BoxFit.cover,
                                  placeholder: kTransparentImage,
                                ),
                              ),
                            ),
                            SizedBox(width: widget.width * 0.015),
                            SizedBox(
                              width: widget.width * 0.55,
                              height: widget.height * 0.06,
                              child: Align(
                                alignment: AppConst.isEn ? Alignment.centerLeft : Alignment.centerRight,
                                child: Text(
                                  widget.title ?? "notAvailable".tr(),
                                  maxLines: 2,
                                  textAlign: TextAlign.start,
                                  overflow: TextOverflow.ellipsis,
                                  style: Theme.of(context).textTheme.headline6!.copyWith(
                                        fontFamily: AppConst.isEn ? 'BebasNeue' : 'Tajawal',
                                        fontWeight: FontWeight.w500,
                                      ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsetsDirectional.only(start: widget.width * 0.15),
                        child: Text(
                          widget.name ?? "notAvailable".tr(),
                          style: Theme.of(context).textTheme.bodyText2!.copyWith(
                                color: const Color(0xFF555555),
                                fontFamily: widget.isEn ? 'Inter-Medium' : 'Tajawal',
                              ),
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            padding: EdgeInsetsDirectional.only(start: widget.width * 0.15),
                            child: Text(
                              timeago.format(DateTime.parse(widget.createdAt ?? DateTime.now().toIso8601String()),
                                  locale: widget.isEn ? 'en' : 'ar'),
                              style: TextStyle(
                                fontSize: 12,
                                color: AppColors.iconsColor,
                                fontFamily: widget.isEn ? 'Inter-Medium' : 'Tajawal',
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(bottom: 12.0, left: 12.0, right: 12.0),
                            child: Icon(
                              Icons.arrow_forward_ios_rounded,
                              textDirection: widget.isEn ? TextDirection.ltr : TextDirection.rtl,
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
