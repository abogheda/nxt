// ignore_for_file: deprecated_member_use

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

ThemeData lightTheme(BuildContext context) {
  bool isEn = context.locale.languageCode == 'en';
  return ThemeData.light().copyWith(
    textTheme: isEn
        ? ThemeData.light().textTheme.apply(fontFamily: 'BebasNeue')
        : ThemeData.light().textTheme.apply(fontFamily: 'Tajawal'),
    primaryTextTheme: isEn
        ? ThemeData.light().textTheme.apply(fontFamily: 'BebasNeue')
        : ThemeData.light().textTheme.apply(fontFamily: 'Tajawal'),
    primaryColor: Colors.black,
    scaffoldBackgroundColor: Colors.white,
    splashColor: Colors.black,
    pageTransitionsTheme: const PageTransitionsTheme(
      builders: <TargetPlatform, PageTransitionsBuilder>{
        TargetPlatform.android: ZoomPageTransitionsBuilder(),
      },
    ),
    brightness: Brightness.light,
    dividerColor: Colors.black,
    colorScheme: Theme.of(context).colorScheme.copyWith(
          primary: Colors.black,
          secondary: Colors.black,
        ),
    inputDecorationTheme: InputDecorationTheme(
      focusColor: Colors.cyan,
      filled: true,
      fillColor: Colors.white,
      contentPadding: const EdgeInsets.all(8),
      border: OutlineInputBorder(
        borderSide: const BorderSide(color: Colors.transparent),
        borderRadius: BorderRadius.circular(18),
      ),
      enabledBorder: OutlineInputBorder(
        borderSide: const BorderSide(color: Colors.transparent),
        borderRadius: BorderRadius.circular(18),
      ),
      focusedBorder: OutlineInputBorder(
        borderSide: const BorderSide(color: Colors.transparent),
        borderRadius: BorderRadius.circular(18),
      ),
      disabledBorder: OutlineInputBorder(
        borderSide: const BorderSide(color: Colors.transparent),
        borderRadius: BorderRadius.circular(18),
      ),
    ),
    textSelectionTheme: const TextSelectionThemeData(
      cursorColor: Colors.black,
    ),
    textButtonTheme: TextButtonThemeData(
      style: TextButton.styleFrom(primary: Colors.black),
    ),
    floatingActionButtonTheme: const FloatingActionButtonThemeData(
      backgroundColor: Colors.grey,
    ),
    checkboxTheme: CheckboxThemeData(
      fillColor: MaterialStateProperty.all(Colors.black),
    ),
  );
}

ThemeData darkTheme(BuildContext context) {
  return ThemeData.dark().copyWith(
    textTheme: ThemeData.light().textTheme.apply(
          fontFamily: 'BebasNeue',
        ),
    primaryTextTheme: ThemeData.light().textTheme.apply(
          fontFamily: 'BebasNeue',
        ),
    primaryColor: Colors.black,
    pageTransitionsTheme: const PageTransitionsTheme(
      builders: <TargetPlatform, PageTransitionsBuilder>{
        TargetPlatform.android: ZoomPageTransitionsBuilder(),
      },
    ),
    textSelectionTheme: const TextSelectionThemeData(
      cursorColor: Colors.grey,
    ),
    splashColor: Colors.black,
    inputDecorationTheme: InputDecorationTheme(
      focusColor: Colors.grey,
      filled: true,
      contentPadding: const EdgeInsets.all(8),
      border: OutlineInputBorder(borderSide: Divider.createBorderSide(context)),
      focusedBorder: OutlineInputBorder(borderSide: Divider.createBorderSide(context)),
      enabledBorder: OutlineInputBorder(borderSide: Divider.createBorderSide(context)),
    ),
    brightness: Brightness.dark,
    dividerColor: Colors.white,
    textButtonTheme: TextButtonThemeData(
      style: TextButton.styleFrom(primary: Colors.white),
    ),
    floatingActionButtonTheme: const FloatingActionButtonThemeData(
      backgroundColor: Colors.black,
    ),
  );
}
