class Package {
  Package({
    this.id,
    this.user,
    this.plan,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.paymentMethod,
    this.renewalCount,
  });

  Package.fromJson(dynamic json) {
    id = json['_id'];
    user = json['user'];
    plan = json['plan'] != null ? Plan.fromJson(json['plan']) : null;

    removed = json['removed'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    v = json['__v'];
    paymentMethod = json['paymentMethod'];
    renewalCount = json['renewalCount'];
  }
  String? id;
  String? user;
  Plan? plan;

  bool? removed;
  String? createdAt;
  String? updatedAt;
  num? v;
  String? paymentMethod;
  num? renewalCount;
  Package copyWith({
    String? id,
    String? user,
    Plan? plan,
    bool? removed,
    String? createdAt,
    String? updatedAt,
    num? v,
    String? paymentMethod,
    num? renewalCount,
  }) =>
      Package(
        id: id ?? this.id,
        user: user ?? this.user,
        plan: plan ?? this.plan,
        removed: removed ?? this.removed,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
        v: v ?? this.v,
        paymentMethod: paymentMethod ?? this.paymentMethod,
        renewalCount: renewalCount ?? this.renewalCount,
      );
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['_id'] = id;
    map['user'] = user;
    if (plan != null) {
      map['plan'] = plan?.toJson();
    }

    map['removed'] = removed;
    map['createdAt'] = createdAt;
    map['updatedAt'] = updatedAt;
    map['__v'] = v;
    map['paymentMethod'] = paymentMethod;
    map['renewalCount'] = renewalCount;
    return map;
  }
}

class Plan {
  Plan({
    this.id,
    this.lang,
    this.price,
    this.months,
    this.country,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
  });

  Plan.fromJson(dynamic json) {
    id = json['_id'];
    if (json['lang'] != null) {
      lang = [];
      json['lang'].forEach((v) {
        lang?.add(Lang.fromJson(v));
      });
    }
    price = json['price'];
    months = json['months'];
    if (json['country'] != null) {
      final v = json['country'];
      final arr0 = <String>[];
      v.forEach((v) {
        arr0.add(v.toString());
      });
      country = arr0;
    }
    removed = json['removed'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    v = json['__v'];
  }
  String? id;
  List<Lang>? lang;
  num? price;
  num? months;
  List<String?>? country;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  num? v;
  Plan copyWith({
    String? id,
    List<Lang>? lang,
    num? price,
    num? months,
    List<String?>? country,
    bool? removed,
    String? createdAt,
    String? updatedAt,
    num? v,
  }) =>
      Plan(
        id: id ?? this.id,
        lang: lang ?? this.lang,
        price: price ?? this.price,
        months: months ?? this.months,
        country: country ?? this.country,
        removed: removed ?? this.removed,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
        v: v ?? this.v,
      );
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['_id'] = id;
    if (lang != null) {
      map['lang'] = lang?.map((v) => v.toJson()).toList();
    }
    map['price'] = price;
    map['months'] = months;
    if (lang != null) {
      final v = lang;
      final arr0 = [];
      for (var v in v!) {
        arr0.add(v.toJson());
      }
      map['lang'] = arr0;
    }
    map['removed'] = removed;
    map['createdAt'] = createdAt;
    map['updatedAt'] = updatedAt;
    map['__v'] = v;
    return map;
  }
}

class Lang {
  Lang({
    this.title,
    this.supTitle,
    this.features,
  });

  Lang.fromJson(dynamic json) {
    title = json['title'];
    supTitle = json['supTitle'];
    features = json['features'] != null ? json['features'].cast<String>() : [];
  }
  String? title;
  String? supTitle;
  List<String>? features;
  Lang copyWith({
    String? title,
    String? supTitle,
    List<String>? features,
  }) =>
      Lang(
        title: title ?? this.title,
        supTitle: supTitle ?? this.supTitle,
        features: features ?? this.features,
      );
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['title'] = title;
    map['supTitle'] = supTitle;
    map['features'] = features;
    return map;
  }
}
