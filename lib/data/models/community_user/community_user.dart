import '../base_models/base_model.dart';

class CommunityUser extends BaseModel {
  String? id;
  CommunityUserUser? user;
  List<CommunityUserTalents?>? talents;
  List<CommunityUserCategories?>? categories;
  List<String?>? specialSkills;
  String? gender;
  int? weight;
  int? height;
  String? weightUnit;
  String? heightUnit;
  String? skinColor;
  String? hairColor;
  String? eyeColor;
  String? portfolioLink;
  List<CommunityUserExperiences?>? experiences;
  List<String>? shots;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  int? v;
  List<CommunityUserReels?>? reels;

  CommunityUser({
    this.id,
    this.user,
    this.talents,
    this.categories,
    this.specialSkills,
    this.gender,
    this.weight,
    this.height,
    this.weightUnit,
    this.heightUnit,
    this.skinColor,
    this.hairColor,
    this.eyeColor,
    this.portfolioLink,
    this.experiences,
    this.shots,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.reels,
  });
  @override
  CommunityUser fromJson(Map<String, dynamic> json) {
    return CommunityUser.fromJson(json);
  }

  CommunityUser.fromJson(Map<String, dynamic> json) {
    id = json['_id']?.toString();
    user = (json['user'] != null) ? CommunityUserUser.fromJson(json['user']) : null;
    if (json['talents'] != null) {
      final v = json['talents'];
      final arr0 = <CommunityUserTalents>[];
      v.forEach((v) {
        arr0.add(CommunityUserTalents.fromJson(v));
      });
      talents = arr0;
    }
    if (json['categories'] != null) {
      final v = json['categories'];
      final arr0 = <CommunityUserCategories>[];
      v.forEach((v) {
        arr0.add(CommunityUserCategories.fromJson(v));
      });
      categories = arr0;
    }
    if (json['specialSkills'] != null) {
      final v = json['specialSkills'];
      final arr0 = <String>[];
      v.forEach((v) {
        arr0.add(v.toString());
      });
      specialSkills = arr0;
    }
    gender = json['gender']?.toString();
    weight = json['weight']?.toInt();
    height = json['height']?.toInt();
    weightUnit = json['weightUnit']?.toString();
    heightUnit = json['heightUnit']?.toString();
    skinColor = json['skinColor']?.toString();
    hairColor = json['hairColor']?.toString();
    eyeColor = json['eyeColor']?.toString();
    portfolioLink = json['portfolioLink']?.toString();
    if (json['experiences'] != null) {
      final v = json['experiences'];
      final arr0 = <CommunityUserExperiences>[];
      v.forEach((v) {
        arr0.add(CommunityUserExperiences.fromJson(v));
      });
      experiences = arr0;
    }
    if (json['shots'] != null) {
      final v = json['shots'];
      final arr0 = <String>[];
      v.forEach((v) {
        arr0.add(v.toString());
      });
      shots = arr0;
    }
    removed = json['removed'];
    createdAt = json['createdAt']?.toString();
    updatedAt = json['updatedAt']?.toString();
    v = json['__v']?.toInt();
    if (json['reels'] != null) {
      final v = json['reels'];
      final arr0 = <CommunityUserReels>[];
      v.forEach((v) {
        arr0.add(CommunityUserReels.fromJson(v));
      });
      reels = arr0;
    }
  }
  @override
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['_id'] = id;
    if (user != null) {
      data['user'] = user!.toJson();
    }
    if (talents != null) {
      final v = talents;
      final arr0 = [];
      for (var v in v!) {
        arr0.add(v!.toJson());
      }
      data['talents'] = arr0;
    }
    if (categories != null) {
      final v = categories;
      final arr0 = [];
      for (var v in v!) {
        arr0.add(v!.toJson());
      }
      data['categories'] = arr0;
    }
    if (specialSkills != null) {
      final v = specialSkills;
      final arr0 = [];
      for (var v in v!) {
        arr0.add(v);
      }
      data['specialSkills'] = arr0;
    }
    data['gender'] = gender;
    data['weight'] = weight;
    data['height'] = height;
    data['weightUnit'] = weightUnit;
    data['heightUnit'] = heightUnit;
    data['skinColor'] = skinColor;
    data['hairColor'] = hairColor;
    data['eyeColor'] = eyeColor;
    data['portfolioLink'] = portfolioLink;
    if (experiences != null) {
      final v = experiences;
      final arr0 = [];
      for (var v in v!) {
        arr0.add(v!.toJson());
      }
      data['experiences'] = arr0;
    }
    if (shots != null) {
      final v = shots;
      final arr0 = [];
      for (var v in v!) {
        arr0.add(v);
      }
      data['shots'] = arr0;
    }
    data['removed'] = removed;
    data['createdAt'] = createdAt;
    data['updatedAt'] = updatedAt;
    data['__v'] = v;
    if (reels != null) {
      final v = reels;
      final arr0 = [];
      for (var v in v!) {
        arr0.add(v!.toJson());
      }
      data['reels'] = arr0;
    }
    return data;
  }
}

class CommunityUserReelsUserConfirmPhone {
  int? codesended;
  String? expiresAt;

  CommunityUserReelsUserConfirmPhone({
    this.codesended,
    this.expiresAt,
  });
  CommunityUserReelsUserConfirmPhone.fromJson(Map<String, dynamic> json) {
    codesended = json['codesended']?.toInt();
    expiresAt = json['expiresAt']?.toString();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['codesended'] = codesended;
    data['expiresAt'] = expiresAt;
    return data;
  }
}

class CommunityUserReelsUser {
  String? id;
  String? name;
  String? email;
  String? phone;
  String? birthDate;
  String? password;
  String? role;
  String? avatar;
  String? provider;
  String? profileStatus;
  bool? verify;
  bool? verifyPhone;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  int? v;
  CommunityUserReelsUserConfirmPhone? confirmPhone;
  bool? privacy;
  bool? terms;
  bool? firstSubscription;
  bool? subscriptionEnd;
  String? countryCode;
  String? countryName;
  String? package;
  String? country;

  CommunityUserReelsUser({
    this.id,
    this.name,
    this.email,
    this.phone,
    this.birthDate,
    this.password,
    this.role,
    this.avatar,
    this.provider,
    this.profileStatus,
    this.verify,
    this.verifyPhone,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.confirmPhone,
    this.privacy,
    this.terms,
    this.firstSubscription,
    this.subscriptionEnd,
    this.countryCode,
    this.countryName,
    this.package,
    this.country,
  });
  CommunityUserReelsUser.fromJson(Map<String, dynamic> json) {
    id = json['_id']?.toString();
    name = json['name']?.toString();
    email = json['email']?.toString();
    phone = json['phone']?.toString();
    birthDate = json['birthDate']?.toString();
    password = json['password']?.toString();
    role = json['role']?.toString();
    avatar = json['avatar']?.toString();
    provider = json['provider']?.toString();
    profileStatus = json['profileStatus']?.toString();
    verify = json['verify'];
    verifyPhone = json['verifyPhone'];
    removed = json['removed'];
    createdAt = json['createdAt']?.toString();
    updatedAt = json['updatedAt']?.toString();
    v = json['__v']?.toInt();
    confirmPhone =
        (json['confirmPhone'] != null) ? CommunityUserReelsUserConfirmPhone.fromJson(json['confirmPhone']) : null;
    privacy = json['privacy'];
    terms = json['terms'];
    firstSubscription = json['firstSubscription'];
    subscriptionEnd = json['subscriptionEnd'];
    countryCode = json['countryCode']?.toString();
    countryName = json['countryName']?.toString();
    package = json['package']?.toString();
    country = json['country']?.toString();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['_id'] = id;
    data['name'] = name;
    data['email'] = email;
    data['phone'] = phone;
    data['birthDate'] = birthDate;
    data['password'] = password;
    data['role'] = role;
    data['avatar'] = avatar;
    data['provider'] = provider;
    data['profileStatus'] = profileStatus;
    data['verify'] = verify;
    data['verifyPhone'] = verifyPhone;
    data['removed'] = removed;
    data['createdAt'] = createdAt;
    data['updatedAt'] = updatedAt;
    data['__v'] = v;
    if (confirmPhone != null) {
      data['confirmPhone'] = confirmPhone!.toJson();
    }
    data['privacy'] = privacy;
    data['terms'] = terms;
    data['firstSubscription'] = firstSubscription;
    data['subscriptionEnd'] = subscriptionEnd;
    data['countryCode'] = countryCode;
    data['countryName'] = countryName;
    data['package'] = package;
    data['country'] = country;
    return data;
  }
}

class CommunityUserReels {
  String? id;
  String? title;
  String? text;
  String? media;
  CommunityUserReelsUser? user;
  String? uploadType;
  int? votesCount;
  int? likesCount;
  int? saveCount;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  int? v;

  CommunityUserReels({
    this.id,
    this.title,
    this.text,
    this.media,
    this.user,
    this.uploadType,
    this.votesCount,
    this.likesCount,
    this.saveCount,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
  });
  CommunityUserReels.fromJson(Map<String, dynamic> json) {
    id = json['_id']?.toString();
    title = json['title']?.toString();
    text = json['text']?.toString();
    media = json['media']?.toString();
    user = (json['user'] != null) ? CommunityUserReelsUser.fromJson(json['user']) : null;
    uploadType = json['uploadType']?.toString();
    votesCount = json['votesCount']?.toInt();
    likesCount = json['likesCount']?.toInt();
    saveCount = json['saveCount']?.toInt();
    removed = json['removed'];
    createdAt = json['createdAt']?.toString();
    updatedAt = json['updatedAt']?.toString();
    v = json['__v']?.toInt();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['_id'] = id;
    data['title'] = title;
    data['text'] = text;
    data['media'] = media;
    if (user != null) {
      data['user'] = user!.toJson();
    }
    data['uploadType'] = uploadType;
    data['votesCount'] = votesCount;
    data['likesCount'] = likesCount;
    data['saveCount'] = saveCount;
    data['removed'] = removed;
    data['createdAt'] = createdAt;
    data['updatedAt'] = updatedAt;
    data['__v'] = v;
    return data;
  }
}

class CommunityUserExperiences {
  String? jobTitle;
  String? roleDescription;
  String? workFrom;
  String? workTo;
  bool? currentWork;

  CommunityUserExperiences({
    this.jobTitle,
    this.roleDescription,
    this.workFrom,
    this.workTo,
    this.currentWork,
  });
  CommunityUserExperiences.fromJson(Map<String, dynamic> json) {
    jobTitle = json['jobTitle']?.toString();
    roleDescription = json['roleDescription']?.toString();
    workFrom = json['workFrom']?.toString();
    workTo = json['workTo']?.toString();
    currentWork = json['currentWork'];
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['jobTitle'] = jobTitle;
    data['roleDescription'] = roleDescription;
    data['workFrom'] = workFrom;
    data['workTo'] = workTo;
    data['currentWork'] = currentWork;
    return data;
  }
}

class CommunityUserCategories {
  String? id;
  String? color;
  String? image;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  int? v;
  bool? featured;
  String? title;
  String? description;

  CommunityUserCategories({
    this.id,
    this.color,
    this.image,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.featured,
    this.title,
    this.description,
  });
  CommunityUserCategories.fromJson(Map<String, dynamic> json) {
    id = json['_id']?.toString();
    color = json['color']?.toString();
    image = json['image']?.toString();
    removed = json['removed'];
    createdAt = json['createdAt']?.toString();
    updatedAt = json['updatedAt']?.toString();
    v = json['__v']?.toInt();
    featured = json['featured'];
    title = json['title']?.toString();
    description = json['description']?.toString();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['_id'] = id;
    data['color'] = color;
    data['image'] = image;
    data['removed'] = removed;
    data['createdAt'] = createdAt;
    data['updatedAt'] = updatedAt;
    data['__v'] = v;
    data['featured'] = featured;
    data['title'] = title;
    data['description'] = description;
    return data;
  }
}

class CommunityUserTalents {
  String? id;
  String? category;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  int? v;
  String? title;
  String? description;

  CommunityUserTalents({
    this.id,
    this.category,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.title,
    this.description,
  });
  CommunityUserTalents.fromJson(Map<String, dynamic> json) {
    id = json['_id']?.toString();
    category = json['category']?.toString();
    removed = json['removed'];
    createdAt = json['createdAt']?.toString();
    updatedAt = json['updatedAt']?.toString();
    v = json['__v']?.toInt();
    title = json['title']?.toString();
    description = json['description']?.toString();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['_id'] = id;
    data['category'] = category;
    data['removed'] = removed;
    data['createdAt'] = createdAt;
    data['updatedAt'] = updatedAt;
    data['__v'] = v;
    data['title'] = title;
    data['description'] = description;
    return data;
  }
}

class CommunityUserUserPackagePlanLang {
  String? title;
  String? supTitle;
  List<String?>? features;

  CommunityUserUserPackagePlanLang({
    this.title,
    this.supTitle,
    this.features,
  });
  CommunityUserUserPackagePlanLang.fromJson(Map<String, dynamic> json) {
    title = json['title']?.toString();
    supTitle = json['supTitle']?.toString();
    if (json['features'] != null) {
      final v = json['features'];
      final arr0 = <String>[];
      v.forEach((v) {
        arr0.add(v.toString());
      });
      features = arr0;
    }
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['title'] = title;
    data['supTitle'] = supTitle;
    if (features != null) {
      final v = features;
      final arr0 = [];
      for (var v in v!) {
        arr0.add(v);
      }
      data['features'] = arr0;
    }
    return data;
  }
}

class CommunityUserUserPackagePlan {
  String? id;
  List<CommunityUserUserPackagePlanLang?>? lang;
  int? price;
  int? months;
  String? country;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  int? v;

  CommunityUserUserPackagePlan({
    this.id,
    this.lang,
    this.price,
    this.months,
    this.country,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
  });
  CommunityUserUserPackagePlan.fromJson(Map<String, dynamic> json) {
    id = json['_id']?.toString();
    if (json['lang'] != null) {
      final v = json['lang'];
      final arr0 = <CommunityUserUserPackagePlanLang>[];
      v.forEach((v) {
        arr0.add(CommunityUserUserPackagePlanLang.fromJson(v));
      });
      lang = arr0;
    }
    price = json['price']?.toInt();
    months = json['months']?.toInt();
    country = json['country']?.toString();
    removed = json['removed'];
    createdAt = json['createdAt']?.toString();
    updatedAt = json['updatedAt']?.toString();
    v = json['__v']?.toInt();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['_id'] = id;
    if (lang != null) {
      final v = lang;
      final arr0 = [];
      for (var v in v!) {
        arr0.add(v!.toJson());
      }
      data['lang'] = arr0;
    }
    data['price'] = price;
    data['months'] = months;
    data['country'] = country;
    data['removed'] = removed;
    data['createdAt'] = createdAt;
    data['updatedAt'] = updatedAt;
    data['__v'] = v;
    return data;
  }
}

class CommunityUserUserPackage {
  String? id;
  String? user;
  CommunityUserUserPackagePlan? plan;
  int? renewalCount;
  String? paymentMethod;
  String? expiryDate;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  int? v;

  CommunityUserUserPackage({
    this.id,
    this.user,
    this.plan,
    this.renewalCount,
    this.paymentMethod,
    this.expiryDate,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
  });
  CommunityUserUserPackage.fromJson(Map<String, dynamic> json) {
    id = json['_id']?.toString();
    user = json['user']?.toString();
    plan = (json['plan'] != null) ? CommunityUserUserPackagePlan.fromJson(json['plan']) : null;
    renewalCount = json['renewalCount']?.toInt();
    paymentMethod = json['paymentMethod']?.toString();
    expiryDate = json['expiryDate']?.toString();
    removed = json['removed'];
    createdAt = json['createdAt']?.toString();
    updatedAt = json['updatedAt']?.toString();
    v = json['__v']?.toInt();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['_id'] = id;
    data['user'] = user;
    if (plan != null) {
      data['plan'] = plan!.toJson();
    }
    data['renewalCount'] = renewalCount;
    data['paymentMethod'] = paymentMethod;
    data['expiryDate'] = expiryDate;
    data['removed'] = removed;
    data['createdAt'] = createdAt;
    data['updatedAt'] = updatedAt;
    data['__v'] = v;
    return data;
  }
}

class CommunityUserUserConfirmPhone {
  int? codesended;
  String? expiresAt;

  CommunityUserUserConfirmPhone({
    this.codesended,
    this.expiresAt,
  });
  CommunityUserUserConfirmPhone.fromJson(Map<String, dynamic> json) {
    codesended = json['codesended']?.toInt();
    expiresAt = json['expiresAt']?.toString();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['codesended'] = codesended;
    data['expiresAt'] = expiresAt;
    return data;
  }
}

class CommunityUserUser {
  String? id;
  String? name;
  String? email;
  String? phone;
  String? birthDate;
  String? password;
  String? role;
  String? avatar;
  String? provider;
  String? profileStatus;
  bool? verify;
  bool? verifyPhone;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  int? v;
  CommunityUserUserConfirmPhone? confirmPhone;
  bool? privacy;
  bool? terms;
  bool? firstSubscription;
  bool? subscriptionEnd;
  String? countryCode;
  String? countryName;
  CommunityUserUserPackage? package;
  String? country;

  CommunityUserUser({
    this.id,
    this.name,
    this.email,
    this.phone,
    this.birthDate,
    this.password,
    this.role,
    this.avatar,
    this.provider,
    this.profileStatus,
    this.verify,
    this.verifyPhone,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.confirmPhone,
    this.privacy,
    this.terms,
    this.firstSubscription,
    this.subscriptionEnd,
    this.countryCode,
    this.countryName,
    this.package,
    this.country,
  });
  CommunityUserUser.fromJson(Map<String, dynamic> json) {
    id = json['_id']?.toString();
    name = json['name']?.toString();
    email = json['email']?.toString();
    phone = json['phone']?.toString();
    birthDate = json['birthDate']?.toString();
    password = json['password']?.toString();
    role = json['role']?.toString();
    avatar = json['avatar']?.toString();
    provider = json['provider']?.toString();
    profileStatus = json['profileStatus']?.toString();
    verify = json['verify'];
    verifyPhone = json['verifyPhone'];
    removed = json['removed'];
    createdAt = json['createdAt']?.toString();
    updatedAt = json['updatedAt']?.toString();
    v = json['__v']?.toInt();
    confirmPhone = (json['confirmPhone'] != null) ? CommunityUserUserConfirmPhone.fromJson(json['confirmPhone']) : null;
    privacy = json['privacy'];
    terms = json['terms'];
    firstSubscription = json['firstSubscription'];
    subscriptionEnd = json['subscriptionEnd'];
    countryCode = json['countryCode']?.toString();
    countryName = json['countryName']?.toString();
    package = (json['package'] != null) ? CommunityUserUserPackage.fromJson(json['package']) : null;
    country = json['country']?.toString();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['_id'] = id;
    data['name'] = name;
    data['email'] = email;
    data['phone'] = phone;
    data['birthDate'] = birthDate;
    data['password'] = password;
    data['role'] = role;
    data['avatar'] = avatar;
    data['provider'] = provider;
    data['profileStatus'] = profileStatus;
    data['verify'] = verify;
    data['verifyPhone'] = verifyPhone;
    data['removed'] = removed;
    data['createdAt'] = createdAt;
    data['updatedAt'] = updatedAt;
    data['__v'] = v;
    if (confirmPhone != null) {
      data['confirmPhone'] = confirmPhone!.toJson();
    }
    data['privacy'] = privacy;
    data['terms'] = terms;
    data['firstSubscription'] = firstSubscription;
    data['subscriptionEnd'] = subscriptionEnd;
    data['countryCode'] = countryCode;
    data['countryName'] = countryName;
    if (package != null) {
      data['package'] = package!.toJson();
    }
    data['country'] = country;
    return data;
  }
}
