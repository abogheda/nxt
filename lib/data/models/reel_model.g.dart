// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'reel_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ReelModel _$ReelModelFromJson(Map<String, dynamic> json) => ReelModel(
      createdAt: json['createdAt'] == null
          ? null
          : DateTime.parse(json['createdAt'] as String),
      updatedAt: json['updatedAt'] == null
          ? null
          : DateTime.parse(json['updatedAt'] as String),
      v: json['__v'] as int?,
      user: json['user'] as String?,
      id: json['_id'] as String?,
      title: json['title'] as String?,
      text: json['text'] as String?,
      media:
          (json['media'] as List<dynamic>?)?.map((e) => e as String).toList(),
      status: json['status'] as String?,
      removed: json['removed'] as bool?,
    );

Map<String, dynamic> _$ReelModelToJson(ReelModel instance) {
  final val = <String, dynamic>{};

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('_id', instance.id);
  writeNotNull('title', instance.title);
  writeNotNull('text', instance.text);
  writeNotNull('media', instance.media);
  writeNotNull('status', instance.status);
  writeNotNull('removed', instance.removed);
  writeNotNull('user', instance.user);
  writeNotNull('createdAt', instance.createdAt?.toIso8601String());
  writeNotNull('updatedAt', instance.updatedAt?.toIso8601String());
  writeNotNull('__v', instance.v);
  return val;
}
