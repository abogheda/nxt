import '../base_models/base_model.dart';

class PlanModel extends BaseModel {
  PlanModel({
    this.id,
    this.name,
    this.email,
    this.phone,
    this.birthDate,
    this.invitationExpiryDate,
    this.password,
    this.role,
    this.avatar,
    this.provider,
    this.providerId,
    this.profileStatus,
    this.expert,
    this.verify,
    this.verifyPhone,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.confirmPhone,
    this.privacy,
    this.terms,
    this.firstSubscription,
    this.subscriptionEnd,
    this.invitation,
    this.countryCode,
    this.countryName,
    this.package,
  });
  @override
  PlanModel fromJson(Map<String, dynamic> json) {
    return PlanModel.fromJson(json);
  }

  PlanModel.fromJson(dynamic json) {
    id = json['_id'];
    name = json['name'];
    email = json['email'];
    phone = json['phone'];
    birthDate = json['birthDate'];
    invitationExpiryDate = json['invitationExpiryDate'];
    password = json['password'];
    role = json['role'];
    avatar = json['avatar'];
    provider = json['provider'];
    providerId = json['providerId'];
    profileStatus = json['profileStatus'];
    expert = json['expert'];
    verify = json['verify'];
    verifyPhone = json['verifyPhone'];
    removed = json['removed'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    v = json['__v'];
    confirmPhone = json['confirmPhone'] != null ? ConfirmPhone.fromJson(json['confirmPhone']) : null;
    privacy = json['privacy'];
    terms = json['terms'];
    firstSubscription = json['firstSubscription'];
    subscriptionEnd = json['subscriptionEnd'];
    invitation = json['invitation'];
    countryCode = json['countryCode'];
    countryName = json['countryName'];
    package = json['package'] != null ? Package.fromJson(json['package']) : null;
  }
  String? id;
  String? name;
  String? email;
  String? phone;
  String? birthDate;
  String? invitationExpiryDate;
  String? password;
  String? role;
  String? avatar;
  String? provider;
  dynamic providerId;
  String? profileStatus;
  dynamic expert;
  bool? verify;
  bool? verifyPhone;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  num? v;
  ConfirmPhone? confirmPhone;
  bool? privacy;
  bool? terms;
  bool? firstSubscription;
  bool? subscriptionEnd;
  bool? invitation;
  String? countryCode;
  String? countryName;
  Package? package;
  PlanModel copyWith({
    String? id,
    String? name,
    String? email,
    String? phone,
    String? birthDate,
    String? invitationExpiryDate,
    String? password,
    String? role,
    String? avatar,
    String? provider,
    dynamic providerId,
    String? profileStatus,
    dynamic expert,
    bool? verify,
    bool? verifyPhone,
    bool? removed,
    String? createdAt,
    String? updatedAt,
    num? v,
    ConfirmPhone? confirmPhone,
    bool? privacy,
    bool? terms,
    bool? firstSubscription,
    bool? subscriptionEnd,
    bool? invitation,
    String? countryCode,
    String? countryName,
    Package? package,
  }) =>
      PlanModel(
        id: id ?? this.id,
        name: name ?? this.name,
        email: email ?? this.email,
        phone: phone ?? this.phone,
        birthDate: birthDate ?? this.birthDate,
        invitationExpiryDate: invitationExpiryDate ?? this.invitationExpiryDate,
        password: password ?? this.password,
        role: role ?? this.role,
        avatar: avatar ?? this.avatar,
        provider: provider ?? this.provider,
        providerId: providerId ?? this.providerId,
        profileStatus: profileStatus ?? this.profileStatus,
        expert: expert ?? this.expert,
        verify: verify ?? this.verify,
        verifyPhone: verifyPhone ?? this.verifyPhone,
        removed: removed ?? this.removed,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
        v: v ?? this.v,
        confirmPhone: confirmPhone ?? this.confirmPhone,
        privacy: privacy ?? this.privacy,
        terms: terms ?? this.terms,
        firstSubscription: firstSubscription ?? this.firstSubscription,
        subscriptionEnd: subscriptionEnd ?? this.subscriptionEnd,
        invitation: invitation ?? this.invitation,
        countryCode: countryCode ?? this.countryCode,
        countryName: countryName ?? this.countryName,
        package: package ?? this.package,
      );

  @override
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['_id'] = id;
    map['name'] = name;
    map['email'] = email;
    map['phone'] = phone;
    map['birthDate'] = birthDate;
    map['invitationExpiryDate'] = invitationExpiryDate;
    map['password'] = password;
    map['role'] = role;
    map['avatar'] = avatar;
    map['provider'] = provider;
    map['providerId'] = providerId;
    map['profileStatus'] = profileStatus;
    map['expert'] = expert;
    map['verify'] = verify;
    map['verifyPhone'] = verifyPhone;
    map['removed'] = removed;
    map['createdAt'] = createdAt;
    map['updatedAt'] = updatedAt;
    map['__v'] = v;
    if (confirmPhone != null) {
      map['confirmPhone'] = confirmPhone?.toJson();
    }
    map['privacy'] = privacy;
    map['terms'] = terms;
    map['firstSubscription'] = firstSubscription;
    map['subscriptionEnd'] = subscriptionEnd;
    map['invitation'] = invitation;
    map['countryCode'] = countryCode;
    map['countryName'] = countryName;
    if (package != null) {
      map['package'] = package?.toJson();
    }
    return map;
  }
}

class Package {
  Package({
    this.id,
    this.user,
    this.plan,
    this.expiryDate,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.paymentMethod,
    this.renewalCount,
  });

  Package.fromJson(dynamic json) {
    id = json['_id'];
    user = json['user'];
    plan = json['plan'] != null ? Plan.fromJson(json['plan']) : null;
    expiryDate = json['expiryDate'];
    removed = json['removed'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    v = json['__v'];
    paymentMethod = json['paymentMethod'];
    renewalCount = json['renewalCount'];
  }
  String? id;
  String? user;
  Plan? plan;
  String? expiryDate;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  num? v;
  String? paymentMethod;
  num? renewalCount;
  Package copyWith({
    String? id,
    String? user,
    Plan? plan,
    String? expiryDate,
    bool? removed,
    String? createdAt,
    String? updatedAt,
    num? v,
    String? paymentMethod,
    num? renewalCount,
  }) =>
      Package(
        id: id ?? this.id,
        user: user ?? this.user,
        plan: plan ?? this.plan,
        expiryDate: expiryDate ?? this.expiryDate,
        removed: removed ?? this.removed,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
        v: v ?? this.v,
        paymentMethod: paymentMethod ?? this.paymentMethod,
        renewalCount: renewalCount ?? this.renewalCount,
      );
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['_id'] = id;
    map['user'] = user;
    if (plan != null) {
      map['plan'] = plan?.toJson();
    }
    map['expiryDate'] = expiryDate;
    map['removed'] = removed;
    map['createdAt'] = createdAt;
    map['updatedAt'] = updatedAt;
    map['__v'] = v;
    map['paymentMethod'] = paymentMethod;
    map['renewalCount'] = renewalCount;
    return map;
  }
}

class Plan {
  Plan({
    this.id,
    this.price,
    this.months,
    this.country,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.title,
    this.supTitle,
    this.features,
  });

  Plan.fromJson(dynamic json) {
    id = json['_id'];
    price = json['price'];
    months = json['months'];
    country = json['country'];
    removed = json['removed'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    v = json['__v'];
    title = json['title'];
    supTitle = json['supTitle'];
    features = json['features'] != null ? json['features'].cast<String>() : [];
  }
  String? id;
  num? price;
  num? months;
  String? country;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  num? v;
  String? title;
  String? supTitle;
  List<String>? features;
  Plan copyWith({
    String? id,
    num? price,
    num? months,
    String? country,
    bool? removed,
    String? createdAt,
    String? updatedAt,
    num? v,
    String? title,
    String? supTitle,
    List<String>? features,
  }) =>
      Plan(
        id: id ?? this.id,
        price: price ?? this.price,
        months: months ?? this.months,
        country: country ?? this.country,
        removed: removed ?? this.removed,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
        v: v ?? this.v,
        title: title ?? this.title,
        supTitle: supTitle ?? this.supTitle,
        features: features ?? this.features,
      );
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['_id'] = id;
    map['price'] = price;
    map['months'] = months;
    map['country'] = country;
    map['removed'] = removed;
    map['createdAt'] = createdAt;
    map['updatedAt'] = updatedAt;
    map['__v'] = v;
    map['title'] = title;
    map['supTitle'] = supTitle;
    map['features'] = features;
    return map;
  }
}

class ConfirmPhone {
  ConfirmPhone({
    this.codesended,
    this.expiresAt,
  });

  ConfirmPhone.fromJson(dynamic json) {
    codesended = json['codesended'];
    expiresAt = json['expiresAt'];
  }
  dynamic codesended;
  String? expiresAt;
  ConfirmPhone copyWith({
    dynamic codesended,
    String? expiresAt,
  }) =>
      ConfirmPhone(
        codesended: codesended ?? this.codesended,
        expiresAt: expiresAt ?? this.expiresAt,
      );
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['codesended'] = codesended;
    map['expiresAt'] = expiresAt;
    return map;
  }
}
