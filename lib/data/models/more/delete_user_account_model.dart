import '../base_models/base_model.dart';

class DeleteUserAccountModel extends BaseModel {
  DeleteUserAccountModel({
    this.id,
    this.name,
    this.email,
    this.phone,
    this.birthDate,
    this.password,
    this.role,
    this.avatar,
    this.provider,
    this.providerId,
    this.profileStatus,
    this.terms,
    this.privacy,
    this.expert,
    this.firstSubscription,
    this.subscriptionEnd,
    this.verify,
    this.verifyPhone,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.country,
    this.countryCode,
    this.countryName,
    this.confirmPhone,
    this.package,
    this.invitation,
  });
  @override
  DeleteUserAccountModel fromJson(Map<String, dynamic> json) {
    return DeleteUserAccountModel.fromJson(json);
  }

  DeleteUserAccountModel.fromJson(dynamic json) {
    id = json['_id'];
    name = json['name'];
    email = json['email'];
    phone = json['phone'];
    birthDate = json['birthDate'];
    password = json['password'];
    role = json['role'];
    avatar = json['avatar'];
    provider = json['provider'];
    providerId = json['providerId'];
    profileStatus = json['profileStatus'];
    terms = json['terms'];
    privacy = json['privacy'];
    expert = json['expert'];
    firstSubscription = json['firstSubscription'];
    subscriptionEnd = json['subscriptionEnd'];
    verify = json['verify'];
    verifyPhone = json['verifyPhone'];
    removed = json['removed'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    v = json['__v'];
    country = json['country'];
    countryCode = json['countryCode'];
    countryName = json['countryName'];
    confirmPhone = json['confirmPhone'] != null ? ConfirmPhone.fromJson(json['confirmPhone']) : null;
    package = json['package'];
    invitation = json['invitation'];
  }
  String? id;
  String? name;
  String? email;
  String? phone;
  String? birthDate;
  String? password;
  String? role;
  String? avatar;
  String? provider;
  dynamic providerId;
  String? profileStatus;
  bool? terms;
  bool? privacy;
  dynamic expert;
  bool? firstSubscription;
  bool? subscriptionEnd;
  bool? verify;
  bool? verifyPhone;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  num? v;
  String? country;
  String? countryCode;
  String? countryName;
  ConfirmPhone? confirmPhone;
  String? package;
  bool? invitation;
  DeleteUserAccountModel copyWith({
    String? id,
    String? name,
    String? email,
    String? phone,
    String? birthDate,
    String? password,
    String? role,
    String? avatar,
    String? provider,
    dynamic providerId,
    String? profileStatus,
    bool? terms,
    bool? privacy,
    dynamic expert,
    bool? firstSubscription,
    bool? subscriptionEnd,
    bool? verify,
    bool? verifyPhone,
    bool? removed,
    String? createdAt,
    String? updatedAt,
    num? v,
    String? country,
    String? countryCode,
    String? countryName,
    ConfirmPhone? confirmPhone,
    String? package,
    List<dynamic>? categories,
    bool? invitation,
  }) =>
      DeleteUserAccountModel(
        id: id ?? this.id,
        name: name ?? this.name,
        email: email ?? this.email,
        phone: phone ?? this.phone,
        birthDate: birthDate ?? this.birthDate,
        password: password ?? this.password,
        role: role ?? this.role,
        avatar: avatar ?? this.avatar,
        provider: provider ?? this.provider,
        providerId: providerId ?? this.providerId,
        profileStatus: profileStatus ?? this.profileStatus,
        terms: terms ?? this.terms,
        privacy: privacy ?? this.privacy,
        expert: expert ?? this.expert,
        firstSubscription: firstSubscription ?? this.firstSubscription,
        subscriptionEnd: subscriptionEnd ?? this.subscriptionEnd,
        verify: verify ?? this.verify,
        verifyPhone: verifyPhone ?? this.verifyPhone,
        removed: removed ?? this.removed,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
        v: v ?? this.v,
        country: country ?? this.country,
        countryCode: countryCode ?? this.countryCode,
        countryName: countryName ?? this.countryName,
        confirmPhone: confirmPhone ?? this.confirmPhone,
        package: package ?? this.package,
        invitation: invitation ?? this.invitation,
      );
  @override
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['_id'] = id;
    map['name'] = name;
    map['email'] = email;
    map['phone'] = phone;
    map['birthDate'] = birthDate;
    map['password'] = password;
    map['role'] = role;
    map['avatar'] = avatar;
    map['provider'] = provider;
    map['providerId'] = providerId;
    map['profileStatus'] = profileStatus;
    map['terms'] = terms;
    map['privacy'] = privacy;
    map['expert'] = expert;
    map['firstSubscription'] = firstSubscription;
    map['subscriptionEnd'] = subscriptionEnd;
    map['verify'] = verify;
    map['verifyPhone'] = verifyPhone;
    map['removed'] = removed;
    map['createdAt'] = createdAt;
    map['updatedAt'] = updatedAt;
    map['__v'] = v;
    map['country'] = country;
    map['countryCode'] = countryCode;
    map['countryName'] = countryName;
    if (confirmPhone != null) {
      map['confirmPhone'] = confirmPhone?.toJson();
    }
    map['package'] = package;
    map['invitation'] = invitation;
    return map;
  }
}

class ConfirmPhone {
  ConfirmPhone({
    this.codesended,
    this.expiresAt,
  });

  ConfirmPhone.fromJson(dynamic json) {
    codesended = json['codesended'];
    expiresAt = json['expiresAt'];
  }
  dynamic codesended;
  String? expiresAt;
  ConfirmPhone copyWith({
    dynamic codesended,
    String? expiresAt,
  }) =>
      ConfirmPhone(
        codesended: codesended ?? this.codesended,
        expiresAt: expiresAt ?? this.expiresAt,
      );
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['codesended'] = codesended;
    map['expiresAt'] = expiresAt;
    return map;
  }
}
