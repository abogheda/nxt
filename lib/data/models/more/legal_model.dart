import '../base_models/base_model.dart';

class LegalModelExperts {
  String? expertTitle;
  String? expertSupTitle;
  String? expertImage;

  LegalModelExperts({
    this.expertTitle,
    this.expertSupTitle,
    this.expertImage,
  });
  LegalModelExperts.fromJson(Map<String, dynamic> json) {
    expertTitle = json['expertTitle']?.toString();
    expertSupTitle = json['expertSupTitle']?.toString();
    expertImage = json['expertImage']?.toString();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['expertTitle'] = expertTitle;
    data['expertSupTitle'] = expertSupTitle;
    data['expertImage'] = expertImage;
    return data;
  }
}

class LegalModel extends BaseModel {
  String? id;
  String? headerTitle;
  String? headerSupTitle;
  List<String?>? headerListTitle;
  String? headerDescription;
  String? section2;
  String? section2Image1;
  String? section2Title1;
  String? section2Description1;
  String? section2Image2;
  String? section2Title2;
  String? section2Description2;
  String? section2Image3;
  String? section2Title3;
  String? section2Description3;
  String? section3Title;
  String? section3SupTitle;
  String? section3Image1;
  String? section3Title1;
  String? section3Description1;
  String? section3Title2;
  String? section3Description2;
  String? section3Title3;
  String? section3Description3;
  String? planTitle;
  String? planSupTitle;
  String? plan1Title;
  String? plan1SupTitle;
  String? plan1Price;
  List<String?>? plan1Features;
  String? plan2Title;
  String? plan2SupTitle;
  String? plan2Price;
  List<String?>? plan2Features;
  String? plan3Title;
  String? plan3SupTitle;
  String? plan3Price;
  List<String?>? plan3Features;
  String? plan4Title;
  String? plan4SupTitle;
  String? plan4Price;
  List<String?>? plan4Features;
  String? lan;
  String? createdAt;
  String? updatedAt;
  int? v;
  String? section3Image2;
  String? section3Image3;
  List<LegalModelExperts?>? experts;
  String? terms;
  String? paymentTerms;
  String? privacy;
  String? eula;
  String? challengesTerms;
  String? communityGuidelines;
  String? headerImage;
  String? viewChallengeImage;
  String? viewChallengetext;

  LegalModel({
    this.id,
    this.headerTitle,
    this.headerSupTitle,
    this.headerListTitle,
    this.headerDescription,
    this.section2,
    this.section2Image1,
    this.section2Title1,
    this.section2Description1,
    this.section2Image2,
    this.section2Title2,
    this.section2Description2,
    this.section2Image3,
    this.section2Title3,
    this.section2Description3,
    this.section3Title,
    this.section3SupTitle,
    this.section3Image1,
    this.section3Title1,
    this.section3Description1,
    this.section3Title2,
    this.section3Description2,
    this.section3Title3,
    this.section3Description3,
    this.planTitle,
    this.planSupTitle,
    this.plan1Title,
    this.plan1SupTitle,
    this.plan1Price,
    this.plan1Features,
    this.plan2Title,
    this.plan2SupTitle,
    this.plan2Price,
    this.plan2Features,
    this.plan3Title,
    this.plan3SupTitle,
    this.plan3Price,
    this.plan3Features,
    this.plan4Title,
    this.plan4SupTitle,
    this.plan4Price,
    this.plan4Features,
    this.lan,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.section3Image2,
    this.section3Image3,
    this.experts,
    this.terms,
    this.paymentTerms,
    this.privacy,
    this.eula,
    this.challengesTerms,
    this.communityGuidelines,
    this.headerImage,
    this.viewChallengeImage,
    this.viewChallengetext,
  });
  @override
  LegalModel fromJson(Map<String, dynamic> json) {
    return LegalModel.fromJson(json);
  }

  LegalModel.fromJson(Map<String, dynamic> json) {
    id = json['_id']?.toString();
    headerTitle = json['headerTitle']?.toString();
    headerSupTitle = json['headerSupTitle']?.toString();
    if (json['headerListTitle'] != null) {
      final v = json['headerListTitle'];
      final arr0 = <String>[];
      v.forEach((v) {
        arr0.add(v.toString());
      });
      headerListTitle = arr0;
    }
    headerDescription = json['headerDescription']?.toString();
    section2 = json['section2']?.toString();
    section2Image1 = json['section2Image1']?.toString();
    section2Title1 = json['section2Title1']?.toString();
    section2Description1 = json['section2Description1']?.toString();
    section2Image2 = json['section2Image2']?.toString();
    section2Title2 = json['section2Title2']?.toString();
    section2Description2 = json['section2Description2']?.toString();
    section2Image3 = json['section2Image3']?.toString();
    section2Title3 = json['section2Title3']?.toString();
    section2Description3 = json['section2Description3']?.toString();
    section3Title = json['section3Title']?.toString();
    section3SupTitle = json['section3SupTitle']?.toString();
    section3Image1 = json['section3Image1']?.toString();
    section3Title1 = json['section3Title1']?.toString();
    section3Description1 = json['section3Description1']?.toString();
    section3Title2 = json['section3Title2']?.toString();
    section3Description2 = json['section3Description2']?.toString();
    section3Title3 = json['section3Title3']?.toString();
    section3Description3 = json['section3Description3']?.toString();
    planTitle = json['planTitle']?.toString();
    planSupTitle = json['planSupTitle']?.toString();
    plan1Title = json['plan1Title']?.toString();
    plan1SupTitle = json['plan1SupTitle']?.toString();
    plan1Price = json['plan1Price']?.toString();
    if (json['plan1Features'] != null) {
      final v = json['plan1Features'];
      final arr0 = <String>[];
      v.forEach((v) {
        arr0.add(v.toString());
      });
      plan1Features = arr0;
    }
    plan2Title = json['plan2Title']?.toString();
    plan2SupTitle = json['plan2SupTitle']?.toString();
    plan2Price = json['plan2Price']?.toString();
    if (json['plan2Features'] != null) {
      final v = json['plan2Features'];
      final arr0 = <String>[];
      v.forEach((v) {
        arr0.add(v.toString());
      });
      plan2Features = arr0;
    }
    plan3Title = json['plan3Title']?.toString();
    plan3SupTitle = json['plan3SupTitle']?.toString();
    plan3Price = json['plan3Price']?.toString();
    if (json['plan3Features'] != null) {
      final v = json['plan3Features'];
      final arr0 = <String>[];
      v.forEach((v) {
        arr0.add(v.toString());
      });
      plan3Features = arr0;
    }
    plan4Title = json['plan4Title']?.toString();
    plan4SupTitle = json['plan4SupTitle']?.toString();
    plan4Price = json['plan4Price']?.toString();
    if (json['plan4Features'] != null) {
      final v = json['plan4Features'];
      final arr0 = <String>[];
      v.forEach((v) {
        arr0.add(v.toString());
      });
      plan4Features = arr0;
    }
    lan = json['lan']?.toString();
    createdAt = json['createdAt']?.toString();
    updatedAt = json['updatedAt']?.toString();
    v = json['__v']?.toInt();
    section3Image2 = json['section3Image2']?.toString();
    section3Image3 = json['section3Image3']?.toString();
    if (json['experts'] != null) {
      final v = json['experts'];
      final arr0 = <LegalModelExperts>[];
      v.forEach((v) {
        arr0.add(LegalModelExperts.fromJson(v));
      });
      experts = arr0;
    }
    terms = json['terms']?.toString();
    paymentTerms = json['paymentTerms']?.toString();
    privacy = json['privacy']?.toString();
    eula = json['eula']?.toString();
    challengesTerms = json['challengesTerms']?.toString();
    communityGuidelines = json['communityGuidelines']?.toString();
    headerImage = json['headerImage']?.toString();
    viewChallengeImage = json['viewChallengeImage']?.toString();
    viewChallengetext = json['viewChallengetext']?.toString();
  }
  @override
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['_id'] = id;
    data['headerTitle'] = headerTitle;
    data['headerSupTitle'] = headerSupTitle;
    if (headerListTitle != null) {
      final v = headerListTitle;
      final arr0 = [];
      for (var v in v!) {
        arr0.add(v);
      }
      data['headerListTitle'] = arr0;
    }
    data['headerDescription'] = headerDescription;
    data['section2'] = section2;
    data['section2Image1'] = section2Image1;
    data['section2Title1'] = section2Title1;
    data['section2Description1'] = section2Description1;
    data['section2Image2'] = section2Image2;
    data['section2Title2'] = section2Title2;
    data['section2Description2'] = section2Description2;
    data['section2Image3'] = section2Image3;
    data['section2Title3'] = section2Title3;
    data['section2Description3'] = section2Description3;
    data['section3Title'] = section3Title;
    data['section3SupTitle'] = section3SupTitle;
    data['section3Image1'] = section3Image1;
    data['section3Title1'] = section3Title1;
    data['section3Description1'] = section3Description1;
    data['section3Title2'] = section3Title2;
    data['section3Description2'] = section3Description2;
    data['section3Title3'] = section3Title3;
    data['section3Description3'] = section3Description3;
    data['planTitle'] = planTitle;
    data['planSupTitle'] = planSupTitle;
    data['plan1Title'] = plan1Title;
    data['plan1SupTitle'] = plan1SupTitle;
    data['plan1Price'] = plan1Price;
    if (plan1Features != null) {
      final v = plan1Features;
      final arr0 = [];
      for (var v in v!) {
        arr0.add(v);
      }
      data['plan1Features'] = arr0;
    }
    data['plan2Title'] = plan2Title;
    data['plan2SupTitle'] = plan2SupTitle;
    data['plan2Price'] = plan2Price;
    if (plan2Features != null) {
      final v = plan2Features;
      final arr0 = [];
      for (var v in v!) {
        arr0.add(v);
      }
      data['plan2Features'] = arr0;
    }
    data['plan3Title'] = plan3Title;
    data['plan3SupTitle'] = plan3SupTitle;
    data['plan3Price'] = plan3Price;
    if (plan3Features != null) {
      final v = plan3Features;
      final arr0 = [];
      for (var v in v!) {
        arr0.add(v);
      }
      data['plan3Features'] = arr0;
    }
    data['plan4Title'] = plan4Title;
    data['plan4SupTitle'] = plan4SupTitle;
    data['plan4Price'] = plan4Price;
    if (plan4Features != null) {
      final v = plan4Features;
      final arr0 = [];
      for (var v in v!) {
        arr0.add(v);
      }
      data['plan4Features'] = arr0;
    }
    data['lan'] = lan;
    data['createdAt'] = createdAt;
    data['updatedAt'] = updatedAt;
    data['__v'] = v;
    data['section3Image2'] = section3Image2;
    data['section3Image3'] = section3Image3;
    if (experts != null) {
      final v = experts;
      final arr0 = [];
      for (var v in v!) {
        arr0.add(v!.toJson());
      }
      data['experts'] = arr0;
    }
    data['terms'] = terms;
    data['paymentTerms'] = paymentTerms;
    data['privacy'] = privacy;
    data['eula'] = eula;
    data['challengesTerms'] = challengesTerms;
    data['communityGuidelines'] = communityGuidelines;
    data['headerImage'] = headerImage;
    data['viewChallengeImage'] = viewChallengeImage;
    data['viewChallengetext'] = viewChallengetext;
    return data;
  }
}
