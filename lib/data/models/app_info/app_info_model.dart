import '../base_models/base_model.dart';

class AppInfoModel extends BaseModel {
  AppInfoModel({
    this.id,
    this.version,
    this.domain,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.isHidden,
    this.isAndroidHidden,
  });
  @override
  AppInfoModel fromJson(Map<String, dynamic> json) {
    return AppInfoModel.fromJson(json);
  }

  AppInfoModel.fromJson(dynamic json) {
    id = json['_id'];
    version = json['version'];
    domain = json['domain'];
    removed = json['removed'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    v = json['__v'];
    isHidden = json['isHidden'];
    isAndroidHidden = json['isAndroidHidden'];
  }
  String? id;
  String? version;
  String? domain;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  num? v;
  bool? isHidden;
  bool? isAndroidHidden;
  AppInfoModel copyWith({
    String? id,
    String? version,
    String? domain,
    bool? removed,
    String? createdAt,
    String? updatedAt,
    num? v,
    bool? isHidden,
    bool? isAndroidHidden,
  }) =>
      AppInfoModel(
        id: id ?? this.id,
        version: version ?? this.version,
        domain: domain ?? this.domain,
        removed: removed ?? this.removed,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
        v: v ?? this.v,
        isHidden: isHidden ?? this.isHidden,
        isAndroidHidden: isAndroidHidden ?? this.isAndroidHidden,
      );
  @override
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['_id'] = id;
    map['version'] = version;
    map['domain'] = domain;
    map['removed'] = removed;
    map['createdAt'] = createdAt;
    map['updatedAt'] = updatedAt;
    map['__v'] = v;
    map['isHidden'] = isHidden;
    map['isAndroidHidden'] = isAndroidHidden;
    return map;
  }
}
