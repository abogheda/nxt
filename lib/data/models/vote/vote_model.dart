import '../base_models/base_model.dart';

class VoteModel extends BaseModel {
  VoteModel({
    this.voter,
    this.userChallenge,
    this.removed,
    this.id,
    this.createdAt,
    this.updatedAt,
    this.v,
  });
  @override
  VoteModel fromJson(Map<String, dynamic> json) {
    return VoteModel.fromJson(json);
  }

  VoteModel.fromJson(dynamic json) {
    voter = json['voter'];
    userChallenge = json['userChallenge'];
    removed = json['removed'];
    id = json['_id'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    v = json['__v'];
  }
  String? voter;
  String? userChallenge;
  bool? removed;
  String? id;
  String? createdAt;
  String? updatedAt;
  num? v;
  VoteModel copyWith({
    String? voter,
    String? userChallenge,
    bool? removed,
    String? id,
    String? createdAt,
    String? updatedAt,
    num? v,
  }) =>
      VoteModel(
        voter: voter ?? this.voter,
        userChallenge: userChallenge ?? this.userChallenge,
        removed: removed ?? this.removed,
        id: id ?? this.id,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
        v: v ?? this.v,
      );
  @override
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['voter'] = voter;
    map['userChallenge'] = userChallenge;
    map['removed'] = removed;
    map['_id'] = id;
    map['createdAt'] = createdAt;
    map['updatedAt'] = updatedAt;
    map['__v'] = v;
    return map;
  }
}
