import '../base_models/base_model.dart';
import '../user_model.dart' as user_model;

class SignUpModel extends BaseModel {
  String? token;
  user_model.User? newUser;
  num? statusCode;
  String? message;
  String? error;
  SignUpModel({
    this.token,
    this.newUser,
    this.statusCode,
    this.message,
    this.error,
  });
  @override
  SignUpModel fromJson(Map<String, dynamic> json) {
    return SignUpModel.fromJson(json);
  }

  SignUpModel.fromJson(Map<String, dynamic> json) {
    token = json['token']?.toString();
    newUser = (json['newUser'] != null) ? user_model.User.fromJson(json['newUser']) : null;
    statusCode = json['statusCode'];
    message = json['message'];
    error = json['error'];
  }
  @override
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['token'] = token;
    if (newUser != null) {
      data['newUser'] = newUser!.toJson();
      data['statusCode'] = statusCode;
      data['message'] = message;
      data['error'] = error;
    }
    return data;
  }
}

// class SignUpModelNewUser {
//   String? name;
//   String? email;
//   String? phone;
//   String? myInvitationCode;
//   String? birthDate;
//   String? password;
//   String? role;
//   String? avatar;
//   String? provider;
//   String? profileStatus;
//   bool? terms;
//   bool? privacy;
//   bool? firstSubscription;
//   bool? subscriptionEnd;
//   bool? invitation;
//   bool? verify;
//   bool? verifyPhone;
//   bool? removed;
//   String? id;
//   String? createdAt;
//   String? updatedAt;
//   int? v;
//   String? country;
//   String? countryCode;
//   String? countryName;
//
//   SignUpModelNewUser({
//     this.name,
//     this.email,
//     this.phone,
//     this.myInvitationCode,
//     this.birthDate,
//     this.password,
//     this.role,
//     this.avatar,
//     this.provider,
//     this.profileStatus,
//     this.terms,
//     this.privacy,
//     this.firstSubscription,
//     this.subscriptionEnd,
//     this.invitation,
//     this.verify,
//     this.verifyPhone,
//     this.removed,
//     this.id,
//     this.createdAt,
//     this.updatedAt,
//     this.v,
//     this.country,
//     this.countryCode,
//     this.countryName,
//   });
//   SignUpModelNewUser.fromJson(Map<String, dynamic> json) {
//     name = json['name']?.toString();
//     email = json['email']?.toString();
//     phone = json['phone']?.toString();
//     myInvitationCode = json['myInvitationCode']?.toString();
//     birthDate = json['birthDate']?.toString();
//     password = json['password']?.toString();
//     role = json['role']?.toString();
//     avatar = json['avatar']?.toString();
//     provider = json['provider']?.toString();
//     profileStatus = json['profileStatus']?.toString();
//     terms = json['terms'];
//     privacy = json['privacy'];
//     firstSubscription = json['firstSubscription'];
//     subscriptionEnd = json['subscriptionEnd'];
//     invitation = json['invitation'];
//     verify = json['verify'];
//     verifyPhone = json['verifyPhone'];
//     removed = json['removed'];
//     id = json['_id']?.toString();
//     createdAt = json['createdAt']?.toString();
//     updatedAt = json['updatedAt']?.toString();
//     v = json['__v']?.toInt();
//     country = json['country']?.toString();
//     countryCode = json['countryCode']?.toString();
//     countryName = json['countryName']?.toString();
//   }
//   Map<String, dynamic> toJson() {
//     final data = <String, dynamic>{};
//     data['name'] = name;
//     data['email'] = email;
//     data['phone'] = phone;
//     data['myInvitationCode'] = myInvitationCode;
//     data['birthDate'] = birthDate;
//     data['password'] = password;
//     data['role'] = role;
//     data['avatar'] = avatar;
//     data['provider'] = provider;
//     data['profileStatus'] = profileStatus;
//     data['terms'] = terms;
//     data['privacy'] = privacy;
//     data['firstSubscription'] = firstSubscription;
//     data['subscriptionEnd'] = subscriptionEnd;
//     data['invitation'] = invitation;
//     data['verify'] = verify;
//     data['verifyPhone'] = verifyPhone;
//     data['removed'] = removed;
//     data['_id'] = id;
//     data['createdAt'] = createdAt;
//     data['updatedAt'] = updatedAt;
//     data['__v'] = v;
//     data['country'] = country;
//     data['countryCode'] = countryCode;
//     data['countryName'] = countryName;
//     return data;
//   }
// }
