import '../base_models/base_model.dart';

class ConfirmPrivacyModel extends BaseModel {
  String? id;
  String? name;
  String? email;
  String? phone;
  String? birthDate;
  String? password;
  String? role;
  String? avatar;
  String? provider;
  String? profileStatus;
  bool? terms;
  bool? privacy;
  bool? firstSubscription;
  bool? subscriptionEnd;
  bool? invitation;
  bool? verify;
  bool? verifyPhone;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  int? v;
  String? country;
  String? countryCode;
  String? countryName;
  String? myInvitationCode;
  num? statusCode;
  String? message;
  String? error;
  ConfirmPrivacyModelConfirmPhone? confirmPhone;

  ConfirmPrivacyModel({
    this.id,
    this.name,
    this.email,
    this.phone,
    this.birthDate,
    this.password,
    this.role,
    this.avatar,
    this.provider,
    this.profileStatus,
    this.terms,
    this.privacy,
    this.firstSubscription,
    this.subscriptionEnd,
    this.invitation,
    this.verify,
    this.verifyPhone,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.country,
    this.countryCode,
    this.countryName,
    this.myInvitationCode,
    this.confirmPhone,
    this.statusCode,
    this.message,
    this.error,
  });
  @override
  ConfirmPrivacyModel fromJson(Map<String, dynamic> json) {
    return ConfirmPrivacyModel.fromJson(json);
  }

  ConfirmPrivacyModel.fromJson(Map<String, dynamic> json) {
    id = json['_id']?.toString();
    name = json['name']?.toString();
    email = json['email']?.toString();
    phone = json['phone']?.toString();
    birthDate = json['birthDate']?.toString();
    password = json['password']?.toString();
    role = json['role']?.toString();
    avatar = json['avatar']?.toString();
    provider = json['provider']?.toString();
    profileStatus = json['profileStatus']?.toString();
    terms = json['terms'];
    privacy = json['privacy'];
    firstSubscription = json['firstSubscription'];
    subscriptionEnd = json['subscriptionEnd'];
    invitation = json['invitation'];
    verify = json['verify'];
    verifyPhone = json['verifyPhone'];
    removed = json['removed'];
    createdAt = json['createdAt']?.toString();
    updatedAt = json['updatedAt']?.toString();
    v = json['__v']?.toInt();
    country = json['country']?.toString();
    countryCode = json['countryCode']?.toString();
    countryName = json['countryName']?.toString();
    myInvitationCode = json['myInvitationCode']?.toString();
    confirmPhone =
        (json['confirmPhone'] != null) ? ConfirmPrivacyModelConfirmPhone.fromJson(json['confirmPhone']) : null;
    statusCode = json['statusCode'];
    message = json['message'];
    error = json['error'];
  }
  @override
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['_id'] = id;
    data['name'] = name;
    data['email'] = email;
    data['phone'] = phone;
    data['birthDate'] = birthDate;
    data['password'] = password;
    data['role'] = role;
    data['avatar'] = avatar;
    data['provider'] = provider;
    data['profileStatus'] = profileStatus;
    data['terms'] = terms;
    data['privacy'] = privacy;
    data['firstSubscription'] = firstSubscription;
    data['subscriptionEnd'] = subscriptionEnd;
    data['invitation'] = invitation;
    data['verify'] = verify;
    data['verifyPhone'] = verifyPhone;
    data['removed'] = removed;
    data['createdAt'] = createdAt;
    data['updatedAt'] = updatedAt;
    data['__v'] = v;
    data['country'] = country;
    data['countryCode'] = countryCode;
    data['countryName'] = countryName;
    data['myInvitationCode'] = myInvitationCode;
    data['statusCode'] = statusCode;
    data['message'] = message;
    data['error'] = error;
    if (confirmPhone != null) {
      data['confirmPhone'] = confirmPhone!.toJson();
    }
    return data;
  }
}

class ConfirmPrivacyModelConfirmPhone {
  String? expiresAt;

  ConfirmPrivacyModelConfirmPhone({
    this.expiresAt,
  });
  ConfirmPrivacyModelConfirmPhone.fromJson(Map<String, dynamic> json) {
    expiresAt = json['expiresAt']?.toString();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['expiresAt'] = expiresAt;
    return data;
  }
}
