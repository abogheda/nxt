import '../base_models/base_model.dart';

import '../more/plan_model.dart';

class VerifyMobileUser extends BaseModel {
  String? id;
  String? name;
  Package? package;
  String? email;
  String? phone;
  String? myInvitationCode;
  String? birthDate;
  String? invitationExpiryDate;
  String? password;
  String? role;
  String? avatar;
  String? provider;
  String? googleId;
  String? facebookId;
  dynamic providerId;
  String? profileStatus;
  dynamic organization;
  bool? verify;
  bool? verifyPhone;
  dynamic verifyCode;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  num? v;
  dynamic expert;
  bool? privacy;
  bool? terms;
  bool? firstSubscription;
  bool? subscriptionEnd;
  bool? invitation;
  String? countryCode;
  String? countryName;
  ConfirmPhone? confirmPhone;

  VerifyMobileUser({
    this.id,
    this.name,
    this.package,
    this.email,
    this.phone,
    this.myInvitationCode,
    this.birthDate,
    this.invitationExpiryDate,
    this.password,
    this.role,
    this.avatar,
    this.provider,
    this.googleId,
    this.facebookId,
    this.providerId,
    this.profileStatus,
    this.organization,
    this.verify,
    this.verifyPhone,
    this.verifyCode,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.expert,
    this.confirmPhone,
    this.privacy,
    this.terms,
    this.firstSubscription,
    this.subscriptionEnd,
    this.invitation,
    this.countryCode,
    this.countryName,
  });

  VerifyMobileUser.fromJson(dynamic json) {
    id = json['_id'];
    name = json['name'];
    email = json['email'];
    phone = json['phone'];
    myInvitationCode = json['myInvitationCode'];
    birthDate = json['birthDate'];
    invitationExpiryDate = json['invitationExpiryDate'];
    password = json['password'];
    role = json['role'];
    avatar = json['avatar'];
    provider = json['provider'];
    providerId = json['providerId'];
    profileStatus = json['profileStatus'];
    expert = json['expert'];
    verify = json['verify'];
    verifyPhone = json['verifyPhone'];
    removed = json['removed'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    v = json['__v'];
    confirmPhone = json['confirmPhone'] != null
        ? ConfirmPhone.fromJson(json['confirmPhone'])
        : null;
    privacy = json['privacy'];
    terms = json['terms'];
    firstSubscription = json['firstSubscription'];
    subscriptionEnd = json['subscriptionEnd'];
    invitation = json['invitation'];
    countryCode = json['countryCode'];
    countryName = json['countryName'];
    package =
        json['package'] != null ? Package.fromJson(json['package']) : null;
  }

  @override
  fromJson(Map<String, dynamic> json) {
    id = json['_id'];
    name = json['name'];
    email = json['email'];
    phone = json['phone'];
    myInvitationCode = json['myInvitationCode'];
    birthDate = json['birthDate'];
    invitationExpiryDate = json['invitationExpiryDate'];
    password = json['password'];
    role = json['role'];
    avatar = json['avatar'];
    provider = json['provider'];
    providerId = json['providerId'];
    profileStatus = json['profileStatus'];
    expert = json['expert'];
    verify = json['verify'];
    verifyPhone = json['verifyPhone'];
    removed = json['removed'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    v = json['__v'];
    confirmPhone = json['confirmPhone'] != null
        ? ConfirmPhone.fromJson(json['confirmPhone'])
        : null;
    privacy = json['privacy'];
    terms = json['terms'];
    firstSubscription = json['firstSubscription'];
    subscriptionEnd = json['subscriptionEnd'];
    invitation = json['invitation'];
    countryCode = json['countryCode'];
    countryName = json['countryName'];
    package =
        json['package'] != null ? Package.fromJson(json['package']) : null;
  }

  @override
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['_id'] = id;
    map['name'] = name;
    if (package != null) {
      map['package'] = package!.toJson();
    }
    map['email'] = email;
    map['phone'] = phone;
    map['myInvitationCode'] = myInvitationCode;
    map['birthDate'] = birthDate;
    map['invitationExpiryDate'] = invitationExpiryDate;
    map['password'] = password;
    map['role'] = role;
    map['avatar'] = avatar;
    map['provider'] = provider;
    map['googleId'] = googleId;
    map['facebookId'] = facebookId;
    map['providerId'] = providerId;
    map['profileStatus'] = profileStatus;
    map['organization'] = organization;
    map['verify'] = verify;
    map['verifyPhone'] = verifyPhone;
    map['verifyCode'] = verifyCode;
    map['removed'] = removed;
    map['createdAt'] = createdAt;
    map['updatedAt'] = updatedAt;
    map['__v'] = v;
    map['expert'] = expert;
    if (confirmPhone != null) {
      map['confirmPhone'] = confirmPhone?.toJson();
    }
    map['privacy'] = privacy;
    map['terms'] = terms;
    map['firstSubscription'] = firstSubscription;
    map['subscriptionEnd'] = subscriptionEnd;
    map['invitation'] = invitation;
    map['countryCode'] = countryCode;
    map['countryName'] = countryName;
    return map;
  }
}
