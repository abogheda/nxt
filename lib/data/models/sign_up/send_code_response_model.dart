import '../base_models/base_model.dart';

class SendCodeResponseModel extends BaseModel {
  SendCodeResponseModel({
    this.remaining,
  });
  @override
  SendCodeResponseModel fromJson(Map<String, dynamic> json) {
    return SendCodeResponseModel.fromJson(json);
  }

  SendCodeResponseModel.fromJson(dynamic json) {
    remaining = json['remaining'];
  }
  num? remaining;
  SendCodeResponseModel copyWith({
    num? remaining,
  }) =>
      SendCodeResponseModel(
        remaining: remaining ?? this.remaining,
      );
  @override
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['remaining'] = remaining;
    return map;
  }
}
