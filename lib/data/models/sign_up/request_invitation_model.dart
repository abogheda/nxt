import '../base_models/base_model.dart';

class RequestInvitationModel extends BaseModel {
  String? user;
  String? message;
  bool? send;
  String? id;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  int? v;

  RequestInvitationModel({
    this.user,
    this.message,
    this.send,
    this.id,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
  });
  @override
  RequestInvitationModel fromJson(Map<String, dynamic> json) {
    return RequestInvitationModel.fromJson(json);
  }

  RequestInvitationModel.fromJson(Map<String, dynamic> json) {
    user = json['user']?.toString();
    message = json['message']?.toString();
    send = json['send'];
    id = json['_id']?.toString();
    removed = json['removed'];
    createdAt = json['createdAt']?.toString();
    updatedAt = json['updatedAt']?.toString();
    v = json['__v']?.toInt();
  }
  @override
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['user'] = user;
    data['message'] = message;
    data['send'] = send;
    data['_id'] = id;
    data['removed'] = removed;
    data['createdAt'] = createdAt;
    data['updatedAt'] = updatedAt;
    data['__v'] = v;
    return data;
  }
}
