class CreateUserModel {
  CreateUserModel({
    this.name,
    this.email,
    this.phone,
    this.password,
    this.website,
    this.scope,
    this.logo,
    this.birthDate,
    this.activationCode,
    this.industry,
    this.promocode,
  });

  CreateUserModel.fromJson(dynamic json) {
    name = json['name'];
    email = json['email'];
    phone = json['phone'];
    birthDate = json['birthDate'];
    password = json['password'];
    website = json['website'];
    logo = json['logo'];
    scope = json['scope'];
    activationCode = json['activationCode'];
    industry = json['industry'];
    promocode = json['promocode'];
  }
  String? name;
  String? email;
  String? phone;
  String? birthDate;
  String? password;
  String? website;
  String? logo;
  String? scope;
  String? activationCode;
  String? industry;
  String? promocode;
  CreateUserModel copyWith({
    String? name,
    String? email,
    String? phone,
    String? birthDate,
    String? password,
    String? website,
    String? logo,
    String? scope,
    String? activationCode,
    String? industry,
    String? promocode,
  }) =>
      CreateUserModel(
        name: name ?? this.name,
        email: email ?? this.email,
        phone: phone ?? this.phone,
        birthDate: birthDate ?? this.birthDate,
        password: password ?? this.password,
        website: website ?? this.website,
        logo: logo ?? this.logo,
        scope: scope ?? this.scope,
        activationCode: activationCode ?? this.activationCode,
        industry: industry ?? this.industry,
        promocode: promocode ?? this.promocode,
      );
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['name'] = name;
    map['email'] = email;
    map['phone'] = phone;
    map['birthDate'] = birthDate;
    map['password'] = password;
    map['website'] = website;
    map['logo'] = logo;
    map['scope'] = scope;
    map['activationCode'] = activationCode;
    map['industry'] = industry;
    map['promocode'] = promocode;
    return map;
  }
}
