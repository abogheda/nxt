import '../base_models/base_model.dart';

class CouponModel extends BaseModel {
  String? sId;
  String? code;
  int? discount;
  int? limit;
  String? expireAt;
  String? plan;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  int? iV;

  CouponModel(
      {this.sId,
        this.code,
        this.discount,
        this.limit,
        this.expireAt,
        this.plan,
        this.removed,
        this.createdAt,
        this.updatedAt,
        this.iV});

  CouponModel.fromJson(json) {
    sId = json['_id'];
    code = json['code'];
    discount = json['discount'];
    limit = json['limit'];
    expireAt = json['expireAt'];
    plan = json['plan'];
    removed = json['removed'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    iV = json['__v'];
  }

  @override
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['_id'] = sId;
    data['code'] = code;
    data['discount'] = discount;
    data['limit'] = limit;
    data['expireAt'] = expireAt;
    data['plan'] = plan;
    data['removed'] = removed;
    data['createdAt'] = createdAt;
    data['updatedAt'] = updatedAt;
    data['__v'] = iV;
    return data;
  }

  @override
  fromJson(Map<String, dynamic> json) {
    // TODO: implement fromJson
    return CouponModel.fromJson(json);
  }
}