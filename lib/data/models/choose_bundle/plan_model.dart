import '../base_models/base_model.dart';

class PlanModel extends BaseModel {
  String? id;
  num? price;
  num? googlePlayPrice;
  num? appleStorePrice;
  num? months;
  bool? display;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  int? iV;
  int? discount;
  int? numOfVerticals;
  String? title;
  String? supTitle;
  List<String>? features;

  PlanModel(
      {this.id,
        this.price,
        this.googlePlayPrice,
        this.appleStorePrice,
        this.months,
        this.display,
        this.removed,
        this.createdAt,
        this.updatedAt,
        this.iV,
        this.discount,
        this.numOfVerticals,
        this.title,
        this.supTitle,
        this.features});

  PlanModel.fromJson(json) {
    id = json['_id'];
    price = json['price'];
    googlePlayPrice = json['googlePlayPrice'];
    appleStorePrice = json['appleStoerPrice'];
    months = json['months'];
    display = json['display'];
    removed = json['removed'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    iV = json['__v'];
    discount = json['descount'];
    numOfVerticals = json['numOfverticals'];
    title = json['title'];
    supTitle = json['supTitle'];
    features = json['features'].cast<String>();
  }

  @override
  fromJson(Map<String, dynamic> json) {
    // TODO: implement fromJson
    return PlanModel.fromJson(json);
  }

  @override
  Map<String, dynamic> toJson() {
    // TODO: implement toJson
    throw UnimplementedError();
  }

}
