import '../base_models/base_model.dart';

class VerifyPhoneModel extends BaseModel {
  VerifyPhoneModel({
    this.statusCode,
    this.message,
    this.error,
    this.id,
    this.name,
    this.email,
    this.phone,
    this.birthDate,
    this.password,
    this.role,
    this.avatar,
    this.provider,
    this.providerId,
    this.profileStatus,
    this.expert,
    this.verify,
    this.verifyPhone,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.confirmPhone,
    this.privacy,
    this.terms,
    this.firstSubscription,
    this.subscriptionEnd,
    this.countryCode,
    this.countryName,
    this.package,
  });

  @override
  VerifyPhoneModel fromJson(Map<String, dynamic> json) {
    return VerifyPhoneModel.fromJson(json);
  }

  VerifyPhoneModel.fromJson(dynamic json) {
    statusCode = json['statusCode'];
    message = json['message'];
    error = json['error'];
    id = json['_id'];
    name = json['name'];
    email = json['email'];
    phone = json['phone'];
    birthDate = json['birthDate'];
    password = json['password'];
    role = json['role'];
    avatar = json['avatar'];
    provider = json['provider'];
    providerId = json['providerId'];
    profileStatus = json['profileStatus'];
    expert = json['expert'];
    verify = json['verify'];
    verifyPhone = json['verifyPhone'];
    removed = json['removed'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    v = json['__v'];
    confirmPhone = json['confirmPhone'] != null ? ConfirmPhone.fromJson(json['confirmPhone']) : null;
    privacy = json['privacy'];
    terms = json['terms'];
    firstSubscription = json['firstSubscription'];
    subscriptionEnd = json['subscriptionEnd'];
    countryCode = json['countryCode'];
    countryName = json['countryName'];
    package = json['package'];
  }
  num? statusCode;
  String? message;
  String? error;
  String? id;
  String? name;
  String? email;
  String? phone;
  String? birthDate;
  String? password;
  String? role;
  String? avatar;
  String? provider;
  dynamic providerId;
  String? profileStatus;
  dynamic expert;
  bool? verify;
  bool? verifyPhone;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  num? v;
  ConfirmPhone? confirmPhone;
  bool? privacy;
  bool? terms;
  bool? firstSubscription;
  bool? subscriptionEnd;
  String? countryCode;
  String? countryName;
  String? package;
  VerifyPhoneModel copyWith({
    num? statusCode,
    String? message,
    String? error,
    String? id,
    String? name,
    String? email,
    String? phone,
    String? birthDate,
    String? password,
    String? role,
    String? avatar,
    String? provider,
    dynamic providerId,
    String? profileStatus,
    dynamic expert,
    bool? verify,
    bool? verifyPhone,
    bool? removed,
    String? createdAt,
    String? updatedAt,
    num? v,
    ConfirmPhone? confirmPhone,
    bool? privacy,
    bool? terms,
    bool? firstSubscription,
    bool? subscriptionEnd,
    String? countryCode,
    String? countryName,
    String? package,
  }) =>
      VerifyPhoneModel(
        statusCode: statusCode ?? this.statusCode,
        message: message ?? this.message,
        error: error ?? this.error,
        id: id ?? this.id,
        name: name ?? this.name,
        email: email ?? this.email,
        phone: phone ?? this.phone,
        birthDate: birthDate ?? this.birthDate,
        password: password ?? this.password,
        role: role ?? this.role,
        avatar: avatar ?? this.avatar,
        provider: provider ?? this.provider,
        providerId: providerId ?? this.providerId,
        profileStatus: profileStatus ?? this.profileStatus,
        expert: expert ?? this.expert,
        verify: verify ?? this.verify,
        verifyPhone: verifyPhone ?? this.verifyPhone,
        removed: removed ?? this.removed,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
        v: v ?? this.v,
        confirmPhone: confirmPhone ?? this.confirmPhone,
        privacy: privacy ?? this.privacy,
        terms: terms ?? this.terms,
        firstSubscription: firstSubscription ?? this.firstSubscription,
        subscriptionEnd: subscriptionEnd ?? this.subscriptionEnd,
        countryCode: countryCode ?? this.countryCode,
        countryName: countryName ?? this.countryName,
        package: package ?? this.package,
      );
  @override
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['statusCode'] = statusCode;
    map['message'] = message;
    map['error'] = error;
    map['_id'] = id;
    map['name'] = name;
    map['email'] = email;
    map['phone'] = phone;
    map['birthDate'] = birthDate;
    map['password'] = password;
    map['role'] = role;
    map['avatar'] = avatar;
    map['provider'] = provider;
    map['providerId'] = providerId;
    map['profileStatus'] = profileStatus;
    map['expert'] = expert;
    map['verify'] = verify;
    map['verifyPhone'] = verifyPhone;
    map['removed'] = removed;
    map['createdAt'] = createdAt;
    map['updatedAt'] = updatedAt;
    map['__v'] = v;
    if (confirmPhone != null) {
      map['confirmPhone'] = confirmPhone?.toJson();
    }
    map['privacy'] = privacy;
    map['terms'] = terms;
    map['firstSubscription'] = firstSubscription;
    map['subscriptionEnd'] = subscriptionEnd;
    map['countryCode'] = countryCode;
    map['countryName'] = countryName;
    map['package'] = package;
    return map;
  }
}

class ConfirmPhone {
  ConfirmPhone({
    this.codesended,
    this.expiresAt,
  });

  ConfirmPhone.fromJson(dynamic json) {
    codesended = json['codesended'];
    expiresAt = json['expiresAt'];
  }
  dynamic codesended;
  String? expiresAt;
  ConfirmPhone copyWith({
    dynamic codesended,
    String? expiresAt,
  }) =>
      ConfirmPhone(
        codesended: codesended ?? this.codesended,
        expiresAt: expiresAt ?? this.expiresAt,
      );
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['codesended'] = codesended;
    map['expiresAt'] = expiresAt;
    return map;
  }
}
