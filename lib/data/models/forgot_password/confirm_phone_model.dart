import '../package/package_model.dart';
import '../base_models/base_model.dart';

class ConfirmPhoneModel extends BaseModel {
  ConfirmPhoneModel({
    this.statusCode,
    this.message,
    this.status,
    this.error,
    this.id,
    this.name,
    this.package,
    this.email,
    this.phone,
    this.password,
    this.role,
    this.avatar,
    this.provider,
    this.facebookId,
    this.googleId,
    this.providerId,
    this.profileStatus,
    this.organization,
    this.verify,
    this.verifyPhone,
    this.verifyCode,
    this.remaining,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
  });
  @override
  ConfirmPhoneModel fromJson(Map<String, dynamic> json) {
    return ConfirmPhoneModel.fromJson(json);
  }

  ConfirmPhoneModel.fromJson(dynamic json) {
    statusCode = json['statusCode'];
    message = json['message'];
    status = json['status'];
    error = json['error'];
    id = json['_id'];
    name = json['name'];
    package = (json['package'] != null) ? Package.fromJson(json['package']) : null;
    email = json['email'];
    phone = json['phone'];
    password = json['password'];
    role = json['role'];
    avatar = json['avatar'];
    provider = json['provider'];
    googleId = json['googleId'];
    facebookId = json['facebookId'];
    providerId = json['providerId'];
    profileStatus = json['profileStatus'];
    organization = json['organization'];
    verify = json['verify'];
    verifyPhone = json['verifyPhone'];
    verifyCode = json['verifyCode'];
    remaining = json['remaining'];
    removed = json['removed'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    v = json['__v'];
  }
  num? statusCode;
  String? message;
  String? status;
  String? error;
  String? id;
  String? name;
  Package? package;
  String? email;
  String? phone;
  String? password;
  String? role;
  String? avatar;
  String? provider;
  String? googleId;
  String? facebookId;
  dynamic providerId;
  String? profileStatus;
  dynamic organization;
  bool? verify;
  bool? verifyPhone;
  num? verifyCode;
  num? remaining;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  num? v;
  ConfirmPhoneModel copyWith({
    num? statusCode,
    String? message,
    String? status,
    String? error,
    String? id,
    String? name,
    Package? package,
    String? email,
    String? phone,
    String? password,
    String? role,
    String? avatar,
    String? provider,
    String? googleId,
    String? facebookId,
    dynamic providerId,
    String? profileStatus,
    dynamic organization,
    bool? verify,
    bool? verifyPhone,
    num? verifyCode,
    num? remaining,
    bool? removed,
    String? createdAt,
    String? updatedAt,
    num? v,
  }) =>
      ConfirmPhoneModel(
        statusCode: statusCode ?? this.statusCode,
        message: message ?? this.message,
        status: status ?? this.status,
        error: error ?? this.error,
        id: id ?? this.id,
        name: name ?? this.name,
        package: package ?? this.package,
        email: email ?? this.email,
        phone: phone ?? this.phone,
        password: password ?? this.password,
        role: role ?? this.role,
        avatar: avatar ?? this.avatar,
        provider: provider ?? this.provider,
        googleId: googleId ?? this.googleId,
        facebookId: facebookId ?? this.facebookId,
        providerId: providerId ?? this.providerId,
        profileStatus: profileStatus ?? this.profileStatus,
        organization: organization ?? this.organization,
        verify: verify ?? this.verify,
        verifyPhone: verifyPhone ?? this.verifyPhone,
        verifyCode: verifyCode ?? this.verifyCode,
        remaining: remaining ?? this.remaining,
        removed: removed ?? this.removed,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
        v: v ?? this.v,
      );
  @override
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['statusCode'] = statusCode;
    map['message'] = message;
    map['status'] = status;
    map['error'] = error;
    map['_id'] = id;
    map['name'] = name;
    if (package != null) {
      map['package'] = package!.toJson();
    }
    map['email'] = email;
    map['phone'] = phone;
    map['password'] = password;
    map['role'] = role;
    map['avatar'] = avatar;
    map['provider'] = provider;
    map['googleId'] = googleId;
    map['facebookId'] = facebookId;
    map['providerId'] = providerId;
    map['profileStatus'] = profileStatus;
    map['organization'] = organization;
    map['verify'] = verify;
    map['verifyPhone'] = verifyPhone;
    map['verifyCode'] = verifyCode;
    map['remaining'] = remaining;
    map['removed'] = removed;
    map['createdAt'] = createdAt;
    map['updatedAt'] = updatedAt;
    map['__v'] = v;
    return map;
  }
}
