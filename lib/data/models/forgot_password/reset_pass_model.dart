import '../../../../data/models/base_models/base_model.dart';

class ResetPassModel extends BaseModel {
  ResetPassModel({
    this.statusCode,
    this.message,
    this.error,
    this.id,
    this.name,
    this.email,
    this.phone,
    this.password,
    this.role,
    this.avatar,
    this.provider,
    this.googleId,
    this.facebookId,
    this.providerId,
    this.profileStatus,
    this.organization,
    this.verify,
    this.verifyPhone,
    this.verifyCode,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
  });
  @override
  ResetPassModel fromJson(Map<String, dynamic> json) {
    return ResetPassModel.fromJson(json);
  }

  ResetPassModel.fromJson(dynamic json) {
    statusCode = json['statusCode'];
    message = json['message'];
    error = json['error'];
    id = json['_id'];
    name = json['name'];
    email = json['email'];
    phone = json['phone'];
    password = json['password'];
    role = json['role'];
    avatar = json['avatar'];
    provider = json['provider'];
    facebookId = json['facebookId'];
    googleId = json['googleId'];
    providerId = json['providerId'];
    profileStatus = json['profileStatus'];
    organization = json['organization'];
    verify = json['verify'];
    verifyPhone = json['verifyPhone'];
    verifyCode = json['verifyCode'];
    removed = json['removed'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    v = json['__v'];
  }
  num? statusCode;
  List? message;
  String? error;
  String? id;
  String? name;
  String? email;
  String? phone;
  String? password;
  String? role;
  String? avatar;
  String? provider;
  String? googleId;
  String? facebookId;
  dynamic providerId;
  String? profileStatus;
  dynamic organization;
  bool? verify;
  bool? verifyPhone;
  dynamic verifyCode;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  num? v;
  ResetPassModel copyWith({
    num? statusCode,
    List? message,
    String? error,
    String? id,
    String? name,
    String? email,
    String? phone,
    String? password,
    String? role,
    String? avatar,
    String? provider,
    String? googleId,
    String? facebookId,
    dynamic providerId,
    String? profileStatus,
    dynamic organization,
    bool? verify,
    bool? verifyPhone,
    dynamic verifyCode,
    bool? removed,
    String? createdAt,
    String? updatedAt,
    num? v,
  }) =>
      ResetPassModel(
        statusCode: statusCode ?? this.statusCode,
        message: message ?? this.message,
        error: error ?? this.error,
        id: id ?? this.id,
        name: name ?? this.name,
        email: email ?? this.email,
        phone: phone ?? this.phone,
        password: password ?? this.password,
        role: role ?? this.role,
        avatar: avatar ?? this.avatar,
        provider: provider ?? this.provider,
        facebookId: facebookId ?? this.facebookId,
        googleId: googleId ?? this.googleId,
        providerId: providerId ?? this.providerId,
        profileStatus: profileStatus ?? this.profileStatus,
        organization: organization ?? this.organization,
        verify: verify ?? this.verify,
        verifyPhone: verifyPhone ?? this.verifyPhone,
        verifyCode: verifyCode ?? this.verifyCode,
        removed: removed ?? this.removed,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
        v: v ?? this.v,
      );
  @override
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['statusCode'] = statusCode;
    map['message'] = message;
    map['error'] = error;
    map['_id'] = id;
    map['name'] = name;
    map['email'] = email;
    map['phone'] = phone;
    map['password'] = password;
    map['role'] = role;
    map['avatar'] = avatar;
    map['provider'] = provider;
    map['facebookId'] = facebookId;
    map['googleId'] = googleId;
    map['providerId'] = providerId;
    map['profileStatus'] = profileStatus;
    map['organization'] = organization;
    map['verify'] = verify;
    map['verifyPhone'] = verifyPhone;
    map['verifyCode'] = verifyCode;
    map['removed'] = removed;
    map['createdAt'] = createdAt;
    map['updatedAt'] = updatedAt;
    map['__v'] = v;
    return map;
  }
}
