// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'home.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

HomeModel _$HomeModelFromJson(Map<String, dynamic> json) => HomeModel(
  id: json['_id'] as String?,
  title: json['title'] as String?,
  supTitle: json['supTitle'] as String?,
  description: json['description'] as String?,
  color: json['color'] as String?,
  removed: json['removed'] as bool?,
  userSaved: json['userSaved'] as bool?,
  createdAt: json['createdAt'] == null ? null : DateTime.parse(json['createdAt'] as String),
  updatedAt: json['updatedAt'] == null ? null : DateTime.parse(json['updatedAt'] as String),
  v: json['__v'] as int?,
)
  ..requirements = json['requirements'] as String?
  ..details = json['details'] as String?
  ..message = json['message'] as String?
  ..error = json['error'] as String?
  ..statusCode = json['statusCode'] as num?
  ..duration = json['duration'] as String?
  ..language = json['language'] as String?
  ..tags = (json['tags'] as List<dynamic>?)?.map((e) => e as String).toList()
  ..prize = json['prize'] as String?
  ..text = json['text'] as String?
  ..media = json['media'] as String?
  ..poster = json['poster'] as String?
  ..category = json['category'] == null ? null : Category.fromJson(json['category'] as Map<String, dynamic>)
  ..author = json['author'] == null ? null : Author.fromJson(json['author'] as Map<String, dynamic>)
  ..homeAuthorModel = json['author'] == null
      ? null
      : home_author_model.HomeAuthorModel.fromJson(json['author'] as Map<String, dynamic>)
  ..registerationStartDate =
  json['registerationStartDate'] == null ? null : DateTime.parse(json['registerationStartDate'] as String)
  ..registerationEndDate =
  json['registerationEndDate'] == null ? null : DateTime.parse(json['registerationEndDate'] as String)
  ..votingStartDate = json['votingStartDate'] == null ? null : DateTime.parse(json['votingStartDate'] as String)
  ..votingEndDate = json['votingEndDate'] == null ? null : DateTime.parse(json['votingEndDate'] as String)
  ..winnerAnnouncementDate =
  json['winnerAnnouncementDate'] == null ? null : DateTime.parse(json['winnerAnnouncementDate'] as String)
  ..featured = json['featured'] as bool?
  ..display = json['display'] as bool?
  ..released = json['released'] as bool?;

Map<String, dynamic> _$HomeModelToJson(HomeModel instance) => <String, dynamic>{
  '_id': instance.id,
  'title': instance.title,
  'statusCode': instance.statusCode,
  'message': instance.message,
  'error': instance.error,
  'supTitle': instance.supTitle,
  'description': instance.description,
  'requirements': instance.requirements,
  'details': instance.details,
  'duration': instance.duration,
  'language': instance.language,
  'tags': instance.tags,
  'prize': instance.prize,
  'text': instance.text,
  'media': instance.media,
  'poster': instance.poster,
  'category': instance.category,
  'author': instance.author,
  'registerationStartDate': instance.registerationStartDate?.toIso8601String(),
  'registerationEndDate': instance.registerationEndDate?.toIso8601String(),
  'votingStartDate': instance.votingStartDate?.toIso8601String(),
  'votingEndDate': instance.votingEndDate?.toIso8601String(),
  'winnerAnnouncementDate': instance.winnerAnnouncementDate?.toIso8601String(),
  'featured': instance.featured,
  'removed': instance.removed,
  'userSaved': instance.userSaved,
  'display': instance.display,
  'released': instance.released,
  'color': instance.color,
  'createdAt': instance.createdAt?.toIso8601String(),
  'updatedAt': instance.updatedAt?.toIso8601String(),
  '__v': instance.v,
};