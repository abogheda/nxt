import 'package:json_annotation/json_annotation.dart';

import '../base_models/base_model.dart';
import '../classes/author.dart';
import '../classes/category.dart';
import '../classes/master_class.dart' as home_author_model;

part 'home.g.dart';

@JsonSerializable()
class HomeModel extends BaseModel {
  @JsonKey(name: '_id')
  String? id;
  String? title;
  String? supTitle;
  String? description;
  String? requirements;
  String? details;
  String? duration;
  String? language;
  List<String>? tags;
  String? prize;
  String? text;
  String? media;
  String? poster;
  Category? category;
  Author? author;
  home_author_model.HomeAuthorModel? homeAuthorModel;
  DateTime? registerationStartDate;
  DateTime? registerationEndDate;
  DateTime? votingStartDate;
  DateTime? votingEndDate;
  DateTime? winnerAnnouncementDate;
  bool? featured;
  bool? removed;
  bool? userSaved;
  bool? display;
  bool? released;
  String? color;
  DateTime? createdAt;
  DateTime? updatedAt;
  num? statusCode;
  String? message;
  String? error;
  @JsonKey(name: '__v')
  int? v;

  HomeModel({
    this.id,
    this.title,
    this.supTitle,
    this.description,
    this.color,
    this.removed,
    this.userSaved,
    this.createdAt,
    this.updatedAt,
    this.v,
    num? statusCode,
    String? message,
    String? error,
  });

  factory HomeModel.fromJson(Map<String, dynamic> json) => _$HomeModelFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$HomeModelToJson(this);

  @override
  fromJson(Map<String, dynamic> json) {
    return _$HomeModelFromJson(json);
  }
}
