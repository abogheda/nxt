import 'package:json_annotation/json_annotation.dart';
import '../base_models/base_model.dart';

part 'home_author.g.dart';

@JsonSerializable()
class HomeAuthor extends BaseModel {
  @JsonKey(name: '_id')
  String? id;
  String? name;
  String? avatar;


  HomeAuthor({
    this.id,
    this.name,
    this.avatar,
   
  });

  factory HomeAuthor.fromJson(Map<String, dynamic> json) =>
      _$HomeAuthorFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$HomeAuthorToJson(this);
  
  @override
  fromJson(Map<String, dynamic> json) {

    throw UnimplementedError();
  }
}
