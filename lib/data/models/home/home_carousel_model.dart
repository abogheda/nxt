import 'package:nxt/data/models/base_models/base_model.dart';

class HomeCarouselModel extends BaseModel {
  String? id;
  String? image;
  String? media;
  HomeCarouselModelCategory? category;
  bool? display;
  bool? removed;
  List<HomeCarouselModelCountry?>? country;
  String? content;

  HomeCarouselModel({
    this.id,
    this.image,
    this.media,
    this.category,
    this.display,
    this.removed,
    this.country,
    this.content,
  });
  @override
  HomeCarouselModel fromJson(Map<String, dynamic> json) {
    return HomeCarouselModel.fromJson(json);
  }

  HomeCarouselModel.fromJson(Map<String, dynamic> json) {
    id = json['_id']?.toString();
    image = json['image']?.toString();
    media = json['media']?.toString();
    // category = (json['category'] != null) ? HomeCarouselModelCategory.fromJson(json['category']) : null;
    display = json['display'];
    removed = json['removed'];
    if (json['country'] != null) {
      final v = json['country'];
      final arr0 = <HomeCarouselModelCountry>[];
      v.forEach((v) {
        arr0.add(HomeCarouselModelCountry.fromJson(v));
      });
      country = arr0;
    }
    content = json['content']?.toString();
  }
  @override
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['_id'] = id;
    data['image'] = image;
    data['media'] = media;
    if (category != null) {
      data['category'] = category!.toJson();
    }
    data['display'] = display;
    data['removed'] = removed;
    if (country != null) {
      final v = country;
      final arr0 = [];
      for (var v in v!) {
        arr0.add(v!.toJson());
      }
      data['country'] = arr0;
    }
    data['content'] = content;
    return data;
  }
}

class HomeCarouselModelCountry {
  String? id;
  String? name;
  String? lat;
  String? long;
  String? azureCode;
  bool? removed;

  HomeCarouselModelCountry({
    this.id,
    this.name,
    this.lat,
    this.long,
    this.azureCode,
    this.removed,
  });
  HomeCarouselModelCountry.fromJson(Map<String, dynamic> json) {
    id = json['_id']?.toString();
    name = json['name']?.toString();
    lat = json['lat']?.toString();
    long = json['long']?.toString();
    azureCode = json['azureCode']?.toString();
    removed = json['removed'];
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['_id'] = id;
    data['name'] = name;
    data['lat'] = lat;
    data['long'] = long;
    data['azureCode'] = azureCode;
    data['removed'] = removed;
    return data;
  }
}

class HomeCarouselModelCategory {
  String? id;
  String? color;
  String? image;
  bool? removed;
  bool? featured;
  List<String?>? tags;
  String? title;
  String? description;

  HomeCarouselModelCategory({
    this.id,
    this.color,
    this.image,
    this.removed,
    this.featured,
    this.tags,
    this.title,
    this.description,
  });
  HomeCarouselModelCategory.fromJson(Map<String, dynamic> json) {
    id = json['_id']?.toString();
    color = json['color']?.toString();
    image = json['image']?.toString();
    removed = json['removed'];
    featured = json['featured'];
    if (json['tags'] != null) {
      final v = json['tags'];
      final arr0 = <String>[];
      v.forEach((v) {
        arr0.add(v.toString());
      });
      tags = arr0;
    }
    title = json['title']?.toString();
    description = json['description']?.toString();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['_id'] = id;
    data['color'] = color;
    data['image'] = image;
    data['removed'] = removed;
    data['featured'] = featured;
    if (tags != null) {
      final v = tags;
      final arr0 = [];
      for (var v in v!) {
        arr0.add(v);
      }
      data['tags'] = arr0;
    }
    data['title'] = title;
    data['description'] = description;
    return data;
  }
}
