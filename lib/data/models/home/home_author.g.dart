// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'home_author.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

HomeAuthor _$HomeAuthorFromJson(Map<String, dynamic> json) => HomeAuthor(
      id: json['_id'] as String?,
      name: json['name'] as String?,
      avatar: json['avatar'] as String?,
    );

Map<String, dynamic> _$HomeAuthorToJson(HomeAuthor instance) =>
    <String, dynamic>{
      '_id': instance.id,
      'name': instance.name,
      'avatar': instance.avatar,
    };
