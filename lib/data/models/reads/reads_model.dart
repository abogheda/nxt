import '../base_models/base_model.dart';

class ReadsModel extends BaseModel {
  ReadsModel({
    this.id,
    this.media,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.title,
    this.description,
    this.details,
    this.tags,
    this.content,
  });
  @override
  ReadsModel fromJson(Map<String, dynamic> json) {
    return ReadsModel.fromJson(json);
  }

  ReadsModel.fromJson(dynamic json) {
    id = json['_id'];
    media = json['media'];
    removed = json['removed'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    v = json['__v'];
    title = json['title'];
    description = json['description'];
    details = json['details'];
    tags = json['tags'] != null ? json['tags'].cast<String>() : [];
    content = json['content'];
  }
  String? id;
  String? media;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  num? v;
  String? title;
  String? description;
  String? details;
  List<String>? tags;
  String? content;
  ReadsModel copyWith({
    String? id,
    String? media,
    bool? removed,
    String? createdAt,
    String? updatedAt,
    num? v,
    String? title,
    String? description,
    String? details,
    List<String>? tags,
    String? content,
  }) =>
      ReadsModel(
        id: id ?? this.id,
        media: media ?? this.media,
        removed: removed ?? this.removed,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
        v: v ?? this.v,
        title: title ?? this.title,
        description: description ?? this.description,
        details: details ?? this.details,
        tags: tags ?? this.tags,
        content: content ?? this.content,
      );
  @override
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['_id'] = id;
    map['media'] = media;
    map['removed'] = removed;
    map['createdAt'] = createdAt;
    map['updatedAt'] = updatedAt;
    map['__v'] = v;
    map['title'] = title;
    map['description'] = description;
    map['details'] = details;
    map['tags'] = tags;
    map['content'] = content;
    return map;
  }
}
