import '../base_models/base_model.dart';

class ReadModelCategory {
  String? id;
  String? color;
  String? image;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  int? v;
  bool? featured;
  String? title;
  String? description;

  ReadModelCategory({
    this.id,
    this.color,
    this.image,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.featured,
    this.title,
    this.description,
  });
  ReadModelCategory.fromJson(Map<String, dynamic> json) {
    id = json['_id']?.toString();
    color = json['color']?.toString();
    image = json['image']?.toString();
    removed = json['removed'];
    createdAt = json['createdAt']?.toString();
    updatedAt = json['updatedAt']?.toString();
    v = json['__v']?.toInt();
    featured = json['featured'];
    title = json['title']?.toString();
    description = json['description']?.toString();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['_id'] = id;
    data['color'] = color;
    data['image'] = image;
    data['removed'] = removed;
    data['createdAt'] = createdAt;
    data['updatedAt'] = updatedAt;
    data['__v'] = v;
    data['featured'] = featured;
    data['title'] = title;
    data['description'] = description;
    return data;
  }
}

class ReadModel extends BaseModel {
  String? id;
  String? media;
  ReadModelCategory? category;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  int? v;
  bool? userSaved;
  String? title;
  String? description;
  String? details;
  List<String>? tags;
  String? content;

  ReadModel({
    this.id,
    this.media,
    this.category,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.userSaved,
    this.title,
    this.description,
    this.details,
    this.tags,
    this.content,
  });
  @override
  ReadModel fromJson(Map<String, dynamic> json) {
    return ReadModel.fromJson(json);
  }

  ReadModel.fromJson(Map<String, dynamic> json) {
    id = json['_id']?.toString();
    media = json['media']?.toString();
    category = (json['category'] != null) ? ReadModelCategory.fromJson(json['category']) : null;
    removed = json['removed'];
    createdAt = json['createdAt']?.toString();
    updatedAt = json['updatedAt']?.toString();
    v = json['__v']?.toInt();
    userSaved = json['userSaved'];
    title = json['title']?.toString();
    description = json['description']?.toString();
    details = json['details']?.toString();
    if (json['tags'] != null) {
      final v = json['tags'];
      final arr0 = <String>[];
      v.forEach((v) {
        arr0.add(v.toString());
      });
      tags = arr0;
    }
    content = json['content']?.toString();
  }
  @override
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['_id'] = id;
    data['media'] = media;
    if (category != null) {
      data['category'] = category!.toJson();
    }
    data['removed'] = removed;
    data['createdAt'] = createdAt;
    data['updatedAt'] = updatedAt;
    data['__v'] = v;
    data['userSaved'] = userSaved;
    data['title'] = title;
    data['description'] = description;
    data['details'] = details;
    if (tags != null) {
      final v = tags;
      final arr0 = [];
      for (var v in v!) {
        arr0.add(v);
      }
      data['tags'] = arr0;
    }
    data['content'] = content;
    return data;
  }
}
