import '../base_models/base_model.dart';

class ExpertProfileModel extends BaseModel {
  String? id;
  String? name;
  String? email;
  String? phone;
  String? password;
  String? role;
  String? avatar;
  String? provider;
  String? profileStatus;
  bool? terms;
  bool? privacy;
  bool? eula;
  ExpertProfileModelExpert? expert;
  bool? firstSubscription;
  bool? subscriptionEnd;
  bool? invitation;
  bool? verify;
  bool? verifyPhone;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  int? v;
  ExpertProfileModelConfirmPhone? confirmPhone;

  ExpertProfileModel({
    this.id,
    this.name,
    this.email,
    this.phone,
    this.password,
    this.role,
    this.avatar,
    this.provider,
    this.profileStatus,
    this.terms,
    this.privacy,
    this.eula,
    this.expert,
    this.firstSubscription,
    this.subscriptionEnd,
    this.invitation,
    this.verify,
    this.verifyPhone,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.confirmPhone,
  });
  @override
  ExpertProfileModel fromJson(Map<String, dynamic> json) {
    return ExpertProfileModel.fromJson(json);
  }

  ExpertProfileModel.fromJson(Map<String, dynamic> json) {
    id = json['_id']?.toString();
    name = json['name']?.toString();
    email = json['email']?.toString();
    phone = json['phone']?.toString();
    password = json['password']?.toString();
    role = json['role']?.toString();
    avatar = json['avatar']?.toString();
    provider = json['provider']?.toString();
    profileStatus = json['profileStatus']?.toString();
    terms = json['terms'];
    privacy = json['privacy'];
    eula = json['eula'];
    expert = (json['expert'] != null) ? ExpertProfileModelExpert.fromJson(json['expert']) : null;
    firstSubscription = json['firstSubscription'];
    subscriptionEnd = json['subscriptionEnd'];
    invitation = json['invitation'];
    verify = json['verify'];
    verifyPhone = json['verifyPhone'];
    removed = json['removed'];
    createdAt = json['createdAt']?.toString();
    updatedAt = json['updatedAt']?.toString();
    v = json['__v']?.toInt();
    confirmPhone =
        (json['confirmPhone'] != null) ? ExpertProfileModelConfirmPhone.fromJson(json['confirmPhone']) : null;
  }
  @override
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['_id'] = id;
    data['name'] = name;
    data['email'] = email;
    data['phone'] = phone;
    data['password'] = password;
    data['role'] = role;
    data['avatar'] = avatar;
    data['provider'] = provider;
    data['profileStatus'] = profileStatus;
    data['terms'] = terms;
    data['privacy'] = privacy;
    data['eula'] = eula;
    if (expert != null) {
      data['expert'] = expert!.toJson();
    }
    data['firstSubscription'] = firstSubscription;
    data['subscriptionEnd'] = subscriptionEnd;
    data['invitation'] = invitation;
    data['verify'] = verify;
    data['verifyPhone'] = verifyPhone;
    data['removed'] = removed;
    data['createdAt'] = createdAt;
    data['updatedAt'] = updatedAt;
    data['__v'] = v;
    if (confirmPhone != null) {
      data['confirmPhone'] = confirmPhone!.toJson();
    }
    return data;
  }
}

class ExpertProfileModelConfirmPhone {
  int? codesended;
  String? expiresAt;

  ExpertProfileModelConfirmPhone({
    this.codesended,
    this.expiresAt,
  });
  ExpertProfileModelConfirmPhone.fromJson(Map<String, dynamic> json) {
    codesended = json['codesended']?.toInt();
    expiresAt = json['expiresAt']?.toString();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['codesended'] = codesended;
    data['expiresAt'] = expiresAt;
    return data;
  }
}

class ExpertProfileModelExpert {
  String? name;
  String? website;
  String? scope;
  String? logo;

  ExpertProfileModelExpert({
    this.name,
    this.website,
    this.scope,
    this.logo,
  });
  ExpertProfileModelExpert.fromJson(Map<String, dynamic> json) {
    name = json['name']?.toString();
    website = json['website']?.toString();
    scope = json['scope']?.toString();
    logo = json['logo']?.toString();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['name'] = name;
    data['website'] = website;
    data['scope'] = scope;
    data['logo'] = logo;
    return data;
  }
}
