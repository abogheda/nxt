// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'challenge.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Challenge _$ChallengeFromJson(Map<String, dynamic> json) => Challenge(
      id: json['_id'] as String?,
      title: json['title'] as String?,
      supTitle: json['supTitle'] as String?,
      description: json['description'] as String?,
      requirements: json['requirements'] as String?,
      prize: json['prize'] as String?,
      lang: (json['lang'] as List<dynamic>?)
          ?.map((e) => Lang.fromJson(e as Map<String, dynamic>))
          .toList(),
      media: json['media'] as String?,
      category: json['category'] == null
          ? null
          : Category.fromJson(json['category'] as Map<String, dynamic>),
      registerationStartDate: json['registerationStartDate'] == null
          ? null
          : DateTime.parse(json['registerationStartDate'] as String),
      registerationEndDate: json['registerationEndDate'] == null
          ? null
          : DateTime.parse(json['registerationEndDate'] as String),
      votingStartDate: json['votingStartDate'] == null
          ? null
          : DateTime.parse(json['votingStartDate'] as String),
      votingEndDate: json['votingEndDate'] == null
          ? null
          : DateTime.parse(json['votingEndDate'] as String),
      winnerAnnouncementDate: json['winnerAnnouncementDate'] == null
          ? null
          : DateTime.parse(json['winnerAnnouncementDate'] as String),
      featured: json['featured'] as bool?,
      removed: json['removed'] as bool?,
      userJoined: json['userJoined'] as bool?,
      userSaved: json['userSaved'] as bool?,
      released: json['released'] as bool?,
      createdAt: json['createdAt'] == null
          ? null
          : DateTime.parse(json['createdAt'] as String),
      updatedAt: json['updatedAt'] == null
          ? null
          : DateTime.parse(json['updatedAt'] as String),
      v: json['__v'] as int?,
    );

Map<String, dynamic> _$ChallengeToJson(Challenge instance) => <String, dynamic>{
      '_id': instance.id,
      'title': instance.title,
      'supTitle': instance.supTitle,
      'description': instance.description,
      'requirements': instance.requirements,
      'prize': instance.prize,
      'media': instance.media,
      'lang': instance.lang,
      'category': instance.category,
      'registerationStartDate':
          instance.registerationStartDate?.toIso8601String(),
      'registerationEndDate': instance.registerationEndDate?.toIso8601String(),
      'votingStartDate': instance.votingStartDate?.toIso8601String(),
      'votingEndDate': instance.votingEndDate?.toIso8601String(),
      'winnerAnnouncementDate':
          instance.winnerAnnouncementDate?.toIso8601String(),
      'featured': instance.featured,
      'removed': instance.removed,
      'userJoined': instance.userJoined,
      'userSaved': instance.userSaved,
      'released': instance.released,
      'createdAt': instance.createdAt?.toIso8601String(),
      'updatedAt': instance.updatedAt?.toIso8601String(),
      '__v': instance.v,
    };
