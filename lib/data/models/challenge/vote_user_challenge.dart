import '../base_models/base_model.dart';

class VoteUserChallengeUser {
  String? id;
  String? name;
  String? email;
  String? phone;
  String? avatar;

  VoteUserChallengeUser({
    this.id,
    this.name,
    this.email,
    this.phone,
    this.avatar,
  });
  VoteUserChallengeUser.fromJson(Map<String, dynamic> json) {
    id = json['_id']?.toString();
    name = json['name']?.toString();
    email = json['email']?.toString();
    phone = json['phone']?.toString();
    avatar = json['avatar']?.toString();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['_id'] = id;
    data['name'] = name;
    data['email'] = email;
    data['phone'] = phone;
    data['avatar'] = avatar;
    return data;
  }
}

class VoteUserChallenge extends BaseModel {
  String? id;
  VoteUserChallengeUser? user;
  String? media;
  String? challenge;
  int? votesCount;
  int? saveCount;
  String? status;
  int? reSubmitCount;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  int? v;
  bool? userSaved;
  bool? userVoted;
  bool? userLiked;
  int? likedCount;

  VoteUserChallenge({
    this.id,
    this.user,
    this.media,
    this.challenge,
    this.votesCount,
    this.saveCount,
    this.status,
    this.reSubmitCount,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.userSaved,
    this.userVoted,
    this.userLiked,
    this.likedCount,
  });
  @override
  VoteUserChallenge fromJson(Map<String, dynamic> json) {
    return VoteUserChallenge.fromJson(json);
  }

  VoteUserChallenge.fromJson(Map<String, dynamic> json) {
    id = json['_id']?.toString();
    user = (json['user'] != null) ? VoteUserChallengeUser.fromJson(json['user']) : null;
    media = json['media']?.toString();
    challenge = json['challenge']?.toString();
    votesCount = json['votesCount']?.toInt();
    saveCount = json['saveCount']?.toInt();
    status = json['status']?.toString();
    reSubmitCount = json['reSubmitCount']?.toInt();
    removed = json['removed'];
    createdAt = json['createdAt']?.toString();
    updatedAt = json['updatedAt']?.toString();
    v = json['__v']?.toInt();
    userSaved = json['userSaved'];
    userVoted = json['userVoted'];
    userLiked = json['userLiked'];
    likedCount = json['likedCount']?.toInt();
  }
  @override
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['_id'] = id;
    if (user != null) {
      data['user'] = user!.toJson();
    }
    data['media'] = media;
    data['challenge'] = challenge;
    data['votesCount'] = votesCount;
    data['saveCount'] = saveCount;
    data['status'] = status;
    data['reSubmitCount'] = reSubmitCount;
    data['removed'] = removed;
    data['createdAt'] = createdAt;
    data['updatedAt'] = updatedAt;
    data['__v'] = v;
    data['userSaved'] = userSaved;
    data['userVoted'] = userVoted;
    data['userLiked'] = userLiked;
    data['likedCount'] = likedCount;
    return data;
  }
}
