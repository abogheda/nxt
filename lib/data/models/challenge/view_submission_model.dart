import '../base_models/base_model.dart';

class ViewSubmissionModel extends BaseModel {
  ViewSubmissionModel({
    this.reSubmitCount,
    this.id,
    this.user,
    this.media,
    this.challenge,
    this.votesCount,
    this.saveCount,
    this.status,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.userVoted,
    this.userSaved,
  });
  @override
  ViewSubmissionModel fromJson(Map<String, dynamic> json) {
    return ViewSubmissionModel.fromJson(json);
  }

  ViewSubmissionModel.fromJson(dynamic json) {
    reSubmitCount = json['reSubmitCount'];
    id = json['_id'];
    user = json['user'];
    media = json['media'];
    challenge = json['challenge'] != null ? Challenge.fromJson(json['challenge']) : null;
    votesCount = json['votesCount'];
    saveCount = json['saveCount'];
    status = json['status'];
    removed = json['removed'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    v = json['__v'];
    userVoted = json['userVoted'];
    userSaved = json['userSaved'];
  }
  String? id;
  String? user;
  String? media;
  Challenge? challenge;
  num? votesCount;
  num? saveCount;
  num? reSubmitCount;
  String? status;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  num? v;
  bool? userVoted;
  bool? userSaved;
  ViewSubmissionModel copyWith({
    String? id,
    String? user,
    String? media,
    Challenge? challenge,
    num? votesCount,
    num? saveCount,
    num? reSubmitCount,
    String? status,
    bool? removed,
    String? createdAt,
    String? updatedAt,
    num? v,
    bool? userVoted,
    bool? userSaved,
  }) =>
      ViewSubmissionModel(
        id: id ?? this.id,
        user: user ?? this.user,
        media: media ?? this.media,
        challenge: challenge ?? this.challenge,
        votesCount: votesCount ?? this.votesCount,
        saveCount: saveCount ?? this.saveCount,
        reSubmitCount: reSubmitCount ?? this.reSubmitCount,
        status: status ?? this.status,
        removed: removed ?? this.removed,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
        v: v ?? this.v,
        userVoted: userVoted ?? this.userVoted,
        userSaved: userSaved ?? this.userSaved,
      );
  @override
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['reSubmitCount'] = reSubmitCount;
    map['_id'] = id;
    map['user'] = user;
    map['media'] = media;
    if (challenge != null) {
      map['challenge'] = challenge?.toJson();
    }
    map['votesCount'] = votesCount;
    map['saveCount'] = saveCount;
    map['status'] = status;
    map['removed'] = removed;
    map['createdAt'] = createdAt;
    map['updatedAt'] = updatedAt;
    map['__v'] = v;
    map['userVoted'] = userVoted;
    map['userSaved'] = userSaved;
    return map;
  }
}

class Challenge {
  Challenge({
    this.id,
    this.lang,
    this.prize,
    this.media,
    this.uploadType,
    this.category,
    this.released,
    this.registerationStartDate,
    this.registerationEndDate,
    this.votingStartDate,
    this.votingEndDate,
    this.winnerAnnouncementDate,
    this.featured,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
  });

  Challenge.fromJson(dynamic json) {
    id = json['_id'];
    if (json['lang'] != null) {
      lang = [];
      json['lang'].forEach((v) {
        lang?.add(Lang.fromJson(v));
      });
    }
    prize = json['prize'];
    media = json['media'];
    uploadType = json['uploadType'];
    category = json['category'] != null ? Category.fromJson(json['category']) : null;
    released = json['released'];
    registerationStartDate = json['registerationStartDate'];
    registerationEndDate = json['registerationEndDate'];
    votingStartDate = json['votingStartDate'];
    votingEndDate = json['votingEndDate'];
    winnerAnnouncementDate = json['winnerAnnouncementDate'];
    featured = json['featured'];
    removed = json['removed'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    v = json['__v'];
  }
  String? id;
  List<Lang>? lang;
  String? prize;
  String? media;
  String? uploadType;
  Category? category;
  bool? released;
  String? registerationStartDate;
  String? registerationEndDate;
  String? votingStartDate;
  String? votingEndDate;
  String? winnerAnnouncementDate;
  bool? featured;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  num? v;
  Challenge copyWith({
    String? id,
    List<Lang>? lang,
    String? prize,
    String? media,
    String? uploadType,
    Category? category,
    bool? released,
    String? registerationStartDate,
    String? registerationEndDate,
    String? votingStartDate,
    String? votingEndDate,
    String? winnerAnnouncementDate,
    bool? featured,
    bool? removed,
    String? createdAt,
    String? updatedAt,
    num? v,
  }) =>
      Challenge(
        id: id ?? this.id,
        lang: lang ?? this.lang,
        prize: prize ?? this.prize,
        media: media ?? this.media,
        uploadType: uploadType ?? this.uploadType,
        category: category ?? this.category,
        released: released ?? this.released,
        registerationStartDate: registerationStartDate ?? this.registerationStartDate,
        registerationEndDate: registerationEndDate ?? this.registerationEndDate,
        votingStartDate: votingStartDate ?? this.votingStartDate,
        votingEndDate: votingEndDate ?? this.votingEndDate,
        winnerAnnouncementDate: winnerAnnouncementDate ?? this.winnerAnnouncementDate,
        featured: featured ?? this.featured,
        removed: removed ?? this.removed,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
        v: v ?? this.v,
      );
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['_id'] = id;
    if (lang != null) {
      map['lang'] = lang?.map((v) => v.toJson()).toList();
    }
    map['prize'] = prize;
    map['media'] = media;
    map['uploadType'] = uploadType;
    if (category != null) {
      map['category'] = category?.toJson();
    }
    map['released'] = released;
    map['registerationStartDate'] = registerationStartDate;
    map['registerationEndDate'] = registerationEndDate;
    map['votingStartDate'] = votingStartDate;
    map['votingEndDate'] = votingEndDate;
    map['winnerAnnouncementDate'] = winnerAnnouncementDate;
    map['featured'] = featured;
    map['removed'] = removed;
    map['createdAt'] = createdAt;
    map['updatedAt'] = updatedAt;
    map['__v'] = v;
    return map;
  }
}

class Category {
  Category({
    this.id,
    this.color,
    this.image,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.featured,
    this.title,
    this.description,
  });

  Category.fromJson(dynamic json) {
    id = json['_id'];
    color = json['color'];
    image = json['image'];
    removed = json['removed'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    v = json['__v'];
    featured = json['featured'];
    title = json['title'];
    description = json['description'];
  }
  String? id;
  String? color;
  String? image;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  num? v;
  bool? featured;
  String? title;
  String? description;
  Category copyWith({
    String? id,
    String? color,
    String? image,
    bool? removed,
    String? createdAt,
    String? updatedAt,
    num? v,
    bool? featured,
    String? title,
    String? description,
  }) =>
      Category(
        id: id ?? this.id,
        color: color ?? this.color,
        image: image ?? this.image,
        removed: removed ?? this.removed,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
        v: v ?? this.v,
        featured: featured ?? this.featured,
        title: title ?? this.title,
        description: description ?? this.description,
      );
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['_id'] = id;
    map['color'] = color;
    map['image'] = image;
    map['removed'] = removed;
    map['createdAt'] = createdAt;
    map['updatedAt'] = updatedAt;
    map['__v'] = v;
    map['featured'] = featured;
    map['title'] = title;
    map['description'] = description;
    return map;
  }
}

class Lang {
  Lang({
    this.title,
    this.supTitle,
    this.description,
    this.requirements,
    this.terms,
  });

  Lang.fromJson(dynamic json) {
    title = json['title'];
    supTitle = json['supTitle'];
    description = json['description'];
    requirements = json['requirements'];
    terms = json['terms'];
  }
  String? title;
  String? supTitle;
  String? description;
  String? requirements;
  String? terms;
  Lang copyWith({
    String? title,
    String? supTitle,
    String? description,
    String? requirements,
    String? terms,
  }) =>
      Lang(
        title: title ?? this.title,
        supTitle: supTitle ?? this.supTitle,
        description: description ?? this.description,
        requirements: requirements ?? this.requirements,
        terms: terms ?? this.terms,
      );
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['title'] = title;
    map['supTitle'] = supTitle;
    map['description'] = description;
    map['requirements'] = requirements;
    map['terms'] = terms;
    return map;
  }
}
