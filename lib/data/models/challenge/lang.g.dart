// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'lang.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Lang _$LangFromJson(Map<String, dynamic> json) => Lang(
      title: json['title'] as String?,
      description: json['description'] as String?,
      requirements: json['requirements'] as String?,
    );

Map<String, dynamic> _$LangToJson(Lang instance) => <String, dynamic>{
      'title': instance.title,
      'description': instance.description,
      'requirements': instance.requirements,
    };
