import 'package:json_annotation/json_annotation.dart';
import '../base_models/base_model.dart';
import 'lang.dart';
import '../classes/category.dart';
import '../home/home.dart';

part 'challenge.g.dart';

@JsonSerializable()
class Challenge extends BaseModel {
  @JsonKey(name: '_id')
  String? id;
  String? title;
  String? supTitle;
  String? description;
  String? requirements;
  String? prize;
  String? media;
  List<Lang>? lang;
  Category? category;
  DateTime? registerationStartDate;
  DateTime? registerationEndDate;
  DateTime? votingStartDate;
  DateTime? votingEndDate;
  DateTime? winnerAnnouncementDate;
  bool? featured;
  bool? removed;
  bool? userJoined;
  bool? userSaved;
  bool? released;
  DateTime? createdAt;
  DateTime? updatedAt;
  @JsonKey(name: '__v')
  int? v;

  Challenge(
      {this.id,
      this.title,
      this.supTitle,
      this.description,
      this.requirements,
      this.prize,
      this.lang,
      this.media,
      this.category,
      this.registerationStartDate,
      this.registerationEndDate,
      this.votingStartDate,
      this.votingEndDate,
      this.winnerAnnouncementDate,
      this.featured,
      this.removed,
      this.userJoined,
      this.userSaved,
      this.released,
      this.createdAt,
      this.updatedAt,
      this.v});

  factory Challenge.fromJson(Map<String, dynamic> json) => _$ChallengeFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$ChallengeToJson(this);

  factory Challenge.fromHomeChallenge(HomeModel model) => Challenge(
        id: model.id,
        title: model.title,
        prize: model.prize,
        media: model.media,
        category: model.category,
        supTitle: model.supTitle,
        createdAt: model.createdAt,
        updatedAt: model.updatedAt,
        description: model.description,
        requirements: model.requirements,
        votingEndDate: model.votingEndDate,
        votingStartDate: model.votingStartDate,
        registerationEndDate: model.registerationEndDate,
        registerationStartDate: model.registerationStartDate,
        winnerAnnouncementDate: model.winnerAnnouncementDate,
      );

  // factory Challenge.fromSearch(SearchJobAndChallengesModel model) => Challenge(
  //       id: model.id,
  //       title: model.title,
  //       supTitle: model.supTitle,
  //       description: model.description,
  //       media: model.media,
  //       createdAt: DateTime.parse(model.createdAt!),
  //       category: Category(color: model.category!.color),
  //     );

  @override
  Challenge fromJson(Map<String, dynamic> json) {
    return Challenge.fromJson(json);
  }
}
