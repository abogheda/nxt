class PlanPriceModel {
  String? sId;
  List<Lang>? lang;
  int? price;
  int? googlePlayPrice;
  int? appleStoerPrice;
  int? months;
  bool? display;
  int? descount;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  int? iV;

  PlanPriceModel(
      {this.sId,
        this.lang,
        this.price,
        this.googlePlayPrice,
        this.appleStoerPrice,
        this.months,
        this.display,
        this.descount,
        this.removed,
        this.createdAt,
        this.updatedAt,
        this.iV});

  PlanPriceModel.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    if (json['lang'] != null) {
      lang = <Lang>[];
      json['lang'].forEach((v) {
        lang!.add(Lang.fromJson(v));
      });
    }
    price = json['price'];
    googlePlayPrice = json['googlePlayPrice'];
    appleStoerPrice = json['appleStoerPrice'];
    months = json['months'];
    display = json['display'];
    descount = json['descount'];
    removed = json['removed'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    iV = json['__v'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['_id'] = sId;
    if (lang != null) {
      data['lang'] = lang!.map((v) => v.toJson()).toList();
    }
    data['price'] = price;
    data['googlePlayPrice'] = googlePlayPrice;
    data['appleStoerPrice'] = appleStoerPrice;
    data['months'] = months;
    data['display'] = display;
    data['descount'] = descount;
    data['removed'] = removed;
    data['createdAt'] = createdAt;
    data['updatedAt'] = updatedAt;
    data['__v'] = iV;
    return data;
  }
}

class Lang {
  String? title;
  String? supTitle;
  List<String>? features;

  Lang({this.title, this.supTitle, this.features});

  Lang.fromJson(Map<String, dynamic> json) {
    title = json['title'];
    supTitle = json['supTitle'];
    features = json['features'].cast<String>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['title'] = title;
    data['supTitle'] = supTitle;
    data['features'] = features;
    return data;
  }
}
