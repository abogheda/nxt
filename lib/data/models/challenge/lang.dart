// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:json_annotation/json_annotation.dart';

part 'lang.g.dart';

@JsonSerializable()
class Lang {
  String? title;
  String? description;
  String? requirements;

  Lang({
    this.title,
    this.description,
    this.requirements,
  });
  factory Lang.fromJson(Map<String, dynamic> json) =>
      _$LangFromJson(json);
  
  Map<String, dynamic> toJson() => _$LangToJson(this);
}
