import '../base_models/base_model.dart';

class ChallengeStatus extends BaseModel {
  ChallengeStatus({
    this.user,
    this.media,
    this.challenge,
    this.votesCount,
    this.saveCount,
    this.status,
    this.removed,
    this.id,
    this.createdAt,
    this.updatedAt,
    this.v,
  });
  @override
  ChallengeStatus fromJson(Map<String, dynamic> json) {
    return ChallengeStatus.fromJson(json);
  }

  ChallengeStatus.fromJson(dynamic json) {
    user = json['user'];
    media = json['media'];
    challenge = json['challenge'];
    votesCount = json['votesCount'];
    saveCount = json['saveCount'];
    status = json['status'];
    removed = json['removed'];
    id = json['_id'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    v = json['__v'];
  }
  String? user;
  String? media;
  String? challenge;
  num? votesCount;
  num? saveCount;
  String? status;
  bool? removed;
  String? id;
  String? createdAt;
  String? updatedAt;
  num? v;
  ChallengeStatus copyWith({
    String? user,
    String? media,
    String? challenge,
    num? votesCount,
    num? saveCount,
    String? status,
    bool? removed,
    String? id,
    String? createdAt,
    String? updatedAt,
    num? v,
  }) =>
      ChallengeStatus(
        user: user ?? this.user,
        media: media ?? media,
        challenge: challenge ?? this.challenge,
        votesCount: votesCount ?? this.votesCount,
        saveCount: saveCount ?? this.saveCount,
        status: status ?? this.status,
        removed: removed ?? this.removed,
        id: id ?? this.id,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
        v: v ?? this.v,
      );
  @override
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['user'] = user;
    map['media'] = media;
    map['challenge'] = challenge;
    map['votesCount'] = votesCount;
    map['saveCount'] = saveCount;
    map['status'] = status;
    map['removed'] = removed;
    map['_id'] = id;
    map['createdAt'] = createdAt;
    map['updatedAt'] = updatedAt;
    map['__v'] = v;
    return map;
  }
}
