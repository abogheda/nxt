// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'talent_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TalentModel _$TalentModelFromJson(Map<String, dynamic> json) => TalentModel(
      id: json['_id'] as String?,
      title: json['title'] as String?,
    );

Map<String, dynamic> _$TalentModelToJson(TalentModel instance) =>
    <String, dynamic>{
      '_id': instance.id,
      'title': instance.title,
    };
