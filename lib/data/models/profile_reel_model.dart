import 'base_models/base_model.dart';

class ProfileReelModel extends BaseModel {
  ProfileReelModel({
    this.id,
    this.title,
    this.text,
    this.media,
    this.user,
    this.uploadType,
    this.votesCount,
    this.saveCount,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
  });

  @override
  ProfileReelModel fromJson(Map<String, dynamic> json) {
    return ProfileReelModel.fromJson(json);
  }

  ProfileReelModel.fromJson(dynamic json) {
    id = json['_id'];
    title = json['title'];
    text = json['text'];
    media = json['media'];
    user = json['user'];
    uploadType = json['uploadType'];
    votesCount = json['votesCount'];
    saveCount = json['saveCount'];
    removed = json['removed'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    v = json['__v'];
  }
  String? id;
  String? title;
  String? text;
  String? media;
  String? user;
  String? uploadType;
  num? votesCount;
  num? saveCount;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  num? v;
  ProfileReelModel copyWith({
    String? id,
    String? title,
    String? text,
    String? media,
    String? user,
    String? uploadType,
    num? votesCount,
    num? saveCount,
    bool? removed,
    String? createdAt,
    String? updatedAt,
    num? v,
  }) =>
      ProfileReelModel(
        id: id ?? this.id,
        title: title ?? this.title,
        text: text ?? this.text,
        media: media ?? this.media,
        user: user ?? this.user,
        uploadType: uploadType ?? this.uploadType,
        votesCount: votesCount ?? this.votesCount,
        saveCount: saveCount ?? this.saveCount,
        removed: removed ?? this.removed,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
        v: v ?? this.v,
      );
  @override
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['_id'] = id;
    map['title'] = title;
    map['text'] = text;
    map['media'] = media;
    map['user'] = user;
    map['uploadType'] = uploadType;
    map['votesCount'] = votesCount;
    map['saveCount'] = saveCount;
    map['removed'] = removed;
    map['createdAt'] = createdAt;
    map['updatedAt'] = updatedAt;
    map['__v'] = v;
    return map;
  }
}
