import '../base_models/base_model.dart';
import '../home/home.dart';
import 'category.dart' as category_model;

class MasterClass extends BaseModel {
  MasterClass({
    this.id,
    this.category,
    this.poster,
    this.author,
    this.homeAuthorModel,
    this.duration,
    this.language,
    this.featured,
    this.display,
    this.tags,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.userSaved,
    this.released,
    this.title,
    this.description,
    this.details,
  });
  @override
  MasterClass fromJson(Map<String, dynamic> json) => MasterClass.fromJson(json);

  factory MasterClass.fromHomeModel(HomeModel model) => MasterClass(
        id: model.id,
        title: model.title,
        poster: model.poster,
        details: model.details,
        language: model.language,
        released: model.released,
        duration: model.duration,
        category: model.category!,
        description: model.description,
        homeAuthorModel: model.homeAuthorModel,
        createdAt: model.createdAt!.toIso8601String(),
        updatedAt: model.updatedAt!.toIso8601String(),
        author: Author(name: model.author!.name ?? 'name not available'),
      );

  MasterClass.fromJson(dynamic json) {
    id = json['_id'];
    category = json['category'] != null ? category_model.Category.fromJson(json['category']) : null;
    poster = json['poster'];
    author = json['author'] != null ? Author.fromJson(json['author']) : null;
    homeAuthorModel = json['author'] != null ? HomeAuthorModel.fromJson(json['author']) : null;
    duration = json['duration'];
    language = json['language'];
    featured = json['featured'];
    display = json['display'];
    if (json['tags'] != null) {
      tags = [];
      json['tags'].forEach((v) {
        tags?.add(Tags.fromJson(v));
      });
    }
    removed = json['removed'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    v = json['__v'];
    userSaved = json['userSaved'];
    released = json['released'];
    title = json['title'];
    description = json['description'];
    details = json['details'];
  }
  String? id;
  category_model.Category? category;
  String? poster;
  Author? author;
  HomeAuthorModel? homeAuthorModel;
  String? duration;
  String? language;
  bool? featured;
  bool? display;
  List<Tags>? tags;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  num? v;
  bool? userSaved;
  bool? released;
  String? title;
  String? description;
  String? details;
  MasterClass copyWith({
    String? id,
    category_model.Category? category,
    String? poster,
    Author? author,
    HomeAuthorModel? homeAuthorModel,
    String? duration,
    String? language,
    bool? featured,
    bool? display,
    List<Tags>? tags,
    bool? removed,
    String? createdAt,
    String? updatedAt,
    num? v,
    bool? userSaved,
    bool? released,
    String? title,
    String? description,
    String? details,
  }) =>
      MasterClass(
        v: v ?? this.v,
        id: id ?? this.id,
        tags: tags ?? this.tags,
        title: title ?? this.title,
        author: author ?? this.author,
        poster: poster ?? this.poster,
        display: display ?? this.display,
        details: details ?? this.details,
        removed: removed ?? this.removed,
        released: released ?? this.released,
        duration: duration ?? this.duration,
        language: language ?? this.language,
        featured: featured ?? this.featured,
        category: category ?? this.category,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
        userSaved: userSaved ?? this.userSaved,
        description: description ?? this.description,
        homeAuthorModel: homeAuthorModel ?? this.homeAuthorModel,
      );
  @override
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['_id'] = id;
    if (category != null) {
      map['category'] = category?.toJson();
    }
    map['poster'] = poster;
    if (author != null) {
      map['author'] = author?.toJson();
    }
    map['duration'] = duration;
    map['language'] = language;
    map['featured'] = featured;
    map['display'] = display;
    if (tags != null) {
      map['tags'] = tags?.map((v) => v.toJson()).toList();
    }
    map['removed'] = removed;
    map['createdAt'] = createdAt;
    map['updatedAt'] = updatedAt;
    map['__v'] = v;
    map['userSaved'] = userSaved;
    map['released'] = released;
    map['title'] = title;
    map['description'] = description;
    map['details'] = details;
    return map;
  }
}

class Tags {
  Tags({
    this.id,
    this.text,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
  });

  Tags.fromJson(dynamic json) {
    id = json['_id'];
    text = json['text'];
    removed = json['removed'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    v = json['__v'];
  }
  String? id;
  String? text;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  num? v;
  Tags copyWith({
    String? id,
    String? text,
    bool? removed,
    String? createdAt,
    String? updatedAt,
    num? v,
  }) =>
      Tags(
        id: id ?? this.id,
        text: text ?? this.text,
        removed: removed ?? this.removed,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
        v: v ?? this.v,
      );
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['_id'] = id;
    map['text'] = text;
    map['removed'] = removed;
    map['createdAt'] = createdAt;
    map['updatedAt'] = updatedAt;
    map['__v'] = v;
    return map;
  }
}

class Author {
  Author({
    this.id,
    this.avatar,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.name,
    this.description,
    this.nationality,
    this.position,
  });

  Author.fromJson(dynamic json) {
    id = json['_id'];
    avatar = json['avatar'];
    removed = json['removed'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    v = json['__v'];
    name = json['name'];
    description = json['description'];
    nationality = json['nationality'];
    position = json['position'];
  }
  String? id;
  String? name;
  String? avatar;
  String? description;
  String? nationality;
  String? position;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  num? v;
  Author copyWith({
    String? id,
    String? avatar,
    bool? removed,
    String? createdAt,
    String? updatedAt,
    num? v,
    String? name,
    String? description,
    String? nationality,
    String? position,
  }) =>
      Author(
        id: id ?? this.id,
        avatar: avatar ?? this.avatar,
        removed: removed ?? this.removed,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
        v: v ?? this.v,
        name: name ?? this.name,
        description: description ?? this.description,
        nationality: nationality ?? this.nationality,
        position: position ?? this.position,
      );
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['_id'] = id;
    map['avatar'] = avatar;
    map['removed'] = removed;
    map['createdAt'] = createdAt;
    map['updatedAt'] = updatedAt;
    map['__v'] = v;
    map['name'] = name;
    map['description'] = description;
    map['nationality'] = nationality;
    map['position'] = position;
    return map;
  }
}

class Category {
  Category({
    this.id,
    this.color,
    this.image,
    this.featured,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.title,
    this.description,
  });

  Category.fromJson(dynamic json) {
    id = json['_id'];
    color = json['color'];
    image = json['image'];
    featured = json['featured'];
    removed = json['removed'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    v = json['__v'];
    title = json['title'];
    description = json['description'];
  }
  String? id;
  String? color;
  String? image;
  bool? featured;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  num? v;
  String? title;
  String? description;
  Category copyWith({
    String? id,
    String? color,
    String? image,
    bool? featured,
    bool? removed,
    String? createdAt,
    String? updatedAt,
    num? v,
    String? title,
    String? description,
  }) =>
      Category(
        id: id ?? this.id,
        color: color ?? this.color,
        image: image ?? this.image,
        featured: featured ?? this.featured,
        removed: removed ?? this.removed,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
        v: v ?? this.v,
        title: title ?? this.title,
        description: description ?? this.description,
      );
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['_id'] = id;
    map['color'] = color;
    map['image'] = image;
    map['featured'] = featured;
    map['removed'] = removed;
    map['createdAt'] = createdAt;
    map['updatedAt'] = updatedAt;
    map['__v'] = v;
    map['title'] = title;
    map['description'] = description;
    return map;
  }
}

///
/// Code generated by jsonToDartModel https://ashamp.github.io/jsonToDartModel/
///
class HomeAuthorModelLang {
/*
{
  "name": "PROFESSOR X",
  "description": "ACTOR",
  "nationality": "EGYPTIAN",
  "position": "ACTING SYNDICATE BOARD MEMBER"
}
*/

  String? name;
  String? description;
  String? nationality;
  String? position;

  HomeAuthorModelLang({
    this.name,
    this.description,
    this.nationality,
    this.position,
  });
  HomeAuthorModelLang.fromJson(Map<String, dynamic> json) {
    name = json['name']?.toString();
    description = json['description']?.toString();
    nationality = json['nationality']?.toString();
    position = json['position']?.toString();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['name'] = name;
    data['description'] = description;
    data['nationality'] = nationality;
    data['position'] = position;
    return data;
  }
}

class HomeAuthorModel {
  String? id;
  List<HomeAuthorModelLang?>? lang;
  String? avatar;
  String? name;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  int? v;

  HomeAuthorModel({
    this.id,
    this.lang,
    this.avatar,
    this.name,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
  });
  HomeAuthorModel.fromJson(Map<String, dynamic> json) {
    id = json['_id']?.toString();
    if (json['lang'] != null) {
      final v = json['lang'];
      final arr0 = <HomeAuthorModelLang>[];
      v.forEach((v) {
        arr0.add(HomeAuthorModelLang.fromJson(v));
      });
      lang = arr0;
    }
    avatar = json['avatar']?.toString();
    name = json['name']?.toString();
    removed = json['removed'];
    createdAt = json['createdAt']?.toString();
    updatedAt = json['updatedAt']?.toString();
    v = json['__v']?.toInt();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['_id'] = id;
    if (lang != null) {
      final v = lang;
      final arr0 = [];
      for (var v in v!) {
        arr0.add(v!.toJson());
      }
      data['lang'] = arr0;
    }
    data['avatar'] = avatar;
    data['name'] = name;
    data['removed'] = removed;
    data['createdAt'] = createdAt;
    data['updatedAt'] = updatedAt;
    data['__v'] = v;
    return data;
  }
}
