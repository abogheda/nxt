import 'package:json_annotation/json_annotation.dart';

part 'author.g.dart';

@JsonSerializable()
class Author {
  @JsonKey(name: '_id')
  String? id;
  String? name;
  String? avatar;
  String? description;
  String? nationality;
  String? position;
  bool? removed;
  DateTime? createdAt;
  DateTime? updatedAt;
  @JsonKey(name: '__v')
  int? v;

  Author(
      {this.id,
      this.name,
      this.avatar,
      this.description,
      this.nationality,
      this.position,
      this.removed,
      this.createdAt,
      this.updatedAt,
      this.v});

  factory Author.fromJson(Map<String, dynamic> json) => Author(
        id: json['_id'] as String?,
        name: json['name'] as String?,
        avatar: json['avatar'] as String?,
        description: json['description'] as String?,
        nationality: json['nationality'] as String?,
        position: json['position'] as String?,
        removed: json['removed'] as bool?,
        createdAt: json['createdAt'] == null ? null : DateTime.parse(json['createdAt'] as String),
        updatedAt: json['updatedAt'] == null ? null : DateTime.parse(json['updatedAt'] as String),
        v: json['__v'] as int?,
      );

  // factory Author.fromJson(Map<String, dynamic> json) =>
  //     _$AuthorFromJson(json);

  Map<String, dynamic> toJson() => _$AuthorToJson(this);
}
