import 'package:json_annotation/json_annotation.dart';

import '../challenge/lang.dart';

part 'category.g.dart';

@JsonSerializable()
class Category {
  @JsonKey(name: '_id')
  String? id;
  String? color;
  String? title;
  String? description;
  bool? removed;
  List<Lang>? lang;
  DateTime? createdAt;
  DateTime? updatedAt;
  @JsonKey(name: '__v')
  int? v;

  Category(
      {this.id,
      this.title,
      this.description,
      this.color,
      this.removed,
      this.createdAt,
      this.updatedAt,
      this.v,
      this.lang});

  factory Category.fromJson(Map<String, dynamic> json) => _$CategoryFromJson(json);

  Map<String, dynamic> toJson() => _$CategoryToJson(this);
}
