import '../base_models/base_model.dart';

class WatchedLectureModel extends BaseModel {
  WatchedLectureModel({
    this.lecture,
    this.user,
    this.nxtClass,
    this.id,
    this.createdAt,
    this.updatedAt,
    this.v,
  });
  @override
  WatchedLectureModel fromJson(Map<String, dynamic> json) {
    return WatchedLectureModel.fromJson(json);
  }

  WatchedLectureModel.fromJson(dynamic json) {
    lecture = json['lecture'];
    user = json['user'];
    nxtClass = json['nxtClass'];
    id = json['_id'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    v = json['__v'];
  }
  String? lecture;
  String? user;
  String? nxtClass;
  String? id;
  String? createdAt;
  String? updatedAt;
  num? v;
  WatchedLectureModel copyWith({
    String? lecture,
    String? user,
    String? nxtClass,
    String? id,
    String? createdAt,
    String? updatedAt,
    num? v,
  }) =>
      WatchedLectureModel(
        lecture: lecture ?? this.lecture,
        user: user ?? this.user,
        nxtClass: nxtClass ?? this.nxtClass,
        id: id ?? this.id,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
        v: v ?? this.v,
      );
  @override
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['lecture'] = lecture;
    map['user'] = user;
    map['nxtClass'] = nxtClass;
    map['_id'] = id;
    map['createdAt'] = createdAt;
    map['updatedAt'] = updatedAt;
    map['__v'] = v;
    return map;
  }
}
