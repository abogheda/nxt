import '../base_models/base_model.dart';

class PaymentMethod extends BaseModel{
  String? id;
  String? name;

  PaymentMethod({this.id, this.name});

  PaymentMethod.fromJson(json) {
    id = json['_id'];
    name = json['name'];
  }

  @override
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['_id'] = id;
    data['name'] = name;
    return data;
  }

  @override
  fromJson(Map<String, dynamic> json) {
    return PaymentMethod.fromJson(json);
  }
}
