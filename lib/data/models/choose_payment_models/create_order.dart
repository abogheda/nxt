import 'package:nxt/data/models/base_models/base_model.dart';

class CreateOrderModel extends BaseModel{
  String? type;
  String? ref;

  CreateOrderModel({this.type, this.ref});

  CreateOrderModel.fromJson(json) {
    type = json['type'];
    ref = json['ref'];
  }

  @override
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['type'] = type;
    data['ref'] = ref;
    return data;
  }

  @override
  fromJson(Map<String, dynamic> json) {
    return CreateOrderModel.fromJson(json);
  }
}
