import 'package:json_annotation/json_annotation.dart';
import 'package:nxt/data/models/base_models/base_model.dart';

part 'get_member_ship_model.g.dart'; 

@JsonSerializable(nullable: true, ignoreUnannotated: false)
class GetMemberShipModel extends BaseModel{
  @JsonKey(name: '_id')
  String? id;
  @JsonKey(name: 'lang')
  List<Lang>? lang;
  @JsonKey(name: 'removed')
  bool? removed;
  @JsonKey(name: 'createdAt')
  String? createdAt;
  @JsonKey(name: 'updatedAt')
  String? updatedAt;
  @JsonKey(name: '__v')
  int? iV;

  GetMemberShipModel({this.id, this.lang, this.removed, this.createdAt, this.updatedAt, this.iV});

   factory GetMemberShipModel.fromJson(json) => _$GetMemberShipModelFromJson(json);

   @override
  Map<String, dynamic> toJson() => _$GetMemberShipModelToJson(this);

  @override
  fromJson(Map<String, dynamic> json) {
    // TODO: implement fromJson
    return GetMemberShipModel.fromJson(json);
  }
}

@JsonSerializable(nullable: true, ignoreUnannotated: false)
class Lang {
  @JsonKey(name: 'title')
  String? title;

  Lang({this.title});

   factory Lang.fromJson(Map<String, dynamic> json) => _$LangFromJson(json);

   Map<String, dynamic> toJson() => _$LangToJson(this);
}

