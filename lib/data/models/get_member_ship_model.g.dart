// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'get_member_ship_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GetMemberShipModel _$GetMemberShipModelFromJson(Map<String, dynamic> json) =>
    GetMemberShipModel(
      id: json['_id'] as String?,
      lang: (json['lang'] as List<dynamic>?)
          ?.map((e) => Lang.fromJson(e as Map<String, dynamic>))
          .toList(),
      removed: json['removed'] as bool?,
      createdAt: json['createdAt'] as String?,
      updatedAt: json['updatedAt'] as String?,
      iV: json['__v'] as int?,
    );

Map<String, dynamic> _$GetMemberShipModelToJson(GetMemberShipModel instance) =>
    <String, dynamic>{
      '_id': instance.id,
      'lang': instance.lang,
      'removed': instance.removed,
      'createdAt': instance.createdAt,
      'updatedAt': instance.updatedAt,
      '__v': instance.iV,
    };

Lang _$LangFromJson(Map<String, dynamic> json) => Lang(
      title: json['title'] as String?,
    );

Map<String, dynamic> _$LangToJson(Lang instance) => <String, dynamic>{
      'title': instance.title,
    };
