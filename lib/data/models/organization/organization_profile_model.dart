import '../base_models/base_model.dart';

class OrganizationProfileModel extends BaseModel {
  OrganizationProfileModel({
    this.id,
    this.name,
    this.email,
    this.phone,
    this.password,
    this.role,
    this.avatar,
    this.provider,
    this.providerId,
    this.profileStatus,
    this.terms,
    this.privacy,
    this.organization,
    this.expert,
    this.firstSubscription,
    this.subscriptionEnd,
    this.invitation,
    this.verify,
    this.verifyPhone,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.country,
    this.countryCode,
    this.countryName,
  });
  @override
  OrganizationProfileModel fromJson(Map<String, dynamic> json) {
    return OrganizationProfileModel.fromJson(json);
  }

  OrganizationProfileModel.fromJson(dynamic json) {
    id = json['_id'];
    name = json['name'];
    email = json['email'];
    phone = json['phone'];
    password = json['password'];
    role = json['role'];
    avatar = json['avatar'];
    provider = json['provider'];
    providerId = json['providerId'];
    profileStatus = json['profileStatus'];
    terms = json['terms'];
    privacy = json['privacy'];
    organization = json['organization'] != null ? Organization.fromJson(json['organization']) : null;
    expert = json['expert'];
    firstSubscription = json['firstSubscription'];
    subscriptionEnd = json['subscriptionEnd'];
    invitation = json['invitation'];
    verify = json['verify'];
    verifyPhone = json['verifyPhone'];
    removed = json['removed'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    v = json['__v'];
    country = json['country'];
    countryCode = json['countryCode'];
    countryName = json['countryName'];
  }
  String? id;
  String? name;
  String? email;
  String? phone;
  String? password;
  String? role;
  String? avatar;
  String? provider;
  dynamic providerId;
  String? profileStatus;
  bool? terms;
  bool? privacy;
  Organization? organization;
  dynamic expert;
  bool? firstSubscription;
  bool? subscriptionEnd;
  bool? invitation;
  bool? verify;
  bool? verifyPhone;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  num? v;
  String? country;
  String? countryCode;
  String? countryName;
  OrganizationProfileModel copyWith({
    String? id,
    String? name,
    String? email,
    String? phone,
    String? password,
    String? role,
    String? avatar,
    String? provider,
    dynamic providerId,
    String? profileStatus,
    bool? terms,
    bool? privacy,
    Organization? organization,
    dynamic expert,
    bool? firstSubscription,
    bool? subscriptionEnd,
    bool? invitation,
    bool? verify,
    bool? verifyPhone,
    bool? removed,
    String? createdAt,
    String? updatedAt,
    num? v,
    String? country,
    String? countryCode,
    String? countryName,
  }) =>
      OrganizationProfileModel(
        id: id ?? this.id,
        name: name ?? this.name,
        email: email ?? this.email,
        phone: phone ?? this.phone,
        password: password ?? this.password,
        role: role ?? this.role,
        avatar: avatar ?? this.avatar,
        provider: provider ?? this.provider,
        providerId: providerId ?? this.providerId,
        profileStatus: profileStatus ?? this.profileStatus,
        terms: terms ?? this.terms,
        privacy: privacy ?? this.privacy,
        organization: organization ?? this.organization,
        expert: expert ?? this.expert,
        firstSubscription: firstSubscription ?? this.firstSubscription,
        subscriptionEnd: subscriptionEnd ?? this.subscriptionEnd,
        invitation: invitation ?? this.invitation,
        verify: verify ?? this.verify,
        verifyPhone: verifyPhone ?? this.verifyPhone,
        removed: removed ?? this.removed,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
        v: v ?? this.v,
        country: country ?? this.country,
        countryCode: countryCode ?? this.countryCode,
        countryName: countryName ?? this.countryName,
      );
  @override
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['_id'] = id;
    map['name'] = name;
    map['email'] = email;
    map['phone'] = phone;
    map['password'] = password;
    map['role'] = role;
    map['avatar'] = avatar;
    map['provider'] = provider;
    map['providerId'] = providerId;
    map['profileStatus'] = profileStatus;
    map['terms'] = terms;
    map['privacy'] = privacy;
    if (organization != null) {
      map['organization'] = organization?.toJson();
    }
    map['expert'] = expert;
    map['firstSubscription'] = firstSubscription;
    map['subscriptionEnd'] = subscriptionEnd;
    map['invitation'] = invitation;
    map['verify'] = verify;
    map['verifyPhone'] = verifyPhone;
    map['removed'] = removed;
    map['createdAt'] = createdAt;
    map['updatedAt'] = updatedAt;
    map['__v'] = v;
    map['country'] = country;
    map['countryCode'] = countryCode;
    map['countryName'] = countryName;
    return map;
  }
}

class Organization {
  Organization({
    this.id,
    this.name,
    this.website,
    this.industry,
    this.size,
    this.type,
    this.logo,
    this.promocode,
    this.userCount,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
  });

  Organization.fromJson(dynamic json) {
    id = json['_id'];
    name = json['name'];
    website = json['website'];
    industry = json['industry'];
    size = json['size'];
    type = json['type'];
    logo = json['logo'];
    promocode = json['promocode'];
    userCount = json['userCount'];
    removed = json['removed'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    v = json['__v'];
  }
  String? id;
  String? name;
  String? website;
  String? industry;
  int? size;
  String? type;
  String? logo;
  String? promocode;
  num? userCount;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  num? v;
  Organization copyWith({
    String? id,
    String? name,
    String? website,
    String? industry,
    int? size,
    String? type,
    String? logo,
    String? promocode,
    num? userCount,
    bool? removed,
    String? createdAt,
    String? updatedAt,
    num? v,
  }) =>
      Organization(
        id: id ?? this.id,
        name: name ?? this.name,
        website: website ?? this.website,
        industry: industry ?? this.industry,
        size: size ?? this.size,
        type: type ?? this.type,
        logo: logo ?? this.logo,
        promocode: promocode ?? this.promocode,
        userCount: userCount ?? this.userCount,
        removed: removed ?? this.removed,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
        v: v ?? this.v,
      );
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['_id'] = id;
    map['name'] = name;
    map['website'] = website;
    map['industry'] = industry;
    map['size'] = size;
    map['type'] = type;
    map['logo'] = logo;
    map['promocode'] = promocode;
    map['userCount'] = userCount;
    map['removed'] = removed;
    map['createdAt'] = createdAt;
    map['updatedAt'] = updatedAt;
    map['__v'] = v;
    return map;
  }
}
