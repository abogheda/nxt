class CreateWorkModel {
  CreateWorkModel({
    this.lang,
    this.poster,
  });

  CreateWorkModel.fromJson(dynamic json) {
    if (json['lang'] != null) {
      lang = [];
      json['lang'].forEach((v) {
        lang?.add(Lang.fromJson(v));
      });
    }
    poster = json['poster'];
  }
  List<Lang>? lang;
  String? poster;
  CreateWorkModel copyWith({
    List<Lang>? lang,
    String? poster,
  }) =>
      CreateWorkModel(
        lang: lang ?? this.lang,
        poster: poster ?? this.poster,
      );
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (lang != null) {
      map['lang'] = lang?.map((v) => v.toJson()).toList();
    }
    map['poster'] = poster;
    return map;
  }
}

class Lang {
  Lang({
    this.tag,
    this.description,
  });

  Lang.fromJson(dynamic json) {
    tag = json['tag'];
    description = json['description'];
  }
  String? tag;
  String? description;
  Lang copyWith({
    String? tag,
    String? description,
  }) =>
      Lang(
        tag: tag ?? this.tag,
        description: description ?? this.description,
      );
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['tag'] = tag;
    map['description'] = description;
    return map;
  }
}
