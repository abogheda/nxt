import '../base_models/base_model.dart';

class WorkModel extends BaseModel {
  String? id;
  String? poster;
  String? organization;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  int? v;
  List<WorkModelLang?>? lang;

  WorkModel({
    this.id,
    this.poster,
    this.organization,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.lang,
  });
  @override
  WorkModel fromJson(Map<String, dynamic> json) {
    return WorkModel.fromJson(json);
  }

  WorkModel.fromJson(Map<String, dynamic> json) {
    id = json['_id']?.toString();
    poster = json['poster']?.toString();
    organization = json['organization']?.toString();
    removed = json['removed'];
    createdAt = json['createdAt']?.toString();
    updatedAt = json['updatedAt']?.toString();
    v = json['__v']?.toInt();
    if (json['lang'] != null) {
      final v = json['lang'];
      final arr0 = <WorkModelLang>[];
      v.forEach((v) {
        arr0.add(WorkModelLang.fromJson(v));
      });
      lang = arr0;
    }
  }
  @override
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['_id'] = id;
    data['poster'] = poster;
    data['organization'] = organization;
    data['removed'] = removed;
    data['createdAt'] = createdAt;
    data['updatedAt'] = updatedAt;
    data['__v'] = v;
    if (lang != null) {
      final v = lang;
      final arr0 = [];
      for (var v in v!) {
        arr0.add(v!.toJson());
      }
      data['lang'] = arr0;
    }
    return data;
  }
}

class WorkModelLang {
  String? tag;
  String? description;

  WorkModelLang({
    this.tag,
    this.description,
  });
  WorkModelLang.fromJson(Map<String, dynamic> json) {
    tag = json['tag']?.toString();
    description = json['description']?.toString();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['tag'] = tag;
    data['description'] = description;
    return data;
  }
}
