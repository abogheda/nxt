// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'lectures.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Lectures _$LecturesFromJson(Map<String, dynamic> json) => Lectures(
      id: json['_id'] as String?,
      title: json['title'] as String?,
      description: json['description'] as String?,
      nxtClass: json['nxtClass'] as String?,
      duration: json['duration'] as String?,
      media: json['media'] as String?,
      order: json['order'] as int?,
      removed: json['removed'] as bool?,
      createdAt: json['createdAt'] == null
          ? null
          : DateTime.parse(json['createdAt'] as String),
      updatedAt: json['updatedAt'] == null
          ? null
          : DateTime.parse(json['updatedAt'] as String),
      v: json['__v'] as int?,
      watched: json['watched'] as bool?,
    );

Map<String, dynamic> _$LecturesToJson(Lectures instance) => <String, dynamic>{
      '_id': instance.id,
      'title': instance.title,
      'description': instance.description,
      'nxtClass': instance.nxtClass,
      'duration': instance.duration,
      'media': instance.media,
      'order': instance.order,
      'removed': instance.removed,
      'createdAt': instance.createdAt?.toIso8601String(),
      'updatedAt': instance.updatedAt?.toIso8601String(),
      '__v': instance.v,
      'watched': instance.watched,
    };
