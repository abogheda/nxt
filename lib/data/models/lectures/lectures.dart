import 'package:json_annotation/json_annotation.dart';

import '../base_models/base_model.dart';

part 'lectures.g.dart';

@JsonSerializable()
class Lectures extends BaseModel {
  @JsonKey(name: '_id')
  String? id;
  String? title;
  String? description;
  String? nxtClass;
  String? duration;
  String? media;
  int? order;
  bool? removed;
  DateTime? createdAt;
  DateTime? updatedAt;
  @JsonKey(name: '__v')
  int? v;
  bool? watched;

  Lectures(
      {this.id,
      this.title,
      this.description,
      this.nxtClass,
      this.duration,
      this.media,
      this.order,
      this.removed,
      this.createdAt,
      this.updatedAt,
      this.v,
      this.watched});

  factory Lectures.fromJson(Map<String, dynamic> json) => _$LecturesFromJson(json);
  @override
  Map<String, dynamic> toJson() => _$LecturesToJson(this);

  @override
  Lectures fromJson(Map<String, dynamic> json) {
    return Lectures.fromJson(json);
  }
}
