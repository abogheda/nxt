import 'base_models/base_model.dart';
import 'package/package_model.dart';

class UserModel extends BaseModel {
  UserModel({
    this.token,
    this.user,
    this.statusCode,
    this.message,
    this.error,
  });
  @override
  UserModel fromJson(Map<String, dynamic> json) {
    return UserModel.fromJson(json);
  }

  UserModel.fromJson(dynamic json) {
    token = json['token'];
    user = json['user'] != null ? User.fromJson(json['user']) : null;
    statusCode = json['statusCode'];
    message = json['message'];
    error = json['error'];
  }
  String? token;
  User? user;
  num? statusCode;
  String? message;
  String? error;
  UserModel copyWith({
    String? token,
    User? user,
    num? statusCode,
    String? message,
    String? error,
  }) =>
      UserModel(
        token: token ?? this.token,
        user: user ?? this.user,
        statusCode: statusCode ?? this.statusCode,
        message: message ?? this.message,
        error: error ?? this.error,
      );
  @override
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['token'] = token;
    if (user != null) {
      map['user'] = user?.toJson();
      map['statusCode'] = statusCode;
      map['message'] = message;
      map['error'] = error;
    }
    return map;
  }
}

class User extends BaseModel {
  User({
    this.id,
    this.bio,
    this.name,
    this.package,
    this.email,
    this.phone,
    this.myInvitationCode,
    this.birthDate,
    this.invitationExpiryDate,
    this.password,
    this.role,
    this.avatar,
    this.provider,
    this.googleId,
    this.facebookId,
    this.providerId,
    this.profileStatus,
    this.organization,
    this.verify,
    this.verifyPhone,
    this.verifyCode,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.privacy,
    this.terms,
    this.eula,
    this.firstSubscription,
    this.subscriptionEnd,
    this.invitation,
    this.country,
    this.countryCode,
    this.countryName,
    this.expert,
    this.confirmPhone,
  });
  @override
  User fromJson(Map<String, dynamic> json) {
    return User.fromJson(json);
  }

  User.fromJson(dynamic json) {
    id = json['_id'];
    bio = json['bio'];
    name = json['name'];
    email = json['email'];
    phone = json['phone'];
    myInvitationCode = json['myInvitationCode'];
    birthDate = json['birthDate'];
    invitationExpiryDate = json['invitationExpiryDate'];
    password = json['password'];
    role = json['role'];
    avatar = json['avatar'];
    provider = json['provider'];
    providerId = json['providerId'];
    profileStatus = json['profileStatus'];
    verify = json['verify'];
    verifyPhone = json['verifyPhone'];
    removed = json['removed'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    v = json['__v'];
    privacy = json['privacy'];
    terms = json['terms'];
    eula = json['eula'];
    firstSubscription = json['firstSubscription'];
    subscriptionEnd = json['subscriptionEnd'];
    invitation = json['invitation'];
    country = json['country'];
    countryCode = json['countryCode'];
    countryName = json['countryName'];
    confirmPhone = json['confirmPhone'] != null ? ConfirmPhone.fromJson(json['confirmPhone']) : null;
    package = json['package'] != null ? Package.fromJson(json['package']) : null;
    expert = (json['expert'] != null) ? Expert.fromJson(json['expert']) : null;
    organization = (json['organization'] != null) ? Organization.fromJson(json['organization']) : null;
  }
  String? name;
  String? bio;
  String? email;
  String? phone;
  String? myInvitationCode;
  String? birthDate;
  String? password;
  String? role;
  String? avatar;
  String? provider;
  String? profileStatus;
  bool? terms;
  bool? privacy;
  bool? eula;
  bool? firstSubscription;
  bool? subscriptionEnd;
  bool? invitation;
  bool? verify;
  bool? verifyPhone;
  String? id;
  String? createdAt;
  String? updatedAt;
  Package? package;
  String? invitationExpiryDate;
  String? googleId;
  String? facebookId;
  dynamic providerId;
  dynamic verifyCode;
  bool? removed;
  num? v;
  String? country;
  String? countryCode;
  String? countryName;
  ConfirmPhone? confirmPhone;
  Expert? expert;
  Organization? organization;

  User copyWith({
    String? id,
    String? name,
    String? bio,
    Package? package,
    String? email,
    String? phone,
    String? myInvitationCode,
    String? birthDate,
    String? invitationExpiryDate,
    String? password,
    String? role,
    String? avatar,
    String? provider,
    String? googleId,
    String? facebookId,
    dynamic providerId,
    String? profileStatus,
    bool? verify,
    bool? verifyPhone,
    dynamic verifyCode,
    bool? removed,
    String? createdAt,
    String? updatedAt,
    num? v,
    ConfirmPhone? confirmPhone,
    bool? privacy,
    bool? terms,
    bool? eula,
    bool? firstSubscription,
    bool? subscriptionEnd,
    bool? invitation,
    String? country,
    String? countryCode,
    String? countryName,
    Expert? expert,
    Organization? organization,
  }) =>
      User(
        id: id ?? this.id,
        name: name ?? this.name,
        bio: bio ?? this.bio,
        package: package ?? this.package,
        email: email ?? this.email,
        phone: phone ?? this.phone,
        myInvitationCode: myInvitationCode ?? this.myInvitationCode,
        birthDate: birthDate ?? this.birthDate,
        invitationExpiryDate: invitationExpiryDate ?? this.invitationExpiryDate,
        password: password ?? this.password,
        role: role ?? this.role,
        avatar: avatar ?? this.avatar,
        provider: provider ?? this.provider,
        googleId: googleId ?? this.googleId,
        facebookId: facebookId ?? this.facebookId,
        providerId: providerId ?? this.providerId,
        profileStatus: profileStatus ?? this.profileStatus,
        verify: verify ?? this.verify,
        verifyPhone: verifyPhone ?? this.verifyPhone,
        verifyCode: verifyCode ?? this.verifyCode,
        removed: removed ?? this.removed,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
        v: v ?? this.v,
        confirmPhone: confirmPhone ?? this.confirmPhone,
        privacy: privacy ?? this.privacy,
        terms: terms ?? this.terms,
        eula: eula ?? this.eula,
        firstSubscription: firstSubscription ?? this.firstSubscription,
        subscriptionEnd: subscriptionEnd ?? this.subscriptionEnd,
        invitation: invitation ?? this.invitation,
        country: country ?? this.country,
        countryCode: countryCode ?? this.countryCode,
        countryName: countryName ?? this.countryName,
        expert: expert ?? this.expert,
        organization: organization ?? this.organization,
      );
  @override
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['_id'] = id;
    map['name'] = name;
    map['bio'] = bio;
    if (package != null) {
      map['package'] = package!.toJson();
    }
    map['email'] = email;
    map['phone'] = phone;
    map['myInvitationCode'] = myInvitationCode;
    map['birthDate'] = birthDate;
    map['invitationExpiryDate'] = invitationExpiryDate;
    map['password'] = password;
    map['role'] = role;
    map['avatar'] = avatar;
    map['provider'] = provider;
    map['googleId'] = googleId;
    map['facebookId'] = facebookId;
    map['providerId'] = providerId;
    map['profileStatus'] = profileStatus;
    map['verify'] = verify;
    map['verifyPhone'] = verifyPhone;
    map['verifyCode'] = verifyCode;
    map['removed'] = removed;
    map['createdAt'] = createdAt;
    map['updatedAt'] = updatedAt;
    map['__v'] = v;
    if (confirmPhone != null) {
      map['confirmPhone'] = confirmPhone?.toJson();
    }
    map['privacy'] = privacy;
    map['terms'] = terms;
    map['eula'] = eula;
    map['firstSubscription'] = firstSubscription;
    map['subscriptionEnd'] = subscriptionEnd;
    map['invitation'] = invitation;
    map['country'] = country;
    map['countryCode'] = countryCode;
    map['countryName'] = countryName;
    if (expert != null) {
      map['expert'] = expert?.toJson();
    }
    if (organization != null) {
      map['organization'] = organization?.toJson();
    }
    return map;
  }
}

class ConfirmPhone {
  ConfirmPhone({this.codesended, this.expiresAt});

  ConfirmPhone.fromJson(dynamic json) {
    codesended = json['codesended'];
    expiresAt = json['expiresAt'];
  }
  num? codesended;
  String? expiresAt;
  ConfirmPhone copyWith({
    num? codesended,
    String? expiresAt,
  }) =>
      ConfirmPhone(
        codesended: codesended ?? this.codesended,
        expiresAt: expiresAt ?? this.expiresAt,
      );
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['codesended'] = codesended;
    map['expiresAt'] = expiresAt;
    return map;
  }
}

class Expert {
  String? name;
  String? website;
  String? scope;
  String? logo;

  Expert({
    this.name,
    this.website,
    this.scope,
    this.logo,
  });
  Expert.fromJson(dynamic json) {
    name = json['name']?.toString();
    website = json['website']?.toString();
    scope = json['scope']?.toString();
    logo = json['logo']?.toString();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['name'] = name;
    data['website'] = website;
    data['scope'] = scope;
    data['logo'] = logo;
    return data;
  }
}

class Organization {
  String? id;
  String? name;
  String? website;
  String? industry;
  String? size;
  String? type;
  String? logo;
  String? promocode;
  int? userCount;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  int? v;

  Organization({
    this.id,
    this.name,
    this.website,
    this.industry,
    this.size,
    this.type,
    this.logo,
    this.promocode,
    this.userCount,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
  });
  Organization.fromJson(dynamic json) {
    id = json['_id']?.toString();
    name = json['name']?.toString();
    website = json['website']?.toString();
    industry = json['industry']?.toString();
    size = json['size']?.toString();
    type = json['type']?.toString();
    logo = json['logo']?.toString();
    promocode = json['promocode']?.toString();
    userCount = json['userCount']?.toInt();
    removed = json['removed'];
    createdAt = json['createdAt']?.toString();
    updatedAt = json['updatedAt']?.toString();
    v = json['__v']?.toInt();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['_id'] = id;
    data['name'] = name;
    data['website'] = website;
    data['industry'] = industry;
    data['size'] = size;
    data['type'] = type;
    data['logo'] = logo;
    data['promocode'] = promocode;
    data['userCount'] = userCount;
    data['removed'] = removed;
    data['createdAt'] = createdAt;
    data['updatedAt'] = updatedAt;
    data['__v'] = v;
    return data;
  }
}
