import 'package:nxt/data/models/base_models/base_model.dart';

class SkillsModel extends BaseModel {
  String? sId;
  String? title;

  SkillsModel({this.sId, this.title});

  SkillsModel.fromJson(json) {
    sId = json['_id'];
    title = json['title'];
  }

  @override
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['_id'] = sId;
    data['title'] = title;
    return data;
  }

  @override
  fromJson(Map<String, dynamic> json) {
    return SkillsModel.fromJson(json);
  }
}
