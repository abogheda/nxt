import '../base_models/base_model.dart';

class TagModel extends BaseModel {
  TagModel({
    this.id,
    this.text,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
  });
  @override
  TagModel fromJson(Map<String, dynamic> json) {
    return TagModel.fromJson(json);
  }

  TagModel.fromJson(dynamic json) {
    id = json['_id'];
    text = json['text'];
    removed = json['removed'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    v = json['__v'];
  }
  String? id;
  String? text;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  num? v;
  TagModel copyWith({
    String? id,
    String? text,
    bool? removed,
    String? createdAt,
    String? updatedAt,
    num? v,
  }) =>
      TagModel(
        id: id ?? this.id,
        text: text ?? this.text,
        removed: removed ?? this.removed,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
        v: v ?? this.v,
      );
  @override
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['_id'] = id;
    map['text'] = text;
    map['removed'] = removed;
    map['createdAt'] = createdAt;
    map['updatedAt'] = updatedAt;
    map['__v'] = v;
    return map;
  }
}
