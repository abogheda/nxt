class CreateJobModel {
  List<PostJobModelLang?>? lang;
  String? gender;
  String? ageFrom;
  String? ageTo;
  String? poster;
  String? uploadType;
  String? category;
  List<String?>? country;
  String? jobStartDate;
  String? jobEndDate;

  CreateJobModel({
    this.lang,
    this.gender,
    this.ageFrom,
    this.ageTo,
    this.poster,
    this.uploadType,
    this.category,
    this.country,
    this.jobStartDate,
    this.jobEndDate,
  });
  CreateJobModel.fromJson(Map<String, dynamic> json) {
    if (json['lang'] != null) {
      final v = json['lang'];
      final arr0 = <PostJobModelLang>[];
      v.forEach((v) {
        arr0.add(PostJobModelLang.fromJson(v));
      });
      lang = arr0;
    }
    gender = json['gender']?.toString();
    ageFrom = json['ageFrom']?.toString();
    ageTo = json['ageTo']?.toString();
    poster = json['poster']?.toString();
    uploadType = json['uploadType']?.toString();
    category = json['category']?.toString();
    if (json['country'] != null) {
      final v = json['country'];
      final arr0 = <String>[];
      v.forEach((v) {
        arr0.add(v.toString());
      });
      country = arr0;
    }
    jobStartDate = json['jobStartDate']?.toString();
    jobEndDate = json['jobEndDate']?.toString();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (lang != null) {
      final v = lang;
      final arr0 = [];
      for (var v in v!) {
        arr0.add(v!.toJson());
      }
      data['lang'] = arr0;
    }
    data['gender'] = gender;
    data['ageFrom'] = ageFrom;
    data['ageTo'] = ageTo;
    data['poster'] = poster;
    data['uploadType'] = uploadType;
    data['category'] = category;
    if (country != null) {
      final v = country;
      final arr0 = [];
      for (var v in v!) {
        arr0.add(v);
      }
      data['country'] = arr0;
    }
    data['jobStartDate'] = jobStartDate;
    data['jobEndDate'] = jobEndDate;
    return data;
  }
}

class PostJobModelLang {
  String? title;
  String? supTitle;
  String? description;
  String? area;

  PostJobModelLang({
    this.title,
    this.supTitle,
    this.description,
    this.area,
  });
  PostJobModelLang.fromJson(Map<String, dynamic> json) {
    title = json['title']?.toString();
    supTitle = json['supTitle']?.toString();
    description = json['description']?.toString();
    area = json['area']?.toString();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['title'] = title;
    data['supTitle'] = supTitle;
    data['description'] = description;
    data['area'] = area;
    return data;
  }
}
