import '../base_models/base_model.dart';

class CreatePostModel extends BaseModel {
  CreatePostModel({
    this.text,
    this.media,
    this.author,
    this.tags,
    this.status,
    this.removed,
    this.id,
    this.createdAt,
    this.updatedAt,
    this.v,
  });
  @override
  CreatePostModel fromJson(Map<String, dynamic> json) {
    return CreatePostModel.fromJson(json);
  }

  CreatePostModel.fromJson(dynamic json) {
    text = json['text'];
    media = json['media'] != null ? json['media'].cast<String>() : [];
    author = json['author'];
    tags = json['tags'] != null ? json['tags'].cast<String>() : [];
    status = json['status'];
    removed = json['removed'];
    id = json['_id'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    v = json['__v'];
  }
  String? text;
  List<String>? media;
  String? author;
  List<String>? tags;
  String? status;
  bool? removed;
  String? id;
  String? createdAt;
  String? updatedAt;
  num? v;
  CreatePostModel copyWith({
    String? text,
    List<String>? media,
    String? author,
    List<String>? tags,
    String? status,
    bool? removed,
    String? id,
    String? createdAt,
    String? updatedAt,
    num? v,
  }) =>
      CreatePostModel(
        text: text ?? this.text,
        media: media ?? this.media,
        author: author ?? this.author,
        tags: tags ?? this.tags,
        status: status ?? this.status,
        removed: removed ?? this.removed,
        id: id ?? this.id,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
        v: v ?? this.v,
      );
  @override
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['text'] = text;
    map['media'] = media;
    map['author'] = author;
    map['tags'] = tags;
    map['status'] = status;
    map['removed'] = removed;
    map['_id'] = id;
    map['createdAt'] = createdAt;
    map['updatedAt'] = updatedAt;
    map['__v'] = v;
    return map;
  }
}
