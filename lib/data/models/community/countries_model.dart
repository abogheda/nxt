import '../base_models/base_model.dart';

class CountriesModel extends BaseModel {
  CountriesModel({
    this.id,
    this.name,
    this.lat,
    this.long,
    this.azureCode,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
  });
  @override
  CountriesModel fromJson(Map<String, dynamic> json) {
    return CountriesModel.fromJson(json);
  }

  CountriesModel.fromJson(dynamic json) {
    id = json['_id'];
    name = json['name'];
    lat = json['lat'];
    long = json['long'];
    azureCode = json['azureCode'];
    removed = json['removed'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    v = json['__v'];
  }
  String? id;
  String? name;
  String? lat;
  String? long;
  String? azureCode;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  num? v;
  CountriesModel copyWith({
    String? id,
    String? name,
    String? lat,
    String? long,
    String? azureCode,
    bool? removed,
    String? createdAt,
    String? updatedAt,
    num? v,
  }) =>
      CountriesModel(
        id: id ?? this.id,
        name: name ?? this.name,
        lat: lat ?? this.lat,
        long: long ?? this.long,
        azureCode: azureCode ?? this.azureCode,
        removed: removed ?? this.removed,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
        v: v ?? this.v,
      );
  @override
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['_id'] = id;
    map['name'] = name;
    map['lat'] = lat;
    map['long'] = long;
    map['azureCode'] = azureCode;
    map['removed'] = removed;
    map['createdAt'] = createdAt;
    map['updatedAt'] = updatedAt;
    map['__v'] = v;
    return map;
  }
}
