import '../base_models/base_model.dart';

class DeletePost extends BaseModel {
  DeletePost({
    this.id,
    this.text,
    this.media,
    this.author,
    this.tags,
    this.status,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
  });
  @override
  DeletePost fromJson(Map<String, dynamic> json) {
    return DeletePost.fromJson(json);
  }

  DeletePost.fromJson(dynamic json) {
    id = json['_id'];
    text = json['text'];
    media = json['media'] != null ? json['media'].cast<String>() : [];
    author = json['author'];
    tags = json['tags'] != null ? json['tags'].cast<String>() : [];
    status = json['status'];
    removed = json['removed'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    v = json['__v'];
  }
  String? id;
  String? text;
  List<String>? media;
  String? author;
  List<String>? tags;
  String? status;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  num? v;
  DeletePost copyWith({
    String? id,
    String? text,
    List<String>? media,
    String? author,
    List<String>? tags,
    String? status,
    bool? removed,
    String? createdAt,
    String? updatedAt,
    num? v,
  }) =>
      DeletePost(
        id: id ?? this.id,
        text: text ?? this.text,
        media: media ?? this.media,
        author: author ?? this.author,
        tags: tags ?? this.tags,
        status: status ?? this.status,
        removed: removed ?? this.removed,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
        v: v ?? this.v,
      );
  @override
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['_id'] = id;
    map['text'] = text;
    map['media'] = media;
    map['author'] = author;
    map['tags'] = tags;
    map['status'] = status;
    map['removed'] = removed;
    map['createdAt'] = createdAt;
    map['updatedAt'] = updatedAt;
    map['__v'] = v;
    return map;
  }
}
