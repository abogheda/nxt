import '../base_models/base_model.dart';

class LikesModel extends BaseModel {
  String? user;
  String? contentId;
  String? contentType;
  String? id;
  String? createdAt;
  String? updatedAt;
  int? v;

  LikesModel({
    this.user,
    this.contentId,
    this.contentType,
    this.id,
    this.createdAt,
    this.updatedAt,
    this.v,
  });
  @override
  LikesModel fromJson(Map<String, dynamic> json) {
    return LikesModel.fromJson(json);
  }

  LikesModel.fromJson(Map<String, dynamic> json) {
    user = json['user']?.toString();
    contentId = json['contentId']?.toString();
    contentType = json['contentType']?.toString();
    id = json['_id']?.toString();
    createdAt = json['createdAt']?.toString();
    updatedAt = json['updatedAt']?.toString();
    v = json['__v']?.toInt();
  }
  @override
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['user'] = user;
    data['contentId'] = contentId;
    data['contentType'] = contentType;
    data['_id'] = id;
    data['createdAt'] = createdAt;
    data['updatedAt'] = updatedAt;
    data['__v'] = v;
    return data;
  }
}
