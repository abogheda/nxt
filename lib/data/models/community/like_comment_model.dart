import '../base_models/base_model.dart';

class LikeCommentModel extends BaseModel {
  LikeCommentModel({
    this.id,
    this.user,
    this.post,
    this.comment,
    this.media,
    this.likesCount,
    this.createdAt,
    this.updatedAt,
    this.v,
  });
  @override
  LikeCommentModel fromJson(Map<String, dynamic> json) {
    return LikeCommentModel.fromJson(json);
  }

  LikeCommentModel.fromJson(dynamic json) {
    id = json['_id'];
    user = json['user'];
    post = json['post'];
    comment = json['comment'];
    media = json['media'];
    likesCount = json['likesCount'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    v = json['__v'];
  }
  String? id;
  String? user;
  String? post;
  String? comment;
  dynamic media;
  num? likesCount;
  String? createdAt;
  String? updatedAt;
  num? v;
  LikeCommentModel copyWith({
    String? id,
    String? user,
    String? post,
    String? comment,
    dynamic media,
    num? likesCount,
    String? createdAt,
    String? updatedAt,
    num? v,
  }) =>
      LikeCommentModel(
        id: id ?? this.id,
        user: user ?? this.user,
        post: post ?? this.post,
        comment: comment ?? this.comment,
        media: media ?? this.media,
        likesCount: likesCount ?? this.likesCount,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
        v: v ?? this.v,
      );
  @override
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['_id'] = id;
    map['user'] = user;
    map['post'] = post;
    map['comment'] = comment;
    map['media'] = media;
    map['likesCount'] = likesCount;
    map['createdAt'] = createdAt;
    map['updatedAt'] = updatedAt;
    map['__v'] = v;
    return map;
  }
}
