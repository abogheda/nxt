import '../base_models/base_model.dart';

class PostModel extends BaseModel {
  String? id;
  String? text;
  List<String?>? media;
  PostModelAuthor? author;
  List<String>? tags;
  String? status;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  int? v;
  bool? userSaved;
  bool? userLiked;
  int? likedCount;

  PostModel({
    this.id,
    this.text,
    this.media,
    this.author,
    this.tags,
    this.status,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.userSaved,
    this.userLiked,
    this.likedCount,
  });
  @override
  PostModel fromJson(Map<String, dynamic> json) {
    return PostModel.fromJson(json);
  }

  PostModel.fromJson(Map<String, dynamic> json) {
    id = json['_id']?.toString();
    text = json['text']?.toString();
    if (json['media'] != null) {
      final v = json['media'];
      final arr0 = <String>[];
      v.forEach((v) {
        arr0.add(v.toString());
      });
      media = arr0;
    }
    author = (json['author'] != null) ? PostModelAuthor.fromJson(json['author']) : null;

    status = json['status']?.toString();
    removed = json['removed'];
    createdAt = json['createdAt']?.toString();
    updatedAt = json['updatedAt']?.toString();
    v = json['__v']?.toInt();
    userSaved = json['userSaved'];
    userLiked = json['userLiked'];
    likedCount = json['likedCount']?.toInt();
  }
  @override
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['_id'] = id;
    data['text'] = text;
    if (media != null) {
      final v = media;
      final arr0 = [];
      for (var v in v!) {
        arr0.add(v);
      }
      data['media'] = arr0;
    }
    if (author != null) {
      data['author'] = author!.toJson();
    }
    data['status'] = status;
    data['removed'] = removed;
    data['createdAt'] = createdAt;
    data['updatedAt'] = updatedAt;
    data['__v'] = v;
    data['userSaved'] = userSaved;
    data['userLiked'] = userLiked;
    data['likedCount'] = likedCount;
    return data;
  }
}

class PostModelAuthor {
  String? id;
  String? name;
  String? avatar;

  PostModelAuthor({
    this.id,
    this.name,
    this.avatar,
  });
  PostModelAuthor.fromJson(Map<String, dynamic> json) {
    id = json['_id']?.toString();
    name = json['name']?.toString();
    avatar = json['avatar']?.toString();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['_id'] = id;
    data['name'] = name;
    data['avatar'] = avatar;
    return data;
  }
}
