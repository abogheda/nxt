import '../base_models/base_model.dart';

class PostJobSuccessModel extends BaseModel {
  PostJobSuccessModel({
    this.gender,
    this.ageTo,
    this.poster,
    this.uploadType,
    this.category,
    this.organization,
    this.country,
    this.jobStartDate,
    this.jobEndDate,
    this.removed,
    this.id,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.title,
    this.supTitle,
    this.description,
    this.area,
  });
  @override
  PostJobSuccessModel fromJson(Map<String, dynamic> json) {
    return PostJobSuccessModel.fromJson(json);
  }

  PostJobSuccessModel.fromJson(dynamic json) {
    gender = json['gender'];
    ageTo = json['ageTo'];
    poster = json['poster'];
    uploadType = json['uploadType'];
    category = json['category'];
    organization = json['organization'];
    // country = json['country'];
    if (json['country'] != null) {
      final v = json['country'];
      final arr1 = <String>[];
      v.forEach((v) {
        arr1.add(v.toString());
      });
      country = arr1;
    }
    jobStartDate = json['jobStartDate'];
    jobEndDate = json['jobEndDate'];
    removed = json['removed'];
    id = json['_id'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    v = json['__v'];
    title = json['title'];
    supTitle = json['supTitle'];
    description = json['description'];
    area = json['area'];
  }
  String? gender;
  String? ageTo;
  String? poster;
  String? uploadType;
  String? category;
  String? organization;
  List<String?>? country;
  String? jobStartDate;
  String? jobEndDate;
  bool? removed;
  String? id;
  String? createdAt;
  String? updatedAt;
  num? v;
  String? title;
  String? supTitle;
  String? description;
  String? area;
  PostJobSuccessModel copyWith({
    String? gender,
    String? ageTo,
    String? poster,
    String? uploadType,
    String? category,
    String? organization,
    String? country,
    String? jobStartDate,
    String? jobEndDate,
    bool? removed,
    String? id,
    String? createdAt,
    String? updatedAt,
    num? v,
    String? title,
    String? supTitle,
    String? description,
    String? area,
  }) =>
      PostJobSuccessModel(
        gender: gender ?? this.gender,
        ageTo: ageTo ?? this.ageTo,
        poster: poster ?? this.poster,
        uploadType: uploadType ?? this.uploadType,
        category: category ?? this.category,
        organization: organization ?? this.organization,
        jobStartDate: jobStartDate ?? this.jobStartDate,
        jobEndDate: jobEndDate ?? this.jobEndDate,
        removed: removed ?? this.removed,
        id: id ?? this.id,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
        v: v ?? this.v,
        title: title ?? this.title,
        supTitle: supTitle ?? this.supTitle,
        description: description ?? this.description,
        area: area ?? this.area,
      );
  @override
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['gender'] = gender;
    map['ageTo'] = ageTo;
    map['poster'] = poster;
    map['uploadType'] = uploadType;
    map['category'] = category;
    map['organization'] = organization;
    if (country != null) {
      final v = country;
      final arr0 = [];
      for (var v in v!) {
        arr0.add(v);
      }
      map['country'] = arr0;
    }
    map['jobStartDate'] = jobStartDate;
    map['jobEndDate'] = jobEndDate;
    map['removed'] = removed;
    map['_id'] = id;
    map['createdAt'] = createdAt;
    map['updatedAt'] = updatedAt;
    map['__v'] = v;
    map['title'] = title;
    map['supTitle'] = supTitle;
    map['description'] = description;
    map['area'] = area;
    return map;
  }
}
