import 'package:nxt/data/models/base_models/base_model.dart';

class PartnerModel extends BaseModel {
  String? id;
  String? name;
  String? website;
  String? industry;
  int? size;
  String? type;
  String? logo;
  String? promocode;
  int? userCount;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  int? v;
  bool? display;
  String? description;

  PartnerModel({
    this.id,
    this.name,
    this.website,
    this.industry,
    this.size,
    this.type,
    this.logo,
    this.promocode,
    this.userCount,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.display,
    this.description,
  });
  @override
  PartnerModel fromJson(Map<String, dynamic> json) {
    return PartnerModel.fromJson(json);
  }

  PartnerModel.fromJson(Map<String, dynamic> json) {
    id = json['_id']?.toString();
    name = json['name']?.toString();
    website = json['website']?.toString();
    industry = json['industry']?.toString();
    size = json['size']?.toInt();
    type = json['type']?.toString();
    logo = json['logo']?.toString();
    promocode = json['promocode']?.toString();
    userCount = json['userCount']?.toInt();
    removed = json['removed'];
    createdAt = json['createdAt']?.toString();
    updatedAt = json['updatedAt']?.toString();
    v = json['__v']?.toInt();
    display = json['display'];
    description = json['description']?.toString();
  }
  @override
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['_id'] = id;
    data['name'] = name;
    data['website'] = website;
    data['industry'] = industry;
    data['size'] = size;
    data['type'] = type;
    data['logo'] = logo;
    data['promocode'] = promocode;
    data['userCount'] = userCount;
    data['removed'] = removed;
    data['createdAt'] = createdAt;
    data['updatedAt'] = updatedAt;
    data['__v'] = v;
    data['display'] = display;
    data['description'] = description;
    return data;
  }
}
