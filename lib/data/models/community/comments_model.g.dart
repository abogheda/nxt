// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'comments_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CommentModel _$CommentModelFromJson(Map<String, dynamic> json) => CommentModel(
      id: json['_id'] as String?,
      user: json['user'] as String?,
      post: json['post'] as String?,
      comment: json['comment'] as String?,
      media: json['media'] as String?,
      userLiked: json['userLiked'] as bool?,
      likesCount: json['likesCount'] as int?,
      likes:
          (json['likes'] as List<dynamic>?)?.map((e) => e as String).toList(),
      createdAt: json['createdAt'] == null
          ? null
          : DateTime.parse(json['createdAt'] as String),
      updatedAt: json['updatedAt'] == null
          ? null
          : DateTime.parse(json['updatedAt'] as String),
      v: json['__v'] as int?,
      author: json['author'] == null
          ? null
          : Author.fromJson(json['author'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$CommentModelToJson(CommentModel instance) =>
    <String, dynamic>{
      '_id': instance.id,
      'user': instance.user,
      'post': instance.post,
      'comment': instance.comment,
      'media': instance.media,
      'userLiked': instance.userLiked,
      'likesCount': instance.likesCount,
      'likes': instance.likes,
      'createdAt': instance.createdAt?.toIso8601String(),
      'updatedAt': instance.updatedAt?.toIso8601String(),
      '__v': instance.v,
      'author': instance.author,
    };
