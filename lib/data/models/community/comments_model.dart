// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:json_annotation/json_annotation.dart';
import 'package:nxt/data/models/base_models/base_model.dart';
import 'package:nxt/data/models/classes/author.dart';

part 'comments_model.g.dart';

@JsonSerializable()
class CommentModel extends BaseModel {
  //int? comments;
  @JsonKey(name: '_id')
  String? id;
  String? user;
  String? post;
  String? comment;
  String? media;
  bool? userLiked;
  int? likesCount;
  List<String>? likes;
  DateTime? createdAt;
  DateTime? updatedAt;
  @JsonKey(name: '__v')
  int? v;
  Author? author;

  CommentModel({
    this.id,
    this.user,
    this.post,
    this.comment,
    this.media,
    this.userLiked,
    this.likesCount,
    this.likes,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.author,
  });

  factory CommentModel.fromJson(Map<String, dynamic> json) => _$CommentModelFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$CommentModelToJson(this);

  @override
  CommentModel fromJson(Map<String, dynamic> json) {
    return CommentModel.fromJson(json);
  }
}
