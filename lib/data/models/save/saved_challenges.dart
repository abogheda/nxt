import '../base_models/base_model.dart';

class SavedChallenges extends BaseModel {
  List<SavedChallengesChallenges?>? challenges;

  SavedChallenges({
    this.challenges,
  });
  @override
  SavedChallenges fromJson(Map<String, dynamic> json) {
    return SavedChallenges.fromJson(json);
  }

  SavedChallenges.fromJson(Map<String, dynamic> json) {
    if (json['challenges'] != null) {
      final v = json['challenges'];
      final arr0 = <SavedChallengesChallenges>[];
      v.forEach((v) {
        arr0.add(SavedChallengesChallenges.fromJson(v));
      });
      challenges = arr0;
    }
  }
  @override
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (challenges != null) {
      final v = challenges;
      final arr0 = [];
      for (var v in v!) {
        arr0.add(v!.toJson());
      }
      data['challenges'] = arr0;
    }
    return data;
  }
}

class SavedChallengesChallengesCategoryLang {
  String? title;
  String? description;

  SavedChallengesChallengesCategoryLang({
    this.title,
    this.description,
  });
  SavedChallengesChallengesCategoryLang.fromJson(Map<String, dynamic> json) {
    title = json['title']?.toString();
    description = json['description']?.toString();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['title'] = title;
    data['description'] = description;
    return data;
  }
}

class SavedChallengesChallengesCategory {
  String? id;
  List<SavedChallengesChallengesCategoryLang?>? lang;
  String? color;
  String? image;
  bool? featured;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  int? v;

  SavedChallengesChallengesCategory({
    this.id,
    this.lang,
    this.color,
    this.image,
    this.featured,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
  });
  SavedChallengesChallengesCategory.fromJson(Map<String, dynamic> json) {
    id = json['_id']?.toString();
    if (json['lang'] != null) {
      final v = json['lang'];
      final arr0 = <SavedChallengesChallengesCategoryLang>[];
      v.forEach((v) {
        arr0.add(SavedChallengesChallengesCategoryLang.fromJson(v));
      });
      lang = arr0;
    }
    color = json['color']?.toString();
    image = json['image']?.toString();
    featured = json['featured'];
    removed = json['removed'];
    createdAt = json['createdAt']?.toString();
    updatedAt = json['updatedAt']?.toString();
    v = json['__v']?.toInt();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['_id'] = id;
    if (lang != null) {
      final v = lang;
      final arr0 = [];
      for (var v in v!) {
        arr0.add(v!.toJson());
      }
      data['lang'] = arr0;
    }
    data['color'] = color;
    data['image'] = image;
    data['featured'] = featured;
    data['removed'] = removed;
    data['createdAt'] = createdAt;
    data['updatedAt'] = updatedAt;
    data['__v'] = v;
    return data;
  }
}

class SavedChallengesChallengesLang {
/*
{
  "title": "THE FORMAT",
  "supTitle": "PRODUCING THE FIRST ANONYMOUS EP WITH UP",
  "description": "TALENTS WILL SUBMIT THEIR TRACKS FOR VOTING",
  "requirements": "test",
  "terms": "The terms "
}
*/

  String? title;
  String? supTitle;
  String? description;
  String? requirements;
  String? terms;

  SavedChallengesChallengesLang({
    this.title,
    this.supTitle,
    this.description,
    this.requirements,
    this.terms,
  });
  SavedChallengesChallengesLang.fromJson(Map<String, dynamic> json) {
    title = json['title']?.toString();
    supTitle = json['supTitle']?.toString();
    description = json['description']?.toString();
    requirements = json['requirements']?.toString();
    terms = json['terms']?.toString();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['title'] = title;
    data['supTitle'] = supTitle;
    data['description'] = description;
    data['requirements'] = requirements;
    data['terms'] = terms;
    return data;
  }
}

class SavedChallengesChallenges {
  String? id;
  List<SavedChallengesChallengesLang?>? lang;
  String? prize;
  String? media;
  String? mediaWeb;
  String? mediaView;
  String? uploadType;
  SavedChallengesChallengesCategory? category;
  bool? released;
  String? registerationStartDate;
  String? registerationEndDate;
  String? votingStartDate;
  String? votingEndDate;
  String? winnerAnnouncementDate;
  bool? featured;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  int? v;
  String? winner;
  String? winnerChallenge;

  SavedChallengesChallenges({
    this.id,
    this.lang,
    this.prize,
    this.media,
    this.mediaWeb,
    this.mediaView,
    this.uploadType,
    this.category,
    this.released,
    this.registerationStartDate,
    this.registerationEndDate,
    this.votingStartDate,
    this.votingEndDate,
    this.winnerAnnouncementDate,
    this.featured,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.winner,
    this.winnerChallenge,
  });
  SavedChallengesChallenges.fromJson(Map<String, dynamic> json) {
    id = json['_id']?.toString();
    if (json['lang'] != null) {
      final v = json['lang'];
      final arr0 = <SavedChallengesChallengesLang>[];
      v.forEach((v) {
        arr0.add(SavedChallengesChallengesLang.fromJson(v));
      });
      lang = arr0;
    }
    prize = json['prize']?.toString();
    media = json['media']?.toString();
    mediaWeb = json['mediaWeb']?.toString();
    mediaView = json['mediaView']?.toString();
    uploadType = json['uploadType']?.toString();
    category = (json['category'] != null) ? SavedChallengesChallengesCategory.fromJson(json['category']) : null;
    released = json['released'];
    registerationStartDate = json['registerationStartDate']?.toString();
    registerationEndDate = json['registerationEndDate']?.toString();
    votingStartDate = json['votingStartDate']?.toString();
    votingEndDate = json['votingEndDate']?.toString();
    winnerAnnouncementDate = json['winnerAnnouncementDate']?.toString();
    featured = json['featured'];
    removed = json['removed'];
    createdAt = json['createdAt']?.toString();
    updatedAt = json['updatedAt']?.toString();
    v = json['__v']?.toInt();
    winner = json['winner']?.toString();
    winnerChallenge = json['winnerChallenge']?.toString();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['_id'] = id;
    if (lang != null) {
      final v = lang;
      final arr0 = [];
      for (var v in v!) {
        arr0.add(v!.toJson());
      }
      data['lang'] = arr0;
    }
    data['prize'] = prize;
    data['media'] = media;
    data['mediaWeb'] = mediaWeb;
    data['mediaView'] = mediaView;
    data['uploadType'] = uploadType;
    if (category != null) {
      data['category'] = category!.toJson();
    }
    data['released'] = released;
    data['registerationStartDate'] = registerationStartDate;
    data['registerationEndDate'] = registerationEndDate;
    data['votingStartDate'] = votingStartDate;
    data['votingEndDate'] = votingEndDate;
    data['winnerAnnouncementDate'] = winnerAnnouncementDate;
    data['featured'] = featured;
    data['removed'] = removed;
    data['createdAt'] = createdAt;
    data['updatedAt'] = updatedAt;
    data['__v'] = v;
    data['winner'] = winner;
    data['winnerChallenge'] = winnerChallenge;
    return data;
  }
}
