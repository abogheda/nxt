import '../base_models/base_model.dart';

class SavedPosts extends BaseModel {
  List<SavedPostsPosts?>? posts;

  SavedPosts({
    this.posts,
  });
  @override
  SavedPosts fromJson(Map<String, dynamic> json) {
    return SavedPosts.fromJson(json);
  }

  SavedPosts.fromJson(Map<String, dynamic> json) {
    if (json['posts'] != null) {
      final v = json['posts'];
      final arr0 = <SavedPostsPosts>[];
      v.forEach((v) {
        arr0.add(SavedPostsPosts.fromJson(v));
      });
      posts = arr0;
    }
  }
  @override
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (posts != null) {
      final v = posts;
      final arr0 = [];
      for (var v in v!) {
        arr0.add(v!.toJson());
      }
      data['posts'] = arr0;
    }
    return data;
  }
}

class SavedPostsPostsTags {
  String? id;
  String? text;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  int? v;

  SavedPostsPostsTags({
    this.id,
    this.text,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
  });
  SavedPostsPostsTags.fromJson(Map<String, dynamic> json) {
    id = json['_id']?.toString();
    text = json['text']?.toString();
    removed = json['removed'];
    createdAt = json['createdAt']?.toString();
    updatedAt = json['updatedAt']?.toString();
    v = json['__v']?.toInt();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['_id'] = id;
    data['text'] = text;
    data['removed'] = removed;
    data['createdAt'] = createdAt;
    data['updatedAt'] = updatedAt;
    data['__v'] = v;
    return data;
  }
}

class SavedPostsPostsAuthorSubscription {
  String? package;
  int? price;
  String? expiryDate;
  String? paymentMethod;

  SavedPostsPostsAuthorSubscription({
    this.package,
    this.price,
    this.expiryDate,
    this.paymentMethod,
  });
  SavedPostsPostsAuthorSubscription.fromJson(Map<String, dynamic> json) {
    package = json['package']?.toString();
    price = json['price']?.toInt();
    expiryDate = json['expiryDate']?.toString();
    paymentMethod = json['paymentMethod']?.toString();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['package'] = package;
    data['price'] = price;
    data['expiryDate'] = expiryDate;
    data['paymentMethod'] = paymentMethod;
    return data;
  }
}

class SavedPostsPostsAuthorConfirmPhone {
  String? expiresAt;

  SavedPostsPostsAuthorConfirmPhone({
    this.expiresAt,
  });
  SavedPostsPostsAuthorConfirmPhone.fromJson(Map<String, dynamic> json) {
    expiresAt = json['expiresAt']?.toString();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['expiresAt'] = expiresAt;
    return data;
  }
}

class SavedPostsPostsAuthor {
  String? id;
  String? name;
  String? email;
  String? phone;
  String? birthDate;
  String? password;
  String? role;
  String? avatar;
  String? provider;
  String? providerId;
  String? profileStatus;
  bool? verify;
  bool? verifyPhone;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  int? v;
  SavedPostsPostsAuthorConfirmPhone? confirmPhone;
  bool? privacy;
  bool? terms;
  SavedPostsPostsAuthorSubscription? subscription;
  bool? firstSubscription;

  SavedPostsPostsAuthor({
    this.id,
    this.name,
    this.email,
    this.phone,
    this.birthDate,
    this.password,
    this.role,
    this.avatar,
    this.provider,
    this.providerId,
    this.profileStatus,
    this.verify,
    this.verifyPhone,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.confirmPhone,
    this.privacy,
    this.terms,
    this.subscription,
    this.firstSubscription,
  });
  SavedPostsPostsAuthor.fromJson(Map<String, dynamic> json) {
    id = json['_id']?.toString();
    name = json['name']?.toString();
    email = json['email']?.toString();
    phone = json['phone']?.toString();
    birthDate = json['birthDate']?.toString();
    password = json['password']?.toString();
    role = json['role']?.toString();
    avatar = json['avatar']?.toString();
    provider = json['provider']?.toString();
    providerId = json['providerId'
            '']
        ?.toString();
    profileStatus = json['profileStatus']?.toString();
    verify = json['verify'];
    verifyPhone = json['verifyPhone'];
    removed = json['removed'];
    createdAt = json['createdAt']?.toString();
    updatedAt = json['updatedAt']?.toString();
    v = json['__v']?.toInt();
    confirmPhone =
        (json['confirmPhone'] != null) ? SavedPostsPostsAuthorConfirmPhone.fromJson(json['confirmPhone']) : null;
    privacy = json['privacy'];
    terms = json['terms'];
    subscription =
        (json['subscription'] != null) ? SavedPostsPostsAuthorSubscription.fromJson(json['subscription']) : null;
    firstSubscription = json['firstSubscription'];
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['_id'] = id;
    data['name'] = name;
    data['email'] = email;
    data['phone'] = phone;
    data['birthDate'] = birthDate;
    data['password'] = password;
    data['role'] = role;
    data['avatar'] = avatar;
    data['provider'] = provider;
    data['providerId'
        ''] = providerId;
    data['profileStatus'] = profileStatus;
    data['verify'] = verify;
    data['verifyPhone'] = verifyPhone;
    data['removed'] = removed;
    data['createdAt'] = createdAt;
    data['updatedAt'] = updatedAt;
    data['__v'] = v;
    if (confirmPhone != null) {
      data['confirmPhone'] = confirmPhone!.toJson();
    }
    data['privacy'] = privacy;
    data['terms'] = terms;
    if (subscription != null) {
      data['subscription'] = subscription!.toJson();
    }
    data['firstSubscription'] = firstSubscription;
    return data;
  }
}

class SavedPostsPosts {
  String? id;
  String? text;
  List<String?>? media;
  SavedPostsPostsAuthor? author;
  List<SavedPostsPostsTags?>? tags;
  String? status;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  int? v;

  SavedPostsPosts({
    this.id,
    this.text,
    this.media,
    this.author,
    this.tags,
    this.status,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
  });
  SavedPostsPosts.fromJson(Map<String, dynamic> json) {
    id = json['_id']?.toString();
    text = json['text']?.toString();
    if (json['media'] != null) {
      final v = json['media'];
      final arr0 = <String>[];
      v.forEach((v) {
        arr0.add(v.toString());
      });
      media = arr0;
    }
    author = (json['author'] != null) ? SavedPostsPostsAuthor.fromJson(json['author']) : null;
    if (json['tags'] != null) {
      final v = json['tags'];
      final arr0 = <SavedPostsPostsTags>[];
      v.forEach((v) {
        arr0.add(SavedPostsPostsTags.fromJson(v));
      });
      tags = arr0;
    }
    status = json['status']?.toString();
    removed = json['removed'];
    createdAt = json['createdAt']?.toString();
    updatedAt = json['updatedAt']?.toString();
    v = json['__v']?.toInt();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['_id'] = id;
    data['text'] = text;
    if (media != null) {
      final v = media;
      final arr0 = [];
      for (var v in v!) {
        arr0.add(v);
      }
      data['media'] = arr0;
    }
    if (author != null) {
      data['author'] = author!.toJson();
    }
    if (tags != null) {
      final v = tags;
      final arr0 = [];
      for (var v in v!) {
        arr0.add(v!.toJson());
      }
      data['tags'] = arr0;
    }
    data['status'] = status;
    data['removed'] = removed;
    data['createdAt'] = createdAt;
    data['updatedAt'] = updatedAt;
    data['__v'] = v;
    return data;
  }
}
