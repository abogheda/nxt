import '../base_models/base_model.dart';

class SavedNxtClasses extends BaseModel {
  List<SavedNxtClassesNxtclasses?>? nxtclasses;

  SavedNxtClasses({
    this.nxtclasses,
  });
  @override
  SavedNxtClasses fromJson(Map<String, dynamic> json) {
    return SavedNxtClasses.fromJson(json);
  }

  SavedNxtClasses.fromJson(Map<String, dynamic> json) {
    if (json['nxtclasses'] != null) {
      final v = json['nxtclasses'];
      final arr0 = <SavedNxtClassesNxtclasses>[];
      v.forEach((v) {
        arr0.add(SavedNxtClassesNxtclasses.fromJson(v));
      });
      nxtclasses = arr0;
    }
  }
  @override
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (nxtclasses != null) {
      final v = nxtclasses;
      final arr0 = [];
      for (var v in v!) {
        arr0.add(v!.toJson());
      }
      data['nxtclasses'] = arr0;
    }
    return data;
  }
}

class SavedNxtClassesNxtclassesTags {
  String? id;
  String? text;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  int? v;

  SavedNxtClassesNxtclassesTags({
    this.id,
    this.text,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
  });
  SavedNxtClassesNxtclassesTags.fromJson(Map<String, dynamic> json) {
    id = json['_id']?.toString();
    text = json['text']?.toString();
    removed = json['removed'];
    createdAt = json['createdAt']?.toString();
    updatedAt = json['updatedAt']?.toString();
    v = json['__v']?.toInt();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['_id'] = id;
    data['text'] = text;
    data['removed'] = removed;
    data['createdAt'] = createdAt;
    data['updatedAt'] = updatedAt;
    data['__v'] = v;
    return data;
  }
}

class SavedNxtClassesNxtclassesAuthorLang {
  String? name;
  String? description;
  String? nationality;
  String? position;

  SavedNxtClassesNxtclassesAuthorLang({
    this.name,
    this.description,
    this.nationality,
    this.position,
  });
  SavedNxtClassesNxtclassesAuthorLang.fromJson(Map<String, dynamic> json) {
    name = json['name']?.toString();
    description = json['description']?.toString();
    nationality = json['nationality']?.toString();
    position = json['position']?.toString();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['name'] = name;
    data['description'] = description;
    data['nationality'] = nationality;
    data['position'] = position;
    return data;
  }
}

class SavedNxtClassesNxtclassesAuthor {
  String? id;
  List<SavedNxtClassesNxtclassesAuthorLang?>? lang;
  String? avatar;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  int? v;

  SavedNxtClassesNxtclassesAuthor({
    this.id,
    this.lang,
    this.avatar,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
  });
  SavedNxtClassesNxtclassesAuthor.fromJson(Map<String, dynamic> json) {
    id = json['_id']?.toString();
    if (json['lang'] != null) {
      final v = json['lang'];
      final arr0 = <SavedNxtClassesNxtclassesAuthorLang>[];
      v.forEach((v) {
        arr0.add(SavedNxtClassesNxtclassesAuthorLang.fromJson(v));
      });
      lang = arr0;
    }
    avatar = json['avatar']?.toString();
    removed = json['removed'];
    createdAt = json['createdAt']?.toString();
    updatedAt = json['updatedAt']?.toString();
    v = json['__v']?.toInt();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['_id'] = id;
    if (lang != null) {
      final v = lang;
      final arr0 = [];
      for (var v in v!) {
        arr0.add(v!.toJson());
      }
      data['lang'] = arr0;
    }
    data['avatar'] = avatar;
    data['removed'] = removed;
    data['createdAt'] = createdAt;
    data['updatedAt'] = updatedAt;
    data['__v'] = v;
    return data;
  }
}

class SavedNxtClassesNxtclassesCategoryLang {
  String? title;
  String? description;

  SavedNxtClassesNxtclassesCategoryLang({
    this.title,
    this.description,
  });
  SavedNxtClassesNxtclassesCategoryLang.fromJson(Map<String, dynamic> json) {
    title = json['title']?.toString();
    description = json['description']?.toString();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['title'] = title;
    data['description'] = description;
    return data;
  }
}

class SavedNxtClassesNxtclassesCategory {
  String? id;
  List<SavedNxtClassesNxtclassesCategoryLang?>? lang;
  String? color;
  String? image;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  int? v;
  bool? featured;

  SavedNxtClassesNxtclassesCategory({
    this.id,
    this.lang,
    this.color,
    this.image,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.featured,
  });
  SavedNxtClassesNxtclassesCategory.fromJson(Map<String, dynamic> json) {
    id = json['_id']?.toString();
    if (json['lang'] != null) {
      final v = json['lang'];
      final arr0 = <SavedNxtClassesNxtclassesCategoryLang>[];
      v.forEach((v) {
        arr0.add(SavedNxtClassesNxtclassesCategoryLang.fromJson(v));
      });
      lang = arr0;
    }
    color = json['color']?.toString();
    image = json['image']?.toString();
    removed = json['removed'];
    createdAt = json['createdAt']?.toString();
    updatedAt = json['updatedAt']?.toString();
    v = json['__v']?.toInt();
    featured = json['featured'];
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['_id'] = id;
    if (lang != null) {
      final v = lang;
      final arr0 = [];
      for (var v in v!) {
        arr0.add(v!.toJson());
      }
      data['lang'] = arr0;
    }
    data['color'] = color;
    data['image'] = image;
    data['removed'] = removed;
    data['createdAt'] = createdAt;
    data['updatedAt'] = updatedAt;
    data['__v'] = v;
    data['featured'] = featured;
    return data;
  }
}

class SavedNxtClassesNxtclassesLang {
/*
{
  "title": "New Course ",
  "description": "New Course Description ",
  "details": "New Course Details "
}
*/

  String? title;
  String? description;
  String? details;

  SavedNxtClassesNxtclassesLang({
    this.title,
    this.description,
    this.details,
  });
  SavedNxtClassesNxtclassesLang.fromJson(Map<String, dynamic> json) {
    title = json['title']?.toString();
    description = json['description']?.toString();
    details = json['details']?.toString();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['title'] = title;
    data['description'] = description;
    data['details'] = details;
    return data;
  }
}

class SavedNxtClassesNxtclasses {
  String? id;
  List<SavedNxtClassesNxtclassesLang?>? lang;
  SavedNxtClassesNxtclassesCategory? category;
  String? poster;
  SavedNxtClassesNxtclassesAuthor? author;
  String? duration;
  String? language;
  bool? featured;
  bool? display;
  List<SavedNxtClassesNxtclassesTags?>? tags;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  int? v;

  SavedNxtClassesNxtclasses({
    this.id,
    this.lang,
    this.category,
    this.poster,
    this.author,
    this.duration,
    this.language,
    this.featured,
    this.display,
    this.tags,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
  });
  SavedNxtClassesNxtclasses.fromJson(Map<String, dynamic> json) {
    id = json['_id']?.toString();
    if (json['lang'] != null) {
      final v = json['lang'];
      final arr0 = <SavedNxtClassesNxtclassesLang>[];
      v.forEach((v) {
        arr0.add(SavedNxtClassesNxtclassesLang.fromJson(v));
      });
      lang = arr0;
    }
    category = (json['category'] != null) ? SavedNxtClassesNxtclassesCategory.fromJson(json['category']) : null;
    poster = json['poster']?.toString();
    author = (json['author'] != null) ? SavedNxtClassesNxtclassesAuthor.fromJson(json['author']) : null;
    duration = json['duration']?.toString();
    language = json['language']?.toString();
    featured = json['featured'];
    display = json['display'];
    if (json['tags'] != null) {
      final v = json['tags'];
      final arr0 = <SavedNxtClassesNxtclassesTags>[];
      v.forEach((v) {
        arr0.add(SavedNxtClassesNxtclassesTags.fromJson(v));
      });
      tags = arr0;
    }
    removed = json['removed'];
    createdAt = json['createdAt']?.toString();
    updatedAt = json['updatedAt']?.toString();
    v = json['__v']?.toInt();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['_id'] = id;
    if (lang != null) {
      final v = lang;
      final arr0 = [];
      for (var v in v!) {
        arr0.add(v!.toJson());
      }
      data['lang'] = arr0;
    }
    if (category != null) {
      data['category'] = category!.toJson();
    }
    data['poster'] = poster;
    if (author != null) {
      data['author'] = author!.toJson();
    }
    data['duration'] = duration;
    data['language'] = language;
    data['featured'] = featured;
    data['display'] = display;
    if (tags != null) {
      final v = tags;
      final arr0 = [];
      for (var v in v!) {
        arr0.add(v!.toJson());
      }
      data['tags'] = arr0;
    }
    data['removed'] = removed;
    data['createdAt'] = createdAt;
    data['updatedAt'] = updatedAt;
    data['__v'] = v;
    return data;
  }
}
