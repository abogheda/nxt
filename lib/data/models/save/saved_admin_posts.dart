import '../base_models/base_model.dart';

class SavedAdminPosts extends BaseModel {
  List<SavedAdminPostsAdminposts?>? adminposts;

  SavedAdminPosts({
    this.adminposts,
  });
  @override
  SavedAdminPosts fromJson(Map<String, dynamic> json) {
    return SavedAdminPosts.fromJson(json);
  }

  SavedAdminPosts.fromJson(Map<String, dynamic> json) {
    if (json['adminposts'] != null) {
      final v = json['adminposts'];
      final arr0 = <SavedAdminPostsAdminposts>[];
      v.forEach((v) {
        arr0.add(SavedAdminPostsAdminposts.fromJson(v));
      });
      adminposts = arr0;
    }
  }
  @override
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (adminposts != null) {
      final v = adminposts;
      final arr0 = [];
      for (var v in v!) {
        arr0.add(v!.toJson());
      }
      data['adminposts'] = arr0;
    }
    return data;
  }
}

class SavedAdminPostsAdminpostsCategoryLang {
  String? title;
  String? description;

  SavedAdminPostsAdminpostsCategoryLang({
    this.title,
    this.description,
  });
  SavedAdminPostsAdminpostsCategoryLang.fromJson(Map<String, dynamic> json) {
    title = json['title']?.toString();
    description = json['description']?.toString();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['title'] = title;
    data['description'] = description;
    return data;
  }
}

class SavedAdminPostsAdminpostsCategory {
  String? id;
  List<SavedAdminPostsAdminpostsCategoryLang?>? lang;
  String? color;
  String? image;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  int? v;
  bool? featured;

  SavedAdminPostsAdminpostsCategory({
    this.id,
    this.lang,
    this.color,
    this.image,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.featured,
  });
  SavedAdminPostsAdminpostsCategory.fromJson(Map<String, dynamic> json) {
    id = json['_id']?.toString();
    if (json['lang'] != null) {
      final v = json['lang'];
      final arr0 = <SavedAdminPostsAdminpostsCategoryLang>[];
      v.forEach((v) {
        arr0.add(SavedAdminPostsAdminpostsCategoryLang.fromJson(v));
      });
      lang = arr0;
    }
    color = json['color']?.toString();
    image = json['image']?.toString();
    removed = json['removed'];
    createdAt = json['createdAt']?.toString();
    updatedAt = json['updatedAt']?.toString();
    v = json['__v']?.toInt();
    featured = json['featured'];
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['_id'] = id;
    if (lang != null) {
      final v = lang;
      final arr0 = [];
      for (var v in v!) {
        arr0.add(v!.toJson());
      }
      data['lang'] = arr0;
    }
    data['color'] = color;
    data['image'] = image;
    data['removed'] = removed;
    data['createdAt'] = createdAt;
    data['updatedAt'] = updatedAt;
    data['__v'] = v;
    data['featured'] = featured;
    return data;
  }
}

class SavedAdminPostsAdminpostsLang {
  String? text;

  SavedAdminPostsAdminpostsLang({
    this.text,
  });
  SavedAdminPostsAdminpostsLang.fromJson(Map<String, dynamic> json) {
    text = json['text']?.toString();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['text'] = text;
    return data;
  }
}

class SavedAdminPostsAdminposts {
  String? id;
  List<SavedAdminPostsAdminpostsLang?>? lang;
  String? media;
  SavedAdminPostsAdminpostsCategory? category;
  bool? featured;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  int? v;

  SavedAdminPostsAdminposts({
    this.id,
    this.lang,
    this.media,
    this.category,
    this.featured,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
  });
  SavedAdminPostsAdminposts.fromJson(Map<String, dynamic> json) {
    id = json['_id']?.toString();
    if (json['lang'] != null) {
      final v = json['lang'];
      final arr0 = <SavedAdminPostsAdminpostsLang>[];
      v.forEach((v) {
        arr0.add(SavedAdminPostsAdminpostsLang.fromJson(v));
      });
      lang = arr0;
    }
    media = json['media']?.toString();
    category = (json['category'] != null) ? SavedAdminPostsAdminpostsCategory.fromJson(json['category']) : null;
    featured = json['featured'];
    removed = json['removed'];
    createdAt = json['createdAt']?.toString();
    updatedAt = json['updatedAt']?.toString();
    v = json['__v']?.toInt();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['_id'] = id;
    if (lang != null) {
      final v = lang;
      final arr0 = [];
      for (var v in v!) {
        arr0.add(v!.toJson());
      }
      data['lang'] = arr0;
    }
    data['media'] = media;
    if (category != null) {
      data['category'] = category!.toJson();
    }
    data['featured'] = featured;
    data['removed'] = removed;
    data['createdAt'] = createdAt;
    data['updatedAt'] = updatedAt;
    data['__v'] = v;
    return data;
  }
}
