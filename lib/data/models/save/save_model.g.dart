// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'save_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SaveModel _$SaveModelFromJson(Map<String, dynamic> json) => SaveModel(
      id: json['_id'] as String?,
      user: json['user'] as String?,
      v: json['__v'] as int?,
      adminPosts: (json['adminPosts'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      classes:
          (json['classes'] as List<dynamic>?)?.map((e) => e as String).toList(),
      createdAt: json['createdAt'] == null
          ? null
          : DateTime.parse(json['createdAt'] as String),
      posts:
          (json['posts'] as List<dynamic>?)?.map((e) => e as String).toList(),
      reels:
          (json['reels'] as List<dynamic>?)?.map((e) => e as String).toList(),
      updatedAt: json['updatedAt'] == null
          ? null
          : DateTime.parse(json['updatedAt'] as String),
      userChallenges: (json['userChallenges'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      challenges: (json['challenges'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      type: $enumDecodeNullable(_$SavedItemsEnumEnumMap, json['type']),
      item: json['item'] as String?,
    );

Map<String, dynamic> _$SaveModelToJson(SaveModel instance) => <String, dynamic>{
      '_id': instance.id,
      'user': instance.user,
      '__v': instance.v,
      'adminPosts': instance.adminPosts,
      'classes': instance.classes,
      'challenges': instance.challenges,
      'createdAt': instance.createdAt?.toIso8601String(),
      'posts': instance.posts,
      'reels': instance.reels,
      'updatedAt': instance.updatedAt?.toIso8601String(),
      'userChallenges': instance.userChallenges,
      'type': _$SavedItemsEnumEnumMap[instance.type],
      'item': instance.item,
    };

const _$SavedItemsEnumEnumMap = {
  SavedItemsEnum.posts: 'posts',
  SavedItemsEnum.adminposts: 'adminposts',
  SavedItemsEnum.nxtclasses: 'nxtclasses',
  SavedItemsEnum.challenges: 'challenges',
  SavedItemsEnum.userchallenges: 'userchallenges',
  SavedItemsEnum.reels: 'reels',
  SavedItemsEnum.reads: 'reads',
  SavedItemsEnum.jobs: 'jobs',
};
