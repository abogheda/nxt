import '../base_models/base_model.dart';

class SavedReads extends BaseModel {
  List<SavedReadsReads?>? reads;

  SavedReads({
    this.reads,
  });
  @override
  SavedReads fromJson(Map<String, dynamic> json) {
    return SavedReads.fromJson(json);
  }

  SavedReads.fromJson(Map<String, dynamic> json) {
    if (json['reads'] != null) {
      final v = json['reads'];
      final arr0 = <SavedReadsReads>[];
      v.forEach((v) {
        arr0.add(SavedReadsReads.fromJson(v));
      });
      reads = arr0;
    }
  }
  @override
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (reads != null) {
      final v = reads;
      final arr0 = [];
      for (var v in v!) {
        arr0.add(v!.toJson());
      }
      data['reads'] = arr0;
    }
    return data;
  }
}

class SavedReadsReadsCategoryLang {
  String? title;
  String? description;

  SavedReadsReadsCategoryLang({
    this.title,
    this.description,
  });
  SavedReadsReadsCategoryLang.fromJson(Map<String, dynamic> json) {
    title = json['title']?.toString();
    description = json['description']?.toString();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['title'] = title;
    data['description'] = description;
    return data;
  }
}

class SavedReadsReadsCategory {
  String? id;
  List<SavedReadsReadsCategoryLang?>? lang;
  String? color;
  String? image;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  int? v;
  bool? featured;

  SavedReadsReadsCategory({
    this.id,
    this.lang,
    this.color,
    this.image,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.featured,
  });
  SavedReadsReadsCategory.fromJson(Map<String, dynamic> json) {
    id = json['_id']?.toString();
    if (json['lang'] != null) {
      final v = json['lang'];
      final arr0 = <SavedReadsReadsCategoryLang>[];
      v.forEach((v) {
        arr0.add(SavedReadsReadsCategoryLang.fromJson(v));
      });
      lang = arr0;
    }
    color = json['color']?.toString();
    image = json['image']?.toString();
    removed = json['removed'];
    createdAt = json['createdAt']?.toString();
    updatedAt = json['updatedAt']?.toString();
    v = json['__v']?.toInt();
    featured = json['featured'];
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['_id'] = id;
    if (lang != null) {
      final v = lang;
      final arr0 = [];
      for (var v in v!) {
        arr0.add(v!.toJson());
      }
      data['lang'] = arr0;
    }
    data['color'] = color;
    data['image'] = image;
    data['removed'] = removed;
    data['createdAt'] = createdAt;
    data['updatedAt'] = updatedAt;
    data['__v'] = v;
    data['featured'] = featured;
    return data;
  }
}

class SavedReadsReadsLang {
  String? title;
  String? description;
  String? details;
  List<String?>? tags;
  String? content;

  SavedReadsReadsLang({
    this.title,
    this.description,
    this.details,
    this.tags,
    this.content,
  });
  SavedReadsReadsLang.fromJson(Map<String, dynamic> json) {
    title = json['title']?.toString();
    description = json['description']?.toString();
    details = json['details']?.toString();
    if (json['tags'] != null) {
      final v = json['tags'];
      final arr0 = <String>[];
      v.forEach((v) {
        arr0.add(v.toString());
      });
      tags = arr0;
    }
    content = json['content']?.toString();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['title'] = title;
    data['description'] = description;
    data['details'] = details;
    if (tags != null) {
      final v = tags;
      final arr0 = [];
      for (var v in v!) {
        arr0.add(v);
      }
      data['tags'] = arr0;
    }
    data['content'] = content;
    return data;
  }
}

class SavedReadsReads {
  String? id;
  List<SavedReadsReadsLang?>? lang;
  String? media;
  SavedReadsReadsCategory? category;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  int? v;

  SavedReadsReads({
    this.id,
    this.lang,
    this.media,
    this.category,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
  });
  SavedReadsReads.fromJson(Map<String, dynamic> json) {
    id = json['_id']?.toString();
    if (json['lang'] != null) {
      final v = json['lang'];
      final arr0 = <SavedReadsReadsLang>[];
      v.forEach((v) {
        arr0.add(SavedReadsReadsLang.fromJson(v));
      });
      lang = arr0;
    }
    media = json['media']?.toString();
    category = (json['category'] != null) ? SavedReadsReadsCategory.fromJson(json['category']) : null;
    removed = json['removed'];
    createdAt = json['createdAt']?.toString();
    updatedAt = json['updatedAt']?.toString();
    v = json['__v']?.toInt();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['_id'] = id;
    if (lang != null) {
      final v = lang;
      final arr0 = [];
      for (var v in v!) {
        arr0.add(v!.toJson());
      }
      data['lang'] = arr0;
    }
    data['media'] = media;
    if (category != null) {
      data['category'] = category!.toJson();
    }
    data['removed'] = removed;
    data['createdAt'] = createdAt;
    data['updatedAt'] = updatedAt;
    data['__v'] = v;
    return data;
  }
}
