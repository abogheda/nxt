// ignore_for_file: public_member_api_docs, sort_constructors_first

import 'package:json_annotation/json_annotation.dart';
import 'package:nxt/data/models/base_models/base_model.dart';

import '../../enum/save.dart';

part 'save_model.g.dart';

@JsonSerializable()
class SaveModel extends BaseModel {
  @JsonKey(name: '_id')
  String? id;
  String? user;
  @JsonKey(name: '__v')
  int? v;
  List<String>? adminPosts;
  List<String>? classes;
  List<String>? challenges;
  DateTime? createdAt;
  List<String>? posts;
  List<String>? reels;
  DateTime? updatedAt;
  List<String>? userChallenges;
  SavedItemsEnum? type;
  String? item;

  SaveModel({
    this.id,
    this.user,
    this.v,
    this.adminPosts,
    this.classes,
    this.createdAt,
    this.posts,
    this.reels,
    this.updatedAt,
    this.userChallenges,
    this.challenges,
    this.type,
    this.item,
  });

  factory SaveModel.fromJson(Map<String, dynamic> json) => _$SaveModelFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$SaveModelToJson(this);

  @override
  fromJson(Map<String, dynamic> json) {
    return _$SaveModelFromJson(json);
  }
}
