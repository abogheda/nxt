import '../base_models/base_model.dart';

class SavedJobs extends BaseModel {
  List<SavedJobsJobs?>? jobs;

  SavedJobs({
    this.jobs,
  });
  @override
  SavedJobs fromJson(Map<String, dynamic> json) {
    return SavedJobs.fromJson(json);
  }

  SavedJobs.fromJson(Map<String, dynamic> json) {
    if (json['jobs'] != null) {
      final v = json['jobs'];
      final arr0 = <SavedJobsJobs>[];
      v.forEach((v) {
        arr0.add(SavedJobsJobs.fromJson(v));
      });
      jobs = arr0;
    }
  }
  @override
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (jobs != null) {
      final v = jobs;
      final arr0 = [];
      for (var v in v!) {
        arr0.add(v!.toJson());
      }
      data['jobs'] = arr0;
    }
    return data;
  }
}

class SavedJobsJobsOrganization {
  String? id;
  String? name;
  String? website;
  String? industry;
  int? size;
  String? type;
  String? logo;
  String? promocode;
  int? userCount;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  int? v;

  SavedJobsJobsOrganization({
    this.id,
    this.name,
    this.website,
    this.industry,
    this.size,
    this.type,
    this.logo,
    this.promocode,
    this.userCount,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
  });
  SavedJobsJobsOrganization.fromJson(Map<String, dynamic> json) {
    id = json['_id']?.toString();
    name = json['name']?.toString();
    website = json['website']?.toString();
    industry = json['industry']?.toString();
    size = json['size']?.toint();
    type = json['type']?.toString();
    logo = json['logo']?.toString();
    promocode = json['promocode']?.toString();
    userCount = json['userCount']?.toInt();
    removed = json['removed'];
    createdAt = json['createdAt']?.toString();
    updatedAt = json['updatedAt']?.toString();
    v = json['__v']?.toInt();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['_id'] = id;
    data['name'] = name;
    data['website'] = website;
    data['industry'] = industry;
    data['size'] = size;
    data['type'] = type;
    data['logo'] = logo;
    data['promocode'] = promocode;
    data['userCount'] = userCount;
    data['removed'] = removed;
    data['createdAt'] = createdAt;
    data['updatedAt'] = updatedAt;
    data['__v'] = v;
    return data;
  }
}

class SavedJobsJobsCategoryLang {
  String? title;
  String? description;

  SavedJobsJobsCategoryLang({
    this.title,
    this.description,
  });
  SavedJobsJobsCategoryLang.fromJson(Map<String, dynamic> json) {
    title = json['title']?.toString();
    description = json['description']?.toString();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['title'] = title;
    data['description'] = description;
    return data;
  }
}

class SavedJobsJobsCategory {
  String? id;
  List<SavedJobsJobsCategoryLang?>? lang;
  String? color;
  String? image;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  int? v;
  bool? featured;

  SavedJobsJobsCategory({
    this.id,
    this.lang,
    this.color,
    this.image,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.featured,
  });
  SavedJobsJobsCategory.fromJson(Map<String, dynamic> json) {
    id = json['_id']?.toString();
    if (json['lang'] != null) {
      final v = json['lang'];
      final arr0 = <SavedJobsJobsCategoryLang>[];
      v.forEach((v) {
        arr0.add(SavedJobsJobsCategoryLang.fromJson(v));
      });
      lang = arr0;
    }
    color = json['color']?.toString();
    image = json['image']?.toString();
    removed = json['removed'];
    createdAt = json['createdAt']?.toString();
    updatedAt = json['updatedAt']?.toString();
    v = json['__v']?.toInt();
    featured = json['featured'];
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['_id'] = id;
    if (lang != null) {
      final v = lang;
      final arr0 = [];
      for (var v in v!) {
        arr0.add(v!.toJson());
      }
      data['lang'] = arr0;
    }
    data['color'] = color;
    data['image'] = image;
    data['removed'] = removed;
    data['createdAt'] = createdAt;
    data['updatedAt'] = updatedAt;
    data['__v'] = v;
    data['featured'] = featured;
    return data;
  }
}

class SavedJobsJobsLang {
  String? title;
  String? supTitle;
  String? description;
  String? area;

  SavedJobsJobsLang({
    this.title,
    this.supTitle,
    this.description,
    this.area,
  });
  SavedJobsJobsLang.fromJson(Map<String, dynamic> json) {
    title = json['title']?.toString();
    supTitle = json['supTitle']?.toString();
    description = json['description']?.toString();
    area = json['area']?.toString();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['title'] = title;
    data['supTitle'] = supTitle;
    data['description'] = description;
    data['area'] = area;
    return data;
  }
}

class SavedJobsJobs {
  String? id;
  List<SavedJobsJobsLang?>? lang;
  String? gender;
  String? age;
  String? poster;
  String? uploadType;
  SavedJobsJobsCategory? category;
  SavedJobsJobsOrganization? organization;
  String? jobStartDate;
  String? jobEndDate;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  int? v;

  SavedJobsJobs({
    this.id,
    this.lang,
    this.gender,
    this.age,
    this.poster,
    this.uploadType,
    this.category,
    this.organization,
    this.jobStartDate,
    this.jobEndDate,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
  });
  SavedJobsJobs.fromJson(Map<String, dynamic> json) {
    id = json['_id']?.toString();
    if (json['lang'] != null) {
      final v = json['lang'];
      final arr0 = <SavedJobsJobsLang>[];
      v.forEach((v) {
        arr0.add(SavedJobsJobsLang.fromJson(v));
      });
      lang = arr0;
    }
    gender = json['gender']?.toString();
    age = json['age']?.toString();
    poster = json['poster']?.toString();
    uploadType = json['uploadType']?.toString();
    category = (json['category'] != null) ? SavedJobsJobsCategory.fromJson(json['category']) : null;
    organization = (json['organization'] != null) ? SavedJobsJobsOrganization.fromJson(json['organization']) : null;
    jobStartDate = json['jobStartDate']?.toString();
    jobEndDate = json['jobEndDate']?.toString();
    removed = json['removed'];
    createdAt = json['createdAt']?.toString();
    updatedAt = json['updatedAt']?.toString();
    v = json['__v']?.toInt();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['_id'] = id;
    if (lang != null) {
      final v = lang;
      final arr0 = [];
      for (var v in v!) {
        arr0.add(v!.toJson());
      }
      data['lang'] = arr0;
    }
    data['gender'] = gender;
    data['age'] = age;
    data['poster'] = poster;
    data['uploadType'] = uploadType;
    if (category != null) {
      data['category'] = category!.toJson();
    }
    if (organization != null) {
      data['organization'] = organization!.toJson();
    }
    data['jobStartDate'] = jobStartDate;
    data['jobEndDate'] = jobEndDate;
    data['removed'] = removed;
    data['createdAt'] = createdAt;
    data['updatedAt'] = updatedAt;
    data['__v'] = v;
    return data;
  }
}
