// ignore_for_file: public_member_api_docs, sort_constructors_first
enum ImageType { file, url }

class UserShotModel {
  final int index;
  final ImageType imageType;
  final String image;

  UserShotModel({
    required this.index,
    required this.imageType,
    required this.image,
  });
  UserShotModel copyWith(
    ImageType type,
    String image,
  ) {
    return UserShotModel(index: index, imageType: type, image: image);
  }
}
