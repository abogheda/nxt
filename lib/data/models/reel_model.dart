import 'package:json_annotation/json_annotation.dart';
import 'base_models/base_model.dart';

part 'reel_model.g.dart';

@JsonSerializable(includeIfNull: false)
class ReelModel extends BaseModel {
  @JsonKey(name: '_id')
  final String? id;
  final String? title;
  final String? text;
  final List<String>? media;
  final String? status;
  final bool? removed;
  final String? user;
  final DateTime? createdAt;
  final DateTime? updatedAt;
  @JsonKey(name: '__v')
  final int? v;

  ReelModel({
    this.createdAt,
    this.updatedAt,
    this.v,
    this.user,
    this.id,
    this.title,
    this.text,
    this.media,
    this.status,
    this.removed,
  });

  factory ReelModel.fromJson(Map<String, dynamic> json) => _$ReelModelFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$ReelModelToJson(this);

  @override
  fromJson(Map<String, dynamic> json) {
    return _$ReelModelFromJson(json);
  }
}
