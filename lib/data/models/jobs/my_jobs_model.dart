import '../base_models/base_model.dart';

class MyJobsModel extends BaseModel {
  String? id;
  String? user;
  String? media;
  MyJobsModelJob? job;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  int? v;

  MyJobsModel({
    this.id,
    this.user,
    this.media,
    this.job,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
  });
  @override
  MyJobsModel fromJson(Map<String, dynamic> json) {
    return MyJobsModel.fromJson(json);
  }

  MyJobsModel.fromJson(Map<String, dynamic> json) {
    id = json['_id']?.toString();
    user = json['user']?.toString();
    media = json['media']?.toString();
    job = (json['job'] != null) ? MyJobsModelJob.fromJson(json['job']) : null;
    removed = json['removed'];
    createdAt = json['createdAt']?.toString();
    updatedAt = json['updatedAt']?.toString();
    v = json['__v']?.toInt();
  }
  @override
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['_id'] = id;
    data['user'] = user;
    data['media'] = media;
    if (job != null) {
      data['job'] = job!.toJson();
    }
    data['removed'] = removed;
    data['createdAt'] = createdAt;
    data['updatedAt'] = updatedAt;
    data['__v'] = v;
    return data;
  }
}

class MyJobsModelJobOrganization {
  String? id;
  String? name;
  String? website;
  String? industry;
  int? size;
  String? type;
  String? logo;
  String? promocode;
  int? userCount;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  int? v;

  MyJobsModelJobOrganization({
    this.id,
    this.name,
    this.website,
    this.industry,
    this.size,
    this.type,
    this.logo,
    this.promocode,
    this.userCount,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
  });
  MyJobsModelJobOrganization.fromJson(Map<String, dynamic> json) {
    id = json['_id']?.toString();
    name = json['name']?.toString();
    website = json['website']?.toString();
    industry = json['industry']?.toString();
    size = json['size']?.toint();
    type = json['type']?.toString();
    logo = json['logo']?.toString();
    promocode = json['promocode']?.toString();
    userCount = json['userCount']?.toInt();
    removed = json['removed'];
    createdAt = json['createdAt']?.toString();
    updatedAt = json['updatedAt']?.toString();
    v = json['__v']?.toInt();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['_id'] = id;
    data['name'] = name;
    data['website'] = website;
    data['industry'] = industry;
    data['size'] = size;
    data['type'] = type;
    data['logo'] = logo;
    data['promocode'] = promocode;
    data['userCount'] = userCount;
    data['removed'] = removed;
    data['createdAt'] = createdAt;
    data['updatedAt'] = updatedAt;
    data['__v'] = v;
    return data;
  }
}

class MyJobsModelJobLang {
  String? title;
  String? supTitle;
  String? description;
  String? area;

  MyJobsModelJobLang({
    this.title,
    this.supTitle,
    this.description,
    this.area,
  });
  MyJobsModelJobLang.fromJson(Map<String, dynamic> json) {
    title = json['title']?.toString();
    supTitle = json['supTitle']?.toString();
    description = json['description']?.toString();
    area = json['area']?.toString();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['title'] = title;
    data['supTitle'] = supTitle;
    data['description'] = description;
    data['area'] = area;
    return data;
  }
}

class MyJobsModelJob {
  String? id;
  List<MyJobsModelJobLang?>? lang;
  String? gender;
  String? ageTo;
  String? ageFrom;
  String? poster;
  String? uploadType;
  String? category;
  MyJobsModelJobOrganization? organization;
  String? jobStartDate;
  String? jobEndDate;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  int? v;

  MyJobsModelJob({
    this.id,
    this.lang,
    this.gender,
    this.ageTo,
    this.ageFrom,
    this.poster,
    this.uploadType,
    this.category,
    this.organization,
    this.jobStartDate,
    this.jobEndDate,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
  });
  MyJobsModelJob.fromJson(Map<String, dynamic> json) {
    id = json['_id']?.toString();
    if (json['lang'] != null) {
      final v = json['lang'];
      final arr0 = <MyJobsModelJobLang>[];
      v.forEach((v) {
        arr0.add(MyJobsModelJobLang.fromJson(v));
      });
      lang = arr0;
    }
    gender = json['gender']?.toString();
    ageTo = json['ageTo']?.toString();
    ageFrom = json['ageFrom']?.toString();
    poster = json['poster']?.toString();
    uploadType = json['uploadType']?.toString();
    category = json['category']?.toString();
    organization = (json['organization'] != null) ? MyJobsModelJobOrganization.fromJson(json['organization']) : null;
    jobStartDate = json['jobStartDate']?.toString();
    jobEndDate = json['jobEndDate']?.toString();
    removed = json['removed'];
    createdAt = json['createdAt']?.toString();
    updatedAt = json['updatedAt']?.toString();
    v = json['__v']?.toInt();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['_id'] = id;
    if (lang != null) {
      final v = lang;
      final arr0 = [];
      for (var v in v!) {
        arr0.add(v!.toJson());
      }
      data['lang'] = arr0;
    }
    data['gender'] = gender;
    data['ageTo'] = ageTo;
    data['ageFrom'] = ageFrom;
    data['poster'] = poster;
    data['uploadType'] = uploadType;
    data['category'] = category;
    if (organization != null) {
      data['organization'] = organization!.toJson();
    }
    data['jobStartDate'] = jobStartDate;
    data['jobEndDate'] = jobEndDate;
    data['removed'] = removed;
    data['createdAt'] = createdAt;
    data['updatedAt'] = updatedAt;
    data['__v'] = v;
    return data;
  }
}
