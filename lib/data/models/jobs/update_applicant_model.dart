import '../base_models/base_model.dart';

class UpdateApplicantModel extends BaseModel {
  bool? acknowledged;
  int? modifiedCount;
  int? upsertedCount;
  int? matchedCount;

  UpdateApplicantModel({
    this.acknowledged,
    this.modifiedCount,
    this.upsertedCount,
    this.matchedCount,
  });
  @override
  UpdateApplicantModel fromJson(Map<String, dynamic> json) {
    return UpdateApplicantModel.fromJson(json);
  }

  UpdateApplicantModel.fromJson(Map<String, dynamic> json) {
    acknowledged = json['acknowledged'];
    modifiedCount = json['modifiedCount']?.toInt();
    upsertedCount = json['upsertedCount']?.toInt();
    matchedCount = json['matchedCount']?.toInt();
  }
  @override
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['acknowledged'] = acknowledged;
    data['modifiedCount'] = modifiedCount;
    data['upsertedCount'] = upsertedCount;
    data['matchedCount'] = matchedCount;
    return data;
  }
}
