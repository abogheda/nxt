import '../base_models/base_model.dart';

class ApplyJobModel extends BaseModel {
  ApplyJobModel({
    this.user,
    this.media,
    this.job,
    this.removed,
    this.id,
    this.createdAt,
    this.updatedAt,
    this.v,
  });
  @override
  ApplyJobModel fromJson(Map<String, dynamic> json) {
    return ApplyJobModel.fromJson(json);
  }

  ApplyJobModel.fromJson(dynamic json) {
    user = json['user'];
    media = json['media'];
    job = json['job'];
    removed = json['removed'];
    id = json['_id'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    v = json['__v'];
  }
  String? user;
  String? media;
  String? job;
  bool? removed;
  String? id;
  String? createdAt;
  String? updatedAt;
  num? v;
  ApplyJobModel copyWith({
    String? user,
    String? media,
    String? job,
    bool? removed,
    String? id,
    String? createdAt,
    String? updatedAt,
    num? v,
  }) =>
      ApplyJobModel(
        user: user ?? this.user,
        media: media ?? this.media,
        job: job ?? this.job,
        removed: removed ?? this.removed,
        id: id ?? this.id,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
        v: v ?? this.v,
      );
  @override
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['user'] = user;
    map['media'] = media;
    map['job'] = job;
    map['removed'] = removed;
    map['_id'] = id;
    map['createdAt'] = createdAt;
    map['updatedAt'] = updatedAt;
    map['__v'] = v;
    return map;
  }
}
