import '../base_models/base_model.dart';

class JobsModel extends BaseModel {
  JobsModel({
    this.id,
    this.gender,
    this.age,
    this.poster,
    this.category,
    this.organization,
    this.removed,
    this.userSaved,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.title,
    this.supTitle,
    this.description,
    this.area,
    this.expert,
  });

  @override
  JobsModel fromJson(Map<String, dynamic> json) {
    return JobsModel.fromJson(json);
  }

  // factory JobsModel.fromSearch(SearchJobAndChallengesModel model) => JobsModel(
  //       id: model.id,
  //       organization: Organization(
  //         id: model.organization!.id,
  //         createdAt: model.organization!.createdAt,
  //         name: model.organization!.name,
  //         logo: model.organization!.logo,
  //         industry: model.organization!.industry,
  //       ),
  //       title: model.title,
  //       createdAt: model.createdAt,
  //       category: Category(
  //         createdAt: model.category!.createdAt,
  //         id: model.category!.id,
  //         color: model.category!.color,
  //       ),
  //     );

  JobsModel.fromJson(dynamic json) {
    id = json['_id'];
    gender = json['gender'];
    age = json['age'];
    poster = json['poster'];
    category = json['category'] != null ? Category.fromJson(json['category']) : null;
    organization = json['organization'] != null ? Organization.fromJson(json['organization']) : null;
    removed = json['removed'];
    userSaved = json['userSaved'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    v = json['__v'];
    title = json['title'];
    supTitle = json['supTitle'];
    description = json['description'];
    area = json['area'];
    expert = (json['expert'] != null) ? JobsModelExpert.fromJson(json['expert']) : null;
  }
  String? id;
  String? gender;
  String? age;
  String? poster;
  Category? category;
  Organization? organization;
  bool? removed;
  bool? userSaved;
  String? createdAt;
  String? updatedAt;
  num? v;
  String? title;
  String? supTitle;
  String? description;
  String? area;
  JobsModelExpert? expert;
  JobsModel copyWith({
    String? id,
    String? gender,
    String? age,
    String? poster,
    Category? category,
    Organization? organization,
    bool? removed,
    bool? userSaved,
    String? createdAt,
    String? updatedAt,
    num? v,
    String? title,
    String? supTitle,
    String? description,
    String? area,
  }) =>
      JobsModel(
        id: id ?? this.id,
        gender: gender ?? this.gender,
        age: age ?? this.age,
        poster: poster ?? this.poster,
        category: category ?? this.category,
        organization: organization ?? this.organization,
        removed: removed ?? this.removed,
        userSaved: userSaved ?? this.userSaved,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
        v: v ?? this.v,
        title: title ?? this.title,
        supTitle: supTitle ?? this.supTitle,
        description: description ?? this.description,
        area: area ?? this.area,
      );
  @override
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['_id'] = id;
    map['gender'] = gender;
    map['age'] = age;
    map['poster'] = poster;
    if (category != null) {
      map['category'] = category?.toJson();
    }
    if (organization != null) {
      map['organization'] = organization?.toJson();
    }
    map['removed'] = removed;
    map['userSaved'] = userSaved;
    map['createdAt'] = createdAt;
    map['updatedAt'] = updatedAt;
    map['__v'] = v;
    map['title'] = title;
    map['supTitle'] = supTitle;
    map['description'] = description;
    map['area'] = area;
    if (expert != null) {
      map['expert'] = expert!.toJson();
    }
    return map;
  }
}

class Organization {
  Organization({
    this.id,
    this.name,
    this.website,
    this.industry,
    this.type,
    this.logo,
    this.promocode,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.userCount,
  });

  Organization.fromJson(dynamic json) {
    id = json['_id'];
    name = json['name'];
    website = json['website'];
    industry = json['industry'];
    type = json['type'];
    logo = json['logo'];
    promocode = json['promocode'];
    removed = json['removed'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    v = json['__v'];
    userCount = json['userCount'];
  }
  String? id;
  String? name;
  String? website;
  String? industry;
  String? type;
  String? logo;
  String? promocode;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  num? v;
  num? userCount;
  Organization copyWith({
    String? id,
    String? name,
    String? website,
    String? industry,
    String? type,
    String? logo,
    String? promocode,
    bool? removed,
    String? createdAt,
    String? updatedAt,
    num? v,
    num? userCount,
  }) =>
      Organization(
        id: id ?? this.id,
        name: name ?? this.name,
        website: website ?? this.website,
        industry: industry ?? this.industry,
        type: type ?? this.type,
        logo: logo ?? this.logo,
        promocode: promocode ?? this.promocode,
        removed: removed ?? this.removed,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
        v: v ?? this.v,
        userCount: userCount ?? this.userCount,
      );
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['_id'] = id;
    map['name'] = name;
    map['website'] = website;
    map['industry'] = industry;
    map['type'] = type;
    map['logo'] = logo;
    map['promocode'] = promocode;
    map['removed'] = removed;
    map['createdAt'] = createdAt;
    map['updatedAt'] = updatedAt;
    map['__v'] = v;
    map['userCount'] = userCount;
    return map;
  }
}

class Category {
  Category({
    this.id,
    this.lang,
    this.color,
    this.image,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.featured,
  });

  Category.fromJson(dynamic json) {
    id = json['_id'];
    if (json['lang'] != null) {
      lang = [];
      json['lang'].forEach((v) {
        lang?.add(Lang.fromJson(v));
      });
    }
    color = json['color'];
    image = json['image'];
    removed = json['removed'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    v = json['__v'];
    featured = json['featured'];
  }
  String? id;
  List<Lang>? lang;
  String? color;
  String? image;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  num? v;
  bool? featured;
  Category copyWith({
    String? id,
    List<Lang>? lang,
    String? color,
    String? image,
    bool? removed,
    String? createdAt,
    String? updatedAt,
    num? v,
    bool? featured,
  }) =>
      Category(
        id: id ?? this.id,
        lang: lang ?? this.lang,
        color: color ?? this.color,
        image: image ?? this.image,
        removed: removed ?? this.removed,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
        v: v ?? this.v,
        featured: featured ?? this.featured,
      );
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['_id'] = id;
    if (lang != null) {
      map['lang'] = lang?.map((v) => v.toJson()).toList();
    }
    map['color'] = color;
    map['image'] = image;
    map['removed'] = removed;
    map['createdAt'] = createdAt;
    map['updatedAt'] = updatedAt;
    map['__v'] = v;
    map['featured'] = featured;
    return map;
  }
}

class Lang {
  Lang({
    this.title,
    this.description,
  });

  Lang.fromJson(dynamic json) {
    title = json['title'];
    description = json['description'];
  }
  String? title;
  String? description;
  Lang copyWith({
    String? title,
    String? description,
  }) =>
      Lang(
        title: title ?? this.title,
        description: description ?? this.description,
      );
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['title'] = title;
    map['description'] = description;
    return map;
  }
}

class JobsModelExpertExpert {
  String? name;
  String? website;
  String? scope;
  String? logo;

  JobsModelExpertExpert({
    this.name,
    this.website,
    this.scope,
    this.logo,
  });
  JobsModelExpertExpert.fromJson(Map<String, dynamic> json) {
    name = json['name']?.toString();
    website = json['website']?.toString();
    scope = json['scope']?.toString();
    logo = json['logo']?.toString();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['name'] = name;
    data['website'] = website;
    data['scope'] = scope;
    data['logo'] = logo;
    return data;
  }
}

class JobsModelExpert {
  String? id;
  String? name;
  String? email;
  String? phone;
  String? password;
  String? role;
  String? avatar;
  String? provider;
  String? profileStatus;
  bool? terms;
  bool? privacy;
  bool? eula;
  JobsModelExpertExpert? expert;
  bool? firstSubscription;
  bool? subscriptionEnd;
  bool? invitation;
  bool? verify;
  bool? verifyPhone;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  int? v;

  JobsModelExpert({
    this.id,
    this.name,
    this.email,
    this.phone,
    this.password,
    this.role,
    this.avatar,
    this.provider,
    this.profileStatus,
    this.terms,
    this.privacy,
    this.eula,
    this.expert,
    this.firstSubscription,
    this.subscriptionEnd,
    this.invitation,
    this.verify,
    this.verifyPhone,
    this.removed,
    this.createdAt,
    this.updatedAt,
  });
  JobsModelExpert.fromJson(Map<String, dynamic> json) {
    id = json['_id']?.toString();
    name = json['name']?.toString();
    email = json['email']?.toString();
    phone = json['phone']?.toString();
    password = json['password']?.toString();
    role = json['role']?.toString();
    avatar = json['avatar']?.toString();
    provider = json['provider']?.toString();
    profileStatus = json['profileStatus']?.toString();
    terms = json['terms'];
    privacy = json['privacy'];
    eula = json['eula'];
    expert = (json['expert'] != null) ? JobsModelExpertExpert.fromJson(json['expert']) : null;
    firstSubscription = json['firstSubscription'];
    subscriptionEnd = json['subscriptionEnd'];
    invitation = json['invitation'];
    verify = json['verify'];
    verifyPhone = json['verifyPhone'];
    removed = json['removed'];
    createdAt = json['createdAt']?.toString();
    updatedAt = json['updatedAt']?.toString();
    v = json['__v']?.toInt();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['_id'] = id;
    data['name'] = name;
    data['email'] = email;
    data['phone'] = phone;
    data['password'] = password;
    data['role'] = role;
    data['avatar'] = avatar;
    data['provider'] = provider;
    data['profileStatus'] = profileStatus;
    data['terms'] = terms;
    data['privacy'] = privacy;
    data['eula'] = eula;
    if (expert != null) {
      data['expert'] = expert!.toJson();
    }
    data['firstSubscription'] = firstSubscription;
    data['subscriptionEnd'] = subscriptionEnd;
    data['invitation'] = invitation;
    data['verify'] = verify;
    data['verifyPhone'] = verifyPhone;
    data['removed'] = removed;
    data['createdAt'] = createdAt;
    data['updatedAt'] = updatedAt;
    data['__v'] = v;
    return data;
  }
}
