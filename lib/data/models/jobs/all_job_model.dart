import '../base_models/base_model.dart';

class AllJobModel extends BaseModel {
  String? id;
  List<AllJobModelLang?>? lang;
  String? gender;
  String? age;
  String? poster;
  String? uploadType;
  AllJobModelCategory? category;
  AllJobModelOrganization? organization;
  String? jobStartDate;
  String? jobEndDate;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  int? v;
  String? ageFrom;
  String? ageTo;
  List<AllJobModelCountry?>? country;
  bool? userSaved;
  bool? userApply;

  AllJobModel({
    this.id,
    this.lang,
    this.gender,
    this.age,
    this.poster,
    this.uploadType,
    this.category,
    this.organization,
    this.jobStartDate,
    this.jobEndDate,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.ageFrom,
    this.ageTo,
    this.country,
    this.userSaved,
    this.userApply,
  });
  @override
  AllJobModel fromJson(Map<String, dynamic> json) {
    return AllJobModel.fromJson(json);
  }

  AllJobModel.fromJson(Map<String, dynamic> json) {
    id = json['_id']?.toString();
    if (json['lang'] != null) {
      final v = json['lang'];
      final arr0 = <AllJobModelLang>[];
      v.forEach((v) {
        arr0.add(AllJobModelLang.fromJson(v));
      });
      lang = arr0;
    }
    gender = json['gender']?.toString();
    age = json['age']?.toString();
    poster = json['poster']?.toString();
    uploadType = json['uploadType']?.toString();
    category = (json['category'] != null) ? AllJobModelCategory.fromJson(json['category']) : null;
    organization = (json['organization'] != null) ? AllJobModelOrganization.fromJson(json['organization']) : null;
    jobStartDate = json['jobStartDate']?.toString();
    jobEndDate = json['jobEndDate']?.toString();
    removed = json['removed'];
    createdAt = json['createdAt']?.toString();
    updatedAt = json['updatedAt']?.toString();
    v = json['__v']?.toInt();
    ageFrom = json['ageFrom']?.toString();
    ageTo = json['ageTo']?.toString();
    if (json['country'] != null) {
      final v = json['country'];
      final arr0 = <AllJobModelCountry>[];
      v.forEach((v) {
        arr0.add(AllJobModelCountry.fromJson(v));
      });
      country = arr0;
    }
    userSaved = json['userSaved'];
    userApply = json['userApply'];
  }
  @override
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['_id'] = id;
    if (lang != null) {
      final v = lang;
      final arr0 = [];
      for (var v in v!) {
        arr0.add(v!.toJson());
      }
      data['lang'] = arr0;
    }
    data['gender'] = gender;
    data['age'] = age;
    data['poster'] = poster;
    data['uploadType'] = uploadType;
    if (category != null) {
      data['category'] = category!.toJson();
    }
    if (organization != null) {
      data['organization'] = organization!.toJson();
    }
    data['jobStartDate'] = jobStartDate;
    data['jobEndDate'] = jobEndDate;
    data['removed'] = removed;
    data['createdAt'] = createdAt;
    data['updatedAt'] = updatedAt;
    data['__v'] = v;
    data['ageFrom'] = ageFrom;
    data['ageTo'] = ageTo;
    if (country != null) {
      final v = country;
      final arr0 = [];
      for (var v in v!) {
        arr0.add(v!.toJson());
      }
      data['country'] = arr0;
    }
    data['userSaved'] = userSaved;
    data['userApply'] = userApply;
    return data;
  }
}

class AllJobModelCountry {
  String? id;
  String? name;
  String? lat;
  String? long;
  String? azureCode;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  int? v;

  AllJobModelCountry({
    this.id,
    this.name,
    this.lat,
    this.long,
    this.azureCode,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
  });
  AllJobModelCountry.fromJson(Map<String, dynamic> json) {
    id = json['_id']?.toString();
    name = json['name']?.toString();
    lat = json['lat']?.toString();
    long = json['long']?.toString();
    azureCode = json['azureCode']?.toString();
    removed = json['removed'];
    createdAt = json['createdAt']?.toString();
    updatedAt = json['updatedAt']?.toString();
    v = json['__v']?.toInt();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['_id'] = id;
    data['name'] = name;
    data['lat'] = lat;
    data['long'] = long;
    data['azureCode'] = azureCode;
    data['removed'] = removed;
    data['createdAt'] = createdAt;
    data['updatedAt'] = updatedAt;
    data['__v'] = v;
    return data;
  }
}

class AllJobModelOrganization {
  String? id;
  String? name;
  String? website;
  String? industry;
  String? type;
  String? logo;
  int? userCount;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  int? v;

  AllJobModelOrganization({
    this.id,
    this.name,
    this.website,
    this.industry,
    this.type,
    this.logo,
    this.userCount,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
  });
  AllJobModelOrganization.fromJson(Map<String, dynamic> json) {
    id = json['_id']?.toString();
    name = json['name']?.toString();
    website = json['website']?.toString();
    industry = json['industry']?.toString();
    type = json['type']?.toString();
    logo = json['logo']?.toString();
    userCount = json['userCount']?.toInt();
    removed = json['removed'];
    createdAt = json['createdAt']?.toString();
    updatedAt = json['updatedAt']?.toString();
    v = json['__v']?.toInt();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['_id'] = id;
    data['name'] = name;
    data['website'] = website;
    data['industry'] = industry;
    data['type'] = type;
    data['logo'] = logo;
    data['userCount'] = userCount;
    data['removed'] = removed;
    data['createdAt'] = createdAt;
    data['updatedAt'] = updatedAt;
    data['__v'] = v;
    return data;
  }
}

class AllJobModelCategory {
  String? id;
  String? color;
  String? image;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  int? v;
  bool? featured;
  List<String?>? tags;
  String? title;
  String? description;

  AllJobModelCategory({
    this.id,
    this.color,
    this.image,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.featured,
    this.tags,
    this.title,
    this.description,
  });
  AllJobModelCategory.fromJson(Map<String, dynamic> json) {
    id = json['_id']?.toString();
    color = json['color']?.toString();
    image = json['image']?.toString();
    removed = json['removed'];
    createdAt = json['createdAt']?.toString();
    updatedAt = json['updatedAt']?.toString();
    v = json['__v']?.toInt();
    featured = json['featured'];
    if (json['tags'] != null) {
      final v = json['tags'];
      final arr0 = <String>[];
      v.forEach((v) {
        arr0.add(v.toString());
      });
      tags = arr0;
    }
    title = json['title']?.toString();
    description = json['description']?.toString();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['_id'] = id;
    data['color'] = color;
    data['image'] = image;
    data['removed'] = removed;
    data['createdAt'] = createdAt;
    data['updatedAt'] = updatedAt;
    data['__v'] = v;
    data['featured'] = featured;
    if (tags != null) {
      final v = tags;
      final arr0 = [];
      for (var v in v!) {
        arr0.add(v);
      }
      data['tags'] = arr0;
    }
    data['title'] = title;
    data['description'] = description;
    return data;
  }
}

class AllJobModelLang {
/*
{
  "title": "New hiring ",
  "supTitle": "This is to apply for the new job.",
  "description": "New hiring ",
  "area": "Cairo"
}
*/

  String? title;
  String? supTitle;
  String? description;
  String? area;

  AllJobModelLang({
    this.title,
    this.supTitle,
    this.description,
    this.area,
  });
  AllJobModelLang.fromJson(Map<String, dynamic> json) {
    title = json['title']?.toString();
    supTitle = json['supTitle']?.toString();
    description = json['description']?.toString();
    area = json['area']?.toString();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['title'] = title;
    data['supTitle'] = supTitle;
    data['description'] = description;
    data['area'] = area;
    return data;
  }
}
