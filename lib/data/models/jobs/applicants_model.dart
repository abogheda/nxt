import '../base_models/base_model.dart';

class ApplicantsModel extends BaseModel {
  String? id;
  ApplicantsModelUser? user;
  String? media;
  String? job;
  String? status;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  int? v;

  ApplicantsModel({
    this.id,
    this.user,
    this.media,
    this.job,
    this.status,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
  });
  @override
  ApplicantsModel fromJson(Map<String, dynamic> json) {
    return ApplicantsModel.fromJson(json);
  }

  ApplicantsModel.fromJson(Map<String, dynamic> json) {
    id = json['_id']?.toString();
    user = (json['user'] != null) ? ApplicantsModelUser.fromJson(json['user']) : null;
    media = json['media']?.toString();
    job = json['job']?.toString();
    status = json['status']?.toString();
    removed = json['removed'];
    createdAt = json['createdAt']?.toString();
    updatedAt = json['updatedAt']?.toString();
    v = json['__v']?.toInt();
  }
  @override
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['_id'] = id;
    if (user != null) {
      data['user'] = user!.toJson();
    }
    data['media'] = media;
    data['job'] = job;
    data['status'] = status;
    data['removed'] = removed;
    data['createdAt'] = createdAt;
    data['updatedAt'] = updatedAt;
    data['__v'] = v;
    return data;
  }
}

class ApplicantsModelUser {
  String? id;
  String? name;
  String? email;
  String? phone;
  String? avatar;

  ApplicantsModelUser({
    this.id,
    this.name,
    this.email,
    this.phone,
    this.avatar,
  });
  ApplicantsModelUser.fromJson(Map<String, dynamic> json) {
    id = json['_id']?.toString();
    name = json['name']?.toString();
    email = json['email']?.toString();
    phone = json['phone']?.toString();
    avatar = json['avatar']?.toString();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['_id'] = id;
    data['name'] = name;
    data['email'] = email;
    data['phone'] = phone;
    data['avatar'] = avatar;
    return data;
  }
}
