import '../base_models/base_model.dart';

class JobModel extends BaseModel {
  JobModel({
    this.id,
    this.gender,
    this.ageTo,
    this.ageFrom,
    this.poster,
    this.uploadType,
    this.category,
    this.organization,
    this.jobStartDate,
    this.jobEndDate,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.userSaved,
    this.userApply,
    this.title,
    this.supTitle,
    this.description,
    this.area,
    this.expert,
  });
  @override
  JobModel fromJson(Map<String, dynamic> json) {
    return JobModel.fromJson(json);
  }

  JobModel.fromJson(dynamic json) {
    id = json['_id'];
    gender = json['gender'];
    ageTo = json['ageTo'];
    ageFrom = json['ageFrom'];
    poster = json['poster'];
    uploadType = json['uploadType'];
    category = json['category'] != null ? Category.fromJson(json['category']) : null;
    organization = json['organization'] != null ? Organization.fromJson(json['organization']) : null;
    jobStartDate = json['jobStartDate'];
    jobEndDate = json['jobEndDate'];
    removed = json['removed'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    v = json['__v'];
    userSaved = json['userSaved'];
    userApply = json['userApply'];
    title = json['title'];
    supTitle = json['supTitle'];
    description = json['description'];
    area = json['area'];
    expert = (json['expert'] != null) ? JobsModelExpert.fromJson(json['expert']) : null;
  }
  String? id;
  String? gender;
  String? ageTo;
  String? ageFrom;
  String? poster;
  String? uploadType;
  Category? category;
  Organization? organization;
  String? jobStartDate;
  String? jobEndDate;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  num? v;
  bool? userSaved;
  bool? userApply;
  String? title;
  String? supTitle;
  String? description;
  String? area;
  JobsModelExpert? expert;
  JobModel copyWith({
    String? id,
    String? gender,
    String? ageTo,
    String? ageFrom,
    String? poster,
    String? uploadType,
    Category? category,
    Organization? organization,
    String? jobStartDate,
    String? jobEndDate,
    bool? removed,
    String? createdAt,
    String? updatedAt,
    num? v,
    bool? userSaved,
    bool? userApply,
    String? title,
    String? supTitle,
    String? description,
    String? area,
  }) =>
      JobModel(
        id: id ?? this.id,
        gender: gender ?? this.gender,
        ageTo: ageTo ?? this.ageTo,
        ageFrom: ageFrom ?? this.ageFrom,
        poster: poster ?? this.poster,
        uploadType: uploadType ?? this.uploadType,
        category: category ?? this.category,
        organization: organization ?? this.organization,
        jobStartDate: jobStartDate ?? this.jobStartDate,
        jobEndDate: jobEndDate ?? this.jobEndDate,
        removed: removed ?? this.removed,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
        v: v ?? this.v,
        userSaved: userSaved ?? this.userSaved,
        userApply: userApply ?? this.userApply,
        title: title ?? this.title,
        supTitle: supTitle ?? this.supTitle,
        description: description ?? this.description,
        area: area ?? this.area,
      );
  @override
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['_id'] = id;
    map['gender'] = gender;
    map['ageTo'] = ageTo;
    map['ageFrom'] = ageFrom;
    map['poster'] = poster;
    map['uploadType'] = uploadType;
    if (category != null) {
      map['category'] = category?.toJson();
    }
    if (organization != null) {
      map['organization'] = organization?.toJson();
    }
    map['jobStartDate'] = jobStartDate;
    map['jobEndDate'] = jobEndDate;
    map['removed'] = removed;
    map['createdAt'] = createdAt;
    map['updatedAt'] = updatedAt;
    map['__v'] = v;
    map['userSaved'] = userSaved;
    map['userApply'] = userApply;
    map['title'] = title;
    map['supTitle'] = supTitle;
    map['description'] = description;
    map['area'] = area;
    if (expert != null) {
      map['expert'] = expert!.toJson();
    }
    return map;
  }
}

class Organization {
  Organization({
    this.id,
    this.name,
    this.website,
    this.industry,
    this.type,
    this.logo,
    this.promocode,
    this.userCount,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
  });

  Organization.fromJson(dynamic json) {
    id = json['_id'];
    name = json['name'];
    website = json['website'];
    industry = json['industry'];
    type = json['type'];
    logo = json['logo'];
    promocode = json['promocode'];
    userCount = json['userCount'];
    removed = json['removed'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    v = json['__v'];
  }
  String? id;
  String? name;
  String? website;
  String? industry;
  String? type;
  dynamic logo;
  String? promocode;
  num? userCount;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  num? v;
  Organization copyWith({
    String? id,
    String? name,
    String? website,
    String? industry,
    String? type,
    dynamic logo,
    String? promocode,
    num? userCount,
    bool? removed,
    String? createdAt,
    String? updatedAt,
    num? v,
  }) =>
      Organization(
        id: id ?? this.id,
        name: name ?? this.name,
        website: website ?? this.website,
        industry: industry ?? this.industry,
        type: type ?? this.type,
        logo: logo ?? this.logo,
        promocode: promocode ?? this.promocode,
        userCount: userCount ?? this.userCount,
        removed: removed ?? this.removed,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
        v: v ?? this.v,
      );
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['_id'] = id;
    map['name'] = name;
    map['website'] = website;
    map['industry'] = industry;
    map['type'] = type;
    map['logo'] = logo;
    map['promocode'] = promocode;
    map['userCount'] = userCount;
    map['removed'] = removed;
    map['createdAt'] = createdAt;
    map['updatedAt'] = updatedAt;
    map['__v'] = v;
    return map;
  }
}

class Category {
  Category({
    this.id,
    this.color,
    this.image,
    this.featured,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.title,
    this.description,
  });

  Category.fromJson(dynamic json) {
    id = json['_id'];
    color = json['color'];
    image = json['image'];
    featured = json['featured'];
    removed = json['removed'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    v = json['__v'];
    title = json['title'];
    description = json['description'];
  }
  String? id;
  String? color;
  String? image;
  bool? featured;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  num? v;
  String? title;
  String? description;
  Category copyWith({
    String? id,
    String? color,
    String? image,
    bool? featured,
    bool? removed,
    String? createdAt,
    String? updatedAt,
    num? v,
    String? title,
    String? description,
  }) =>
      Category(
        id: id ?? this.id,
        color: color ?? this.color,
        image: image ?? this.image,
        featured: featured ?? this.featured,
        removed: removed ?? this.removed,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
        v: v ?? this.v,
        title: title ?? this.title,
        description: description ?? this.description,
      );
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['_id'] = id;
    map['color'] = color;
    map['image'] = image;
    map['featured'] = featured;
    map['removed'] = removed;
    map['createdAt'] = createdAt;
    map['updatedAt'] = updatedAt;
    map['__v'] = v;
    map['title'] = title;
    map['description'] = description;
    return map;
  }
}

class JobsModelExpertExpert {
  String? name;
  String? website;
  String? scope;
  String? logo;

  JobsModelExpertExpert({
    this.name,
    this.website,
    this.scope,
    this.logo,
  });
  JobsModelExpertExpert.fromJson(Map<String, dynamic> json) {
    name = json['name']?.toString();
    website = json['website']?.toString();
    scope = json['scope']?.toString();
    logo = json['logo']?.toString();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['name'] = name;
    data['website'] = website;
    data['scope'] = scope;
    data['logo'] = logo;
    return data;
  }
}

class JobsModelExpert {
  String? id;
  String? name;
  String? email;
  String? phone;
  String? password;
  String? role;
  String? avatar;
  String? provider;
  String? profileStatus;
  bool? terms;
  bool? privacy;
  bool? eula;
  JobsModelExpertExpert? expert;
  bool? firstSubscription;
  bool? subscriptionEnd;
  bool? invitation;
  bool? verify;
  bool? verifyPhone;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  int? v;

  JobsModelExpert({
    this.id,
    this.name,
    this.email,
    this.phone,
    this.password,
    this.role,
    this.avatar,
    this.provider,
    this.profileStatus,
    this.terms,
    this.privacy,
    this.eula,
    this.expert,
    this.firstSubscription,
    this.subscriptionEnd,
    this.invitation,
    this.verify,
    this.verifyPhone,
    this.removed,
    this.createdAt,
    this.updatedAt,
  });
  JobsModelExpert.fromJson(Map<String, dynamic> json) {
    id = json['_id']?.toString();
    name = json['name']?.toString();
    email = json['email']?.toString();
    phone = json['phone']?.toString();
    password = json['password']?.toString();
    role = json['role']?.toString();
    avatar = json['avatar']?.toString();
    provider = json['provider']?.toString();
    profileStatus = json['profileStatus']?.toString();
    terms = json['terms'];
    privacy = json['privacy'];
    eula = json['eula'];
    expert = (json['expert'] != null) ? JobsModelExpertExpert.fromJson(json['expert']) : null;
    firstSubscription = json['firstSubscription'];
    subscriptionEnd = json['subscriptionEnd'];
    invitation = json['invitation'];
    verify = json['verify'];
    verifyPhone = json['verifyPhone'];
    removed = json['removed'];
    createdAt = json['createdAt']?.toString();
    updatedAt = json['updatedAt']?.toString();
    v = json['__v']?.toInt();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['_id'] = id;
    data['name'] = name;
    data['email'] = email;
    data['phone'] = phone;
    data['password'] = password;
    data['role'] = role;
    data['avatar'] = avatar;
    data['provider'] = provider;
    data['profileStatus'] = profileStatus;
    data['terms'] = terms;
    data['privacy'] = privacy;
    data['eula'] = eula;
    if (expert != null) {
      data['expert'] = expert!.toJson();
    }
    data['firstSubscription'] = firstSubscription;
    data['subscriptionEnd'] = subscriptionEnd;
    data['invitation'] = invitation;
    data['verify'] = verify;
    data['verifyPhone'] = verifyPhone;
    data['removed'] = removed;
    data['createdAt'] = createdAt;
    data['updatedAt'] = updatedAt;
    data['__v'] = v;
    return data;
  }
}
