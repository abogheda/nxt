import 'base_model.dart';

class BaseSingleResponse<T extends BaseModel> {
  final int? statusCode;
  dynamic message;
  bool? succeeded;
  final T? data;

  BaseSingleResponse({
    this.statusCode,
    this.message,
    this.succeeded,
    this.data,
  });

  factory BaseSingleResponse.fromJson(json, BaseModel? model) {
    // if (json['message'] == 'Invalid Token') {
    //   PopupHelper.showBasicSnack(msg: 'accountWasDeleted'.tr());
    //   HiveHelper.logout();
    //   if (HiveHelper.getUserInfo?.user?.facebookId != null) {
    //     FacebookAuth.instance.logOut().then((value) {});
    //   }
    //   if (HiveHelper.getUserInfo?.user?.googleId != null) {
    //     GoogleSignIn().signOut().then((value) {});
    //   }
    //   MagicRouter.navigateAndPopAll(const LoginScreen());
    // }
    return BaseSingleResponse(
      message: json["message"],
      statusCode: json["statusCode"],
      data: json == null ? null : model?.fromJson(json) as T,
    );
  }
}
