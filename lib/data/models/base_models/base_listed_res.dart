import 'base_model.dart';

class BaseListedResponse<T extends BaseModel> {
  final int? statusCode;
  String? message;
  bool? succeeded;
  final List<T>? data;

  BaseListedResponse({
    this.statusCode,
    this.message,
    this.succeeded,
    this.data,
  });

  factory BaseListedResponse.fromJson(json, BaseModel model) {
    // if (json['message'] == 'Invalid Token') {
    //   PopupHelper.showBasicSnack(msg: 'accountWasDeleted'.tr());
    //   HiveHelper.logout();
    //   if (HiveHelper.getUserInfo?.user?.facebookId != null) {
    //     FacebookAuth.instance.logOut().then((value) {});
    //   }
    //   if (HiveHelper.getUserInfo?.user?.googleId != null) {
    //     GoogleSignIn().signOut().then((value) {});
    //   }
    //   MagicRouter.navigateAndPopAll(const LoginScreen());
    // }
    return BaseListedResponse(
      // statusCode: json["statusCode"],
      // message: json["message"],
      // succeeded: json["succeeded"],
      data: json == null
          ? null
          : List<T>.from((json as List<dynamic>).map((x) => model.fromJson(x as Map<String, dynamic>)).toList()),
    );
  }
}
