// import 'package:nxt/data/models/base_models/base_model.dart';
//
// class SearchJobAndChallengesModel extends BaseModel {
//   String? id;
//   String? gender;
//   String? age;
//   String? poster;
//   String? media;
//   String? uploadType;
//   SearchJobAndChallengesModelCategory? category;
//   SearchJobAndChallengesModelOrganization? organization;
//   String? country;
//   String? jobStartDate;
//   String? jobEndDate;
//   bool? removed;
//   String? createdAt;
//   String? updatedAt;
//   int? v;
//   String? title;
//   String? supTitle;
//   String? description;
//   String? area;
//
//   SearchJobAndChallengesModel({
//     this.id,
//     this.gender,
//     this.age,
//     this.poster,
//     this.media,
//     this.uploadType,
//     this.category,
//     this.organization,
//     this.country,
//     this.jobStartDate,
//     this.jobEndDate,
//     this.removed,
//     this.createdAt,
//     this.updatedAt,
//     this.v,
//     this.title,
//     this.supTitle,
//     this.description,
//     this.area,
//   });
//   @override
//   SearchJobAndChallengesModel fromJson(Map<String, dynamic> json) {
//     return SearchJobAndChallengesModel.fromJson(json);
//   }
//
//   SearchJobAndChallengesModel.fromJson(Map<String, dynamic> json) {
//     id = json['_id']?.toString();
//     gender = json['gender']?.toString();
//     age = json['age']?.toString();
//     poster = json['poster']?.toString();
//     media = json['media']?.toString();
//     uploadType = json['uploadType']?.toString();
//     category = (json['category'] != null) ? SearchJobAndChallengesModelCategory.fromJson(json['category']) : null;
//     organization =
//         (json['organization'] != null) ? SearchJobAndChallengesModelOrganization.fromJson(json['organization']) : null;
//     country = json['country']?.toString();
//     jobStartDate = json['jobStartDate']?.toString();
//     jobEndDate = json['jobEndDate']?.toString();
//     removed = json['removed'];
//     createdAt = json['createdAt']?.toString();
//     updatedAt = json['updatedAt']?.toString();
//     v = json['__v']?.toInt();
//     title = json['title']?.toString();
//     supTitle = json['supTitle']?.toString();
//     description = json['description']?.toString();
//     area = json['area']?.toString();
//   }
//   @override
//   Map<String, dynamic> toJson() {
//     final data = <String, dynamic>{};
//     data['_id'] = id;
//     data['gender'] = gender;
//     data['age'] = age;
//     data['poster'] = poster;
//     data['media'] = media;
//     data['uploadType'] = uploadType;
//     if (category != null) {
//       data['category'] = category!.toJson();
//     }
//     if (organization != null) {
//       data['organization'] = organization!.toJson();
//     }
//     data['country'] = country;
//     data['jobStartDate'] = jobStartDate;
//     data['jobEndDate'] = jobEndDate;
//     data['removed'] = removed;
//     data['createdAt'] = createdAt;
//     data['updatedAt'] = updatedAt;
//     data['__v'] = v;
//     data['title'] = title;
//     data['supTitle'] = supTitle;
//     data['description'] = description;
//     data['area'] = area;
//     return data;
//   }
// }
//
// class SearchJobAndChallengesModelOrganization {
//   String? id;
//   String? name;
//   String? website;
//   String? industry;
//   String? size;
//   String? type;
//   String? logo;
//   String? promocode;
//   int? userCount;
//   bool? removed;
//   String? createdAt;
//   String? updatedAt;
//   int? v;
//
//   SearchJobAndChallengesModelOrganization({
//     this.id,
//     this.name,
//     this.website,
//     this.industry,
//     this.size,
//     this.type,
//     this.logo,
//     this.promocode,
//     this.userCount,
//     this.removed,
//     this.createdAt,
//     this.updatedAt,
//     this.v,
//   });
//   SearchJobAndChallengesModelOrganization.fromJson(Map<String, dynamic> json) {
//     id = json['_id']?.toString();
//     name = json['name']?.toString();
//     website = json['website']?.toString();
//     industry = json['industry']?.toString();
//     size = json['size']?.toString();
//     type = json['type']?.toString();
//     logo = json['logo']?.toString();
//     promocode = json['promocode']?.toString();
//     userCount = json['userCount']?.toInt();
//     removed = json['removed'];
//     createdAt = json['createdAt']?.toString();
//     updatedAt = json['updatedAt']?.toString();
//     v = json['__v']?.toInt();
//   }
//   Map<String, dynamic> toJson() {
//     final data = <String, dynamic>{};
//     data['_id'] = id;
//     data['name'] = name;
//     data['website'] = website;
//     data['industry'] = industry;
//     data['size'] = size;
//     data['type'] = type;
//     data['logo'] = logo;
//     data['promocode'] = promocode;
//     data['userCount'] = userCount;
//     data['removed'] = removed;
//     data['createdAt'] = createdAt;
//     data['updatedAt'] = updatedAt;
//     data['__v'] = v;
//     return data;
//   }
// }
//
// class SearchJobAndChallengesModelCategoryLang {
//   String? title;
//   String? description;
//
//   SearchJobAndChallengesModelCategoryLang({
//     this.title,
//     this.description,
//   });
//   SearchJobAndChallengesModelCategoryLang.fromJson(Map<String, dynamic> json) {
//     title = json['title']?.toString();
//     description = json['description']?.toString();
//   }
//   Map<String, dynamic> toJson() {
//     final data = <String, dynamic>{};
//     data['title'] = title;
//     data['description'] = description;
//     return data;
//   }
// }
//
// class SearchJobAndChallengesModelCategory {
//   String? id;
//   List<SearchJobAndChallengesModelCategoryLang?>? lang;
//   String? color;
//   String? image;
//   bool? featured;
//   bool? removed;
//   String? createdAt;
//   String? updatedAt;
//   int? v;
//   List<String?>? tags;
//
//   SearchJobAndChallengesModelCategory({
//     this.id,
//     this.lang,
//     this.color,
//     this.image,
//     this.featured,
//     this.removed,
//     this.createdAt,
//     this.updatedAt,
//     this.v,
//     this.tags,
//   });
//   SearchJobAndChallengesModelCategory.fromJson(Map<String, dynamic> json) {
//     id = json['_id']?.toString();
//     if (json['lang'] != null) {
//       final v = json['lang'];
//       final arr0 = <SearchJobAndChallengesModelCategoryLang>[];
//       v.forEach((v) {
//         arr0.add(SearchJobAndChallengesModelCategoryLang.fromJson(v));
//       });
//       lang = arr0;
//     }
//     color = json['color']?.toString();
//     image = json['image']?.toString();
//     featured = json['featured'];
//     removed = json['removed'];
//     createdAt = json['createdAt']?.toString();
//     updatedAt = json['updatedAt']?.toString();
//     v = json['__v']?.toInt();
//     if (json['tags'] != null) {
//       final v = json['tags'];
//       final arr0 = <String>[];
//       v.forEach((v) {
//         arr0.add(v.toString());
//       });
//       tags = arr0;
//     }
//   }
//   Map<String, dynamic> toJson() {
//     final data = <String, dynamic>{};
//     data['_id'] = id;
//     if (lang != null) {
//       final v = lang;
//       final arr0 = [];
//       for (var v in v!) {
//         arr0.add(v!.toJson());
//       }
//       data['lang'] = arr0;
//     }
//     data['color'] = color;
//     data['image'] = image;
//     data['featured'] = featured;
//     data['removed'] = removed;
//     data['createdAt'] = createdAt;
//     data['updatedAt'] = updatedAt;
//     data['__v'] = v;
//     if (tags != null) {
//       final v = tags;
//       final arr0 = [];
//       for (var v in v!) {
//         arr0.add(v);
//       }
//       data['tags'] = arr0;
//     }
//     return data;
//   }
// }
