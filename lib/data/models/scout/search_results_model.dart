import '../base_models/base_model.dart';

class SearchResultsModel extends BaseModel {
  SearchResultsModel({
    this.id,
    this.name,
    this.email,
    this.phone,
    this.birthDate,
    this.password,
    this.role,
    this.avatar,
    this.provider,
    this.providerId,
    this.profileStatus,
    this.terms,
    this.privacy,
    this.expert,
    this.firstSubscription,
    this.subscriptionEnd,
    this.verify,
    this.verifyPhone,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.country,
    this.countryCode,
    this.countryName,
    this.confirmPhone,
    this.package,
  });
  @override
  SearchResultsModel fromJson(Map<String, dynamic> json) {
    return SearchResultsModel.fromJson(json);
  }

  SearchResultsModel.fromJson(dynamic json) {
    id = json['_id'];
    name = json['name'];
    email = json['email'];
    phone = json['phone'];
    birthDate = json['birthDate'];
    password = json['password'];
    role = json['role'];
    avatar = json['avatar'];
    provider = json['provider'];
    providerId = json['providerId'];
    profileStatus = json['profileStatus'];
    terms = json['terms'];
    privacy = json['privacy'];
    expert = json['expert'];
    firstSubscription = json['firstSubscription'];
    subscriptionEnd = json['subscriptionEnd'];
    verify = json['verify'];
    verifyPhone = json['verifyPhone'];
    removed = json['removed'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    v = json['__v'];
    country = json['country'];
    countryCode = json['countryCode'];
    countryName = json['countryName'];
    confirmPhone = json['confirmPhone'] != null ? ConfirmPhone.fromJson(json['confirmPhone']) : null;
    package = json['package'] != null ? Package.fromJson(json['package']) : null;
  }
  String? id;
  String? name;
  String? email;
  String? phone;
  String? birthDate;
  String? password;
  String? role;
  String? avatar;
  String? provider;
  dynamic providerId;
  String? profileStatus;
  bool? terms;
  bool? privacy;
  dynamic expert;
  bool? firstSubscription;
  bool? subscriptionEnd;
  bool? verify;
  bool? verifyPhone;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  num? v;
  String? country;
  String? countryCode;
  String? countryName;
  ConfirmPhone? confirmPhone;
  Package? package;
  SearchResultsModel copyWith({
    String? id,
    String? name,
    String? email,
    String? phone,
    String? birthDate,
    String? password,
    String? role,
    String? avatar,
    String? provider,
    dynamic providerId,
    String? profileStatus,
    bool? terms,
    bool? privacy,
    dynamic expert,
    bool? firstSubscription,
    bool? subscriptionEnd,
    bool? verify,
    bool? verifyPhone,
    bool? removed,
    String? createdAt,
    String? updatedAt,
    num? v,
    String? country,
    String? countryCode,
    String? countryName,
    ConfirmPhone? confirmPhone,
    Package? package,
  }) =>
      SearchResultsModel(
        id: id ?? this.id,
        name: name ?? this.name,
        email: email ?? this.email,
        phone: phone ?? this.phone,
        birthDate: birthDate ?? this.birthDate,
        password: password ?? this.password,
        role: role ?? this.role,
        avatar: avatar ?? this.avatar,
        provider: provider ?? this.provider,
        providerId: providerId ?? this.providerId,
        profileStatus: profileStatus ?? this.profileStatus,
        terms: terms ?? this.terms,
        privacy: privacy ?? this.privacy,
        expert: expert ?? this.expert,
        firstSubscription: firstSubscription ?? this.firstSubscription,
        subscriptionEnd: subscriptionEnd ?? this.subscriptionEnd,
        verify: verify ?? this.verify,
        verifyPhone: verifyPhone ?? this.verifyPhone,
        removed: removed ?? this.removed,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
        v: v ?? this.v,
        country: country ?? this.country,
        countryCode: countryCode ?? this.countryCode,
        countryName: countryName ?? this.countryName,
        confirmPhone: confirmPhone ?? this.confirmPhone,
        package: package ?? this.package,
      );
  @override
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['_id'] = id;
    map['name'] = name;
    map['email'] = email;
    map['phone'] = phone;
    map['birthDate'] = birthDate;
    map['password'] = password;
    map['role'] = role;
    map['avatar'] = avatar;
    map['provider'] = provider;
    map['providerId'] = providerId;
    map['profileStatus'] = profileStatus;
    map['terms'] = terms;
    map['privacy'] = privacy;
    map['expert'] = expert;
    map['firstSubscription'] = firstSubscription;
    map['subscriptionEnd'] = subscriptionEnd;
    map['verify'] = verify;
    map['verifyPhone'] = verifyPhone;
    map['removed'] = removed;
    map['createdAt'] = createdAt;
    map['updatedAt'] = updatedAt;
    map['__v'] = v;
    map['country'] = country;
    map['countryCode'] = countryCode;
    map['countryName'] = countryName;
    if (confirmPhone != null) {
      map['confirmPhone'] = confirmPhone?.toJson();
    }
    if (package != null) {
      map['package'] = package?.toJson();
    }
    return map;
  }
}

class Package {
  Package({
    this.id,
    this.user,
    this.plan,
    this.renewalCount,
    this.paymentMethod,
    this.expiryDate,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
  });

  Package.fromJson(dynamic json) {
    id = json['_id'];
    user = json['user'];
    plan = json['plan'] != null ? Plan.fromJson(json['plan']) : null;
    renewalCount = json['renewalCount'];
    paymentMethod = json['paymentMethod'];
    expiryDate = json['expiryDate'];
    removed = json['removed'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    v = json['__v'];
  }
  String? id;
  String? user;
  Plan? plan;
  num? renewalCount;
  String? paymentMethod;
  String? expiryDate;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  num? v;
  Package copyWith({
    String? id,
    String? user,
    Plan? plan,
    num? renewalCount,
    String? paymentMethod,
    String? expiryDate,
    bool? removed,
    String? createdAt,
    String? updatedAt,
    num? v,
  }) =>
      Package(
        id: id ?? this.id,
        user: user ?? this.user,
        plan: plan ?? this.plan,
        renewalCount: renewalCount ?? this.renewalCount,
        paymentMethod: paymentMethod ?? this.paymentMethod,
        expiryDate: expiryDate ?? this.expiryDate,
        removed: removed ?? this.removed,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
        v: v ?? this.v,
      );
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['_id'] = id;
    map['user'] = user;
    if (plan != null) {
      map['plan'] = plan?.toJson();
    }
    map['renewalCount'] = renewalCount;
    map['paymentMethod'] = paymentMethod;
    map['expiryDate'] = expiryDate;
    map['removed'] = removed;
    map['createdAt'] = createdAt;
    map['updatedAt'] = updatedAt;
    map['__v'] = v;
    return map;
  }
}

class Plan {
  Plan({
    this.id,
    this.lang,
    this.price,
    this.months,
    this.country,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
  });

  Plan.fromJson(dynamic json) {
    id = json['_id'];
    if (json['lang'] != null) {
      lang = [];
      json['lang'].forEach((v) {
        lang?.add(Lang.fromJson(v));
      });
    }
    price = json['price'];
    months = json['months'];
    country = json['country'];
    removed = json['removed'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    v = json['__v'];
  }
  String? id;
  List<Lang>? lang;
  num? price;
  num? months;
  String? country;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  num? v;
  Plan copyWith({
    String? id,
    List<Lang>? lang,
    num? price,
    num? months,
    String? country,
    bool? removed,
    String? createdAt,
    String? updatedAt,
    num? v,
  }) =>
      Plan(
        id: id ?? this.id,
        lang: lang ?? this.lang,
        price: price ?? this.price,
        months: months ?? this.months,
        country: country ?? this.country,
        removed: removed ?? this.removed,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
        v: v ?? this.v,
      );
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['_id'] = id;
    if (lang != null) {
      map['lang'] = lang?.map((v) => v.toJson()).toList();
    }
    map['price'] = price;
    map['months'] = months;
    map['country'] = country;
    map['removed'] = removed;
    map['createdAt'] = createdAt;
    map['updatedAt'] = updatedAt;
    map['__v'] = v;
    return map;
  }
}

class Lang {
  Lang({
    this.title,
    this.supTitle,
    this.features,
  });

  Lang.fromJson(dynamic json) {
    title = json['title'];
    supTitle = json['supTitle'];
    features = json['features'] != null ? json['features'].cast<String>() : [];
  }
  String? title;
  String? supTitle;
  List<String>? features;
  Lang copyWith({
    String? title,
    String? supTitle,
    List<String>? features,
  }) =>
      Lang(
        title: title ?? this.title,
        supTitle: supTitle ?? this.supTitle,
        features: features ?? this.features,
      );
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['title'] = title;
    map['supTitle'] = supTitle;
    map['features'] = features;
    return map;
  }
}

class ConfirmPhone {
  ConfirmPhone({
    this.codesended,
    this.expiresAt,
  });

  ConfirmPhone.fromJson(dynamic json) {
    codesended = json['codesended'];
    expiresAt = json['expiresAt'];
  }
  dynamic codesended;
  String? expiresAt;
  ConfirmPhone copyWith({
    dynamic codesended,
    String? expiresAt,
  }) =>
      ConfirmPhone(
        codesended: codesended ?? this.codesended,
        expiresAt: expiresAt ?? this.expiresAt,
      );
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['codesended'] = codesended;
    map['expiresAt'] = expiresAt;
    return map;
  }
}
