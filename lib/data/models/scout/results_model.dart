import '../base_models/base_model.dart';

class ResultsModel extends BaseModel {
  ResultsModel({
    this.id,
    this.user,
    this.talents,
    this.categories,
    this.specialSkills,
    this.gender,
    this.weight,
    this.height,
    this.weightUnit,
    this.heightUnit,
    this.skinColor,
    this.hairColor,
    this.eyeColor,
    this.portfolioLink,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
  });
  @override
  ResultsModel fromJson(Map<String, dynamic> json) {
    return ResultsModel.fromJson(json);
  }

  ResultsModel.fromJson(dynamic json) {
    id = json['_id'];
    user = json['user'] != null ? User.fromJson(json['user']) : null;
    if (json['talents'] != null) {
      talents = [];
      json['talents'].forEach((v) {
        talents?.add(Talents.fromJson(v));
      });
    }
    if (json['categories'] != null) {
      categories = [];
      json['categories'].forEach((v) {
        categories?.add(Categories.fromJson(v));
      });
    }
    specialSkills = json['specialSkills'] != null ? json['specialSkills'].cast<String>() : [];
    gender = json['gender'];
    weight = json['weight'];
    height = json['height'];
    weightUnit = json['weightUnit'];
    heightUnit = json['heightUnit'];
    skinColor = json['skinColor'];
    hairColor = json['hairColor'];
    eyeColor = json['eyeColor'];
    portfolioLink = json['portfolioLink'];
    removed = json['removed'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    v = json['__v'];
  }
  String? id;
  User? user;
  List<Talents>? talents;
  List<Categories>? categories;
  List<String>? specialSkills;
  String? gender;
  num? weight;
  num? height;
  String? weightUnit;
  String? heightUnit;
  String? skinColor;
  String? hairColor;
  String? eyeColor;
  String? portfolioLink;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  num? v;
  ResultsModel copyWith({
    String? id,
    User? user,
    List<Talents>? talents,
    List<Categories>? categories,
    List<String>? specialSkills,
    String? gender,
    num? weight,
    num? height,
    String? weightUnit,
    String? heightUnit,
    String? skinColor,
    String? hairColor,
    String? eyeColor,
    String? portfolioLink,
    bool? removed,
    String? createdAt,
    String? updatedAt,
    num? v,
  }) =>
      ResultsModel(
        id: id ?? this.id,
        user: user ?? this.user,
        talents: talents ?? this.talents,
        categories: categories ?? this.categories,
        specialSkills: specialSkills ?? this.specialSkills,
        gender: gender ?? this.gender,
        weight: weight ?? this.weight,
        height: height ?? this.height,
        weightUnit: weightUnit ?? this.weightUnit,
        heightUnit: heightUnit ?? this.heightUnit,
        skinColor: skinColor ?? this.skinColor,
        hairColor: hairColor ?? this.hairColor,
        eyeColor: eyeColor ?? this.eyeColor,
        portfolioLink: portfolioLink ?? this.portfolioLink,
        removed: removed ?? this.removed,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
        v: v ?? this.v,
      );
  @override
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['_id'] = id;
    if (user != null) {
      map['user'] = user?.toJson();
    }
    if (talents != null) {
      map['talents'] = talents?.map((v) => v.toJson()).toList();
    }
    if (categories != null) {
      map['categories'] = categories?.map((v) => v.toJson()).toList();
    }
    map['specialSkills'] = specialSkills;
    map['gender'] = gender;
    map['weight'] = weight;
    map['height'] = height;
    map['weightUnit'] = weightUnit;
    map['heightUnit'] = heightUnit;
    map['skinColor'] = skinColor;
    map['hairColor'] = hairColor;
    map['eyeColor'] = eyeColor;
    map['portfolioLink'] = portfolioLink;
    map['removed'] = removed;
    map['createdAt'] = createdAt;
    map['updatedAt'] = updatedAt;
    map['__v'] = v;
    return map;
  }
}

class Categories {
  Categories({
    this.id,
    this.lang,
    this.color,
    this.image,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.featured,
    this.tags,
  });

  Categories.fromJson(dynamic json) {
    id = json['_id'];
    if (json['lang'] != null) {
      lang = [];
      json['lang'].forEach((v) {
        lang?.add(Lang.fromJson(v));
      });
    }
    color = json['color'];
    image = json['image'];
    removed = json['removed'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    v = json['__v'];
    featured = json['featured'];
    tags = json['tags'] != null ? json['tags'].cast<String>() : [];
  }
  String? id;
  List<Lang>? lang;
  String? color;
  String? image;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  num? v;
  bool? featured;
  List<String>? tags;
  Categories copyWith({
    String? id,
    List<Lang>? lang,
    String? color,
    String? image,
    bool? removed,
    String? createdAt,
    String? updatedAt,
    num? v,
    bool? featured,
    List<String>? tags,
  }) =>
      Categories(
        id: id ?? this.id,
        lang: lang ?? this.lang,
        color: color ?? this.color,
        image: image ?? this.image,
        removed: removed ?? this.removed,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
        v: v ?? this.v,
        featured: featured ?? this.featured,
        tags: tags ?? this.tags,
      );
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['_id'] = id;
    if (lang != null) {
      map['lang'] = lang?.map((v) => v.toJson()).toList();
    }
    map['color'] = color;
    map['image'] = image;
    map['removed'] = removed;
    map['createdAt'] = createdAt;
    map['updatedAt'] = updatedAt;
    map['__v'] = v;
    map['featured'] = featured;
    map['tags'] = tags;
    return map;
  }
}

class Lang {
  Lang({
    this.title,
    this.description,
  });

  Lang.fromJson(dynamic json) {
    title = json['title'];
    description = json['description'];
  }
  String? title;
  String? description;
  Lang copyWith({
    String? title,
    String? description,
  }) =>
      Lang(
        title: title ?? this.title,
        description: description ?? this.description,
      );
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['title'] = title;
    map['description'] = description;
    return map;
  }
}

class Talents {
  Talents({
    this.id,
    this.lang,
    this.category,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
  });

  Talents.fromJson(dynamic json) {
    id = json['_id'];
    if (json['lang'] != null) {
      lang = [];
      json['lang'].forEach((v) {
        lang?.add(Lang.fromJson(v));
      });
    }
    category = json['category'];
    removed = json['removed'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    v = json['__v'];
  }
  String? id;
  List<Lang>? lang;
  String? category;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  num? v;
  Talents copyWith({
    String? id,
    List<Lang>? lang,
    String? category,
    bool? removed,
    String? createdAt,
    String? updatedAt,
    num? v,
  }) =>
      Talents(
        id: id ?? this.id,
        lang: lang ?? this.lang,
        category: category ?? this.category,
        removed: removed ?? this.removed,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
        v: v ?? this.v,
      );
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['_id'] = id;
    if (lang != null) {
      map['lang'] = lang?.map((v) => v.toJson()).toList();
    }
    map['category'] = category;
    map['removed'] = removed;
    map['createdAt'] = createdAt;
    map['updatedAt'] = updatedAt;
    map['__v'] = v;
    return map;
  }
}

class User {
  User({
    this.id,
    this.name,
    this.email,
    this.phone,
    this.birthDate,
    this.role,
    this.avatar,
    this.provider,
    this.profileStatus,
    this.terms,
    this.privacy,
    this.firstSubscription,
    this.subscriptionEnd,
    this.verify,
    this.verifyPhone,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.country,
    this.countryCode,
    this.countryName,
    this.package,
  });

  User.fromJson(dynamic json) {
    id = json['_id'];
    name = json['name'];
    email = json['email'];
    phone = json['phone'];
    birthDate = json['birthDate'];
    role = json['role'];
    avatar = json['avatar'];
    provider = json['provider'];
    profileStatus = json['profileStatus'];
    terms = json['terms'];
    privacy = json['privacy'];
    firstSubscription = json['firstSubscription'];
    subscriptionEnd = json['subscriptionEnd'];
    verify = json['verify'];
    verifyPhone = json['verifyPhone'];
    removed = json['removed'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    v = json['__v'];
    country = json['country'];
    countryCode = json['countryCode'];
    countryName = json['countryName'];
    package = json['package'];
  }
  String? id;
  String? name;
  String? email;
  String? phone;
  String? birthDate;
  String? role;
  String? avatar;
  String? provider;
  String? profileStatus;
  bool? terms;
  bool? privacy;
  bool? firstSubscription;
  bool? subscriptionEnd;
  bool? verify;
  bool? verifyPhone;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  num? v;
  String? country;
  String? countryCode;
  String? countryName;
  String? package;
  User copyWith({
    String? id,
    String? name,
    String? email,
    String? phone,
    String? birthDate,
    String? role,
    String? avatar,
    String? provider,
    String? profileStatus,
    bool? terms,
    bool? privacy,
    bool? firstSubscription,
    bool? subscriptionEnd,
    bool? verify,
    bool? verifyPhone,
    bool? removed,
    String? createdAt,
    String? updatedAt,
    num? v,
    String? country,
    String? countryCode,
    String? countryName,
    String? package,
  }) =>
      User(
        id: id ?? this.id,
        name: name ?? this.name,
        email: email ?? this.email,
        phone: phone ?? this.phone,
        birthDate: birthDate ?? this.birthDate,
        role: role ?? this.role,
        avatar: avatar ?? this.avatar,
        provider: provider ?? this.provider,
        profileStatus: profileStatus ?? this.profileStatus,
        terms: terms ?? this.terms,
        privacy: privacy ?? this.privacy,
        firstSubscription: firstSubscription ?? this.firstSubscription,
        subscriptionEnd: subscriptionEnd ?? this.subscriptionEnd,
        verify: verify ?? this.verify,
        verifyPhone: verifyPhone ?? this.verifyPhone,
        removed: removed ?? this.removed,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
        v: v ?? this.v,
        country: country ?? this.country,
        countryCode: countryCode ?? this.countryCode,
        countryName: countryName ?? this.countryName,
        package: package ?? this.package,
      );
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['_id'] = id;
    map['name'] = name;
    map['email'] = email;
    map['phone'] = phone;
    map['birthDate'] = birthDate;
    map['role'] = role;
    map['avatar'] = avatar;
    map['provider'] = provider;
    map['profileStatus'] = profileStatus;
    map['terms'] = terms;
    map['privacy'] = privacy;
    map['firstSubscription'] = firstSubscription;
    map['subscriptionEnd'] = subscriptionEnd;
    map['verify'] = verify;
    map['verifyPhone'] = verifyPhone;
    map['removed'] = removed;
    map['createdAt'] = createdAt;
    map['updatedAt'] = updatedAt;
    map['__v'] = v;
    map['country'] = country;
    map['countryCode'] = countryCode;
    map['countryName'] = countryName;
    map['package'] = package;
    return map;
  }
}
