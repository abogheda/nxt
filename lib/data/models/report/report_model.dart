import '../base_models/base_model.dart';

class ReportModel extends BaseModel {
  ReportModel({
    this.user,
    this.contentType,
    this.removed,
    this.id,
    this.createdAt,
    this.updatedAt,
    this.v,
  });
  @override
  ReportModel fromJson(Map<String, dynamic> json) {
    return ReportModel.fromJson(json);
  }

  ReportModel.fromJson(dynamic json) {
    user = json['user'];
    contentType = json['contentType'];
    removed = json['removed'];
    id = json['_id'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    v = json['__v'];
  }
  String? user;
  String? contentType;
  bool? removed;
  String? id;
  String? createdAt;
  String? updatedAt;
  num? v;
  ReportModel copyWith({
    String? user,
    String? contentType,
    bool? removed,
    String? id,
    String? createdAt,
    String? updatedAt,
    num? v,
  }) =>
      ReportModel(
        user: user ?? this.user,
        contentType: contentType ?? this.contentType,
        removed: removed ?? this.removed,
        id: id ?? this.id,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
        v: v ?? this.v,
      );
  @override
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['user'] = user;
    map['contentType'] = contentType;
    map['removed'] = removed;
    map['_id'] = id;
    map['createdAt'] = createdAt;
    map['updatedAt'] = updatedAt;
    map['__v'] = v;
    return map;
  }
}
