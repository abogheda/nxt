// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

User _$UserFromJson(Map<String, dynamic> json) => User(
      shots:
          (json['shots'] as List<dynamic>?)?.map((e) => e as String).toList(),
      avatar: json['avatar'] as String?,
      name: json['name'] as String?,
      package:
          json['package'] == null ? null : Package.fromJson(json['package']),
      email: json['email'] as String?,
      experiences: (json['experiences'] as List<dynamic>?)
          ?.map((e) => JobExperienceModel.fromJson(e as Map<String, dynamic>))
          .toList(),
      talents:
          (json['talents'] as List<dynamic>?)?.map((e) => e as String).toList(),
      specialSkills: (json['specialSkills'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      gender: json['gender'] as String?,
      weight: json['weight'] as num?,
      height: json['height'] as num?,
      skinColor: json['skinColor'] as String?,
      hairColor: json['hairColor'] as String?,
      eyeColor: json['eyeColor'] as String?,
      portfolioLink: json['portfolioLink'] as String?,
      id: json['_id'] as String?,
      mobileNumber: json['mobileNumber'] as String?,
      birthDate: json['birthDate'] as String?,
      createdAt: json['createdAt'] == null
          ? null
          : DateTime.parse(json['createdAt'] as String),
      isProfileComplete: json['isProfileComplete'] as bool?,
      verifiedMobile: json['verifiedMobile'] as bool?,
      fullName: json['fullName'] as String?,
      firstName: json['firstName'] as String?,
      lastName: json['lastName'] as String?,
      profileStatus:
          $enumDecodeNullable(_$ProfileStatusEnumMap, json['profileStatus']),
    );

Map<String, dynamic> _$UserToJson(User instance) {
  final val = <String, dynamic>{};

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('_id', instance.id);
  writeNotNull('avatar', instance.avatar);
  writeNotNull('mobileNumber', instance.mobileNumber);
  writeNotNull('birthDate', instance.birthDate);
  writeNotNull('createdAt', instance.createdAt?.toIso8601String());
  writeNotNull('isProfileComplete', instance.isProfileComplete);
  writeNotNull('verifiedMobile', instance.verifiedMobile);
  writeNotNull('name', instance.name);
  writeNotNull('package', instance.package);
  writeNotNull('email', instance.email);
  writeNotNull('fullName', instance.fullName);
  writeNotNull('firstName', instance.firstName);
  writeNotNull('lastName', instance.lastName);
  writeNotNull('talents', instance.talents);
  writeNotNull('specialSkills', instance.specialSkills);
  writeNotNull('gender', instance.gender);
  writeNotNull('weight', instance.weight);
  writeNotNull('height', instance.height);
  writeNotNull('skinColor', instance.skinColor);
  writeNotNull('hairColor', instance.hairColor);
  writeNotNull('eyeColor', instance.eyeColor);
  writeNotNull('portfolioLink', instance.portfolioLink);
  writeNotNull('experiences', instance.experiences);
  writeNotNull('shots', instance.shots);
  writeNotNull('profileStatus', _$ProfileStatusEnumMap[instance.profileStatus]);
  return val;
}

const _$ProfileStatusEnumMap = {
  ProfileStatus.complete: 'Completed',
  ProfileStatus.skipped: 'Skipped',
  ProfileStatus.notCompleted: 'NotCompleted',
};
