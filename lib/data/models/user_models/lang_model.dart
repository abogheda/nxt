import 'package:json_annotation/json_annotation.dart';

part 'lang_model.g.dart';
/*
 {
      "_id": "630be62bcc517c89925cf542",
      "title": "Movies",
      "description": "Movies",
      "removed": false,
      "createdAt": "2022-08-28T22:03:23.169Z",
      "updatedAt": "2022-08-28T22:03:23.169Z",
      "__v": 0
    },
*/

@JsonSerializable()
class LangModel {
  @JsonKey(name: '_id')
  final String? title;
  final String? description;

  LangModel({
    this.title,
    this.description,
  });

  factory LangModel.fromJson(Map<String, dynamic> json) => _$LangModelFromJson(json);

  Map<String, dynamic> toJson() => _$LangModelToJson(this);
}
