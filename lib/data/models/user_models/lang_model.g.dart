// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'lang_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LangModel _$LangModelFromJson(Map<String, dynamic> json) => LangModel(
      title: json['_id'] as String?,
      description: json['description'] as String?,
    );

Map<String, dynamic> _$LangModelToJson(LangModel instance) => <String, dynamic>{
      '_id': instance.title,
      'description': instance.description,
    };
