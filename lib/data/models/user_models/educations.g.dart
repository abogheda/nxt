// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'educations.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Educations _$EducationsFromJson(Map<String, dynamic> json) => Educations(
      educationType: json['educationType'] as String?,
      courseName: json['courseName'] as String?,
      courseLocation: json['courseLocation'] as String?,
    );

Map<String, dynamic> _$EducationsToJson(Educations instance) =>
    <String, dynamic>{
      'educationType': instance.educationType,
      'courseName': instance.courseName,
      'courseLocation': instance.courseLocation,
    };
