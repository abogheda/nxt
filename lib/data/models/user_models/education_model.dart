class EducationModel {
  String? educationType;
  String? courseName;
  String? courseLocation;

  EducationModel({this.educationType, this.courseName, this.courseLocation});

  EducationModel.fromJson(Map<String, dynamic> json) {
    educationType = json['educationType'];
    courseName = json['courseName'];
    courseLocation = json['courseLocation'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['educationType'] = educationType;
    data['courseName'] = courseName;
    data['courseLocation'] = courseLocation;
    return data;
  }
}
