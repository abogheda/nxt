import 'package:json_annotation/json_annotation.dart';
import 'package:nxt/data/models/user_models/social_media.dart';
import 'education_model.dart';
import 'educations.dart';
import 'job_experience_model.dart';

part 'complete_prof_model.g.dart';

@JsonSerializable(explicitToJson: true, includeIfNull: false)
class CompleteProfileModel {
  List<String>? talents;
  List<String>? categories;
  List<String>? shots;
  List<String>? photos;
  List<String>? audios;
  List<String>? documents;
  List<String>? specialSkills;
  // List<EducationModel>? educationExperiences;
  String? gender;
  String? bio;
  double? height;
  double? weight;
  String? skinColor;
  String? hairColor;
  String? eyeColor;
  String? portfolioLink;
  String? nationality;
  String? country;
  String? membership;
  String? membershipDocument;

  List<JobExperienceModel>? experiences;
  List<EducationModel>? educations;
  List<SocialMedia>? socialLinks;

  CompleteProfileModel({
    this.talents,
    this.educations,
    this.membership,
    this.membershipDocument,
    this.socialLinks,
    this.categories,
    this.experiences,
    this.audios,
    this.documents,
    this.photos,
    this.eyeColor,
    this.hairColor,
    this.height,
    this.portfolioLink,
    // this.educationExperiences,
    this.bio,
    this.nationality,
    this.country,
    this.skinColor,
    this.specialSkills,
    this.gender,
    this.weight,
    this.shots,
  });

  factory CompleteProfileModel.fromJson(Map<String, dynamic> json) => _$CompleteProfileModelFromJson(json);

  Map<String, dynamic> toJson() => _$CompleteProfileModelToJson(this);
}
