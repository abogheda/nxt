import 'package:nxt/data/models/user_models/social_media.dart';

import '../package/package_model.dart';
import '../base_models/base_model.dart';
import '../skills_model.dart';
import '../talent_model.dart';
import '../user_model.dart';
import 'education_model.dart';
import 'job_experience_model.dart';
import 'membership_model.dart';

class UsersProfileModel extends BaseModel {
  UsersProfileModel(
      {this.id,
      this.bio,
      this.nationality,
      this.user,
      this.membership,
      this.actingAgeFrom,
      this.actingAgeTo,
      this.talents,
      this.socialMedia,
      this.educations,
      this.categories,
      this.specialSkills,
      this.gender,
      this.weight,
      this.height,
      this.skinColor,
      this.hairColor,
      this.eyeColor,
      this.portfolioLink,
      this.experiences,
      this.shots,
      this.removed,
      this.createdAt,
      this.updatedAt,
      this.audios,
      this.photos,
      this.documents});
  @override
  UsersProfileModel fromJson(Map<String, dynamic> json) {
    return UsersProfileModel.fromJson(json);
  }

  UsersProfileModel.fromJson(dynamic json) {
    id = json['_id'];
    bio = json['bio'];
    actingAgeFrom = json['actingAgeFrom'];
    actingAgeTo = json['actingAgeTo'];
    user = json['user'] != null ? UserProfile.fromJson(json['user']) : null;
    membership = json['membership'] != null
        ? MembershipModel.fromJson(json['membership'])
        : null;
    if (json['talents'] != null) {
      talents = [];
      json['talents'].forEach((v) {
        talents?.add(TalentModel.fromJson(v));
      });
    }
    if (json['educations'] != null) {
      educations = <EducationModel>[];
      json['educations'].forEach((v) {
        educations!.add(EducationModel.fromJson(v));
      });
    }
    if (json['categories'] != null) {
      categories = [];
      json['categories'].forEach((v) {
        categories?.add(Categories.fromJson(v));
      });
    }
    if (json['specialSkills'] != null) {
      specialSkills = [];
      json['specialSkills'].forEach((v) {
        specialSkills?.add(SkillsModel.fromJson(v));
      });
    }
    gender = json['gender'];
    nationality = json['nationality'];
    weight = json['weight'];
    height = json['height'];
    skinColor = json['skinColor'];
    hairColor = json['hairColor'];
    eyeColor = json['eyeColor'];
    portfolioLink = json['portfolioLink'];
    if (json['experiences'] != null) {
      experiences = [];
      json['experiences'].forEach((v) {
        experiences?.add(JobExperienceModel.fromJson(v));
      });
    }
    if (json['socialLinks'] != null) {
      socialMedia = [];
      json['socialLinks'].forEach((v) {
        socialMedia?.add(SocialMedia.fromJson(v));
      });
    }
    shots = json['shots'] != null ? json['shots'].cast<String>() : [];
    audios = json['audios'] != null ? json['audios'].cast<String>() : [];
    documents =
        json['documents'] != null ? json['documents'].cast<String>() : [];
    photos = json['photos'] != null ? json['photos'].cast<String>() : [];
    removed = json['removed'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    v = json['__v'];
  }
  String? id;
  String? bio;
  int? actingAgeFrom;
  int? actingAgeTo;
  UserProfile? user;
  List<TalentModel>? talents;
  List<EducationModel>? educations;
  MembershipModel? membership;
  List<Categories>? categories;
  List<SkillsModel>? specialSkills;
  List<SocialMedia>? socialMedia;
  String? gender;
  String? nationality;
  num? weight;
  num? height;
  String? skinColor;
  String? hairColor;
  String? eyeColor;
  String? portfolioLink;
  List<JobExperienceModel>? experiences;
  List<String>? shots;
  List<String>? audios;
  List<String>? documents;
  List<String>? photos;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  num? v;
  UsersProfileModel copyWith({
    String? id,
    String? bio,
    String? nationality,
    int? actingAgeFrom,
    int? actingAgeTo,
    UserProfile? user,
    List<TalentModel>? talents,
    MembershipModel? membership,
    List<SocialMedia>? socialMedia,
    List<Categories>? categories,
    List<SkillsModel>? specialSkills,
    List<String>? audios,
    List<String>? documents,
    List<String>? photos,
    String? gender,
    num? weight,
    num? height,
    String? skinColor,
    String? hairColor,
    String? eyeColor,
    String? portfolioLink,
    List<JobExperienceModel>? experiences,
    List<String>? shots,
    bool? removed,
    String? createdAt,
    String? updatedAt,
  }) =>
      UsersProfileModel(
        id: id ?? this.id,
        bio: bio ?? this.bio,
        nationality: nationality ?? this.nationality,
        actingAgeTo: actingAgeTo ?? this.actingAgeTo,
        actingAgeFrom: actingAgeFrom ?? this.actingAgeFrom,
        user: user ?? this.user,
        membership: membership ?? this.membership,
        talents: talents ?? this.talents,
        socialMedia: socialMedia ?? this.socialMedia,
        educations: educations ?? educations,
        categories: categories ?? this.categories,
        specialSkills: specialSkills ?? this.specialSkills,
        gender: gender ?? this.gender,
        weight: weight ?? this.weight,
        height: height ?? this.height,
        audios: audios ?? this.audios,
        skinColor: skinColor ?? this.skinColor,
        documents: documents ?? this.documents,
        photos: photos ?? this.photos,
        hairColor: hairColor ?? this.hairColor,
        eyeColor: eyeColor ?? this.eyeColor,
        portfolioLink: portfolioLink ?? this.portfolioLink,
        experiences: experiences ?? this.experiences,
        shots: shots ?? this.shots,
        removed: removed ?? this.removed,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
      );
  @override
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['_id'] = id;
    map['bio'] = bio;
    if (user != null) {
      map['user'] = user?.toJson();
    }
    if (talents != null) {
      map['talents'] = talents?.map((v) => v.toJson()).toList();
    }
    if (membership != null) {
      map['membership'] = membership?.toJson();
    }
    if (categories != null) {
      map['categories'] = categories?.map((v) => v.toJson()).toList();
    }
    if (educations != null) {
      map['educations'] = educations!.map((v) => v.toJson()).toList();
    }
    if (specialSkills != null) {
      map['specialSkills'] = specialSkills!.map((v) => v.toJson()).toList();
    }
    if (socialMedia != null) {
      map['socialLinks'] = socialMedia!.map((v) => v.toJson()).toList();
    }
    map['gender'] = gender;
    map['nationality'] = nationality;
    map['actingAgeTo'] = actingAgeTo;
    map['actingAgeFrom'] = actingAgeFrom;
    map['weight'] = weight;
    map['height'] = height;
    map['skinColor'] = skinColor;
    map['hairColor'] = hairColor;
    map['eyeColor'] = eyeColor;
    map['portfolioLink'] = portfolioLink;
    if (experiences != null) {
      map['experiences'] = experiences?.map((v) => v.toJson()).toList();
    }
    map['shots'] = shots;
    map['photos'] = photos;
    map['documents'] = documents;
    map['audios'] = audios;
    map['removed'] = removed;
    map['createdAt'] = createdAt;
    map['updatedAt'] = updatedAt;
    map['__v'] = v;
    return map;
  }
}

class Experiences {
  Experiences({
    this.jobTitle,
    this.roleDescription,
    this.workFrom,
    this.workTo,
  });

  Experiences.fromJson(dynamic json) {
    jobTitle = json['jobTitle'];
    roleDescription = json['roleDescription'];
    workFrom = json['workFrom'];
    workTo = json['workTo'];
  }
  String? jobTitle;
  String? roleDescription;
  String? workFrom;
  String? workTo;
  Experiences copyWith({
    String? jobTitle,
    String? roleDescription,
    String? workFrom,
    String? workTo,
  }) =>
      Experiences(
        jobTitle: jobTitle ?? this.jobTitle,
        roleDescription: roleDescription ?? this.roleDescription,
        workFrom: workFrom ?? this.workFrom,
        workTo: workTo ?? this.workTo,
      );
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['jobTitle'] = jobTitle;
    map['roleDescription'] = roleDescription;
    map['workFrom'] = workFrom;
    map['workTo'] = workTo;
    return map;
  }
}

class Categories extends BaseModel {
  Categories({
    this.id,
    this.color,
    this.image,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.featured,
    this.title,
    this.description,
  });
  @override
  Categories fromJson(Map<String, dynamic> json) {
    return Categories.fromJson(json);
  }

  Categories.fromJson(dynamic json) {
    id = json['_id'];
    color = json['color'];
    image = json['image'];
    removed = json['removed'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    v = json['__v'];
    featured = json['featured'];
    title = json['title'];
    description = json['description'];
  }
  String? id;
  String? color;
  String? image;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  num? v;
  bool? featured;
  String? title;
  String? description;
  Categories copyWith({
    String? id,
    String? color,
    String? image,
    bool? removed,
    String? createdAt,
    String? updatedAt,
    num? v,
    bool? featured,
    String? title,
    String? description,
  }) =>
      Categories(
        id: id ?? this.id,
        color: color ?? this.color,
        image: image ?? this.image,
        removed: removed ?? this.removed,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
        v: v ?? this.v,
        featured: featured ?? this.featured,
        title: title ?? this.title,
        description: description ?? this.description,
      );
  @override
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['_id'] = id;
    map['color'] = color;
    map['image'] = image;
    map['removed'] = removed;
    map['createdAt'] = createdAt;
    map['updatedAt'] = updatedAt;
    map['__v'] = v;
    map['featured'] = featured;
    map['title'] = title;
    map['description'] = description;
    return map;
  }
}

class Talents {
  Talents({
    this.id,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.title,
    this.description,
  });

  Talents.fromJson(dynamic json) {
    id = json['_id'];
    removed = json['removed'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    v = json['__v'];
    title = json['title'];
    description = json['description'];
  }
  String? id;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  num? v;
  String? title;
  String? description;
  Talents copyWith({
    String? id,
    bool? removed,
    String? createdAt,
    String? updatedAt,
    num? v,
    String? title,
    String? description,
  }) =>
      Talents(
        id: id ?? this.id,
        removed: removed ?? this.removed,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
        v: v ?? this.v,
        title: title ?? this.title,
        description: description ?? this.description,
      );
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['_id'] = id;
    map['removed'] = removed;
    map['createdAt'] = createdAt;
    map['updatedAt'] = updatedAt;
    map['__v'] = v;
    map['title'] = title;
    map['description'] = description;
    return map;
  }
}

class UserProfile {
  UserProfile({
    this.id,
    this.name,
    this.bio,
    this.gender,
    this.nationality,
    this.actingAgeFrom,
    this.actingAgeTo,
    this.package,
    this.email,
    this.phone,
    this.myInvitationCode,
    this.role,
    this.avatar,
    this.provider,
    this.googleId,
    this.facebookId,
    this.providerId,
    this.profileStatus,
    this.organization,
    this.verify,
    this.verifyPhone,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.confirmPhone,
    this.birthDate,
    this.invitationExpiryDate,
    this.privacy,
    this.terms,
    this.eula,
    this.firstSubscription,
    this.subscriptionEnd,
    this.invitation,
    this.countryCode,
    this.countryName,
    this.expert,
    this.profileCountry,
  });

  UserProfile.fromJson(dynamic json) {
    id = json['_id'];
    bio = json['bio'];
    name = json['name'];
    package =
        (json['package'] != null) ? Package.fromJson(json['package']) : null;
    email = json['email'];
    phone = json['phone'];
    myInvitationCode = json['myInvitationCode'];
    role = json['role'];
    avatar = json['avatar'];
    provider = json['provider'];
    facebookId = json['facebookId'];
    googleId = json['googleId'];
    providerId = json['providerId'];
    profileStatus = json['profileStatus'];
    organization = json['organization'];
    verify = json['verify'];
    verifyPhone = json['verifyPhone'];
    removed = json['removed'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    v = json['__v'];
    birthDate = json['birthDate'];
    invitationExpiryDate = json['invitationExpiryDate'];
    confirmPhone = json['confirmPhone'] != null
        ? ConfirmPhone.fromJson(json['confirmPhone'])
        : null;
    privacy = json['privacy'];
    terms = json['terms'];
    eula = json['eula'];
    firstSubscription = json['firstSubscription'];
    subscriptionEnd = json['subscriptionEnd'];
    invitation = json['invitation'];
    countryCode = json['countryCode'];
    countryName = json['countryName'];
    gender = json['gender'];
    actingAgeFrom = json['actingAgeFrom'];
    actingAgeTo = json['actingAgeTo'];
    nationality = json['nationality'];
    profileCountry = json['profileCountry'];
    package =
        json['package'] != null ? Package.fromJson(json['package']) : null;
  }
  String? id;
  String? name;
  String? bio;
  Package? package;
  String? email;
  String? phone;
  String? gender;
  int? actingAgeFrom;
  int? actingAgeTo;
  String? nationality;
  String? profileCountry;
  String? birthDate;
  String? myInvitationCode;
  String? role;
  String? avatar;
  String? provider;
  String? googleId;
  String? facebookId;
  dynamic providerId;
  String? profileStatus;
  dynamic organization;
  bool? verify;
  bool? verifyPhone;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  num? v;
  ConfirmPhone? confirmPhone;
  String? invitationExpiryDate;
  dynamic expert;
  bool? privacy;
  bool? terms;
  bool? eula;
  bool? firstSubscription;
  bool? subscriptionEnd;
  bool? invitation;
  String? countryCode;
  String? countryName;
  UserProfile copyWith({
    String? id,
    String? name,
    String? bio,
    Package? package,
    String? email,
    String? phone,
    String? gender,
    int? actingAgeFrom,
    int? actingAgeTo,
    String? nationality,
    String? profileCountry,
    String? myInvitationCode,
    String? role,
    String? avatar,
    String? provider,
    String? facebookId,
    String? googleId,
    dynamic providerId,
    String? profileStatus,
    dynamic organization,
    bool? verify,
    bool? verifyPhone,
    bool? removed,
    String? createdAt,
    String? updatedAt,
    num? v,
    ConfirmPhone? confirmPhone,
    String? birthDate,
    String? invitationExpiryDate,
    dynamic expert,
    bool? privacy,
    bool? terms,
    bool? eula,
    bool? firstSubscription,
    bool? subscriptionEnd,
    bool? invitation,
    String? countryCode,
    String? countryName,
  }) =>
      UserProfile(
        id: id ?? this.id,
        name: name ?? this.name,
        bio: bio ?? this.bio,
        gender: gender ?? this.gender,
        nationality: nationality ?? this.nationality,
        actingAgeTo: actingAgeTo ?? this.actingAgeTo,
        actingAgeFrom: actingAgeFrom ?? this.actingAgeFrom,
        profileCountry: profileCountry ?? this.profileCountry,
        package: package ?? this.package,
        email: email ?? this.email,
        phone: phone ?? this.phone,
        myInvitationCode: myInvitationCode ?? this.myInvitationCode,
        role: role ?? this.role,
        avatar: avatar ?? this.avatar,
        provider: provider ?? this.provider,
        facebookId: facebookId ?? this.facebookId,
        googleId: googleId ?? this.googleId,
        providerId: providerId ?? this.providerId,
        profileStatus: profileStatus ?? this.profileStatus,
        organization: organization ?? this.organization,
        verify: verify ?? this.verify,
        verifyPhone: verifyPhone ?? this.verifyPhone,
        removed: removed ?? this.removed,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
        v: v ?? this.v,
        confirmPhone: confirmPhone ?? this.confirmPhone,
        birthDate: birthDate ?? this.birthDate,
        invitationExpiryDate: invitationExpiryDate ?? this.invitationExpiryDate,
        expert: expert ?? this.expert,
        privacy: privacy ?? this.privacy,
        terms: terms ?? this.terms,
        eula: eula ?? this.eula,
        firstSubscription: firstSubscription ?? this.firstSubscription,
        subscriptionEnd: subscriptionEnd ?? this.subscriptionEnd,
        invitation: invitation ?? this.invitation,
        countryCode: countryCode ?? this.countryCode,
        countryName: countryName ?? this.countryName,
      );
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['_id'] = id;
    map['name'] = name;
    map['bio'] = bio;
    if (package != null) {
      map['package'] = package!.toJson();
    }
    map['email'] = email;
    map['phone'] = phone;
    map['myInvitationCode'] = myInvitationCode;
    map['role'] = role;
    map['avatar'] = avatar;
    map['provider'] = provider;
    map['googleId'] = googleId;
    map['facebookId'] = facebookId;
    map['providerId'] = providerId;
    map['profileStatus'] = profileStatus;
    map['organization'] = organization;
    map['verify'] = verify;
    map['verifyPhone'] = verifyPhone;
    map['removed'] = removed;
    map['createdAt'] = createdAt;
    map['updatedAt'] = updatedAt;
    map['__v'] = v;
    if (confirmPhone != null) {
      map['confirmPhone'] = confirmPhone?.toJson();
    }
    map['birthDate'] = birthDate;
    map['invitationExpiryDate'] = invitationExpiryDate;
    map['expert'] = expert;
    map['privacy'] = privacy;
    map['terms'] = terms;
    map['eula'] = eula;
    map['firstSubscription'] = firstSubscription;
    map['subscriptionEnd'] = subscriptionEnd;
    map['invitation'] = invitation;
    map['countryCode'] = countryCode;
    map['countryName'] = countryName;
    map['gender'] = gender;
    map['actingAgeFrom'] = actingAgeFrom;
    map['actingAgeTo'] = actingAgeTo;
    map['nationality'] = nationality;
    map['profileCountry'] = profileCountry;
    return map;
  }
}
