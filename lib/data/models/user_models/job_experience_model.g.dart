// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'job_experience_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

JobExperienceModel _$JobExperienceModelFromJson(Map<String, dynamic> json) =>
    JobExperienceModel(
      id: json['_id'] as String?,
      jobTitle: json['jobTitle'] as String?,
      roleDescription: json['roleDescription'] as String?,
      workFrom: json['workFrom'] == null
          ? null
          : DateTime.parse(json['workFrom'] as String),
      workTo: json['workTo'] == null
          ? null
          : DateTime.parse(json['workTo'] as String),
    );

Map<String, dynamic> _$JobExperienceModelToJson(JobExperienceModel instance) {
  final val = <String, dynamic>{};

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('_id', instance.id);
  writeNotNull('jobTitle', instance.jobTitle);
  writeNotNull('roleDescription', instance.roleDescription);
  writeNotNull('workFrom', instance.workFrom?.toIso8601String());
  writeNotNull('workTo', instance.workTo?.toIso8601String());
  return val;
}
