import 'package:json_annotation/json_annotation.dart';

import '../base_models/base_model.dart';

part 'membership_model.g.dart'; 

@JsonSerializable(nullable: true, ignoreUnannotated: false)
class  MembershipModel  extends BaseModel{
  @JsonKey(name: '_id')
  String? id;
  @JsonKey(name: 'removed')
  bool? removed;
  @JsonKey(name: 'createdAt')
  String? createdAt;
  @JsonKey(name: 'updatedAt')
  String? updatedAt;
  @JsonKey(name: '__v')
  int? iV;
  @JsonKey(name: 'title')
  String? title;

  MembershipModel({this.id, this.removed, this.createdAt, this.updatedAt, this.iV, this.title});

   factory MembershipModel.fromJson( json) => _$MembershipModelFromJson(json);
  @override

   Map<String, dynamic> toJson() => _$MembershipModelToJson(this);

  @override
  MembershipModel fromJson(Map<String, dynamic> json) => MembershipModel.fromJson(json);
}

