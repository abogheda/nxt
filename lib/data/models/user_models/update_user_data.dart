import '../base_models/base_model.dart';

class UpdateUserData extends BaseModel {
  UpdateUserData({
    this.expert,
    this.id,
    this.name,
    this.email,
    this.phone,
    this.password,
    this.role,
    this.avatar,
    this.provider,
    this.googleId,
    this.facebookId,
    this.providerId,
    this.profileStatus,
    this.organization,
    this.verify,
    this.verifyPhone,
    this.removed,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.confirmPhone,
    this.birthDate,
  });
  @override
  UpdateUserData fromJson(Map<String, dynamic> json) {
    return UpdateUserData.fromJson(json);
  }

  UpdateUserData.fromJson(dynamic json) {
    expert = json['expert'];
    id = json['_id'];
    name = json['name'];
    email = json['email'];
    phone = json['phone'];
    password = json['password'];
    role = json['role'];
    avatar = json['avatar'];
    provider = json['provider'];
    facebookId = json['facebookId'];
    googleId = json['googleId'];
    providerId = json['providerId'];
    profileStatus = json['profileStatus'];
    organization = json['organization'];
    verify = json['verify'];
    verifyPhone = json['verifyPhone'];
    removed = json['removed'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    v = json['__v'];
    confirmPhone = json['confirmPhone'] != null ? ConfirmPhone.fromJson(json['confirmPhone']) : null;
    birthDate = json['birthDate'];
  }
  dynamic expert;
  String? id;
  String? name;
  String? email;
  String? phone;
  String? password;
  String? role;
  String? avatar;
  String? provider;
  String? googleId;
  String? facebookId;
  dynamic providerId;
  String? profileStatus;
  dynamic organization;
  bool? verify;
  bool? verifyPhone;
  bool? removed;
  String? createdAt;
  String? updatedAt;
  num? v;
  ConfirmPhone? confirmPhone;
  String? birthDate;
  UpdateUserData copyWith({
    dynamic expert,
    String? id,
    String? name,
    String? email,
    String? phone,
    String? password,
    String? role,
    String? avatar,
    String? provider,
    String? facebookId,
    String? googleId,
    dynamic providerId,
    String? profileStatus,
    dynamic organization,
    bool? verify,
    bool? verifyPhone,
    bool? removed,
    String? createdAt,
    String? updatedAt,
    num? v,
    ConfirmPhone? confirmPhone,
    String? birthDate,
  }) =>
      UpdateUserData(
        expert: expert ?? this.expert,
        id: id ?? this.id,
        name: name ?? this.name,
        email: email ?? this.email,
        phone: phone ?? this.phone,
        password: password ?? this.password,
        role: role ?? this.role,
        avatar: avatar ?? this.avatar,
        provider: provider ?? this.provider,
        facebookId: facebookId ?? this.facebookId,
        googleId: googleId ?? this.googleId,
        providerId: providerId ?? this.providerId,
        profileStatus: profileStatus ?? this.profileStatus,
        organization: organization ?? this.organization,
        verify: verify ?? this.verify,
        verifyPhone: verifyPhone ?? this.verifyPhone,
        removed: removed ?? this.removed,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
        v: v ?? this.v,
        confirmPhone: confirmPhone ?? this.confirmPhone,
        birthDate: birthDate ?? this.birthDate,
      );
  @override
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['expert'] = expert;
    map['_id'] = id;
    map['name'] = name;
    map['email'] = email;
    map['phone'] = phone;
    map['password'] = password;
    map['role'] = role;
    map['avatar'] = avatar;
    map['provider'] = provider;
    map['facebookId'] = facebookId;
    map['googleId'] = googleId;
    map['providerId'] = providerId;
    map['profileStatus'] = profileStatus;
    map['organization'] = organization;
    map['verify'] = verify;
    map['verifyPhone'] = verifyPhone;
    map['removed'] = removed;
    map['createdAt'] = createdAt;
    map['updatedAt'] = updatedAt;
    map['__v'] = v;
    if (confirmPhone != null) {
      map['confirmPhone'] = confirmPhone?.toJson();
    }
    map['birthDate'] = birthDate;
    return map;
  }
}

class ConfirmPhone {
  ConfirmPhone({
    this.codesended,
    this.expiresAt,
  });

  ConfirmPhone.fromJson(dynamic json) {
    codesended = json['codesended'];
    expiresAt = json['expiresAt'];
  }
  dynamic codesended;
  String? expiresAt;
  ConfirmPhone copyWith({
    dynamic codesended,
    String? expiresAt,
  }) =>
      ConfirmPhone(
        codesended: codesended ?? this.codesended,
        expiresAt: expiresAt ?? this.expiresAt,
      );
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['codesended'] = codesended;
    map['expiresAt'] = expiresAt;
    return map;
  }
}
