import 'package:json_annotation/json_annotation.dart';

part 'job_experience_model.g.dart';

@JsonSerializable(includeIfNull: false,explicitToJson: true)
class JobExperienceModel {
  @JsonKey(name: '_id')
  final String? id;
  String? jobTitle;
  String? roleDescription;
  DateTime? workFrom;
  DateTime? workTo;

  JobExperienceModel({
    this.id,
    this.jobTitle,
    this.roleDescription,
    this.workFrom,
    this.workTo,
  });

  factory JobExperienceModel.fromJson(Map<String, dynamic> json) =>
      _$JobExperienceModelFromJson(json);

  Map<String, dynamic> toJson() => _$JobExperienceModelToJson(this);
}
