import 'package:json_annotation/json_annotation.dart'; 

part 'social_media.g.dart'; 

@JsonSerializable(nullable: true, ignoreUnannotated: false)
class SocialMedia {
  @JsonKey(name: 'name')
  String? name;
  @JsonKey(name: 'link')
  String? link;

  SocialMedia({this.name, this.link});

   factory SocialMedia.fromJson(Map<String, dynamic> json) => _$SocialMediaFromJson(json);

   Map<String, dynamic> toJson() => _$SocialMediaToJson(this);
}

