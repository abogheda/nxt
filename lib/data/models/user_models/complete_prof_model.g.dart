// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'complete_prof_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CompleteProfileModel _$CompleteProfileModelFromJson(
        Map<String, dynamic> json) =>
    CompleteProfileModel(
      talents:
          (json['talents'] as List<dynamic>?)?.map((e) => e as String).toList(),
      educations: (json['educations'] as List<dynamic>?)
          ?.map((e) => EducationModel.fromJson(e as Map<String, dynamic>))
          .toList(),
      membership: json['membership'] as String?,
      membershipDocument: json['membershipDocument'] as String?,
      socialLinks: (json['socialLinks'] as List<dynamic>?)
          ?.map((e) => SocialMedia.fromJson(e as Map<String, dynamic>))
          .toList(),
      categories: (json['categories'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      experiences: (json['experiences'] as List<dynamic>?)
          ?.map((e) => JobExperienceModel.fromJson(e as Map<String, dynamic>))
          .toList(),
      audios:
          (json['audios'] as List<dynamic>?)?.map((e) => e as String).toList(),
      documents: (json['documents'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      photos:
          (json['photos'] as List<dynamic>?)?.map((e) => e as String).toList(),
      eyeColor: json['eyeColor'] as String?,
      hairColor: json['hairColor'] as String?,
      height: (json['height'] as num?)?.toDouble(),
      portfolioLink: json['portfolioLink'] as String?,
      bio: json['bio'] as String?,
      nationality: json['nationality'] as String?,
      country: json['country'] as String?,
      skinColor: json['skinColor'] as String?,
      specialSkills: (json['specialSkills'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      gender: json['gender'] as String?,
      weight: (json['weight'] as num?)?.toDouble(),
      shots:
          (json['shots'] as List<dynamic>?)?.map((e) => e as String).toList(),
    );

Map<String, dynamic> _$CompleteProfileModelToJson(
    CompleteProfileModel instance) {
  final val = <String, dynamic>{};

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('talents', instance.talents);
  writeNotNull('categories', instance.categories);
  writeNotNull('shots', instance.shots);
  writeNotNull('photos', instance.photos);
  writeNotNull('audios', instance.audios);
  writeNotNull('documents', instance.documents);
  writeNotNull('specialSkills', instance.specialSkills);
  writeNotNull('gender', instance.gender);
  writeNotNull('bio', instance.bio);
  writeNotNull('height', instance.height);
  writeNotNull('weight', instance.weight);
  writeNotNull('skinColor', instance.skinColor);
  writeNotNull('hairColor', instance.hairColor);
  writeNotNull('eyeColor', instance.eyeColor);
  writeNotNull('portfolioLink', instance.portfolioLink);
  writeNotNull('nationality', instance.nationality);
  writeNotNull('country', instance.country);
  writeNotNull('membership', instance.membership);
  writeNotNull('membershipDocument', instance.membershipDocument);
  writeNotNull(
      'experiences', instance.experiences?.map((e) => e.toJson()).toList());
  writeNotNull(
      'educations', instance.educations?.map((e) => e.toJson()).toList());
  writeNotNull(
      'socialLinks', instance.socialLinks?.map((e) => e.toJson()).toList());
  return val;
}
