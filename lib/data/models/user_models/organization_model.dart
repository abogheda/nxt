// ignore_for_file: public_member_api_docs, sort_constructors_first
/*
  "organization": {
      "name": "test",
      "website": "test.com",
      "industry": "industry",
      "size": "5",
      "type": "male",
      "logo": "nxt"
    },
*/
import 'package:json_annotation/json_annotation.dart';

part 'organization_model.g.dart';

@JsonSerializable()
class Organization {
  String? name;
  String? website;
  String? industry;
  int? size;
  String? type;
  String? logo;

  Organization({
    this.name,
    this.website,
    this.industry,
    this.size,
    this.type,
    this.logo,
  });
  factory Organization.fromJson(Map<String, dynamic> json) => _$OrganizationFromJson(json);

  Map<String, dynamic> toJson() => _$OrganizationToJson(this);
}
