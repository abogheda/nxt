import 'package:json_annotation/json_annotation.dart';
import '../../enum/profile_status.dart';
import '../package/package_model.dart';
import '../base_models/base_model.dart';
import 'job_experience_model.dart';

part 'user_model.g.dart';

@JsonSerializable(includeIfNull: false)
class User extends BaseModel {
  @JsonKey(name: "_id")
  final String? id;
  final String? avatar;
  final String? mobileNumber;
  final String? birthDate;
  final DateTime? createdAt;
  final bool? isProfileComplete;
  final bool? verifiedMobile;
  final String? name;
  final Package? package;
  final String? email;
  final String? fullName;
  final String? firstName;
  final String? lastName;
  final List<String>? talents;
  final List<String>? specialSkills;
  final String? gender;
  final num? weight;
  final num? height;
  final String? skinColor;
  final String? hairColor;
  final String? eyeColor;
  final String? portfolioLink;
  final List<JobExperienceModel>? experiences;
  final List<String>? shots;
  ProfileStatus? profileStatus;

  User(
      {this.shots,
      this.avatar,
      this.name,
      this.package,
      this.email,
      this.experiences,
      this.talents,
      this.specialSkills,
      this.gender,
      this.weight,
      this.height,
      this.skinColor,
      this.hairColor,
      this.eyeColor,
      this.portfolioLink,
      this.id,
      this.mobileNumber,
      this.birthDate,
      this.createdAt,
      this.isProfileComplete,
      this.verifiedMobile,
      this.fullName,
      this.firstName,
      this.lastName,
      this.profileStatus});

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$UserToJson(this);

  @override
  User fromJson(Map<String, dynamic> json) {
    return User.fromJson(json);
  }
}
