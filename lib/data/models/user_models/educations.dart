import 'package:json_annotation/json_annotation.dart'; 

part 'educations.g.dart'; 

@JsonSerializable(nullable: true, ignoreUnannotated: false)
class Educations {
  @JsonKey(name: 'educationType')
  String? educationType;
  @JsonKey(name: 'courseName')
  String? courseName;
  @JsonKey(name: 'courseLocation')
  String? courseLocation;

  Educations({this.educationType, this.courseName, this.courseLocation});

   factory Educations.fromJson(Map<String, dynamic> json) => _$EducationsFromJson(json);

   Map<String, dynamic> toJson() => _$EducationsToJson(this);
}

