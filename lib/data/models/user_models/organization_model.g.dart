// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'organization_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Organization _$OrganizationFromJson(Map<String, dynamic> json) => Organization(
      name: json['name'] as String?,
      website: json['website'] as String?,
      industry: json['industry'] as String?,
      size: json['size'] as int?,
      type: json['type'] as String?,
      logo: json['logo'] as String?,
    );

Map<String, dynamic> _$OrganizationToJson(Organization instance) =>
    <String, dynamic>{
      'name': instance.name,
      'website': instance.website,
      'industry': instance.industry,
      'size': instance.size,
      'type': instance.type,
      'logo': instance.logo,
    };
