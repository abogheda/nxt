import 'package:json_annotation/json_annotation.dart';
import 'base_models/base_model.dart';
import 'user_models/lang_model.dart';

part 'talent_model.g.dart';

@JsonSerializable()
class TalentModel extends BaseModel {
  @JsonKey(name: '_id')
  final String? id;
  final String? title;


  TalentModel({
    this.id,
    this.title,
  });

  factory TalentModel.fromJson(Map<String, dynamic> json) => _$TalentModelFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$TalentModelToJson(this);

  @override
  TalentModel fromJson(Map<String, dynamic> json) => TalentModel.fromJson(json);
}
