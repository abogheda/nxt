import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:easy_localization/easy_localization.dart';

import '../../models/base_models/base_listed_res.dart';
import '../../models/base_models/base_single_res.dart';
import '../../models/choose_bundle/coupon_model.dart';
import '../../models/choose_bundle/plan_model.dart';
import '../imports.dart';

class BundleRepo {
  static Future<BaseListedResponse<PlanModel>> plansList({bool display = true}) async {
    final res = await api.get(path: '/plans?display=true',);
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseListedResponse<PlanModel>.fromJson(
        resp.data,
        PlanModel(),
      );
    } else {
      String msg = (res as Right).value;
      return BaseListedResponse(message: msg.tr(), succeeded: false);
    }
  }

  static Future<BaseSingleResponse<CouponModel>> getCoupon({required String planId,required String code}) async {
    final res = await api.get(path: "/referralCode/$code/$planId");
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseSingleResponse<CouponModel>.fromJson(resp.data, CouponModel());
    } else {
      String msg = (res as Right).value;
      return BaseSingleResponse(
        message: msg.tr(),
        succeeded: false,
      );
    }
  }

}
