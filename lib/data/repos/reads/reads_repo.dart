import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:easy_localization/easy_localization.dart';
import '../../models/reads/read_model.dart';
import '../../models/reads/reads_model.dart';

import '../../models/base_models/base_listed_res.dart';
import '../../models/base_models/base_single_res.dart';
import '../imports.dart';

class ReadsRepo {
  static Future<BaseListedResponse<ReadsModel>> getAllReads() async {
    final res = await api.get(path: "/reads");
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseListedResponse<ReadsModel>.fromJson(
        resp.data,
        ReadsModel(),
      );
    } else {
      String msg = (res as Right).value;
      return BaseListedResponse(
        message: msg.tr(),
        succeeded: false,
      );
    }
  }

  static Future<BaseSingleResponse<ReadModel>> getRead({required String reaId}) async {
    final res = await api.get(path: "/reads/$reaId");
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseSingleResponse<ReadModel>.fromJson(
        resp.data,
        ReadModel(),
      );
    } else {
      String msg = (res as Right).value;
      return BaseSingleResponse(
        message: msg.tr(),
        succeeded: false,
      );
    }
  }
}
