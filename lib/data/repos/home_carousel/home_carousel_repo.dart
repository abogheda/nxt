import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:easy_localization/easy_localization.dart';

import '../../models/base_models/base_listed_res.dart';
import '../../models/base_models/base_single_res.dart';
import '../../models/home/home_carousel_model.dart';
import '../../models/home/home_carousels_model.dart';
import '../imports.dart';

class HomeCarouselRepo {
  static Future<BaseListedResponse<HomeCarouselsModel>> getHomeCarouselsData() async {
    final res = await api.get(path: "/HomeCarousels");
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseListedResponse<HomeCarouselsModel>.fromJson(
        resp.data,
        HomeCarouselsModel(),
      );
    } else {
      String msg = (res as Right).value;
      return BaseListedResponse(
        message: msg.tr(),
        succeeded: false,
      );
    }
  }

  static Future<BaseSingleResponse<HomeCarouselModel>> getHomeCarouselById({required String id}) async {
    final res = await api.get(path: "/HomeCarousels/$id");
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseSingleResponse<HomeCarouselModel>.fromJson(resp.data, HomeCarouselModel());
    } else {
      String msg = (res as Right).value;
      return BaseSingleResponse(
        message: msg.tr(),
        succeeded: false,
      );
    }
  }
}
