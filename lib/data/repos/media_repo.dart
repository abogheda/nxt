import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import '../models/base_models/base_single_res.dart';
import '../../utils/helpers/popup_helper.dart';

import '../models/profile_reel_model.dart';
import '../models/base_models/base_listed_res.dart';
import '../models/reel_model.dart';

import '../models/challenge/challenge_status.dart';
import 'imports.dart';

class MediaRepo {
  static Future<BaseListedResponse<ReelModel>> uploadReels({required String url}) async {
    final res = await api.post(path: "/reels", body: {"media": url});
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseListedResponse<ReelModel>.fromJson(resp.data[0], ReelModel());
    } else {
      String msg = (res as Right).value;
      return BaseListedResponse(
        message: msg.tr(),
        succeeded: false,
      );
    }
  }

  static Future<ChallengeStatus?> postChallengeMedia({required String media, required String challengeId}) async {
    final res = await api.post(
      path: "/user-challenges",
      body: {"media": media, "challenge": challengeId},
    );
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      final challengeStatus = ChallengeStatus.fromJson(resp.data);
      if (resp.statusCode == 200 || resp.statusCode == 201) {
        return challengeStatus;
      } else {
        return challengeStatus;
      }
    } else {
      String msg = (res as Right).value;
      PopupHelper.showBasicSnack(msg: msg, color: Colors.red);
    }
    return null;
  }

  static Future<ChallengeStatus?> patchChallengeMedia({required String media, required String challengeId}) async {
    final res = await api.patch(
      path: "/user-challenges/resubmit",
      mapBody: {"media": media, "challenge": challengeId},
    );
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      final challengeStatus = ChallengeStatus.fromJson(resp.data);
      if (resp.statusCode == 200 || resp.statusCode == 201) {
        return challengeStatus;
      } else {
        return challengeStatus;
      }
    } else {
      String msg = (res as Right).value;
      PopupHelper.showBasicSnack(msg: msg, color: Colors.red);
    }
    return null;
  }

  static Future<BaseListedResponse<ProfileReelModel>> getAllUserReels() async {
    final res = await api.get(path: "/reels");
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseListedResponse<ProfileReelModel>.fromJson(
        resp.data,
        ProfileReelModel(),
      );
    } else {
      String msg = (res as Right).value;
      return BaseListedResponse(
        message: msg.tr(),
        succeeded: false,
      );
    }
  }

  static Future<BaseSingleResponse<ProfileReelModel>> deleteProfileReel({required String reelId}) async {
    final res = await api.delete(path: "/reels/$reelId");
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseSingleResponse<ProfileReelModel>.fromJson(
        resp.data,
        ProfileReelModel(),
      );
    } else {
      String msg = (res as Right).value;
      return BaseSingleResponse(
        message: msg.tr(),
        succeeded: false,
      );
    }
  }
}
