import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:easy_localization/easy_localization.dart';
import '../../models/app_info/app_info_model.dart';
import '../../models/base_models/base_single_res.dart';
import '../imports.dart';

class AppInfoRepo {
  static Future<BaseSingleResponse<AppInfoModel>> getAppInfo() async {
    final res = await api.get(path: '/app-info');
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseSingleResponse<AppInfoModel>.fromJson(resp.data, AppInfoModel());
    } else {
      String msg = (res as Right).value;
      return BaseSingleResponse(
        message: msg.tr(),
        succeeded: false,
      );
    }
  }
}
