import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:easy_localization/easy_localization.dart';
import '../../models/base_models/base_listed_res.dart';
import '../../../utils/constants/app_const.dart';
import '../../models/base_models/base_single_res.dart';
import '../../models/jobs/my_jobs_model.dart';
import '../../models/more/delete_user_account_model.dart';
import '../../models/more/legal_model.dart';
import '../../models/more/plan_model.dart';
import '../imports.dart';

class MoreRepo {
  static Future<BaseSingleResponse<DeleteUserAccountModel>> deleteUserAccount() async {
    final res = await api.get(path: '/auth/delete-me');
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseSingleResponse<DeleteUserAccountModel>.fromJson(
        resp.data,
        DeleteUserAccountModel(),
      );
    } else {
      String msg = (res as Right).value;
      return BaseSingleResponse(
        message: msg.tr(),
        succeeded: false,
      );
    }
  }

  static Future<BaseSingleResponse<LegalModel>> getLegal() async {
    final res = await api.get(path: "/dynamic-content/${AppConst.isEn ? 'en' : 'ar'}");
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseSingleResponse<LegalModel>.fromJson(
        resp.data,
        LegalModel(),
      );
    } else {
      String msg = (res as Right).value;
      return BaseSingleResponse(
        message: msg.tr(),
        succeeded: false,
      );
    }
  }

  static Future<BaseSingleResponse<PlanModel>> getCurrentPlan() async {
    final res = await api.get(path: "/auth/validate");
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseSingleResponse<PlanModel>.fromJson(
        resp.data,
        PlanModel(),
      );
    } else {
      String msg = (res as Right).value;
      return BaseSingleResponse(
        message: msg.tr(),
        succeeded: false,
      );
    }
  }

  static Future<BaseListedResponse<MyJobsModel>> getMyJobs() async {
    final res = await api.get(path: "/jobsApplication/user");
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseListedResponse<MyJobsModel>.fromJson(
        resp.data,
        MyJobsModel(),
      );
    } else {
      String msg = (res as Right).value;
      return BaseListedResponse(
        message: msg.tr(),
        succeeded: false,
      );
    }
  }
}
