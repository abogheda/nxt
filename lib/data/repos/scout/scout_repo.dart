import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:easy_localization/easy_localization.dart';

import '../../models/base_models/base_listed_res.dart';
import '../../models/scout/results_model.dart';
import '../../models/scout/search_results_model.dart';
import '../imports.dart';

class ScoutRepo {
  static Future<BaseListedResponse<ResultsModel>> getUserResultsByFilters(
      {required Map<String, dynamic> filtrationMap}) async {
    final res = await api.get(path: "/users/all", query: filtrationMap);
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseListedResponse<ResultsModel>.fromJson(resp.data, ResultsModel());
    } else {
      String msg = (res as Right).value;
      return BaseListedResponse(message: msg.tr(), succeeded: false);
    }
  }

  static Future<BaseListedResponse<SearchResultsModel>> searchForUsers({required String name}) async {
    final res = await api.get(path: "/auth/all-users", query: {'name': name});
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseListedResponse<SearchResultsModel>.fromJson(resp.data, SearchResultsModel());
    } else {
      String msg = (res as Right).value;
      return BaseListedResponse(message: msg.tr(), succeeded: false);
    }
  }

  // static Future<BaseListedResponse<SearchJobAndChallengesModel>> searchForJobsAndChallenges(
  //     {required String word}) async {
  //   final res = await api.get(path: "/categories/scout", query: {'word': word});
  //   if (res.isLeft()) {
  //     final Response resp = (res as Left).value;
  //     return BaseListedResponse<SearchJobAndChallengesModel>.fromJson(resp.data, SearchJobAndChallengesModel());
  //   } else {
  //     String msg = (res as Right).value;
  //     return BaseListedResponse(message: msg.tr(), succeeded: false);
  //   }
  // }
}
