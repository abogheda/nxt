import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:easy_localization/easy_localization.dart';
import '../../models/sign_up/confirm_terms_model.dart';
import '../../models/sign_up/create_user_model.dart';

import '../../../utils/constants/app_const.dart';
import '../../models/base_models/base_single_res.dart';
import '../../models/forgot_password/confirm_phone_model.dart';
import '../../models/forgot_password/reset_pass_model.dart';
import '../../models/forgot_password/verify_phone_model.dart';
import '../../models/sign_up/change_number_model.dart';
import '../../models/sign_up/confirm_privacy_model.dart';
import '../../models/sign_up/request_invitation_model.dart';
import '../../models/sign_up/send_code_response_model.dart';
import '../../models/sign_up/sign_up_model.dart';
import '../../models/sign_up/use_invitation_model.dart';
import '../../models/sign_up/verify_code_model.dart';
import '../../models/user_model.dart';
import '../imports.dart';

class AuthRepo {
  static Future<BaseSingleResponse<User>> sendFirebaseToken({required String firebaseToken}) async {
    final res = await api.patch(path: '/user-session/$firebaseToken');
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseSingleResponse<User>.fromJson(resp.data, User());
    } else {
      String msg = (res as Right).value;
      return BaseSingleResponse(message: msg.tr(), succeeded: false);
    }
  }

  static Future<BaseSingleResponse<ConfirmPhoneModel>> confirmPhone({required String phone}) async {
    final res = await api.post(
      path: EndPoints.confirmPhone,
      body: {'phone': phone},
    );
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseSingleResponse<ConfirmPhoneModel>.fromJson(
        resp.data,
        ConfirmPhoneModel(),
      );
    } else {
      String msg = (res as Right).value;
      return BaseSingleResponse(
        message: msg.tr(),
        succeeded: false,
      );
    }
  }

  static Future<BaseSingleResponse<ChangeNumberModel>> changeNumber({required String phone}) async {
    final res = await api.post(
      path: '/auth/rest-phone',
      body: {'phone': phone},
    );
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseSingleResponse<ChangeNumberModel>.fromJson(
        resp.data,
        ChangeNumberModel(),
      );
    } else {
      String msg = (res as Right).value;
      return BaseSingleResponse(message: msg.tr(), succeeded: false);
    }
  }

  static Future<BaseSingleResponse<VerifyPhoneModel>> verifyPhoneWithCodeAndNumber(
      {required String text, required String phone}) async {
    final res = await api.post(
      path: "/auth/verify-phone",
      body: {'verifyCode': int.parse(text), 'phone': phone},
    );
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseSingleResponse<VerifyPhoneModel>.fromJson(
        resp.data,
        VerifyPhoneModel(),
      );
    } else {
      String msg = (res as Right).value;
      return BaseSingleResponse(
        message: msg.tr(),
        succeeded: false,
      );
    }
  }

  static Future<BaseSingleResponse<VerifyCodeModel>> verifyPhoneWithCode({required int code}) async {
    final res = await api.post(
      path: "/auth/verify",
      body: {'verifyCode': code},
    );
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseSingleResponse<VerifyCodeModel>.fromJson(
        resp.data,
        VerifyCodeModel(),
      );
    } else {
      String msg = (res as Right).value;
      return BaseSingleResponse(
        message: msg.tr(),
        succeeded: false,
      );
    }
  }

  static Future<BaseSingleResponse<ResetPassModel>> resetPass({required String password, required String phone}) async {
    final res = await api.post(
      path: EndPoints.restPass,
      body: {
        'password': password,
        'phone': phone,
      },
    );
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseSingleResponse<ResetPassModel>.fromJson(
        resp.data,
        ResetPassModel(),
      );
    } else {
      String msg = (res as Right).value;
      return BaseSingleResponse(
        message: msg.tr(),
        succeeded: false,
      );
    }
  }

  static Future<BaseSingleResponse<UserModel>> userLogin({required String password, required String phone}) async {
    final res = await api.post(
      path: EndPoints.login,
      body: {'phone': phone, 'password': password},
    );
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseSingleResponse<UserModel>.fromJson(
        resp.data,
        UserModel(),
      );
    } else {
      String msg = (res as Right).value;
      return BaseSingleResponse(
        message: msg.tr(),
        succeeded: false,
      );
    }
  }

  static Future<BaseSingleResponse<SignUpModel>> talentSignUp({required CreateUserModel createUserModel}) async {
    final res = await api.post(path: '/auth/signup', body: createUserModel.toJson());
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseSingleResponse<SignUpModel>.fromJson(resp.data, SignUpModel());
    } else {
      String msg = (res as Right).value;
      return BaseSingleResponse(message: msg.tr(), succeeded: false);
    }
  }

  static Future<BaseSingleResponse<SignUpModel>> expertSignUp({required CreateUserModel createUserModel}) async {
    final res = await api.post(path: '/expert/create', body: createUserModel.toJson());
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseSingleResponse<SignUpModel>.fromJson(resp.data, SignUpModel());
    } else {
      String msg = (res as Right).value;
      return BaseSingleResponse(message: msg.tr(), succeeded: false);
    }
  }

  static Future<BaseSingleResponse<User>> postExpertInfo({required CreateUserModel createUserModel}) async {
    final res = await api.post(path: '/expert/expertInfo', body: createUserModel.toJson());
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseSingleResponse<User>.fromJson(resp.data, User());
    } else {
      String msg = (res as Right).value;
      return BaseSingleResponse(message: msg.tr(), succeeded: false);
    }
  }

  static Future<BaseSingleResponse<User>> patchExpertInfo({required CreateUserModel createUserModel}) async {
    final res = await api.patch(path: '/expert', body: createUserModel.toJson());
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseSingleResponse<User>.fromJson(resp.data, User());
    } else {
      String msg = (res as Right).value;
      return BaseSingleResponse(message: msg.tr(), succeeded: false);
    }
  }

  static Future<BaseSingleResponse<SignUpModel>> organizationSignUp({required CreateUserModel createUserModel}) async {
    final res = await api.post(path: '/organization-types/create', body: createUserModel.toJson());
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseSingleResponse<SignUpModel>.fromJson(resp.data, SignUpModel());
    } else {
      String msg = (res as Right).value;
      return BaseSingleResponse(message: msg.tr(), succeeded: false);
    }
  }

  static Future<BaseSingleResponse<SendCodeResponseModel>> sendCode() async {
    final res = await api.get(path: '/auth/sendCode');
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseSingleResponse<SendCodeResponseModel>.fromJson(resp.data, SendCodeResponseModel());
    } else {
      String msg = (res as Right).value;
      return BaseSingleResponse(message: msg.tr(), succeeded: false);
    }
  }

  static Future<BaseSingleResponse<ConfirmTermsModel>> confirmTerms() async {
    final res = await api.get(path: '/auth/legal?legal=paymentTerms');
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseSingleResponse<ConfirmTermsModel>.fromJson(
        resp.data,
        ConfirmTermsModel(),
      );
    } else {
      String msg = (res as Right).value;
      return BaseSingleResponse(
        message: msg.tr(),
        succeeded: false,
      );
    }
  }

  static Future<BaseSingleResponse<UseInvitationModel>> useInvitation({required String invitationCode}) async {
    final res = await api.get(path: '/invitation/use/$invitationCode');
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseSingleResponse<UseInvitationModel>.fromJson(
        resp.data,
        UseInvitationModel(),
      );
    } else {
      String msg = (res as Right).value;
      return BaseSingleResponse(
        message: msg.tr(),
        succeeded: false,
      );
    }
  }

  static Future<BaseSingleResponse<RequestInvitationModel>> requestNewInvitation() async {
    final res = await api.get(path: '/invitation/request');
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseSingleResponse<RequestInvitationModel>.fromJson(
        resp.data,
        RequestInvitationModel(),
      );
    } else {
      String msg = (res as Right).value;
      return BaseSingleResponse(
        message: msg.tr(),
        succeeded: false,
      );
    }
  }

  static Future<BaseSingleResponse<ConfirmPrivacyModel>> confirmPrivacy() async {
    final res = await api.get(path: '/auth/legal?legal=privacy');
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseSingleResponse<ConfirmPrivacyModel>.fromJson(
        resp.data,
        ConfirmPrivacyModel(),
      );
    } else {
      String msg = (res as Right).value;
      return BaseSingleResponse(
        message: msg.tr(),
        succeeded: false,
      );
    }
  }

  static Future<BaseSingleResponse<ConfirmPrivacyModel>> confirmEULA() async {
    final res = await api.get(path: '/auth/legal?legal=eula');
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseSingleResponse<ConfirmPrivacyModel>.fromJson(
        resp.data,
        ConfirmPrivacyModel(),
      );
    } else {
      String msg = (res as Right).value;
      return BaseSingleResponse(
        message: msg.tr(),
        succeeded: false,
      );
    }
  }

  static Future<BaseSingleResponse<UserModel>> googleSignIn({required String token}) async {
    final res = await api.get(path: '/auth/google-signin/$token');
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseSingleResponse<UserModel>.fromJson(
        resp.data,
        UserModel(),
      );
    } else {
      String msg = (res as Right).value;
      return BaseSingleResponse(
        message: msg.tr(),
        succeeded: false,
      );
    }
  }



  static Future<BaseSingleResponse<UserModel>> facebookSignIn({required String token}) async {
    final res = await api.get(
      path: '/auth/facebook-signin/$token',
    );
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseSingleResponse<UserModel>.fromJson(
        resp.data,
        UserModel(),
      );
    } else {
      String msg = (res as Right).value;
      return BaseSingleResponse(
        message: msg.tr(),
        succeeded: false,
      );
    }
  }
}
