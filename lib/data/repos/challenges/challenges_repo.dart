import 'dart:developer';

import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:easy_localization/easy_localization.dart';
import '../../models/challenge/joined_challenge.dart';
import '../../models/challenge/view_submission_model.dart' as view_submission;
import '../../models/challenge/won_challenge_model.dart';
import '../../../view/screens/challenges/screens/view_challenge/view_challenge_model.dart';

import '../../models/base_models/base_listed_res.dart';
import '../../models/base_models/base_single_res.dart';
import '../../models/challenge/vote_user_challenge.dart';
import '../../models/challenge/challenge.dart';
import '../imports.dart';

class ChallengeRepo {
  static Future<BaseListedResponse<Challenge>> getChallenges(String? filterID) async {
    final res = await api.get(
      path: filterID == null ? "/challenges/country" : "/challenges/country?category=$filterID",
    );
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseListedResponse<Challenge>.fromJson(
        resp.data,
        Challenge(),
      );
    } else {
      String msg = (res as Right).value;
      return BaseListedResponse(
        message: msg.tr(),
        succeeded: false,
      );
    }
  }

  static Future<BaseListedResponse<JoinedChallengeAll>> getJoinedChallenges(String? filterID) async {
    final res = await api.get(
      path: filterID == null ? "/user-challenges/userjoined" : "/user-challenges/userjoined?category=$filterID",
    );
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseListedResponse<JoinedChallengeAll>.fromJson(
        resp.data,
        JoinedChallengeAll(),
      );
    } else {
      String msg = (res as Right).value;
      return BaseListedResponse(
        message: msg.tr(),
        succeeded: false,
      );
    }
  }

  static Future<BaseListedResponse<WonChallengeModel>> getWonChallenges(String? filterID) async {
    final res = await api.get(
      path: filterID == null ? "/challenges/winner" : "/challenges/winner?category=$filterID",
    );
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseListedResponse<WonChallengeModel>.fromJson(
        resp.data,
        WonChallengeModel(),
      );
    } else {
      String msg = (res as Right).value;
      return BaseListedResponse(
        message: msg.tr(),
        succeeded: false,
      );
    }
  }

  static Future<BaseSingleResponse<ViewChallengeModel>> getUserChallengeForUser(String? challengeId) async {
    final res = await api.get(path: "/challenges/info/$challengeId");
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      log("res : $resp");
      return BaseSingleResponse<ViewChallengeModel>.fromJson(
        resp.data,
        ViewChallengeModel(),
      );
    } else {
      String msg = (res as Right).value;
      return BaseSingleResponse(
        message: msg.tr(),
        succeeded: false,
      );
    }
  }

  static Future<BaseSingleResponse<view_submission.ViewSubmissionModel>> getSubmission(String? challengeId) async {
    final res = await api.get(path: "/user-challenges/user/$challengeId");
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseSingleResponse<view_submission.ViewSubmissionModel>.fromJson(
        resp.data,
        view_submission.ViewSubmissionModel(),
      );
    } else {
      String msg = (res as Right).value;
      return BaseSingleResponse(
        message: msg.tr(),
        succeeded: false,
      );
    }
  }

  static Future<BaseListedResponse<VoteUserChallenge>> getVotesForChallenge(String? challengeId) async {
    final res = await api.get(path: "/user-challenges/all/$challengeId");
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseListedResponse<VoteUserChallenge>.fromJson(
        resp.data,
        VoteUserChallenge(),
      );
    } else {
      String msg = (res as Right).value;
      return BaseListedResponse(
        message: msg.tr(),
        succeeded: false,
      );
    }
  }
}
