import 'dart:io';

import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:mime/mime.dart';
import 'package:path/path.dart';

import 'imports.dart';

class GeneralRepo {
  static Future<String?> _getUploadLink({
    required String fileName,
    String? folder,
  }) async {
    final res = await api.get(path: "/upload", query: {
      "filename": fileName,
      "folder": folder ?? "user",
    });
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      if (resp.statusCode == 200 || resp.data is String) {
        return resp.data;
      }
      return null;
    } else {
      return null;
    }
  }

  static Future<Either<List<String>, String>> uploadMedia(
    List<File> files, {
    void Function(int fileNum, int progress, int total)? onProgress,
  }) async {
    final List<String?> uploadingUrls = [];
    final List<String> mediaUrls = [];
    uploadingUrls.addAll(await Future.wait(files.map((f) => _getUploadLink(fileName: basename(f.path)))));
    if (uploadingUrls.any((element) => element == null)) {
      return Right('normalErrorMsg'.tr());
    }

    final Dio dio = Dio();
    for (var i = 0; i < files.length; i++) {
      final res = await dio.put(
        uploadingUrls[i]!,
        options: Options(
          contentType: lookupMimeType(files[i].path),
          headers: {"Content-Length": files[i].lengthSync()},
        ),
        data: files[i].openRead(),
        onSendProgress: (count, total) {
          onProgress?.call(i, count, total);
        },
      );
      if (res.statusCode == 200) {
        mediaUrls.add(uploadingUrls[i]!.split("?").first);
      } else {
        dio.close();
        return Right('normalErrorMsg'.tr());
      }
    }
    // dio.close();
    return Left(mediaUrls);
  }
}
