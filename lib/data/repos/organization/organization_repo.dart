import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:easy_localization/easy_localization.dart';

import '../../models/base_models/base_listed_res.dart';
import '../../models/base_models/base_single_res.dart';
import '../../models/organization/create_work_model.dart';
import '../../models/organization/organization_profile_model.dart';
import '../../models/organization/work_model.dart';
import '../imports.dart';

class OrganizationRepo {
  static Future<BaseSingleResponse<OrganizationProfileModel>> getOrganizationDataValidate() async {
    final res = await api.get(path: '/auth/validate');
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseSingleResponse<OrganizationProfileModel>.fromJson(resp.data, OrganizationProfileModel());
    } else {
      String msg = (res as Right).value;
      return BaseSingleResponse(message: msg.tr(), succeeded: false);
    }
  }

  static Future<BaseSingleResponse<WorkModel>> createNewWork({required CreateWorkModel createWorkModel}) async {
    final res = await api.post(path: "/works", body: createWorkModel.toJson());
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseSingleResponse<WorkModel>.fromJson(resp.data, WorkModel());
    } else {
      String msg = (res as Right).value;
      return BaseSingleResponse(
        message: msg.tr(),
        succeeded: false,
      );
    }
  }

  static Future<BaseSingleResponse<WorkModel>> editNewWork(
      {required CreateWorkModel createWorkModel, required String workId}) async {
    final res = await api.patch(path: "/works/$workId", body: createWorkModel.toJson());
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseSingleResponse<WorkModel>.fromJson(resp.data, WorkModel());
    } else {
      String msg = (res as Right).value;
      return BaseSingleResponse(
        message: msg.tr(),
        succeeded: false,
      );
    }
  }

  static Future<BaseListedResponse<WorkModel>> getAllWorks() async {
    final res = await api.get(path: "/works", language: 'ALL');
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseListedResponse<WorkModel>.fromJson(resp.data, WorkModel());
    } else {
      String msg = (res as Right).value;
      return BaseListedResponse(
        message: msg.tr(),
        succeeded: false,
      );
    }
  }

  static Future<BaseSingleResponse<WorkModel>> deleteWork({required String id}) async {
    final res = await api.delete(path: "/works/$id");
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseSingleResponse<WorkModel>.fromJson(resp.data, WorkModel());
    } else {
      String msg = (res as Right).value;
      return BaseSingleResponse(message: msg.tr(), succeeded: false);
    }
  }
}
