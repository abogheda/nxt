import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:easy_localization/easy_localization.dart';

import '../../models/base_models/base_single_res.dart';
import '../../models/expert/expert_profile_model.dart';
import '../imports.dart';

class ExpertRepo {
  static Future<BaseSingleResponse<ExpertProfileModel>> getExpertDataValidate() async {
    final res = await api.get(path: '/auth/validate');
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseSingleResponse<ExpertProfileModel>.fromJson(resp.data, ExpertProfileModel());
    } else {
      String msg = (res as Right).value;
      return BaseSingleResponse(message: msg.tr(), succeeded: false);
    }
  }
}
