import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:easy_localization/easy_localization.dart';
import '../../models/base_models/base_single_res.dart';
import '../../models/lectures/lectures.dart';

import '../../models/base_models/base_listed_res.dart';
import '../../models/classes/master_class.dart';
import '../../models/classes/watched_lecture_model.dart';
import '../imports.dart';

class ClassesRepo {
  static Future<BaseListedResponse<MasterClass>> getClasses(String? filterID) async {
    final res = await api.get(
      path: filterID == null ? "/nxt-classes" : "/nxt-classes?category=$filterID",
    );
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseListedResponse<MasterClass>.fromJson(
        resp.data,
        MasterClass(),
      );
    } else {
      String msg = (res as Right).value;
      return BaseListedResponse(
        message: msg.tr(),
        succeeded: false,
      );
    }
  }

  static Future<BaseSingleResponse<MasterClass>> getClassById({required String id}) async {
    final res = await api.get(
      path: '/nxt-classes/$id',
    );
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseSingleResponse<MasterClass>.fromJson(
        resp.data,
        MasterClass(),
      );
    } else {
      String msg = (res as Right).value;
      return BaseSingleResponse(
        message: msg.tr(),
        succeeded: false,
      );
    }
  }

  static Future<BaseListedResponse<Lectures>> getLectureById({required String id}) async {
    final res = await api.get(
      path: "/lectures/class/$id",
    );
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseListedResponse<Lectures>.fromJson(
        resp.data,
        Lectures(),
      );
    } else {
      String msg = (res as Right).value;
      return BaseListedResponse(
        message: msg.tr(),
        succeeded: false,
      );
    }
  }

  static Future<BaseSingleResponse<WatchedLectureModel>> markLectureWatched(
      {required String lectureId, required String classId}) async {
    final res = await api.post(path: "/lectures/watched", body: {
      "lecture": lectureId,
      "nxtClass": classId,
    });
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseSingleResponse<WatchedLectureModel>.fromJson(
        resp.data,
        WatchedLectureModel(),
      );
    } else {
      String msg = (res as Right).value;
      return BaseSingleResponse(
        message: msg.tr(),
        succeeded: false,
      );
    }
  }
}
