
import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:easy_localization/easy_localization.dart';

import '../../models/base_models/base_listed_res.dart';
import '../../models/base_models/base_single_res.dart';
import '../../models/choose_payment_models/create_order.dart';
import '../../models/choose_payment_models/payment_method.dart';
import '../imports.dart';

class PaymentRepo{
  static Future<BaseListedResponse<PaymentMethod>> paymentMethodList() async {
    final res = await api.get(path: '/payments/payment-methods',);
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseListedResponse<PaymentMethod>.fromJson(
        resp.data,
        PaymentMethod(),
      );
    } else {
      String msg = (res as Right).value;
      return BaseListedResponse(message: msg.tr(), succeeded: false);
    }
  }


  static Future<BaseSingleResponse<CreateOrderModel>> createOrder({required String planId,
    String code = '',
    String challengeId = '',
    String paymentMethod = '',String phoneNumber = '',}) async {
    final res = await api.get(path: "/payments/create-order/query?planId=$planId&challengeId=$challengeId&paymentMethod=$paymentMethod&mobileNumber=$phoneNumber");
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseSingleResponse<CreateOrderModel>.fromJson(resp.data["data"], CreateOrderModel());
    } else {
      String msg = (res as Right).value;
      return BaseSingleResponse(
        message: msg,
        succeeded: false,
      );
    }
  }
}