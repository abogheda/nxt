part of '../imports.dart';

class SaveRepo {
  static Future<BaseSingleResponse<SavedChallenges>> getSavedChallenges() async {
    final res = await api.get(path: "/saved-items?item=challenges");
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseSingleResponse<SavedChallenges>.fromJson(
        resp.data,
        SavedChallenges(),
      );
    } else {
      String msg = (res as Right).value;
      return BaseSingleResponse(
        message: msg.tr(),
        succeeded: false,
      );
    }
  }

  static Future<BaseSingleResponse<SavedNxtClasses>> getSavedClasses() async {
    final res = await api.get(path: "/saved-items?item=nxtclasses");
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseSingleResponse<SavedNxtClasses>.fromJson(
        resp.data,
        SavedNxtClasses(),
      );
    } else {
      String msg = (res as Right).value;
      return BaseSingleResponse(
        message: msg.tr(),
        succeeded: false,
      );
    }
  }

  static Future<BaseSingleResponse<SavedJobs>> getSavedJobs() async {
    final res = await api.get(path: "/saved-items?item=jobs");
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseSingleResponse<SavedJobs>.fromJson(
        resp.data,
        SavedJobs(),
      );
    } else {
      String msg = (res as Right).value;
      return BaseSingleResponse(
        message: msg.tr(),
        succeeded: false,
      );
    }
  }

  static Future<BaseSingleResponse<SavedReads>> getSavedReads() async {
    final res = await api.get(path: "/saved-items?item=reads");
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseSingleResponse<SavedReads>.fromJson(
        resp.data,
        SavedReads(),
      );
    } else {
      String msg = (res as Right).value;
      return BaseSingleResponse(
        message: msg.tr(),
        succeeded: false,
      );
    }
  }

  static Future<BaseSingleResponse<SaveModel>> addToSavedItems(SaveModel model) async {
    final res = await api.post(path: "/saved-items", body: {
      "item": model.item,
      "type": model.type?.save,
    });
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseSingleResponse<SaveModel>.fromJson(
        resp.data,
        SaveModel(),
      );
    } else {
      String msg = (res as Right).value;
      return BaseSingleResponse(
        message: msg.tr(),
        succeeded: false,
      );
    }
  }

  static Future<BaseSingleResponse<SaveModel>> deleteFromSavedItems(SaveModel model) async {
    final res = await api.delete(path: "/saved-items", body: {
      "item": model.item,
      "type": model.type?.save,
    });
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseSingleResponse<SaveModel>.fromJson(
        resp.data,
        SaveModel(),
      );
    } else {
      String msg = (res as Right).value;
      return BaseSingleResponse(
        message: msg.tr(),
        succeeded: false,
      );
    }
  }
}
