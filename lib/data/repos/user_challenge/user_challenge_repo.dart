import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:easy_localization/easy_localization.dart';
import '../../models/vote/vote_model.dart';
import '../imports.dart';

import '../../models/base_models/base_single_res.dart';
import '../../models/community/likes_model.dart';

class UserChallengeRepo {
  static Future<BaseSingleResponse<VoteModel>> addVote({required String challengeId}) async {
    final res = await api.post(path: "/user-challenges/votes/$challengeId");
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseSingleResponse<VoteModel>.fromJson(
        resp.data,
        VoteModel(),
      );
    } else {
      String msg = (res as Right).value;
      return BaseSingleResponse(
        message: msg.tr(),
        succeeded: false,
      );
    }
  }

  static Future<BaseSingleResponse<LikesModel>> addOrRemoveLikeOnUserChallenge(
      {required String userChallengeId}) async {
    final res = await api.post(path: "/likes", body: {
      "contentId": userChallengeId,
      "contentType": 'userchallenges-reels',
    });
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseSingleResponse<LikesModel>.fromJson(
        resp.data,
        LikesModel(),
      );
    } else {
      String msg = (res as Right).value;
      return BaseSingleResponse(
        message: msg.tr(),
        succeeded: false,
      );
    }
  }

  static Future<BaseSingleResponse<VoteModel>> deleteVote({required String challengeId}) async {
    final res = await api.delete(path: "/user-challenges/votes/$challengeId");
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseSingleResponse<VoteModel>.fromJson(resp.data, VoteModel());
    } else {
      String msg = (res as Right).value;
      return BaseSingleResponse(message: msg.tr(), succeeded: false);
    }
  }
}
