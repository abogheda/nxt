part of '../imports.dart';

class HomeRepo {
  static Future<BaseListedResponse<HomeModel>> getHomePageData(String? filterID) async {
    final res = await api.get(path: filterID == null ? "/categories/home" : "/categories/home?category=$filterID");
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseListedResponse<HomeModel>.fromJson(resp.data, HomeModel());
    } else {
      String msg = (res as Right).value;
      return BaseListedResponse(message: msg.tr(), succeeded: false);
    }
  }
}
