part of '../imports.dart';

class ProfileRepo {
  static Future<BaseListedResponse<TalentModel>> getTalents({required String categories}) async {
    final res = await api.get(path: "/talents?category=$categories");
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseListedResponse<TalentModel>.fromJson(resp.data, TalentModel());
    } else {
      String msg = (res as Right).value;
      return BaseListedResponse(
        message: msg.tr(),
        succeeded: false,
      );
    }
  }
  static Future<BaseListedResponse<TalentModel>> getAllTalents() async {
    final res = await api.get(path: "/talents");
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseListedResponse<TalentModel>.fromJson(resp.data, TalentModel());
    } else {
      String msg = (res as Right).value;
      return BaseListedResponse(
        message: msg.tr(),
        succeeded: false,
      );
    }
  }


  static Future<BaseListedResponse<GetMemberShipModel>> getAllMemberShip() async {
    final res = await api.get(path: "/users/membership");
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      print("res : $resp");
      return BaseListedResponse<GetMemberShipModel>.fromJson(resp.data, GetMemberShipModel());
    } else {
      String msg = (res as Right).value;
      return BaseListedResponse(
        message: msg.tr(),
        succeeded: false,
      );
    }
  }


  static Future<BaseListedResponse<Categories>> getCategories() async {
    final res = await api.get(path: "/categories");
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseListedResponse<Categories>.fromJson(resp.data, Categories());
    } else {
      String msg = (res as Right).value;
      return BaseListedResponse(
        message: msg.tr(),
        succeeded: false,
      );
    }
  }
  static Future<BaseListedResponse<SkillsModel>> getSkills() async {
    final res = await api.get(path: "/skills");
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseListedResponse<SkillsModel>.fromJson(resp.data, SkillsModel());
    } else {
      String msg = (res as Right).value;
      return BaseListedResponse(
        message: msg.tr(),
        succeeded: false,
      );
    }
  }

  static Future<BaseSingleResponse<User>> completeProfilePatch(CompleteProfileModel model) async {
    final res = await api.patch(
      path: "/users/profile",
      mapBody: model.toJson(),
    );
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseSingleResponse<User>.fromJson(resp.data, User());
    } else {
      String msg = (res as Right).value;
      return BaseSingleResponse(
        message: msg.tr(),
        succeeded: false,
      );
    }
  }

  static Future<BaseSingleResponse<User>> completeProfilePost(CompleteProfileModel model) async {
    final res = await api.post(
      path: "/users/profile",
      body: model.toJson(),
    );
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseSingleResponse<User>.fromJson(resp.data, User());
    } else {
      String msg = (res as Right).value;
      return BaseSingleResponse(
        message: msg.tr(),
        succeeded: false,
      );
    }
  }

  static Future<BaseSingleResponse<UpdateUserData>> updatePassword({
    required String currentPassword,
    required String newPassword,
  }) async {
    final res = await api.post(
      path: '/auth/change-password',
      body: {"currentPassword": currentPassword, "password": newPassword},
    );
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseSingleResponse<UpdateUserData>.fromJson(resp.data, UpdateUserData());
    } else {
      String msg = (res as Right).value;
      return BaseSingleResponse(
        message: msg.tr(),
        succeeded: false,
      );
    }
  }

  static Future<BaseSingleResponse<User>> editProfile(CompleteProfileModel model) async {
    final res = await api.patch(
      path: "/users/profile",
      mapBody: model.toJson(),
    );
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseSingleResponse<User>.fromJson(resp.data, User());
    } else {
      String msg = (res as Right).value;
      return BaseSingleResponse(
        message: msg.tr(),
        succeeded: false,
      );
    }
  }

  static Future<BaseSingleResponse<UpdateUserData>> editUserProfileData({
    required String name,
    required String email,
    required String birthDate,
    required String avatar,
    required String nationality,
    required String country,
    required String gender,
    required int actingAgeFrom,
    required int actingAgeTo,
  }) async {
    print("object1 : $name,$email,$birthDate,$avatar,$gender,$actingAgeTo,$actingAgeFrom,$country,$nationality");

    final res = await api.patch(
      path: '/auth',
      mapBody: {
        "name": name,
        "email": email,
        "avatar": avatar,
        "birthDate": birthDate,
        "actingAgeFrom": actingAgeFrom,
        "actingAgeTo": actingAgeTo,
        "nationality": nationality,
        "profileCountry": country,
        "gender" :gender
      },
    );
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseSingleResponse<UpdateUserData>.fromJson(resp.data, UpdateUserData());
    } else {
      String msg = (res as Right).value;
      return BaseSingleResponse(message: msg, succeeded: false);
    }
  }

  static Future<BaseSingleResponse<UpdateUserData>> editUserLanguage({required String language}) async {
    final res = await api.patch(
      path: '/auth',
      mapBody: {'language': language},
    );
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseSingleResponse<UpdateUserData>.fromJson(resp.data, UpdateUserData());
    } else {
      String msg = (res as Right).value;
      return BaseSingleResponse(message: msg, succeeded: false);
    }
  }

  static Future<BaseSingleResponse<UpdateUserData>> editUserBirthDate({
    required String birthDate,
  }) async {
    final res = await api.patch(
      path: '/auth',
      mapBody: {"birthDate": birthDate},
    );
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseSingleResponse<UpdateUserData>.fromJson(resp.data, UpdateUserData());
    } else {
      String msg = (res as Right).value;
      return BaseSingleResponse(message: msg, succeeded: false);
    }
  }

  static Future<BaseSingleResponse<UsersProfileModel>> getUserData() async {
    final res = await api.get(path: "/users/profile");
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseSingleResponse<UsersProfileModel>.fromJson(resp.data, UsersProfileModel());
    } else {
      String msg = (res as Right).value;
      return BaseSingleResponse(
        message: msg.tr(),
        succeeded: false,
      );
    }
  }

  static Future<BaseSingleResponse<CommunityUser>> getUserDataById({required String userId}) async {
    final res = await api.get(path: '/users/$userId');
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseSingleResponse<CommunityUser>.fromJson(resp.data, CommunityUser());
    } else {
      String msg = (res as Right).value;
      return BaseSingleResponse(message: msg.tr(), succeeded: false);
    }
  }

  static Future<BaseSingleResponse<UsersProfileModel>> getUserDataValidate() async {
    final res = await api.get(path: '/auth/validate');
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseSingleResponse<UsersProfileModel>.fromJson(resp.data, UsersProfileModel());
    } else {
      String msg = (res as Right).value;
      return BaseSingleResponse(
        message: msg.tr(),
        succeeded: false,
      );
    }
  }

  static Future<BaseSingleResponse<user_model.User>> getUserValidate() async {
    final res = await api.get(path: '/auth/validate');
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseSingleResponse<user_model.User>.fromJson(resp.data, user_model.User());
    } else {
      String msg = (res as Right).value;
      return BaseSingleResponse(
        message: msg.tr(),
        succeeded: false,
      );
    }
  }

  static Future<BaseSingleResponse<User>> skipCompleteProfile() async {
    final res = await api.get(path: '/users/skipProfile');
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseSingleResponse<User>.fromJson(resp.data, User());
    } else {
      String msg = (res as Right).value;
      return BaseSingleResponse(
        message: msg.tr(),
        succeeded: false,
      );
    }
  }


}
