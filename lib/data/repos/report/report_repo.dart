import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:easy_localization/easy_localization.dart';
import '../../models/report/report_model.dart';

import '../../models/base_models/base_single_res.dart';
import '../imports.dart';

class ReportRepo {
  static Future<BaseSingleResponse<ReportModel>> reportItem(
      {required String contentId, required String contentType}) async {
    final res = await api.post(
      path: "/report",
      body: {"contentId": contentId.toString(), "contentType": contentType},
    );
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseSingleResponse<ReportModel>.fromJson(
        resp.data,
        ReportModel(),
      );
    } else {
      String msg = (res as Right).value;
      return BaseSingleResponse(
        message: msg.tr(),
        succeeded: false,
      );
    }
  }
}
