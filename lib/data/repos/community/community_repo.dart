part of '../imports.dart';

class CommunityRepo {
  static Future<BaseListedResponse<PostModel>> getAllPosts() async {
    final res = await api.get(path: "/posts/approved");
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseListedResponse<PostModel>.fromJson(
        resp.data,
        PostModel(),
      );
    } else {
      String msg = (res as Right).value;
      return BaseListedResponse(
        message: msg.tr(),
        succeeded: false,
      );
    }
  }

  static Future<BaseListedResponse<TagModel>> getTags() async {
    final res = await api.get(
      path: "/tags",
    );
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseListedResponse<TagModel>.fromJson(
        resp.data,
        TagModel(),
      );
    } else {
      String msg = (res as Right).value;
      return BaseListedResponse(
        message: msg.tr(),
        succeeded: false,
      );
    }
  }

  static Future<BaseSingleResponse<LikesModel>> addOrRemoveLikeOnPost({required String postId}) async {
    final res = await api.post(path: "/likes", body: {
      "contentId": postId,
      "contentType": "posts",
    });
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseSingleResponse<LikesModel>.fromJson(
        resp.data,
        LikesModel(),
      );
    } else {
      String msg = (res as Right).value;
      return BaseSingleResponse(
        message: msg.tr(),
        succeeded: false,
      );
    }
  }

  static Future<BaseSingleResponse<LikeCommentModel>> addOrRemoveLikeOnComment({required String id}) async {
    final res = await api.get(
      path: "/posts/comment-like/$id",
    );
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseSingleResponse<LikeCommentModel>.fromJson(
        resp.data,
        LikeCommentModel(),
      );
    } else {
      String msg = (res as Right).value;
      return BaseSingleResponse(
        message: msg.tr(),
        succeeded: false,
      );
    }
  }

  static Future<BaseSingleResponse<CreatePostModel>> createPost({
    required String? text,
    required List<String>? media,
    required List<String>? tags,
  }) async {
    final res = await api.post(
      path: "/posts",
      body: {
        "text": text,
        "media": media,
        "tags": tags,
      },
    );
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseSingleResponse<CreatePostModel>.fromJson(resp.data, CreatePostModel());
    } else {
      String msg = (res as Right).value;
      return BaseSingleResponse(
        message: msg.tr(),
        succeeded: false,
      );
    }
  }

  static Future<BaseSingleResponse<DeletePost>> deletePost({required String? postId}) async {
    final res = await api.delete(path: "/posts/$postId");
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseSingleResponse<DeletePost>.fromJson(resp.data, DeletePost());
    } else {
      String msg = (res as Right).value;
      return BaseSingleResponse(
        message: msg.tr(),
        succeeded: false,
      );
    }
  }

  static Future<BaseListedResponse<CommentModel>> getAllComments({required String id}) async {
    final res = await api.get(
      path: "/posts/comments/$id",
    );
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseListedResponse<CommentModel>.fromJson(
        resp.data,
        CommentModel(),
      );
    } else {
      String msg = (res as Right).value;
      return BaseListedResponse(
        message: msg.tr(),
        succeeded: false,
      );
    }
  }

  static Future<BaseSingleResponse<CommentModel>> addComment(CommentModel commentModel) async {
    final res = await api.post(
      path: "/posts/add-comment",
      body: {"post": commentModel.post, "comment": commentModel.comment, "media": commentModel.media},
    );
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseSingleResponse<CommentModel>.fromJson(resp.data, CommentModel());
    } else {
      String msg = (res as Right).value;
      return BaseSingleResponse(message: msg.tr(), succeeded: false);
    }
  }

  static Future<BaseSingleResponse<CommentModel>> removeComment(CommentModel commentModel) async {
    final res = await api.delete(
      path: "/posts/remove-comment/$id",
    );
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseSingleResponse<CommentModel>.fromJson(
        resp.data,
        CommentModel(),
      );
    } else {
      String msg = (res as Right).value;
      return BaseSingleResponse(message: msg.tr(), succeeded: false);
    }
  }

//==== Jobs methods ====
  //TODO: add the delete job here
  static Future<BaseListedResponse<JobsModel>> deleteJob() async {
    final res = await api.delete(path: "/jobs");
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseListedResponse<JobsModel>.fromJson(resp.data, JobsModel());
    } else {
      String msg = (res as Right).value;
      return BaseListedResponse(
        message: msg.tr(),
        succeeded: false,
      );
    }
  }

  static Future<BaseListedResponse<JobsModel>> getAllJobs() async {
    final res = await api.get(path: "/jobs?all=true");
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseListedResponse<JobsModel>.fromJson(resp.data, JobsModel());
    } else {
      String msg = (res as Right).value;
      return BaseListedResponse(
        message: msg.tr(),
        succeeded: false,
      );
    }
  }

  static Future<BaseListedResponse<JobsModel>> getAllJobsForOwner() async {
    final res = await api.get(path: "/jobs");
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseListedResponse<JobsModel>.fromJson(resp.data, JobsModel());
    } else {
      String msg = (res as Right).value;
      return BaseListedResponse(
        message: msg.tr(),
        succeeded: false,
      );
    }
  }

  static Future<BaseListedResponse<CountriesModel>> getCountries() async {
    final res = await api.get(path: "/country");
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseListedResponse<CountriesModel>.fromJson(resp.data, CountriesModel());
    } else {
      String msg = (res as Right).value;
      return BaseListedResponse(
        message: msg.tr(),
        succeeded: false,
      );
    }
  }

  static Future<BaseListedResponse<ApplicantsModel>> getApplicants({required String jobId, String? status}) async {
    final res = await api.get(path: "/jobsApplication/$jobId", query: {'status': status});
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseListedResponse<ApplicantsModel>.fromJson(resp.data, ApplicantsModel());
    } else {
      String msg = (res as Right).value;
      return BaseListedResponse(
        message: msg.tr(),
        succeeded: false,
      );
    }
  }

  static Future<BaseSingleResponse<UpdateApplicantModel>> updateJobApplicationStatus(
      {required String applicantId, required String status}) async {
    final res = await api.patch(path: "/jobsApplication/status/$status", body: [applicantId]);
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseSingleResponse<UpdateApplicantModel>.fromJson(resp.data, UpdateApplicantModel());
    } else {
      String msg = (res as Right).value;
      return BaseSingleResponse(
        message: msg.tr(),
        succeeded: false,
      );
    }
  }

  static Future<BaseSingleResponse<JobModel>> getJobById({required String jobId}) async {
    final res = await api.get(path: "/jobs/$jobId");
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseSingleResponse<JobModel>.fromJson(resp.data, JobModel());
    } else {
      String msg = (res as Right).value;
      return BaseSingleResponse(
        message: msg.tr(),
        succeeded: false,
      );
    }
  }

  static Future<BaseSingleResponse<AllJobModel>> getJobByIdAllLanguage({required String jobId}) async {
    final res = await api.get(path: "/jobs/$jobId", language: 'ALL');
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseSingleResponse<AllJobModel>.fromJson(resp.data, AllJobModel());
    } else {
      String msg = (res as Right).value;
      return BaseSingleResponse(
        message: msg.tr(),
        succeeded: false,
      );
    }
  }

  static Future<BaseSingleResponse<ApplyJobModel>> applyForJob({String? media, required String jobId}) async {
    final res = await api.post(path: "/jobsApplication", body: {"media": media ?? '', "job": jobId});
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseSingleResponse<ApplyJobModel>.fromJson(resp.data, ApplyJobModel());
    } else {
      String msg = (res as Right).value;
      return BaseSingleResponse(
        message: msg.tr(),
        succeeded: false,
      );
    }
  }

  static Future<BaseSingleResponse<PostJobSuccessModel>> createNewJob({required CreateJobModel postJobModel}) async {
    final res = await api.post(path: "/jobs", body: postJobModel.toJson());
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseSingleResponse<PostJobSuccessModel>.fromJson(resp.data, PostJobSuccessModel());
    } else {
      String msg = (res as Right).value;
      return BaseSingleResponse(
        message: msg.tr(),
        succeeded: false,
      );
    }
  }

  static Future<BaseSingleResponse<PostJobSuccessModel>> editJob(
      {required CreateJobModel postJobModel, required String jobId}) async {
    final res = await api.patch(path: "/jobs/$jobId", body: postJobModel.toJson());
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      if (resp.statusCode != 400) {
        return BaseSingleResponse<PostJobSuccessModel>.fromJson(resp.data, PostJobSuccessModel());
      } else {
        return BaseSingleResponse(
          message: resp.statusMessage.toString(),
          succeeded: false,
        );
      }
    } else {
      String msg = (res as Right).value;
      return BaseSingleResponse(
        message: msg.tr(),
        succeeded: false,
      );
    }
  }

  static Future<BaseListedResponse<PartnerModel>> getAllPartners() async {
    final res = await api.get(path: "/organization-types");
    if (res.isLeft()) {
      final Response resp = (res as Left).value;
      return BaseListedResponse<PartnerModel>.fromJson(resp.data, PartnerModel());
    } else {
      String msg = (res as Right).value;
      return BaseListedResponse(
        message: msg.tr(),
        succeeded: false,
      );
    }
  }
}
