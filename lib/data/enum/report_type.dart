enum ReportType {
  post("post"),
  userchallenge("userchallenge"),
  postcomment("postcomment"),
  reel("reel"),
  ;

  const ReportType(this.type);

  final String type;

  @override
  String toString() {
    return name;
  }
}
