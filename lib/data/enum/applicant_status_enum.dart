enum ApplicantStatusEnum {
  approved("Approved"),
  rejected("Rejected"),
  pending("Pending"),
  ;

  const ApplicantStatusEnum(this.type);

  final String type;

  @override
  String toString() => name;
}
