enum UploadType {
  audio("audio"),
  video("video"),
  text("text"),
  ;

  const UploadType(this.type);

  final String type;

  @override
  String toString() {
    return name;
  }
}
