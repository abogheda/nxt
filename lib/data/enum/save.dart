// // ignore_for_file: constant_identifier_names

import 'package:json_annotation/json_annotation.dart';

enum SavedItemsEnum {
  @JsonValue('posts')
  posts("posts"),
  @JsonValue('adminposts')
  adminposts("adminposts"),
  @JsonValue('nxtclasses')
  nxtclasses("nxtclasses"),
  @JsonValue('challenges')
  challenges("challenges"),
  @JsonValue('userchallenges')
  userchallenges("userchallenges"),
  @JsonValue('reels')
  reels("reels"),
  @JsonValue('reads')
  reads("reads"),
  @JsonValue('jobs')
  jobs("jobs"),
  ;

  const SavedItemsEnum(this.save);

  final String save;

  @override
  String toString() {
    return name;
  }
}
