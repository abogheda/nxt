// // ignore_for_file: constant_identifier_names

import 'package:json_annotation/json_annotation.dart';

enum ProfileStatus {
  @JsonValue('Completed')
  complete("Completed"),
  @JsonValue('Skipped')
  skipped("Skipped"),
  @JsonValue('NotCompleted')
  notCompleted("NotCompleted"),
  ;

  const ProfileStatus(this.status);

  final String status;

  @override
  String toString() {
    return name;
  }

  
}

