// ignore_for_file: constant_identifier_names

import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:nxt/utils/constants/app_colors.dart';

enum TalentCategoryType {
  @JsonValue(1)
  music("6343f07a7bebf4a58ceff37c"),
  @JsonValue(2)
  filmMaking("63417ea5b5b422d3c605cd01"),
  @JsonValue(3)
  acting("63417ebdb5b422d3c605cd04"),
  ;

  const TalentCategoryType(this.id);

  final String id;

  @override
  String toString() => name;

  Color color() {
    switch (this) {
      case TalentCategoryType.music:
        return AppColors.blueColor;
      case TalentCategoryType.filmMaking:
        return AppColors.redColor;
      case TalentCategoryType.acting:
        return AppColors.yellowColor;
    }
  }
}
