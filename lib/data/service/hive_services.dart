// ignore_for_file: depend_on_referenced_packages

import 'dart:developer';
import 'dart:io';

import 'package:hive_flutter/hive_flutter.dart';
import 'remote_service.dart';
import '../../utils/constants/app_const.dart';
import '../models/user_model.dart';
import 'package:path_provider/path_provider.dart' show getTemporaryDirectory;

class HiveHelper {
  static final Box _appBox = Hive.box('app_memory');
  static final Box _userBox = Hive.box('user_memory');

  static Box get appBox => _appBox;
  static Box get userBox => _userBox;

  static Future<void> init() async {
    await Hive.initFlutter();
    await Hive.openBox("app_memory");
    await Hive.openBox('user_memory');
  }

//==== Theme Caching Methods ====
  static Future<void> cacheTheme({required bool? value}) async {
    await writeAppBox(key: AppConst.kIsDark, value: value);
  }

  static Future<bool> get getTheme async {
    bool isDark;
    if (_appBox.containsKey(AppConst.kIsDark)) {
      isDark = await _appBox.get(AppConst.kIsDark);
    } else {
      isDark = false;
    }
    return isDark;
  }

  static Future<void> cacheNotificationsIsNotAllowed({required bool? value}) async {
    await writeAppBox(key: 'allowNotifications', value: value);
  }

  static Future<bool> get getNotificationsStatus async {
    bool isNotificationAllowed;
    if (_appBox.containsKey('allowNotifications')) {
      isNotificationAllowed = await _appBox.get('allowNotifications');
    } else {
      isNotificationAllowed = true;
    }
    return isNotificationAllowed;
  }

//==== OnBoarding Caching Methods ====

  static Future<void> cacheOnBoarding({required bool? value}) async {
    await writeAppBox(key: 'isFirstTime', value: value);
  }

  static Future<bool> get getOnBoarding async {
    bool isFirstTime;
    if (_appBox.containsKey('isFirstTime')) {
      isFirstTime = await _appBox.get('isFirstTime');
    } else {
      await _appBox.put('isFirstTime', true);
      isFirstTime = await _appBox.get('isFirstTime');
    }
    return isFirstTime;
  }

//==== Complete Profile Caching Methods ====
  static Future<bool> get isNotCompleted async {
    if (getUserInfo == null) {
      return true;
    } else if (getUserInfo!.user!.profileStatus == 'NotCompleted') {
      return true;
    } else if (getUserInfo!.user!.profileStatus != 'NotCompleted') {
      return false;
    }
    return true;
  }

  static bool get isRegularUser {
    if (getUserInfo == null) {
      return true;
    } else if (getUserInfo!.user!.role == 'Organization' || getUserInfo!.user!.role == 'Expert') {
      return false;
    }
    return true;
  }

  static bool get isEgypt {
    if (getCountry == null) {
      return false;
    }
    if (getCountry == 'Egypt') {
      return true;
    }
    return false;
  }

//==== User Caching Methods ====
  static Future<void> logout() async {
    if (APIService.dio != null) {
      APIService.resetHeaderToken();
    }
    await _userBox.deleteAll([
      'userModel',
      'token',
      'name',
      'avatar',
      'birthDate',
      'keepMeLoggedIn',
    ]);
    await _userBox.clear();
    String appDir = (await getTemporaryDirectory()).path;
    Directory(appDir).delete(recursive: true);
  }

  static Future<void> cacheUserInfo({
    required String token,
    required UserModel userModel,
  }) async {
    await cacheUserToken(token: token);
    await cacheUserModel(userModel: userModel);
  }

  static Future<void> cacheUserModel({required UserModel userModel}) async =>
      await _userBox.put('userModel', userModel.toJson());

  static Future<void> cacheUserToken({required String token}) async => await _userBox.put('token', token);
  static Future<void> cacheVerticals({required int length}) async => await _userBox.put('length', length);
  static Future<void> cacheFromHome({required bool fromHome}) async => await _appBox.put('fromHome', fromHome);
  static bool? get fromHome => _appBox.get("fromHome");

  static int? get getCacheVerticals => _userBox.get("length");
  static String? get getUserToken => _userBox.get('token');

  static Future<void> cacheKeepMeLoggedIn({required bool value}) async => await _userBox.put('keepMeLoggedIn', value);

  static bool? get getKeepMeLoggedIn => _userBox.get('keepMeLoggedIn');

  static Future<void> cacheProfileInfo({
    required String? name,
    required String? avatar,
    required String? birthDate,
     String? nationality,
     String? country,
     String? gender,
     int? actingAgeFrom,
     int? actingAgeTo,
  }) async {
    await _userBox.putAll({
      'name': name,
      'avatar': avatar,
      'birthDate': birthDate,
      "nationality":nationality,
      "country":country,
      "gender":gender,
      "actingAgeFrom":actingAgeFrom,
      "actingAgeTo":actingAgeTo,

    });
  }

  static Future<void> cacheUserName({required String name}) async => await _userBox.put('name', name);

  static String? get getUserName => _userBox.get('name');

  static Future<void> cacheUserAvatar({required String avatar}) async => await _userBox.put('avatar', avatar);

  static String? get getUserAvatar => _userBox.get('avatar');

  static Future<void> cacheUserBirthDate({required String birthDate}) async =>
      await _userBox.put('birthDate', birthDate);

  static String? get getUserBirthDate => _userBox.get('birthDate');

  static Future<void> cacheCountry({required String country}) async => await _userBox.put('country', country);

  static String? get getCountry => _userBox.get('country');

  static Future<void> checkAndCacheCountry({required String? userPhone}) async {
    if (userPhone == null) return;
    bool startsWith({required String firstThreeDigits}) => userPhone.startsWith(firstThreeDigits);
    if (startsWith(firstThreeDigits: '+2010') ||
        startsWith(firstThreeDigits: '+2011') ||
        startsWith(firstThreeDigits: '+2012') ||
        startsWith(firstThreeDigits: '+2015')) {
      await HiveHelper.cacheCountry(country: 'Egypt');
    } else {
      await HiveHelper.userBox.delete('country');
    }
  }

  static bool get isLogged => getUserInfo != null;

  static UserModel? get getUserInfo {
    UserModel? userModel;
    if (_userBox.containsKey('userModel')) {
      try {
        userModel = UserModel.fromJson(_userBox.get('userModel'));
      } catch (e) {
        log('|==|' * 22);
        log('HiveHelper.getUserInfo');
        log(e.toString());
        log('|==|' * 22);
      }
    }
    return userModel;
  }

//==== App Caching Methods ====
  static Future<void> writeAppBox({
    required String key,
    required dynamic value,
  }) async =>
      await _appBox.put(key, value);

  static bool hasDataUserBox({required String key}) => _userBox.containsKey(key);
}
