import 'package:dartz/dartz.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'hive_services.dart';
import 'package:dio/dio.dart';
import '../../utils/helpers/connectivity_helper.dart';
import '../../utils/helpers/popup_helper.dart';

import '../../utils/constants/app_const.dart';

class APIService {
  static Dio? dio;
  APIService(String url) {
    dio = Dio(BaseOptions(
      baseUrl: url,
      // connectTimeout: 5 * 1000,
      // receiveTimeout: 5 * 1000,
      headers: {
        "Content-Type": 'application/json',
        'accept-language': AppConst.isEn ? 'en' : 'ar',
      },
      validateStatus: (val) => val! < 500,
    ));
    setHeaderToken(token: HiveHelper.getUserToken);
  }
  static void setHeaderToken({required String? token}) {
    if (token != null) {
      dio!.options.headers["Authorization"] = "Bearer $token";
    }
  }

  static void resetHeaderToken() => dio!.options.headers["Authorization"] = '';

  Future<Either<Response, String>> get({
    required String path,
    Map<String, dynamic>? query,
    String? language,
  }) async {
    if (!(await hasNetworkConnection())) {
      PopupHelper.showBasicSnack(msg: 'netError'.tr(), color: Colors.red);
      return Right('netError'.tr());
    }

    try {
      final res = await dio!.get(
        path,
        queryParameters: query,
        options: Options(
          headers: {
            "Content-Type": 'application/json',
            'accept-language': language ?? (AppConst.isEn ? 'en' : 'ar'),
          },
        ),
      );
      return Left(res);
    } catch (e) {
      return _catchError(e);
    }
  }

  Future<Either<Response, String>> post({
    required String path,
    Map<String, dynamic>? query,
    Map<String, dynamic>? body,
    List<Map<String, dynamic>>? listedBody,
  }) async {
    if (!(await hasNetworkConnection())) {
      PopupHelper.showBasicSnack(msg: 'netError'.tr(), color: Colors.red);
      return Right('netError'.tr());
    }
    try {
      final res = await dio!.post(
        path,
        queryParameters: query,
        data: body ?? listedBody,
      );
      return Left(res);
    } catch (e) {
      return _catchError(e);
    }
  }

  Future<Either<Response, String>> put({
    required String path,
    Map<String, dynamic>? query,
    Map<String, dynamic>? body,
    List<Map<String, dynamic>>? listedBody,
  }) async {
    if (!(await hasNetworkConnection())) {
      PopupHelper.showBasicSnack(msg: 'netError'.tr(), color: Colors.red);
      return Right('netError'.tr());
    }
    try {
      final res = await dio!.put(
        path,
        queryParameters: query,
        data: body ?? listedBody,
      );
      return Left(res);
    } catch (e) {
      return _catchError(e);
    }
  }

  Future<Either<Response, String>> patch({
    required String path,
    Map<String, dynamic>? query,
    Map<String, dynamic>? mapBody,
    dynamic body,
    List<Map<String, dynamic>>? listedMapBody,
  }) async {
    if (!(await hasNetworkConnection())) {
      PopupHelper.showBasicSnack(msg: 'netError'.tr(), color: Colors.red);
      return Right('netError'.tr());
    }
    try {
      final res = await dio!.patch(
        path,
        queryParameters: query,
        data: body ?? mapBody ?? listedMapBody,
      );
      return Left(res);
    } catch (e) {
      return _catchError(e);
    }
  }

  Future<Either<Response, String>> delete({
    required String path,
    Map<String, dynamic>? query,
    Map<String, dynamic>? body,
  }) async {
    if (!(await hasNetworkConnection())) {
      return Right("netError".tr());
    }
    try {
      final res = await dio!.delete(
        path,
        queryParameters: query,
        data: body,
      );
      return Left(res);
    } catch (e) {
      return _catchError(e);
    }
  }

  Right<Response, String> _catchError(Object e) {
    late String msg;
    if (e is DioError) {
      msg = e.message!;
    } else {
      msg = e.toString();
    }
    return Right(msg);
  }
}
