import 'package:get_it/get_it.dart';
import '../../data/service/remote_service.dart';
import '../constants/app_const.dart';

final getIt = GetIt.instance;

void locatorSetup() {
  getIt.registerLazySingleton<APIService>(() => APIService  (AppConst.isProduction
      ? "${AppConst.baseProdUrl}/${AppConst.apiVersion}"
      : "${AppConst.baseDevUrl}/${AppConst.apiVersion}"));
}
