import 'dart:developer';
import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../config/router/router.dart';
import '../../view/screens/challenges/cubit/challenge_cubit.dart';
import '../../view/screens/challenges/screens/view_challenge/view_challenge_screen.dart';
import '../constants/app_const.dart';

class DynamicLinkService {
  Future<Uri> createDynamicLink({required String url}) async {
    final DynamicLinkParameters parameters = DynamicLinkParameters(
        link: Uri.parse(AppConst.isProduction ? "${AppConst.productionUrl}/$url" : "${AppConst.developmentUrl}/$url"),
        uriPrefix: 'https://projectnxt.page.link',
        androidParameters: const AndroidParameters(packageName: "me.rebelsunited.projectnxt", minimumVersion: 30),
        iosParameters:
            const IOSParameters(bundleId: 'smartlink.com.nxtapp', appStoreId: '6444449199', minimumVersion: '1.0.1'));
    final dynamicLink = await FirebaseDynamicLinks.instance.buildShortLink(parameters);
    final shortUrl = dynamicLink.shortUrl;
    log("shortUrl: ${shortUrl.toString()}");
    return shortUrl;
  }

  Future handleDynamicLinks() async {
    final PendingDynamicLinkData? data = await FirebaseDynamicLinks.instance.getInitialLink();
    _handleDeepLink(data: data, onFinished: () {});
    FirebaseDynamicLinks.instance.onLink
        .listen((dynamicLinkData) async => await navigateToScreen(linkPath: dynamicLinkData.link.path))
        .onError((error) {
      log('onLink error');
      log(error.message);
    });
  }

  void _handleDeepLink({required PendingDynamicLinkData? data, Function()? onFinished}) async {
    final Uri? deepLink = data?.link;
    if (deepLink != null) await navigateToScreen(linkPath: deepLink.path);
  }

  Future<void> navigateToScreen({required String linkPath}) async {
    if (linkPath.contains('/challenge/view-challenge/')) {
      final challengeIdFromDynamicLink = linkPath.split('view-challenge')[1];
      await MagicRouter.navigateTo(BlocProvider.value(
        value: ChallengeCubit()..onInit(challengeId: challengeIdFromDynamicLink.toString()),
        child: ViewChallengeScreen(challengeId: challengeIdFromDynamicLink.toString()),
      ));
    }
  }
}
