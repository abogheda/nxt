import 'package:google_sign_in/google_sign_in.dart';

class GoogleSIgnInApi {
  static Future<GoogleSignInAuthentication?> login() async {
    final GoogleSignIn googleSignIn = GoogleSignIn(scopes: [
      'email',
      'https://www.googleapis.com/auth/contacts.readonly',
    ]);
    final GoogleSignInAccount? googleSignInAccount = await googleSignIn.signIn();
    GoogleSignInAuthentication googleSignInAuthentication = await googleSignInAccount!.authentication;
    return googleSignInAuthentication;
  }
}
