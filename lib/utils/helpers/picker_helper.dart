import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:images_picker/images_picker.dart';
import 'package:nxt/utils/constants/app_const.dart';
import 'package:open_filex/open_filex.dart';
import 'package:image_picker/image_picker.dart';
import 'package:flutter_datetime_picker_bdaya/flutter_datetime_picker_bdaya.dart';

import '../../config/router/router.dart';

class PickerHelper {
  static void openFile({required String path}) => OpenFilex.open(path);

  static Future<File?> pickTextFile() async {
    File? file;
    FilePickerResult? result = await FilePicker.platform.pickFiles(
      type: FileType.custom,
      allowMultiple: false,
      allowedExtensions: ['pdf', 'doc', 'docx', 'txt'],
    );
    if (result != null) {
      file = File(result.files.single.path!);
      print("file is : $file");
      return file;
    } else {
      return null;
    }
  }

  static Future<File?> pickAudioFile() async {
    File? file;
    FilePickerResult? result = await FilePicker.platform.pickFiles(
      allowMultiple: false,
      type: FileType.custom,
      allowedExtensions: ['mp3', 'm4a', 'wav', 'caf'],
    );
    if (result != null) {
      file = File(result.files.single.path!);
      return file;
    } else {
      return null;
    }
  }

  static Future<File?> pickMP4File() async {
    File? file;
    FilePickerResult? result = await FilePicker.platform.pickFiles(
      allowMultiple: false,
      type: FileType.custom,
      allowedExtensions: ['mp4'],
    );
    if (result != null) {
      file = File(result.files.single.path!);
      return file;
    } else {
      return null;
    }
  }

  static Future<File?> pickVideoFile() async {
    final video = await ImagesPicker.pick(
      count: 1,
      maxTime: 90,
      pickType: PickType.video,
    );
    if (video != null) return File(video.first.path);
    return null;
  }

  static Future<File?> pickFrontCameraImage() async {
    final picker = ImagePicker();
    XFile? imageFile = await picker.pickImage(
      source: ImageSource.camera,
      preferredCameraDevice: CameraDevice.front,
    );
    if (imageFile != null) return File(imageFile.path);
    return null;
  }

  static Future<File?> pickGalleryImage() async {
    final photo = await ImagesPicker.pick(count: 1, pickType: PickType.image);
    if (photo != null) return File(photo.first.path);
    return null;
  }

  static Future<DateTime?> pickDate({
    String? dateFormat,
    DateTime? maxTime,
    DateTime? minTime,
    DateTime? currentTime,
    DatePickerTheme? theme,
    dynamic Function()? onCancel,
    locale,
  }) async {
    DateTime? datePicked;
    datePicked = await DatePicker.showDatePicker(
      MagicRouter.currentContext!,
      maxTime: maxTime,
      minTime: minTime,
      showTitleActions: true,
      locale: locale ?? AppConst.isEn ? LocaleType.en : LocaleType.ar,
      onCancel: onCancel,
      onChanged: (date) => datePicked = date,
      onConfirm: (date) => datePicked = date,
      currentTime: currentTime ?? DateTime.now(),
      theme: theme ??
          DatePickerTheme(
            doneStyle: TextStyle(
              fontSize: 16,
              color: Colors.blue,
              fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal',
            ),
            cancelStyle: TextStyle(
              fontSize: 16,
              color: Colors.black54,
              fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal',
            ),
            itemStyle: TextStyle(
              fontSize: 18,
              color: const Color(0xFF000046),
              fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal',
            ),
          ),
    );
    return datePicked;
  }
}
