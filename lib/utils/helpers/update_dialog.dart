// ignore_for_file: use_build_context_synchronously

import 'dart:async';
import 'dart:developer';
import 'dart:io';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:nxt/utils/constants/app_const.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:new_version_plus/new_version_plus.dart';
import '../../view/screens/more/more_widgets/custom_logout_dialog.dart';
import 'connectivity_helper.dart';

void checkNewVersion(BuildContext context) async {
  NewVersionPlus newVersion = NewVersionPlus(
    androidId: 'me.rebelsunited.projectnxt',
    iOSId: '6444449199',
  );
  if (Platform.isAndroid) {
    Timer(const Duration(milliseconds: 800), () async {
      if (await hasNetworkConnection()) {
        try {
          final status = await newVersion.getVersionStatus();
          if (status != null) {
            if (status.canUpdate) {
              showDialog(
                barrierDismissible: false,
                context: context,
                builder: (BuildContext context) => UpdateDialog(appLink: status.appStoreLink),
              );
            }
          }
        } catch (e) {
          log('|==|' * 22);
          log('checkNewVersion');
          log(e.toString());
          log('|==|' * 22);
        }
      }
    });
  }
}

class UpdateDialog extends StatefulWidget {
  final String appLink;

  const UpdateDialog({Key? key, required this.appLink}) : super(key: key);

  @override
  State<UpdateDialog> createState() => _UpdateDialogState();
}

class _UpdateDialogState extends State<UpdateDialog> {
  @override
  void dispose() {
    SystemNavigator.pop();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      duration: const Duration(milliseconds: 500),
      curve: Curves.fastLinearToSlowEaseIn,
      child: AlertDialog(
        actionsAlignment: MainAxisAlignment.center,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15.0)),
        content: Text(
          "updateApp".tr(),
          textAlign: TextAlign.center,
          style:
              Theme.of(context).textTheme.titleMedium!.copyWith(fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal'),
        ),
        actions: [
          SimpleButton(
            text: "exit".tr(),
            invertedColors: true,
            onPressed: () => SystemNavigator.pop(animated: true),
          ),
          const SizedBox(width: 4),
          SimpleButton(
            text: "update".tr(),
            onPressed: () async => await launchUrl(
              Uri.parse(widget.appLink),
              mode: LaunchMode.externalApplication,
            ),
          ),
        ],
      ),
    );
  }
}
