import 'package:easy_localization/easy_localization.dart';
import 'package:url_launcher/url_launcher.dart';

class Validators {
  static String? generalField(String? value) {
    if (value!.isEmpty) {
      return 'empty_field'.tr();
    } else {
      return null;
    }
  }

  static String? email(String? email) {
    if (email!.isEmpty) {
      return 'validation.email_empty'.tr();
    } else if (!email.contains('@') || !email.contains('.')) {
      return 'EX: example@mail.com';
    } else {
      return null;
    }
  }

  static String? weight(String? weight) {
    if (weight == null || weight.isEmpty) {
      return ("empty_field".tr());
    } else if (int.parse(weight) > 300) {
      return ("weight_max".tr());
    } else if (int.parse(weight) <= 0) {
      return ("weight0".tr());
    } else {
      return null;
    }
  }

  static String? height(String? height) {
    if (height == null || height.isEmpty) {
      return ("empty_field".tr());
    } else if (int.parse(height) > 250) {
      return ("height_max".tr());
    } else if (int.parse(height) <= 0) {
      return ("height0".tr());
    }
    return null;
  }

  static Future<bool> checkIfUrlIsValid({required String url}) async {
    if (await canLaunchUrl(Uri.parse(url))) {
      return true;
    }
    return false;
  }

  static String? password(String? pass) {
    //  RegExp regex = RegExp(r'^(?=.*?[A-Z])(?=.*?[a-zA-Z0-9]+$)(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#$%^&*(),.?":{}|<>])');
    bool hasSpecialCharacters = pass!.contains(RegExp(r'[@#$%^&*(),.?!":{}|<>]'));
    bool hasUppercase = pass.contains(RegExp(r'[A-Z]'));
    // bool hasLowercase = pass.contains(RegExp(r'[a-z]'));
    bool hasDigits = pass.contains(RegExp(r'[0-9]'));
    // } else if (!regex.hasMatch(pass)) {
    if (pass.isEmpty) {
      return "validation.password_empty".tr();
    } else if (pass.length < 8) {
      return "validation.password_length".tr();
    } else if (pass.contains(' ')) {
      return "validation.white_space".tr();
    } else if (!hasSpecialCharacters) {
      return "validation.password_special".tr();
    } else if (!hasUppercase) {
      return "validation.password_upper".tr();
    }
    // else if (!hasLowercase) {
    //   return "validation.password_lower".tr();
    // }
    else if (!hasDigits) {
      return "validation.password_digits".tr();
    } else {
      return null;
    }
  }

  static String? phone(String? phone) {
    if (phone!.isEmpty) {
      return 'validation.phone_empty'.tr();
    } else if (phone.trim().length < 11 && !phone.trim().startsWith("01") && phone.runtimeType != int) {
      return "validation.phone_valid".tr();
    } else {
      return null;
    }
  }
}
// import 'package:easy_localization/easy_localization.dart';
// import 'package:flutter/material.dart';
// import '../../utils/constants/resources.dart';
//
// class ErrorStateWidget extends StatelessWidget {
//   const ErrorStateWidget({Key? key, required this.onRefresh}) : super(key: key);
//   final Future<void> Function() onRefresh;
//   @override
//   Widget build(BuildContext context) {
//     return RefreshIndicator(
//       onRefresh: onRefresh,
//       child: ListView(
//         padding: EdgeInsets.only(left: width * 0.03, right: width * 0.03, top: height * 0.33),
//         children: [
//           Text(
//             'errorOccurred'.tr(),
//             textAlign: TextAlign.center,
//             style: AppTextStyles.huge44.copyWith(
//               fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal',
//             ),
//           ),
//         ],
//       ),
//     );
//   }
// }
