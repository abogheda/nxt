import 'package:facebook_app_events/facebook_app_events.dart';

class FacebookEventsHelper {
  static final _facebookAppEvents = FacebookAppEvents();

  static Future<void> logEvent({required String eventName, Map<String, dynamic>? params}) async {
    await _facebookAppEvents.logEvent(
      name: eventName,
      parameters: params,
    );
    // final id= await _facebookAppEvents.getApplicationId();
    // print(id);
  }
}
