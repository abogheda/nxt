import 'dart:async';
import 'dart:developer';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:nxt/utils/helpers/utils.dart';

import 'flutter_local_notifications.dart';

class NotificationHelper {
  static final NotificationHelper instance = NotificationHelper._();
  static final _messaging = FirebaseMessaging.instance;
  NotificationHelper._();

  Future<void> setupNotification() async {
    await _messaging.requestPermission(
      alert: true,
      announcement: false,
      badge: true,
      carPlay: false,
      criticalAlert: false,
      provisional: true,
      sound: true,
    );

    _messaging.setForegroundNotificationPresentationOptions(alert: false, badge: false, sound: false);

    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      log('!=!=!=' * 22);
      log('1');
      log('NotificationHelper.FirebaseMessaging.onMessage');
      log('$message');
      log('!=!=!=' * 22);
      Future.delayed(
        const Duration(milliseconds: 300),
        () {
          FlutterLocalNotificationUtils.instance.showInstantNotification(
              id: Utils.createUniqueId(),
              title: '${message.notification!.title!} 1',
              body:
                  '${message.data} ${message.from} ${message.messageType} ${message.senderId} ${message.ttl} ${message.category} 1');
        },
      );
    });

    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
      //TODO: add the switch case for navigation here
    });

    FirebaseMessaging.instance.getInitialMessage().then((message) {
      if (message != null) {
        //TODO: add the switch case for navigation here
      }
    });
  }

  Future<String?> getToken() async {
    return await _messaging.getToken();
  }
}
