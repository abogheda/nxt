import 'package:flutter/material.dart';
import '../../config/router/router.dart';
import 'resources.dart';

class AppTextStyles {
  static final regular12 = TextStyle(
    fontSize: 12,
    fontWeight: FontWeight.w600,
    fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal',
    color: Theme.of(MagicRouter.currentContext!).primaryColor,
  );
  static final regular16 = TextStyle(
    fontSize: 16,
    color: Theme.of(MagicRouter.currentContext!).primaryColor,
  );

  static final medium14 = TextStyle(
    fontSize: 14,
    fontWeight: FontWeight.w500,
    fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal',
    color: Theme.of(MagicRouter.currentContext!).primaryColor,
  );

  static final bold12 = TextStyle(
    fontSize: 12,
    fontWeight: FontWeight.bold,
    fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal',
    color: Theme.of(MagicRouter.currentContext!).primaryColor,
  );
  static final bold14 = TextStyle(
    fontSize: 14,
    fontWeight: FontWeight.bold,
    fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal',
    color: Theme.of(MagicRouter.currentContext!).primaryColor,
  );
  static final bold13 = TextStyle(
    fontSize: 13,
    fontWeight: FontWeight.bold,
    fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal',
    color: Theme.of(MagicRouter.currentContext!).primaryColor,
  );
  static final bold18 = TextStyle(
    fontSize: 18,
    fontWeight: FontWeight.bold,
    fontFamily: AppConst.isEn ? 'BebasNeue' : 'Tajawal',
    color: Theme.of(MagicRouter.currentContext!).primaryColor,
  );

  static final black13 =
      TextStyle(fontSize: 13, color: Theme.of(MagicRouter.currentContext!).primaryColor, fontWeight: FontWeight.w900);
  static final black14 = TextStyle(
      fontSize: 14,
      color: Theme.of(MagicRouter.currentContext!).primaryColor,
      fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal',
      fontWeight: FontWeight.w900);
  static final black15 =
      TextStyle(fontSize: 15, color: Theme.of(MagicRouter.currentContext!).primaryColor, fontWeight: FontWeight.w900);
  static final black18 =
      TextStyle(fontSize: 18, color: Theme.of(MagicRouter.currentContext!).primaryColor, fontWeight: FontWeight.w900);

  static final semiBold13 =
      TextStyle(fontSize: 13, fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal', fontWeight: FontWeight.bold);
  static final semiBold14 =
      TextStyle(fontSize: 14, fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal', fontWeight: FontWeight.bold);
  static final semiBold16 =
      TextStyle(fontSize: 16, fontFamily: AppConst.isEn ? 'Montserrat' : 'Tajawal', fontWeight: FontWeight.bold);

  static final huge75 = TextStyle(fontSize: 75, fontFamily: AppConst.isEn ? 'BebasNeue' : 'Tajawal', height: .88);
  static final huge44 = TextStyle(fontSize: 44, fontFamily: AppConst.isEn ? 'BebasNeue' : 'Tajawal', height: .88);
  static final huge36 = TextStyle(fontSize: 36, fontFamily: AppConst.isEn ? 'BebasNeue' : 'Tajawal', height: .88);
  static final huge30 = TextStyle(fontSize: 30, fontFamily: AppConst.isEn ? 'BebasNeue' : 'Tajawal', height: .88);
  static final huge20 = TextStyle(fontSize: 20, fontFamily: AppConst.isEn ? 'BebasNeue' : 'Tajawal', height: .88);
  static final huge17 = TextStyle(fontSize: 17, fontFamily: AppConst.isEn ? 'BebasNeue' : 'Tajawal', height: .88);
}
