import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

import '../../config/router/router.dart';
import '../helpers/connectivity_helper.dart';

class AppConst {
  static bool isProduction = false;
  // static bool isProduction = true;
  static const baseDevUrl = "https://nxt.smartlink.lol/api";
  static const baseProdUrl = "https://api.projectnxt.app/api";
  static const developmentUrl = "https://projectnxt.smartlink.lol";
  static const productionUrl = "https://projectnxt.app";
  static const apiVersion = "v1";
  static const kIsDark = "isDark";
  static const token = "token";
  static const userData = "userData";
  static const notCompleted = "NotCompleted";
  static const skinColors = ["Very Fair", "Fair", "Medium", "Olive", "Brown", "Dark Brown"];
  static const hairColors = ["Black", "Dark Brown", "Brown", "Gold", "Grey", "Red", "Dark Red", "Bald", "Hijab"];
  static const eyeColors = ["Honey", "Brown", "Light Blue", "Blue", "Green", "Gray"];
  static const internetConnectionError = "internetConnectionError";


 static const cloudRegion = 'europe-west1';
  static  const storeKeyConsumable = 'sub1';
  static const storeKeyUpgrade = 'pro';
  static  const storeKeySubscription = 'dash_subscription_doubler';

  static bool get isEn => MagicRouter.currentContext!.locale.languageCode == 'en';
}

class EndPoints {
  static const login = "/auth/signin";
  static const confirmPhone = "/auth/confirm-phone";
  static const restPass = "/auth/rest-pass";
}

extension IsEn on Widget {
  bool get isEn => MagicRouter.currentContext!.locale.languageCode == 'en';
}

extension Height on Widget {
  double get height => MediaQuery.of(MagicRouter.currentContext!).size.height;
}

extension Width on Widget {
  double get width => MediaQuery.of(MagicRouter.currentContext!).size.width;
}

extension HasInternetConnection on Widget {
  Future<bool> get hasInternetConnection async => await hasNetworkConnection();
}

extension StringExtension on String {
  String get capitalize => isNotEmpty ? this[0].toUpperCase() + substring(1) : this;
}

extension HexString on String {
  int toHex() => int.parse(replaceAll('#', '0xff'));
}

extension DurationExtensions on Duration {
  String toHoursMinutes() {
    String twoDigitMinutes = _toTwoDigits(inMinutes.remainder(60));
    return "${_toTwoDigits(inHours)}:$twoDigitMinutes";
  }

  String toHoursMinutesSeconds() {
    String twoDigitMinutes = _toTwoDigits(inMinutes.remainder(60));
    String twoDigitSeconds = _toTwoDigits(inSeconds.remainder(60));
    return "${_toTwoDigits(inHours)}:$twoDigitMinutes:$twoDigitSeconds";
  }

  String _toTwoDigits(int n) {
    if (n >= 10) return "$n";
    return "0$n";
  }
}
