import 'package:flutter/material.dart';

class AppColors {
  static const redColor = Color(0xFFF91515);
  static const yellowColor = Color(0xFFFFCE00);
  static const blueColor = Color(0xFF1567F9);
  static const greyTxtColor = Color(0xFF727272);
  static const iconsColor = Color(0xFFA5A5A5);
  static const borderGreyColor = Color(0xFFE8E6EA);
  static const greyNavbar = Color(0xFF808080);
  static const greyOutText = Color(0xFF707070);
  static const semiBlack = Color(0xFF555555);
  static const rejectRedColor = Color(0xFFFF8E8E);
  static const agreeGreenColor = Color(0xFF83D599);
}
