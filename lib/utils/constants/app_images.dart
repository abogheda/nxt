const String placeHolderUrl =
    'https://upload.wikimedia.org/wikipedia/commons/thumb/3/3f/Placeholder_view_vector.svg/681px-Placeholder_view_vector.svg.png';
const String userAvatarPlaceHolderUrl =
    'https://upload.wikimedia.org/wikipedia/commons/7/7c/Profile_avatar_placeholder_large.png';

class AppImages {
  static String getEyeImage(String name) => "assets/images/${name}_eye.webp";
  static String getSkinImage(String name) => "assets/images/${name}_skin.webp";
  static String getHairImage(String name) => "assets/images/${name}_hair.webp";
}
